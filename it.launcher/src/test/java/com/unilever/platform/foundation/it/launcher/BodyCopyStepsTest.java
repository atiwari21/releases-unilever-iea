package com.unilever.platform.foundation.it.launcher;

import org.json.JSONException;
import org.junit.Assert;
import org.junit.Test;
/**
* JUnit test class for body copy steps . 
* @author 
*
*/
public class BodyCopyStepsTest extends JUnitBaseClass {

   //final static String COMPONENT_PATH = "/content/junittest/en/package4/jcr:content/flexi_hero_par/bodycopysteps.data.json";
   final static String COMPONENT_PATH_WITH_IMAGE = "/content/junittest/en/package3_test/jcr:content/flexi_hero_par/bodycopysteps.data.json";

   /* (non-Javadoc)
    * @see com.unilever.platform.foundation.it.launcher.JUnitBaseClass#test()
    */
/*  @Override
   @Test
   public void testContent() throws JSONException {
       String expectedJSONString= getStaticFileAsString("adaptiveimage.json");
       String actualJSON = getComponentJsonAsString(COMPONENT_PATH);
       JSONAssert.assertEquals(expectedJSONString, actualJSON,true);
   }
   */
   @Override
   @Test
   public void testSchema() throws JSONException {
       Assert.assertEquals(validateSchema("schema/bodycopysteps.txt", COMPONENT_PATH_WITH_IMAGE),"true");
      // Assert.assertEquals(validateSchema("schema/bodycopysteps_with.txt",COMPONENT_PATH_WITH_IMAGE),"true");
   }
}

