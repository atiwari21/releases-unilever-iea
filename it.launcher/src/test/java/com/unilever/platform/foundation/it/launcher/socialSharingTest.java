package com.unilever.platform.foundation.it.launcher;

import org.json.JSONException;
import org.junit.Assert;

public class socialSharingTest extends JUnitBaseClass {

	final static String COMPONENT_PATH="/content/junittest/jcr:content/flexi_hero_par/socialsharing.data.json";
	@Override
	public void testSchema() throws JSONException {
		Assert.assertEquals(validateSchema("schema/socialSharing.txt", COMPONENT_PATH),"true");
		
	}

}
