package com.unilever.platform.foundation.it.launcher;

import org.json.JSONException;
import org.junit.Assert;
import org.junit.Test;

public class ArticleListingTest extends JUnitBaseClass{
    final static String COMPONENT_PATH = "/content/junittest/en/package5/jcr:content/flexi_hero_par/articlelisting.data.json";    
   
   
    /* (non-Javadoc)
     * @see com.unilever.platform.foundation.it.launcher.JUnitBaseClass#testSchema()
     */
    @Override
    @Test
    public void testSchema() throws JSONException {
        Assert.assertEquals(validateSchema("schema/articleListing.txt", COMPONENT_PATH),"true");
    }
}
