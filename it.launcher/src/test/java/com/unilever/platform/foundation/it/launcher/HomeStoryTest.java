package com.unilever.platform.foundation.it.launcher;

import org.json.JSONException;
import org.junit.Assert;
import org.junit.Test;

/**
* JUnit test class for product story . 
* @author 
*
*/
public class HomeStoryTest extends JUnitBaseClass{

    final static String COMPONENT_PATH = "/content/junittest/en/package3_test/jcr:content/flexi_hero_par/homestory.data.json";

    /* (non-Javadoc)
     * @see com.unilever.platform.foundation.it.launcher.JUnitBaseClass#test()
     */
/*  @Override
    @Test
    public void testContent() throws JSONException {
        String expectedJSONString= getStaticFileAsString("adaptiveimage.json");
        String actualJSON = getComponentJsonAsString(COMPONENT_PATH);
        JSONAssert.assertEquals(expectedJSONString, actualJSON,true);
    }
    */
    @Override
    @Test
    public void testSchema() throws JSONException {
        Assert.assertEquals(validateSchema("schema/homeStory.txt", COMPONENT_PATH),"true");
    }
}
