package com.unilever.platform.foundation.it.launcher;

import org.json.JSONException;
import org.junit.Assert;
import org.junit.Test;

public class FeaturedArticleTest extends JUnitBaseClass{
	final static String COMPONENT_PATH = "/content/junittest/en/package5/jcr:content/flexi_hero_par/featuredarticle.data.json";

	   /* (non-Javadoc)
	    * @see com.unilever.platform.foundation.it.launcher.JUnitBaseClass#test()
	    */
	/*  @Override
	   @Test
	   public void testContent() throws JSONException {
	       String expectedJSONString= getStaticFileAsString("adaptiveimage.json");
	       String actualJSON = getComponentJsonAsString(COMPONENT_PATH);
	       JSONAssert.assertEquals(expectedJSONString, actualJSON,true);
	   }
	   */
	   @Override
	   @Test
	   public void testSchema() throws JSONException {
	       Assert.assertEquals(validateSchema("schema/featuredArticle.txt", COMPONENT_PATH),"true");
	   }
}
