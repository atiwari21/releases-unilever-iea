package com.unilever.platform.foundation.it.launcher;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.commons.lang.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.sling.testing.tools.http.RequestExecutor;
import org.apache.sling.testing.tools.sling.SlingTestBase;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

/**
 * All JUnit test case related classes which intent to test the json output of individual class MUST extend from 
 * this class. 
 * 
 * @author nbhart
 *
 */
public abstract class JUnitBaseClass extends SlingTestBase {
	
    /**
     * logger object.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(JUnitBaseClass.class);
	
	/**
	 * Reads the static json file as string from file system. 
	 * @return json file in string
	 */
	public String getStaticFileAsString(String fileName) {
		String fileContent = StringUtils.EMPTY;
        
		BufferedReader bufferedReader = null;
		try {
			bufferedReader = new BufferedReader(new InputStreamReader(Thread.currentThread()
	                .getContextClassLoader().getResourceAsStream(fileName)));
			StringBuffer  sb = new StringBuffer ();
			String line = bufferedReader.readLine();

			while (line != null) {
				sb.append(line);
				line = bufferedReader.readLine();
			}
			fileContent= sb.toString();
		} catch (FileNotFoundException fnfe) {
			LOGGER.error("FileNotFoundException while reading the json file from the file system", fnfe);
		} catch (IOException ioe) {
			LOGGER.error("IOException while reading the json file from the file system", ioe);
		} finally {
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch (IOException ioe) {
					LOGGER.error("IOException while closing the Buffered reader", ioe);
				}
			}
		}

		return fileContent;
	}

	/**
	 * Hits the component json and return the response as string.
	 * @return dynamic json as string.
	 */
	public String getComponentJsonAsString(String componentPath) {
		String content = StringUtils.EMPTY;
		try {
			RequestExecutor result = getRequestExecutor().execute(
					getRequestBuilder().buildGetRequest(componentPath)
							.withCredentials(this.getServerUsername(),
									this.getServerPassword()));
			if (result != null) {
				content = result.getContent();
			}
		} catch (ClientProtocolException cpe) {
			LOGGER.error("ClientProtocolException while reading the json file from the file system", cpe);
		} catch (IOException ioe) {
			LOGGER.error("IOException while reading the json file from the file system", ioe);
		}
		return content;
	}
	
	public String validateSchema(String schemaFileName, String componentPath) {
        String strJson = getComponentJsonAsString(componentPath);
        String strJsonSchema = getStaticFileAsString(schemaFileName);
                try {
                	
                    JsonSchemaFactory factory = JsonSchemaFactory.byDefault();

                    JsonNode schema = JsonLoader.fromString(strJsonSchema);
                    final JsonSchema requestSchema = factory.getJsonSchema(schema);
                    JsonLoader.fromString(strJson);
                    JsonNode json = JsonLoader.fromString(strJson);
                    ProcessingReport report;
                    report = requestSchema.validate(json);
                    if (!report.isSuccess() ) {
                    	throw new RuntimeException(report.toString());
                       
                    } 

                } catch (ProcessingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return "true";
    }

	/**
	 * Holds the logic to compare the two json and use asserts to decide pass /fail of JUnit test case.
	 * @throws JSONException
	 */
	/*public abstract void testContent() throws JSONException; */
	
	
	/**
	 * Holds the logic to validate if the component is returning the json in the agreed json schema.
	 * @throws JSONException
	 */
	public abstract void testSchema() throws JSONException;
	
}
