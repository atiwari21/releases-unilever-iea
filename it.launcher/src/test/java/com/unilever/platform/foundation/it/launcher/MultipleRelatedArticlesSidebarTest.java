package com.unilever.platform.foundation.it.launcher;

import org.json.JSONException;
import org.junit.Assert;
import org.junit.Test;

public class MultipleRelatedArticlesSidebarTest extends JUnitBaseClass {
	private static final String COMPONENT_PATH="/content/junittest/en/package5/jcr:content/flexi_hero_par/multiplerelatedartic_1852334795.data.json";
	@Test
	@Override
	public void testSchema() throws JSONException {		
		Assert.assertEquals(validateSchema("schema/multipleRelatedArticlesSidebar.txt", COMPONENT_PATH),"true");
	}
}
