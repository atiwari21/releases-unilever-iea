package com.unilever.platform.foundation.it.launcher;

import org.json.JSONException;
import org.junit.Assert;

public class bodyCopyHeadlineTest extends JUnitBaseClass {

	final static String COMPONENT_PATH="/content/junittest/jcr:content/flexi_hero_par/bodycopyheadline.data.json";
	@Override
	public void testSchema() throws JSONException {
		Assert.assertEquals(validateSchema("schema/bodyCopyHeadline.txt", COMPONENT_PATH),"true");
		
	}

}
