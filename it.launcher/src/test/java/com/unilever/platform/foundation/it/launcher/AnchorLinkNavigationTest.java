package com.unilever.platform.foundation.it.launcher;

import org.json.JSONException;
import org.junit.Assert;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;


/**
 * JUnit test class for adaptive image. 
 * @author nbhart
 *
 */
public class AnchorLinkNavigationTest extends JUnitBaseClass {

	final static String COMPONENT_PATH = "/content/junittest/en/package1_test/jcr:content/flexi_hero_par/anchorlinknavigation.data.json";

	/* (non-Javadoc)
	 * @see com.unilever.platform.foundation.it.launcher.JUnitBaseClass#test()
	 */
/*	@Override
	@Test
	public void testContent() throws JSONException {
		String expectedJSONString= getStaticFileAsString("adaptiveimage.json");
		String actualJSON = getComponentJsonAsString(COMPONENT_PATH);
		JSONAssert.assertEquals(expectedJSONString, actualJSON,true);
	}
	*/
	@Override
	@Test
	public void testSchema() throws JSONException {
		Assert.assertEquals(validateSchema("schema/anchorLinkNavigation.txt", COMPONENT_PATH),"true");
	}
}
