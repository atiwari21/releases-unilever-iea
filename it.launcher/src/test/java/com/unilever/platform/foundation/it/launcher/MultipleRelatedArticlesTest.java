package com.unilever.platform.foundation.it.launcher;

import org.json.JSONException;
import org.junit.Assert;
import org.junit.Test;

public class MultipleRelatedArticlesTest extends JUnitBaseClass{
	private static final String COMPONENT_PATH="/content/junittest/en/package5/jcr:content/flexi_hero_par/multiplerelatedartic.data.json";
	@Test
	@Override
	public void testSchema() throws JSONException {		
		Assert.assertEquals(validateSchema("schema/multipleRelatedArticle.txt", COMPONENT_PATH),"true");
	}
	
}
