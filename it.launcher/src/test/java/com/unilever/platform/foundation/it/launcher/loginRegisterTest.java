package com.unilever.platform.foundation.it.launcher;

import org.json.JSONException;
import org.junit.Assert;

public class loginRegisterTest extends JUnitBaseClass {

	final static String COMPONENT_PATH="/content/junittest/jcr:content/flexi_hero_par/loginregister.data.json";
	@Override
	public void testSchema() throws JSONException {
		Assert.assertEquals(validateSchema("schema/loginRegister.txt", COMPONENT_PATH),"true");
		
	}

}
