package com.unilever.platform.foundation.it.launcher;

import org.json.JSONException;
import org.junit.Assert;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;


/**
 * JUnit test class for adaptive image. 
 * @author nbhart
 *
 */
public class VideoPlayerTest extends JUnitBaseClass {

    final static String COMPONENT_PATH = "/content/junittest/en/flexitest/jcr:content/videoplayer.data.json";

    /* (non-Javadoc)
     * @see com.unilever.platform.foundation.it.launcher.JUnitBaseClass#test()
     */
/*  @Override
    @Test
    public void testContent() throws JSONException {
        String expectedJSONString= getStaticFileAsString("adaptiveimage.json");
        String actualJSON = getComponentJsonAsString(COMPONENT_PATH);
        JSONAssert.assertEquals(expectedJSONString, actualJSON,true);
    }
    */
    @Override
    @Test
    public void testSchema() throws JSONException {
        Assert.assertEquals(validateSchema("schema/videoPlayer.txt", COMPONENT_PATH),"true");
    }
}
