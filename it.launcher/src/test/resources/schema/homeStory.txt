{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "id": "http://unilever.com",
  "type": "object",
  "properties": {
    "data": {
      "id": "http://unilever.com/data",
      "type": "object",
      "properties": {
        "path": {
          "id": "http://unilever.com/data/path",
          "type": "string"
        },
        "componentType": {
          "id": "http://unilever.com/data/componentType",
          "type": "string"
        },
        "ajaxURL": {
          "id": "http://unilever.com/data/ajaxURL",
          "type": "string"
        },
        "clientSideRendering": {
          "id": "http://unilever.com/data/clientSideRendering",
          "type": "boolean"
        },
        "viewType": {
          "id": "http://unilever.com/data/viewType",
          "type": "string"
        },
        "randomNumber": {
          "id": "http://unilever.com/data/randomNumber",
          "type": "string"
        },
        "isContainer": {
          "id": "http://unilever.com/data/isContainer",
          "type": "boolean"
        },
        "homeStory": {
          "id": "http://unilever.com/data/homeStory",
          "type": "object",
          "properties": {
            "market": {
              "id": "http://unilever.com/data/homeStory/market",
              "type": "string"
            },
            "anchorLinkNavigation": {
              "id": "http://unilever.com/data/homeStory/anchorLinkNavigation",
              "type": "object",
              "properties": {
                "showAsNavigation": {
                  "id": "http://unilever.com/data/homeStory/anchorLinkNavigation/showAsNavigation",
                  "type": "string"
                },
                "label": {
                  "id": "http://unilever.com/data/homeStory/anchorLinkNavigation/label",
                  "type": "string"
                },
                "id": {
                  "id": "http://unilever.com/data/homeStory/anchorLinkNavigation/id",
                  "type": "string"
                }
              }
            },
            "brandName": {
              "id": "http://unilever.com/data/homeStory/brandName",
              "type": "string"
            },
            "stories": {
              "id": "http://unilever.com/data/homeStory/stories",
              "type": "array",
              "items": {
                "id": "http://unilever.com/data/homeStory/stories/0",
                "type": "object",
                "properties": {
                  "title": {
                    "id": "http://unilever.com/data/homeStory/stories/0/title",
                    "type": "string"
                  },
                  "shortCopy": {
                    "id": "http://unilever.com/data/homeStory/stories/0/shortCopy",
                    "type": "string"
                  },
                  "leftImage": {
                    "id": "http://unilever.com/data/homeStory/stories/0/leftImage",
                    "type": "object",
                    "properties": {
                      "url": {
                        "id": "http://unilever.com/data/homeStory/stories/0/leftImage/url",
                        "type": "string"
                      },
                      "fileName": {
                        "id": "http://unilever.com/data/homeStory/stories/0/leftImage/fileName",
                        "type": "string"
                      },
                      "isNotAdaptiveImage": {
                        "id": "http://unilever.com/data/homeStory/stories/0/leftImage/isNotAdaptiveImage",
                        "type": "boolean"
                      },
                      "extension": {
                        "id": "http://unilever.com/data/homeStory/stories/0/leftImage/extension",
                        "type": "string"
                      },
                      "altImage": {
                        "id": "http://unilever.com/data/homeStory/stories/0/leftImage/altImage",
                        "type": "string"
                      },
                      "title": {
                        "id": "http://unilever.com/data/homeStory/stories/0/leftImage/title",
                        "type": "string"
                      },
                      "path": {
                        "id": "http://unilever.com/data/homeStory/stories/0/leftImage/path",
                        "type": "string"
                      },
                      "isProduct": {
                        "id": "http://unilever.com/data/homeStory/stories/0/leftImage/isProduct",
                        "type": "string"
                      }
                    }
                  },
                  "rightImage": {
                    "id": "http://unilever.com/data/homeStory/stories/0/rightImage",
                    "type": "object",
                    "properties": {
                      "url": {
                        "id": "http://unilever.com/data/homeStory/stories/0/rightImage/url",
                        "type": "string"
                      },
                      "fileName": {
                        "id": "http://unilever.com/data/homeStory/stories/0/rightImage/fileName",
                        "type": "string"
                      },
                      "isNotAdaptiveImage": {
                        "id": "http://unilever.com/data/homeStory/stories/0/rightImage/isNotAdaptiveImage",
                        "type": "boolean"
                      },
                      "extension": {
                        "id": "http://unilever.com/data/homeStory/stories/0/rightImage/extension",
                        "type": "string"
                      },
                      "altImage": {
                        "id": "http://unilever.com/data/homeStory/stories/0/rightImage/altImage",
                        "type": "string"
                      },
                      "title": {
                        "id": "http://unilever.com/data/homeStory/stories/0/rightImage/title",
                        "type": "string"
                      },
                      "path": {
                        "id": "http://unilever.com/data/homeStory/stories/0/rightImage/path",
                        "type": "string"
                      },
                      "isProduct": {
                        "id": "http://unilever.com/data/homeStory/stories/0/rightImage/isProduct",
                        "type": "string"
                      }
                    }
                  },
                  "centreImage": {
                    "id": "http://unilever.com/data/homeStory/stories/0/centreImage",
                    "type": "object",
                    "properties": {
                      "url": {
                        "id": "http://unilever.com/data/homeStory/stories/0/centreImage/url",
                        "type": "string"
                      },
                      "fileName": {
                        "id": "http://unilever.com/data/homeStory/stories/0/centreImage/fileName",
                        "type": "string"
                      },
                      "isNotAdaptiveImage": {
                        "id": "http://unilever.com/data/homeStory/stories/0/centreImage/isNotAdaptiveImage",
                        "type": "boolean"
                      },
                      "extension": {
                        "id": "http://unilever.com/data/homeStory/stories/0/centreImage/extension",
                        "type": "string"
                      },
                      "altImage": {
                        "id": "http://unilever.com/data/homeStory/stories/0/centreImage/altImage",
                        "type": "string"
                      },
                      "title": {
                        "id": "http://unilever.com/data/homeStory/stories/0/centreImage/title",
                        "type": "string"
                      },
                      "path": {
                        "id": "http://unilever.com/data/homeStory/stories/0/centreImage/path",
                        "type": "string"
                      }
                    }
                  },
                  "quote": {
                    "id": "http://unilever.com/data/homeStory/stories/0/quote",
                    "type": "object",
                    "properties": {
                      "referenceText": {
                        "id": "http://unilever.com/data/homeStory/stories/0/quote/referenceText",
                        "type": "string"
                      },
                      "text": {
                        "id": "http://unilever.com/data/homeStory/stories/0/quote/text",
                        "type": "string"
                      }
                    }
                  },
                  "cta": {
                    "id": "http://unilever.com/data/homeStory/stories/0/cta",
                    "type": "object",
                    "properties": {
                      "text": {
                        "id": "http://unilever.com/data/homeStory/stories/0/cta/text",
                        "type": "string"
                      },
                      "url": {
                        "id": "http://unilever.com/data/homeStory/stories/0/cta/url",
                        "type": "string"
                      },
                      "openInWindow": {
                        "id": "http://unilever.com/data/homeStory/stories/0/cta/openInWindow",
                        "type": "string"
                      },
                      "containerTag": {
                        "id": "http://unilever.com/data/homeStory/stories/0/cta/containerTag",
                        "type": "object",
                        "properties": {
                          "action": {
                            "id": "http://unilever.com/data/homeStory/stories/0/cta/containerTag/action",
                            "type": "string"
                          },
                          "information": {
                            "id": "http://unilever.com/data/homeStory/stories/0/cta/containerTag/information",
                            "type": "string"
                          },
                          "category": {
                            "id": "http://unilever.com/data/homeStory/stories/0/cta/containerTag/category",
                            "type": "string"
                          },
                          "attr": {
                            "id": "http://unilever.com/data/homeStory/stories/0/cta/containerTag/attr",
                            "type": "string"
                          }
                        }
                      }
                    }
                  }
                }
              }
            },
            "componentName": {
              "id": "http://unilever.com/data/homeStory/componentName",
              "type": "string"
            }
          }
        },
        "resourceType": {
          "id": "http://unilever.com/data/resourceType",
          "type": "string"
        }
      },
      "required": [
        "path",
        "componentType",
        "ajaxURL",
        "clientSideRendering",
        "viewType",
        "randomNumber",
        "isContainer",
        "homeStory",
        "resourceType"
      ]
    },
    "responseHeader": {
      "id": "http://unilever.com/responseHeader",
      "type": "object",
      "properties": {
        "messages": {
          "id": "http://unilever.com/responseHeader/messages",
          "type": "array"
        },
        "status": {
          "id": "http://unilever.com/responseHeader/status",
          "type": "string"
        }
      }
    }
  },
  "required": [
    "data",
    "responseHeader"
  ]
}