{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "id": "http://unilever.com",
  "type": "object",
  "properties": {
    "data": {
      "id": "http://unilever.com/data",
      "type": "object",
      "properties": {
        "path": {
          "id": "http://unilever.com/data/path",
          "type": "string"
        },
        "componentType": {
          "id": "http://unilever.com/data/componentType",
          "type": "string"
        },
        "brandName": {
          "id": "http://unilever.com/data/brandName",
          "type": "string"
        },
        "ajaxURL": {
          "id": "http://unilever.com/data/ajaxURL",
          "type": "string"
        },
        "clientSideRendering": {
          "id": "http://unilever.com/data/clientSideRendering",
          "type": "boolean"
        },
        "viewType": {
          "id": "http://unilever.com/data/viewType",
          "type": "string"
        },
        "randomNumber": {
          "id": "http://unilever.com/data/randomNumber",
          "type": "string"
        },
        "isContainer": {
          "id": "http://unilever.com/data/isContainer",
          "type": "boolean"
        },
        "hero": {
          "id": "http://unilever.com/data/hero",
          "type": "object",
          "properties": {
            "isReadMoreEnable": {
              "id": "http://unilever.com/data/hero/isReadMoreEnable",
              "type": "string"
            },
            "instanceType": {
              "id": "http://unilever.com/data/hero/instanceType",
              "type": "string"
            },
            "mainCopy": {
              "id": "http://unilever.com/data/hero/mainCopy",
              "type": "string"
            },
            "introCopy": {
              "id": "http://unilever.com/data/hero/introCopy",
              "type": "string"
            },
            "label": {
              "id": "http://unilever.com/data/hero/label",
              "type": "object",
              "properties": {
                "readMore": {
                  "id": "http://unilever.com/data/hero/label/readMore",
                  "type": "string"
                },
                "readLess": {
                  "id": "http://unilever.com/data/hero/label/readLess",
                  "type": "string"
                }
              }
            },
            "video": {
              "id": "http://unilever.com/data/hero/video",
              "type": "object",
              "properties": {
                "id": {
                  "id": "http://unilever.com/data/hero/video/id",
                  "type": "string"
                },
                "previewImageUrl": {
                  "id": "http://unilever.com/data/hero/video/previewImageUrl",
                  "type": "object",
                  "properties": {
                    "url": {
                      "id": "http://unilever.com/data/hero/video/previewImageUrl/url",
                      "type": "string"
                    },
                    "fileName": {
                      "id": "http://unilever.com/data/hero/video/previewImageUrl/fileName",
                      "type": "string"
                    },
                    "isNotAdaptiveImage": {
                      "id": "http://unilever.com/data/hero/video/previewImageUrl/isNotAdaptiveImage",
                      "type": "boolean"
                    },
                    "extension": {
                      "id": "http://unilever.com/data/hero/video/previewImageUrl/extension",
                      "type": "string"
                    },
                    "altImage": {
                      "id": "http://unilever.com/data/hero/video/previewImageUrl/altImage",
                      "type": "string"
                    },
                    "title": {
                      "id": "http://unilever.com/data/hero/video/previewImageUrl/title",
                      "type": "string"
                    },
                    "path": {
                      "id": "http://unilever.com/data/hero/video/previewImageUrl/path",
                      "type": "string"
                    }
                  }
                }
              }
            },
            "type": {
              "id": "http://unilever.com/data/hero/type",
              "type": "string"
            },
            "headline": {
              "id": "http://unilever.com/data/hero/headline",
              "type": "string"
            }
          }
        },
        "resourceType": {
          "id": "http://unilever.com/data/resourceType",
          "type": "string"
        }
      },
      "required": [
        "path",
        "componentType",
        "brandName",
        "ajaxURL",
        "clientSideRendering",
        "viewType",
        "randomNumber",
        "isContainer",
        "hero",
        "resourceType"
      ]
    },
    "responseHeader": {
      "id": "http://unilever.com/responseHeader",
      "type": "object",
      "properties": {
        "messages": {
          "id": "http://unilever.com/responseHeader/messages",
          "type": "array"
        },
        "status": {
          "id": "http://unilever.com/responseHeader/status",
          "type": "string"
        }
      }
    }
  },
  "required": [
    "data",
    "responseHeader"
  ]
}