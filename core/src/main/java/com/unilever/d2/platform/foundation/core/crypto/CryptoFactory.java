/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.d2.platform.foundation.core.crypto;

import com.unilever.d2.platform.foundation.core.util.Key;
import com.unilever.d2.platform.foundation.core.util.KeyStore;

/**
 * Factory to provide instance of Cryptor class.
 * 
 * 
 */
public class CryptoFactory {
 
 /**
  * Added private constructor to hide implicit one.
  */
 private CryptoFactory() {
  
 }
 
 /**
  * Gets the single instance of CryptoFactory.
  * 
  * @param encriptionKey
  *         the encription key
  * @param encriptionSalt
  *         the encription salt
  * @return single instance of CryptoFactory
  */
 public static Cryptor getInstance(String encriptionKey, String encriptionSalt) {
  Key pass = KeyStore.getKey(encriptionKey, encriptionSalt);
  return new Cryptor(pass);
  
 }
}
