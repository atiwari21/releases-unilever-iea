/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.d2.platform.foundation.core.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * The Class ResourceUtils.
 */
public class ResourceUtils {
 
 /**
  * Get resource type of current resource.
  * 
  * @param resources
  *         the resources
  * @return resource type
  */
 public static String getCurrentResourceName(final Map<String, Object> resources) {
  String resourceName = StringUtils.EMPTY;
  Resource resource = BaseViewHelper.getCurrentResource(resources);
  if (resource != null) {
   resourceName = resource.getName();
  }
  return resourceName;
 }
 
 /**
  * Get resource type of current resource.
  * 
  * @param resources
  *         the resources
  * @return resource type
  */
 public static String getCurrentResourceParentName(final Map<String, Object> resources) {
  String parentResourceName = StringUtils.EMPTY;
  Resource resource = BaseViewHelper.getCurrentResource(resources);
  if (resource != null) {
   parentResourceName = resource.getParent().getName();
  }
  return parentResourceName;
 }
 
 /**
  * Get resource type of current resource.
  * 
  * @param resources
  *         the resources
  * @return resource type
  */
 public static int getComponentCount(Resource resources) {
  List<String> resourcesPathFullList = new ArrayList<String>();
  Iterator<Resource> childResourceItr = resources.listChildren();
  while (childResourceItr.hasNext()) {
   resourcesPathFullList.addAll(ComponentUtil.getNonParChildResourceNameList(childResourceItr.next()));
  }
  return resourcesPathFullList.size();
 }
 
 /**
  * Gets the component count.
  * 
  * @param resources
  *         the resources
  * @param iparsysName
  *         the iparsys name
  * @return the component count
  */
 public static int getComponentCount(final Map<String, Object> resources, String iparsysName) {
  Page currentPage = BaseViewHelper.getCurrentPage(resources);
  Page parentPage = ResourceUtils.getIparsysParent(currentPage, iparsysName);
  List<String> resourcesPathFullList = new ArrayList<String>();
  if (parentPage != null) {
   Resource jcrContentResource = parentPage.getContentResource();
   if (jcrContentResource != null) {
    Iterator<Resource> childResourceItr = jcrContentResource.listChildren();
    while (childResourceItr.hasNext()) {
     Resource childResource = childResourceItr.next();
     if (StringUtils.equalsIgnoreCase(childResource.getName(), iparsysName)) {
      resourcesPathFullList.addAll(ComponentUtil.getNonParChildResourceNameList(childResource));
     }
    }
   }
  }
  return resourcesPathFullList.size();
 }
 
 /**
  * Gets the actual page component count.
  * 
  * @param resources
  *         the resources
  * @return the actual page component count
  */
 public static int getActualPageComponentCount(final Map<String, Object> resources) {
  Page actualPage = BaseViewHelper.getActualPage(resources);
  Resource jcrContentResource = actualPage.getContentResource();
  List<String> resourcesPathFullList = new ArrayList<String>();
  if (jcrContentResource != null) {
   Iterator<Resource> childResourceItr = jcrContentResource.listChildren();
   while (childResourceItr.hasNext()) {
    Resource childResource = childResourceItr.next();
    resourcesPathFullList.addAll(ComponentUtil.getNonParChildResourceNameList(childResource));
   }
  }
  return resourcesPathFullList.size();
 }
 
 /**
  * Gets the iparsys parent.
  * 
  * @param page
  *         the page
  * @param iparsysName
  *         the iparsys name
  * @return the iparsys parent
  */
 public static Page getIparsysParent(Page page, String iparsysName) {
  Resource jcrContentResource = page.getContentResource();
  boolean isIparsysHeaderParent = false;
  Resource childResource = null;
  Page resourcePage = null;
  if (jcrContentResource != null) {
   Iterator<Resource> childResourceItr = jcrContentResource.listChildren();
   while (childResourceItr.hasNext()) {
    childResource = childResourceItr.next();
    if (StringUtils.equalsIgnoreCase(childResource.getName(), iparsysName) && !ResourceUtils.checkIfInheritanceDisabled(childResource) && !ResourceUtils.checkIfInheritanceCancelled(childResource)) {
     isIparsysHeaderParent = true;
     break;
    }
   }
  }
  if (childResource != null && isIparsysHeaderParent) {
   PageManager pageManager = page.getPageManager();
   resourcePage = pageManager.getContainingPage(childResource.getPath());
  }
  return (isIparsysHeaderParent && resourcePage != null) || (page.getParent() == null) ? resourcePage : getIparsysParent(page.getParent(),
    iparsysName);
 }
 
 /**
  * Check if inheritance disabled.
  * 
  * @param resource
  *         the resource
  * @return true, if successful
  */
 public static boolean checkIfInheritanceDisabled(Resource resource) {
  Iterator<Resource> childResourceItr = resource.listChildren();
  boolean isInheritanceDisabled = false;
  while (childResourceItr.hasNext()) {
   Resource childResource = childResourceItr.next();
   ValueMap valuemap = childResource.adaptTo(ValueMap.class);
   if (StringUtils.equalsIgnoreCase(childResource.getResourceType(), "foundation/components/iparsys/par")) {
    String inheritanceValue = (String) valuemap.get("inheritance");
    if (StringUtils.isNotBlank(inheritanceValue) && StringUtils.equalsIgnoreCase(inheritanceValue, "cancel")) {
     isInheritanceDisabled = true;
     break;
    }
   }
  }
  
  return isInheritanceDisabled;
  
 }
 
 /**
  * Check if inheritance cancelled.
  * 
  * @param resource
  *         the resource
  * @return true, if successful
  */
 public static boolean checkIfInheritanceCancelled(Resource resource) {
  ValueMap valuemap = resource.adaptTo(ValueMap.class);
  boolean isInheritanceCancelled = false;
  String inheritanceValue = (String) valuemap.get("inheritance");
  if (StringUtils.isNotBlank(inheritanceValue) && StringUtils.equalsIgnoreCase(inheritanceValue, "cancel")) {
   isInheritanceCancelled = true;
  }
  return isInheritanceCancelled;
  
 }
 
}
