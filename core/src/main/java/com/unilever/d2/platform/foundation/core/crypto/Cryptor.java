/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.d2.platform.foundation.core.crypto;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unilever.d2.platform.foundation.core.util.Key;
import com.unilever.platform.foundation.constants.UnileverConstants;

/**
 * The is used to encrypt-decrypt data using AES-256 Cryptography Algorithm. See <a
 * href="https://en.wikipedia.org/wiki/Advanced_Encryption_Standard">Advanced_Encryption_Standard</a> for further detail.
 * 
 */

public class Cryptor {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(Cryptor.class);
 
 /** The cipher. */
 private static Cipher cipher;
 
 /** The password. */
 private static SecretKey password;
 
 /** The i v param spec. */
 private static IvParameterSpec iVParamSpec;
 
 /** The Constant PBKDF2_WITH_HMAC_SHA256. */
 // Password-based key-derivation algorithm using the specified pseudo-random function (<HmacSHA256>)
 private static final String PBKDF2_WITH_HMAC_SHA256 = "PBKDF2WithHmacSHA256";
 
 // A transformation is a string that describes the operation (or set of operations) to be performed on the given input, to produce some output. A
 /** The Constant TRANSFORMATION. */
 // transformation always includes the name of a cryptographic algorithm , and may be followed by a feedback mode and padding scheme.
 private static final String TRANSFORMATION = "AES/CBC/PKCS5Padding";
 
 /** The Constant ALGORITHM. */
 // The name of the secret-key algorithm to be associated with the given key material.
 private static final String ALGORITHM = "AES";
 
 /** The Constant ITERATION_COUNT. */
 /*
  * An iteration count has traditionally served the purpose of increasing the cost of producing keys from a password, thereby also increasing the
  * difficulty of attack. A minimum of 1000 iterations is recommended. This will increase the cost of exhaustive search for passwords significantly,
  * without a noticeable impact in the cost of deriving individual keys.
  */
 private static final int ITERATION_COUNT = 1000;
 
 /** The Constant INIT_VECTOR. */
 // Needs to be 16 ASCII characters long
 private static final byte[] INIT_VECTOR = "UnileverSapient1".getBytes();
 
 /** The Constant KEY_SIZE. */
 // It can be 128, 192 or 256
 static final int KEY_SIZE = 128;
 
 /**
  * Constructor.
  * 
  * @param secKey
  *         the sec key
  */
 public Cryptor(Key secKey) {
  try {
   final SecretKeyFactory factory = SecretKeyFactory.getInstance(PBKDF2_WITH_HMAC_SHA256);
   final KeySpec spec = new PBEKeySpec(secKey.getKey().toCharArray(), secKey.getSalt().getBytes(), ITERATION_COUNT, KEY_SIZE);
   final SecretKey tmp = factory.generateSecret(spec);
   password = new SecretKeySpec(tmp.getEncoded(), ALGORITHM);
   cipher = Cipher.getInstance(TRANSFORMATION);
   iVParamSpec = new IvParameterSpec(INIT_VECTOR);
   
  } catch (NoSuchAlgorithmException e) {
   LOGGER.error("No such algorithm " + ALGORITHM, e);
  } catch (NoSuchPaddingException e) {
   LOGGER.error("No such padding PKCS7", e);
  } catch (InvalidKeySpecException e) {
   LOGGER.error("Invalid Key ", e);
  }
 }
 
 /**
  * The method is used to encrypt-dcrypt data based on the crypt mode. Valid values of crypt mode is
  * <ul>
  * <li>ENCRYPT_MODE</li>
  * <li>DCRYPT_MODE</li>
  * </ul>
  * 
  * @param cryptMode
  *         the crypt mode
  * @param data
  *         the data
  * @return the string
  */
 public String crypt(String cryptMode, String data) {
  
  try {
   switch (cryptMode) {
    case UnileverConstants.ENCRYPT_MODE:
     cipher.init(Cipher.ENCRYPT_MODE, password, iVParamSpec);
     byte[] encryptedData = cipher.doFinal(data.getBytes("UTF-8"));
     return Base64.getEncoder().encodeToString(encryptedData);
    case UnileverConstants.DECRYPT_MODE:
     cipher.init(Cipher.DECRYPT_MODE, password, iVParamSpec);
     byte[] decodedValue = Base64.getDecoder().decode(data.getBytes());
     byte[] decryptedVal = cipher.doFinal(decodedValue);
     return new String(decryptedVal);
    default:
   }
  } catch (InvalidKeyException e) {
   LOGGER.error("Invalid key  (invalid encoding, wrong length, uninitialized, etc).", e);
   
  } catch (InvalidAlgorithmParameterException e) {
   LOGGER.error("Invalid or inappropriate algorithm parameters for " + ALGORITHM, e);
  } catch (IllegalBlockSizeException e) {
   LOGGER.error("The length of data provided to a block cipher is incorrect", e);
  } catch (BadPaddingException e) {
   LOGGER.error("The input data but the data is not padded properly.", e);
  } catch (UnsupportedEncodingException e) {
   LOGGER.error("Encoding is not supported ", e);
  }
  
  return null;
  
 }
 
}
