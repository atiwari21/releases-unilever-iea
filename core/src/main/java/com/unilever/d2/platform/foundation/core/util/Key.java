/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.d2.platform.foundation.core.util;

/**
 * Key class to hold encryption key and salt for {@code Cryptor}.
 */
public class Key {
 
 /** The enctiption key. */
 private String enctiptionKey;
 
 /** The encription salt. */
 private String encriptionSalt;
 
 /**
  * Gets the key.
  * 
  * @return key
  */
 public String getKey() {
  return enctiptionKey;
 }
 
 /**
  * Sets the key.
  * 
  * @param key
  *         to set encryption key
  */
 public void setKey(String key) {
  this.enctiptionKey = key;
 }
 
 /**
  * Gets the salt.
  * 
  * @return salt
  */
 public String getSalt() {
  return encriptionSalt;
 }
 
 /**
  * Sets the salt.
  * 
  * @param salt
  *         to set salt
  */
 public void setSalt(String salt) {
  this.encriptionSalt = salt;
 }
}
