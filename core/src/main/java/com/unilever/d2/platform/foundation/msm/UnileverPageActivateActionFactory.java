/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.d2.platform.foundation.msm;

import javax.jcr.Session;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.io.JSONWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationException;
import com.day.cq.replication.ReplicationStatus;
import com.day.cq.replication.Replicator;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.WCMException;
import com.day.cq.wcm.msm.api.ActionConfig;
import com.day.cq.wcm.msm.api.LiveAction;
import com.day.cq.wcm.msm.api.LiveActionFactory;
import com.day.cq.wcm.msm.api.LiveRelationship;
import com.day.cq.wcm.msm.api.LiveStatus;

/**
 * A factory for creating UnileverPageActivateActionFactory objects.
 */
@Component(metatype = false, label = "unilever live action to activate target page on rollout of component")
@Service
public class UnileverPageActivateActionFactory implements LiveActionFactory<LiveAction> {
 
 @Reference
 private Replicator replicator;
 
 protected static final Logger logger = LoggerFactory.getLogger(UnileveFormLiveActionFactory.class);
 /** The Constant ACTION_NAME. */
 @Property({ "targetActivateComponentRollout" })
 static final String ACTION_NAME = "liveActionName";
 
 @Override
 public LiveAction createAction(Resource arg0) throws WCMException {
  return new UnileverPageActivateAction(ACTION_NAME, replicator);
 }
 
 @Override
 public String createsAction() {
  return ACTION_NAME;
 }
 
 private static class UnileverPageActivateAction implements LiveAction {
  
  /** The name. */
  private String name;
  
  private Replicator replicator;
  
  /**
   * Instantiates a new unileve page activate action.
   * 
   * @param nm
   *         the nm
   * @param replicator
   *         the replicator
   */
  public UnileverPageActivateAction(String nm, Replicator replicator) {
   this.name = nm;
   this.replicator = replicator;
  }
  
  @Override
  public void execute(Resource source, Resource target, LiveRelationship liveRelation, boolean autoSave, boolean resetRollout) throws WCMException {
   LiveStatus liveRelationStatus = liveRelation.getStatus();
   if (!liveRelationStatus.isCancelled() && !liveRelationStatus.isPage()) {
    ResourceResolver resolver = target.getResourceResolver();
    PageManager manager = resolver.adaptTo(PageManager.class);
    Page page = manager.getContainingPage(target);
    if (page != null) {
     try {
      replicator.replicate(resolver.adaptTo(Session.class), ReplicationActionType.ACTIVATE, page.getPath());
      ReplicationStatus replicationStatus = page.adaptTo(ReplicationStatus.class);
      if (replicationStatus.isActivated()) {
       logger.debug("page activated" + page.getPath());
      }
     } catch (ReplicationException e) {
      logger.error("repliated page" + page.getPath());
      logger.error("Exception Caused" + e);
     }
    }
   }
   
  }
  
  @Override
  public String getName() {
   return this.name;
  }
  
  /**
   * @deprecated
   */
  @Override
  @Deprecated
  public String getParameterName() {
   return null;
  }
  
  /**
   * @deprecated
   */
  @Override
  @Deprecated
  public String[] getPropertiesNames() {
   return null;
  }
  
  @Override
  public int getRank() {
   return 0;
  }
  
  /**
   * @deprecated
   */
  @Override
  @Deprecated
  public String getTitle() {
   return null;
  }
  
  /**
   * @deprecated
   */
  @Override
  @Deprecated
  public void write(JSONWriter arg0) throws JSONException {
   //empty method
  }
  
  /**
   * @deprecated
   */
  @Override
  @Deprecated
  public void execute(ResourceResolver arg0, LiveRelationship arg1, ActionConfig arg2, boolean arg3) throws WCMException {
   //Empty method
  }
  
  /**
   * @deprecated
   */
  @Override
  @Deprecated
  public void execute(ResourceResolver arg0, LiveRelationship arg1, ActionConfig arg2, boolean arg3, boolean arg4) throws WCMException {
   logger.error("hello");
  }
  
 }
 
}
