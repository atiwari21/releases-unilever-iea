/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.d2.platform.foundation.core.util;

/**
 * Utility class to hold different version of key-salt pair.
 * 
 */
public class KeyStore {
 
 /**
  * Instantiates a new key store.
  */
 private KeyStore() {
  
 }
 
 /**
  * Gets the key.
  * 
  * @param encriptionKey
  *         the encription key
  * @param encriptionSalt
  *         the encription salt
  * @return the key
  */
 public static Key getKey(String encriptionKey, String encriptionSalt) {
  Key key = new Key();
  key.setKey(encriptionKey);
  key.setSalt(encriptionSalt);
  
  return key;
 }
 
}
