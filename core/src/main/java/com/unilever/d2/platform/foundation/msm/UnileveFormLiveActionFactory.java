/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.d2.platform.foundation.msm;

import java.util.Map;

import javax.jcr.Node;
import javax.jcr.Session;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.io.JSONWriter;
import org.apache.sling.jcr.resource.JcrResourceConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.WCMException;
import com.day.cq.wcm.msm.api.ActionConfig;
import com.day.cq.wcm.msm.api.LiveAction;
import com.day.cq.wcm.msm.api.LiveActionFactory;
import com.day.cq.wcm.msm.api.LiveRelationship;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.core.dto.BrandMarketBean;

/**
 * A factory for creating UnileveFormLiveAction objects.
 */
@Component(metatype = false)
@Service
public class UnileveFormLiveActionFactory implements LiveActionFactory<LiveAction> {
 
 /** The Constant ACTION_NAME. */
 @Property({ "unileverFormLiveAction" })
 static final String ACTION_NAME = "liveActionName";
 
 /**
  * Creates Action
  * 
  * @param config
  *         the config
  * 
  * @return the live action
  * 
  * @see com.day.cq.wcm.msm.api.LiveActionFactory#createAction(org.apache.sling.api.resource.Resource)
  */
 public LiveAction createAction(Resource config) {
  return new UnileveFormLiveAction(ACTION_NAME);
 }
 
 /**
  * @return action name
  * @see com.day.cq.wcm.msm.api.LiveActionFactory#createsAction()
  */
 public String createsAction() {
  return ACTION_NAME;
 }
 
 /**
  * The Class UnileveFormLiveAction.
  */
 private static class UnileveFormLiveAction implements LiveAction {
  
  /** The Constant SLASH. */
  private static final String SLASH = "/";
  
  /** The Constant APPS. */
  private static final String APPS = "/apps";
  
  /** The Constant UNILEVER_IEA_COMPONENTS. */
  private static final String UNILEVER_IEA_COMPONENTS = "unilever-iea/components";
  
  /** The Constant UNILEVER_IEA_TEMPLATE. */
  private static final String UNILEVER_IEA_TEMPLATE = "unilever-iea/templates";
  
  /** The Constant UNILEVER_IEA_PAGE_COMPONENTS. */
  private static final String UNILEVER_IEA_PAGE_COMPONENTS = "unilever-iea/pagecomponents";
  
  /** The name. */
  private String name;
  
  /** The Constant log. */
  private static final Logger LOG = LoggerFactory.getLogger(UnileveFormLiveAction.class);
  
  /**
   * Instantiates a new unileve form live action.
   * 
   * @param nm
   *         the nm
   */
  public UnileveFormLiveAction(String nm) {
   this.name = nm;
  }
  
  /**
   * @deprecated
   * 
   * @param source
   *         the source
   * @param target
   *         the target
   * @param liverel
   *         the liverel
   * @param autoSave
   *         the autoSave
   * @param isResetRollout
   *         the isResetRollout
   * 
   * @see com.day.cq.wcm.msm.api.LiveAction#execute(org.apache.sling.api.resource.Resource, org.apache.sling.api.resource.Resource,
   *      com.day.cq.wcm.msm.api.LiveRelationship, boolean, boolean)
   */
  @Deprecated
  public void execute(Resource source, Resource target, LiveRelationship liverel, boolean autoSave, boolean isResetRollout) throws WCMException {
   LOG.info(" *** Executing UnileveFormLiveAction *** ");
   if (null == source || source.adaptTo(Node.class) == null || null == target || target.adaptTo(Node.class) == null) {
    LOG.debug("LiveAction skipped source '{}' and/or target '{}' are missing", source, target);
    return;
   }
   LOG.debug("LiveAction executing from {} to {}", source, target);
   ResourceResolver resolver = target.getResourceResolver();
   if (resolver == null) {
    LOG.debug("LiveAction skipped, resolver is null");
    return;
   }
   ValueMap sourcevm = (ValueMap) source.adaptTo(ValueMap.class);
   String appName = getAppName(target, resolver);
   updateProperty(sourcevm, target, resolver, JcrResourceConstants.SLING_RESOURCE_TYPE_PROPERTY, UNILEVER_IEA_COMPONENTS, appName + "/components",
     autoSave);
   updateProperty(sourcevm, target, resolver, JcrResourceConstants.SLING_RESOURCE_TYPE_PROPERTY, UNILEVER_IEA_PAGE_COMPONENTS, appName
     + "/pagecomponents", autoSave);
   updateProperty(sourcevm, target, resolver, NameConstants.NN_TEMPLATE, UNILEVER_IEA_TEMPLATE, appName + "/templates", autoSave);
  }
  
  /**
   * Update property.
   * 
   * @param sourcevm
   *         the sourcevm
   * @param target
   *         the target
   * @param resolver
   *         the resolver
   * @param propName
   *         the prop name
   * @param propValuePart
   *         the prop value part
   * @param newPropValuePart
   *         the new prop value part
   * @param autoSave
   *         the auto save
   * @throws WCMException
   *          the WCM exception
   */
  public void updateProperty(Map<String, Object> sourcevm, Resource target, ResourceResolver resolver, String propName, String propValuePart,
    String newPropValuePart, boolean autoSave) throws WCMException {
   String propValue = MapUtils.getString(sourcevm, propName);
   String newPropValue = propValue;
   if (propValue != null && propValue.contains(propValuePart)) {
    newPropValue = propValue.replaceAll(propValuePart, newPropValuePart);
    String tempPropValue = newPropValue.startsWith(APPS) ? newPropValue : APPS + SLASH + newPropValue;
    if (resolver.getResource(tempPropValue) == null) {
     WCMException wcmException = new WCMException("Failed to find the wrapper of  " + newPropValue);
     LOG.error(wcmException.getMessage(), wcmException);
     throw wcmException;
    }
    if (!newPropValue.equals(propValue)) {
     Session session = resolver.adaptTo(Session.class);
     try {
      target.adaptTo(Node.class).setProperty(propName, newPropValue);
      if (autoSave && (null != session) && (session.hasPendingChanges())) {
       session.save();
      }
      LOG.info(" *** Target node sling:resourceType property updated: {} ***", newPropValue);
     } catch (Exception e) {
      LOG.error(" *** Target node property not updated: {} ***", e);
     }
     
    }
   }
  }
  
  /**
   * Gets the app name.
   * 
   * @param target
   *         the target
   * @param resolver
   *         the resolver
   * @return the app name
   */
  private String getAppName(Resource target, ResourceResolver resolver) {
   PageManager pageManager = null;
   pageManager = resolver.adaptTo(PageManager.class);
   Page targetPage = pageManager.getContainingPage(target);
   String appName = null;
   try {
    appName = resolver.adaptTo(ConfigurationService.class).getConfigValue(targetPage, "brand", "appName");
   } catch (ConfigurationNotFoundException e) {
    LOG.error("appName not found ", e.getMessage(),e);
   }
   if (StringUtils.isBlank(appName)) {
    appName = new BrandMarketBean(targetPage).getBrand();
   }
   return appName;
  }
  
  /*
   * (non-Javadoc)
   * 
   * @see com.day.cq.wcm.msm.api.LiveAction#getName()
   */
  public String getName() {
   return this.name;
  }
  
  /**
   * @deprecated
   * 
   * @param arg0
   *         the resource resolver
   * @param arg1
   *         the live relationship
   * @param arg2
   *         the action config
   * @param arg3
   * 
   * @see com.day.cq.wcm.msm.api.LiveAction#execute(org.apache.sling.api.resource.ResourceResolver, com.day.cq.wcm.msm.api.LiveRelationship,
   *      com.day.cq.wcm.msm.api.ActionConfig, boolean)
   */
  @Deprecated
  public void execute(ResourceResolver arg0, LiveRelationship arg1, ActionConfig arg2, boolean arg3) throws WCMException {
  }
  
  /**
   * @deprecated
   * 
   * @param arg0
   *         the resource resolver
   * @param arg1
   *         the live relationship
   * @param arg2
   *         the action config
   * @param arg3
   * @param arg4
   * @see com.day.cq.wcm.msm.api.LiveAction#execute(org.apache.sling.api.resource.ResourceResolver, com.day.cq.wcm.msm.api.LiveRelationship,
   *      com.day.cq.wcm.msm.api.ActionConfig, boolean, boolean)
   */
  @Deprecated
  public void execute(ResourceResolver arg0, LiveRelationship arg1, ActionConfig arg2, boolean arg3, boolean arg4) throws WCMException {
   //
  }
  
  /**
   * @deprecated
   * 
   * @see com.day.cq.wcm.msm.api.LiveAction#getParameterName()
   * @return parameter name
   */
  @Deprecated
  public String getParameterName() {
   return null;
  }
  
  /**
   * (non-Javadoc)
   * 
   * @deprecated
   * 
   * @return
   * 
   * @see com.day.cq.wcm.msm.api.LiveAction#getPropertiesNames()
   */
  @Deprecated
  public String[] getPropertiesNames() {
   return null;
  }
  
  /**
   * @deprecated
   * @return integer
   * @see com.day.cq.wcm.msm.api.LiveAction#getRank()
   */
  @Deprecated
  public int getRank() {
   return 0;
  }
  
  /**
   * @deprecated
   * @return
   * @see com.day.cq.wcm.msm.api.LiveAction#getTitle()
   */
  @Deprecated
  public String getTitle() {
   return null;
  }
  
  /**
   * @deprecated
   * @param arg0
   *         the JSONWriter
   * @see com.day.cq.wcm.msm.api.LiveAction#write(org.apache.sling.commons.json.io.JSONWriter)
   */
  @Deprecated
  public void write(JSONWriter arg0) throws JSONException {
  }
 }
}
