/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.utils.AdaptiveUtil;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.SocialShareHelper;
import com.unilever.platform.foundation.template.dto.Image;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * <p>
 * The goal of this component is to display quote on pages as well as to share the quote to social sharing via addThis widget on pages. The social
 * sharing options will be managed via global configurations and hence will remain same across the site pages. Once configured the social share
 * options will be available on the page for end user to share that particular page on the social sharing sites. Below are the properties which will
 * be displayed in this component
 * 
 * </p>
 */
@Component(description = "QuoteViewHelperImpl", immediate = true, metatype = true, label = "QuoteViewHelperImpl")
@Service(value = { QuoteViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.QUOTE, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class QuoteViewHelperImpl extends BaseViewHelper {
 /**
  * Constant for retrieving quote component property.
  */
 
 /** The Constant QUOTE_TEXT. */
 private static final String QUOTE_TEXT = "text";
 
 /** The Constant REFERENCE. */
 private static final String REFERENCE = "reference";
 
 /** The Constant QUOTE_IMAGE_PATH. */
 private static final String QUOTE_IMAGE_PATH = "quoteImage";
 
 /** The Constant QUOTE_IMAGE_PATH_JSON_LABEL. */
 private static final String QUOTE_IMAGE_PATH_JSON_LABEL = "quoteImage";
 
 /** The Constant QUOTE_IMAGE_ALT_TEXT. */
 private static final String QUOTE_IMAGE_ALT_TEXT = "imageAltText";
 
 /** The Constant ENABLE_SOCIAL_SHARE. */
 private static final String ENABLE_SOCIAL_SHARE = "enableSocialShare";
 
 /** The Constant SOCIAL_SHARING. */
 private static final String SOCIAL_SHARING = "socialSharing";
 
 @Reference
 ConfigurationService configurationService;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(QuoteViewHelperImpl.class);
 
 /**
  * The method processData overrides method is responsible validating author input and returning a list of sections containing
  * text,reference,enableSocialShare and socialSharing This method converts the multiwidget input (a String Array) into sections of maps so that it
  * can be accessed in JSP in consistent way.This method also validates the link url provided by author.
  * 
  * @param content
  *         , - {@link Map} of all user input received as input <code>ViewHelper.onGetData</code> method
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return {@link Map} holding the data
  */
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  LOGGER.debug("QuoteViewHelperImpl #processData called for processing fixed list.");
  Map<String, Object> jsonProperties = getProperties(content);
  
  Page currentPage = getCurrentPage(resources);
  
  Map<String, Object> quoteProperties = new LinkedHashMap<String, Object>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  String text = MapUtils.getString(jsonProperties, QUOTE_TEXT, StringUtils.EMPTY);
  String reference = MapUtils.getString(jsonProperties, REFERENCE, StringUtils.EMPTY);
  Map<String, Object> quoteImageMap = getQuoteImageProperties(jsonProperties, currentPage, getSlingRequest(resources));
  LOGGER.debug("created map for quote image, size of the image map is " + quoteImageMap.size());
  if (MapUtils.isNotEmpty(quoteImageMap)) {
   quoteProperties.put(QUOTE_IMAGE_PATH_JSON_LABEL, quoteImageMap);
  } else {
   Image blankImage = new Image("", "", "", currentPage, getSlingRequest(resources));
   quoteProperties.put(QUOTE_IMAGE_PATH_JSON_LABEL, blankImage.convertToMap());
  }
  
  quoteProperties.put("quoteText", text);
  quoteProperties.put("reference", reference);
  quoteProperties.put(ENABLE_SOCIAL_SHARE, MapUtils.getBoolean(jsonProperties, ENABLE_SOCIAL_SHARE, false));
  Map<String, Object> socialShareProperties = SocialShareHelper.addThisWidget(resources, configurationService);
  if (AdaptiveUtil.isAdaptive(getSlingRequest(resources))) {
   socialShareProperties.put("headline", getI18n(resources).get("socialShareAdaptive.headline"));
  }
  quoteProperties.put(SOCIAL_SHARING, socialShareProperties);
  LOGGER.debug("Quote Properties Map: " + quoteProperties);
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(getJsonNameSpace(content), quoteProperties);
  
  return data;
  
 }
 
 /**
  * Gets the quote image properties.
  * 
  * @param properties
  *         the properties
  * @param page
  *         the page
  * @param slingHttpServletRequest
  * @return the quote image properties
  */
 private Map<String, Object> getQuoteImageProperties(Map<String, Object> properties, Page page, SlingHttpServletRequest slingHttpServletRequest) {
  Map<String, Object> quoteImageMap = new LinkedHashMap<String, Object>();
  String altImage = checkNullValues(MapUtils.getString(properties, QUOTE_IMAGE_ALT_TEXT));
  String quoteImagePath = checkNullValues(MapUtils.getString(properties, QUOTE_IMAGE_PATH));
  Image quoteImage = new Image(quoteImagePath, altImage, altImage, page, slingHttpServletRequest);
  quoteImageMap = quoteImage.convertToMap();
  return quoteImageMap;
  
 }
 
 /**
  * Check null values.
  * 
  * @param key
  *         the key
  * @return the string
  */
 private String checkNullValues(Object key) {
  String value = "";
  if (null == key) {
   value = "";
  } else if ("".equals(key)) {
   value = "";
  } else {
   value = key.toString();
  }
  
  return value;
  
 }
 
}
