/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * The Class ProductNutrientsViewHelperImpl.
 */
@Component(description = "ProductNutrientsViewHelperImpl", immediate = true, metatype = true, label = "Product Nutrients Component")
@Service(value = { ProductNutrientsViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.PRODUCT_NUTRIENTS, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class ProductNutrientsViewHelperImpl extends BaseViewHelper {
 
 /** The Constant PRODUCT. */
 private static final String PRODUCT = "product";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ProductNutrientsViewHelperImpl.class);
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  
  LOGGER.info("Product Nutrients View Helper #processData called for processing fixed list.");
  
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  Map<String, Object> productProperties = new HashMap<String, Object>();
  
  Map<String, Object> productNutrientsMap = new LinkedHashMap<String, Object>();
  
  Map<String, Object> dialogProperties = getProperties(content);
  String jsonNameSpace = getJsonNameSpace(content);
  PageManager pageManager = getResourceResolver(resources).adaptTo(PageManager.class);
  SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) resources.get(ProductConstants.REQUEST);
  Resource compResource = slingRequest.getResource();
  Page currentPage = pageManager.getContainingPage(compResource);
  
  String productPath = MapUtils.getString(dialogProperties, "productPath");
  
  Page productPage = productPath != null ? currentPage.getPageManager().getPage(productPath) : getCurrentPage(resources);
  
  productProperties = ProductHelper.getProductMap(productPage, jsonNameSpace, resources);
  
  productNutrientsMap.put(PRODUCT, productProperties);
  
  data.put(jsonNameSpace, productNutrientsMap);
  return data;
 }
 
}
