/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.foundation.viewhelper.impl;

import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.dam.DAMAssetUtility;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.VideoHelper;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.Image;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * <p>
 * The goal of this component is to display the videoPlayer properties and preview images with option to play video . The component will be configured
 * by editor to provide video id preview image url and image alt text.
 * 
 * </p>
 */
@Component(description = "VideoPlayer Viewhelper Impl", immediate = true, metatype = true, label = "HeroLaunchVideo")
@Service(value = { VideoPlayerV2ViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.VIDEO_PLAYER_V2, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class VideoPlayerV2ViewHelperImpl extends BaseViewHelper {
 
 /**
  * Constant for retrieving heroLaunchVideo component property.
  */
 
 private static final String VIDEO_TITLE = "videoTitle";
 
 private static final String PREVIEW_IMAGE_ALT_TEXT = "previewImageAltText";
 
 private static final String VIDEO_ID = "videoId";
 
 private static final String PREVIEW_IMAGE = "previewImage";
 
 private static final String VIDEO_SOURCE = "videoSource";
 
 private static final String TEXT = "text";
 
 private static final String RECORD_DATE = "recordDate";
 
 /**
  * logger object.
  **/
 private static final Logger LOGGER = LoggerFactory.getLogger(VideoPlayerV2ViewHelperImpl.class);
 
 /**
  * The method processData overrides method is responsible validating author input and returning a list of sections containing rich text,text This
  * method converts the multiwidget input (a String Array) into sections of maps so that it can be accessed in JSP in consistent way.This method also
  * validates the link url provided by author.
  * 
  * @param content
  *         , - {@link Map} of all user input received as input <code>ViewHelper.onGetData</code> method
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return {@link Map} holding the data
  */
 @Override
 @SuppressWarnings("unchecked")
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  
  LOGGER.debug("hero Launch Video View Helper #processData called for processing fixed list.");
  Map<String, Object> properties = (Map<String, Object>) content.get(PROPERTIES);
  
  Map<String, Object> videoPlayerMap = new LinkedHashMap<String, Object>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  
  String videoTitle = MapUtils.getString(properties, VIDEO_TITLE, StringUtils.EMPTY);
  String altImage = MapUtils.getString(properties, PREVIEW_IMAGE_ALT_TEXT, StringUtils.EMPTY);
  String previewImage = MapUtils.getString(properties, PREVIEW_IMAGE, StringUtils.EMPTY);
  String videoSource = MapUtils.getString(properties, VIDEO_SOURCE, "youtube");
  GregorianCalendar recordDate = (GregorianCalendar) MapUtils.getObject(properties, RECORD_DATE);
  
  Image image = new Image(previewImage, altImage, videoTitle, getCurrentPage(resources), getSlingRequest(resources));
  videoPlayerMap.put("videoTitle", videoTitle);
  videoPlayerMap.put("videoSource", videoSource);
  videoPlayerMap.put("videoId", MapUtils.getString(properties, VIDEO_ID, StringUtils.EMPTY));
  videoPlayerMap.put("previewImage", image.convertToMap());
  videoPlayerMap.put("videoControls", VideoHelper.getVideoControls(properties, videoSource));
  videoPlayerMap.put("videoDescription", MapUtils.getString(properties, TEXT, StringUtils.EMPTY));
  videoPlayerMap.put("recordDate", DAMAssetUtility.getEffectiveDateFromCalendar(recordDate));
  videoPlayerMap.put("randomNumber", MapUtils.getString(properties, UnileverConstants.RANDOM_NUMBER, StringUtils.EMPTY));
  LOGGER.debug("Video Player Properties Map Size" + videoPlayerMap.size());
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(getJsonNameSpace(content), videoPlayerMap);
  
  return data;
  
 }
 
}
