/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.servlet;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.jcr.Node;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.http.osgi.services.HttpClientBuilderFactory;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.request.RequestParameterMap;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.dam.api.Asset;
import com.unilever.platform.aem.foundation.constants.CommonConstants;

/**
 * The Class ZipOnFlyServlet.
 */
@SlingServlet(label = "Unilever download zip file servlet", resourceTypes = { CommonConstants.DEFAULT_SERVLET_NAME }, methods = {
  HttpConstants.METHOD_POST, HttpConstants.METHOD_GET }, selectors = { "dwnld" }, extensions = { "zip" })
@Properties({
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.foundation.components.DownloadZipFileServlet", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "Unilever download zip file servlet", propertyPrivate = false) })
public class DownloadZipFileServlet extends SlingAllMethodsServlet {
 
 /** The Constant DATA. */
 private static final String DATA = "data.zip";
 
 /** The Constant ZIP_FILE_NAME. */
 private static final String ZIP_FILE_NAME = "zfName";
 
 /** The Constant PATH. */
 private static final String PATH = "path";
 
 /** The factory. */
 @Reference
 HttpClientBuilderFactory factory;
 
 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = 1L;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(DownloadZipFileServlet.class);
 
 /** The Constant ONE_GB. */
 private static final int ONE_GB = 1024;
 
 /** The Constant TWO_GB. */
 private static final int TWO_GB = 2048;
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
  doPost(request, response);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingAllMethodsServlet#doPost(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
  ServletOutputStream sos = response.getOutputStream();
  try {
   ResourceResolver resolver = request.getResourceResolver();
   
   RequestParameterMap requestParametrs = request.getRequestParameterMap();
   String[] files = getRequestParamsArray(requestParametrs, PATH).toArray(new String[0]);
   String zipFileName = requestParametrs.containsKey(ZIP_FILE_NAME) ? requestParametrs.getValue(ZIP_FILE_NAME).toString() : StringUtils.EMPTY;
   String data = DATA;
   
   if (files != null && files.length > 0 && resolver != null) {
    
    byte[] zip = zipFiles(files, resolver);
    
    getZipWithFileName(response, zipFileName, data);
    sos.write(zip);
    
   }
   
  } catch (Exception e) {
   sos.print(e.getMessage());
   LOGGER.error("error in doPost", e);
  } finally {
   sos.flush();
  }
  
 }
 
 /**
  * Gets the zip with file name.
  * 
  * @param response
  *         the response
  * @param zipFileName
  *         the zip file name
  * @param data
  *         the data
  * @return the zip with file name
  */
 private void getZipWithFileName(SlingHttpServletResponse response, String zipFileName, String data) {
  if (StringUtils.isNotBlank(zipFileName)) {
   response.setContentType("application/zip");
   response.setHeader("Content-Disposition", "attachment; filename=" + zipFileName);
  } else {
   response.setContentType("application/zip");
   response.setHeader("Content-Disposition", "attachment; filename=" + data);
  }
 }
 
 /**
  * Zip files.
  * 
  * @param files
  *         the files
  * @param resolver
  *         the resolver
  * @param session
  *         the session
  * @return the byte[]
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  */
 private byte[] zipFiles(String[] files, ResourceResolver resolver) throws IOException {
  ByteArrayOutputStream baos = new ByteArrayOutputStream();
  ZipOutputStream zos = new ZipOutputStream(baos);
  byte[] bytes = new byte[TWO_GB];
  File f = File.createTempFile("tmp", ".txt");
  String tempLocation = f.getParent();
  LOGGER.debug("tempLocation of downloadable file {}", tempLocation);
  for (String fileName : files) {
   
   Resource resource = resolver.getResource(fileName);
   
   File file = getFileFromInputStream(resource, tempLocation);
   FileInputStream fis = new FileInputStream(file);
   
   if (fis != null && resource != null) {
    resource.adaptTo(Node.class);
    BufferedInputStream bis = new BufferedInputStream(fis);
    zos.putNextEntry(new ZipEntry(resource.getName()));
    int bytesRead;
    while ((bytesRead = bis.read(bytes)) != -1) {
     zos.write(bytes, 0, bytesRead);
    }
    zos.closeEntry();
    bis.close();
    fis.close();
   }
  }
  zos.flush();
  baos.flush();
  zos.close();
  baos.close();
  
  return baos.toByteArray();
 }
 
 /**
  * Gets the file from input stream.
  * 
  * @param resource
  *         the resource
  * @param tempLocation
  * @return the file from input stream
  */
 private File getFileFromInputStream(Resource resource, String tempLocation) {
  
  File file = new File(tempLocation + "/" + resource.getName());
  
  InputStream inputStream = retrieveContentFromCRXRepository(resource);
  if (inputStream != null) {
   OutputStream outputStream = null;
   try {
    int read = 0;
    outputStream = new FileOutputStream(file);
    byte[] bytes = new byte[ONE_GB];
    
    while ((read = inputStream.read(bytes)) != -1) {
     outputStream.write(bytes, 0, read);
    }
    
   } catch (Exception exception) {
    LOGGER.error("Error while converting file", exception);
   } finally {
    if (inputStream != null) {
     try {
      inputStream.close();
     } catch (IOException e) {
      LOGGER.error("IOException ", e);
     }
    }
    if (outputStream != null) {
     try {
      outputStream.close();
     } catch (IOException e) {
      LOGGER.error("IOException ", e);
     }
    }
   }
  }
  
  return file;
 }
 
 /**
  * Retrieve content from crx repository.
  * 
  * @param resource
  *         the rs
  * @return the input stream
  */
 public static InputStream retrieveContentFromCRXRepository(Resource resource) {
  InputStream inputStream = null;
  try {
   
   if (resource != null) {
    Asset asset = resource.adaptTo(Asset.class);
    if (asset != null) {
     inputStream = asset.getOriginal().getStream();
    } else {
     Node ntFileNode = resource.adaptTo(Node.class);
     Node ntResourceNode = ntFileNode.getNode(JcrConstants.JCR_CONTENT);
     inputStream = ntResourceNode.getProperty(JcrConstants.JCR_DATA).getBinary().getStream();
    }
   }
  } catch (Exception exception) {
   LOGGER.error("Error while getting inputstream", exception);
  } finally {
   try {
    inputStream.close();
   } catch (IOException ioException) {
    LOGGER.error("Error while closing inputstream", ioException);
   }
  }
  return inputStream;
 }
 
 /**
  * Gets the request params array.
  * 
  * @param requestParamMap
  *         the request param map
  * @param key
  *         the key
  * @return the request params array
  */
 private Set<String> getRequestParamsArray(RequestParameterMap requestParamMap, String key) {
  
  Set<String> valueSet = new HashSet<String>();
  if (requestParamMap != null && requestParamMap.containsKey(key)) {
   for (RequestParameter rsParamMap : requestParamMap.get(key)) {
    valueSet.add(rsParamMap.getString());
   }
  }
  return valueSet;
  
 }
}
