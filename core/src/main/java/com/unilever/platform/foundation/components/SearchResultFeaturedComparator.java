/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.components;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.jcr.RepositoryException;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unilever.platform.aem.foundation.core.dto.SearchDTO;
import com.unilever.platform.foundation.viewhelper.constants.ResourceCommonConstants;

/**
 * The Class SearchResultFeaturedComparator.
 */
public class SearchResultFeaturedComparator implements Comparator<Object> {
 
 /** The tags list. */
 private List<String> tagsDataList;
 
 /** The featuretag. */
 private String[] featuredtag;
 
 /** The sort by date. */
 boolean sortedByDate;
 /**
  * logger object.
  **/
 private static final Logger LOGGER = LoggerFactory.getLogger(SearchResultFeaturedComparator.class);
 
 /**
  * Instantiates a new search result comparator.
  * 
  * @param tagsCollection
  *         the tags collection
  * @param featuretag
  *         the featuretag
  * @param sortByDate
  *         the sort by date
  */
 public SearchResultFeaturedComparator(List<String> tagsCollection, String[] featuretag, boolean sortByDate) {
  super();
  tagsDataList = tagsCollection;
  this.featuredtag = featuretag != null?featuretag:new String[]{};
  this.sortedByDate = sortByDate;
 }
 
 @Override
 /*
  * (non-Javadoc)
  * 
  * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
  */
 public int compare(Object resultNodeObj1, Object resultNodeObj2) {
  SearchDTO resultNode1 = (SearchDTO) resultNodeObj1;
  SearchDTO resultNode2 = (SearchDTO) resultNodeObj2;
  
  boolean featured1 = false;
  boolean featured2 = false;
  
  int count1 = 0;
  int count2 = 0;
  Date resultNodeOneUpdatedDate = null;
  Date resultNodeTwoUpdatedDate = null;
  try {
   for (Value tag1 : resultNode1.getResultNodeTags()) {
    if (tagsDataList.contains(tag1.getString())) {
     count1++;
    }
   }
   resultNodeOneUpdatedDate = resultNode1.getReplicationTime();
   for (Value tag2 : resultNode2.getResultNodeTags()) {
    if (tagsDataList.contains(tag2.getString())) {
     count2++;
    }
   }
   boolean[] featuredFlagArray = setFeaturedFlags(featuredtag, resultNode1, resultNode2, featured1, featured2);
   featured1 = featuredFlagArray[0];
   featured2 = featuredFlagArray[1];
   resultNodeTwoUpdatedDate = resultNode2.getReplicationTime();
  } catch (ValueFormatException e) {
   LOGGER.error("Value format exception " + e);
  } catch (IllegalStateException e) {
   LOGGER.error("Illegal State Exception " + e);
  } catch (RepositoryException e) {
   LOGGER.error("Repository Exception " + e);
  }
  
  return getResultAfterSorting(featured1, featured2, count1, count2, resultNodeOneUpdatedDate, resultNodeTwoUpdatedDate);
    }
    
    /**
     * sets result after sorting pages based on current criteria
     * 
     * @param featured1
     * @param featured2
     * @param count1
     * @param count2
     * @param resultNodeOneUpdatedDate
     * @param resultNodeTwoUpdatedDate
     * @param res
     * @return
     */
 private int getResultAfterSorting(boolean featured1, boolean featured2, int count1, int count2, Date resultNodeOneUpdatedDate,
            Date resultNodeTwoUpdatedDate) {
  int res = count2 - count1;
  if (featured1 == featured2) {
   if (count2 > count1) {
    res = 1;
    return res;
            } else if (count2 < count1) {
    res = -1;
    return res;
            }
   res = getResultWithSameFeaturedFlag(resultNodeOneUpdatedDate, resultNodeTwoUpdatedDate, res);
        } else {
   if (featured1) {
    res = -1;
            } else if (featured2) {
    res = 1;
            }
        }
  return res;
    }
    
    /**
     * returns result
     * 
     * @param count1
     * @param count2
     * @param resultNodeOneUpdatedDate
     * @param resultNodeTwoUpdatedDate
     * @param res
     * @return
     */
 private int getResultWithSameFeaturedFlag(Date resultNodeOneUpdatedDate, Date resultNodeTwoUpdatedDate, int res) {
  int localResult = res;
  if (this.sortedByDate && localResult == 0 && (resultNodeOneUpdatedDate != null && resultNodeTwoUpdatedDate != null)) {
   localResult = setResultBySortDate(resultNodeOneUpdatedDate, resultNodeTwoUpdatedDate, localResult);
        } else if (this.sortedByDate && resultNodeOneUpdatedDate != null) {
   localResult = -1;
        } else if (this.sortedByDate && resultNodeTwoUpdatedDate != null) {
   localResult = 1;
        } else {
   localResult = -1;
        }
  return localResult;
    }

    /**sets result by sorting date
     * @param resultNodeOneUpdatedDate
     * @param resultNodeTwoUpdatedDate
     * @param localResult
     * @return
     */
 private int setResultBySortDate(Date resultNodeOneUpdatedDate, Date resultNodeTwoUpdatedDate, int localResult) {
  int result = localResult;
  if (resultNodeOneUpdatedDate.compareTo(resultNodeTwoUpdatedDate) > 0) {
   result = -1;
        } else if (resultNodeOneUpdatedDate.compareTo(resultNodeTwoUpdatedDate) < 0) {
   result = 1;
        }
  return result;
    }
    
    /**
     * sets boolean falg array with featured 1 and featured 2 flags
     * 
     * @param featuredtag
     * @param resultNode1
     * @param resultNode2
     * @param featured1
     * @param featured2
     * @return
     */
 private boolean[] setFeaturedFlags(String[] featuredtag, SearchDTO resultNode1, SearchDTO resultNode2, boolean featured1, boolean featured2) {
  boolean[] featuredFlagArray = new boolean[ResourceCommonConstants.TWO];
  featuredFlagArray[0] = featured1;
  featuredFlagArray[1] = featured2;
  for (int i = 0; i < featuredtag.length; i++) {
   try {
    for (Value tag1 : resultNode1.getResultNodeTags()) {
     if (tag1.getString().startsWith(featuredtag[i])) {
      featuredFlagArray[0] = true;
      break;
                    }
                }
    for (Value tag2 : resultNode2.getResultNodeTags()) {
     if (tag2.getString().startsWith(featuredtag[i])) {
      featuredFlagArray[1] = true;
      break;
                    }
                }
    if (featuredFlagArray[0] != featuredFlagArray[1]) {
     break;
                }
            } catch (RepositoryException e) {
    LOGGER.error("Repository Exception " + e);
            }
        }
  return featuredFlagArray;
    }
}
