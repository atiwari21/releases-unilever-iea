/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.SocialShareHelper;
import com.unilever.platform.foundation.components.helper.SocialTABHelper;
import com.unilever.platform.foundation.constants.CommonConstants;
import com.unilever.platform.foundation.template.dto.SocialTabQueryDTO;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * The Class FeaturedInstagramPostViewHelperImpl.
 * 
 */
@Component(description = "FeaturedInstagramPostViewHelperImpl", immediate = true, metatype = true, label = "Featured Instagram Post Component")
@Service(value = { FeaturedInstagramPostViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.FEATURED_INSTAGRAM_POST, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class FeaturedInstagramPostViewHelperImpl extends SocialTabBaseViewHelperImpl {
 
 /** The Constant SHOW_NUMBER_OF_VIEW. */
 private static final String SHOW_NUMBER_OF_VIEW = "showNumberOfView";
 
 /** The Constant SHOW_PROFILE_PIC. */
 private static final String SHOW_PROFILE_PIC = "showProfilePic";
 
 /** The Constant SOCIAL_SHARING. */
 private static final String SOCIAL_SHARING = "socialSharing";
 
 /** The Constant ACCOUNT_ID. */
 private static final String ACCOUNT_ID = "accountId";
 
 /** The Constant CTA_LABEL_KEY. */
 private static final String CTA_LABEL_KEY = "featuredInstagramPostTab.ctaLabel";
 
 /** The Constant READ_LESS_TEXT. */
 private static final String READ_LESS_TEXT = "featuredInstagramPost.readLessLabelText";
 
 /** The Constant READ_MORE_TEXT. */
 private static final String READ_MORE_TEXT = "featuredInstagramPost.readMoreLabelText";
 
 /** The Constant CTA_LABEL. */
 private static final String CTA_LABEL = "ctaLabel";
 
 /** The Constant CTA_URL. */
 private static final String CTA_URL = "ctaUrl";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(FeaturedInstagramPostViewHelperImpl.class);
 
 /** The Constant FEATURED_INSTAGRAM_POST_CAT. */
 private static final String FEATURED_INSTAGRAM_POST_CAT = "featuredInstagramPost";
 
 /** The Constant ZERO. */
 private static final Integer ZERO = 0;
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.impl.SocialTabBaseViewHelperImpl#processData(java.util.Map, java.util.Map)
  */
 @SuppressWarnings("unchecked")
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  
  LOGGER.info("Brand Social Channels View Helper #processData called for processing fixed list.");
  
  Map<String, Object> ctaMap = new LinkedHashMap<String, Object>();
  
  Map<String, Object> properties = getProperties(content);
  Page currentPage = getCurrentPage(resources);
  
  String jsonNameSpace = getJsonNameSpace(content);
  I18n i18n = getI18n(resources);
  
  Map<String, Object> featuredInstagramPostMap = new LinkedHashMap<String, Object>();
  
  String postId = getPostId(properties);
  String accountId = getAccountId(properties);
  
  String ctaLabel = i18n.get(CTA_LABEL_KEY);
  String readMoreText = i18n.get(READ_MORE_TEXT);
  String readLessText = i18n.get(READ_LESS_TEXT);
  String numberOfView = MapUtils.getString(properties, SHOW_NUMBER_OF_VIEW, StringUtils.EMPTY);
  String profilePicture = MapUtils.getString(properties, SHOW_PROFILE_PIC, StringUtils.EMPTY);
  String socialSharing = MapUtils.getString(properties, SOCIAL_SHARING, StringUtils.EMPTY);
  String newWindow = MapUtils.getString(properties, OPEN_IN_NEW_WINDOW, StringUtils.EMPTY);
  
  String showNumberOfView = numberOfView != null && conditionCheck(numberOfView) ? CommonConstants.BOOLEAN_TRUE : ProductConstants.FALSE;
  String showProfilePicture = profilePicture != null && conditionCheck(profilePicture) ? CommonConstants.BOOLEAN_TRUE : ProductConstants.FALSE;
  String isSocialSharing = socialSharing != null && conditionCheck(socialSharing) ? CommonConstants.BOOLEAN_TRUE : ProductConstants.FALSE;
  String openInNewWindow = newWindow != null && "true".equals(newWindow) ? CommonConstants.BOOLEAN_TRUE : ProductConstants.FALSE;
  
  ctaMap.put(CTA_LABEL, ctaLabel);
  ctaMap.put(CTA_URL, MapUtils.getString(properties, CTA_URL, StringUtils.EMPTY));
  ctaMap.put(OPEN_IN_NEW_WINDOW, Boolean.valueOf(openInNewWindow));
  
  featuredInstagramPostMap.put("cta", ctaMap);
  featuredInstagramPostMap.put("showNumberOfView", Boolean.valueOf(showNumberOfView));
  featuredInstagramPostMap.put("showProfilePicture", Boolean.valueOf(showProfilePicture));
  featuredInstagramPostMap.put("isSocialSharing", Boolean.valueOf(isSocialSharing));
  featuredInstagramPostMap.put("readMoreText", readMoreText);
  featuredInstagramPostMap.put("readLessText", readLessText);
  
  if (Boolean.valueOf(isSocialSharing)) {
   featuredInstagramPostMap.put(SOCIAL_SHARING, SocialShareHelper.addThisWidget(resources, configurationService));
  }
  
  getRetrivalQueryMap(resources, properties, currentPage, featuredInstagramPostMap, postId, accountId);
  
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  data.put(jsonNameSpace, featuredInstagramPostMap);
  
  return data;
 }
 
 /**
  * Condition check.
  * 
  * @param numberOfView
  *         the number of view
  * @return true, if successful
  */
 private static boolean conditionCheck(String numberOfView) {
  return "true".equals(numberOfView);
 }
 
 /**
  * Retrival query new map.
  * 
  * @param resources
  *         the resources
  * @param properties
  *         the properties
  * @param currentPage
  *         the current page
  * @param featuredInstagramPostMap
  *         the featured instagram post map
  * @param postId
  *         the post id
  * @param accountId
  *         the account id
  */
 @SuppressWarnings("unchecked")
 private void getRetrivalQueryMap(Map<String, Object> resources, Map<String, Object> properties, Page currentPage,
   Map<String, Object> featuredInstagramPostMap, String postId, String accountId) {
  Map<String, String> configMap = ComponentUtil.getConfigMap(currentPage, FEATURED_INSTAGRAM_POST_CAT);
  int numberOfPosts = MapUtils.getInteger(configMap, NUMBER_OF_POSTS, ZERO);
  int maxImages = MapUtils.getInteger(configMap, MAXIMUM_IMAGES, ZERO);
  
  SocialTabQueryDTO tabQuery = getSocialAggregatorQueryDataObj(resources, properties, currentPage, configurationService, numberOfPosts, maxImages);
  SocialTABHelper.addRetrievalQueryParametrsMap(featuredInstagramPostMap, tabQuery, true);
  
  Map<String, Object> retrivalQueryParams = (Map<String, Object>) featuredInstagramPostMap.get("retrivalQueryParams");
  Object[] socialChannelNamesMapArr = (Object[]) retrivalQueryParams.get("socialChannelNames");
  if (socialChannelNamesMapArr.length > 0) {
   Map<String, Object> socialChannelNamesMap = (Map<String, Object>) socialChannelNamesMapArr[0];
   socialChannelNamesMap.put("postId", postId);
   if (accountId != null && !accountId.isEmpty()) {
    socialChannelNamesMap.put(ACCOUNT_ID, accountId);
   }
  }
 }
}
