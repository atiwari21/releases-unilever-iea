/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.servlet;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.ServletException;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.http.util.EntityUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONArray;
import org.json.JSONObject;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.unilever.platform.aem.foundation.core.service.SearchRetrivalService;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.constants.CommonConstants;

/**
 * The Class RecipiePageRetrievalServlet.
 */
@SlingServlet(paths = { "/bin/public/cms/recipiePages" }, methods = { HttpConstants.METHOD_GET, HttpConstants.METHOD_POST }, extensions = { "json" }, label = "Recipie Page Retrieval Servlet", selectors = { "data" })
@Properties(value = {
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.foundation.servlet.RecipiePageRetrievalServlet", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "Recipie Page Retrieval", propertyPrivate = false) })
public class SearchDataServlet extends SlingAllMethodsServlet {
 
 /** The Constant TAG_ID. */
 private static final String TAG_ID = "tagID";
 
 /** The Constant INPUT_PATH. */
 private static final String INPUT_PATH = "inputPath";
 
 /** The Constant SUB_BRAND. */
 private static final String SUB_BRAND = "subBrand";
 
 /**
     * 
     */
 private static final long serialVersionUID = 3719683544395870819L;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(SearchDataServlet.class);
 
 @Reference
 private QueryBuilder builder;
 
 @Reference
 private SearchRetrivalService service;
 
 @Override
 protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
  String searchInputPath = request.getParameter(INPUT_PATH);
  String subBrandName = request.getParameter(SUB_BRAND);
  JSONArray array = new JSONArray();
  String[] a = ArrayUtils.EMPTY_STRING_ARRAY;
  a = service.getTagsRelation(searchInputPath, subBrandName);
  for (String query : a) {
   array.put(query);
  }
  response.getWriter().write(array.toString());
 }
 
 @SuppressWarnings("deprecation")
 @Override
 protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
  if (request.getRequestParameterMap().containsKey(INPUT_PATH) && request.getRequestParameterMap().containsKey(SUB_BRAND)) {
   String searchInputPath = (String) request.getParameter(INPUT_PATH);
   String subBrandName = (String) request.getParameter(SUB_BRAND);
   JSONArray array = new JSONArray();
   String[] a = ArrayUtils.EMPTY_STRING_ARRAY;
   a = service.getTagsRelation(searchInputPath, URLDecoder.decode(subBrandName)); 
   for (String query : a) {
    array.put(query);
   }
   response.getWriter().write(array.toString());
   response.setContentType("application/json");
   response.setCharacterEncoding("utf-8");
  } else {
   String brand = (String) request.getParameter("brand");
   String locale = (String) request.getParameter("locale");
   String limit = (String) request.getParameter("l");
   String offset = (String) request.getParameter("o");
   String pagePath = "/content";
   if (StringUtils.isNotEmpty(brand)) {
    pagePath += "/" + StringUtils.lowerCase(brand);
    if (StringUtils.isNotEmpty(locale)) {
     locale = locale.toLowerCase().replace("-", "_");
     pagePath += "/" + locale;
    }
   }
   if (request.getParameterMap().containsKey("ids")) {
    Map<String, String> map = new HashMap<String, String>();
    map.put("path", pagePath);
    map.put("type", "cq:PageContent");
    map.put("property", "recipeId");
    String propertyValue = "property.%s_value";
    String ids = request.getParameter("ids");
    if (ids.endsWith(",")) {
     ids = ids.substring(0, ids.length() - 1);
    }
    String[] idsArray = ids.split(",");
    List<String> idList = new LinkedList<String>(Arrays.asList(idsArray));
    for (int i = 0; i < idsArray.length; i++) {
     map.put(String.format(propertyValue, i + 1), idsArray[i]);
    }
    String properties = "jcr:title jcr:description jcr:path sling:alias recipeId";
    map.put("properties", properties);
    map.put("p.hits", "selective");
    if (StringUtils.isNotEmpty(limit) && StringUtils.isNotEmpty(offset)) {
     map.put("p.limit", limit);
     map.put("p.offset", offset);
    }
    ResourceResolver resolver = request.getResourceResolver();
    Query query = builder.createQuery(PredicateGroup.create(map), resolver.adaptTo(Session.class));
    JSONObject jsonObject = new JSONObject();
    TagManager tagManager = resolver.adaptTo(TagManager.class);
    SearchResult result = query.getResult();
    long totMatches = result.getTotalMatches();
    List<Hit> hits = result.getHits();
    int currentCount = hits.size();
    jsonObject.put("totalResults", totMatches);
    jsonObject.put("matches", currentCount);
    jsonObject.put("startIndex", result.getStartIndex());
    JSONObject hitObjectMap = new JSONObject();
    for (Hit hit : hits) {
     JSONObject hitObject = new JSONObject();
     ValueMap valueMap = null;
     try {
      valueMap = hit.getProperties();
      Resource jcrResource = resolver.getResource(hit.getPath());
      String path = jcrResource.getParent().getPath();
      
      if (request.getRequestParameterMap().containsKey("env")) {
       hitObject.put("path", path);
      } else {
       String alias = (String) valueMap.getOrDefault("sling:alias", StringUtils.EMPTY);
       path = ComponentUtil.getFullURL(resolver, path, request);
       if (StringUtils.isNotEmpty(alias)) {
        alias = "/" + alias;
        path = path.replaceAll(path.substring(path.lastIndexOf("/"), path.length()), alias);
        path = ComponentUtil.getFullURL(resolver, path, request);
        if (!StringUtils.contains(path, CommonConstants.PAGE_EXTENSION)) {
         path += CommonConstants.PAGE_EXTENSION;
        }
       }
       hitObject.put("path", path);
      }
      Tag[] tags = tagManager.getTags(jcrResource);
      List<JSONObject> tagArray = new ArrayList<JSONObject>();
      for (Tag tag : tags) {
       JSONObject tagObject = new JSONObject();
       String tagID = tag.getTagID();
       tagObject.put(TAG_ID, tagID);
       tagObject.put("title", tag.getTitle(request.getLocale()));
       tagArray.add(tagObject);
      }
      Comparator<JSONObject> c = new Comparator<JSONObject>() {
       
       @Override
       public int compare(JSONObject jsonObject1, JSONObject jsonObject2) {
        return ((String) jsonObject1.get(TAG_ID)).compareTo((String) jsonObject2.get(TAG_ID));
       }
      };
      
      Collections.sort(tagArray, c);
      hitObject.put("tags", tagArray);
      hitObject.put("title", valueMap.getOrDefault(JcrConstants.JCR_TITLE, StringUtils.EMPTY));
      hitObject.put("description", valueMap.getOrDefault(JcrConstants.JCR_DESCRIPTION, StringUtils.EMPTY));
      String recipeId = (String) valueMap.get("recipeId");
      hitObjectMap.put(recipeId, hitObject);
      idList.remove(recipeId);
      
     } catch (RepositoryException e) {
      LOGGER.error("", e);
     }
    }
    if (!idList.isEmpty()) {
     for (String id : idList) {
      hitObjectMap.put(id, MapUtils.EMPTY_MAP);
     }
    }
    jsonObject.put("hits", hitObjectMap);
    response.setContentType("application/json");
    response.setCharacterEncoding("utf-8");
    response.getWriter().write(jsonObject.toString());
   }
  }
 }
}
