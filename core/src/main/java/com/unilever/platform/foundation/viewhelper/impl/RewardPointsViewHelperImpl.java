/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * <p>
 * Responsible for reading author input for Reward Points component and generating JSON containing all properties entered by author in dialog or
 * config.
 * </p>
 */
@Component(description = "RewardPointsViewHelperImpl", immediate = true, metatype = true, label = "RewardPointsViewHelperImpl")
@Service(value = { RewardPointsViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.REWARD_POINTS, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class RewardPointsViewHelperImpl extends BaseViewHelper {
 
 /** Constant for code entered label. */
 private static final String CODE_ENTERED_LABEL = "codeEnteredLabel";
 
 /** Constant for code entered heading text. */
 private static final String CODE_ENTERED_HEADING_TEXT = "codeEnteredHeadingText";
 
 /** Constant for step points label. */
 private static final String POINTS_LABEL = "pointsLabel";
 
 /** Constant for points heading text. */
 private static final String POINTS_HEADING_TEXT = "pointsHeadingText";
 /**
  * logger object.
  **/
 private static final Logger LOGGER = LoggerFactory.getLogger(RewardPointsViewHelperImpl.class);
 
 /** Constant for global configuration category of reward points component */
 private static final String GLOBAL_CONFIG_CAT = "rewardPoints";
 
 /** Constant for service URL */
 private static final String SERVICE_URL = "serviceUrl";
 
 @Reference
 ConfigurationService configurationService;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.info("RewardPointsViewHelperImpl #processData called for processing fixed list.");
  
  Map<String, Object> rewardPointsProperties = getProperties(content);
  Map<String, Object> rewardPointsFinalMap = new LinkedHashMap<String, Object>();
  
  String showType = MapUtils.getString(rewardPointsProperties, "show", StringUtils.EMPTY);
  String pointsHeadingText = MapUtils.getString(rewardPointsProperties, POINTS_HEADING_TEXT, StringUtils.EMPTY);
  String pointsLabel = MapUtils.getString(rewardPointsProperties, POINTS_LABEL, StringUtils.EMPTY);
  String codeEnteredHeadingText = MapUtils.getString(rewardPointsProperties, CODE_ENTERED_HEADING_TEXT, StringUtils.EMPTY);
  String codeEnteredLabel = MapUtils.getString(rewardPointsProperties, CODE_ENTERED_LABEL, StringUtils.EMPTY);
  String rewardServiceUrl = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, getCurrentPage(resources), GLOBAL_CONFIG_CAT,
    SERVICE_URL);
  rewardPointsFinalMap.put("rewardServiceUrl", rewardServiceUrl);
  
  if ("pointsAndNumber".equals(showType)) {
   rewardPointsFinalMap.put(POINTS_HEADING_TEXT, pointsHeadingText);
   rewardPointsFinalMap.put(POINTS_LABEL, pointsLabel);
   rewardPointsFinalMap.put(CODE_ENTERED_HEADING_TEXT, codeEnteredHeadingText);
   rewardPointsFinalMap.put(CODE_ENTERED_LABEL, codeEnteredLabel);
  } else if ("pointsOnly".equals(showType)) {
   rewardPointsFinalMap.put(POINTS_HEADING_TEXT, pointsHeadingText);
   rewardPointsFinalMap.put(POINTS_LABEL, pointsLabel);
  } else {
   rewardPointsFinalMap.put(CODE_ENTERED_HEADING_TEXT, codeEnteredHeadingText);
   rewardPointsFinalMap.put(CODE_ENTERED_LABEL, codeEnteredLabel);
  }
  
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(getJsonNameSpace(content), rewardPointsFinalMap);
  return data;
 }
 
}
