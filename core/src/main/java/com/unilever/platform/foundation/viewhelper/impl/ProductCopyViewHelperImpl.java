/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.adobe.cq.commerce.api.Product;
import com.adobe.cq.commerce.common.CommerceHelper;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.cache.ComponentViewAndViewHelperCache;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.utils.CollectionUtils;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.utils.AdaptiveUtil;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.components.helper.ProductHelperExt;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * The class ProductCopyViewHelperImpl
 */
@Component(description = "ProductCopyViewHelperImpl", immediate = true, metatype = true, label = "ProductCopyViewHelperImpl")
@Service(value = { ProductCopyViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.PRODUCT_COPY, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class ProductCopyViewHelperImpl extends BaseViewHelper {
 
 /**
  * Constant for retrieving product copy source text.
  */
 private static final String PRODUCT_COPY_SOURCE_TEXT = "text";
 /**
  * Constant for retrieving accordion source product data property.
  */
 private static final String PRODUCT_COPY_DATA = "product data";
 /**
  * Constant for retrieving product copy attribute property .
  */
 private static final String PRODUCT_COPY_PROPERTY_NAME = "productCopyPropertyName";
 /**
  * Constant for setting type of retrieving data either by manual entry or product data.
  */
 private static final String PRODUCT_COPY_SOURCE = "source";
 
 /** Constant for product data title. */
 private static final String PRODUCT_COPY_PRODUCT_TITLE = "productCopyCategoryTitle";
 
 /** Constant for product copy title. */
 private static final String PRODUCT_COPY_TITLE = "productCopyTitle";
 
 /** Constant for product copy description. */
 private static final String PRODUCT_COPY_TEXT = "productCopyText";
 
 /** Constant for heading label. */
 private static final String PRODUCT_COPY_HEADING = "headline";
 
 /** Constant forgetting adaptive attribute topLink. */
 private static final String TOP_LINK_I18N = "productCopy.topLink";
 
 /** Constant for description label. */
 private static final String PRODUCT_COPY_DESCRIPTION = "description";
 
 /** Constant for description label. */
 private static final String TOP_LINK = "topLink";
 
 /**
  * logger object.
  **/
 private static final Logger LOGGER = LoggerFactory.getLogger(ProductCopyViewHelperImpl.class);
 
 /** The component view and view helper cache. */
 @Reference
 ComponentViewAndViewHelperCache componentViewAndViewHelperCache;
 
 /**
  * The method processData overrides method is responsible validating author input and returning a list of sections containing rich text,text This
  * method converts the multiwidget input (a String Array) into sections of maps so that it can be accessed in JSP in consistent way.This method also
  * validates the link url provided by author.
  * 
  * @param content
  *         , - {@link Map} of all user input received as input <code>ViewHelper.onGetData</code> method
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return {@link Map} holding the data
  */
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  
  LOGGER.info("Product Copy View Helper #processData called for processing fixed list.");
  
  Map<String, Object> productCopyMap = new LinkedHashMap<String, Object>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  
  productCopyMap = getProductCopyData(content, resources);
  return productCopyMap;
 }
 
 /**
  * @param resources
  * @param content
  * @return
  */
 @SuppressWarnings("unchecked")
 public Map<String, Object> getProductCopyData(final Map<String, Object> content, final Map<String, Object> resources) {
  Map<String, Object> productCopyMap = new LinkedHashMap<String, Object>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  Map<String, Object> productCopyProperties = (Map<String, Object>) content.get(PROPERTIES);
  String jsonNameSpace = MapUtils.getString(content, "jsonNameSpace");
  
  String productCopySource = MapUtils.getString(productCopyProperties, PRODUCT_COPY_SOURCE);
  String productCopyTitle = MapUtils.getString(productCopyProperties, PRODUCT_COPY_TITLE);
  String productCopyProductTitle = MapUtils.getString(productCopyProperties, PRODUCT_COPY_PRODUCT_TITLE);
  SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) resources.get(ProductConstants.REQUEST);
  boolean adaptiveNature = AdaptiveUtil.isAdaptive(slingRequest);
  String adaptiveTopLInk = getI18nTopLink(resources);
  // checking source entry
  if (StringUtils.isNotEmpty(productCopySource) && productCopySource.equals(PRODUCT_COPY_SOURCE_TEXT)) {
   
   populateTextData(productCopyProperties, productCopyMap, jsonNameSpace, productCopyTitle, adaptiveTopLInk, adaptiveNature);
  } else if (StringUtils.isNotEmpty(productCopySource) && productCopySource.equals(PRODUCT_COPY_DATA)) {
   productCopyMap = populateProductData(resources, productCopyProperties, jsonNameSpace, productCopyProductTitle);
   
  } else {
   List<Map<String, Object>> blankProductCopyListFinal = new ArrayList<Map<String, Object>>();
   Map<String, Object> blankProductCopyLists = new LinkedHashMap<String, Object>();
   blankProductCopyLists.put(PRODUCT_COPY_HEADING, "");
   blankProductCopyLists.put(PRODUCT_COPY_DESCRIPTION, blankProductCopyListFinal.toArray());
   productCopyMap.put(jsonNameSpace, blankProductCopyLists);
  }
  
  return productCopyMap;
 }
 
 /**
  * @param resources
  * @return
  */
 private String getI18nTopLink(Map<String, Object> resources) {
  return getI18n(resources).get(TOP_LINK_I18N);
 }
 
 /**
  * This method is used to generate a map based on text authored.
  * 
  * @param productCopyProperties
  *         the product copy properties
  * @param productCopyMap
  *         the product copy map
  * @param jsonNameSpace
  *         the json name space
  * @param productCopyTitle
  *         the product copy title
  * @return product copy map
  */
 private void populateTextData(Map<String, Object> productCopyProperties, Map<String, Object> productCopyMap, String jsonNameSpace,
   String productCopyTitle, String adaptiveTopLink, boolean adaptiveNature) {
  String featureAdaptiveTopLink = adaptiveTopLink;
  boolean adaptiveNatureFlag = adaptiveNature;
  List<String> propertyNames = new ArrayList<String>();
  propertyNames.add(PRODUCT_COPY_TEXT);
  LOGGER.debug("created product Copy component properties Map,size of the Map is : " + productCopyProperties.size());
  
  List<Map<String, String>> productCopySectionsList = CollectionUtils.convertMultiWidgetToList(productCopyProperties, propertyNames);
  
  List<Map<String, Object>> sectionsListFinal = prepareSectionList(productCopySectionsList);
  
  LOGGER.debug("Final list of Map for product copy created,size of the list is :- " + sectionsListFinal.size());
  Map<String, Object> productCopyList = new LinkedHashMap<String, Object>();
  productCopyList.put(PRODUCT_COPY_HEADING, productCopyTitle);
  productCopyList.put(PRODUCT_COPY_DESCRIPTION, sectionsListFinal.toArray());
  if (adaptiveNatureFlag && StringUtils.isNotBlank(featureAdaptiveTopLink)) {
   productCopyList.put(TOP_LINK, adaptiveTopLink);
  }
  productCopyMap.put(jsonNameSpace, productCopyList);
 }
 
 /**
  * This method is used to retrieve product copy description in a map using product copy attributes.
  * 
  * @param resources
  *         the resources
  * @param productCopyProperties
  *         the product copy properties
  * @param jsonNameSpace
  *         the json name space
  * @param productCopyProductTitle
  *         the product copy product title
  * @return product copy map
  */
 private Map<String, Object> populateProductData(final Map<String, Object> resources, Map<String, Object> productCopyProperties,
   String jsonNameSpace, String productCopyProductTitle) {
  Map<String, Object> productCopyMap;
  ResourceResolver resourceResolver = (ResourceResolver) resources.get(ProductConstants.RESOURCE_RESOLVER);
  SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) resources.get(ProductConstants.REQUEST);
  Resource currentResource = slingRequest.getResource();
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  Product currentProduct = CommerceHelper.findCurrentProduct(pageManager.getContainingPage(currentResource));
  if (currentProduct == null) {
   currentProduct = getProductFromSelector(pageManager.getContainingPage(currentResource), slingRequest);
  }
  List<String> productCopyPropertyName = new ArrayList<String>();
  productCopyPropertyName.add(PRODUCT_COPY_PROPERTY_NAME);
  LOGGER.debug("created product Copy component properties Map,size of the Map is :" + productCopyProperties.size());
  String adaptiveTopLinkData = getI18nTopLink(resources);
  // adding list to list of map
  List<Map<String, String>> productCopySectionsList = CollectionUtils.convertMultiWidgetToList(productCopyProperties, productCopyPropertyName);
  
  Map<String, Object> productCopyProductProperties = new LinkedHashMap<String, Object>();
  List<Map<String, Object>> productCopyDetailList = new ArrayList<Map<String, Object>>();
  int i = 0;
  int sectionListLength = productCopySectionsList.size();
  if (currentProduct != null) {
   Map<String, Object> productCopyProductDetails = null;
   productCopyProductProperties.put(PRODUCT_COPY_HEADING, productCopyProductTitle);
   // creating list of product copy attributes which will look for
   // product
   for (i = 0; i < sectionListLength; i++) {
    productCopyProductDetails = new LinkedHashMap<String, Object>();
    Map<String, String> map = productCopySectionsList.get(i);
    String propertyNameKey = map.values().iterator().next();
    String productPropertyAttribute = ProductHelper.getProperty(currentProduct, propertyNameKey);
    if (StringUtils.isNotEmpty(productPropertyAttribute)) {
     productCopyProductDetails.put(PRODUCT_COPY_SOURCE_TEXT, productPropertyAttribute);
    }
    if (MapUtils.isNotEmpty(productCopyProductDetails)) {
     productCopyDetailList.add(productCopyProductDetails);
    }
   }
   LOGGER.debug("created list of product properties for product copy,size of the list is " + productCopyDetailList.size());
   productCopyProductProperties.put(PRODUCT_COPY_DESCRIPTION, productCopyDetailList.toArray());
   if (AdaptiveUtil.isAdaptive(slingRequest) && StringUtils.isNotBlank(adaptiveTopLinkData)) {
    productCopyProductProperties.put(TOP_LINK, adaptiveTopLinkData);
   }
  } else {
   productCopyProductProperties.put(PRODUCT_COPY_DESCRIPTION, productCopyDetailList.toArray());
   
   LOGGER.info("Product cannot be found for current page {}", currentResource.getPath());
   
  }
  
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(jsonNameSpace, productCopyProductProperties);
  productCopyMap = data;
  return productCopyMap;
 }
 
 /**
  * This method is used to prepare section list based on the input provided by author.
  * 
  * @param sectionsList
  *         section list
  * @return the list of maps
  */
 private List<Map<String, Object>> prepareSectionList(final List<Map<String, String>> sectionsList) {
  
  List<Map<String, Object>> finalSectionsList = new ArrayList<Map<String, Object>>();
  Map<String, Object> finalSectionsMap = null;
  for (Map<String, String> sectionMap : sectionsList) {
   
   String sectionHeading = sectionMap.get(PRODUCT_COPY_TEXT);
   
   if (StringUtils.isNotEmpty(sectionHeading)) {
    finalSectionsMap = new LinkedHashMap<String, Object>();
    finalSectionsMap.put(PRODUCT_COPY_SOURCE_TEXT, sectionHeading);
    
    finalSectionsList.add(finalSectionsMap);
    
   }
  }
  
  return finalSectionsList;
 }
 
 /**
  * 
  * @param page
  * @param slingRequest
  * @return {@link Product}
  */
 private Product getProductFromSelector(Page page, SlingHttpServletRequest slingRequest) {
  String productId = slingRequest.getParameter(ProductConstants.EAN) != null ? slingRequest.getParameter(ProductConstants.EAN) : "";
  return ProductHelperExt.findProduct(page, productId);
  
 }
}
