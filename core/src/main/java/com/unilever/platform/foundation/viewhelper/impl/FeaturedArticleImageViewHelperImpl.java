/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.core.dto.SearchDTO;
import com.unilever.platform.aem.foundation.core.service.SearchService;
import com.unilever.platform.foundation.components.SearchResultFeaturedComparator;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.constants.CommonConstants;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.Article;
import com.unilever.platform.foundation.viewhelper.constants.MultipleRelatedProductsConstants;

/**
 * <p>
 * Responsible for reading author input for featured article component and returns a featured article.It can be tag driven or manual entry.
 * 
 * </p>
 */
@Component(description = "Featured Article Image View Helper Impl", immediate = true, metatype = true, label = "Featured Article Image View Helper")
@Service(value = { FeaturedArticleImageViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.FEATURED_ARTICLE, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class FeaturedArticleImageViewHelperImpl extends MultipleRelatedArticlesBaseViewHelperImpl {
 
 /** Constant for retrieving featured article type. */
 private static final String FEATURED_ARTICLE_TYPE = "featuredArticleType";
 
 /** Constant for retrieving featured article page. */
 private static final String FEATURED_ARTICLE_PAGE = "articlePage";
 
 /**
  * Constant for setting featured article type auto.
  */
 
 private static final String AUTO = "Auto";
 
 /**
  * Constant for retrieving anchor link navigation label.
  */
 private static final String ANCHOR_MAP = "anchorLinkNavigation";
 
 /**
  * Constant for retrieving cta label.
  */
 private static final String TITLE = "title";
 
 /**
  * Constant for setting article label.
  */
 
 private static final String ARTICLE = "article";
 
 /**
  * Constant for setting content type label.
  */
 
 private static final String CONTENT_TYPE = "contentType";
 
 /**
  * Constant for setting content type value label.
  */
 
 private static final String ARTICLE_TYPE = "Article";
 
 /**
  * Constant for saving the featured article type(auto or manual) in json
  */
 private static final String EDITOR_PICK = "editorPick";
 
 /**
  * Constant for retrieving i18 featured article content type.
  */
 
 private static final String FEATURED_ARTICLE_I18N = "featuredArticle.contentType";
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The search service. */
 @Reference
 SearchService searchService;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(FeaturedArticleImageViewHelperImpl.class);
 
 /**
  * The method processData overrides method is responsible validating author input and returning a map Map can be accessed in JSP in consistent
  * way.This method also validates the link url provided by author.
  * 
  * @param content
  *         , - {@link Map} of all user input received as input <code>ViewHelper.onGetData</code> method
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return {@link Map} holding the data
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.info("Featured article View Helper #processData called for processing fixed list.");
  Map<String, Object> featuredArticleProperties = getProperties(content);
  Map<String, Object> featuredArticleFinalMap = new LinkedHashMap<String, Object>();
  Page currentPage = getCurrentPage(resources);
  String gloablConfigCatName = MapUtils.getString(featuredArticleProperties, UnileverConstants.GLOBAL_CONFIGURATION_CATEGORY_NAME);
  LOGGER.debug("Global Configuration category for FeaturedArticleImageViewHelperImpl = " + gloablConfigCatName);
  Map<String, String> configMap = ComponentUtil.getConfigMap(currentPage, gloablConfigCatName);
  // getting base content
  String featuredArticleType = MapUtils.getString(featuredArticleProperties, FEATURED_ARTICLE_TYPE);
  if (StringUtils.isNotEmpty(featuredArticleType)) {
   if (AUTO.equals(featuredArticleType)) {
    List<SearchDTO> articlesResultList = getArticlesPagePathList(configMap, featuredArticleProperties, resources);
    String buttonText = MapUtils.getString(featuredArticleProperties, TITLE);
    List<String> articlesPagePathList = getSortedListBasedOnComparator(articlesResultList, configMap, currentPage, resources,
      featuredArticleProperties);
    List<Map<String, Object>> articleListData = getArticleListData(articlesPagePathList, currentPage, resources);
    LOGGER.debug("created list for featured article,Number of items in this list is " + articleListData.size());
    Map<String, String> anchorMap = ComponentUtil.getAnchorLinkMap(featuredArticleProperties);
    LOGGER.debug("created map for anchor link featured article and size of anchor map is " + anchorMap.size());
    Map<String, Object> data = new LinkedHashMap<String, Object>();
    data.put(ANCHOR_MAP, anchorMap);
    data.put("buttonText", buttonText);
    if (articleListData != null && !articleListData.isEmpty()) {
     String articlePagePath = articleListData.get(0).get(UnileverConstants.URL).toString();
     int startIndex = StringUtils.indexOf(articlePagePath, UnileverConstants.CONTENT);
     int endIndex = StringUtils.indexOf(articlePagePath, CommonConstants.PAGE_EXTENSION);
     articlePagePath = StringUtils.substring(articlePagePath, startIndex, endIndex);
     Page articlePage = getPageManager(resources).getContainingPage(articlePagePath);
     if (articlePage != null) {
      articleType(data, articlePage, resources);
     }
     data.put(ARTICLE, articleListData.get(0));
    } else {
     data.put(ARTICLE, articleListData.toArray());
    }
    data.put(EDITOR_PICK, false);
    featuredArticleFinalMap.put(getJsonNameSpace(content), data);
   } else {
    featuredArticleFinalMap = getArticleManual(featuredArticleProperties, resources, content);
   }
  } else {
   List<Map<String, Object>> blankFeaturedArticletListFinal = new ArrayList<Map<String, Object>>();
   featuredArticleFinalMap.put(getJsonNameSpace(content), blankFeaturedArticletListFinal.toArray());
  }
  return featuredArticleFinalMap;
 }
 
 /**
  * Article type.
  * 
  * @param slingRequest
  *         the sling request
  * @param data
  *         the data
  * @param articlePage
  *         the article page
  */
 private void articleType(Map<String, Object> data, Page articlePage, Map<String, Object> resources) {
  Map<String, Object> articlePageProperties = articlePage.getProperties();
  String contentType = MapUtils.getString(articlePageProperties, CONTENT_TYPE);
  LOGGER.info("article page content type for featured article is " + contentType);
  I18n i18n = getI18n(resources);
  String featuredArticleType = i18n.get(FEATURED_ARTICLE_I18N);
  LOGGER.info("article type for featured article from i18n is " + featuredArticleType);
  String articleType = i18n.getVar(CONTENT_TYPE.toLowerCase() + CommonConstants.DOT_CHAR + contentType);
  if ((ARTICLE_TYPE).equalsIgnoreCase(contentType) && StringUtils.isNotBlank(featuredArticleType)) {
   data.put(UnileverConstants.LABEL, featuredArticleType);
  } else if ((ARTICLE_TYPE).equalsIgnoreCase(contentType)
    && StringUtils.equalsIgnoreCase(articleType, CONTENT_TYPE.toLowerCase() + CommonConstants.DOT_CHAR + contentType)) {
   data.put(UnileverConstants.LABEL, contentType.toLowerCase());
  } else if ((ARTICLE_TYPE).equalsIgnoreCase(contentType)) {
   data.put(UnileverConstants.LABEL, articleType.toLowerCase());
  }
 }
 
 /**
  * this method used to get article page attributes based on user entry.
  * 
  * @param jsonNameSpace
  *         the json name space
  * @param featuredArticleProperties
  *         the featured article properties
  * @param currentPage
  *         the current page
  * @param pageManager
  *         the page manager
  * @param slingRequest
  *         the sling request
  * @return a list of map
  */
 private Map<String, Object> getArticleManual(Map<String, Object> featuredArticleProperties, Map<String, Object> resources,
   Map<String, Object> content) {
  String articlepage = MapUtils.getString(featuredArticleProperties, FEATURED_ARTICLE_PAGE);
  Page articleManualPage = getPageManager(resources).getContainingPage(articlepage);
  String ctaLabelManual = MapUtils.getString(featuredArticleProperties, TITLE);
  
  Map<String, Object> featuredArticleFinalMap = new LinkedHashMap<String, Object>();
  Map<String, Object> blankArticleMap = new LinkedHashMap<String, Object>();
  if (articleManualPage != null) {
   Article article = new Article();
   article.setData(articleManualPage, getSlingRequest(resources));
   Map<String, Object> articleManualMap = article.convertToMap();
   LOGGER.debug("created map for featured article manual page ,Size of the Map is " + articleManualMap.size());
   Map<String, String> anchorMap = ComponentUtil.getAnchorLinkMap(featuredArticleProperties);
   LOGGER.debug("created map for anchor link featured article and size of anchor map is " + anchorMap.size());
   featuredArticleFinalMap.put(ANCHOR_MAP, anchorMap);
   featuredArticleFinalMap.put("buttonText", ctaLabelManual);
   articleType(featuredArticleFinalMap, articleManualPage, resources);
   if (MapUtils.isNotEmpty(articleManualMap)) {
    featuredArticleFinalMap.put(ARTICLE, articleManualMap);
   } else {
    featuredArticleFinalMap.put(ARTICLE, blankArticleMap);
   }
  } else {
   featuredArticleFinalMap.put(ARTICLE, blankArticleMap);
  }
  featuredArticleFinalMap.put(EDITOR_PICK, true);
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  data.put(getJsonNameSpace(content), featuredArticleFinalMap);
  return data;
 }
 
 /**
  * Gets the sorted final list based on comparator.
  * 
  * @param articlesResultList
  *         the articles result list
  * @param configMap
  *         the config map
  * @param currentPage
  *         the current page
  * @return the sorted final list based on comparator
  */
 protected List<String> getSortedListBasedOnComparator(List<SearchDTO> articlesResultList, Map<String, String> configMap, Page currentPage,
   Map<String, Object> resources, Map<String, Object> properties) {
  int max = CommonConstants.THREE;
  try {
   max = StringUtils.isBlank(configMap.get(UnileverConstants.MAX)) ? CommonConstants.THREE : Integer.parseInt(configMap.get(UnileverConstants.MAX));
  } catch (NumberFormatException nfe) {
   LOGGER.error("Maximum featured article for page list must be number");
   LOGGER.error(ExceptionUtils.getStackTrace(nfe));
  }
  String[] include = configMap.get(UnileverConstants.INCLUDE_LIST) != null ? configMap.get(UnileverConstants.INCLUDE_LIST).split(
    CommonConstants.COMMA_CHAR) : new String[] {};
  
  List<String> includeTagIdList = getIncludeTagIdList(currentPage, include);
  LOGGER.debug("Maximum featured articles for page list= " + max);
  String orderbyTagId = configMap.get(MultipleRelatedProductsConstants.ORDER_BY) != null ? configMap.get(MultipleRelatedProductsConstants.ORDER_BY)
    : StringUtils.EMPTY;
  String[] featuredTagFromConfig = StringUtils.isNotBlank(orderbyTagId) ? orderbyTagId.split(CommonConstants.COMMA_CHAR) : new String[] {};
  String includeSegmentRulesProp = MapUtils.getString(properties, "includeSegmentRules", StringUtils.EMPTY);
  boolean includeSegmentRules = ("true").equalsIgnoreCase(includeSegmentRulesProp) ? true : false;
  featuredTagFromConfig = ComponentUtil.getUpdatedIncludedArrayFromResources(resources, featuredTagFromConfig, includeSegmentRules);
  long startTime = System.currentTimeMillis();
  
  Collections.sort(articlesResultList, new SearchResultFeaturedComparator(includeTagIdList, featuredTagFromConfig, true));
  
  long stopTime = System.currentTimeMillis();
  
  LOGGER.info("Time elapsed in soring the result of Featured article = " + (stopTime - startTime));
  List<SearchDTO> articlesResultListFinal = articlesResultList.size() > max ? articlesResultList.subList(0, max) : articlesResultList;
  LOGGER.debug("List of article pages created for featured article after sorting and size of the list is " + articlesResultListFinal.size());
  return getPathList(articlesResultListFinal);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.impl. FeaturedArticleImageViewHelperImpl#getSearchService()
  */
 protected SearchService getSearchService() {
  
  return this.searchService;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.impl. FeaturedArticleImageViewHelperImpl#getConfigService()
  */
 protected ConfigurationService getConfigService() {
  
  return this.configurationService;
 }
 
}
