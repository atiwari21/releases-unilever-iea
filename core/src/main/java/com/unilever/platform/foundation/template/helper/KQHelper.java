/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.template.helper;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUse;
import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfiguration;

/**
 * The Class kritiqueHelper.
 */
public class KQHelper extends WCMUse {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(KQHelper.class);
 
 /** The Constant KRTTIQUE_KEY. */
 private static final String KRTTIQUE_KEY = "kritique";
 
 /** The Constant ENABLED. */
 private static final String ENABLED = "enabled";
 
 /** The Constant KQ_WIDGET_ID. */
 private static final String KQ_WIDGET_ID = "id";
 
 /** The Constant KQ_WIDGET_SERVICE_URI. */
 private static final String KQ_WIDGET_SERVICE_URI = "kritiqueServiceURI";
 
 /** The Constant KQ_WIDGET_BRAND_ID. */
 private static final String KQ_WIDGET_BRAND_ID = "brandId";
 
 /** The Constant KQ_WIDGET_LOCALE_ID. */
 private static final String KQ_WIDGET_LOCALE_ID = "localeId";
 
 /** The Constant KQ_WIDGET_API_KEY. */
 private static final String KQ_WIDGET_API_KEY = "apiKey";
 
 /** The enabled. */
 private boolean enabled;
 
 /** The id. */
 private String id;
 
 /** The service uri. */
 private String serviceURI;
 
 /** The brand id. */
 private String brandId;
 
 /** The locale id. */
 private String localeId;
 
 /** The api key. */
 private String apiKey;
 
 /** The kq map. */
 private Map<String, String> kqMap;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.adobe.cq.sightly.WCMUse#activate()
  */
 @Override
 public void activate() {
  
  Page currentPage = getCurrentPage();
  SlingScriptHelper sling = getSlingScriptHelper();
  
  enabled = isEnabled(currentPage, sling);
  
  if (enabled) {
   id = getWidgetId(currentPage, sling);
   serviceURI = getWidgetServiceURI(sling);
   brandId = getWidgetBrandId(currentPage, sling);
   localeId = getWidgetLocaleId(currentPage, sling);
   apiKey = getApiKey(currentPage, sling);
   kqMap = getKQMap(currentPage, sling);
  }
 }
 
 /**
  * Checks if is enabled.
  * 
  * @return true, if is enabled
  */
 public boolean isEnabled() {
  return enabled;
 }
 
 /**
  * Gets the id.
  * 
  * @return the id
  */
 public String getId() {
  return id;
 }
 
 /**
  * Gets the service uri.
  * 
  * @return the service uri
  */
 public String getServiceURI() {
  return serviceURI;
 }
 
 /**
  * Gets the brand id.
  * 
  * @return the brand id
  */
 public String getBrandId() {
  return brandId;
 }
 
 /**
  * Gets the locale id.
  * 
  * @return the locale id
  */
 public String getLocaleId() {
  return localeId;
 }
 
 /**
  * Gets the api key.
  * 
  * @return the api key
  */
 public String getApiKey() {
  return apiKey;
 }
 
 /**
  * Gets the KQ map.
  * 
  * @return the KQ map
  */
 public Map<String, String> getKQMap() {
  return kqMap;
 }
 
 /**
  * Checks if is enabled.
  * 
  * @param page
  *         the page
  * @param sling
  *         the sling
  * @return true, if is enabled
  */
 private static boolean isEnabled(Page page, SlingScriptHelper sling) {
  
  Map<String, String> categoryMap = null;
  boolean isEnabled = false;
  String enabledValue = null;
  try {
   ConfigurationService configurationService = sling.getService(ConfigurationService.class);
   categoryMap = configurationService.getCategoryConfiguration(page, KRTTIQUE_KEY);
   if ((categoryMap != null) && (categoryMap.containsKey(ENABLED))) {
    enabledValue = categoryMap.get(ENABLED);
    if ((enabledValue != null) && ("true".equals(enabledValue))) {
     isEnabled = true;
    }
   }
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Error in fetching rating & review category for page : " + page.getPath(), e);
  }
  return isEnabled;
 }
 
 /**
  * Gets the widget service uri.
  * 
  * @param page
  *         the page
  * @param sling
  *         the sling
  * @return the widget service uri
  */
 private static String getWidgetServiceURI(SlingScriptHelper sling) {
  
  return sling.getService(GlobalConfiguration.class).getConfigValue(KQ_WIDGET_SERVICE_URI);
 }
 
 /**
  * Gets the widget brand id.
  * 
  * @param page
  *         the page
  * @param sling
  *         the sling
  * @return the widget brand id
  */
 private static String getWidgetBrandId(Page page, SlingScriptHelper sling) {
  
  String widgetBrandId = StringUtils.EMPTY;
  
  Map<String, String> categoryMap = getKQMap(page, sling);
  if ((categoryMap != null) && (categoryMap.containsKey(KQ_WIDGET_BRAND_ID))) {
   widgetBrandId = categoryMap.get(KQ_WIDGET_BRAND_ID);
   if (widgetBrandId != null) {
    widgetBrandId = widgetBrandId.toLowerCase();
   }
  }
  
  return widgetBrandId;
 }
 
 /**
  * Gets the widget id.
  * 
  * @param page
  *         the page
  * @param sling
  *         the sling
  * @return the widget id
  */
 private static String getWidgetId(Page page, SlingScriptHelper sling) {
  
  String widgetId = StringUtils.EMPTY;
  
  Map<String, String> categoryMap = getKQMap(page, sling);
  if ((categoryMap != null) && (categoryMap.containsKey(KQ_WIDGET_ID))) {
   widgetId = categoryMap.get(KQ_WIDGET_ID);
   if (widgetId != null) {
    widgetId = widgetId.toLowerCase();
   }
  }
  
  return widgetId;
 }
 
 /**
  * Gets the widget locale id.
  * 
  * @param page
  *         the page
  * @param sling
  *         the sling
  * @return the widget locale id
  */
 private static String getWidgetLocaleId(Page page, SlingScriptHelper sling) {
  
  String localeId = StringUtils.EMPTY;
  
  Map<String, String> categoryMap = getKQMap(page, sling);
  if ((categoryMap != null) && (categoryMap.containsKey(KQ_WIDGET_LOCALE_ID))) {
   localeId = categoryMap.get(KQ_WIDGET_LOCALE_ID);
   if (localeId != null) {
    localeId = localeId.toLowerCase();
   }
  }
  
  return localeId;
 }
 
 /**
  * Gets the api key.
  * 
  * @param page
  *         the page
  * @param sling
  *         the sling
  * @return the api key
  */
 private static String getApiKey(Page page, SlingScriptHelper sling) {
  
  String apiKey = StringUtils.EMPTY;
  
  Map<String, String> categoryMap = getKQMap(page, sling);
  if ((categoryMap != null) && (categoryMap.containsKey(KQ_WIDGET_API_KEY))) {
   apiKey = categoryMap.get(KQ_WIDGET_API_KEY);
   if (apiKey != null) {
    apiKey = apiKey.toLowerCase();
   }
  }
  
  return apiKey;
 }
 
 /**
  * Gets the KQ map.
  * 
  * @param page
  *         the page
  * @param sling
  *         the sling
  * @return the KQ map
  */
 public static Map<String, String> getKQMap(Page page, SlingScriptHelper sling) {
  
  Map<String, String> categoryMap = null;
  
  try {
   ConfigurationService configurationService = sling.getService(ConfigurationService.class);
   categoryMap = configurationService.getCategoryConfiguration(page, KRTTIQUE_KEY);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Error in fetching KQ map ", e);
  }
  
  return categoryMap;
 }
}
