/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.xss.XSSFilter;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfiguration;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.aem.foundation.core.service.SearchRetrivalService;
import com.unilever.platform.aem.foundation.utils.AdaptiveUtil;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * <p>
 * Responsible for reading author input if provided and i18n keys for generating the json for basic blank search page.
 * </p>
 */
@Component(description = "SearchResultsViewHelperImpl", immediate = true, metatype = true, label = "SearchResultsViewHelperImpl")
@Service(value = { SearchResultsViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.SEARCH_RESULTS, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class SearchResultsViewHelperImpl extends BaseViewHelper {
 
 private static final String SEARCH_INPUT_CATEGORY = "searchInput";
 
 private static final int TWO = 2;
 
 private static final String PAGE_NUM_EQUALS = "PageNum=";
 
 private static final String VALUE = "value";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(SearchResultsViewHelperImpl.class);
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 @Reference
 SearchRetrivalService solrSearchResultsRetrieval;
 
 @Reference
 XSSFilter filter;
 
 /** The Constant RESULTS_LABEL_MAP. */
 private static final String RESULTS_LABEL_MAP = "ResultsLabel";
 
 /** The Constant NO_RESULTS_LABEL_MAP. */
 private static final String NO_RESULTS_LABEL_MAP = "NoResultsLabel";
 
 /** The Constant SUGGESTIONS_LABEL_MAP . */
 private static final String SUGGESTIONS_LABEL_MAP = "search";
 
 /** The Constant DROPDOWN_LABEL_MAP. */
 private static final String DROPDOWN_LABEL_MAP = "dropDownLabel";
 /** The Constant FILTER_LABEL_MAP. */
 private static final String FILTER_LABEL_MAP = "filterLabel";
 /** The Constant QUERY_PARAM_MAP. */
 private static final String RETRIVAL_QUERY_PARAM_MAP = "RetrivalQueryParams";
 
 /** The Constant RETRIVAL_SERVLET. */
 private static final String RETRIVAL_SERVLET = "requestServlet";
 
 private static final String EQUALS = "=";
 
 /** The Constant SUGGESTION_SERVLET. */
 private static final String SUGGESTION_SERVLET = "requestServlet";
 
 private static final String SEARCH_LABEL = "label";
 
 private static final String PAGE_NUM = "PageNum";
 
 private static final String PLACEHOLDER = "placeHolder";
 
 private static final String RESULTS = "actualResults";
 
 /** The Constant RESULTS_PER_PAGE. */
 private static final String RESULTS_PER_PAGE = "resultPerPage";
 public static final String RESULTPERPAGE = "resultPerPage";
 
 private static final String RESULT_PER_PAGE_DEFAULT_COUNT = "10";
 
 public static final String COUNTRYCODE = "countryCode";
 private static final String QUERY_STRING = "queryString";
 private static final String QUERY_PARAM = "q";
 private static final String HEADLINE = "headline";
 private static final String QUERY_FLAG = "queryFlag";
 private static final String RATING_REVIEWS = "ratingReviews";
 private static final String BAZAAR_VOICE = "bazaarvoice";
 private static final String KRITIQUE = "kritique";
 private static final String SERVICE_PROVIDER_NAME = "serviceProviderName";
 
 /**
  * The method processData overrides method is responsible validating author input and returning a map
  * 
  * @param content
  *         {@link Map} of all user input received as input
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return Map holding the result data.
  */
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  LOGGER.info("inside SearchResultsViewHelperImpl view helper");
  Map<String, Object> data = new HashMap<String, Object>();
  SlingHttpServletRequest slingRequest = getSlingRequest(resources);
  I18n i18n = getI18n(resources);
  Page currentPage = getCurrentPage(resources);
  
  Map<String, Object> searchResultsMap = new HashMap<String, Object>();
  Map<String, Object> requestedData = getRetrivalQueryParamsMap(resources);
  Map<String, String> searchConfig = new HashMap<String, String>();
  Map<String, String> productNewlaunch = new HashMap<String, String>();
  String jsonNameSpace = MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE);
  String resultPerPage = RESULT_PER_PAGE_DEFAULT_COUNT;
  Map<String, String> countryConfig = new HashMap<String, String>();
  
  String customizableTag = StringUtils.EMPTY;
  try {
   customizableTag = configurationService.getConfigValue(currentPage, UnileverConstants.EGIFTING_CONFIG_CAT, "customizableTag");
  } catch (ConfigurationNotFoundException e) {
   customizableTag = ProductConstants.FALSE;
   LOGGER.error("could not find customizableTag key in egifting category", e);
  }
  
  try {
   searchConfig = configurationService.getCategoryConfiguration(currentPage, "searchConfiguration");
   productNewlaunch = configurationService.getCategoryConfiguration(currentPage, "product");
   countryConfig = configurationService.getCategoryConfiguration(currentPage, "countryConfiguration");
   
   searchResultsMap.put(RESULTS_PER_PAGE, searchConfig.get(RESULTPERPAGE));
   searchResultsMap.put("customizableTag", customizableTag);
   searchResultsMap.put("personalizableLabel", i18n.get(ProductConstants.PERSONALIZABLE));
   searchResultsMap.put(RESULTS_LABEL_MAP, getResultsLabelMap(i18n, productNewlaunch));
   searchResultsMap.put(NO_RESULTS_LABEL_MAP, getNoResultsMap(i18n));
   searchResultsMap.put(DROPDOWN_LABEL_MAP, getDropDownLabels(i18n));
   searchResultsMap.put(FILTER_LABEL_MAP, getFilterMap(i18n));
   searchResultsMap.put(ProductConstants.REVIEW_MAP, getReviewMapProperties(currentPage, searchConfig));
   searchResultsMap.put(RETRIVAL_QUERY_PARAM_MAP, requestedData);
   searchResultsMap.put(SUGGESTIONS_LABEL_MAP, getAutoSuggestionLabelMap(resources, countryConfig.get("code")));
   
   if (AdaptiveUtil.isAdaptive(slingRequest)) {
    searchResultsMap.put(RESULTS, getSearchResults(slingRequest, resultPerPage));
    searchResultsMap.put(QUERY_STRING, filter.filter(slingRequest.getParameter(QUERY_PARAM)));
    searchResultsMap.put(HEADLINE, i18n.get("search.headingtext"));
    searchResultsMap.put(QUERY_FLAG, slingRequest.getParameter(QUERY_PARAM) == null ? false : true);
   }
   data.put(jsonNameSpace, searchResultsMap);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Exception in READING CONFIGURATIONS  .............." + ExceptionUtils.getStackTrace(e));
  }
  return data;
 }
 
 private Map<String, Object> getSearchResults(SlingHttpServletRequest request, String resultPerPage) {
  
  Map<String, Object> results = new HashMap<String, Object>();
  
  try {
   results = solrSearchResultsRetrieval.getSearchResult(request, true);
   if ((results != null) && (!results.isEmpty()) && (results.containsKey("totalResults"))) {
    int totalResults = Integer.parseInt(String.valueOf(results.get("totalResults")));
    if (totalResults > 0) {
     int noPages = totalResults / Integer.parseInt(resultPerPage);
     int remainder = totalResults % Integer.parseInt(resultPerPage);
     if (remainder > 0) {
      noPages++;
     }
     
     int qStringPageNo = Integer.parseInt(request.getParameter(PAGE_NUM) == null ? "0" : request.getParameter(PAGE_NUM));
     String qString = getQueryStringUsingParameters(request);
     
     results.put("extremePageLinks", getExtremePageLinksMap(request, qString, noPages, qStringPageNo));
     results.put("pagecount", noPages);
     results.put("paginationLinks", getPaginationList(request, qString, noPages, qStringPageNo).toArray());
    }
   }
  } catch (Exception e) {
   LOGGER.error("Exception in getting Search Result Map" + ExceptionUtils.getStackTrace(e));
  }
  
  return results;
 }
 
 private List<Map<String, Object>> getPaginationList(SlingHttpServletRequest request, String queryString, int noOfPages, int pageNumber) {
  
  List<Map<String, Object>> paginationLinks = new ArrayList<Map<String, Object>>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  int queryStringPageNo = pageNumber;
  for (int i = 1; i <= noOfPages; i++) {
   String url = request.getRequestURI() + queryString + PAGE_NUM + EQUALS + String.valueOf(i);
   if (queryStringPageNo == 0) {
    paginationLinks.add(getPageURLMap(i, url, true));
    queryStringPageNo++;
   } else if (queryStringPageNo == i) {
    paginationLinks.add(getPageURLMap(i, url, true));
   } else {
    paginationLinks.add(getPageURLMap(i, url, false));
   }
  }
  
  return paginationLinks;
 }
 
 private String getQueryStringUsingParameters(SlingHttpServletRequest request) {
  
  String qString = "?";
  Map<String, String> newMap = new HashMap<String, String>();
  Map paramMap = request.getParameterMap();
  
  for (Object key : paramMap.keySet()) {
   Object[] row = (Object[]) paramMap.get(key);
   newMap.put((String) key, (String) row[0]);
  }
  
  if (newMap.containsKey(PAGE_NUM)) {
   newMap.remove(PAGE_NUM);
  }
  
  for (String key : newMap.keySet()) {
   qString = qString + key + "=" + newMap.get(key) + "&";
  }
  return qString;
 }
 
 private Map<String, Object> getExtremePageLinksMap(SlingHttpServletRequest request, String queryString, int noOfPages, int queryStringPageNo) {
  Map<String, Object> extremePageMap = new HashMap<String, Object>();
  
  String firstPageUrl = request.getRequestURI() + queryString + "PageNum=1";
  String lastPageUrl = request.getRequestURI() + queryString + PAGE_NUM_EQUALS + noOfPages;
  String nextPageUrl = StringUtils.EMPTY;
  String previousPageUrl = StringUtils.EMPTY;
  
  if ((queryStringPageNo != 0) && (queryStringPageNo <= noOfPages)) {
   if (queryStringPageNo == 1) {
    nextPageUrl = request.getRequestURI() + queryString + PAGE_NUM_EQUALS + (queryStringPageNo + 1);
    previousPageUrl = request.getRequestURI() + queryString + PAGE_NUM_EQUALS + queryStringPageNo;
   } else if (queryStringPageNo == noOfPages) {
    nextPageUrl = request.getRequestURI() + queryString + PAGE_NUM_EQUALS + noOfPages;
    previousPageUrl = request.getRequestURI() + queryString + PAGE_NUM_EQUALS + (queryStringPageNo - 1);
   } else {
    nextPageUrl = request.getRequestURI() + queryString + PAGE_NUM_EQUALS + (queryStringPageNo + 1);
    previousPageUrl = request.getRequestURI() + queryString + PAGE_NUM_EQUALS + (queryStringPageNo - 1);
   }
  } else if (queryStringPageNo == 0) {
   if (noOfPages > 1) {
    nextPageUrl = request.getRequestURI() + queryString + PAGE_NUM_EQUALS + (queryStringPageNo + TWO);
    previousPageUrl = request.getRequestURI() + queryString + PAGE_NUM_EQUALS + (queryStringPageNo + 1);
   } else {
    nextPageUrl = request.getRequestURI() + queryString + PAGE_NUM_EQUALS + (queryStringPageNo + 1);
    previousPageUrl = request.getRequestURI() + queryString + PAGE_NUM_EQUALS + (queryStringPageNo + 1);
   }
  }
  
  extremePageMap.put("firstPage", getExtremePageURLMap(firstPageUrl));
  extremePageMap.put("lastPage", getExtremePageURLMap(lastPageUrl));
  extremePageMap.put("nextPage", getExtremePageURLMap(nextPageUrl));
  extremePageMap.put("previousPage", getExtremePageURLMap(previousPageUrl));
  
  return extremePageMap;
 }
 
 private Map<String, Object> getExtremePageURLMap(String url) {
  
  Map<String, Object> pageUrlMap = new HashMap<String, Object>();
  
  pageUrlMap.put("url", url);
  pageUrlMap.put("isenabled", true);
  
  return pageUrlMap;
 }
 
 private Map<String, Object> getPageURLMap(int pageNo, String url, Boolean flag) {
  
  Map<String, Object> pageUrlMap = new HashMap<String, Object>();
  
  pageUrlMap.put(SEARCH_LABEL, pageNo);
  pageUrlMap.put("url", url);
  pageUrlMap.put("iscurrentPage", flag);
  
  return pageUrlMap;
 }
 
 private Map<String, Object> getRetrivalQueryParamsMap(Map<String, Object> resources) {
  Map<String, Object> requestedData = new HashMap<String, Object>();
  Page currentPage = getCurrentPage(resources);
  String retrivalHandlerUrl = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, SEARCH_INPUT_CATEGORY,
    RETRIVAL_SERVLET);
  if (StringUtils.isNotEmpty(retrivalHandlerUrl)) {
   requestedData.put(RETRIVAL_SERVLET, retrivalHandlerUrl);
  } else {
   requestedData.put(RETRIVAL_SERVLET, "/bin/search/retrivalhandler");
  }
  
  return requestedData;
 }
 
 private Map<String, Object> getAutoSuggestionLabelMap(Map<String, Object> resources, String country) {
  Map<String, Object> map = new HashMap<String, Object>();
  Map<String, Object> suggestionLabel = new HashMap<String, Object>();
  I18n i18n = getI18n(resources);
  Page currentPage = getCurrentPage(resources);
  map.put(PLACEHOLDER, i18n.get("search.type.something.here"));
  map.put(SEARCH_LABEL, i18n.get("search.label"));
  String suggestionHandlerUrl = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, SEARCH_INPUT_CATEGORY,
    "searchSuggestionHandler");
  if (StringUtils.isNotEmpty(suggestionHandlerUrl)) {
   map.put(SUGGESTION_SERVLET, suggestionHandlerUrl);
  } else {
   map.put(SUGGESTION_SERVLET, "/bin/search/suggestionhandler");
  }
  map.put(COUNTRYCODE, country);
  suggestionLabel.put(SEARCH_INPUT_CATEGORY, map);
  return suggestionLabel;
  
 }
 
 private Map<String, Object> getNoResultsMap(I18n i18n) {
  Map<String, Object> map = new HashMap<String, Object>();
  map.put(SEARCH_LABEL, i18n.get("searchListing.didYouMean"));
  map.put("headline", i18n.get("searchListing.noresult.suggestion"));
  map.put("description", i18n.get("searchListing.noresult.headline"));
  return map;
  
 }
 
 private Map<String, Object> getResultsLabelMap(I18n i18n, Map<String, String> productNewlaunch) {
  Map<String, Object> resultMap = new HashMap<String, Object>();
  
  resultMap.put("loadMoreLabel", i18n.get("searchListing.label.seeAll"));
  resultMap.put("resultsForCopy", i18n.get("searchListing.label.resultfor"));
  resultMap.put("showingLabelCopy", i18n.get("searchListing.label.showing"));
  resultMap.put("newLaunch", productNewlaunch.get("isNewProductLaunchDateLimit"));
  return resultMap;
  
 }
 
 private Map<String, Object> getDropDownLabels(I18n i18n) {
  Map<String, Object> filterMap = new HashMap<String, Object>();
  filterMap.put("inEverything", i18n.get("searchListing.everythingLabel"));
  filterMap.put("inProducts", i18n.get("searchListing.productLabel"));
  filterMap.put("inArticles", i18n.get("searchListing.articleLabel"));
  return filterMap;
  
 }
 
 private Map<String, Object> getFilterMap(I18n i18n) {
  List<Map<String, String>> listing = new ArrayList<Map<String, String>>();
  Map<String, String> filterMap1 = new HashMap<String, String>();
  filterMap1.put("id", "");
  filterMap1.put(VALUE, i18n.get("searchListing.all.filter"));
  
  Map<String, String> filterMap2 = new HashMap<String, String>();
  filterMap2.put("id", "Product");
  filterMap2.put(VALUE, i18n.get("searchListing.products.filter"));
  
  Map<String, String> filterMap3 = new HashMap<String, String>();
  filterMap3.put("id", "Article");
  filterMap3.put(VALUE, i18n.get("searchListing.articles.filter"));
  listing.add(filterMap1);
  listing.add(filterMap2);
  listing.add(filterMap3);
  
  Map<String, Object> map = new HashMap<String, Object>();
  map.put("enabled", "true");
  map.put(SEARCH_LABEL, i18n.get("showing"));
  map.put("listing", listing.toArray());
  return map;
 }
 
 /**
  * Gets the review map properties.
  * 
  * @param page
  *         the page
  * @return the review map properties
  */
 private Map<String, String> getReviewMapProperties(Page page, Map<String, String> searchConfig) {
  Map<String, String> reviewMapProperties = new HashMap<String, String>();
  String serviceProviderNameReview = null;
  String serviceProviderName = StringUtils.EMPTY;
  try {
   Resource resource = page.getContentResource();
   InheritanceValueMap valueMap = new HierarchyNodeInheritanceValueMap(resource);
   if (valueMap != null) {
    serviceProviderName = (String) valueMap.getInherited(RATING_REVIEWS, StringUtils.EMPTY);
   }
   if (StringUtils.isNotBlank(serviceProviderName)) {
    serviceProviderNameReview = serviceProviderName;
   } else {
    serviceProviderNameReview = configurationService.getConfigValue(page, "ratingAndReviews", SERVICE_PROVIDER_NAME);
   }
  } catch (ConfigurationNotFoundException e) {
   LOGGER.debug("could not find serviceProviderName key in serviceProviderNameShopNow category", e);
  }
  try {
   reviewMapProperties = configurationService.getCategoryConfiguration(page, serviceProviderNameReview);
   if (reviewMapProperties != null) {
    GlobalConfiguration globalConfiguration = page.adaptTo(GlobalConfiguration.class);
    String[] excludedConfig = globalConfiguration.getCommonGlobalConfigValue("excludedSEOProperties").split(",");
    for (String excludeKey : excludedConfig) {
     if (reviewMapProperties.containsKey(excludeKey)) {
      reviewMapProperties.remove(excludeKey);
     }
    }
   }
   String enabled = searchConfig.get("reviewEnable");
   reviewMapProperties.put("enabled", StringUtils.isBlank(enabled) ? "true" : enabled);
   if (BAZAAR_VOICE.equalsIgnoreCase(serviceProviderNameReview) || serviceProviderNameReview.startsWith(BAZAAR_VOICE)) {
    reviewMapProperties.put(SERVICE_PROVIDER_NAME, BAZAAR_VOICE);
   } else if (KRITIQUE.equalsIgnoreCase(serviceProviderNameReview) || serviceProviderNameReview.startsWith(KRITIQUE)) {
    reviewMapProperties.put(SERVICE_PROVIDER_NAME, KRITIQUE);
   }
  } catch (ConfigurationNotFoundException e) {
   LOGGER.debug("could not find serviceProviderNameReview category in configuration", e);
  }
  LOGGER.debug("Review Map Properties : " + reviewMapProperties);
  return reviewMapProperties;
 }
 
}
