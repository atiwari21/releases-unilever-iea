/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.constants;

/**
 * The Class GiftConfiguratorConstants.
 */
public class PaymentFormConstants {
 
 /** The Constant GLOBAL_CONFIG_CAT. */
 public static final String GLOBAL_CONFIG_CAT = "paymentForm";
 
 /** The Constant PAYMENT_FORM_CARD_NUMBER_LABEL. */
 public static final String PAYMENT_FORM_CARD_NUMBER_LABEL = "paymentForm.cardNumberLabel";
 
 /** The Constant PAYMENT_FORM_EXPIRY_DATE_LABEL. */
 public static final String PAYMENT_FORM_EXPIRY_DATE_LABEL = "paymentForm.expiryDateLabel";
 
 /** The Constant PAYMENT_FORM_TOOL_TIP. */
 public static final String PAYMENT_FORM_TOOL_TIP = "paymentForm.toolTip";
 
 /** The Constant PAYMENT_FORM_CVV_LABEL. */
 public static final String PAYMENT_FORM_CVV_LABEL = "paymentForm.cvvLabel";
 
 /** The Constant PAYMENT_FORM_PAYPAL_BUTTON_LABEL. */
 public static final String PAYMENT_FORM_PAYPAL_BUTTON_LABEL = "paymentForm.payPalButtonLabel";
 
 /** The Constant PAYMENT_FORM_CARDPAY_BUTTON_LABEL. */
 public static final String PAYMENT_FORM_CARDPAY_BUTTON_LABEL = "paymentForm.cardPayButtonLabel";
 
 /** The Constant PAYMENT_FORM_ERROR_TITLE. */
 public static final String PAYMENT_FORM_ERROR_TITLE = "paymentForm.errorTitle";
 
 /** The Constant PAYMENT_FORM_ERROR_DESCRIPTION. */
 public static final String PAYMENT_FORM_ERROR_DESCRIPTION = "paymentForm.errorDescription";
 
 /** The Constant CARD_NUMBER_LABEL. */
 public static final String CARD_NUMBER_LABEL = "cardNumberLabel";
 
 /** The Constant EXPIRY_DATE_LABEL. */
 public static final String EXPIRY_DATE_LABEL = "expiryDateLabel";
 
 /** The Constant TOOL_TIP. */
 public static final String TOOL_TIP = "toolTip";
 
 /** The Constant CVV_LABEL. */
 public static final String CVV_LABEL = "cvvLabel";
 
 /** The Constant PAYPAL_BUTTON_LABEL. */
 public static final String PAYPAL_BUTTON_LABEL = "payPalButtonLabel";
 
 /** The Constant CARDPAY_BUTTON_LABEL. */
 public static final String CARDPAY_BUTTON_LABEL = "cardPayButtonLabel";
 
 /** The Constant ERRORMSG. */
 public static final String ERRORMSG = "errormsg";
 
 /** The Constant CLIENT_TOKEN_URL. */
 public static final String CLIENT_TOKEN_URL = "clientTokenUrl";
 
 /** The Constant EXPIRY_DATE_YEAR_PLACEHOLDER. */
 public static final String EXPIRY_DATE_YEAR_PLACEHOLDER = "expiryDateYearPlaceholder";
 
 /** The Constant EXPIRY_DATE_MONTH_PLACEHOLDER. */
 public static final String EXPIRY_DATE_MONTH_PLACEHOLDER = "expiryDateMonthPlaceholder";
 
 /** The Constant PAYMENT_FORM_EXPIRY_DATE_YEAR_PLACEHOLDER. */
 public static final String PAYMENT_FORM_EXPIRY_DATE_YEAR_PLACEHOLDER = "paymentForm.expiryDateYearPlaceholder";
 
 /** The Constant PAYMENT_FORM_EXPIRY_DATE_MONTH_PLACEHOLDER. */
 public static final String PAYMENT_FORM_EXPIRY_DATE_MONTH_PLACEHOLDER = "paymentForm.expiryDateMonthPlaceholder";
 
 /** The Constant PAYMENT_FORM_TITLE. */
 public static final String PAYMENT_FORM_TITLE = "paymentForm.title";
 
 /** The Constant FIELDS. */
 public static final String FIELDS = "fields";
 
 /* constructor */
 private PaymentFormConstants() {
  
 }
 
}
