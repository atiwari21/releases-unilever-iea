/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.cache.ComponentViewAndViewHelperCache;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.constants.CommonConstants;
import com.unilever.platform.foundation.template.dto.Image;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * <p>
 * Responsible for reading author input for pr0duct story component and generating a list of storysections setting in pageContext for the view.
 * 
 * </p>
 */
@Component(description = "ProductHomeStoryViewHelperImpl", immediate = true, metatype = true, label = "ProductHomeStoryViewHelperImpl")
@Service(value = { HomeStoryViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.HOME_STORY, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class HomeStoryViewHelperImpl extends BaseViewHelper {
 
 /**
  * Constant for retrieving product home story title.
  */
 private static final String PRODUCT_HOME_STORY_TITLE = "homeStoryTitle";
 /**
  * Constant for retrieving product home story short copy .
  */
 private static final String PRODUCT_SHORT_COPY = "shortCopyText";
 /**
  * Constant for retrieving product home story left image.
  */
 private static final String LEFT_IMAGE = "leftImageStory";
 /**
  * Constant for setting product home story right image.
  */
 private static final String RIGHT_IMAGE = "rightImageStory";
 /**
  * Constant for setting product home story centre image.
  */
 private static final String CENTRE_IMAGE = "centreImageStory";
 
 /** Constant for product home story cta text title. */
 private static final String LABEL_TEXT = "ctaLabelText";
 /**
  * Constant for product home story cta text link.
  */
 private static final String LINK_TEXT = "ctaLinkText";
 /**
  * Constant for setting testimonial label.
  */
 private static final String QUOTE_REFER = "quote";
 
 /** Constant for reference link. */
 private static final String REFERENCE = "referenceText";
 
 /** Constant for quote text. */
 private static final String QUOTE_TEXT = "quoteText";
 
 /** Constant for left image alt text. */
 private static final String ALTLEFT = "altLeft";
 
 /** Constant for right image alt text. */
 private static final String ALT_RIGHT = "altRight";
 
 /** Constant for centre image alt text. */
 private static final String ALT_CENTRE = "altCentre";
 /**
  * Constant for setting check whether the left image is of product.
  */
 private static final String IS_LEFT_IMAGE_PRODUCT = "isLeftImageProduct";
 /**
  * Constant for setting check whether the right image is of product.
  */
 private static final String IS_RIGHT_IMAGE_PRODUCT = "isRightImageProduct";
 /**
  * Constant for setting component type.
  */
 private static final int INT_TWO = 2;
 /**
  * Constant for setting cta action type.
  */
 private static final String IS_PRODUCT_YES = "Yes";
 /**
  * Constant for setting actionButton label.
  */
 private static final String CTA = "cta";
 /**
  * Constant for setting anchor id label.
  */
 private static final String ANCHOR_ID = "id";
 /**
  * Constant for setting short copy description label.
  */
 private static final String SHORT_COPY = "shortCopy";
 /**
  * Constant for setting anchor map label.
  */
 private static final String ANCHOR_MAP = "anchorLinkNavigation";
 /**
  * Constant for setting i18 left image label.
  */
 private static final String PROPERTY_JSON_LEFT_IMAGE = "leftImage";
 /**
  * Constant for setting i18 right image label.
  */
 private static final String PROPERTY_JSON_RIGHT_IMAGE = "rightImage";
 /**
  * Constant for setting i18 background image label.
  */
 private static final String PROPERTY_JSON_CENTRE_IMAGE = "centreImage";
 /**
  * Constant for setting anchor link flag.
  */
 private static final String SHOW_AS_NAVIGATION = "showAsNavigation";
 /**
  * Constant for setting read anchor link label.
  */
 private static final String LABEL = "label";
 /**
  * Constant for setting read stories label.
  */
 private static final String PRODUCT_STORIES = "stories";
 /**
  * Constant for setting quote text label.
  */
 private static final String QUOTE = "text";
 /**
  * Constant for setting random number label.
  */
 private static final String RANDOM_NUMBER = "randomNumber";
 /**
  * Constant for setting image title label.
  */
 private static final String TITLE = "title";
 /**
  * Constant for setting space.
  */
 private static final String SPACE = "\\s";
 
 /**
  * Constant for retrieving open in new window.
  */
 private static final String OPEN_IN_WINDOW = "openInWindow";
 /**
  * Constant for retrieving left image to be adaptive or not.
  */
 private static final String IS_LEFTIMAGE_FEATURE = "leftFeatureFlag";
 /**
  * Constant for retrieving right image to be adaptive or not.
  */
 private static final String IS_RIGHTIMAGE_FEATURE = "rightFeatureFlag";
 /**
  * Constant for retrieving background image to be adaptive or not.
  */
 private static final String IS_CENTREIMAGE_FEATURE = "centreFeatureFlag";
 /**
  * Constant for retrieving feature image flag.
  */
 private static final String FEATURE_FLAG = "featureImage";
 /**
  * Constant for retrieving false flag.
  */
 private static final String STRING_FALSE = "false";
 /**
  * logger object.
  **/
 private static final Logger LOGGER = LoggerFactory.getLogger(HomeStoryViewHelperImpl.class);
 
 /** The component view and view helper cache. */
 @Reference
 ComponentViewAndViewHelperCache componentViewAndViewHelperCache;
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /**
  * The method processData overrides method is responsible validating author input and returning a list of sections containing rich text,text This
  * method converts the multiwidget input (a String Array) into sections of maps so that it can be accessed in JSP in consistent way.This method also
  * validates the link url provided by author.
  * 
  * @param content
  *         , - {@link Map} of all user input received as input <code>ViewHelper.onGetData</code> method
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return {@link Map} holding the data
  */
 @Override
 @SuppressWarnings("unchecked")
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  LOGGER.info("Product story View Helper #processData called for processing fixed list.");
  Map<String, Object> productStoryProperties = (Map<String, Object>) content.get(PROPERTIES);
  Map<String, Object> productStoryFinalMap = new LinkedHashMap<String, Object>();
  ResourceResolver resourceResolver = (ResourceResolver) resources.get(ProductConstants.RESOURCE_RESOLVER);
  SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) resources.get(ProductConstants.REQUEST);
  Resource currentResource = slingRequest.getResource();
  String jsonNameSpace = MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE);
  if (StringUtils.isNotEmpty(jsonNameSpace)) {
   String showAsNavigation = MapUtils.getString(productStoryProperties, SHOW_AS_NAVIGATION);
   String randomNumber = MapUtils.getString(productStoryProperties, RANDOM_NUMBER);
   randomNumber = changeToEmptyString(randomNumber);
   showAsNavigation = changeToEmptyString(showAsNavigation);
   String label = MapUtils.getString(productStoryProperties, LABEL);
   label = changeToEmptyString(label);
   String anchorID = null;
   if (StringUtils.isNotEmpty(label)) {
    anchorID = label.replaceAll(SPACE, "") + randomNumber;
   } else {
    anchorID = "";
   }
   
   Map<String, Object> anchorLinkMap = new LinkedHashMap<String, Object>();
   
   // Anchor link
   
   if (StringUtils.isNotEmpty(showAsNavigation) && showAsNavigation.equals(CommonConstants.BOOLEAN_TRUE)) {
    anchorLinkMap.put(SHOW_AS_NAVIGATION, CommonConstants.BOOLEAN_TRUE);
    anchorLinkMap.put(LABEL, label);
    anchorLinkMap.put(ANCHOR_ID, anchorID);
   } else {
    anchorLinkMap.put(SHOW_AS_NAVIGATION, STRING_FALSE);
    anchorLinkMap.put(LABEL, label);
    anchorLinkMap.put(ANCHOR_ID, anchorID);
   }
   Map<String, Object> productStoryMap = new LinkedHashMap<String, Object>();
   productStoryMap.put(ANCHOR_MAP, anchorLinkMap);
   
   List<Map<String, String>> productStorySectionsList = ComponentUtil.getNestedMultiFieldProperties(currentResource, "sections");
   LOGGER.debug("created homestory sections list,size of the list is " + productStorySectionsList.size());
   List<Map<String, Object>> sectionsListFinals = new ArrayList<Map<String, Object>>();
   List<Map<String, Object>> sectionsListFinal = prepareSectionList(productStorySectionsList, resourceResolver, currentResource, resources);
   sectionsListFinals.addAll(sectionsListFinal);
   LOGGER.debug("Final list of Map for home story created,size of the list is :- " + sectionsListFinal.size());
   productStoryMap.put(PRODUCT_STORIES, sectionsListFinals.toArray());
   productStoryFinalMap.put(jsonNameSpace, productStoryMap);
  } else {
   List<Map<String, Object>> blankProductStoryListFinal = new ArrayList<Map<String, Object>>();
   productStoryFinalMap.put(jsonNameSpace, blankProductStoryListFinal.toArray());
  }
  
  return productStoryFinalMap;
  
 }
 
 /**
  * This method is used to get left image properties like path,alt text for image extension etc.
  * 
  * @param leftImageResource
  *         the left image resource
  * @param page
  *         the page
  * @return the left image properties
  */
 private Map<String, Object> getLeftImageProperties(Map<String, String> leftImageResource, Page page, Map<String, Object> resources) {
  Map<String, Object> leftImageMap = new LinkedHashMap<String, Object>();
  String altLeft = changeToEmptyString(leftImageResource.get(ALTLEFT));
  String isLeftImageProduct = leftImageResource.get(IS_LEFT_IMAGE_PRODUCT);
  String leftImage = leftImageResource.get(LEFT_IMAGE);
  Image leftStoryImage = new Image(leftImage, altLeft, altLeft, page, getSlingRequest(resources));
  leftImageMap = leftStoryImage.convertToMap();
  String[] leftImageArray = leftImage.split(CommonConstants.DOT_SPLITTER);
  if (leftImageArray.length >= INT_TWO && isLeftImageProduct.equals(IS_PRODUCT_YES)) {
   leftImageMap.put(CommonConstants.IS_PRODUCT, CommonConstants.BOOLEAN_TRUE);
  } else {
   leftImageMap.put(CommonConstants.IS_PRODUCT, STRING_FALSE);
  }
  return leftImageMap;
  
 }
 
 /**
  * This method is used to get right image properties like path,alt text for image extension etc.
  * 
  * @param rightImageResource
  *         the right image resource
  * @param page
  *         the page
  * @return the image properties
  */
 
 private Map<String, Object> getRightImageProperties(Map<String, String> rightImageResource, Page page, Map<String, Object> resources) {
  Map<String, Object> rightImageMap = new LinkedHashMap<String, Object>();
  String altRight = changeToEmptyString(rightImageResource.get(ALT_RIGHT));
  String isRightImageProduct = rightImageResource.get(IS_RIGHT_IMAGE_PRODUCT);
  String rightImage = rightImageResource.get(RIGHT_IMAGE);
  Image rightStoryImage = new Image(rightImage, altRight, altRight, page, getSlingRequest(resources));
  rightImageMap = rightStoryImage.convertToMap();
  String[] rightImageArray = rightImage.split(CommonConstants.DOT_SPLITTER);
  if (rightImageArray.length >= INT_TWO && isRightImageProduct.equals(IS_PRODUCT_YES)) {
   rightImageMap.put(CommonConstants.IS_PRODUCT, CommonConstants.BOOLEAN_TRUE);
  } else {
   rightImageMap.put(CommonConstants.IS_PRODUCT, STRING_FALSE);
  }
  return rightImageMap;
  
 }
 
 /**
  * This method is used to get centre image properties like path,alt text for image extension etc.
  * 
  * @param centreImageResource
  *         the centre image resource
  * @param page
  *         the page
  * @return the image properties
  */
 
 private Map<String, Object> getCentreImageProperties(Map<String, String> centreImageResource, Page page, Map<String, Object> leftImageMap,
   Map<String, Object> rightImageMap, Map<String, Object> resources) {
  Map<String, Object> centreImageMap = new LinkedHashMap<String, Object>();
  String leftFeatureAdaptive = centreImageResource.get(IS_LEFTIMAGE_FEATURE);
  String rightFeatureAdaptive = centreImageResource.get(IS_RIGHTIMAGE_FEATURE);
  String centreFeatureAdaptive = centreImageResource.get(IS_CENTREIMAGE_FEATURE);
  String altCentre = changeToEmptyString(centreImageResource.get(ALT_CENTRE));
  String centreImage = centreImageResource.get(CENTRE_IMAGE);
  Image centreStoryImage = new Image(centreImage, altCentre, altCentre, page, getSlingRequest(resources));
  centreImageMap = centreStoryImage.convertToMap();
  if (IS_PRODUCT_YES.equals(leftFeatureAdaptive)) {
   leftImageMap.put(FEATURE_FLAG, true);
   rightImageMap.put(FEATURE_FLAG, false);
   centreImageMap.put(FEATURE_FLAG, false);
  } else if (IS_PRODUCT_YES.equals(rightFeatureAdaptive)) {
   rightImageMap.put(FEATURE_FLAG, true);
   leftImageMap.put(FEATURE_FLAG, false);
   centreImageMap.put(FEATURE_FLAG, false);
  } else if (IS_PRODUCT_YES.equals(centreFeatureAdaptive)) {
   centreImageMap.put(FEATURE_FLAG, true);
   rightImageMap.put(FEATURE_FLAG, false);
   leftImageMap.put(FEATURE_FLAG, false);
  } else if (IS_PRODUCT_YES.equals(centreImageResource.get(IS_LEFT_IMAGE_PRODUCT))) {
   leftImageMap.put(FEATURE_FLAG, true);
   rightImageMap.put(FEATURE_FLAG, false);
   centreImageMap.put(FEATURE_FLAG, false);
  } else if (IS_PRODUCT_YES.equals(centreImageResource.get(IS_RIGHT_IMAGE_PRODUCT))) {
   rightImageMap.put(FEATURE_FLAG, true);
   leftImageMap.put(FEATURE_FLAG, false);
   centreImageMap.put(FEATURE_FLAG, false);
  } else {
   leftImageMap.put(FEATURE_FLAG, true);
   rightImageMap.put(FEATURE_FLAG, false);
   centreImageMap.put(FEATURE_FLAG, false);
  }
  return centreImageMap;
  
 }
 
 /**
  * Change to empty string.
  * 
  * @param value
  *         the value
  * @return the string
  */
 // change null value to empty string
 public String changeToEmptyString(String value) {
  String newValue = value;
  if (StringUtils.isEmpty(newValue)) {
   newValue = "";
  }
  return newValue;
 }
 
 /**
  * This method is used to prepare section list based on the input provided by author.
  * 
  * @param sectionsList
  *         section list
  * @param resourceResolver
  *         the resource resolver
  * @param analyticsCategory
  *         the analytics category
  * @param currentResource
  *         the current resource
  * @return the list of maps
  */
 private List<Map<String, Object>> prepareSectionList(final List<Map<String, String>> sectionsList, ResourceResolver resourceResolver,
   Resource currentResource, Map<String, Object> resources) {
  
  List<Map<String, Object>> finalSectionsList = new ArrayList<Map<String, Object>>();
  Map<String, Object> finalSectionsMap = null;
  for (Map<String, String> sectionMap : sectionsList) {
   
   // getting each field from content map
   String homeStoryTitle = MapUtils.getString(sectionMap, PRODUCT_HOME_STORY_TITLE, StringUtils.EMPTY);
   String shortCopyText = MapUtils.getString(sectionMap, PRODUCT_SHORT_COPY, StringUtils.EMPTY);
   String newWindow = MapUtils.getString(sectionMap, OPEN_IN_WINDOW);
   String openInNewWindow = newWindow != null && "[true]".equals(newWindow) ? "Yes" : "No";
   // getting links and labels for CTA button
   String labelText = MapUtils.getString(sectionMap, LABEL_TEXT);
   String linkText = MapUtils.getString(sectionMap, LINK_TEXT);
   String linktext;
   if (StringUtils.isEmpty(linkText)) {
    linktext = changeToEmptyString(linkText);
   } else {
    linktext = ComponentUtil.getFullURL(resourceResolver, linkText, getSlingRequest(resources));
   }
   
   // creatimg quote text and reference text map
   Map<String, Object> quoteMap = null;
   String quoteText = MapUtils.getString(sectionMap, QUOTE_TEXT, StringUtils.EMPTY);
   String referenceText = MapUtils.getString(sectionMap, REFERENCE, StringUtils.EMPTY);
   
   PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
   Page currentPage = pageManager.getContainingPage(currentResource);
   
   // multifield left image
   Map<String, Object> leftImageMap = getLeftImageProperties(sectionMap, currentPage, resources);
   // multifield right image
   Map<String, Object> rightImageMap = getRightImageProperties(sectionMap, currentPage, resources);
   // multifield centre image
   Map<String, Object> centreImageMap = getCentreImageProperties(sectionMap, currentPage, leftImageMap, rightImageMap, resources);
   
   Map<String, Object> ctaButtonMap = new LinkedHashMap<String, Object>();
   
   // creating individual map for each story and making a list
   if (StringUtils.isNotEmpty(homeStoryTitle)) {
    finalSectionsMap = new LinkedHashMap<String, Object>();
    finalSectionsMap.put(TITLE, homeStoryTitle);
    finalSectionsMap.put(SHORT_COPY, shortCopyText);
    if (MapUtils.isNotEmpty(leftImageMap)) {
     finalSectionsMap.put(PROPERTY_JSON_LEFT_IMAGE, leftImageMap);
    }
    if (MapUtils.isNotEmpty(rightImageMap)) {
     finalSectionsMap.put(PROPERTY_JSON_RIGHT_IMAGE, rightImageMap);
    }
    if (MapUtils.isNotEmpty(centreImageMap)) {
     finalSectionsMap.put(PROPERTY_JSON_CENTRE_IMAGE, centreImageMap);
    }
    
    if (StringUtils.isNotBlank(quoteText)) {
     quoteMap = new LinkedHashMap<String, Object>();
     quoteMap.put(REFERENCE, referenceText);
     quoteMap.put(QUOTE, quoteText);
     finalSectionsMap.put(QUOTE_REFER, quoteMap);
    }
    
    ctaButtonMap.put(ProductConstants.TEXT, labelText);
    ctaButtonMap.put(ProductConstants.URL, linktext);
    ctaButtonMap.put(OPEN_IN_WINDOW, openInNewWindow);
    finalSectionsMap.put(CTA, ctaButtonMap);
    finalSectionsList.add(finalSectionsMap);
    
   }
  }
  
  return finalSectionsList;
 }
 
}
