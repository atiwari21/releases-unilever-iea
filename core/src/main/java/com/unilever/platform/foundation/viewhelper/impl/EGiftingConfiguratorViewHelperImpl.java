/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.commerce.api.CustomProduct;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.components.helper.ProductHelperExt;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * This class prepares the data map for e Gifting Configurator.
 */
@Component(description = "EGiftingConfiguratorViewHelperImpl", immediate = true, metatype = true, label = "e Gifting Configurator ViewHelper")
@Service(value = { EGiftingConfiguratorViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.E_GIGTING_CONFIGURATOR, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class EGiftingConfiguratorViewHelperImpl extends BaseViewHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ProductImagesViewHelperImpl.class);
 private static final String PRODUCT_PAGE = "productPage";
 private static final String PERSONALISE_GIFT_CTA = "label";
 private static final String PERSONALISE_GIFT_URL = "url";
 private static final String PERSONALISED_CTA_MAP = "personalisedCta";
 private static final String PERSONALISE_GIFT_CTA_LABEL = "personalise.this.gift.label";
 
 @Reference
 ConfigurationService configurationService;
 
 @SuppressWarnings("unchecked")
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("e Gifting Configurator View Helper #processData called .");
  ResourceResolver resourceResolver = (ResourceResolver) resources.get(ProductConstants.RESOURCE_RESOLVER);
  Map<String, Object> productImagesProperties = (Map<String, Object>) content.get(PROPERTIES);
  SlingHttpServletRequest slingRequest = getSlingRequest(resources);
  Page currentPage = getCurrentPage(resources);
  Map<String, Object> properties = new LinkedHashMap<String, Object>();
  Map<String, Object> productProperties = new HashMap<String, Object>();
  UNIProduct product = null;
  String jsonNameSapce = getJsonNameSpace(content);
  String productPagePath = MapUtils.getString(productImagesProperties, PRODUCT_PAGE);
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  if (StringUtils.isNotBlank(productPagePath)) {
   product = ProductHelper.getUniProduct(pageManager.getPage(productPagePath));
  } else {
   product = ProductHelper.getUniProduct(currentPage, slingRequest);
  }
  boolean flag = ProductHelperExt.isCustomizableProduct(product, configurationService, currentPage);
  if (product != null) {
   setProductGiftAttributes(product, currentPage, productProperties, flag);
   productProperties.put(ProductConstants.PRICE_ENABLED, ProductHelper.isPriceEnabled(configurationService, currentPage));
  } else {
   LOGGER.info("No Product is available for current page");
  }
  setPersonalisedCta(currentPage, productProperties, product, getI18n(resources), slingRequest);
  setDeliveryDetailAttributes(currentPage, productProperties);
  Map<String, Object> containerTagMap = ProductHelper.getProductContainerTagMap(currentPage, product);
  
  productProperties.put(UnileverConstants.CONTAINER_TAG, containerTagMap);
  
  properties.put(ProductConstants.PRODUCT_GIFT_DETAILS_MAP, productProperties);
  LOGGER.info("The e Gifting Configurator map is successfully generated");
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(jsonNameSapce, properties);
  return data;
 }
 
 /**
  * <p>
  * This method sets the personaliseThisGiftCta, and personaliseThisGiftUrl which is fetched from the siteConfig
  * </p>
  * .
  * 
  * @param currentPage
  *         the current page
  * @param productProperties
  */
 private void setPersonalisedCta(Page currentPage, Map<String, Object> productProperties, UNIProduct product, I18n i18n,
   SlingHttpServletRequest slingRequest) {
  Map<String, Object> personalisedCtaMap = new HashMap<String, Object>();
  String personaliseThisGiftUrl = StringUtils.EMPTY;
  if (product != null) {
   String productId = product.getUniqueID();
   if (StringUtils.isBlank(productId)) {
    productId = product.getSKU();
   }
   try {
    personaliseThisGiftUrl = configurationService.getConfigValue(currentPage, ProductConstants.EGIFTING_CAT, ProductConstants.TX1_URL);
   } catch (ConfigurationNotFoundException e) {
    LOGGER.error("could not find tx1URL key in customisedcta category", e);
   }
   personaliseThisGiftUrl = StringUtils.isEmpty(personaliseThisGiftUrl) ? "" : ComponentUtil.getFullURL(currentPage.getContentResource()
     .getResourceResolver(), personaliseThisGiftUrl, slingRequest)
     + "?" + ProductConstants.EAN + "=" + productId;
  }
  personalisedCtaMap.put(PERSONALISE_GIFT_CTA, i18n.get(PERSONALISE_GIFT_CTA_LABEL));
  personalisedCtaMap.put(PERSONALISE_GIFT_URL, personaliseThisGiftUrl);
  productProperties.put(PERSONALISED_CTA_MAP, personalisedCtaMap);
  
 }
 
 /**
  * Sets the product gift attributes.
  * 
  * @param uniProduct
  *         the uni product
  * @param currentPage
  *         the current page
  * @param slingRequest
  *         the sling request
  * @return the product gift map
  */
 private void setProductGiftAttributes(UNIProduct uniProduct, Page currentPage, Map<String, Object> productProperties, boolean flag) {
  
  CustomProduct customProduct = uniProduct.getCustomProduct();
  String currency = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, ProductConstants.CURRENCY,
    ProductConstants.CURRENCY_SYMBOL);
  productProperties.put("isCustomizableProduct", flag);
  if (flag && customProduct != null) {
   DecimalFormat df = new DecimalFormat("#0.00");
   String customTotalPrice = customProduct.getCustomizeProductPrice() != null ? df.format(customProduct.getCustomizeProductPrice()) : "0.0";
   String wrappingDescriptionText = customProduct.getCustomizeGiftWrapDescription() != null ? customProduct.getCustomizeGiftWrapDescription()
     : StringUtils.EMPTY;
   String personalisationDesc = customProduct.getCustomizeDescription() != null ? customProduct.getCustomizeDescription() : "";
   productProperties.put(ProductConstants.CUSTOMISED_PRICE, StringUtils.isNotBlank(customTotalPrice) ? customTotalPrice : StringUtils.EMPTY);
   productProperties.put(ProductConstants.GIFT_WRAP_CONDITIONS_COPY, wrappingDescriptionText);
   productProperties.put(ProductConstants.PERSONALISED_DESC, personalisationDesc);
   productProperties.put(ProductConstants.CURRENCY_SYMBOL, currency);
  } else {
   productProperties.put(ProductConstants.CUSTOMISED_PRICE, StringUtils.EMPTY);
   productProperties.put(ProductConstants.GIFT_WRAP_CONDITIONS_COPY, StringUtils.EMPTY);
   productProperties.put(ProductConstants.PERSONALISED_DESC, StringUtils.EMPTY);
   productProperties.put(ProductConstants.CURRENCY_SYMBOL, currency);
   LOGGER.info("Product is not customisable");
  }
 }
 
 /**
  * <p>
  * This method sets the delivery data map containing delivery condition copy, and delivery tool tip text which is fetched from the siteConfig
  * </p>
  * .
  * 
  * @param currentPage
  *         the current page
  * @param productProperties
  */
 private void setDeliveryDetailAttributes(Page currentPage, Map<String, Object> productProperties) {
  
  String deliveryConditionCopy = StringUtils.EMPTY;
  String deliveryTooltip = StringUtils.EMPTY;
  try {
   deliveryConditionCopy = configurationService.getConfigValue(currentPage, ProductConstants.EGIFTING_CAT, ProductConstants.DELIVERY_CONDITIONS_COPY);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find deliverConditionCopy key in configuration ", e);
  }
  try {
   deliveryTooltip = configurationService.getConfigValue(currentPage, ProductConstants.EGIFTING_CAT, ProductConstants.DELIVERY_TOOL_TIP);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find deliveryTooltip key in configuration ", e);
  }
  productProperties.put(ProductConstants.DELIVERY_CONDITIONS_COPY, deliveryConditionCopy);
  productProperties.put(ProductConstants.DELIVERY_TOOL_TIP, deliveryTooltip);
  
 }
 
}
