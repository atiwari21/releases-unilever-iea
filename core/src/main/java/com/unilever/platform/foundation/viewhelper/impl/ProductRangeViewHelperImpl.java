/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.tagging.Tag;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.cache.ComponentViewAndViewHelperCache;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.core.dto.SearchDTO;
import com.unilever.platform.aem.foundation.core.service.SearchParam;
import com.unilever.platform.aem.foundation.core.service.SearchService;
import com.unilever.platform.foundation.components.ProductRangeComparator;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.constants.CommonConstants;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.constants.MultipleRelatedProductsConstants;

/**
 * <p>
 * Responsible for reading author input for multipleRelatedProductsAlternate component and get all the details of products which are tagged to the
 * current page and generate a product list. Also read the site configuration for maximum number configured for the product list.
 * </p>
 * .
 */
@Component(description = "ProductRangeViewHelperImpl", immediate = true, metatype = true, label = "ProductRangeViewHelperImpl")
@Service(value = { ProductRangeViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.PRODUCT_RANGE, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class ProductRangeViewHelperImpl extends MultipleRelatedProductsBaseViewHelperImpl {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ProductRangeViewHelperImpl.class);
 
 /** The Constant CTA_LABEL. */
 private static final String CTA_LABEL = "productRange.ctaLabel";
 
 /**
  * Constant for setting cta label.
  */
 
 private static final String LABEL_TEXT = "labelText";
 
 /**
  * Constant for setting navigation label.
  */
 
 private static final String NAVIGATION_URL = "navigationURL";
 
 /** The Constant GLOBAL_CONFIG_CAT. */
 private static final String GLOBAL_CONFIG_CAT = "productRange";
 
 /** The Constant FIFTY. */
 private static final int FIFTY = 50;
 
 /** The Constant DEBUG_STRING. */
 private static final String DEBUG_STRING = "could not find filterNamespaces key in productListing category";
 
 /** The Constant CTA_LABEL_KEY. */
 private static final String CTA_LABEL_KEY = "ctaLabel";
 
 /** The component view and view helper cache. */
 /*
  * reference for cache object
  */
 @Reference
 ComponentViewAndViewHelperCache componentViewAndViewHelperCache;
 /*
  * reference for site configuration service
  */
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The search service. */
 @Reference
 SearchService searchService;
 
 /**
  * The method processData overrides method is responsible validating author input and returning a list of products, which are tagged to the current
  * page and maximum number for the links from site configuration to be displayed on the page.
  * 
  * @param content
  *         {@link Map} of all user input received as input
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return Map holding the result data.
  */
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  LOGGER.debug("Executing ProductRangeViewHelperImpl.java. Inside processData method");
  Map<String, Object> data = new HashMap<String, Object>();
  String jsonNameSpace = MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE);
  Map<String, String> configMap = ComponentUtil.getConfigMap(getCurrentPage(resources), GLOBAL_CONFIG_CAT);
  
  Map<String, Object> productPangeMap = new HashMap<String, Object>();
  productPangeMap = getValuesForPageType(resources, jsonNameSpace, configMap);
  
  data.put(jsonNameSpace, productPangeMap);
  return data;
 }
 
 /**
  * Gets the values for page type.
  * 
  * @param jsonNameSpace
  *         the json name space
  * @param resourceResolver
  *         the resource resolver
  * @param properties
  *         the properties
  * @param currentPage
  *         the current page
  * @param pageManager
  *         the page manager
  * @param configMap
  *         the config map
  * @param tagManager
  * @return the values for page type
  */
 private Map<String, Object> getValuesForPageType(final Map<String, Object> resources, String jsonNameSpace, Map<String, String> configMap) {
  Page currentPage = getCurrentPage(resources);
  Map<String, Object> dataManual = new LinkedHashMap<String, Object>();
  Map<String, Object> finalProperties = new LinkedHashMap<String, Object>();
  Map<String, Object> ctaMap = new LinkedHashMap<String, Object>();
  ResourceResolver resourceResolver = getResourceResolver(resources);
  List<String> pageTagIdListForOrTags = new ArrayList<String>();
  List<String> pageTagIdListForAndTags = new ArrayList<String>();
  List<String> excludeNamespace = getNamespaceForAndCondList(currentPage);
  List<String> includeNamespace = getNamespaceForOrCondList(currentPage);
  
  populatePageTagIdList(pageTagIdListForOrTags, pageTagIdListForAndTags, includeNamespace, excludeNamespace, currentPage);
  if (!pageTagIdListForOrTags.isEmpty() && pageTagIdListForOrTags != null) {
   List<SearchDTO> rangePagesPathList = getFilteredProductSet(pageTagIdListForOrTags, pageTagIdListForAndTags, resources, configMap);
   
   List<String> rangePagesPathListSorted = getSortedFinalListBasedOnComparator(resources, rangePagesPathList, configMap);
   if (rangePagesPathListSorted != null && !rangePagesPathListSorted.isEmpty()) {
    String landingPagePath = rangePagesPathListSorted.get(0);
    Resource resource = resourceResolver.resolve(landingPagePath);
    Page currentLandingPage = resource.adaptTo(Page.class);
    
    ComponentUtil.setPageProperties(finalProperties, currentLandingPage, currentPage, getSlingRequest(resources));
    
    String landingPageFullUrl = ComponentUtil.getFullURL(resourceResolver, landingPagePath, getSlingRequest(resources));
    if (StringUtils.isNotEmpty(landingPageFullUrl)) {
     ctaMap.put(LABEL_TEXT, getI18n(resources).get(CTA_LABEL));
     ctaMap.put(NAVIGATION_URL, landingPageFullUrl);
    }
    finalProperties.put(CTA_LABEL_KEY, ctaMap);
    dataManual.put(jsonNameSpace, finalProperties);
   } else {
    dataManual.put(jsonNameSpace, StringUtils.EMPTY);
   }
  } else {
   dataManual.put(jsonNameSpace, StringUtils.EMPTY);
  }
  
  return dataManual;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.impl. MultipleRelatedProductsBaseViewHelperImpl
  * #getSortedFinalListBasedOnComparator(java.util.List, java.util.Map, com.day.cq.wcm.api.Page)
  */
 protected List<String> getSortedFinalListBasedOnComparator(final Map<String, Object> resources, List<SearchDTO> productsResultList,
   Map<String, String> configMap) {
  int max = CommonConstants.FOUR;
  try {
   max = StringUtils.isBlank(configMap.get(UnileverConstants.MAX)) ? CommonConstants.FOUR : Integer.parseInt(configMap.get(UnileverConstants.MAX));
  } catch (NumberFormatException nfe) {
   LOGGER.error("Maximum article for page list must be number", nfe);
  }
  String[] includeSortList = !StringUtils.isBlank(configMap.get(MultipleRelatedProductsConstants.INCLUDE_SORT_LIST)) ? configMap.get(
    MultipleRelatedProductsConstants.INCLUDE_SORT_LIST).split(CommonConstants.COMMA_CHAR) : new String[] {};
  String[] exclude = !StringUtils.isBlank(configMap.get(UnileverConstants.EXCLUDE_LIST)) ? configMap.get(UnileverConstants.EXCLUDE_LIST).split(
    CommonConstants.COMMA_CHAR) : new String[] {};
  List<String> includeTagIdList = getIncludeTagIdList(resources, includeSortList, exclude);
  LOGGER.debug("Maximum articles for page list= " + max);
  long startTime = System.currentTimeMillis();
  
  Collections.sort(productsResultList, new ProductRangeComparator(includeTagIdList));
  
  long stopTime = System.currentTimeMillis();
  
  LOGGER.info("Time elapsed in soring the result of MultipleRelatedArticles = " + (stopTime - startTime));
  List<SearchDTO> productsResultListFinal = productsResultList.size() > max ? productsResultList.subList(0, max) : productsResultList;
  return getPathList(productsResultListFinal);
 }
 
 /**
  * Populate page tag id list.
  * 
  * @param pageTagIdListForOrTags
  *         the page tag id list for or tags
  * @param pageTagIdListForAndTags
  *         the page tag id list for and tags
  * @param namespaceForOrTags
  *         the namespace for or tags
  * @param namespaceForAndTags
  *         the namespace for and tags
  * @param currentPage
  *         the current page
  * @param properties
  *         the properties
  */
 private void populatePageTagIdList(List<String> pageTagIdListForOrTags, List<String> pageTagIdListForAndTags, List<String> namespaceForOrTags,
   List<String> namespaceForAndTags, Page currentPage) {
  
  Tag[] pageTagList = currentPage.getTags();
  
  for (Tag tagId : pageTagList) {
   LOGGER.debug("page tagId list  {}", pageTagList.toString());
   for (String namespaceForAndTag : namespaceForAndTags) {
    
    if (StringUtils.startsWith(tagId.getTagID(), namespaceForAndTag.trim()) && !namespaceForAndTag.isEmpty()) {
     pageTagIdListForAndTags.add(tagId.getTagID());
     
    }
   }
   for (String namespaceForOrTag : namespaceForOrTags) {
    
    if (StringUtils.startsWith(tagId.getTagID(), namespaceForOrTag.trim()) && !namespaceForOrTags.isEmpty()) {
     pageTagIdListForOrTags.add(tagId.getTagID());
     
    }
   }
   
  }
  
 }
 
 /**
  * Gets the namespace for or cond list.
  * 
  * @param page
  *         the page
  * @param dialogProperties
  *         the dialog properties
  * @return the namespace for or cond list
  */
 private List<String> getNamespaceForOrCondList(Page page) {
  
  List<String> namespaceForOrCondList = new ArrayList<String>();
  try {
   String val = configurationService.getConfigValue(page, GLOBAL_CONFIG_CAT, UnileverConstants.INCLUDE_LIST);
   if (StringUtils.isNotBlank(val)) {
    String[] valArr = val.split(UnileverConstants.COMMA);
    for (String string : valArr) {
     namespaceForOrCondList.add(string.isEmpty() ? "" : string);
    }
   }
  } catch (ConfigurationNotFoundException e) {
   LOGGER.debug(DEBUG_STRING, e);
  }
  
  LOGGER.debug("Namespaces for OR Condition List   = " + namespaceForOrCondList);
  return namespaceForOrCondList;
 }
 
 /**
  * Gets the namespace for and cond list.
  * 
  * @param page
  *         the page
  * @param dialogProperties
  *         the dialog properties
  * @return the namespace for and cond list
  */
 private List<String> getNamespaceForAndCondList(Page page) {
  
  List<String> namespaceForAndCondList = new ArrayList<String>();
  try {
   String val = configurationService.getConfigValue(page, GLOBAL_CONFIG_CAT, UnileverConstants.EXCLUDE_LIST);
   String[] valArr = val.split(UnileverConstants.COMMA);
   if (valArr != null) {
    for (String string : valArr) {
     namespaceForAndCondList.add(string.isEmpty() ? "" : string);
    }
   }
   
  } catch (ConfigurationNotFoundException e) {
   LOGGER.debug(DEBUG_STRING, e);
  }
  
  LOGGER.debug("Namespaces for And Condition List   = " + namespaceForAndCondList);
  return namespaceForAndCondList;
 }
 
 /**
  * Gets the filtered product set.
  * 
  * @param pageTagIdListForOrTags
  *         the page tag id list for or tags
  * @param pageTagIdListForAndTags
  *         the page tag id list for and tags
  * @param currentPage
  *         the current page
  * @return the filtered product set
  */
 private List<SearchDTO> getFilteredProductSet(List<String> pageTagIdListForOrTags, List<String> pageTagIdListForAndTags,
   Map<String, Object> resources, Map<String, String> configMap) {
  List<String> taggedPagesList = new ArrayList<String>();
  List<String> negateTag = new ArrayList<String>();
  Page currentPage = getCurrentPage(resources);
  String currentPagePath = currentPage.getPath();
  String cqTemplete = getCqTemplete(configMap);
  Map<String, List<String>> propMap = new HashMap<String, List<String>>();
  List<String> propValueList = new ArrayList<String>();
  propValueList.add(cqTemplete);
  propMap.put(UnileverConstants.JCR_CONTENT_CQ_TEMPLATE, propValueList);
  int limit = getLimit(currentPage);
  List<SearchDTO> searchResult = searchService.getCreteriaPagesList(pageTagIdListForOrTags, pageTagIdListForAndTags, negateTag, currentPagePath,
    propMap, new SearchParam(0, limit, UnileverConstants.DESCENDING), getResourceResolver(resources));
  for (SearchDTO searchDTO : searchResult) {
   taggedPagesList.add(searchDTO.getResultNodePath());
  }
  LOGGER.debug("Valid Pages Path List  = " + taggedPagesList.size());
  
  return searchResult;
 }
 
 /**
  * Gets the cq templete.
  * 
  * @param currentPage
  *         the current page
  * @param configMap
  *         the config map
  * @return the cq templete
  */
 private String getCqTemplete(Map<String, String> configMap) {
  String cQTemplate = StringUtils.EMPTY;
  cQTemplate = !StringUtils.isBlank(configMap.get(NameConstants.PN_TEMPLATE)) ? configMap.get(NameConstants.PN_TEMPLATE) : StringUtils.EMPTY;
  return cQTemplate;
 }
 
 /**
  * Gets the limit.
  * 
  * @param page
  *         the page
  * @return the limit
  */
 private int getLimit(Page page) {
  int limit = FIFTY;
  try {
   String val = configurationService.getConfigValue(page, GLOBAL_CONFIG_CAT, "limit");
   limit = StringUtils.isNotBlank(val) ? Integer.parseInt(val) : limit;
  } catch (ConfigurationNotFoundException e) {
   LOGGER.debug(DEBUG_STRING, e);
  }
  return limit;
 }
 
 /**
  * Gets the config service.
  * 
  * @return configurationService
  */
 protected ConfigurationService getConfigService() {
  
  return this.configurationService;
 }
 
 /**
  * Gets the search service.
  * 
  * @return searchService
  */
 protected SearchService getSearchService() {
  
  return this.searchService;
 }
 
}
