/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.sapient.platform.iea.aem.utils.constants.CommonConstants;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * This class prepares the data map for Breadcrumb.
 */
@Component(description = "BreadcrumbViewHelperImpl", immediate = true, metatype = true, label = "BreadcrumbViewHelperImpl")
@Service(value = { BreadcrumbViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.BREADCRUMB, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class BreadcrumbViewHelperImpl extends BaseViewHelper {
 
 /**
  * Identifier for parent level.
  */
 private static final String PARENT_LEVEL = "parentlevel";
 
 /**
  * Identifier for page name.
  */
 private static final String PAGE_NAME = "pageName";
 /**
  * Identifier for page URL.
  */
 private static final String PAGE_URL = "pageURL";
 
 /**
  * logger object.
  */
 private static final Logger LOGGER = LoggerFactory.getLogger(BreadcrumbViewHelperImpl.class);
 
 /** The Constant GLOBAL_CONFIG_CAT. */
 private static final String GLOBAL_CONFIG_CAT = "breadCrumb";
 
 /**
  * <p>
  * This method returns Breadcrumb configuration map. If the all data is required then all the flyOutMenu is also returned along with the navigation
  * data.
  * </p>
  * <p>
  * This method also returns the Breadcrumb data map when all data is not selected.
  * </p>
  * 
  * @param content
  *         {@link Map} of all user input received as input
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return Map holding the result data.
  **/
 
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  LOGGER.info("inside breadcrumb viewhelper processData");
  Page currentPage = getCurrentPage(resources);
  Map<String, String> configMap = ComponentUtil.getConfigMap(currentPage, GLOBAL_CONFIG_CAT);
  Map<String, Object> breadcrumbFinalDataMap = new HashMap<String, Object>();
  
  String jsonNameSpace = getJsonNameSpace(content);
  
  breadcrumbFinalDataMap.put(jsonNameSpace, prepareDataMap(content, resources, configMap));
  
  LOGGER.debug("The breadcrumb data mep returned is " + breadcrumbFinalDataMap);
  return breadcrumbFinalDataMap;
 }
 
 /**
  * This method prepares the data map required. It collects all the page level configurations and clubs them into a map which is then returned id all
  * data is selected else it returns only the navigation data map.
  * 
  * @param content
  *         The navigation content map from where the page path can be extracted
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * 
  * @return The Navigation Menu Map.
  */
 private Map<String, Object> prepareDataMap(final Map<String, Object> content, final Map<String, Object> resources, Map<String, String> configMap) {
  
  List<Map<String, Object>> navigationList = new ArrayList<Map<String, Object>>();
  Map<String, Object> navigationFinalMap = new HashMap<String, Object>();
  
  Map<String, Object> primaryNavigationMap;
  
  Map<String, Object> properties = getProperties(content);
  ResourceResolver resourceResolver = getResourceResolver(resources);
  SlingHttpServletRequest request = getSlingRequest(resources);
  
  /** done as a part of UAT fix **/
  
  String currentPagePath = request.getPathInfo();
  currentPagePath = resourceResolver.resolve(currentPagePath).getPath();
  Page page = resourceResolver.adaptTo(PageManager.class).getContainingPage(currentPagePath);
  
  /** get the input parent level from configuration if not entered in dialog **/
  String typeBreadcrumb = MapUtils.getString(properties, "breadcrumbType");
  String parentlevelstr = StringUtils.EMPTY;
  if(StringUtils.isNotBlank(typeBreadcrumb) && StringUtils.equalsIgnoreCase("page", typeBreadcrumb)){
          String startingPagePath = MapUtils.getString(properties, "startingpage");
          Page startingPage = resourceResolver.adaptTo(PageManager.class).getContainingPage(startingPagePath);
          parentlevelstr = String.valueOf(startingPage.getDepth()-1);
  }else{
      parentlevelstr = MapUtils.getString(properties, PARENT_LEVEL); 
  }
  
  int parentlevel = 0;
  
  /** check if input in dialog is numeric and non empty **/
  if (StringUtils.isNotEmpty(parentlevelstr) && StringUtils.isNumeric(parentlevelstr)) {
   parentlevel = Integer.valueOf(parentlevelstr);
  } else {
   parentlevel = !StringUtils.isBlank(configMap.get(PARENT_LEVEL)) ? Integer.parseInt(configMap.get(PARENT_LEVEL)) : parentlevel;
  }
  
  /**
   * -1 is needed since get depth is not in sync with the getAbsoluteParent function
   **/
  int currentLevelX = page.getDepth();
  
  int currentLevel = currentLevelX - 1;
  
  if (parentlevel != 0) {
   
   Page pageobj;
   String pageURL;
   String pageName;
   
   while (parentlevel <= currentLevel) {
    
    pageobj = page.getAbsoluteParent(parentlevel);
    
    if (pageobj != null && !pageobj.isHideInNav()) {
     primaryNavigationMap = new HashMap<String, Object>();
     
     pageURL = ComponentUtil.getFullURL(resourceResolver, pageobj.getPath(), request);
     
     pageName = pageNameSelection(pageobj);
     
     primaryNavigationMap.put(PAGE_NAME, pageName);
     
     addPageUrl(parentlevel, currentLevel, pageURL, primaryNavigationMap);
     
     LOGGER.debug("Map for breadcrumbs list " + primaryNavigationMap);
     navigationList.add(primaryNavigationMap);
     
    }
    parentlevel++;
    
   }
  }
  
  navigationFinalMap.put("items", navigationList.toArray());
  return navigationFinalMap;
 }
 
 private String pageNameSelection(final Page pageobj) {
  
  String pageName;
  
  if (pageobj.getNavigationTitle() != null) {
   pageName = pageobj.getNavigationTitle();
  } else {
   pageName = pageobj.getTitle();
  }
  
  return pageName;
 }
 
 private void addPageUrl(final int parentlevel, final int currentLevel, final String pageURL, final Map<String, Object> primaryNavigationMap) {
  if (parentlevel < currentLevel) {
   primaryNavigationMap.put(PAGE_URL, pageURL);
  } else {
   primaryNavigationMap.put(PAGE_URL, "");
  }
 }
 
}
