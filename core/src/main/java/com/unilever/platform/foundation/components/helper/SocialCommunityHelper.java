/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.foundation.components.helper;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * The Class SocialCommunityHelper.
 */
public class SocialCommunityHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(SocialCommunityHelper.class);
 
 /** The Constant OPINION_TEXT. */
 private static final String OPINION_TEXT = "opinionText";
 
 /** The Constant ID. */
 private static final String ID = "id";
 
 /** The Constant OPINION_TYPE. */
 private static final String OPINION_TYPE = "opinionType";
 
 /** The Constant END_DATE. */
 private static final String END_DATE = "endDate";
 
 /** The Constant START_DATE. */
 private static final String START_DATE = "startDate";
 
 /** The Constant OPINION_IMAGE. */
 private static final String OPINION_IMAGE = "opinionImage";
 
 /** The Constant OPINION_IMAGE_ALT_TEXT. */
 private static final String OPINION_IMAGE_ALT_TEXT = "opinionImageAltText";
 
 /** The Constant TEXT_AND_IMAGE. */
 private static final String TEXT_AND_IMAGE = "textAndImage";
 
 /** The Constant TEXT_ONLY. */
 private static final String TEXT_ONLY = "textOnly";
 
 /** The Constant TEXT. */
 private static final String TEXT = "text";
 
 /** The Constant FREE_TEXT_CHAR_COUNT. */
 private static final String FREE_TEXT_CHAR_COUNT = "freeTextCharCount";
 
 /** The Constant FREE_TEXT_PLACEHOLDER. */
 private static final String FREE_TEXT_PLACEHOLDER = "freeTextPlaceholder";
 
 /** The Constant FREE_TEXT. */
 private static final String FREE_TEXT = "freeText";
 
 /** The Constant IMAGE_ALT_TEXT. */
 private static final String IMAGE_ALT_TEXT = "questionImageAltText";
 
 /** The Constant IMAGE. */
 private static final String IMAGE = "questionImage";
 
 /** The Constant SUB_HEADLINE. */
 private static final String SUB_HEADLINE = "questionSubHeadline";
 
 /** The Constant HEADLINE. */
 private static final String HEADLINE = "questionHeadline";
 
 /**
  * Instantiates a new social community helper.
  */
 private SocialCommunityHelper() {
 }
 
 /**
  * Gets the question resource properties.
  * 
  * @param resources
  *         the resources
  * @param questionResource
  *         the question resource
  * @return the question resource properties
  */
 public static Map<String, Object> getQuestionResourceProperties(Map<String, Object> resources, Resource questionResource) {
  Map<String, Object> questionMap = new HashMap<String, Object>();
  if (questionResource != null) {
   Map<String, Object> questionProps = (Map<String, Object>) questionResource.getValueMap();
   Map<String, Object> imageMap = new HashMap<String, Object>();
   
   Page page = BaseViewHelper.getCurrentPage(resources);
   
   String opinionType = null == questionProps.get(OPINION_TYPE) ? StringUtils.EMPTY : questionProps.get(OPINION_TYPE).toString();
   String headLine = null == questionProps.get(HEADLINE) ? StringUtils.EMPTY : questionProps.get(HEADLINE).toString();
   String subHeadline = null == questionProps.get(SUB_HEADLINE) ? StringUtils.EMPTY : questionProps.get(SUB_HEADLINE).toString();
   String image = null == questionProps.get(IMAGE) ? StringUtils.EMPTY : questionProps.get(IMAGE).toString();
   String altText = null == questionProps.get(IMAGE_ALT_TEXT) ? StringUtils.EMPTY : questionProps.get(IMAGE_ALT_TEXT).toString();
   imageMap = ImageHelper.getImageMap(image, page, image, headLine, altText, BaseViewHelper.getSlingRequest(resources));
   GregorianCalendar startDate = (GregorianCalendar) MapUtils.getObject(questionProps, START_DATE);
   GregorianCalendar endDate = (GregorianCalendar) MapUtils.getObject(questionProps, END_DATE);
   String parseStartDate = ComponentUtil.getFormattedDateString(startDate, null);
   String parseEndDate = ComponentUtil.getFormattedDateString(endDate, null);
   String id = null == questionProps.get(ID) ? StringUtils.EMPTY : questionProps.get(ID).toString();
   questionMap = getOpinionTypeDetails(resources, questionResource, questionProps, page, opinionType);
   Map<String, Object> questionConfigurations = GlobalConfigurationUtility.getOverriddenConfigMap("question", page, questionProps);
   questionMap.put("id", id);
   questionMap.put("headline", headLine);
   questionMap.put("subHeadline", subHeadline);
   questionMap.put("startDate", parseStartDate);
   questionMap.put("endDate", parseEndDate);
   questionMap.put("subHeadline", subHeadline);
   questionMap.put("image", imageMap);
   questionMap.put("opinionType", opinionType);
   questionMap.put("configuration", questionConfigurations);
  } else {
   LOGGER.debug("questionResource is null");
  }
  return questionMap;
 }
 
 /**
  * Gets the opinion type details.
  * 
  * @param resources
  *         the resources
  * @param questionResource
  *         the question resource
  * @param questionProps
  *         the question props
  * @param page
  *         the page
  * @param opinionType
  *         the opinion type
  * @return the opinion type details
  */
 public static Map<String, Object> getOpinionTypeDetails(Map<String, Object> resources, Resource questionResource, Map<String, Object> questionProps,
   Page page, String opinionType) {
  Map<String, Object> questionMap = new HashMap<String, Object>();
  if (FREE_TEXT.equals(opinionType)) {
   Map<String, Object> freeTextMap = new HashMap<String, Object>();
   
   String freeTextPlaceholder = null == questionProps.get(FREE_TEXT_PLACEHOLDER) ? StringUtils.EMPTY : questionProps.get(FREE_TEXT_PLACEHOLDER)
     .toString();
   String freeTextCharCount = null == questionProps.get(FREE_TEXT_CHAR_COUNT) ? StringUtils.EMPTY : questionProps.get(FREE_TEXT_CHAR_COUNT)
     .toString();
   
   freeTextMap.put("placeHolderText", freeTextPlaceholder);
   freeTextMap.put("characterLimit", Integer.parseInt(freeTextCharCount));
   questionMap.put("freeText", freeTextMap);
   
  } else if (TEXT.equals(opinionType)) {
   List<Map<String, String>> textOnly = ComponentUtil.getNestedMultiFieldProperties(questionResource, TEXT_ONLY);
   questionMap.put(TEXT, textOnly);
  } else if (TEXT_AND_IMAGE.equals(opinionType)) {
   
   List<Map<String, String>> textAndImage = ComponentUtil.getNestedMultiFieldProperties(questionResource, TEXT_AND_IMAGE);
   List<Map<String, Object>> textAndImageFinalList = new ArrayList<Map<String, Object>>();
   
   Iterator<Map<String, String>> iterator = textAndImage.iterator();
   while (iterator.hasNext()) {
    Map<String, Object> tempTextAndImageMap = new HashMap<String, Object>();
    
    Map<String, String> textAndImageMap = iterator.next();
    String answerText = null == textAndImageMap.get(OPINION_TEXT) ? StringUtils.EMPTY : textAndImageMap.get(OPINION_TEXT);
    String imageAltText = null == textAndImageMap.get(OPINION_IMAGE_ALT_TEXT) ? StringUtils.EMPTY : textAndImageMap.get(OPINION_IMAGE_ALT_TEXT);
    String opinionImage = null == textAndImageMap.get(OPINION_IMAGE) ? StringUtils.EMPTY : textAndImageMap.get(OPINION_IMAGE);
    Map<String, Object> tempImageMap = getImageMapFromPath(resources, page, imageAltText, opinionImage);
    
    tempTextAndImageMap.put("answerText", answerText);
    tempTextAndImageMap.put("image", tempImageMap);
    textAndImageFinalList.add(tempTextAndImageMap);
    
   }
   
   questionMap.put("text", textAndImageFinalList);
   
  }
  return questionMap;
 }
 
 /**
  * Gets the image map from path.
  * 
  * @param resources
  *         the resources
  * @param page
  *         the page
  * @param altText
  *         the alt text
  * @param imagePath
  *         the image path
  * @return the image map from path
  */
 public static Map<String, Object> getImageMapFromPath(Map<String, Object> resources, Page page, String altText, String imagePath) {
  String imageName = ImageHelper.fetchImageFileName(imagePath);
  return ImageHelper.getImageMap(imagePath, page, imagePath, imageName, altText, BaseViewHelper.getSlingRequest(resources));
  
 }
 
}
