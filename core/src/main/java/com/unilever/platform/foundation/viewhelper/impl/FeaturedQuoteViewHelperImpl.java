/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.utils.AdaptiveUtil;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.SocialShareHelper;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * <p>
 * The goal of this component is to display featured quote on pages as well as to share the quote to social sharing via addThis widget on pages. The
 * social sharing options will be managed via global configurations and hence will remain same across the site pages. Once configured the social share
 * options will be available on the page for end user to share that particular page on the social sharing sites. Below are the properties which will
 * be displayed in this component
 * 
 * </p>
 */
@Component(description = "Featured Quote Viewhelper Impl", immediate = true, metatype = true, label = "k")
@Service(value = { FeaturedQuoteViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.FEATURED_QUOTE, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class FeaturedQuoteViewHelperImpl extends BaseViewHelper {
 /**
  * Constant for retrieving featuredQuote component property.
  */
 
 /** The Constant ALT_IMAGE. */
 private static final String TEXT = "text";
 
 /** The Constant VIDEO_ID. */
 private static final String REFERENCE = "reference";
 /** The Constant ID. */
 private static final String ENABLE_SOCIAL_SHARE = "enableSocialShare";
 
 /** The Constant SOCIAL_SHARING. */
 private static final String SOCIAL_SHARING = "socialSharing";
 
 /** The Constant FALSE. */
 private static final String FALSE = "false";
 
 /**
  * logger object.
  **/
 
 @Reference
 ConfigurationService configurationService;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(FeaturedQuoteViewHelperImpl.class);
 
 /**
  * The method processData overrides method is responsible validating author input and returning a list of sections containing
  * text,reference,enableSocialShare and socialSharing This method converts the multiwidget input (a String Array) into sections of maps so that it
  * can be accessed in JSP in consistent way.This method also validates the link url provided by author.
  * 
  * @param content
  *         , - {@link Map} of all user input received as input <code>ViewHelper.onGetData</code> method
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return {@link Map} holding the data
  */
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  
  LOGGER.debug("hero View Helper #processData called for processing fixed list.");
  Map<String, Object> properties = getProperties(content);
  
  Map<String, Object> featuredQuoteProperties = new LinkedHashMap<String, Object>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  String text = MapUtils.getString(properties, TEXT);
  String reference = MapUtils.getString(properties, REFERENCE);
  String enableSocialShare = MapUtils.getString(properties, ENABLE_SOCIAL_SHARE);
  if (enableSocialShare == null) {
   enableSocialShare = FALSE;
  }
  
  featuredQuoteProperties.put(TEXT, text);
  featuredQuoteProperties.put(REFERENCE, StringUtils.defaultString(reference));
  featuredQuoteProperties.put(ENABLE_SOCIAL_SHARE, StringUtils.defaultString(enableSocialShare));
  Map<String, Object> socialShareProperties = SocialShareHelper.addThisWidget(resources, configurationService);
  if (AdaptiveUtil.isAdaptive(getSlingRequest(resources))) {
   socialShareProperties.put("headline", getI18n(resources).get("socialShareAdaptive.headline"));
  }
  featuredQuoteProperties.put(SOCIAL_SHARING, socialShareProperties);
  LOGGER.debug("featured Quote Properties Map: " + featuredQuoteProperties);
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(getJsonNameSpace(content), featuredQuoteProperties);
  
  return data;
  
 }
 
}
