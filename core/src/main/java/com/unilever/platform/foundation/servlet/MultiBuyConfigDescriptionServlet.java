/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.servlet;

import java.io.IOException;
import java.io.Writer;

import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.io.JSONWriter;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;

/**
 * This class sets the tool tip of multi buy configuration.
 */
@SlingServlet(paths = { "/bin/multibuy/config/tooltip" }, methods = { HttpConstants.METHOD_GET, HttpConstants.METHOD_POST })
@Properties(value = {
        @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.aem.foundation.servlets.MultiBuyConfigDescriptionServlet", propertyPrivate = false),
        @Property(name = Constants.SERVICE_DESCRIPTION, value = "updates the field description based on query param key", propertyPrivate = false) })
public class MultiBuyConfigDescriptionServlet extends SlingAllMethodsServlet {
    
    private static final String FIELD_DESCRIPTION = "fieldDescription";
    /**
     * The constant serial version id
     */
    private static final long serialVersionUID = -8543633030820856997L;
    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(MultiBuyConfigDescriptionServlet.class);
    
    /*
     * (non-Javadoc)
     * 
     * @see org.apache.sling.api.servlets.SlingAllMethodsServlet#doPost(org.apache.sling.api.SlingHttpServletRequest,
     * org.apache.sling.api.SlingHttpServletResponse)
     */
    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
     * org.apache.sling.api.SlingHttpServletResponse)
     */
    @Override
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws ServletException, IOException {
        String fieldDescription = request.getParameter(FIELD_DESCRIPTION);
        if (StringUtils.isNotBlank(fieldDescription)) {
            I18n i18n = new I18n(request);
            Writer out = response.getWriter();
            JSONWriter writer = new JSONWriter(out);
            setMultiBUyConfigFieldDescription(fieldDescription, i18n, writer);
        }
    }
    
    /**
     * sets the multi buy configuration field description
     * 
     * @param fieldDescription
     * @param i18n
     * @param writer
     */
    private void setMultiBUyConfigFieldDescription(String fieldDescription, I18n i18n, JSONWriter writer) {
        try {
            writer.array();
            writer.object();
            writer.key("text").value(FIELD_DESCRIPTION);
            writer.key("value").value(i18n.get(fieldDescription));
            writer.endObject();
            writer.endArray();
        } catch (JSONException e) {
            LOGGER.error("RepositoryException occurred ", e);
        }
    }
}
