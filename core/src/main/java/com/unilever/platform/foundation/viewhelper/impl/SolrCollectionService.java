/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;
/**
 * Interface SolrCollectionService
 */
public interface SolrCollectionService {
 /**
  * method to get collection
  * @param brandName
  * @param locale
  * @return
  **/
 public String getCollection(String brandName, String locale);
 
}
