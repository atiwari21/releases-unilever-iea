/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.dam.DAMAssetUtility;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.SocialTABHelper;
import com.unilever.platform.foundation.constants.CommonConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * <p>
 * Responsible for Fetching Image and Caption related to the Twitter.
 * 
 * </p>
 */
@Component(description = "FeaturedTweetViewHelperImpl", immediate = true, metatype = true, label = "Featured Tweet Tab Component")
@Service(value = { FeaturedTweetViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.FEATURED_TWEET, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class FeaturedTweetViewHelperImpl extends BaseViewHelper {
 
 /** The Constant LINK_LABEL. */
 private static final String CTA_URL = "ctaUrl";
 
 /** The Constant LABEL. */
 private static final String IMAGE_PATH = "imagePath";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(FeaturedTweetViewHelperImpl.class);
 
 /** The Constant TWEET_DETAILS. */
 private static final String TWEET_DETAILS = "tweetDetails";
 
 /** The Constant ROTATION_PERIOD. */
 private static final String ROTATION_PERIOD = "rotationPeriod";
 
 /** The Constant START_DATE. */
 private static final String START_DATE = "startDate";
 
 /** The Constant CTA_LABEL. */
 private static final String CTA_LABEL = "ctaLabel";
 
 /** The Constant ZERO. */
 private static final String ZERO = "0";
 
 /** The Constant CTA_LABEL_KEY. */
 private static final String CTA_LABEL_KEY = "featuredTweet.ctaLabel";
 
 /** The Constant IMAGE_LINKS. */
 private static final String IMAGE_LINKS = "imageLinks";
 
 /** The Constant OPEN_IN_NEW_WINDOW. */
 private static final String OPEN_IN_NEW_WINDOW = "openInNewWindow";
 
 /**
  * The method processData overrides method is responsible validating author input and returning a map
  * 
  * @param content
  *         {@link Map} of all user input received as input
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return Map holding the result data.
  */
 
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  
  LOGGER.info("Featured Tweet View Helper #processData called for processing fixed list.");
  
  Map<String, Object> featuredTweetMap = new LinkedHashMap<String, Object>();
  Map<String, Object> properties = getProperties(content);
  
  ResourceResolver resourceResolver = getResourceResolver(resources);
  String jsonNameSpace = getJsonNameSpace(content);
  I18n i18n = getI18n(resources);
  Resource currentResource = getCurrentResource(resources);
  Page currentPage = getCurrentPage(resources);
  
  String ctaLabel = i18n.get(CTA_LABEL_KEY);
  String rotationPeriod = MapUtils.getString(properties, ROTATION_PERIOD, ZERO);
  double dValue = Double.parseDouble(rotationPeriod);
  int rotationPeriodTemp = (int) Math.ceil(dValue);
  GregorianCalendar startDate = (GregorianCalendar) MapUtils.getObject(properties, START_DATE);
  
  featuredTweetMap.put(ROTATION_PERIOD, Integer.toString(rotationPeriodTemp));
  featuredTweetMap.put(START_DATE, DAMAssetUtility.getEffectiveDateFromCalendar(startDate));
  featuredTweetMap.put(CTA_LABEL, ctaLabel);
  
  // Adding data related to each tweet
  addTweetDetails(resourceResolver, currentResource, currentPage, featuredTweetMap, resources);
  
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  data.put(jsonNameSpace, featuredTweetMap);
  
  return data;
  
 }
 
 /**
  * Adds the tweet details.
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param currentResource
  *         the current resource
  * @param currentPage
  *         the current page
  * @param featuredTweetMap
  *         the featured tweet map
  */
 private static void addTweetDetails(ResourceResolver resourceResolver, Resource currentResource, Page currentPage,
   Map<String, Object> featuredTweetMap, Map<String, Object> resources) {
  
  List<Map<String, Object>> featuredTweetList = new ArrayList<Map<String, Object>>();
  List<Map<String, String>> imageLinksList = ComponentUtil.getNestedMultiFieldProperties(currentResource, IMAGE_LINKS);
  
  for (Map<String, String> map : imageLinksList) {
   String imgPath = MapUtils.getString(map, IMAGE_PATH, StringUtils.EMPTY);
   String ctaUrl = MapUtils.getString(map, CTA_URL, StringUtils.EMPTY);
   String newWindow = MapUtils.getString(map, OPEN_IN_NEW_WINDOW, StringUtils.EMPTY);
   String openInNewWindow = newWindow != null && ("[true]".equals(newWindow) || "[yes]".equals(newWindow)) ? CommonConstants.BOOLEAN_TRUE
     : ProductConstants.FALSE;
   
   Map<String, Object> featuredTweetDetails = SocialTABHelper.getImageMapWithMetadata(imgPath, currentPage, resourceResolver,
     getSlingRequest(resources));
   featuredTweetDetails.put(CTA_URL, ctaUrl);
   
   featuredTweetDetails.put(OPEN_IN_NEW_WINDOW, Boolean.valueOf(openInNewWindow));
   // adding map to list
   featuredTweetList.add(featuredTweetDetails);
  }
  featuredTweetMap.put(TWEET_DETAILS, featuredTweetList.toArray());
 }
}
