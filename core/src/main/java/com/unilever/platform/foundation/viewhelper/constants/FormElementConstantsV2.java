/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.constants;

/**
 * This class holds all the constants that are used in the form elements V2.
 * 
 */
public final class FormElementConstantsV2 {
 
 /** The Constant FIELD_LABEL. */
 public static final String FIELD_LABEL = "fieldLabel";
 
 /** The Constant FIELD_ID. */
 public static final String FIELD_ID = "fieldId";
 
 /** The Constant PLACE_HOLDER_VALUE. */
 public static final String PLACE_HOLDER_VALUE = "placeHolderValue";
 
 /** The Constant DEFAULT_VALUE. */
 public static final String DEFAULT_VALUE = "defaultValue";
 
 /** The Constant TOOL_TIP_TEXT. */
 public static final String TOOL_TIP_TEXT = "tooltipText";
 
 /** The Constant DISABLED. */
 public static final String DISABLED = "disabled";
 
 /** The Constant READ_ONLY. */
 public static final String READ_ONLY = "readOnly";
 
 /** The Constant TEXT_FIELD_MAX_LENGTH. */
 public static final String TEXT_FIELD_MAX_LENGTH = "textFieldMaxLength";
 
 /** The Constant TEXT_FIELD_MANDATORY_FIELD. */
 public static final String TEXT_FIELD_MANDATORY_FIELD = "textFieldMandatoryField";
 
 /** The Constant REQUIRED_FIELD. */
 public static final String REQUIRED_FIELD = "required";
 
 /** The Constant TEXT_FIELD_REQUIRED_ERROR_MESSAGE. */
 public static final String TEXT_FIELD_REQUIRED_ERROR_MESSAGE = "textFieldRequiredErrorMessage";
 
 /** The Constant REQUIRED_MESSAGE. */
 public static final String REQUIRED_MESSAGE = "requiredMessage";
 
 /** The Constant TEXT_FIELD_CONSTRAINT_TYPE. */
 public static final String TEXT_FIELD_CONSTRAINT_TYPE = "textFieldConstraintType";
 
 /** The Constant CONSTRAINT_TYPE. */
 public static final String CONSTRAINT_TYPE = "constraintType";
 
 /** The Constant TEXT_FIELD_VALIDATION_ERROR_MESSAGE. */
 public static final String TEXT_FIELD_VALIDATION_ERROR_MESSAGE = "textFieldValidationErrorMessage";
 
 /** The Constant TEXT_FIELD_VALIDATION_EXPRESSION. */
 public static final String TEXT_FIELD_VALIDATION_EXPRESSION = "textFieldValidationExpression";
 
 /** The Constant RULES. */
 public static final String RULES = "rules";
 
 /** The Constant VIEW_TYPE. */
 public static final String VIEW_TYPE = "viewType";
 
 /** The Constant VALIDATIONS_MANDATORY_FIELD. */
 public static final String VALIDATIONS_MANDATORY_FIELD = "validationsMandatoryField";
 
 /** The Constant ROWS. */
 public static final String ROWS = "rows";
 
 /** The Constant VALIDATIONS_REQUIRED_ERROR_MESSAGE. */
 public static final String VALIDATIONS_REQUIRED_ERROR_MESSAGE = "validationsRequiredErrorMessage";
 
 /** The Constant VALIDATIONS_CONSTRAINT_TYPE. */
 public static final String VALIDATIONS_CONSTRAINT_TYPE = "validationsConstraintType";
 
 /** The Constant VALIDATIONS_VALIDATION_EXPRESSION. */
 public static final String VALIDATIONS_VALIDATION_EXPRESSION = "validationsvalidationExpression";
 
 /** The Constant VALIDATIONS_VALIDATION_ERROR_MESSAGE. */
 public static final String VALIDATIONS_VALIDATION_ERROR_MESSAGE = "validationsvalidationErrorMessage";
 
 /** The Constant NUMBER_MANDATORY_FIELD. */
 public static final String NUMBER_MANDATORY_FIELD = "numberMandatoryField";
 
 /** The Constant NUMBER_REQUIRED_ERROR_MESSAGE. */
 public static final String NUMBER_REQUIRED_ERROR_MESSAGE = "numberRequiredErrorMessage";
 
 /** The Constant NUMBER_VALIDATION_ERROR_MESSAGE. */
 public static final String NUMBER_VALIDATION_ERROR_MESSAGE = "numberValidationErrorMessage";
 
 /** The Constant DATE_MANDATORY_FIELD. */
 public static final String DATE_MANDATORY_FIELD = "dateMandatoryField";
 
 /** The Constant DATE_INCLUDE_INTERVAL_RESTRICTION. */
 public static final String DATE_INCLUDE_INTERVAL_RESTRICTION = "dateIncludeIntervalRestriction";
 
 /** The Constant DATE_REQUIRED_ERROR_MESSAGE. */
 public static final String DATE_REQUIRED_ERROR_MESSAGE = "dateRequiredErrorMessage";
 
 /** The Constant DATE_CONSTRAINT_TYPE. */
 public static final String DATE_CONSTRAINT_TYPE = "dateConstraintType";
 
 /** The Constant DATE_VALIDATION_EXPRESSION. */
 public static final String DATE_VALIDATION_EXPRESSION = "dateValidationExpression";
 
 /** The Constant DATE_VALIDATION_ERROR_MESSAGE. */
 public static final String DATE_VALIDATION_ERROR_MESSAGE = "dateValidationErrorMessage";
 
 /** The Constant NAME. */
 public static final String NAME = "name";
 
 /** The Constant CHECKED_BY_DEFAULT. */
 public static final String CHECKED_BY_DEFAULT = "checkedByDefault";
 
 /** The Constant FIELD_VALUE. */
 public static final String FIELD_VALUE = "fieldValue";
 
 /** The Constant DROP_DOWN_MANDATORY_FIELD. */
 public static final String DROP_DOWN_MANDATORY_FIELD = "dropdownMandatoryField";
 
 /** The Constant DROP_DOWN_REQUIRED_ERROR_MESSAGE. */
 public static final String DROP_DOWN_REQUIRED_ERROR_MESSAGE = "dropdownRequiredErrorMessage";
 
 /** The Constant ALLOW_MULTIPLE_FILES. */
 public static final String ALLOW_MULTIPLE_FILES = "allowMultipleFiles";
 
 /** The Constant FILE_UPLOAD_MAX_SIZE. */
 public static final String FILE_UPLOAD_MAX_SIZE = "fileuploadMaxFileSize";
 
 /** The Constant FILE_UPLOAD_MANDATORY_FIELD. */
 public static final String FILE_UPLOAD_MANDATORY_FIELD = "fileuploadMandatoryField";
 
 /** The Constant FILE_UPLOAD_REQUIRED_ERROR_MESSAGE. */
 public static final String FILE_UPLOAD_REQUIRED_ERROR_MESSAGE = "fileuploadRequiredErrorMessage";
 
 /** The Constant FILE_UPLOAD_SUPPORTED_FILE_EXTENSION. */
 public static final String FILE_UPLOAD_SUPPORTED_FILE_EXTENSION = "fileuploadSupportedFileExtension";
 
 /** The Constant FILE_UPLOAD_VALIDATION_ERROR_MESSAGE. */
 public static final String FILE_UPLOAD_VALIDATION_ERROR_MESSAGE = "fileuploadValidationErrorMessage";
 
 /** The Constant HIDDEN_VALUE. */
 public static final String HIDDEN_VALUE = "hiddenValue";
 
 /** The Constant FALSE. */
 public static final String FALSE = "false";
 
 /** The Constant TELEPHONE_NUMBER_REQUIRED_REEOR_MESSAGE. */
 public static final String TELEPHONE_NUMBER_REQUIRED_REEOR_MESSAGE = "telephoneNumberRequiredErrorMessage";
 
 /** The Constant TELEPHONE_NUMBER_MANDATORY_FIELD. */
 public static final String TELEPHONE_NUMBER_MANDATORY_FIELD = "telephoneNumbermandatoryField";
 
 /** The Constant TELEPHONE_NUMBER_CONSTRAINT_TYPE. */
 public static final String TELEPHONE_NUMBER_CONSTRAINT_TYPE = "telephoneNumberConstraintType";
 
 /** The Constant TELEPHONE_NUMBER_VALIDATION_EXPRESSION. */
 public static final String TELEPHONE_NUMBER_VALIDATION_EXPRESSION = "telephoneNumbervalidationExpression";
 
 /** The Constant TELEPHONE_NUMBER_VALIDATION_ERROR_MESSAGE. */
 public static final String TELEPHONE_NUMBER_VALIDATION_ERROR_MESSAGE = "telephoneNumbervalidationErrorMessage";
 
 /** The Constant TELEPHONE_NUMBER_VALIDATION_ERROR_MESSAGE. */
 public static final String TELEPHONE_NUMBER_MAXIMUM_LENGTH = "telephoneNumMaximumLength";
 
 /** The Constant CARD_NUMBER_REQUIRED_ERROR_MESSAGE. */
 public static final String CARD_NUMBER_REQUIRED_ERROR_MESSAGE = "cardNumberRequiredErrorMessage";
 
 /** The Constant CARD_NUMBER_CONSTRAINT_TYPE. */
 public static final String CARD_NUMBER_CONSTRAINT_TYPE = "cardNumberConstraintType";
 
 /** The Constant CARD_NUMBER_VALIDATION_EXPRESSION. */
 public static final String CARD_NUMBER_VALIDATION_EXPRESSION = "cardNumbervalidationExpression";
 
 /** The Constant CARD_NUMBER_VALIDATION_ERROR_MESSAGE. */
 public static final String CARD_NUMBER_VALIDATION_ERROR_MESSAGE = "cardNumbervalidationErrorMessage";
 
 /** The Constant DATE_CONDITION. */
 public static final String DATE_CONDITION = "dateCondition";
 
 /** The Constant DATE_YEAR. */
 public static final String DATE_YEAR = "dateYear";
 
 /** The Constant DATE_MONTH. */
 public static final String DATE_MONTH = "dateMonth";
 
 /** The Constant DATE_DAY. */
 public static final String DATE_DAY = "dateDay";
 
 /** The Constant DISPLAY_TEXT. */
 public static final String DISPLAY_TEXT = "displayText";
 
 /** The Constant VALUE. */
 public static final String VALUE = "value";
 
 /** The Constant FIELD_LABEL_RADIO. */
 public static final String FIELD_LABEL_RADIO = "fieldLabelRadio";
 
 /** The Constant FIELD_VALUE_RADIO. */
 public static final String FIELD_VALUE_RADIO = "fieldValueRadio";
 
 /** The Constant FIELD_ID_RADIO. */
 public static final String FIELD_ID_RADIO = "fieldIdRadio";
 
 /** The Constant TOOL_TIP_TEXT_RADIO. */
 public static final String TOOL_TIP_TEXT_RADIO = "tooltipTextRadio";
 
 /** The Constant CHECKED_BY_DEFAULT_RADIO. */
 public static final String CHECKED_BY_DEFAULT_RADIO = "checkedByDefaultRadio";
 
 /** The Constant TOOL_TIP_TEXT_RADIO. */
 public static final String DISABLED_RADIO = "disabledRadio";
 
 /** The Constant CHECKED_BY_DEFAULT_RADIO. */
 public static final String READ_ONLY_RADIO = "readOnlyRadio";
 
 /** The Constant CHECKED_BY_DEFAULT_RADIO. */
 public static final String GROUP_NAME = "groupName";
 
 /** The Constant MAX_FILE_SIZE. */
 public static final String MAX_FILE_SIZE = "maxFileSize";
 
 /** The Constant SUPPORTED_FILE_EXTENSIONS. */
 public static final String SUPPORTED_FILE_EXTENSIONS = "supportedFileExtensions";
 
 /** The Constant MSG. */
 public static final String MSG = "msg";
 
 /** The Constant SERVLET_PATH. */
 public static final String SERVLET_PATH = "servletPath";
 
 /** The Constant NUMBER. */
 public static final String NUMBER = "number";
 
 /** The Constant FORMSUBMIT. */
 public static final String FORMSUBMIT = "formsubmit";
 
 /** The Constant FORMBUTTON. */
 public static final String FORMBUTTON = "formbutton";
 
 /** The Constant BUTTONTYPE. */
 public static final String BUTTONTYPE = "buttonType";
 
 /** The Constant GOOGLE_CAPTHCHA_KEY. */
 public static final String GOOGLE_CAPTHCHA_KEY = "googleCapthchaKey";
 
 /** The Constant HIDDENFIELD. */
 public static final String HIDDENFIELD = "hiddenfield";
 
 /** The Constant DROPDOWN. */
 public static final String DROPDOWN = "dropdown";
 
 /** The Constant CHECKBOX. */
 public static final String CHECKBOX = "checkbox";
 
 /** The Constant TEXTAREA. */
 public static final String TEXTAREA = "textarea";
 
 /** The Constant CAPTCHA. */
 public static final String CAPTCHA = "captcha";
 
 /** The Constant RADIO. */
 public static final String RADIO = "radio";
 
 /** The Constant ELEMENT_TYPE. */
 public static final String ELEMENT_TYPE = "elementType";
 
 /** The Constant PATTERN. */
 public static final String PATTERN = "pattern";
 
 public static final String FILEUPLOAD = "fileupload";
 
 public static final String DATE = "date";
 
 public static final String DATA_TYPE = "dataType";
    
 public static final String ESCAPE_HTML_REGEX = "\\<.*?>";
 
 public static final String COUPON_CODE = "couponcode";
 public static final String INVALID_CODE = "invalidCodeMessage";
 public static final String CODE_EXPIRED_MSG = "codeExpiredMessage";
 public static final String GLOBAL_COUNT_EXCEED_MSG = "globalCountExceedMsg";
 public static final String USER_COUNT_EXCEED_MSG = "userCountExceedMsg";
 public static final String J_INVALID_CODE = "invalidCodeMessage";
 public static final String J_CODE_EXPIRED_MSG = "codeExpiredMessage";
 public static final String J_GLOBAL_COUNT_EXCEED_MSG = "globalCountExceedMsg";
 public static final String J_USER_COUNT_EXCEED_MSG = "userCountExceedMsg";
 public static final String J_COUPON_VALIDATION_MSG = "couponValidationMsg";
 public static final String J_ERROR_CODE_NOT_REDEEMED = "1001";
 public static final String J_ERROR_NO_DOWNSTREAM_SYSTEM = "1002";
 public static final String J_ERROR_CAMPGN_ID_NOT_PASSED = "1003";
 public static final String J_ERROR_NAME_NOT_RETRIEVED = "1004";
 public static final String J_ERROR_CODE_EXPIRY_MSG = "1005";
 public static final String J_ERROR_INVALID_CODE_MSG = "1006";
 public static final String J_ERROR_EXCEED_lIMIT_FOR_USER = "1007";
 public static final String J_ERROR_EXCEED_USAGE_lIMIT_MSG = "1008";
 public static final String I18N_ERROR_CODE_EXPIRY_MSG = "couponCode.expiryMessage";
 public static final String I18N_ERROR_EXCEED_lIMIT_FOR_USER = "couponCode.exceedUsageLimitForUserMessage";
 public static final String I18N_ERROR_EXCEED_USAGE_lIMIT_MSG = "couponCode.exceedUsageLimitMessage";
 public static final String I18N_ERROR_INVALID_CODE_MSG = "couponCode.invalidCodeMessage";
 public static final String I18N_ERROR_CODE_NOT_REDEEMED = "couponCode.couponNotRedeemed";
 public static final String I18N_ERROR_NO_DOWNSTREAM_SYSTEM = "couponCode.noDownStreamSystem";
 public static final String I18N_ERROR_CAMPGN_ID_NOT_PASSED = "aemCampaignId.idNotPassed";
 public static final String I18N_ERROR_NAME_NOT_RETRIEVED = "couponCode.nameNotRetrieved";
 public static final String J_COUPON_ERROR_CODE = "couponErrorCodes";
    

  /* Private Constructor.
  */
 private FormElementConstantsV2() {
  
 }
 
}
