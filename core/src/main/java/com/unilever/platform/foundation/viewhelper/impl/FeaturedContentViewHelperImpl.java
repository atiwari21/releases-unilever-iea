/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.tagging.Tag;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.core.service.SearchService;
import com.unilever.platform.aem.foundation.dam.DAMAssetUtility;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.LandingPageListHelper;
import com.unilever.platform.foundation.components.helper.TaggingComponentsHelper;
import com.unilever.platform.foundation.components.helper.VideoHelper;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.Image;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * <p>
 * Responsible for reading author input for featured category component and returns a featured product.
 * 
 * </p>
 */
@Component(description = "FeaturedContentViewHelperImpl", immediate = true, metatype = true, label = "FeaturedContentViewHelperImpl")
@Service(value = { FeaturedContentViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.FEATURED_CONTENT, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class FeaturedContentViewHelperImpl extends BaseViewHelper {
 
 private static final String LABEL = "label";
 
 private static final String VALUE = "value";
 
 private static final String TAGS = "tags";
 
 private static final String PARENT = "parent";
 
 private static final String OVERRIDE_GLOBAL_CONFIG = "overrideGlobalConfig";
 
 /**
  * Constant for retrieving background image.
  */
 private static final String FEATURED_BACKGROUND = "backgroundImage";
 
 /**
  * Constant for retrieving alt text for background.
  */
 
 private static final String ALT_BACKGROUND = "backgroundImgAltText";
 
 /**
  * Constant for setting cta label.
  */
 
 private static final String LABEL_TEXT = "labelText";
 
 /**
  * Constant for setting navigation label.
  */
 
 private static final String CTA_URL = "ctaUrl";
 
 /**
  * Constant for setting cta map.
  */
 
 private static final String CTA = "cta";
 
 /**
  * Constant for setting cta value.
  */
 
 private static final String CTA_TEXT = "ctaText";
 
 private static final String FEATURE_TAGS = "featureTags";
 
 /** The Constant NEW_WINDOW. */
 private static final String NEW_WINDOW = "newWindow";
 
 /** The Constant OPEN_IN_NEW_WINDOW. */
 private static final String OPEN_IN_NEW_WINDOW = "openInNewWindow";
 
 /** The Constant GLOBAL_CONFIG_CAT. */
 private static final String GLOBAL_CONFIG_CAT = "featuredContent";
 
 /** The Constant FEATURED_CONTENT_TYPE. */
 private static final String FEATURED_CONTENT_TYPE = "featuredContentType";
 
 /**
  * Constant for setting HEADING.
  */
 
 private static final String HEADING = "heading";
 /** The Constant SHORT SUB HEADING. */
 private static final String SHORT_SUB_HEADING = "shortSubheading";
 /** The Constant TEXT. */
 private static final String TEXT = "text";
 /** The Constant MANUAL CONTENT. */
 private static final String MANUAL_CONTENT = "Provide Manual Content";
 /** The Constant IDENTIFY PAGE BY TAGS. */
 private static final String IDENTIFY_PAGE_BY_TAGS = "Identify Page By Tags";
 /** The Constant IDENTIFY MANUALLY SELECT PAGE. */
 private static final String MAUNUALLY_SELECT_PAGE = "Manually Select Page";
 /** The Constant START_DATE. */
 private static final String START_DATE = "startDate";
 /** The Constant END_DATE. */
 private static final String END_DATE = "endDate";
 /** The Constant PAGE CTA LABEL. */
 private static final String PAGE_CTA_LABEL = "pageCtaLabel";
 /** The Constant PAGE CTA URL. */
 private static final String PAGE_CTA_URL = "pageCtaUrl";
 /** The Constant PAGE OPEN IN NEW WINDOWE. */
 private static final String PAGE_OPEN_IN_NEW_WINDOW = "openInNewWindow";
 /** The Constant TEASER IMAGE FLAG. */
 private static final String TEASER_IMAGE_FLAG = "teaserImageFlag";
 /** The Constant TEASER COPY FLAG. */
 private static final String TEASER_COPY_FLAG = "teaserCopyFlag";
 /** The Constant TEASER LONG COPY. */
 private static final String TEASER_LONG_COPY_FLAG = "teaserLongCopyFlag";
 /** The Constant TEASER PUBLISH DATE. */
 private static final String TEASER_PUBLISHDATE_FLAG = "teaserPublishDateFlag";
 /** The Constant PAGE PARENT DETAILS FLAG. */
 private static final String PARENT_DETAILS_FLAG = "parentPageDetailsFlag";
 /** The Constant TAGS PAGE CTA LABEL. */
 private static final String TAGSPAGE_CTA_LABEL = "tagsPageCtaLabel";
 /** The Constant PAGE CTA LABEL. */
 private static final String TAGS_PAGE_OPEN_IN_NEW_WINDOW = "tagsPageOpenInNewWindow";
 /** The Constant PAGE CTA LABEL. */
 private static final String CONTENT_TYPE = "contentType";
 /** The Constant VIDEO_ID. */
 private static final String VIDEO_ID = "videoId";
 /**
  * logger object.
  **/
 
 @Reference
 ConfigurationService configurationService;
 
 /** The search service. */
 @Reference
 SearchService searchService;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(FeaturedContentViewHelperImpl.class);
 
 /** The Constant FOREGROUND_IMAGE. */
 private static final String FOREGROUND_IMAGE = "foregroundImage";
 
 /** The Constant ALT_FOREGROUND. */
 private static final String ALT_FOREGROUND = "altForegroundImage";
 
 /** The Constant MODE. */
 private static final String MODE = "mode";
 private static final String DATE_FORMATTER = "yyyy-MM-dd'T'HH:mm:ss";
 
 /**
  * The method processData overrides method is responsible validating author input and returning a map Map can be accessed in JSP in consistent
  * way.This method also validates the link url provided by author.
  * 
  * @param content
  *         , - {@link Map} of all user input received as input <code>ViewHelper.onGetData</code> method
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return {@link Map} holding the data
  */
 @Override
 @SuppressWarnings("unchecked")
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.info("FeaturedContent View Helper #processData called for processing fixed list.");
  Map<String, Object> properties = (Map<String, Object>) content.get(PROPERTIES);
  Map<String, Object> featuredContentFinalMap = new LinkedHashMap<String, Object>();
  ResourceResolver resourceResolver = getResourceResolver(resources);
  
  String jsonNameSpace = MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE);
  String featuredContentType = MapUtils.getString(properties, FEATURED_CONTENT_TYPE);
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  
  SlingHttpServletRequest slingRequest = getSlingRequest(resources);
  Resource compResource = slingRequest.getResource();
  Page currentPage = pageManager.getContainingPage(compResource);
  
  Map<String, String> configMap = ComponentUtil.getConfigMap(currentPage, GLOBAL_CONFIG_CAT);
  if (MANUAL_CONTENT.equals(featuredContentType)) {
   featuredContentFinalMap = getValuesManualContent(resourceResolver, properties, currentPage, pageManager, getSlingRequest(resources), resources);
  } else if (MAUNUALLY_SELECT_PAGE.equals(featuredContentType)) {
   featuredContentFinalMap = getValuesForManualSelectPage(resourceResolver, properties, currentPage, pageManager, configMap,
     getSlingRequest(resources), resources);
  } else if (IDENTIFY_PAGE_BY_TAGS.equals(featuredContentType)) {
   featuredContentFinalMap = getPageByTagSearch(resourceResolver, properties, currentPage, pageManager, configMap, getSlingRequest(resources),
     resources);
  }
  setVideoControlDetails(properties, featuredContentFinalMap, featuredContentType, currentPage);
  Map<String, Object> dataManual = new LinkedHashMap<String, Object>();
  dataManual.put(jsonNameSpace, featuredContentFinalMap);
  LOGGER.debug("created final data Map used by featured Content, size of this map is " + dataManual.size());
  return dataManual;
  
 }
 
 /**
  * sets video control map
  * 
  * @param properties
  * @param featuredContentFinalMap
  * @param featuredContentType
  * @param slingRequest
  * @param currentPage
  */
 private void setVideoControlDetails(Map<String, Object> properties, Map<String, Object> featuredContentFinalMap, String featuredContentType,
  Page currentPage) {
  if (StringUtils.isNotBlank(featuredContentType)) {
   boolean includeVideo = MapUtils.getBooleanValue(properties, "includeVideo", false);
   String videoSource = MapUtils.getString(properties, "videoSourceType", "youtube");
   String videoId = MapUtils.getString(properties, VIDEO_ID, StringUtils.EMPTY);
   featuredContentFinalMap.put("includeVideo", includeVideo);
   featuredContentFinalMap.put("videoSource", videoSource);
   featuredContentFinalMap.put(VIDEO_ID, MapUtils.getString(properties, VIDEO_ID, StringUtils.EMPTY));
   featuredContentFinalMap.put(UnileverConstants.CONTAINER_TAG, ComponentUtil.getVideoContainerTagMap(currentPage, videoId));
   featuredContentFinalMap.put("videoControls", VideoHelper.getVideoControls(properties, videoSource));
  }
 }
 
 /**
  * gets the manual selected page attributes map
  * 
  * @param resourceResolver
  * @param properties
  * @param currentPage
  * @param slingHttpServletRequest
  * @return
  */
 private Map<String, Object> getValuesForManualSelectPage(ResourceResolver resourceResolver, Map<String, Object> properties, Page currentPage,
   PageManager pageManager, Map<String, String> configMap, SlingHttpServletRequest slingHttpServletRequest, Map<String, Object> resources) {
  Map<String, Object> finalProperties = new LinkedHashMap<String, Object>();
  String manualSelectedPage = MapUtils.getString(properties, PAGE_CTA_URL, StringUtils.EMPTY);
  Page currentLandingPage = pageManager.getContainingPage(manualSelectedPage);
  if (StringUtils.isNotBlank(manualSelectedPage)) {
   setPagePropertiesAndOverrideGlobalConfig(resourceResolver, properties, currentPage, configMap, finalProperties, currentLandingPage,
     slingHttpServletRequest);
  }
  commonProperties(finalProperties, properties, currentPage, slingHttpServletRequest);
  getCtaMap(properties, finalProperties, resourceResolver, MapUtils.getString(properties, PAGE_CTA_URL, StringUtils.EMPTY), PAGE_CTA_LABEL,
    PAGE_OPEN_IN_NEW_WINDOW, slingHttpServletRequest);
  List<Map<String, Object>> tagList = new LinkedList<Map<String, Object>>();
  Tag[] currentLandingPageTags = (currentLandingPage != null) ? currentLandingPage.getTags() : new Tag[] {};
  if (StringUtils.isNotBlank(currentLandingPageTags.toString())) {
   for (Tag individualTag : currentLandingPageTags) {
    Map<String, Object> tagMap = new LinkedHashMap<String, Object>();
    tagMap.put(LABEL, individualTag.getTitle(currentLandingPage.getLanguage(false)));
    tagMap.put(VALUE, individualTag.getTagID());
    tagList.add(tagMap);
   }
  }
  finalProperties.put(TAGS, tagList.toArray());
  String[] featureTagNameSpaces = StringUtils.isNotBlank(MapUtils.getString(properties, FEATURE_TAGS)) ? ComponentUtil.getPropertyValueArray(
    properties, FEATURE_TAGS) : new String[] {};
  LandingPageListHelper.addFeatureTagsData(featureTagNameSpaces, finalProperties, currentLandingPage, resources);
  return finalProperties;
 }
 
 /**
  * @param resourceResolver
  * @param properties
  * @param currentPage
  * @param configMap
  * @param finalProperties
  * @param currentLandingPage
  * @param slingHttpServletRequest
  */
 private void setPagePropertiesAndOverrideGlobalConfig(ResourceResolver resourceResolver, Map<String, Object> properties, Page currentPage,
   Map<String, String> configMap, Map<String, Object> finalProperties, Page currentLandingPage, SlingHttpServletRequest slingHttpServletRequest) {
  ComponentUtil.setPageProperties(finalProperties, currentLandingPage, currentPage, slingHttpServletRequest);
  setFormattedDate(finalProperties, currentLandingPage);
  setOverridenLandingAttribute(properties, finalProperties, configMap, currentLandingPage, resourceResolver, slingHttpServletRequest);
 }
 
 /**
  * gets the formatted date
  * 
  * @param finalProperties
  * @param currentLandingPage
  */
 private void setFormattedDate(Map<String, Object> finalProperties, Page currentLandingPage) {
  GregorianCalendar date = null;
  if (currentLandingPage != null) {
   if (MapUtils.getObject(currentLandingPage.getProperties(), UnileverConstants.PUBLISH_DATE) instanceof GregorianCalendar) {
    date = (GregorianCalendar) MapUtils.getObject(currentLandingPage.getProperties(), UnileverConstants.PUBLISH_DATE);
   } else {
    SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMATTER);
    String publishDaate = MapUtils.getString(currentLandingPage.getProperties(), UnileverConstants.PUBLISH_DATE, StringUtils.EMPTY);
    try {
     Date dateObject = dateFormat.parse(publishDaate);
     Calendar calendar = Calendar.getInstance();
     calendar.setTime(dateObject);
     date = (GregorianCalendar) calendar;
    } catch (ParseException e) {
     LOGGER.warn("Date is not parsable due to incorrect format");
    }
   }
  }
  String publishDate = (date != null) ? ComponentUtil.getFormattedDateString(date, "dd/MM/YYYY") : StringUtils.EMPTY;
  finalProperties.put(UnileverConstants.PUBLISH_DATE, publishDate);
 }
 
 /**
  * setting enable for teaser title,copy,image and publish date
  * 
  * @param properties
  * @param finalProperties
  * @param slingHttpServletRequest
  */
 private void setOverridenLandingAttribute(Map<String, Object> properties, Map<String, Object> finalProperties, Map<String, String> configMap,
   Page currentPage, ResourceResolver resourceResolver, SlingHttpServletRequest slingHttpServletRequest) {
  boolean overrideGlobalConfig = MapUtils.getBooleanValue(properties, OVERRIDE_GLOBAL_CONFIG, false);
  boolean teaserImageFlag = MapUtils.getBooleanValue(configMap, TEASER_IMAGE_FLAG, true);
  boolean teasetCopyFlag = MapUtils.getBooleanValue(configMap, TEASER_COPY_FLAG, false);
  boolean teaserLongCopyFlag = MapUtils.getBooleanValue(configMap, TEASER_LONG_COPY_FLAG, false);
  boolean publishDateFlag = MapUtils.getBooleanValue(configMap, TEASER_PUBLISHDATE_FLAG, false);
  boolean parentPageDetails = MapUtils.getBooleanValue(configMap, PARENT_DETAILS_FLAG, false);
  if (overrideGlobalConfig) {
   teaserImageFlag = MapUtils.getBooleanValue(properties, TEASER_IMAGE_FLAG, false);
   teasetCopyFlag = MapUtils.getBooleanValue(properties, TEASER_COPY_FLAG, false);
   teaserLongCopyFlag = MapUtils.getBooleanValue(properties, TEASER_LONG_COPY_FLAG, false);
   publishDateFlag = MapUtils.getBooleanValue(properties, TEASER_PUBLISHDATE_FLAG, false);
   parentPageDetails = MapUtils.getBooleanValue(properties, PARENT_DETAILS_FLAG, false);
  }
  Map<String, Object> parentPageMap = new LinkedHashMap<String, Object>();
  if (currentPage != null && currentPage.getParent() != null) {
   parentPageMap.put(UnileverConstants.URL, ComponentUtil.getFullURL(resourceResolver, currentPage.getParent().getPath(), slingHttpServletRequest));
   parentPageMap.put(UnileverConstants.TITLE, currentPage.getParent().getTitle());
   ComponentUtil.setPageProperties(parentPageMap, currentPage.getParent(), currentPage, slingHttpServletRequest);
   setFormattedDate(parentPageMap, currentPage.getParent());
   setLandingAttributes(teaserImageFlag, parentPageMap, UnileverConstants.TEASER_IMAGE, parentPageDetails);
   setLandingAttributes(teasetCopyFlag, parentPageMap, UnileverConstants.TEASER_COPY, parentPageDetails);
   setLandingAttributes(teaserLongCopyFlag, parentPageMap, UnileverConstants.TEASER_LONG_COPY, parentPageDetails);
   setLandingAttributes(publishDateFlag, parentPageMap, UnileverConstants.PUBLISH_DATE, parentPageDetails);
   finalProperties.put(PARENT, parentPageMap);
  }
  setLandingAttributes(teaserImageFlag, finalProperties, UnileverConstants.TEASER_IMAGE, true);
  setLandingAttributes(teasetCopyFlag, finalProperties, UnileverConstants.TEASER_COPY, true);
  setLandingAttributes(teaserLongCopyFlag, finalProperties, UnileverConstants.TEASER_LONG_COPY, true);
  setLandingAttributes(publishDateFlag, finalProperties, UnileverConstants.PUBLISH_DATE, true);
 }
 
 /**
  * overriding laning attributes map according to user inputs
  * 
  * @param teaserFlag
  * @param finalProperties
  * @param key
  * @param parentPageDetailsFlag
  */
 private void setLandingAttributes(boolean teaserFlag, Map<String, Object> finalProperties, String key, boolean parentPageDetails) {
  Map<String, Object> teaserImageMap = new LinkedHashMap<String, Object>();
  teaserImageMap.put(UnileverConstants.URL, StringUtils.EMPTY);
  if (!parentPageDetails) {
   if (key.equals(UnileverConstants.TEASER_IMAGE)) {
    finalProperties.put(key, teaserImageMap);
   } else {
    finalProperties.put(key, StringUtils.EMPTY);
   }
  } else if (!teaserFlag && key.equals(UnileverConstants.TEASER_IMAGE) && finalProperties.get(key) != null) {
   finalProperties.put(key, teaserImageMap);
  } else if (finalProperties.get(key) != null && !teaserFlag) {
   finalProperties.put(key, StringUtils.EMPTY);
  }
 }
 
 /**
  * Gets the page based on tag based search
  * 
  * @param resourceResolver
  * @param properties
  * @param currentPage
  * @param pageManager
  * @param slingHttpServletRequest
  * @return page details map
  */
 private Map<String, Object> getPageByTagSearch(ResourceResolver resourceResolver, Map<String, Object> properties, Page currentPage,
   PageManager pageManager, Map<String, String> configMap, SlingHttpServletRequest slingHttpServletRequest, Map<String, Object> resources) {
  Map<String, Object> finalProperties = new LinkedHashMap<String, Object>();
  List<String> propertyNames = new ArrayList<String>();
  propertyNames.add(CONTENT_TYPE);
  List<String> propValueList = new ArrayList<String>();
  List<Map<String, String>> contentTypeList = com.sapient.platform.iea.aem.core.utils.CollectionUtils.convertMultiWidgetToList(properties,
    propertyNames);
  for (Map<String, String> contentType : contentTypeList) {
   if (StringUtils.isNotBlank(MapUtils.getString(contentType, CONTENT_TYPE))) {
    propValueList.add(MapUtils.getString(contentType, CONTENT_TYPE));
   }
  }
  if (CollectionUtils.isEmpty(propValueList)) {
   propValueList.add(MapUtils.getString(configMap, CONTENT_TYPE, StringUtils.EMPTY));
  }
  Map<String, List<String>> propMap = new HashMap<String, List<String>>();
  propMap.put(UnileverConstants.JCR_CONTENT_CONTENT_TYPE, propValueList);
  List<String> pagePathList = TaggingComponentsHelper.getSortedPagePathList(properties, currentPage, pageManager, configMap, propMap,
    resourceResolver, searchService, resources);
  String pagePath = (CollectionUtils.isNotEmpty(pagePathList)) ? pagePathList.get(0) : StringUtils.EMPTY;
  setPagePropertiesAndOverrideGlobalConfig(resourceResolver, properties, pageManager.getContainingPage(pagePath), configMap, finalProperties,
    pageManager.getContainingPage(pagePath), slingHttpServletRequest);
  getCtaMap(properties, finalProperties, resourceResolver, pagePath, TAGSPAGE_CTA_LABEL, TAGS_PAGE_OPEN_IN_NEW_WINDOW, slingHttpServletRequest);
  commonProperties(finalProperties, properties, currentPage, slingHttpServletRequest);
  String[] featureTagNameSpaces = StringUtils.isNotBlank(MapUtils.getString(properties, FEATURE_TAGS)) ? ComponentUtil.getPropertyValueArray(
    properties, FEATURE_TAGS) : new String[] {};
  LandingPageListHelper.addFeatureTagsData(featureTagNameSpaces, finalProperties, pageManager.getContainingPage(pagePath), resources);
  return finalProperties;
  
 }
 
 /**
  * Gets the values for manually authored.
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param properties
  *         the properties
  * @param currentPage
  *         the current page
  * @param pageManager
  *         the page manager
  * @param slingHttpServletRequest
  * @param jsonNameSpace
  *         the json name space
  * 
  * @return the values for page type
  */
 private Map<String, Object> getValuesManualContent(ResourceResolver resourceResolver, Map<String, Object> properties, Page currentPage,
   PageManager pageManager, SlingHttpServletRequest slingHttpServletRequest, Map<String, Object> resources) {
  Map<String, Object> finalProperties = new LinkedHashMap<String, Object>();
  String heading = MapUtils.getString(properties, HEADING);
  String shortSubHeading = MapUtils.getString(properties, SHORT_SUB_HEADING);
  String longSubHeading = MapUtils.getString(properties, TEXT);
  finalProperties.put(UnileverConstants.TEASER_TITLE, heading);
  finalProperties.put(UnileverConstants.TEASER_COPY, shortSubHeading);
  finalProperties.put(UnileverConstants.TEASER_LONG_COPY, longSubHeading);
  String foregroundImagePath = MapUtils.getString(properties, FOREGROUND_IMAGE, StringUtils.EMPTY);
  String foregroundImageAltText = MapUtils.getString(properties, ALT_FOREGROUND, StringUtils.EMPTY);
  Image image = new Image(foregroundImagePath, foregroundImageAltText, foregroundImageAltText, currentPage, slingHttpServletRequest);
  Map<String, Object> foregroundImageMap = image.convertToMap();
  if (MapUtils.isNotEmpty(foregroundImageMap)) {
   finalProperties.put(UnileverConstants.TEASER_IMAGE, foregroundImageMap);
  }
  // adding common properties map
  commonProperties(finalProperties, properties, currentPage, slingHttpServletRequest);
  // adding cta map
  getCtaMap(properties, finalProperties, resourceResolver, MapUtils.getString(properties, CTA_URL, StringUtils.EMPTY), CTA_TEXT, NEW_WINDOW,
    slingHttpServletRequest);
  Page productPage = pageManager.getContainingPage(MapUtils.getString(properties, CTA_URL, StringUtils.EMPTY));
  String[] featureTagNameSpaces = StringUtils.isNotBlank(MapUtils.getString(properties, FEATURE_TAGS)) ? ComponentUtil.getPropertyValueArray(
    properties, FEATURE_TAGS) : new String[] {};
  LandingPageListHelper.addFeatureTagsData(featureTagNameSpaces, finalProperties, productPage, resources);
  LOGGER.debug("created Map for getting page type properties used by spotlight, size of this map is " + finalProperties.size());
  return finalProperties;
  
 }
 
 /**
  * Common properties.
  * 
  * @param finalProperties
  *         the final properties
  * @param properties
  *         the featured category properties
  * @param slingHttpServletRequest
  * @return the map
  */
 private Map<String, Object> commonProperties(Map<String, Object> finalProperties, Map<String, Object> properties, Page currentPage,
   SlingHttpServletRequest slingHttpServletRequest) {
  String spotlightType = MapUtils.getString(properties, FEATURED_CONTENT_TYPE);
  finalProperties.put(MODE, StringUtils.isNotBlank(spotlightType) ? spotlightType : MANUAL_CONTENT);
  Map<String, Object> backgroundImageMap = getImageProperties(properties, currentPage, slingHttpServletRequest);
  if (MapUtils.isNotEmpty(backgroundImageMap)) {
   finalProperties.put(FEATURED_BACKGROUND, backgroundImageMap);
  }
  GregorianCalendar startDate = (GregorianCalendar) MapUtils.getObject(properties, START_DATE);
  finalProperties.put(START_DATE, DAMAssetUtility.getEffectiveDateFromCalendar(startDate));
  GregorianCalendar endDate = (GregorianCalendar) MapUtils.getObject(properties, END_DATE);
  finalProperties.put(END_DATE, DAMAssetUtility.getEffectiveDateFromCalendar(endDate));
  LOGGER.debug("created Map for common fields used by spotlight, size of this map is " + finalProperties.size());
  return finalProperties;
  
 }
 
 /**
  * This method is used to get background image properties like path, alt text for image, extension etc.
  * 
  * @param properties
  *         the properties
  * @param page
  *         the page
  * @param slingHttpServletRequest
  * @return the image properties
  */
 private Map<String, Object> getImageProperties(Map<String, Object> properties, Page page, SlingHttpServletRequest slingHttpServletRequest) {
  Map<String, Object> backgroundImageMap = new LinkedHashMap<String, Object>();
  String altImage = MapUtils.getString(properties, ALT_BACKGROUND, StringUtils.EMPTY);
  String backgroundImage = MapUtils.getString(properties, FEATURED_BACKGROUND, StringUtils.EMPTY);
  Image productBackgroundImage = new Image(backgroundImage, altImage, altImage, page, slingHttpServletRequest);
  backgroundImageMap = productBackgroundImage.convertToMap();
  return backgroundImageMap;
  
 }
 
 /**
  * Gets the cta map.
  * 
  * @param properties
  *         the properties
  * @param finalProperties
  *         the final properties
  * @param resourceResolver
  *         the resource resolver
  * @param landingPage
  *         the landing page
  * @return the cta map
  */
 private Map<String, Object> getCtaMap(Map<String, Object> properties, Map<String, Object> finalProperties, ResourceResolver resourceResolver,
   String landingPage, String ctaLabel, String openInNewWindow, SlingHttpServletRequest slingRequest) {
  Map<String, Object> ctaMap = new LinkedHashMap<String, Object>();
  String ctaUrl = StringUtils.isNotBlank(landingPage) ? ComponentUtil.getFullURL(resourceResolver, landingPage, slingRequest) : StringUtils.EMPTY;
  
  if (StringUtils.isNotBlank(ctaUrl)) {
   ctaMap.put(LABEL_TEXT, MapUtils.getString(properties, ctaLabel, StringUtils.EMPTY));
   ctaMap.put(CTA_URL, ctaUrl);
  }
  ctaMap.put(OPEN_IN_NEW_WINDOW, MapUtils.getBoolean(properties, openInNewWindow, false));
  finalProperties.put(CTA, ctaMap);
  LOGGER.debug("created Map for cta fields used by spotlight, size of this map is " + finalProperties.size());
  return finalProperties;
 }
 
 /**
  * Gets the search service.
  * 
  * @return the search service
  */
 protected SearchService getSearchService() {
  
  return this.searchService;
 }
 
}
