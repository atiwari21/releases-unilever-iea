/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.foundation.components.helper;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * The Class ProductBundlingToBinUtil.
 */
public final class ProductBundlingToBinUtil {
    
    private static final String MULTI_BUY_PRODUCTS_CTA_LABELS = "multiBuyProductsCtaLabels";
    private static final String MULTI_BUY_PRODUCTS_CONFIG = "multiBuyProductsConfig";
    private static final String ADD_MULTIPLE_PRODUCTS_TO_BAG_CTA_LABEL = "addMultipleProductsToBagCtaLabel";
    private static final String PRODUCT_MULTI_BUY_ADD_MULTIPLE_PRODUCTS_TO_BAG_CTA_LABEL = "productMultiBuy.addMultipleProductsToBagCtaLabel";
    private static final String ADD_SINGLE_PRODUCT_TO_BAG_CTA_LABEL = "addSingleProductToBagCtaLabel";
    private static final String PRODUCT_MULTI_BUY_ADD_SINGLE_PRODUCT_TO_BAG_CTA_LABEL = "productMultiBuy.addSingleProductToBagCtaLabel";
    private static final String NO_PRODUCTS_SELECTED_CTA_LABEL = "noProductsSelectedCtaLabel";
    private static final String PRODUCT_MULTI_BUY_NO_PRODUCTS_SELECTED_CTA_LABEL = "productMultiBuy.noProductsSelectedCtaLabel";
    private static final String ADD_TO_BAG_HEADING_TEXT = "addToBagHeadingText";
    private static final String PRODUCT_MULTI_BUY_ADD_TO_BAG_HEADING_TEXT = "productMultiBuy.addToBagHeadingText";
    private static final String NOT_AVAILABLE_CTA_LABEL = "NotAvailableCtaLabel";
    private static final String PRODUCT_MULTI_BUY_NOT_AVAILABLE_CTA_LABEL = "productMultiBuy.NotAvailableCtaLabel";
    private static final String REMOVE_PRODUCT_CTA_LABEL = "removeProductCtaLabel";
    private static final String PRODUCT_MULTI_BUY_REMOVE_PRODUCT_CTA_LABEL = "productMultiBuy.removeProductCtaLabel";
    private static final String ADD_PRODUCT_CTA_LABEL = "addProductCtaLabel";
    private static final String PRODUCT_MULTI_BUY_ADD_PRODUCT_CTA_LABEL = "productMultiBuy.addProductCtaLabel";
    private static final String MULTI_BUY_DEFAULT_PRODUCT_SELECTION = "multiBuyDefaultProductSelection";
    private static final String MULTI_BUY_ALLOW_ADD_REMOVE = "multiBuyAllowAddRemove";
    private static final String MULTI_BUY_ENABLED = "multiBuyEnabled";
    private static final String MULTI_BUY_SUPPORTED = "multiBuySupported";
    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductBundlingToBinUtil.class);
    
    /**
     * Instantiates a new ProductBundlingToBin util.
     */
    private ProductBundlingToBinUtil() {
        
    }
    
    /**
     * gets the product page paths based on multiple buy supported config.
     *
     * @param productsPagePathList the products page path list
     * @param currentPage the current page
     * @param configurationService the configuration service
     * @return list of product pages
     */
    public static List<String> getProductsListForMultipleBuy(List<String> productsPagePathList, Page currentPage,
            ConfigurationService configurationService) {
        List<String> multipleProductsPagePathList = productsPagePathList;
        String multiBuySupported = getMultiBuySupported(currentPage, configurationService);
        if (ProductHelper.getUniProduct(currentPage) != null && ProductConstants.TRUE.equals(multiBuySupported) && !multipleProductsPagePathList.contains(currentPage.getPath())) {
            List<String> productsPagesList = productsPagePathList;
            multipleProductsPagePathList = new LinkedList<String>();
            multipleProductsPagePathList.add(currentPage.getPath());
            for (String pagePath : productsPagesList) {
                multipleProductsPagePathList.add(pagePath);
            }
        }
        LOGGER.debug("product list size is:" + multipleProductsPagePathList.size());
        return multipleProductsPagePathList;
    }

    /**returns string true if multiBuySupported in enabled
     * @param currentPage
     * @param configurationService
     * @return
     */
    private static String getMultiBuySupported(Page currentPage, ConfigurationService configurationService) {
        String serviceProviderName = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
                ProductConstants.SHOPNOW, ProductConstants.SERVICE_PROVIDER_NAME);
        String multiBuySupported = ProductConstants.FALSE;
        if (StringUtils.isNotBlank(serviceProviderName)) {
            multiBuySupported = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, serviceProviderName,
                    MULTI_BUY_SUPPORTED);
        }
        return multiBuySupported;
    }
    
    /**
     * sets multiBuy properties to resource json map.
     *
     * @param resourceProperties the resource properties
     * @param finalProperties the final properties
     * @param configMap the config map
     * @param overrideGlobalConfig the override global config
     * @param configurationService the configuration service
     * @param currentPage the current page
     */
    public static void setMultiBuyProductsConfigProperties(Map<String, Object> resourceProperties, Map<String, Object> finalProperties,
            Map<String, String> configMap, boolean overrideGlobalConfig, ConfigurationService configurationService, Page currentPage) {
        String multiBuyEnabled = MapUtils.getString(configMap, MULTI_BUY_ENABLED, ProductConstants.FALSE);
        String multiBuyAllowAddRemove = MapUtils.getString(configMap, MULTI_BUY_ALLOW_ADD_REMOVE, ProductConstants.FALSE);
        String multiBuyDefaultProductSelection = MapUtils.getString(configMap, MULTI_BUY_DEFAULT_PRODUCT_SELECTION, ProductConstants.FALSE);
        if (overrideGlobalConfig) {
            multiBuyEnabled = MapUtils.getString(resourceProperties, MULTI_BUY_ENABLED, ProductConstants.FALSE);
            multiBuyAllowAddRemove = MapUtils.getString(resourceProperties, MULTI_BUY_ALLOW_ADD_REMOVE, ProductConstants.FALSE);
            multiBuyDefaultProductSelection = MapUtils.getString(resourceProperties, MULTI_BUY_DEFAULT_PRODUCT_SELECTION, ProductConstants.FALSE);
        }
        Map<String, Object> multiBuyProductsConfig = new LinkedHashMap<String, Object>();
        multiBuyProductsConfig.put(MULTI_BUY_SUPPORTED, Boolean.parseBoolean(getMultiBuySupported(currentPage, configurationService)));
        multiBuyProductsConfig.put(MULTI_BUY_ENABLED, Boolean.parseBoolean(multiBuyEnabled));
        multiBuyProductsConfig.put(MULTI_BUY_ALLOW_ADD_REMOVE, Boolean.parseBoolean(multiBuyAllowAddRemove));
        multiBuyProductsConfig.put(MULTI_BUY_DEFAULT_PRODUCT_SELECTION, Boolean.parseBoolean(multiBuyDefaultProductSelection));
        finalProperties.put(MULTI_BUY_PRODUCTS_CONFIG, multiBuyProductsConfig);
    }
    
    /**
     * sets multi buy cta labels to resource json map
     * 
     * @param resources
     * @param finalProperties
     */
    public static void setMultiBuyProductsCtaLabels(final Map<String, Object> resources, Map<String, Object> finalProperties) {
        I18n i18n = BaseViewHelper.getI18n(resources);
        Map<String, Object> multiBuyProductsLabels = new LinkedHashMap<String, Object>();
        multiBuyProductsLabels.put(ADD_PRODUCT_CTA_LABEL, i18n.get(PRODUCT_MULTI_BUY_ADD_PRODUCT_CTA_LABEL));
        multiBuyProductsLabels.put(REMOVE_PRODUCT_CTA_LABEL, i18n.get(PRODUCT_MULTI_BUY_REMOVE_PRODUCT_CTA_LABEL));
        multiBuyProductsLabels.put(NOT_AVAILABLE_CTA_LABEL, i18n.get(PRODUCT_MULTI_BUY_NOT_AVAILABLE_CTA_LABEL));
        multiBuyProductsLabels.put(ADD_TO_BAG_HEADING_TEXT, i18n.get(PRODUCT_MULTI_BUY_ADD_TO_BAG_HEADING_TEXT));
        multiBuyProductsLabels.put(NO_PRODUCTS_SELECTED_CTA_LABEL, i18n.get(PRODUCT_MULTI_BUY_NO_PRODUCTS_SELECTED_CTA_LABEL));
        multiBuyProductsLabels.put(ADD_SINGLE_PRODUCT_TO_BAG_CTA_LABEL, i18n.get(PRODUCT_MULTI_BUY_ADD_SINGLE_PRODUCT_TO_BAG_CTA_LABEL));
        multiBuyProductsLabels.put(ADD_MULTIPLE_PRODUCTS_TO_BAG_CTA_LABEL, i18n.get(PRODUCT_MULTI_BUY_ADD_MULTIPLE_PRODUCTS_TO_BAG_CTA_LABEL));
        finalProperties.put(MULTI_BUY_PRODUCTS_CTA_LABELS, multiBuyProductsLabels);
    }
    
    /**
     * sets multi buy cta labels to resource json map.
     *
     * @param httpServletRequest the http servlet request
     * @param finalProperties the final properties
     * @param currentPage the current page
     */
    public static void setMultiBuyProductsCtaLabels(SlingHttpServletRequest httpServletRequest, Map<String, Object> finalProperties, Page currentPage) {
        I18n i18n = BaseViewHelper.getI18n(httpServletRequest, currentPage);
        Map<String, Object> multiBuyProductsLabels = new LinkedHashMap<String, Object>();
        multiBuyProductsLabels.put(ADD_PRODUCT_CTA_LABEL, i18n.get(PRODUCT_MULTI_BUY_ADD_PRODUCT_CTA_LABEL));
        multiBuyProductsLabels.put(REMOVE_PRODUCT_CTA_LABEL, i18n.get(PRODUCT_MULTI_BUY_REMOVE_PRODUCT_CTA_LABEL));
        multiBuyProductsLabels.put(NOT_AVAILABLE_CTA_LABEL, i18n.get(PRODUCT_MULTI_BUY_NOT_AVAILABLE_CTA_LABEL));
        multiBuyProductsLabels.put(ADD_TO_BAG_HEADING_TEXT, i18n.get(PRODUCT_MULTI_BUY_ADD_TO_BAG_HEADING_TEXT));
        multiBuyProductsLabels.put(NO_PRODUCTS_SELECTED_CTA_LABEL, i18n.get(PRODUCT_MULTI_BUY_NO_PRODUCTS_SELECTED_CTA_LABEL));
        multiBuyProductsLabels.put(ADD_SINGLE_PRODUCT_TO_BAG_CTA_LABEL, i18n.get(PRODUCT_MULTI_BUY_ADD_SINGLE_PRODUCT_TO_BAG_CTA_LABEL));
        multiBuyProductsLabels.put(ADD_MULTIPLE_PRODUCTS_TO_BAG_CTA_LABEL, i18n.get(PRODUCT_MULTI_BUY_ADD_MULTIPLE_PRODUCTS_TO_BAG_CTA_LABEL));
        finalProperties.put(MULTI_BUY_PRODUCTS_CTA_LABELS, multiBuyProductsLabels);
    }
}
