/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.constants;

/**
 * This class holds all the constants that are used in the SocialMediaGallery.
 */
public class SocialMediaGalleryConstants {
 
 /** The Constant GENERAL_CONFIG_HEADING_TEXT. */
 public static final String GENERAL_CONFIG_HEADING_TEXT = "headingText";
 
 public static final String BUY_IT_NOW_LABEL = "butItNowLabel";
 
 /** The Constant GENERAL_CONFIG_SUB_HEADING_TEXT. */
 public static final String GENERAL_CONFIG_SUB_HEADING_TEXT = "subheadingText";
 
 /** The Constant GENERAL_CONFIG_LONG_HEADING_TEXT. */
 public static final String GENERAL_CONFIG_LONG_HEADING_TEXT = "longSubHeading";
 
 /** The Constant GENERAL_CONFIG_CTA_LABEL. */
 public static final String GENERAL_CONFIG_CTA_LABEL = "ctaLabel";
 
 /** The Constant GENERAL_CONFIG_CTA_URL. */
 public static final String GENERAL_CONFIG_CTA_URL = "ctaUrl";
 
 /** The Constant GENERAL_CONFIG_NEW_WINDOW. */
 public static final String GENERAL_CONFIG_NEW_WINDOW = "openInNewWindow";
 
 /** The Constant GENERAL_CONFIG_GALLERY_TYPE. */
 public static final String GENERAL_CONFIG_GALLERY_TYPE = "galleryType";
 
 /** The Constant GENERAL_CONFIG_CHANNEL_TWITTER. */
 public static final String GENERAL_CONFIG_CHANNEL_TWITTER = "twitter";
 
 /** The Constant GENERAL_CONFIG_CHANNEL_INSTAGRAM. */
 public static final String GENERAL_CONFIG_CHANNEL_INSTAGRAM = "instagram";
 
 /** The Constant GENERAL_CONFIG_CHANNEL_FACEBOOK. */
 public static final String GENERAL_CONFIG_CHANNEL_FACEBOOK = "facebook";
 
 /** The Constant GENERAL_CONFIG_CHANNEL_VIDEO. */
 public static final String GENERAL_CONFIG_CHANNEL_VIDEO = "video";
 
 /** The Constant GENERAL_CONFIG_CHANNEL_LOCAL_ASSET. */
 public static final String GENERAL_CONFIG_CHANNEL_LOCAL_ASSET = "localAsset";
 
 /** The Constant GENERAL_CONFIG_ENABLE_FILTER. */
 public static final String GENERAL_CONFIG_ENABLE_FILTER = "enableFilter";
 
 /** The Constant SOCIAL_MEDIA_GALLERY_PAGINATION_CTA_LABEL. */
 public static final String SOCIAL_MEDIA_GALLERY_PAGINATION_CTA_LABEL = "socialMediaGallery.paginationCTALabel";
 
 /** The Constant SOCIAL_MEDIA_GALLERY_SOCIAL_POST_LIKE_SUFFIX. */
 public static final String SOCIAL_MEDIA_GALLERY_SOCIAL_POST_LIKE_SUFFIX = "socialMediaGallery.socialPostLikesSuffix";
 
 /** The Constant SOCIAL_MEDIA_GALLERY_VIEW_ARTICLE_HEADING_TEXT. */
 public static final String SOCIAL_MEDIA_GALLERY_VIEW_ARTICLE_HEADING_TEXT = "socialMediaGallery.viewArticleHeadingText";
 
 /** The Constant SOCIAL_MEDIA_GALLERY_VIEW_ARTICLE_CTA_LABEL. */
 public static final String SOCIAL_MEDIA_GALLERY_VIEW_ARTICLE_CTA_LABEL = "socialMediaGallery.viewArticleCtaLabel";
 
 /** The Constant SOCIAL_MEDIA_GALLERY_VIEW_PRODUCT_HEADING_TEXT. */
 public static final String SOCIAL_MEDIA_GALLERY_VIEW_PRODUCT_HEADING_TEXT = "socialMediaGallery.viewProductHeadingText";
 
 /** The Constant SOCIAL_MEDIA_GALLERY_VIEW_PRODUCT_CTA_LABEL. */
 public static final String SOCIAL_MEDIA_GALLERY_VIEW_PRODUCT_CTA_LABEL = "socialMediaGallery.viewProductCTALabel";
 
 /** The Constant SOCIAL_MEDIA_GALLERY_DESCRIPTION_READ_MORE_LABEL. */
 public static final String SOCIAL_MEDIA_GALLERY_DESCRIPTION_READ_MORE_LABEL = "socialMediaGallery.descriptionReadMoreLabel";
 
 /** The Constant SOCIAL_MEDIA_GALLERY_DESCRIPTION_READ_LESS_LABEL. */
 public static final String SOCIAL_MEDIA_GALLERY_DESCRIPTION_READ_LESS_LABEL = "socialMediaGallery.descriptionReadLessLabel";
 
 /** The Constant SOCIAL_MEDIA_GALLERY_FILTER_KEYWORD_SEARCH_LABEL. */
 public static final String SOCIAL_MEDIA_GALLERY_FILTER_KEYWORD_SEARCH_LABEL = "socialMediaGallery.filterKeywordSearchLabel";
 
 /** The Constant SOCIAL_MEDIA_GALLERY_FILTER_KEYWORD_SEARCH_LABEL. */
 public static final String SOCIAL_MEDIA_GALLERY_FILTER_TITLE_TEXT = "socialMediaGallery.filterTitleText";
 
 /** The Constant SOCIAL_MEDIA_GALLERY_FILTER_RESET_FILTER_LABEL. */
 public static final String SOCIAL_MEDIA_GALLERY_FILTER_RESET_FILTER_LABEL = "socialMediaGallery.filterResetFilterLabel";
 
 /** The Constant SOCIAL_MEDIA_GALLERY_FILTER_SUBMIT_LABEL. */
 public static final String SOCIAL_MEDIA_GALLERY_FILTER_SUBMIT_LABEL = "socialMediaGallery.filterSubmitLabel";
 
 /** The Constant SOCIAL_MEDIA_GALLERY_SEARCH_RESULTS_HEADLINE. */
 public static final String SOCIAL_MEDIA_GALLERY_SEARCH_RESULTS_HEADLINE = "socialMediaGallery.searchResultsHeadline";
 
 /** The Constant SOCIAL_MEDIA_GALLERY_NO_SEARCH_RESULT_HEADLINE. */
 public static final String SOCIAL_MEDIA_GALLERY_NO_SEARCH_RESULT_HEADLINE = "socialMediaGallery.searchResultsNoResultsHeadline";
 
 /** The Constant SOCIAL_MEDIA_GALLERY_NO_SEARCH_RESULT_MESSAGE. */
 public static final String SOCIAL_MEDIA_GALLERY_NO_SEARCH_RESULT_MESSAGE = "socialMediaGallery.searchResultsNoResultsMessage";
 
 /** The Constant PAGINATION_KEY. */
 public static final String PAGINATION_KEY = "pagination";
 
 /** The Constant PAGINATION_TYPE. */
 public static final String PAGINATION_TYPE = "paginationType";
 
 /** The Constant ITEMS_PER_PAGE. */
 public static final String ITEMS_PER_PAGE = "itemsPerPage";
 
 /** The Constant PAGINATION_CTA_LABEL. */
 public static final String PAGINATION_CTA_LABEL = "paginationCtaLabel";
 
 /** The Constant STATIC. */
 public static final String STATIC = "static";
 
 /** The Constant AGGREGATOR_SERVICE. */
 public static final String AGGREGATOR_SERVICE = "socialMediaGalleryAggregatorService";
 
 /** The Constant URL. */
 public static final String URL = "url";
 
 /** The Constant GENERAL_CONFIG. */
 public static final String GENERAL_CONFIG = "generalConfig";
 
 /** The Constant FIXED_LIST. */
 public static final String FIXED_LIST = "fixed";
 
 /** The Constant CHANNEL_TYPE. */
 public static final String CHANNEL_TYPE = "ChannelType";
 
 /** The Constant DYNAMIC_GALLERY. */
 public static final String DYNAMIC_GALLERY = "dynamic";
 
 /** The Constant BRAND_NAME_PARAMETER. */
 public static final String BRAND_NAME_PARAMETER = "brandNameParameter";
 
 /** The Constant REQUEST_SERVLET_URL. */
 public static final String REQUEST_SERVLET_URL = "requestServletUrl";
 
 /** The Constant RETRIEVAL_QUERY_PARAMS. */
 public static final String RETRIEVAL_QUERY_PARAMS = "retrivalQueryParams";
 
 /** The Constant SOCIAL_MEDIA_GALLERY. */
 public static final String SOCIAL_MEDIA_GALLERY = "socialMediaGallery";
 
 /** The Constant LIMIT_RESULTS_TO. */
 public static final String LIMIT_RESULTS_TO = "limitResultsTo";
 
 /** The Constant EXPANDABLE_PANEL_FLAG. */
 public static final String EXPANDABLE_PANEL_FLAG = "expandablePanel";
 
 /** The Constant LIMIT. */
 public static final String LIMIT = "visualLimit";
 
 /** The Constant EXPANDABLE_PANEL. */
 public static final String EXPANDABLE_PANEL = "expandablePanel";
 
 /** The Constant NAME. */
 public static final String NAME = "name";
 
 /** The Constant URL_JSON_KEY. */
 public static final String URL_JSON_KEY = "url";
 
 /** The Constant URL_CONFIG_KEY. */
 public static final String URL_CONFIG_KEY = "Url";
 
 /** The Constant LOGO. */
 public static final String LOGO = "Logo";
 
 /** The Constant IMAGE_PATH. */
 public static final String IMAGE_PATH = "imagePath";
 
 /** The Constant ACCOUNT_ID_JSON. */
 public static final String ACCOUNT_ID_JSON = "AccountIdJson";
 
 /** The Constant ACCOUNT_ID. */
 public static final String ACCOUNT_ID = "accountId";
 
 /** The Constant HASH_TAGS. */
 public static final String HASH_TAGS = "hashTags";
 
 /** The Constant JSON. */
 public static final String JSON = "Json";
 
 /** The Constant HASH_TAG_NAME. */
 public static final String HASH_TAG_NAME = "hashTagName";
 
 /** The Constant ID. */
 public static final String ID = "id";
 
 /** The Constant HASH. */
 public static final String HASH = "#";
 
 /** The Constant IMAGE_ONLY. */
 public static final String IMAGE_ONLY = "imageOnly";
 
 /** The Constant VIDEO_ONLY. */
 public static final String VIDEO_ONLY = "videoOnly";
 
 /** The Constant TEXT_ONLY. */
 public static final String TEXT_ONLY = "textOnly";
 
 /** The Constant SEARCH_POST_BY. */
 public static final String SEARCH_POST_BY = "searchPostBy";
 
 /** The Constant ACCOUNT_IDS. */
 public static final String ACCOUNT_IDS = "accountIds";
 
 /** The Constant VIDEO_SOURCE. */
 public static final String VIDEO_SOURCE = "videoSource";
 
 /** The Constant VIDEO_TYPE. */
 public static final String VIDEO_TYPE = "videoType";
 
 /** The Constant VIDEO_ID. */
 public static final String VIDEO_ID = "videoId";
 
 /** The Constant LIST. */
 public static final String LIST = "list";
 
 /** The Constant LIST_NAME. */
 public static final String LIST_NAME = "listName";
 
 /** The Constant POST_ID. */
 public static final String POST_ID = "postId";
 
 /** The Constant FIXED_VIDEO_ID. */
 public static final String FIXED_VIDEO_ID = "fixedVideoId";
 
 /** The Constant FIXED_VIDEO_SOURCE. */
 public static final String FIXED_VIDEO_SOURCE = "fixedVideoSource";
 
 /** The Constant FIXED_VIDEO_TYPE. */
 public static final String FIXED_VIDEO_TYPE = "fixedVideoType";
 
 /** The Constant FIXED_ACCOUNT_ID. */
 public static final String FIXED_ACCOUNT_ID = "fixedAccountId";
 
 /** The Constant FIXED_POST_ID. */
 public static final String FIXED_POST_ID = "fixedPostId";
 
 /** The Constant YOU_TUBE. */
 public static final String YOU_TUBE = "youtube";
 
 /** The Constant PLAY_LIST. */
 public static final String PLAY_LIST = "playlist";
 
 /** The Constant FIRST_LABEL. */
 public static final String FIRST_LABEL = "firstLabel";
 
 /** The Constant LAST_LABEL. */
 public static final String LAST_LABEL = "lastLabel";
 
 /** The Constant PREVIOUS_LABEL. */
 public static final String PREVIOUS_LABEL = "previousLabel";
 
 /** The Constant NEXT_LABEL. */
 public static final String NEXT_LABEL = "nextLabel";
 
 /** The Constant POST_LINKED_TO_URL. */
 public static final String POST_LINKED_TO_URL = "postLinkedToUrl";
 
 /** The Constant ASSOCIATED_PAGE. */
 public static final String ASSOCIATED_PAGE = "associatedPage";
 
 /** The Constant ARTICLE. */
 public static final String ARTICLE = "article";
 
 /** The Constant PRODUCT. */
 public static final String PRODUCT = "product";
 
 /** The Constant TEASER_IMAGE. */
 public static final String TEASER_IMAGE = "teaserImage";
 
 /** The Constant TEASER_ALT_TEXT. */
 public static final String TEASER_ALT_TEXT = "teaserAltText";
 
 /** The Constant TEASER_TITLE. */
 public static final String TEASER_TITLE = "teaserTitle";
 
 /** The Constant SOCIAL_CHANNELS. */
 public static final String SOCIAL_CHANNELS = "socialChannels";
 
 /** The Constant POST_POSITION. */
 public static final String POST_POSITION = "postPosition";
 
 /** The Constant POST_CLASS. */
 public static final String POST_CLASS = "postClass";
 
 /** The Constant IS_LINK_TO_PAGE. */
 public static final String IS_LINK_TO_PAGE = "isLinkToPage";
 
 /** The Constant LINK_POST_TO_PRODUCT_OR_PAGE. */
 public static final String LINK_POST_TO_PRODUCT_OR_PAGE = "LinkPostToProductOrPage";
 
 /** The Constant OPEN_IN_NEW_WINDOW. */
 public static final String OPEN_IN_NEW_WINDOW = "openInNewWindow";
 
 /** The Constant LINK_POST_TO. */
 public static final String LINK_POST_TO = "LinkPostTo";
 
 /** The Constant LINK_TYPE. */
 public static final String LINK_TYPE = "linkType";
 
 /** The Constant CUSTOM_INTERNAL. */
 public static final String CUSTOM_INTERNAL = "customInternal";
 
 /** The Constant CUSTOM_HEADING_TEXT. */
 public static final String CUSTOM_HEADING_TEXT = "CustomLinkHeadingText";
 
 /** The Constant CUSTOM_CTA_LABEL. */
 public static final String CUSTOM_CTA_LABEL = "CustomCtaLabel";
 
 /** The Constant CUSTOM_EXTERNAL. */
 public static final String CUSTOM_EXTERNAL = "customExternal";
 
 /** The Constant CUSTOM_TEASER_TITLE. */
 public static final String CUSTOM_TEASER_TITLE = "CustomTeaserTitle";
 
 /** The Constant CUSTOM_LINK_TITLE. */
 public static final String CUSTOM_LINK_TITLE = "teaserTitle";
 
 /** The Constant CUSTOM_LINK_DESCRIPTION. */
 public static final String CUSTOM_LINK_DESCRIPTION = "teaserCopy";
 
 /** The Constant CUSTOM_TEASER_DESCRIPTION. */
 public static final String CUSTOM_TEASER_DESCRIPTION = "CustomTeaserDescription";
 
 /** The Constant CUSTOM_TEASER_IMAGE. */
 public static final String CUSTOM_TEASER_IMAGE = "CustomTeaserImage";
 
 /** The Constant CUSTOM_TEASER_IMAGE_ALT. */
 public static final String CUSTOM_TEASER_IMAGE_ALT = "CustomTeaserImageAlt";
 
 /** The Constant CUSTOM_LINK_THUMBNAIL_IMAGE. */
 public static final String CUSTOM_LINK_THUMBNAIL_IMAGE = "customLinkThumbnailImage";
 
 /** The Constant SET_CUSTOM_CHANNEL_IMAGE. */
 public static final String SET_CUSTOM_CHANNEL_IMAGE = "setCustomChannelImage";
 
 /** The Constant CHANNEL_IMAGE. */
 public static final String CHANNEL_IMAGE = "channelImage";
 
 /** The Constant CHANNEL_IMAGE_ALT. */
 public static final String CHANNEL_IMAGE_ALT = "channelImageAlt";
 
 /** The Constant TAB_IMAGE. */
 public static final String TAB_IMAGE = "tabImage";
 
 /** The Constant LINK_POST_CONFIG_JSON. */
 public static final String LINK_POST_CONFIG_JSON = "LinkPostsConfigJson";
 
 /** The Constant FIXED_LIST_POST. */
 public static final String FIXED_LIST_POST = "fixedLinkPostsConfigJson";
 
 /** The Constant FIXED_CHANNEL_TYPE. */
 public static final String FIXED_CHANNEL_TYPE = "fixedChannelType";
 
 /** The Constant SOCIAL_CHANNEL_NAMES. */
 public static final String SOCIAL_CHANNEL_NAMES = "socialChannelNames";
 
 /** The Constant TRUE. */
 public static final String TRUE = "[true]";
 
 /** The Constant FALSE. */
 public static final String FALSE = "[false]";
 
 /** The Constant IS_CHANNEL_IMAGE. */
 public static final String IS_CHANNEL_IMAGE = "isChannelImage";
 
 /** The Constant IMAGE. */
 public static final String IMAGE = "image";
 
 /** The Constant SOCIAL_CONFIGURATION. */
 public static final String SOCIAL_CONFIGURATION = "socialConfiguration";
 
 /** The Constant PAGE_LISTING_PAGINATION_FIRST. */
 public static final String PAGE_LISTING_PAGINATION_FIRST = "pageListing.pagination.first";
 
 /** The Constant PAGE_LISTING_PAGINATION_LAST. */
 public static final String PAGE_LISTING_PAGINATION_LAST = "pageListing.pagination.last";
 
 /** The Constant PAGE_LISTING_PAGINATION_PREVIOUS. */
 public static final String PAGE_LISTING_PAGINATION_PREVIOUS = "pageListing.pagination.previous";
 
 /** The Constant PAGE_LISTING_PAGINATION_NEXT. */
 public static final String PAGE_LISTING_PAGINATION_NEXT = "pageListing.pagination.next";
 
 /** The Constant FIXED_POST_TITLE. */
 public static final String FIXED_POST_TITLE = "fixedPostTitle";
 
 /** The Constant FIXED_POST_SHORT_DESCRIPTION. */
 public static final String FIXED_POST_SHORT_DESCRIPTION = "fixedPostShortDescription";
 
 /** The Constant FIXED_POST_LONG_DESCRIPTION. */
 public static final String FIXED_POST_LONG_DESCRIPTION = "fixedPostLongDescription";
 
 /** The Constant FIXED_POST_ADD_CUSTOM_TITLE_AND_DESC. */
 public static final String FIXED_POST_ADD_CUSTOM_TITLE_AND_DESC = "fixedAddCustomTitleCaption";
 
 /** The Constant ADD_CUSTOM_TITLE_DESC. */
 public static final String ADD_CUSTOM_TITLE_DESC = "addCustomTitleAndDesc";
 
 /** The Constant FIXED_LIST_SOCIAL. */
 public static final String FIXED_LIST_SOCIAL = "fixedListSocial";
 
 /** The Constant SEARCH_HEADING. */
 public static final String SEARCH_HEADING = "searchHeading";
 
 /** The Constant SEARCH_DESCRIPTION. */
 public static final String SEARCH_DESCRIPTION = "searchDescription";
 
 /** The Constant SEARCH_PLACEHOLDER. */
 public static final String SEARCH_PLACEHOLDER = "searchPlaceholder";
 
 /** The Constant FILTER_HEADING. */
 public static final String FILTER_HEADING = "filterHeading";
 
 /** The Constant FILTER_DESCRIPTION. */
 public static final String FILTER_DESCRIPTION = "filterDescription";
 
 /** The Constant ENABLE_COLLAPSE_FUNCTION. */
 public static final String ENABLE_COLLAPSE_FUNCTION = "enableCollapseFunction";
 
 /** The Constant ENABLE_SEARCH_INPUT. */
 public static final String ENABLE_SEARCH_INPUT = "enableSearchInput";
 
 /** The Constant FILTER_SEARCH_PLACEHOLDER. */
 public static final String FILTER_SEARCH_PLACEHOLDER = "filterSearchPlaceholder";
 
 /** The Constant KEYWORD_FILTERS. */
 public static final String KEYWORD_FILTERS = "keywordFilters";
 
 /** The Constant FILTER_CONFIG. */
 public static final String FILTER_CONFIG = "filterConfig";
 
 /** The Constant DESCRIPTION_READ_MORE_LABEL. */
 public static final String DESCRIPTION_READ_MORE_LABEL = "descriptionReadMoreLabel";
 
 /** The Constant DESCRIPTION_READ_LESS_LABEL. */
 public static final String DESCRIPTION_READ_LESS_LABEL = "descriptionReadLessLabel";
 
 /** The Constant FILTER_KEYWORDS_SEARCH_LABEL. */
 public static final String FILTER_KEYWORDS_SEARCH_LABEL = "filterKeywordSearchLabel";
 
 /** The Constant FILTER_RESET_FILTER_LABEL. */
 public static final String FILTER_RESET_FILTER_LABEL = "filterResetFilterLabel";
 
 /** The Constant SEARCH_RESULT_HEADLINE. */
 public static final String SEARCH_RESULT_HEADLINE = "searchResultsHeadline";
 
 /** The Constant FILTER_SUBMIT_LABEL. */
 public static final String FILTER_SUBMIT_LABEL = "filterSubmitLabel";
 
 /** The Constant NO_SEARCH_RESULT_MESSAGE. */
 public static final String NO_SEARCH_RESULT_MESSAGE = "searchResultsNoResultsMessage";
 
 /** The Constant NO_SEARCH_RESULT_HEADLINE. */
 public static final String NO_SEARCH_RESULT_HEADLINE = "noSearchResultHeadline";
 
 /** The Constant FILTER_TITLE_TEXT. */
 public static final String FILTER_TITLE_TEXT = "filterTitleText";
 
 /** The Constant ENABLE_CAROUEL_PNAEL. */
 public static final String ENABLE_CAROUEL_PNAEL = "enableNextPreviousPanel";
 
 /** The Constant ENABLE_BUT_IT_NOW. */
 public static final String ENABLE_BUY_IT_NOW = "buyItNow";
 
 /** The Constant VISUAL_OPEN_NEW_WINDOW. */
 public static final String VISUAL_OPEN_NEW_WINDOW = "visualOpenInNewWindow";
 
 /** The Constant LINK_TO_SOCIAL_CHANNEL. */
 public static final String LINK_TO_SOCIAL_CHANNEL = "linkToSocialChannel";
 
 /** The Constant SHOW_AUTHOR_DETAILS. */
 public static final String SHOW_AUTHOR_DETAILS = "showAuthorDetails";
 
 /** The Constant SHOW_AUTHOR_DETAILS. */
 public static final String EXPAND_PANEL_ON_DESCRIPTION = "enableExpandCollapseOnDescription";
 
 /** The Constant MEDIA. */
 public static final String MEDIA = "media";
 
 /** The Constant DISPLAY_OVERLAY. */
 public static final String DISPLAY_OVERLAY = "displayInOverlay";
 
 /** The Constant AUTOPLAY. */
 public static final String AUTOPLAY = "autoplay";
 
 /** The Constant HIDE_CONTROLS. */
 public static final String HIDE_CONTROLS = "hideVideoControls";
 
 /** The Constant MUTE_AUDIO. */
 public static final String MUTE_AUDIO = "muteVideoAudio";
 
 /** The Constant LOOP_VIDEO_PLAYBACK. */
 public static final String LOOP_VIDEO_PLAYBACK = "loopVideoPlayback";
 
 /** The Constant SHOW_RELATED_VIDEO_AT_END. */
 public static final String SHOW_RELATED_VIDEO_AT_END = "showRelatedVideos";
 
 /** The Constant SOCIAL_MEDIA_GALLERY_VIDEO_VIDEO_SETTINGS. */
 public static final String SOCIAL_MEDIA_GALLERY_VIDEO_VIDEO_SETTINGS = "socialMediaGalleryVideoSettings";
 
 /** The Constant SOCIAL_MEDIA_GALLERY_VIDEO_VIDEO_SETTINGS. */
 public static final String SOCIAL_MEDIA_GALLERY_VIEW_PRODUCT_BUY_IT_NOW = "socialMediaGallery.viewProductBuyItNowLabel";
 
 /** The Constant SOCIAL_MEDIA_GALLERY_VIDEO_VIDEO_SETTINGS. */
 public static final String SOCIAL_MEDIA_GALLERY_VIEW_MULTIPLE_HEADING_TEXT = "socialMediaGallery.viewMutltipleHeadingText";
 
 /** The Constant SOCIAL_POST_LIKES_SUFFIX. */
 public static final String SOCIAL_POST_LIKES_SUFFIX = "socialPostLikesSuffix";
 
 /** The Constant VIEW_MULTIPLE_HEADING_TEXT. */
 public static final String VIEW_MULTIPLE_HEADING_TEXT = "viewMutltipleHeadingText";
 
 /** The Constant FILTER_SHOW_MORE_LABEL. */
 public static final String SOCIAL_MEDIA_GALLERY_FILTER_SHOW_MORE_LABEL = "socialMediaGallery.filterShowMoreLabel";
 
 /** The Constant FILTER_SHOW_LESS_LABEL. */
 public static final String SOCIAL_MEDIA_GALLERY_FILTER_SHOW_LESS_LABEL = "socialMediaGallery.filterShowLessLabel";
 
 /** The Constant FILTER_SHOW_MORE_LABEL. */
 public static final String FILTER_SHOW_MORE_LABEL = "filterShowMoreLabel";
 
 /** The Constant FILTER_SHOW_LESS_LABEL. */
 public static final String FILTER_SHOW_LESS_LABEL = "filterShowLessLabel";
 
 /** The Constant FIXED_TAB_IMAGE. */
 public static final String FIXED_TAB_IMAGE = "fixedTabImage";
 
 /** The Constant FIXED_TAB_IMAGE_ALT. */
 public static final String FIXED_TAB_IMAGE_ALT = "fixedTabImageAlt";

}
