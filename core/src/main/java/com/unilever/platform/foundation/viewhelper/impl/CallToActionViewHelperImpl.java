/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.Resource;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * This class prepares the data map for Call To Action Component It reads the values from the dialog and passes the info as a json.
 */

@Component(description = "CallToActionViewHelperImpl", immediate = true, metatype = true, label = "Call To Action")
@Service(value = { CallToActionViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.CALL_TO_ACTION, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class CallToActionViewHelperImpl extends BaseViewHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(CallToActionViewHelperImpl.class);
 
 /** The Constant OPEN_IN_NEW_WINDOW. */
 private static final String OPEN_IN_NEW_WINDOW = "openInNewWindow";
 
 /** The Constant CTA_LABEL. */
 private static final String CTA_LABEL = "ctaLabel";
 
 /** The Constant CTA_URL. */
 private static final String CTA_URL = "ctaUrl";
 
 /** The Constant NEW_WINDOW. */
 private static final String NEW_WINDOW = "newWindow";
 
 @Reference
 ConfigurationService configurationService;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("Executing CallToActionViewHelperImpl. Inside processData method ");
  Map<String, Object> jsonProperties = new HashMap<String, Object>();
  Map<String, Object> callToActionProperties = getProperties(content);
  Resource currentResource = getCurrentResource(resources);
  String ctaUrl = StringUtils.EMPTY;
  String ctaLabel = StringUtils.EMPTY;
  
  LOGGER.info("Component found for current page {}", currentResource.getName());
  ctaUrl = MapUtils.getString(callToActionProperties, CTA_URL, StringUtils.EMPTY);
  ctaLabel = MapUtils.getString(callToActionProperties, CTA_LABEL, StringUtils.EMPTY);
  if (ctaUrl != null && !ctaUrl.isEmpty()) {
   jsonProperties.put(CTA_URL, ComponentUtil.getFullURL(getResourceResolver(resources), ctaUrl, getSlingRequest(resources)));
  } else {
   jsonProperties.put(CTA_URL, ctaUrl);
  }
  
  jsonProperties.put("ctaLabel", ctaLabel);
  jsonProperties.put(OPEN_IN_NEW_WINDOW, MapUtils.getBoolean(callToActionProperties, NEW_WINDOW, false));
  
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(getJsonNameSpace(content), jsonProperties);
  return data;
  
 }
 
}
