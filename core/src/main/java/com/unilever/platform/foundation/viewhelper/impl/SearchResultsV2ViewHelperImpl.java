/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.xss.XSSFilter;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.i18n.I18n;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfiguration;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.aem.foundation.core.service.SearchRetrivalService;
import com.unilever.platform.aem.foundation.utils.AdaptiveUtil;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.dto.SearchFilterDTO;
import com.unilever.platform.foundation.components.dto.SearchFilterOptionsDTO;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.SearchHelper;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * The Class SearchResultsV2ViewHelperImpl.
 */
@Component(description = "SearchResultsV2ViewHelperImpl", immediate = true, metatype = true, label = "SearchResultsV2ViewHelperImpl")
@Service(value = { SearchResultsV2ViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.SEARCH_RESULTS_V2, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class SearchResultsV2ViewHelperImpl extends BaseViewHelper {
 
 private static final String SUB_BRAND_NAME = "subBrandName";
 
 private static final String QUERY_PARAM_QMARK = "?";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(SearchResultsV2ViewHelperImpl.class);
 
 /** The Constant FILTER_LABEL_MAP. */
 private static final String FILTER_LABEL_MAP = "tabContentTypes";
 
 /** The Constant RESULTS_LABEL_MAP. */
 private static final String RESULTS_LABEL_MAP = "resultsLabel";
 
 /** The Constant NO_RESULTS_LABEL_MAP. */
 private static final String NO_RESULTS_LABEL_MAP = "noResultsLabel";
 
 /** The Constant SUGGESTIONS_LABEL_MAP . */
 private static final String SUGGESTIONS_LABEL_MAP = "search";
 
 /** The Constant DROPDOWN_LABEL_MAP. */
 private static final String DROPDOWN_CONNECTOR_LABEL_MAP = "dropDownLabel";
 
 /** The Constant QUERY_PARAM_MAP. */
 private static final String RETRIVAL_QUERY_PARAM_MAP = "retrivalQueryParams";
 
 /** The Constant EQUALS. */
 private static final String EQUALS = "=";
 
 /** The Constant SUGGESTION_SERVLET. */
 private static final String SUGGESTION_SERVLET = "requestServlet";
 
 /** The Constant SEARCH_LABEL. */
 private static final String SEARCH_LABEL = "label";
 
 /** The Constant PAGE_NUM. */
 private static final String PAGE_NUM = "PageNum";
 
 /** The Constant PLACEHOLDER. */
 private static final String PLACEHOLDER = "placeHolder";
 
 /** The Constant RESULTS. */
 private static final String RESULTS = "actualResults";
 
 /** The Constant RESULTS_PER_PAGE. */
 public static final String RESULTPERPAGE = "resultPerPage";
 
 /** The Constant RESULT_PER_PAGE_DEFAULT_COUNT. */
 private static final String RESULT_PER_PAGE_DEFAULT_COUNT = "10";
 
 /** The Constant COUNTRYCODE. */
 public static final String COUNTRYCODE = "countryCode";
 
 /** The Constant QUERY_STRING. */
 private static final String QUERY_STRING = "queryString";
 
 /** The Constant QUERY_PARAM. */
 private static final String QUERY_PARAM = "q";
 
 /** The Constant HEADLINE. */
 private static final String HEADLINE = "headline";
 
 /** The Constant QUERY_FLAG. */
 private static final String QUERY_FLAG = "queryFlag";
 
 /** The Constant RATING_REVIEWS. */
 private static final String RATING_REVIEWS = "ratingReviews";
 
 /** The Constant TWO. */
 private static final int TWO = 2;
 
 private static final String BAZAAR_VOICE = "bazaarvoice";
 private static final String KRITIQUE = "kritique";
 private static final String SERVICE_PROVIDER_NAME = "serviceProviderName";
 private static final String NO_RESULT_MESSAGE = "noResultMessage";
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The solr search results retrieval. */
 @Reference
 SearchRetrivalService solrSearchResultsRetrieval;
 
 /** The filter. */
 @Reference
 XSSFilter filter;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.info("inside SearchResultsV2ViewHelperImpl view helper");
  Map<String, Object> data = new HashMap<String, Object>();
  SlingHttpServletRequest slingRequest = getSlingRequest(resources);
  Map requestParamMap = slingRequest.getParameterMap();
  ResourceResolver resolver = getResourceResolver(resources);
  I18n i18n = getI18n(resources);
  Map<String, Object> properties = getProperties(content);
  boolean overrideGlobalConfig = false;
  Page currentPage = getCurrentPage(resources);
  String commaSeparatedContentTypes = requestParamMap.containsKey("cts") ? slingRequest.getParameter("cts") : StringUtils.EMPTY;
  Map<String, Object> searchResultsMap = new LinkedHashMap<String, Object>();
  //
  Map<String, String> configMapInput = ComponentUtil.getConfigMap(currentPage, SearchHelper.SEARCH_INPUT);
  Map<String, Object> configurationsMap = new HashMap<String, Object>();
  
  // If override configuration is set to true, get overriden config map or else put values from dialog
  Map<String, Object> inputProperties = null;
  if (requestParamMap.containsKey("sip")) {
   String inputPath = slingRequest.getParameter("sip");
   inputProperties = getOverridenConfig(inputPath, resolver);
   overrideGlobalConfig = MapUtils.getBoolean(inputProperties, "overrideGlobalConfig", false);
  }
  
  Map<String, String> searchConfig = new HashMap<String, String>();
  Map<String, String> productNewlaunch = new HashMap<String, String>();
  String jsonNameSpace = MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE);
  String resultPerPage = RESULT_PER_PAGE_DEFAULT_COUNT;
  Map<String, String> countryConfig = new HashMap<String, String>();
  
  String customizableTag = StringUtils.EMPTY;
  try {
   customizableTag = configurationService.getConfigValue(currentPage, UnileverConstants.EGIFTING_CONFIG_CAT, "customizableTag");
  } catch (ConfigurationNotFoundException e) {
   customizableTag = ProductConstants.FALSE;
   LOGGER.error("could not find customizableTag key in egifting category", e);
  }
  Map<String, Object> generalConfig = getGeneralConfig(resources);
  searchResultsMap.put("newText", i18n.get(ProductConstants.NEW_PRODUCT_LABEL));
  try {
   searchConfig = configurationService.getCategoryConfiguration(currentPage, "searchConfiguration");
   productNewlaunch = configurationService.getCategoryConfiguration(currentPage, "product");
   countryConfig = configurationService.getCategoryConfiguration(currentPage, "countryConfiguration");
   
   Map<String, Object> finalMap = SearchHelper.getSearchConfigurations(content, resources);
   
   searchResultsMap.put("customizableTag", customizableTag);
   searchResultsMap.put("personalizableLabel", i18n.get(ProductConstants.PERSONALIZABLE));
   searchResultsMap.put(RESULTS_LABEL_MAP, getResultsLabelMap(i18n, productNewlaunch));
   searchResultsMap.put(NO_RESULTS_LABEL_MAP, getNoResultsMap(i18n, (String) generalConfig.getOrDefault(NO_RESULT_MESSAGE, StringUtils.EMPTY)));
   generalConfig.remove(NO_RESULT_MESSAGE);
   searchResultsMap.put("generalConfig", generalConfig);
   searchResultsMap.put(DROPDOWN_CONNECTOR_LABEL_MAP, i18n.get("searchInputV2.inLabel"));
   searchResultsMap.put(ProductConstants.REVIEW_MAP, getReviewMapProperties(currentPage, searchConfig));
   searchResultsMap.put("defaultContentTypes", SearchHelper.getDefaultContentTypes(searchConfig));
   searchResultsMap.put(RETRIVAL_QUERY_PARAM_MAP, finalMap.get(SUGGESTION_SERVLET));
   finalMap.remove(SUGGESTION_SERVLET);
   searchResultsMap.put(SUGGESTIONS_LABEL_MAP,
     getAutoSuggestionLabelMap(i18n, countryConfig.get("code"), (String) finalMap.get("searchSuggestionHandler")));
   finalMap.remove("searchSuggestionHandler");
   int awardsCount = MapUtils.getIntValue(searchConfig, "awardsCount", 3);
   searchResultsMap.put("awardsCount", awardsCount);
   
   if (AdaptiveUtil.isAdaptive(slingRequest)) {
    searchResultsMap.put(RESULTS, getSearchResults(slingRequest, resultPerPage));
    searchResultsMap.put(QUERY_STRING, filter.filter(slingRequest.getParameter(QUERY_PARAM)));
    searchResultsMap.put(HEADLINE, i18n.get("search.headingtext"));
    searchResultsMap.put(QUERY_FLAG, slingRequest.getParameter(QUERY_PARAM) == null ? false : true);
   }
   Map<String, String> configMap = ComponentUtil.getConfigMap(currentPage, "contentType");
   Map<String, String> showMoreLabels = getShowMoreMap(configMap, MapUtils.getIntValue(finalMap, "perContentCount"), i18n);
   if (showMoreLabels.size() > 0) {
    searchResultsMap.put("showMore", showMoreLabels);
   }
   
   searchResultsMap.put("videoFilterPrefixText", i18n.get("searchResultsV2.videoFilterPrefixText"));
   searchResultsMap.put("videoFilterAllContentOptionText", i18n.get("searchResultsV2.videoFilterAllContentOptionText"));
   searchResultsMap.put("videoFilterVideoContentOptionText", i18n.get("searchResultsV2.videoFilterVideoContentOptionText"));
   // piece of code for displaying search filters
   searchResultsMap.put("configurations", configurationsMap);
   boolean isExternal = MapUtils.getBoolean(searchConfig, "externalSearch", false);
   searchResultsMap.put("isExternal", isExternal);
   List<Map<String, Object>> tabContentTypes = new ArrayList<Map<String, Object>>();
   if (!MapUtils.getBoolean(finalMap, SearchHelper.DISABLE_CONTENT_TYPE_FILTER, false)) {
    tabContentTypes = setTabsAndFilters(properties, resources, configMap, isExternal, searchResultsMap, commaSeparatedContentTypes);
   }
   searchResultsMap.put(FILTER_LABEL_MAP, tabContentTypes.toArray());
   searchResultsMap.put("pagination", SearchHelper.getPaginationMap(properties, getI18n(resources)));
   
   searchResultsMap.put("static", RecipeListingViewHelperImpl.getStaticData(i18n));
   searchResultsMap.put("errorMessage", i18n.get("searchListing.errorMessage"));
   setSubBrandData(requestParamMap, searchResultsMap, resolver, resources, slingRequest);
   setGlobalConfigMap(overrideGlobalConfig, configMapInput, inputProperties, configurationsMap, tabContentTypes);
   
   searchResultsMap.put("mobileFilterKey", i18n.get("searchListing.mobileFilterKey"));
   SearchHelper.setSearchTagsParams(resources, searchResultsMap);
   data.put(jsonNameSpace, searchResultsMap);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Exception in READING CONFIGURATIONS  .............." + e.getMessage());
  }
  return data;
  
 }
 
 /**
  * Gets the general config.
  * 
  * @param resources
  *         the resources
  * @return the general config
  */
 public Map<String, Object> getGeneralConfig(final Map<String, Object> resources) {
  Map<String, Object> generalConfig = new HashMap<String, Object>();
  ResourceResolver resolver;
  resolver = getResourceResolver(resources);
  TagManager tagManager = resolver.adaptTo(TagManager.class);
  ValueMap valueMap = getCurrentResource(resources).adaptTo(ValueMap.class);
  String[] featureTagsArray = ArrayUtils.EMPTY_STRING_ARRAY;
  generalConfig.put("searchHeading", valueMap.getOrDefault("searchHeading", StringUtils.EMPTY));
  generalConfig.put("searchSubHeading", valueMap.getOrDefault("searchSubHeading", StringUtils.EMPTY));
  generalConfig.put(NO_RESULT_MESSAGE, valueMap.getOrDefault(NO_RESULT_MESSAGE, StringUtils.EMPTY));
  List<Map<String, String>> featureTags = new LinkedList<Map<String, String>>();
  for (String featureTag : (String[]) valueMap.getOrDefault("featureTag", ArrayUtils.EMPTY_STRING_ARRAY)) {
   Tag featureTagNode = tagManager.resolve(featureTag);
   if (featureTagNode != null) {
    Map<String, String> featureTagObject = new HashMap<String, String>();
    String tagId = featureTagNode.getTagID();
    featureTagsArray = (String[]) ArrayUtils.add(featureTagsArray, tagId);
    String tagIdToSet = StringUtils.contains(featureTagNode.getPath(), "rms") ? featureTagNode.getTagID() : featureTagNode.getLocalTagID();
    featureTagObject.put("tagID", tagIdToSet);
    
    featureTagObject.put("title", featureTagNode.getTitle(BaseViewHelper.getLocale(BaseViewHelper.getCurrentPage(resources))));
    featureTags.add(featureTagObject);
   }
  }
  Object[] recipeArray = !featureTags.isEmpty() ? featureTags.toArray() : ArrayUtils.EMPTY_STRING_ARRAY;
  generalConfig.put("featureTags", recipeArray);
  boolean isFeatureTagInteractionTrue = ComponentUtil.isFeatureTagInteractionEnabled(BaseViewHelper.getCurrentPage(resources));
  generalConfig.put(UnileverConstants.IS_ENABLED_FEATURE_TAG_INTERACTION, isFeatureTagInteractionTrue);
  if (isFeatureTagInteractionTrue) {
   generalConfig.put("interactionFeatureTags", ComponentUtil.getFeatureTagList(resources, featureTagsArray));
  }
  return generalConfig;
 }
 
 /**
  * Gets the results label map.
  * 
  * @param i18n
  *         the i 18 n
  * @param productNewlaunch
  *         the product newlaunch
  * @return the results label map
  */
 private Map<String, Object> getResultsLabelMap(I18n i18n, Map<String, String> productNewlaunch) {
  Map<String, Object> resultMap = new HashMap<String, Object>();
  
  resultMap.put("loadMoreLabel", i18n.get("searchListing.label.seeAll"));
  resultMap.put("resultsForCopy", i18n.get("searchListing.label.resultfor"));
  resultMap.put("showingLabelCopy", i18n.get("searchListing.label.showing"));
  resultMap.put("newLaunch", productNewlaunch.get("isNewProductLaunchDateLimit"));
  return resultMap;
  
 }
 
 /**
  * Gets the no results map.
  * 
  * @param i18n
  *         the i 18 n
  * @param noResultMessage
  *         the no result message
  * @return the no results map
  */
 private Map<String, Object> getNoResultsMap(I18n i18n, String noResultMessage) {
  Map<String, Object> map = new HashMap<String, Object>();
  map.put(SEARCH_LABEL, i18n.get("searchListing.didYouMean"));
  map.put("headline", i18n.get("searchListing.noresult.suggestion"));
  map.put("description", noResultMessage);
  map.put("tagDescription", i18n.get("searchListing.NoFilterMessage"));
  map.put("noResultCategoryMessage", i18n.get("searchListing.noResultCategoryMessage"));
  return map;
  
 }
 
 /**
  * Gets the review map properties.
  * 
  * @param page
  *         the page
  * @param searchConfig
  *         the search config
  * @return the review map properties
  */
 private Map<String, String> getReviewMapProperties(Page page, Map<String, String> searchConfig) {
  Map<String, String> reviewMapProperties = new HashMap<String, String>();
  String serviceProviderNameReview = null;
  String serviceProviderName = StringUtils.EMPTY;
  try {
   Resource resource = page.getContentResource();
   InheritanceValueMap valueMap = new HierarchyNodeInheritanceValueMap(resource);
   if (valueMap != null) {
    serviceProviderName = (String) valueMap.getInherited(RATING_REVIEWS, StringUtils.EMPTY);
   }
   if (StringUtils.isNotBlank(serviceProviderName)) {
    serviceProviderNameReview = serviceProviderName;
   } else {
    serviceProviderNameReview = configurationService.getConfigValue(page, "ratingAndReviews", SERVICE_PROVIDER_NAME);
   }
  } catch (ConfigurationNotFoundException e) {
   LOGGER.debug("could not find serviceProviderName key in serviceProviderNameShopNow category", e);
  }
  try {
   reviewMapProperties = configurationService.getCategoryConfiguration(page, serviceProviderNameReview);
   if (reviewMapProperties != null) {
    GlobalConfiguration globalConfiguration = page.adaptTo(GlobalConfiguration.class);
    String[] excludedConfig = globalConfiguration.getCommonGlobalConfigValue("excludedSEOProperties").split(",");
    for (String excludeKey : excludedConfig) {
     if (reviewMapProperties.containsKey(excludeKey)) {
      reviewMapProperties.remove(excludeKey);
     }
    }
   }
   String enabled = searchConfig.get("reviewEnable");
   reviewMapProperties.put("enabled", StringUtils.isBlank(enabled) ? "true" : enabled);
   if (BAZAAR_VOICE.equalsIgnoreCase(serviceProviderNameReview) || serviceProviderNameReview.startsWith(BAZAAR_VOICE)) {
    reviewMapProperties.put(SERVICE_PROVIDER_NAME, BAZAAR_VOICE);
   } else if (KRITIQUE.equalsIgnoreCase(serviceProviderNameReview) || serviceProviderNameReview.startsWith(KRITIQUE)) {
    reviewMapProperties.put(SERVICE_PROVIDER_NAME, KRITIQUE);
   }
  } catch (ConfigurationNotFoundException e) {
   LOGGER.debug("could not find serviceProviderNameReview category in configuration", e);
  }
  LOGGER.debug("Review Map Properties : " + reviewMapProperties);
  return reviewMapProperties;
 }
 
 /**
  * Gets the auto suggestion label map.
  * 
  * @param i18n
  *         the i 18 n
  * @param country
  *         the country
  * @param suggestionHandler
  *         the suggestion handler
  * @return the auto suggestion label map
  */
 private Map<String, Object> getAutoSuggestionLabelMap(I18n i18n, String country, String suggestionHandler) {
  Map<String, Object> map = new HashMap<String, Object>();
  Map<String, Object> suggestionLabel = new HashMap<String, Object>();
  
  map.put(PLACEHOLDER, i18n.get("search.type.something.here"));
  map.put(SEARCH_LABEL, i18n.get("search.label"));
  map.put(SUGGESTION_SERVLET, suggestionHandler);
  map.put(COUNTRYCODE, country);
  suggestionLabel.put("searchInput", map);
  return suggestionLabel;
  
 }
 
 /**
  * Gets the search results.
  * 
  * @param request
  *         the request
  * @param resultPerPage
  *         the result per page
  * @return the search results
  */
 private Map<String, Object> getSearchResults(SlingHttpServletRequest request, String resultPerPage) {
  
  Map<String, Object> results = new HashMap<String, Object>();
  
  try {
   results = solrSearchResultsRetrieval.getSearchResult(request, true);
   if ((results != null) && (!results.isEmpty()) && (results.containsKey("totalResults"))) {
    int totalResults = Integer.parseInt(String.valueOf(results.get("totalResults")));
    if (totalResults > 0) {
     int noPages = totalResults / Integer.parseInt(resultPerPage);
     int remainder = totalResults % Integer.parseInt(resultPerPage);
     noPages = remainder > 0 ? noPages++ : noPages;
     int qStringPageNo = Integer.parseInt(request.getParameter(PAGE_NUM) == null ? "0" : request.getParameter(PAGE_NUM));
     String qString = getQueryStringUsingParameters(request);
     
     results.put("extremePageLinks", getExtremePageLinksMap(request, qString, noPages, qStringPageNo));
     results.put("pagecount", noPages);
     results.put("paginationLinks", getPaginationList(request, qString, noPages, qStringPageNo).toArray());
    }
   }
  } catch (Exception e) {
   LOGGER.error("Exception in getting Search Result Map" + ExceptionUtils.getStackTrace(e));
  }
  
  return results;
 }
 
 /**
  * Gets the query string using parameters.
  * 
  * @param request
  *         the request
  * @return the query string using parameters
  */
 private String getQueryStringUsingParameters(SlingHttpServletRequest request) {
  
  String qString = QUERY_PARAM_QMARK;
  Map<String, String> newMap = new HashMap<String, String>();
  Map paramMap = request.getParameterMap();
  
  for (Object key : paramMap.keySet()) {
   Object[] row = (Object[]) paramMap.get(key);
   newMap.put((String) key, (String) row[0]);
  }
  
  if (newMap.containsKey(PAGE_NUM)) {
   newMap.remove(PAGE_NUM);
  }
  
  for (String key : newMap.keySet()) {
   qString = qString + key + EQUALS + newMap.get(key) + "&";
  }
  return qString;
 }
 
 /**
  * Gets the extreme page links map.
  * 
  * @param request
  *         the request
  * @param queryString
  *         the query string
  * @param noOfPages
  *         the no of pages
  * @param queryStringPageNo
  *         the query string page no
  * @return the extreme page links map
  */
 private Map<String, Object> getExtremePageLinksMap(SlingHttpServletRequest request, String queryString, int noOfPages, int queryStringPageNo) {
  Map<String, Object> extremePageMap = new HashMap<String, Object>();
  
  String firstPageUrl = request.getRequestURI() + queryString + "PageNum=1";
  String lastPageUrl = request.getRequestURI() + queryString + PAGE_NUM + EQUALS + noOfPages;
  String nextPageUrl = StringUtils.EMPTY;
  String previousPageUrl = StringUtils.EMPTY;
  
  if ((queryStringPageNo != 0) && (queryStringPageNo <= noOfPages)) {
   if (queryStringPageNo == 1) {
    nextPageUrl = request.getRequestURI() + queryString + PAGE_NUM + EQUALS + (queryStringPageNo + 1);
    previousPageUrl = request.getRequestURI() + queryString + PAGE_NUM + EQUALS + queryStringPageNo;
   } else if (queryStringPageNo == noOfPages) {
    nextPageUrl = request.getRequestURI() + queryString + PAGE_NUM + EQUALS + noOfPages;
    previousPageUrl = request.getRequestURI() + queryString + PAGE_NUM + EQUALS + (queryStringPageNo - 1);
   } else {
    nextPageUrl = request.getRequestURI() + queryString + PAGE_NUM + EQUALS + (queryStringPageNo + 1);
    previousPageUrl = request.getRequestURI() + queryString + PAGE_NUM + EQUALS + (queryStringPageNo - 1);
   }
  } else if (queryStringPageNo == 0) {
   if (noOfPages > 1) {
    nextPageUrl = request.getRequestURI() + queryString + PAGE_NUM + EQUALS + (queryStringPageNo + TWO);
    previousPageUrl = request.getRequestURI() + queryString + PAGE_NUM + EQUALS + (queryStringPageNo + 1);
   } else {
    nextPageUrl = request.getRequestURI() + queryString + PAGE_NUM + EQUALS + (queryStringPageNo + 1);
    previousPageUrl = request.getRequestURI() + queryString + PAGE_NUM + EQUALS + (queryStringPageNo + 1);
   }
  }
  
  extremePageMap.put("firstPage", getExtremePageURLMap(firstPageUrl));
  extremePageMap.put("lastPage", getExtremePageURLMap(lastPageUrl));
  extremePageMap.put("nextPage", getExtremePageURLMap(nextPageUrl));
  extremePageMap.put("previousPage", getExtremePageURLMap(previousPageUrl));
  
  return extremePageMap;
 }
 
 /**
  * Gets the extreme page URL map.
  * 
  * @param url
  *         the url
  * @return the extreme page URL map
  */
 private Map<String, Object> getExtremePageURLMap(String url) {
  
  Map<String, Object> pageUrlMap = new HashMap<String, Object>();
  
  pageUrlMap.put("url", url);
  pageUrlMap.put("isenabled", true);
  
  return pageUrlMap;
 }
 
 /**
  * Gets the pagination list.
  * 
  * @param request
  *         the request
  * @param queryString
  *         the query string
  * @param noOfPages
  *         the no of pages
  * @param pageNumber
  *         the page number
  * @return the pagination list
  */
 private List<Map<String, Object>> getPaginationList(SlingHttpServletRequest request, String queryString, int noOfPages, int pageNumber) {
  
  List<Map<String, Object>> paginationLinks = new ArrayList<Map<String, Object>>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  int queryStringPageNo = pageNumber;
  for (int i = 1; i <= noOfPages; i++) {
   String url = request.getRequestURI() + queryString + PAGE_NUM + EQUALS + String.valueOf(i);
   if (queryStringPageNo == 0) {
    paginationLinks.add(getPageURLMap(i, url, true));
    queryStringPageNo++;
   } else if (queryStringPageNo == i) {
    paginationLinks.add(getPageURLMap(i, url, true));
   } else {
    paginationLinks.add(getPageURLMap(i, url, false));
   }
  }
  
  return paginationLinks;
 }
 
 /**
  * Gets the page URL map.
  * 
  * @param pageNo
  *         the page no
  * @param url
  *         the url
  * @param flag
  *         the flag
  * @return the page URL map
  */
 private Map<String, Object> getPageURLMap(int pageNo, String url, Boolean flag) {
  
  Map<String, Object> pageUrlMap = new HashMap<String, Object>();
  
  pageUrlMap.put(SEARCH_LABEL, pageNo);
  pageUrlMap.put("url", url);
  pageUrlMap.put("iscurrentPage", flag);
  
  return pageUrlMap;
 }
 
 /**
  * Gets the show more map.
  * 
  * @param configMap
  *         the config map
  * @param perContentCount
  *         the per content count
  * @param i18n
  *         the i 18 n
  * @return the show more map
  */
 private Map<String, String> getShowMoreMap(Map<String, String> configMap, int perContentCount, I18n i18n) {
  String showMoreFormat = "search.%s.showmore";
  Map<String, String> showMoreMap = new HashMap<String, String>();
  for (String contentType : configMap.keySet()) {
   String contentTypeWithoutSpace = contentType.replaceAll("\\s", "");
   showMoreMap.put(contentType, i18n.get(String.format(showMoreFormat, contentTypeWithoutSpace)));
  }
  Map<String, Object> count = new HashMap<String, Object>();
  count.put("perContentCount", perContentCount);
  return showMoreMap;
  
 }
 
 private static Map<String, Object> getSubBrands(ResourceResolver resolver, String path, String selectedBrand, Map<String, Object> resources,
   StringBuilder hideSubBrandString) {
  Map<String, Object> subBrandMap = new HashMap<String, Object>();
  String[] subBrands = ArrayUtils.EMPTY_STRING_ARRAY;
  Resource searchInputNode = resolver.resolve(path);
  if (searchInputNode != null) {
   ValueMap searchInputMap = searchInputNode.adaptTo(ValueMap.class);
   subBrands = (String[]) searchInputMap.get(SUB_BRAND_NAME);
   StringBuilder selectedBrandCssClass = new StringBuilder();
   hideSubBrandString.append(MapUtils.getBooleanValue(searchInputMap, "hideSubBrand"));
   subBrandMap.put("selectSubBrand", MapUtils.getBooleanValue(searchInputMap, "selectSubBrand"));
   subBrandMap.put("subBrands", SearchHelper.getSubBrandProperties(subBrands, resources, path, selectedBrandCssClass, selectedBrand));
  }
  
  return subBrandMap;
 }
 
 private Map<String, Object> getOverridenConfig(String path, ResourceResolver resolver) {
  Map<String, Object> overriddenConfig = new HashMap<String, Object>();
  Resource resource = resolver.resolve(path);
  if (resource != null) {
   ValueMap map = resource.adaptTo(ValueMap.class);
   overriddenConfig = map;
  }
  return overriddenConfig;
 }
 
 /**
  * 
  * @param overrideGlobalConfig
  * @param configMapInput
  * @param inputProperties
  * @param configurationsMap
  * @param tabContentTypes
  */
 public static void setGlobalConfigMap(boolean overrideGlobalConfig, Map<String, String> configMapInput, Map<String, Object> inputProperties,
   Map<String, Object> configurationsMap, List<Map<String, Object>> tabContentTypes) {
  if (overrideGlobalConfig) {
   Map<String, Object> overriddenMap = GlobalConfigurationUtility.getOverriddenConfigMap(configMapInput, inputProperties);
   configurationsMap.put(SearchHelper.MIN_CHAR_TO_TRIGGER_SEARCH_JSON_LABEL,
     MapUtils.getInteger(overriddenMap, SearchHelper.MIN_CHAR_TO_TRIGGER_SEARCH, SearchHelper.THREE));
   configurationsMap.put(SearchHelper.DISABLE_AUTO_SUGGEST_JSON_LABEL, MapUtils.getBoolean(overriddenMap, SearchHelper.DISABLE_AUTO_SUGGEST, false));
   configurationsMap.put(SearchHelper.DISABLE_AUTO_COMPLETE_JSON_LABEL,
     MapUtils.getBoolean(overriddenMap, SearchHelper.DISABLE_AUTO_COMPLETE, false));
   configurationsMap.put(SearchHelper.AUTO_SUGGEST_RESULT_COUNT_JSON_LABEL,
     MapUtils.getInteger(overriddenMap, SearchHelper.AUTO_SUGGEST_RESULT_COUNT, SearchHelper.FIVE));
   if (CollectionUtils.isEmpty(tabContentTypes)) {
    configurationsMap.put(SearchHelper.DISABLE_CONTENT_TYPE_FILTER_JSON_LABEL, true);
   } else {
    configurationsMap.put(SearchHelper.DISABLE_CONTENT_TYPE_FILTER_JSON_LABEL,
      MapUtils.getBoolean(overriddenMap, SearchHelper.DISABLE_CONTENT_TYPE_FILTER, false));
   }
   configurationsMap.put(SearchHelper.SEARCH_SUGGESTION_HANDLER_JSON_LABEL,
     MapUtils.getString(overriddenMap, SearchHelper.SEARCH_SUGGESTION_HANDLER, ""));
   configurationsMap.put(SearchHelper.REQUEST_SERVLET_JSON_LABEL, MapUtils.getString(overriddenMap, SearchHelper.REQUEST_SERVLET, ""));
  } else {
   configurationsMap.put(SearchHelper.MIN_CHAR_TO_TRIGGER_SEARCH_JSON_LABEL,
     MapUtils.getInteger(configMapInput, SearchHelper.MIN_CHAR_TO_TRIGGER_SEARCH, SearchHelper.THREE));
   configurationsMap.put(SearchHelper.DISABLE_AUTO_SUGGEST_JSON_LABEL, MapUtils.getBoolean(configMapInput, SearchHelper.DISABLE_AUTO_SUGGEST, false));
   configurationsMap.put(SearchHelper.DISABLE_AUTO_COMPLETE_JSON_LABEL,
     MapUtils.getBoolean(configMapInput, SearchHelper.DISABLE_AUTO_COMPLETE, false));
   configurationsMap.put(SearchHelper.AUTO_SUGGEST_RESULT_COUNT_JSON_LABEL,
     MapUtils.getInteger(configMapInput, SearchHelper.AUTO_SUGGEST_RESULT_COUNT, SearchHelper.FIVE));
   if (CollectionUtils.isEmpty(tabContentTypes)) {
    configurationsMap.put(SearchHelper.DISABLE_CONTENT_TYPE_FILTER_JSON_LABEL, true);
   } else {
    configurationsMap.put(SearchHelper.DISABLE_CONTENT_TYPE_FILTER_JSON_LABEL,
      MapUtils.getBoolean(configMapInput, SearchHelper.DISABLE_CONTENT_TYPE_FILTER, false));
   }
   configurationsMap.put(SearchHelper.SEARCH_SUGGESTION_HANDLER_JSON_LABEL,
     MapUtils.getString(configMapInput, SearchHelper.SEARCH_SUGGESTION_HANDLER, ""));
   configurationsMap.put(SearchHelper.REQUEST_SERVLET_JSON_LABEL, MapUtils.getString(configMapInput, SearchHelper.REQUEST_SERVLET, ""));
  }
 }
 
 /**
  * 
  * @param requestParamMap
  * @param searchResultsMap
  * @param resolver
  * @param resources
  * @param slingRequest
  */
 public static void setSubBrandData(Map requestParamMap, Map<String, Object> searchResultsMap, ResourceResolver resolver,
   Map<String, Object> resources, ServletRequest slingRequest) {
  if (requestParamMap.containsKey("sip") && requestParamMap.containsKey("sbn")) {
   String searchInputPath = slingRequest.getParameter("sip");
   String selectedBrand = slingRequest.getParameter("sbn");
   StringBuilder hideSubBrand = new StringBuilder();
   searchResultsMap.put("subBrandMap", getSubBrands(resolver, searchInputPath, selectedBrand, resources, hideSubBrand));
   searchResultsMap.put("hideSubBrand", Boolean.parseBoolean(hideSubBrand.toString()));
  }
 }
 
 /**
  * 
  * @param properties
  * @param resources
  * @param configMap
  * @param isExternal
  * @param searchResultsMap
  * @param commaSeparatedContentTypes
  * @return {@link List}
  */
 public static List<Map<String, Object>> setTabsAndFilters(Map<String, Object> properties, Map<String, Object> resources,
   Map<String, String> configMap, boolean isExternal, Map<String, Object> searchResultsMap, String commaSeparatedContentTypes) {
   
  Page currentPage = getCurrentPage(resources);
  I18n i18n = getI18n(resources);
  ResourceResolver resolver = getResourceResolver(resources);
  String separatedContentTypes = commaSeparatedContentTypes;
  
  Map<String, String> externalContentTypeMap = ComponentUtil.getConfigMap(currentPage, "externalContentTypes");
  if (properties.containsKey("filterOptionJson")) {
   SearchFilterDTO filterDTO = new SearchFilterDTO();
   filterDTO.setFilterHeader(MapUtils.getString(properties, "filterHeading", StringUtils.EMPTY));
   filterDTO.setSubFilterHeader(MapUtils.getString(properties, "filterSubHeading", StringUtils.EMPTY));
   filterDTO.setClearAllFiltersKey(i18n.get("search.clearAllFilters.label"));
   List<SearchFilterOptionsDTO> options = SearchHelper.getFilterOptions(resources, "filterOptionJson", configMap, resolver, i18n,
     externalContentTypeMap, isExternal);
   filterDTO.setFilterOptions(options);
   LOGGER.info("filter DTO is", filterDTO);
   searchResultsMap.put("filters", filterDTO);
  }
  Map<String, String> contentTypesFromInput = new HashMap<String, String>();
  if (StringUtils.isNotEmpty(separatedContentTypes)) {
   separatedContentTypes += ",everything";
  }
  for (String contentName : separatedContentTypes.split(",")) {
   if (configMap.containsKey(contentName)) {
    contentTypesFromInput.put(contentName, configMap.get(contentName));
   }
  }
  List<Map<String, Object>> tabContentType = new ArrayList<Map<String, Object>>();
  if (MapUtils.isNotEmpty(contentTypesFromInput)) {
   tabContentType = SearchHelper.getContentType(contentTypesFromInput, currentPage, i18n, isExternal);
  } else {
   tabContentType = SearchHelper.getContentType(configMap, currentPage, i18n, isExternal);
  }
  String sortingOrder = GlobalConfigurationUtility.getValueFromConfiguration(currentPage.adaptTo(ConfigurationService.class), currentPage,
    "sortOrderCategory", "searchTabOrder");
  if (StringUtils.isNotBlank(sortingOrder)) {
   List<String> sortingOrderList = Arrays.asList(sortingOrder.split(","));
   tabContentType = ComponentUtil.sortListByConfigOrder(tabContentType, "contentName", sortingOrderList);
  }
  return tabContentType;
 }
 
}
