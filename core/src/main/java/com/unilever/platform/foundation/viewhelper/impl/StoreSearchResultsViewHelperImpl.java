/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfiguration;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.components.helper.StoreLocatorUtility;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;
import com.unilever.platform.foundation.viewhelper.constants.StoreLocatorConstants;

/**
 * The Class StoreSearchResultsViewHelperImpl.
 */
@Component(description = "StoreSearchResultsViewHelperImpl", immediate = true, metatype = true, label = "StoreSearchResultsViewHelperImpl")
@Service(value = { StoreSearchResultsViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.STORE_SEARCH_RESULTS_NEW, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class StoreSearchResultsViewHelperImpl extends BaseViewHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(StoreSearchResultsViewHelperImpl.class);
 
 /*
  * reference for site configuration service
  */
 /** The Constant STORE_LOCATER. */
 private static final String STORE_LOCATOR = "storeLocator";
 
 private static final String REFRESH_CTA_LABEL = "refreshCtaLabel";
 
 private static final String REFRESH_CTA_LABEL_KEY = "storeSearchResults.refreshCtaLabel";
 
 private static final String SERVICE_ERROR_MESSAGE = "serviceErrorMessage";
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The global configuration. */
 @Reference
 GlobalConfiguration globalConfiguration;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData (java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  LOGGER.debug("Executing StoreSearchResultsViewHelperImpl.java. Inside processData method");
  Map<String, Object> data = new HashMap<String, Object>();
  Map<String, Object> properties = getProperties(content);
  Page currentPage = getCurrentPage(resources);
  String jsonNameSpace = getJsonNameSpace(content);
  Map<String, Object> storeSearchResultsMap = new HashMap<String, Object>();
  I18n i18n = getI18n(resources);
  ResourceResolver resourceResolver = getResourceResolver(resources);
  
  storeSearchResultsMap.putAll(getDialogProperties(properties));
  storeSearchResultsMap.put("storeData", getStoreSearchResultMap(i18n, currentPage, resourceResolver));
  storeSearchResultsMap.put(ProductConstants.REVIEW_MAP, ProductHelper.getReviewMapProperties(configurationService, getCurrentPage(resources)));
  
  UNIProduct uniProduct = ProductHelper.getUniProduct(currentPage, getSlingRequest(resources));
  if (uniProduct != null) {
   Map<String, Object> productProperties = ProductHelper.getProductMap(uniProduct, jsonNameSpace, resources, currentPage);
   storeSearchResultsMap.put(ProductConstants.PRODUCT_MAP, MapUtils.isNotEmpty(productProperties) ? productProperties : StringUtils.EMPTY);
   
  } else {
   LOGGER.debug("UniProduct obj is null in getting from ProductHelperExt.findProduct");
  }
  String componentName = getComponentName(getCurrentResource(resources), getSlingRequest(resources));
  storeSearchResultsMap.put(STORE_LOCATOR, StoreLocatorUtility.getFieldsMap(currentPage, configurationService, i18n, componentName));
  storeSearchResultsMap.put(ProductConstants.SHOPNOW_MAP,
    ProductHelper.getshopNowProperties(configurationService, currentPage, i18n, getResourceResolver(resources), getSlingRequest(resources)));
  boolean geoCodeFlag = Boolean.parseBoolean(GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, "storeLocator",
    "geoCodeFlag"));
  
  storeSearchResultsMap.put("geoCodeFlag", geoCodeFlag);
  
  data.put(jsonNameSpace, storeSearchResultsMap);
  return data;
 }
 
 /**
  * Gets the store search result map.
  * 
  * @param i18n
  *         the i18n
  * @param currentPage
  *         the current page
  * @param resourceResolver
  * @param jsonNameSpace
  *         the json name space
  * @return the store search result map
  */
 private Map<String, Object> getStoreSearchResultMap(I18n i18n, Page currentPage, ResourceResolver resourceResolver) {
  Map<String, Object> storeSearchResultsMap = new HashMap<String, Object>();
  String noOfStores = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
    StoreLocatorConstants.GLOBAL_CONFIG_CAT, StoreLocatorConstants.NO_OF_STORES);
  
  String loadMoreDisplayCount = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
    StoreLocatorConstants.GLOBAL_CONFIG_CAT, StoreLocatorConstants.LOAD_MORE_DISPLAY_COUNT);
  
  storeSearchResultsMap.put(StoreLocatorConstants.NO_OF_STORES, StringUtils.isNotBlank(noOfStores) ? Integer.parseInt(noOfStores)
    : StoreLocatorConstants.HUNDRED);
  
  storeSearchResultsMap.put(StoreLocatorConstants.LOAD_MORE_DISPLAY_COUNT,
    StringUtils.isNotBlank(loadMoreDisplayCount) ? Integer.parseInt(loadMoreDisplayCount) : StoreLocatorConstants.TEN);
  
  String storeLocatorServiceURL = globalConfiguration.getConfigValue(StoreLocatorConstants.SERVICE_URL_KEY);
  
  storeSearchResultsMap.put(StoreLocatorConstants.SERVICE_URL, resourceResolver.map(storeLocatorServiceURL));
  storeSearchResultsMap.put(StoreLocatorConstants.TABLE_HEADING, getTableHeading(i18n));
  storeSearchResultsMap.put(StoreLocatorConstants.MAP_API, GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
    StoreLocatorConstants.GLOBAL_CONFIG_CAT, StoreLocatorConstants.MAP_API));
  storeSearchResultsMap.put(StoreLocatorConstants.GEO_CODE_API, GlobalConfigurationUtility.getValueFromConfiguration(configurationService,
    currentPage, StoreLocatorConstants.GLOBAL_CONFIG_CAT, StoreLocatorConstants.GEO_CODE_API));
  storeSearchResultsMap.put("staticText", getStaticMap(i18n));
  storeSearchResultsMap.putAll(GlobalConfigurationUtility.getMapFromConfiguration(configurationService, currentPage, "storeSearchResult"));
  return storeSearchResultsMap;
 }
 
 /**
  * Gets the static map.
  * 
  * @param i18n
  *         the i18n
  * @return the static map
  */
 private Map<String, Object> getStaticMap(I18n i18n) {
  Map<String, Object> staticText = new HashMap<String, Object>();
  staticText.put(StoreLocatorConstants.SEARCH_DESCRIPTION, i18n.get(StoreLocatorConstants.STORE_SEARCH_RESULTS_SEARCH_DESCRIPTION));
  staticText.put(StoreLocatorConstants.ANOTHER_PRODUCT_LABEL, i18n.get(StoreLocatorConstants.STORE_SEARCH_RESULTS_ANOTHER_PRODUCT_LABEL));
  staticText.put(StoreLocatorConstants.SELECTED_PRODUCT_LABEL, i18n.get(StoreLocatorConstants.STORE_SEARCH_RESULTS_SELECTED_PRODUCT_LABEL));
  staticText.put(StoreLocatorConstants.BUY_ONLINE_CTA, i18n.get(StoreLocatorConstants.STORE_SEARCH_RESULTS_BUY_ONLINE_CTA));
  staticText.put(StoreLocatorConstants.SHOW_ME_STORES_CTA, i18n.get(StoreLocatorConstants.STORE_SEARCH_RESULTS_SHOW_ME_STORES_CTA));
  staticText.put(StoreLocatorConstants.LOAD_MORE_CTA, i18n.get(StoreLocatorConstants.STORE_SEARCH_RESULTS_LOAD_MORE_CTA));
  staticText.put(StoreLocatorConstants.HEADING_TEXT, i18n.get(StoreLocatorConstants.STORE_SEARCH_RESULTS_HEADING_TEXT));
  staticText.put(StoreLocatorConstants.ZIPCODE_HELPTEXT, i18n.get(StoreLocatorConstants.STORE_SEARCH_RESULTS_ZIPCODE_HELPTEXT));
  staticText.put(StoreLocatorConstants.SEARCHRADIUS_PREFIX, i18n.get(StoreLocatorConstants.STORE_SEARCH_RESULTS_SEARCHRADIUS_PREFIX));
  staticText.put(StoreLocatorConstants.SEARCHRADIUS_METRICS, i18n.get(StoreLocatorConstants.STORE_SEARCH_RESULTS_SEARCH_RADIUS_METRICS));
  staticText.put(StoreLocatorConstants.STORELIST_DIRECTION_CTALABEL,
    i18n.get(StoreLocatorConstants.STORE_SEARCH_RESULTS_STORELIST_DIRECTION_CTALABEL));
  staticText.put(StoreLocatorConstants.SHOWMORE_CTALABEL, i18n.get(StoreLocatorConstants.STORE_SEARCH_RESULTS_STORELIST_SHOWMORE_CTALABEL));
  staticText.put(StoreLocatorConstants.BUYONLINE_PREFIX, i18n.get(StoreLocatorConstants.STORE_SEARCH_RESULTS_STORELIST_BUYONLINE_PREFIX));
  staticText.put(StoreLocatorConstants.SUMMARY_TEXT, i18n.get(StoreLocatorConstants.STORE_SEARCH_RESULTS_STORELIST_SUMMARY_TEXT));
  
  staticText.put(REFRESH_CTA_LABEL, i18n.get(REFRESH_CTA_LABEL_KEY));
  staticText.put(SERVICE_ERROR_MESSAGE, i18n.get("storeLocator.serviceErrorMessage"));
  staticText.put(StoreLocatorConstants.PRINT_LABEL, i18n.get(StoreLocatorConstants.STORE_SEARCH_RESULTS_PRINT_LABEL));
  staticText.put(StoreLocatorConstants.SHARE_LOCATION, i18n.get(StoreLocatorConstants.STORE_SEARCH_RESULTS_SHARE_LOCATION));
  
  return staticText;
 }
 
 /**
  * Gets the table heading.
  * 
  * @param i18n
  *         the i18n
  * @return the table heading
  */
 private Map<String, Object> getTableHeading(I18n i18n) {
  Map<String, Object> tableHeadingMap = new HashMap<String, Object>();
  tableHeadingMap.put(StoreLocatorConstants.COLUMN_STORE, i18n.get(StoreLocatorConstants.STORE_SEARCH_RESULTS_COLUMN_STORE));
  tableHeadingMap.put(StoreLocatorConstants.COLUMN_PHONE, i18n.get(StoreLocatorConstants.STORE_SEARCH_RESULTS_COLUMN_PHONE));
  tableHeadingMap.put(StoreLocatorConstants.COLUMN_ADDRESS, i18n.get(StoreLocatorConstants.STORE_SEARCH_RESULTS_COLUMN_ADDRESS));
  tableHeadingMap.put(StoreLocatorConstants.COLUMN_DISTANCE, i18n.get(StoreLocatorConstants.STORE_SEARCH_RESULTS_COLUMN_DISTANCE));
  return tableHeadingMap;
 }
 
 /**
  * Gets the dialog properties.
  * 
  * @param properties
  *         the properties
  * @return the dialog properties
  */
 private Map<String, Object> getDialogProperties(Map<String, Object> properties) {
  Map<String, Object> map = new HashMap<String, Object>();
  String heading = MapUtils.getString(properties, "headingText", StringUtils.EMPTY);
  
  String subHeading = MapUtils.getString(properties, "subHeadingText", StringUtils.EMPTY);
  
  String noResultText = MapUtils.getString(properties, "noResultCopy", StringUtils.EMPTY);
  
  map.put("heading", heading);
  map.put("subheading", subHeading);
  map.put("noResultText", noResultText);
  return map;
  
 }
}
