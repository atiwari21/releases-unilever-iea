/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.template.helper;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.jcr.Node;
import javax.servlet.jsp.PageContext;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.settings.SlingSettingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.core.service.AnalyticsService;
import com.unilever.platform.foundation.template.dto.AnalyticsDTO;

/**
 * The Class AnalyticHelper.
 */
public class AnalyticHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(AnalyticHelper.class);
 
 /**
  * Instantiates a new analytic helper.
  */
 private AnalyticHelper() {
  
 }
 
 /**
  * Sets the analytics attributes.
  * 
  * @param pageContext
  *         the page context
  * @param sling
  *         the sling
  * @param currentPage
  *         the current page
  * @param currentNode
  *         the current node
  */
 public static void setAnalyticsAttributes(PageContext pageContext, SlingScriptHelper sling, Page currentPage, Node currentNode) {
  LOGGER.debug("Inside setAnalyticsAttributes of AnalyticHelper");
  SlingSettingsService slingSettingsService = sling.getService(SlingSettingsService.class);
  Set<String> runModes = slingSettingsService != null ? slingSettingsService.getRunModes() : null;
  
  Map<String, String> paramMap = new HashMap<String, String>();
  Map<String, String> domainList = new HashMap<String, String>();
  
  AnalyticsService as = (AnalyticsService) sling.getService(AnalyticsService.class);
  if (as != null && currentPage != null) {
   paramMap = as.getAnalyticsBasicProperties(currentPage);
   domainList = as.getAnalyticsConfiguration(currentPage, currentNode);
   LOGGER.debug("paramMap : {}", paramMap);
   LOGGER.debug("domainList : {}", domainList);
   if (!domainList.isEmpty() && !paramMap.isEmpty()) {
    AnalyticsDTO analyticsDTO = new AnalyticsDTO(paramMap, domainList);
    if (runModes != null && runModes.contains("prod") && runModes.contains("publish")) {
     analyticsDTO.setIsProdEnv(true);
    } else {
     analyticsDTO.setIsProdEnv(false);
    }
    pageContext.setAttribute("analyticsDTO", analyticsDTO);
   }
   String title = currentPage.getTitle() != null ? currentPage.getTitle() : StringUtils.EMPTY;
   pageContext.setAttribute("pageType", getPageType(currentPage));
   pageContext.setAttribute("contentType", title);
  }
  
 }
 
 /**
  * Gets the page type.
  * 
  * @param currentPage
  *         the current page
  * @return the page type
  */
 private static String getPageType(Page currentPage) {
  LOGGER.debug("Inside getContentType of AnalyticHelper");
  return currentPage.getContentResource() != null && currentPage.getContentResource().getValueMap() != null ? MapUtils.getString(currentPage
    .getContentResource().getValueMap(), "contentType", StringUtils.EMPTY) : StringUtils.EMPTY;
  
 }
}
