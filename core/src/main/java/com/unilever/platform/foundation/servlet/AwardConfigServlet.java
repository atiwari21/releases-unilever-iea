/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.servlet;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.PropertyUnbounded;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.unilever.platform.aem.foundation.configuration.ConfigurationHelper;
import com.unilever.platform.aem.foundation.constants.CommonConstants;
import com.unilever.platform.foundation.components.helper.AwardHelper;
import com.unilever.platform.foundation.template.dto.Award;

/**
 * The Class AwardConfigServlet.
 */
@SlingServlet(resourceTypes = CommonConstants.DEFAULT_SERVLET_NAME, metatype = true, selectors = { "awardsConfig" }, extensions = { "html", "json" })
@Property(name = "resourceTypes", unbounded = PropertyUnbounded.ARRAY, label = "awards component resource types", value = { "unilever-aem/components/content/award" })
public class AwardConfigServlet extends SlingAllMethodsServlet {
 
 /** The factory. */
 @Reference
 private ResourceResolverFactory factory;
 
 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = 1L;
 
 /** The Constant LOGGER. */
 private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(AwardConfigServlet.class);
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
  Map<String, LinkedHashSet<Award>> awardSet = new HashMap<String, LinkedHashSet<Award>>();
  response.setContentType("application/json");
  OutputStream outputStream = response.getOutputStream();
  ResourceResolver resolver = null;
  try {
   resolver = ConfigurationHelper.getResourceResolver(factory, "readService");
  } catch (LoginException e) {
   LOGGER.debug("ResourceResolver is not available", e);
   LOGGER.warn("ResourceResolver is not available" + e.getMessage());
  }
  
  if (resolver != null) {
   Resource awardsPath = request.getResource()!=null?resolver.resolve(request.getResource().getPath()):null;
   PageManager manager = resolver.adaptTo(PageManager.class);
   Page awardsPage = awardsPath != null ? manager.getContainingPage(awardsPath) : null;
   Resource parResource = awardsPage != null ? resolver.getResource(awardsPage.getContentResource().getPath() + "/par") : null;
   if (parResource != null) {
    Set<Award> awardList = new LinkedHashSet<Award>();
    Iterator<Resource> awards = parResource.listChildren();
    while (awards.hasNext()) {
     Resource awardResource = awards.next();
     ValueMap properties = awardResource.adaptTo(ValueMap.class);
     Award award = AwardHelper.getPathSpecificAward(properties, awardsPage, resolver, request);
     awardList.add(award);
    }
    AwardHelper.addPathSpecificAssociatedAwards(awardList, resolver, awardSet);
    resolver.close();
    Gson gson = new GsonBuilder().serializeNulls().create();
    String jsonResponseString = gson.toJson(awardSet, new TypeToken<Map<String, LinkedHashSet<Award>>>() {
     
     
    }.getType());
    outputStream.write(jsonResponseString.getBytes("UTF-8"));
   }
  } else {
   outputStream.write(new JSONObject().toString().getBytes("UTF-8"));
  }
 }
 
}
