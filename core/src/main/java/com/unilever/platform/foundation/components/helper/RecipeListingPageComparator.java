/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.components.helper;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import com.day.cq.tagging.Tag;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;

/**
 * The Class RecipeListingPageComparator.
 */
public class RecipeListingPageComparator implements Comparator<Object> {
 
 /** The promotion tag ids. */
 private String[] promotionTagIds;
 
 /** The page manager. */
 private PageManager pageManager;
 
 /**
  * Instantiates a new page sorting helper.
  * 
  * @param promotionTagIds
  *         the promotion tag Ids
  * 
  * @param pageManager
  *         the page manager
  */
 public RecipeListingPageComparator(String[] promotionTagIds, PageManager pageManager) {
  super();
  this.promotionTagIds = promotionTagIds;
  this.pageManager = pageManager;
 }
 
 @Override
 public int compare(Object obj1, Object obj2) {
  Page pageOne = this.pageManager.getPage(obj1.toString());
  Page pageTwo = this.pageManager.getPage(obj2.toString());
  int pageOnePromotionCount = getpromotionCount(pageOne, this.promotionTagIds);
  int pageTwoPromotionCount = getpromotionCount(pageTwo, this.promotionTagIds);
  return pageTwoPromotionCount - pageOnePromotionCount;
 }
 
 private int getpromotionCount(Page page, String[] promotionTagIds) {
  int promotionTagCount = 0;
  if (page != null) {
   Tag[] tagArr = page.getTags();
   for (Tag tag : tagArr) {
    String tagId = tag.getTagID();
    if (promotionTagIds != null && promotionTagIds.length > 0 && isMatchingTags(Arrays.asList(promotionTagIds), tagId)) {
     promotionTagCount++;
    }
   }
  }
  return promotionTagCount;
  
 }
 
 /**
  * Checks if is matching tags.
  * 
  * @param tagsList
  *         the tags list
  * @param tagId
  *         the tag id
  * @return true, if is matching tags
  */
 private boolean isMatchingTags(List<String> tagsList, String tagId) {
  boolean tagFlag = false;
  for (String individualTag : tagsList) {
   if (tagId.startsWith(individualTag)) {
    tagFlag = true;
    break;
   }
  }
  
  return tagFlag;
 }
}
