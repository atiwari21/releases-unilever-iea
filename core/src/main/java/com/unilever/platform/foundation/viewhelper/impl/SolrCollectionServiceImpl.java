/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.StringTokenizer;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.unilever.platform.aem.foundation.core.service.SolrConfigurationService;
/**
 * the class SolrCollectionServiceImpl
 */
@Component(immediate = true)
@Service(value = { SolrCollectionService.class })
public class SolrCollectionServiceImpl implements SolrCollectionService {
 @Reference
 private SolrConfigurationService solrConfig;
 
 String collectionTitle = "collection1";
 
 @Override
 public String getCollection(String brandName, String locale) {
  String[] collectionsBrandsArray = solrConfig.getSolrCollectionsArray();
  for (String collectionName : collectionsBrandsArray) {
   StringTokenizer str1 = new StringTokenizer(collectionName, ":");
   while (str1.hasMoreTokens()) {
    String collection = "";
    String collectionBrand = str1.nextToken();
    String collectionLocale = str1.nextToken();
    collection = str1.nextToken();
    if (brandName.equalsIgnoreCase(collectionLocale) && locale.equalsIgnoreCase(collectionBrand)) {
     collectionTitle = collection;
    }
   }
  }
  return collectionTitle;
 }
 
}
