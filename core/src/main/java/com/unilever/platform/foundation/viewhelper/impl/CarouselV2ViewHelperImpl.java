/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sapient.platform.iea.aem.core.utils.CollectionUtils;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * This view helper is corresponding to carousel with panels. It prepares the data map for carousel.
 * 
 * 
 */
@Component(description = "CarouselV2ViewHelperImpl", immediate = true, metatype = true, label = "CarouselV2ViewHelperImpl")
@Service(value = { CarouselV2ViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.CAROUSEL, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class CarouselV2ViewHelperImpl extends BaseViewHelper {
 
 /** The Constant CLASS. */
 private static final String CLASS = "class";
 
 /** The Constant TYPE. */
 private static final String TYPE = "type";
 
 /** The Constant STYLE. */
 private static final String STYLE = "style";
 
 /** The Constant CAROUSEL. */
 private static final String CAROUSEL = "carousel V2";
 
 /** The Constant COMPONENT_PARENT_TYPE. */
 private static final String COMPONENT_PARENT_TYPE = "componentParentType";
 /**
  * Constant for holding slides key in the final data map returned to be consumed by json.
  */
 private static final String SLIDES_KEY = "slides";
 /**
  * Constant for holding slideType property of carousel.
  */
 private static final String PANEL_PATH = "panelPath";
 /**
  * Constant for holding intervalsOfRotation property of carousel.
  */
 private static final String INTERVALS_OF_ROTATION = "intervalsOfRotation";
 /**
  * Constant for holding panel component.
  */
 private static final String HERO = "herov2";
 /**
  * Constant for holding properties.
  */
 private static final String PROPERTIES = "properties";
 
 /** The Constant RESOURCE_TYPE. */
 private static final String RESOURCE_TYPE = "resourceType";
 /**
  * logger object.
  */
 private static final String RANDOM_NUMBER = "randomNumber";
 
 private static final Logger LOGGER = LoggerFactory.getLogger(CarouselV2ViewHelperImpl.class);
 
 /**
  * <p>
  * This method returns panel configuration map.
  * </p>
  * 
  * @param content
  *         {@link Map} of all user input received as input
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return Map holding the result data.
  * 
  */
 @SuppressWarnings("unchecked")
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  LOGGER.info("inside carousel angular view Helper");
  Map<String, Object> properties = (Map<String, Object>) content.get(PROPERTIES);
  
  String jsonNameSpace = MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE);
  
  Map<String, Object> carouselDataMapFinal = new HashMap<String, Object>();
  List<Map<String, Object>> slideList = new ArrayList<Map<String, Object>>();
  
  slideList = getSlideData(properties, resources);
  
  Map<String, Object> carouselDataMap = new LinkedHashMap<String, Object>();
  carouselDataMap.put(SLIDES_KEY, slideList.toArray());
  int intervalOfRotation = MapUtils.getIntValue(properties, INTERVALS_OF_ROTATION, 0);
  carouselDataMap.put(INTERVALS_OF_ROTATION, intervalOfRotation);
  carouselDataMapFinal.put(jsonNameSpace, carouselDataMap);
  LOGGER.debug("The carousel data map returned is " + carouselDataMap);
  
  return carouselDataMapFinal;
 }
 
 /**
  * This method is used to get the data of all the slides configured inside carousel.
  * 
  * @param properties
  *         -map of all properties.
  * @param resources
  *         -map of all resources.
  * @return -returns the slides list with all the data.
  */
 public List<Map<String, Object>> getSlideData(final Map<String, Object> properties, final Map<String, Object> resources) {
  String[] heroPanelPath = new String[1];
  List<Map<String, Object>> slideList = new ArrayList<Map<String, Object>>();
  if (properties.containsKey(PANEL_PATH)) {
   if (properties.get(PANEL_PATH) instanceof String) {
    String panelPathValue = (String) properties.get(PANEL_PATH);
    heroPanelPath[0] = panelPathValue;
   } else {
    heroPanelPath = (String[]) properties.get(PANEL_PATH);
   }
   slideList = allDataMapForCarousel(heroPanelPath, resources);
  }
  return slideList;
  
 }
 
 /**
  * This method prepares the data map for all data json which incororates all the panel information along with carousel data.
  * 
  * @param heroPanelPath
  *         the hero panel path
  * @param resources
  *         the resources
  * @return the all data map
  */
 private List<Map<String, Object>> allDataMapForCarousel(final String[] heroPanelPath, final Map<String, Object> resources) {
  
  Map<String, Object> panelContent = new HashMap<String, Object>();
  ResourceResolver resourceResolver = (ResourceResolver) resources.get("RESOURCE_RESOLVER");
  List<Map<String, Object>> slideList = new ArrayList<Map<String, Object>>();
  
  for (int size = 0; size < heroPanelPath.length; size++) {
   if (heroPanelPath[size].contains(HERO)) {
    Map<String, Object> valueMapForPanel = CollectionUtils.getResourceValueMapasMap(heroPanelPath[size], resourceResolver);
    
    String panelResourcePath = "/apps/" + resourceResolver.getResource(heroPanelPath[size]).getResourceType();
    
    ViewHelper panelViewHelper = null;
    String panelJsonNameSpace = null;
    try {
     
     if (null != super.getComponentViewAndViewHelperCache()) {
      panelViewHelper = super.getComponentViewAndViewHelperCache().retrieveViewHelper(panelResourcePath, null);
      panelJsonNameSpace = super.getComponentViewAndViewHelperCache().retrieveJsonNameSpace(panelResourcePath);
     } else {
      LOGGER.error(" Error in obtaining componentViewAndViewHelperCache object ");
     }
    } catch (IOException exception) {
     LOGGER.debug("Exception occured in getting the view helper for panel " + exception.getMessage() + "Exception caught is" + exception);
     LOGGER.error("Exception occured in getting the view helper for panel " + exception.getMessage() + "Exception caught is" + exception);
    }
    LOGGER.info("The value map for carousel panel is " + valueMapForPanel);
    panelContent.put(PROPERTIES, valueMapForPanel);
    panelContent.put("KEY_CURRENT_CONTENT_PAGE_PATH", heroPanelPath[size]);
    panelContent.put(UnileverComponents.JSON_NAME_SPACE, panelJsonNameSpace);
    
    if (null != panelViewHelper) {
     Map<String, Object> panelMap = panelViewHelper.processData(panelContent, resources);
     if (panelMap.containsKey(panelJsonNameSpace) && panelMap.get(panelJsonNameSpace) instanceof Map) {
      Map<String, Object> jsonNameSpaceMap = (Map<String, Object>) panelMap.get(panelJsonNameSpace);
      jsonNameSpaceMap.put(STYLE, getStyleMap(valueMapForPanel));
      jsonNameSpaceMap.put(RANDOM_NUMBER, MapUtils.getString(valueMapForPanel, RANDOM_NUMBER, "0"));
      panelMap.put(panelJsonNameSpace, jsonNameSpaceMap);
     }
     panelMap.put(COMPONENT_PARENT_TYPE, CAROUSEL);
     LOGGER.debug("Resource Type for the panel component is : - " + panelResourcePath);
     panelMap.put(RESOURCE_TYPE, panelResourcePath);
     
     slideList.add(panelMap);
    }
   }
  }
  LOGGER.debug("The number of panels configured is " + slideList.size());
  return slideList;
 }
 
 /**
  * This method returns style map containing type and class of panel.
  * 
  * @param panelMap
  *         the map of hero panel
  * @return style map
  */
 private Object getStyleMap(Map<String, Object> panelMap) {
  Map<String, Object> styleMap = new HashMap<String, Object>();
  styleMap.put(TYPE, MapUtils.getString(panelMap, TYPE, StringUtils.EMPTY));
  styleMap.put(CLASS, MapUtils.getString(panelMap, CLASS, StringUtils.EMPTY));
  
  return styleMap;
 }
 
}
