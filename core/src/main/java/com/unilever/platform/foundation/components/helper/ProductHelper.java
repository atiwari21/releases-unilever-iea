/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.components.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.api.CommerceException;
import com.adobe.cq.commerce.api.Product;
import com.adobe.cq.commerce.common.CommerceHelper;
import com.day.cq.commons.ImageResource;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.i18n.I18n;
import com.day.cq.tagging.Tag;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.unilever.platform.aem.foundation.commerce.api.CustomProduct;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfiguration;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.constants.ContainerTagConstants;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;
import com.unilever.platform.foundation.viewhelper.constants.StoreLocatorConstants;

/**
 * The Class ProductHelper.
 * 
 * @author nbhart
 */
public final class ProductHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ProductHelper.class);
 private static final String RATING_REVIEWS = "ratingReviews";
 
 private static final String CUSTOM_BODY_CLASS = "customBodyClass";
 
 private static final String PRODUCT_CATEGORY = "productCategory";
 
 private static final String BAZAARVOICE = "bazaarvoice";
 
 private static final String HEADER = "header";
 
 private static final String KRITIQUE = "kritique";
 
 private static final String SERVICE_PROVIDER_NAME = "serviceProviderName";
 
 /**
  * Instantiates a new product helper.
  */
 private ProductHelper() {
  
 }
 
 /**
  * The Constant LOGGER.
  * 
  * @param product
  *         the product
  * @param page
  *         the page
  * @param configurationService
  *         the configuration service
  * @param slingRequest
  *         the sling request
  * @param imageList
  * @return the product images list
  */
 public static Boolean getProductImagesList(Product product, Page page, ConfigurationService configurationService,
   SlingHttpServletRequest slingRequest, List<Map<String, Object>> imageList) {
  List<ImageResource> images = product.getImages();
  Boolean hasVideos = false;
  Map<String, Object> image;
  Map<String, Object> zoomImage = null;
  Map<String, Object> thumbImage = null;
  String videoId = StringUtils.EMPTY;
  for (ImageResource imageResource : images) {
   Boolean hasVideo = false;
   if (imageResource.getFileReference() != null) {
    String zoomImageSize = null;
    String thumbnailImageSuffix = null;
    image = new LinkedHashMap<String, Object>();
    zoomImage = new LinkedHashMap<String, Object>();
    thumbImage = new LinkedHashMap<String, Object>();
    try {
     zoomImageSize = configurationService.getConfigValue(page, ProductConstants.PRODUCTOVERVIEW, ProductConstants.ZOOM_IMAGE_SIZE);
    } catch (ConfigurationNotFoundException e) {
     LOGGER.error("could not find zoomImageSize key in ProductOverviewCategory category", e);
    }
    
    ImageHelper.addImageParameters(imageResource, image, imageResource.getFileReference(), page, slingRequest);
    ImageHelper.addZoomImageParameters(imageResource, zoomImage, imageResource.getFileReference(), page, zoomImageSize, slingRequest);
    ImageHelper.addImageParameters(imageResource, thumbImage, imageResource.getFileReference(), page, slingRequest);
    image.put(ProductConstants.ZOOM_MAP + "Image", zoomImage);
    try {
     thumbnailImageSuffix = configurationService.getConfigValue(page, ProductConstants.PRODUCTOVERVIEW, ProductConstants.THUMBNAIL_IMAGE_SUFFIX);
    } catch (ConfigurationNotFoundException e) {
     LOGGER.error("could not find thumbnailImageSuffix key in ProductOverview category", e);
    }
    if (StringUtils.isBlank(thumbnailImageSuffix)) {
     thumbnailImageSuffix = ".ulenscale.100x100.";
    }
    thumbImage.put(UnileverConstants.URL, thumbImage.get(UnileverConstants.URL) + thumbnailImageSuffix + thumbImage.getOrDefault("extension", "png"));
    image.put(ProductConstants.THUMB_IMAGE_LABEL, thumbImage);
    if (null != imageResource.adaptTo(ValueMap.class).get(ProductConstants.VIDEO_ID)) {
     videoId = imageResource.adaptTo(ValueMap.class).get(ProductConstants.VIDEO_ID).toString();
     if (StringUtils.isNotBlank(videoId)) {
      hasVideo = true;
      hasVideos = true;
      image.put("videoControls", getVideoControls(slingRequest, configurationService, page));
     }
     
    }
    image.put(ProductConstants.VIDEO_ID, videoId);
    image.put("hasVideo", hasVideo);
    if (image != null && !image.isEmpty()) {
     imageList.add(image);
    }
   }
   
  }
  
  return hasVideos;
 }
 
 /**
  * Gets the video controls.
  * 
  * @param configurationService
  *         the configuration service
  * @param page
  *         the page
  * @param slingRequest
  *         the slingRequest
  * @return the video control map
  */
 public static Map<String, Object> getVideoControls(SlingHttpServletRequest slingRequest, ConfigurationService configurationService, Page page) {
  Resource resource = slingRequest.getResource();
  Map<String, Object> videoControlsMap = new HashMap<String, Object>();
  try {
   if (resource != null) {
    String componentName = BaseViewHelper.getComponentName(resource, slingRequest);
    if (StringUtils.isNotBlank(componentName)) {
     try {
      configurationService.getCategoryConfiguration(page, componentName);
     } catch (ConfigurationNotFoundException e) {
      LOGGER.error("product images configuration does not exist", e);
      componentName = ProductConstants.PRODUCTOVERVIEW;
     }
    }
    videoControlsMap.put("displayInOverlay", Boolean.valueOf(configurationService.getConfigValue(page, componentName, "displayInOverlay")));
    videoControlsMap.put("autoplay", Boolean.valueOf(configurationService.getConfigValue(page, componentName, "autoplay")));
    videoControlsMap.put("hideVideoControls", Boolean.valueOf(configurationService.getConfigValue(page, componentName, "hideVideoControls")));
    videoControlsMap.put("muteVideoAudio", Boolean.valueOf(configurationService.getConfigValue(page, componentName, "muteVideoAudio")));
    videoControlsMap.put("loopVideoPlayback", Boolean.valueOf(configurationService.getConfigValue(page, componentName, "loopVideoPlayback")));
    videoControlsMap.put("relatedVideoAtEnd", Boolean.valueOf(configurationService.getConfigValue(page, componentName, "relatedVideoAtEnd")));
    videoControlsMap.put("videoProviderDetails", configurationService.getConfigValue(page, componentName, "videoProviderDetails"));
    
   }
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find keys in category videoControl" + e);
  }
  
  return videoControlsMap;
 }
 
 /**
  * Gets the uni product.
  * 
  * @param page
  *         the currentPage
  * @return the uni product
  */
 public static UNIProduct getUniProduct(Page page) {
  return getUniProduct(page, null);
 }
 
 /**
  * Gets the uni product.
  * 
  * @param page
  *         the currentPage
  * @param slingRequest
  *         the slingRequest
  * @return the uni product
  */
 public static UNIProduct getUniProduct(Page page, SlingHttpServletRequest slingRequest) {
  UNIProduct uniProduct = null;
  if (page != null) {
   Product product = CommerceHelper.findCurrentProduct(page);
   if (product instanceof UNIProduct) {
    uniProduct = (UNIProduct) product;
   }
   
   if (uniProduct == null && slingRequest != null) {
    String[] selectors = slingRequest.getRequestPathInfo().getSelectors();
    String productId = ProductHelperExt.getProductId(selectors) != null ? ProductHelperExt.getProductId(selectors) : slingRequest
      .getParameter(ProductConstants.EAN);
    return ProductHelperExt.findProduct(page, productId);
   }
  }
  return uniProduct;
 }
 
 /**
  * Gets the custom product images list.
  * 
  * @param product
  *         the product
  * @param page
  *         the page
  * @param slingRequest
  *         the sling request
  * @param configurationService
  *         the configurationService
  * @return the custom product images list
  */
 public static List<Map<String, Object>> getCustomProductImagesList(CustomProduct product, Page page, SlingHttpServletRequest slingRequest,
   ConfigurationService configurationService) {
  ImageResource imageResource = product.getCustomImage();
  List<Map<String, Object>> imageList = new ArrayList<Map<String, Object>>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  Map<String, Object> image;
  Boolean hasVideo = false;
  String videoId = StringUtils.EMPTY;
  if (imageResource != null && StringUtils.isNotBlank(imageResource.getFileReference())) {
   image = new LinkedHashMap<String, Object>();
   ImageHelper.addImageParameters(imageResource, image, imageResource.getFileReference(), page, slingRequest);
   if (null != imageResource.adaptTo(ValueMap.class).get(ProductConstants.VIDEO_ID)) {
    videoId = imageResource.adaptTo(ValueMap.class).get(ProductConstants.VIDEO_ID).toString();
    if (!StringUtils.isEmpty(videoId)) {
     hasVideo = true;
     image.put("videoControls", getVideoControls(slingRequest, configurationService, page));
    }
   }
   image.put(ProductConstants.VIDEO_ID, videoId);
   image.put("hasVideo", hasVideo);
   if (image != null && !image.isEmpty()) {
    imageList.add(image);
   }
  }
  
  return imageList;
 }
 
 /**
  * Reads the property from product object and return back the corresponding value.
  * 
  * @param product
  *         the product
  * @param propertyName
  *         the property name
  * @return property value.
  */
 public static String getProperty(Product product, String propertyName) {
  String propertyValue = StringUtils.EMPTY;
  if (product != null && StringUtils.isNotBlank(propertyName)) {
   propertyValue = product.getProperty(propertyName, String.class);
  }
  
  if (StringUtils.isBlank(propertyValue)) {
   propertyValue = StringUtils.EMPTY;
  }
  
  return propertyValue;
 }
 
 /**
  * Reads two property from product object and return back the corresponding value.
  * 
  * @param product
  *         the product
  * @param propertyOne
  *         the property one
  * @param propertyTwo
  *         the property two
  * @param separator
  *         the separator
  * @return the string
  */
 public static String concatProductProperties(Product product, String propertyOne, String propertyTwo, String separator) {
  String finalString = StringUtils.EMPTY;
  String propertyOneValue = getProperty(product, propertyOne);
  String propertyTwoValue = getProperty(product, propertyTwo);
  if (StringUtils.isNotEmpty(propertyTwoValue)) {
   if (StringUtils.isNotEmpty(propertyOneValue)) {
    finalString = propertyOneValue + separator + propertyTwoValue;
   } else {
    finalString = propertyTwoValue;
   }
  } else {
   finalString = propertyOneValue;
  }
  return finalString;
 }
 
 /**
  * Gets the product map.
  * 
  * @param productPage
  *         the product page
  * @param componentName
  *         the component name
  * @param resources
  *         the resources
  * @return the product map
  */
 public static Map<String, Object> getProductMap(Page productPage, String componentName, Map<String, Object> resources) {
  /** The product properties. */
  Map<String, Object> productProperties = new LinkedHashMap<String, Object>();
  SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) resources.get(ProductConstants.REQUEST);
  Resource currentResource = slingRequest.getResource();
  PageManager pageManager = ((ResourceResolver) resources.get("RESOURCE_RESOLVER")).adaptTo(PageManager.class);
  Page currentPage = pageManager.getContainingPage(currentResource);
  Product product = CommerceHelper.findCurrentProduct(productPage);
  UNIProduct currentProduct = null;
  if (product instanceof UNIProduct) {
   currentProduct = (UNIProduct) product;
  }
  
  if (currentProduct != null) {
   productProperties = getProductMap(currentProduct, componentName, resources, currentPage);
  }
  
  return productProperties;
 }
 
 /**
  * Gets the product map.
  * 
  * @param uniProduct
  *         the uni product
  * @param componentName
  *         the component name
  * @param resources
  *         the resources
  * @param currentPage
  *         the current page
  * @param i18n
  *         the i18n
  * @return the product map
  */
 public static Map<String, Object> getProductMap(UNIProduct uniProduct, String componentName, Map<String, Object> resources, Page currentPage) {
  return getProductMap(uniProduct, componentName, resources, currentPage, true);
  
 }
 
 /**
  * adds a property show info label to product details if variant has property show info label
  * 
  * @param productProperties
  * @param variants
  */
 public static void getProductInfoLabel(Map<String, Object> productProperties, Map<String, Object> variants) {
  if (variants.get(ProductConstants.SHOW_PRODUCTINFO_LABEL) != null) {
   productProperties.put(ProductConstants.SHOW_PRODUCTINFO_LABEL, true);
   variants.remove(ProductConstants.SHOW_PRODUCTINFO_LABEL);
  }
 }
 
 /**
  * Gets the zoom map properties.
  * 
  * @param zoomMapProperties
  *         the zoom map properties
  * @param configurationService
  *         the configuration service
  * @param i18n
  *         the i18n
  * @param page
  *         the page
  * @return the zoom map properties
  */
 public static Map<String, Object> getZoomMapProperties(Map<String, Object> zoomMapProperties, ConfigurationService configurationService, I18n i18n,
   Page page) {
  zoomMapProperties.put(ProductConstants.LABEL, i18n.get(ProductConstants.ZOOM_LABEL));
  
  try {
   zoomMapProperties.put(ProductConstants.ENABLED,
     configurationService.getConfigValue(page, ProductConstants.PRODUCTOVERVIEW, ProductConstants.IS_PRODUCT_OVERVIEW_ZOOM_ENABLED));
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find isProductOverviewZoomEnabled key in Zoom category", e);
   zoomMapProperties.put(ProductConstants.ENABLED, ProductConstants.FALSE);
  }
  LOGGER.debug("Zoom Map Properties : " + zoomMapProperties);
  return zoomMapProperties;
 }
 
 /**
  * Gets the static map properties.
  * 
  * @param staticMapProperties
  *         the static map properties
  * @param zoomMapProperties
  *         the zoom map properties
  * @param configurationService
  *         the configuration service
  * @param i18n
  *         the i18n
  * @param page
  *         the page
  * @return the static map properties
  */
 public static Map<String, Object> getStaticMapProperties(Map<String, Object> staticMapProperties, Map<String, Object> zoomMapProperties,
   ConfigurationService configurationService, I18n i18n, Page page) {
  staticMapProperties.put(ProductConstants.RETAIL, i18n.get(ProductConstants.RETAIL_LABEL));
  staticMapProperties.put(ProductConstants.SHOW_PRICE, ProductConstants.FALSE);
  staticMapProperties.put(ProductConstants.SIZE, StringUtils.EMPTY);
  try {
   staticMapProperties.put(ProductConstants.SIZE, configurationService.getConfigValue(page, ProductConstants.PRODUCTOVERVIEW, ProductConstants.SIZE));
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find size key in productOverview category", e);
   
  }
  String showPrice = ProductConstants.FALSE;
  try {
   showPrice = configurationService.getConfigValue(page, ProductConstants.PRODUCTOVERVIEW, ProductConstants.ISRRPENABLED);
   staticMapProperties.put(ProductConstants.SHOW_PRICE, showPrice);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find isRRPEnabled key in productOverview category", e);
   
  }
  staticMapProperties.put(ProductConstants.ZOOM_MAP, getZoomMapProperties(zoomMapProperties, configurationService, i18n, page));
  LOGGER.debug("Static Map Properties : " + staticMapProperties);
  return staticMapProperties;
 }
 
 /**
  * Gets the shop now properties.
  * 
  * @param configurationService
  *         the configuration service
  * @param page
  *         the page
  * @param i18n
  * @param resourceResolver
  *         the resourceResolver
  * @param slingRequest
  *         the slingRequest
  * @return the shop now properties
  */
 public static Map<String, String> getshopNowProperties(
 
 ConfigurationService configurationService, Page page, I18n i18n, ResourceResolver resourceResolver, SlingHttpServletRequest slingRequest) {
  String serviceProviderNameShopNow = null;
  Map<String, String> shopNowProperties = new HashMap<String, String>();
  try {
   serviceProviderNameShopNow = (configurationService.getConfigValue(page, ProductConstants.SHOPNOW_MAP, ProductConstants.SERVICE_PROVIDER_NAME));
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find serviceProviderName key in shopNow category", e);
   
  }
  
  try {
   shopNowProperties = configurationService.getCategoryConfiguration(page, serviceProviderNameShopNow);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find serviceProviderNameShopNow category", e);
   shopNowProperties.put(ProductConstants.ENABLED, ProductConstants.FALSE);
  }
  String shopNowI18NKey = shopNowProperties.get("ctaCopy");
  if (shopNowI18NKey != null) {
   shopNowProperties.put("ctaCopy", i18n.get(shopNowI18NKey));
  }
  shopNowProperties.put(ContainerTagConstants.PAGE_URL, ComponentUtil.getFullURL(resourceResolver, page.getPath(), slingRequest));
  LOGGER.debug("Shop Now Properties : " + shopNowProperties);
  return shopNowProperties;
 }
 
 /**
  * Gets product list .
  * 
  * @param productPagePathList
  *         the product page path list
  * @param resources
  *         the resources
  * @param componentName
  *         the component name
  * @param productRatingFlag
  *         the product rating flag
  * @return productList map.
  */
 public static List<Map<String, Object>> getProductList(List<String> productPagePathList, Map<String, Object> resources, String componentName,
   boolean productRatingFlag) {
  List<Map<String, Object>> productList = new ArrayList<Map<String, Object>>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  
  ResourceResolver resourceResolver = (ResourceResolver) resources.get(ProductConstants.RESOURCE_RESOLVER);
  Page currentPage = BaseViewHelper.getCurrentPage(resources);
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  I18n i18n = BaseViewHelper.getI18n(resources);
  for (String pagePath : productPagePathList) {
   String url = ComponentUtil.getFullURL(resourceResolver, pagePath, BaseViewHelper.getSlingRequest(resources));
   Map<String, Object> linkTextMap = new HashMap<String, Object>();
   linkTextMap.put(UnileverConstants.URL, url);
   Page productPage = pageManager.getPage(pagePath);
   UNIProduct uniProduct = getUniProduct(productPage);
   Map<String, Object> map = getProductMap(uniProduct, componentName, resources, currentPage, productRatingFlag);
   map.put(ProductConstants.TEXT_LINK, linkTextMap);
   Boolean isNewProduct = isNewProduct(productPage, currentPage);
   if (isNewProduct) {
    map.put(ProductConstants.ISNEWPRODUCT, i18n.get(ProductConstants.NEW_PRODUCT_LABEL));
   } else {
    map.put(ProductConstants.ISNEWPRODUCT, StringUtils.EMPTY);
   }
   
   productList.add(map);
   
  }
  
  return productList;
 }
 
 /**
  * Gets the product list.
  * 
  * @param productPagePathList
  *         the product page path list
  * @param resources
  *         the resources
  * @param componentName
  *         the component name
  * @return the product list
  */
 public static List<Map<String, Object>> getProductList(List<String> productPagePathList, Map<String, Object> resources, String componentName) {
  return getProductList(productPagePathList, resources, componentName, true);
 }
 
 /**
  * Gets the review map properties.
  * 
  * @param configurationService
  *         the configuration service
  * @param page
  *         the page
  * @return the review map properties
  */
 public static Map<String, String> getReviewMapProperties(
 
 ConfigurationService configurationService, Page page) {
  String isRatingAndReviewEnabled = null;
  String serviceProviderName = StringUtils.EMPTY;
  Map<String, String> reviewMapProperties = new HashMap<String, String>();
  try {
   Resource resource = page.getContentResource();
   InheritanceValueMap valueMap = new HierarchyNodeInheritanceValueMap(resource);
   if (valueMap != null) {
    serviceProviderName = (String) valueMap.getInherited(RATING_REVIEWS, StringUtils.EMPTY);
   }
   if (StringUtils.isBlank(serviceProviderName)) {
    serviceProviderName = configurationService.getConfigValue(page, ProductConstants.RATING_AND_REVIEWS,
      ProductConstants.SERVICE_PROVIDER_NAME);
   }
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find serviceProviderName key in serviceProviderNameShopNow category", e);
  }
  try {
   reviewMapProperties = configurationService.getCategoryConfiguration(page, serviceProviderName);
   GlobalConfiguration globalConfiguration = page.adaptTo(GlobalConfiguration.class);
   String[] excludedConfig = globalConfiguration.getCommonGlobalConfigValue("excludedSEOProperties").split(",");
   for (String excludeKey : excludedConfig) {
    if (reviewMapProperties.containsKey(excludeKey)) {
     reviewMapProperties.remove(excludeKey);
    }
   }
   isRatingAndReviewEnabled = configurationService.getConfigValue(page, ProductConstants.PRODUCTOVERVIEW,
     ProductConstants.IS_RATING_AND_REVIEW_ENABLED);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find isRatingAndReviewEnabled key in productOverview category", e);
  }
  reviewMapProperties.put(ProductConstants.ENABLED, isRatingAndReviewEnabled);
  if (BAZAARVOICE.equalsIgnoreCase(serviceProviderName) || serviceProviderName.startsWith(BAZAARVOICE)) {
   serviceProviderName = BAZAARVOICE;
  } else if (KRITIQUE.equalsIgnoreCase(serviceProviderName) || serviceProviderName.startsWith(KRITIQUE)) {
   serviceProviderName = KRITIQUE;
  }
  reviewMapProperties.put(SERVICE_PROVIDER_NAME, serviceProviderName);
  LOGGER.debug("Review Map Properties : " + reviewMapProperties);
  return reviewMapProperties;
 }
 
 /**
  * Gets the review map properties.
  * 
  * @param configurationService
  *         the configuration service
  * @param categoryName
  *         the category name
  * @param page
  *         the page
  * @return the review map properties
  */
 public static Map<String, String> getReviewMapProperties(ConfigurationService configurationService, Page page, String categoryName,
   String componentName) {
  
  GlobalConfiguration globalConfiguration = page.adaptTo(GlobalConfiguration.class);
  Map<String, String> reviewMapProperties = new HashMap<String, String>();
  
  String serviceProviderName = StringUtils.EMPTY;
  String isRatingAndReviewEnabled = StringUtils.EMPTY;
  String isRatingAndReviewComponentEnabled = StringUtils.EMPTY;
  
  InheritanceValueMap valueMap = new HierarchyNodeInheritanceValueMap(page.getContentResource());
  if (valueMap != null) {
   serviceProviderName = (String) valueMap.getInherited(RATING_REVIEWS, StringUtils.EMPTY);
  }
  
  if (StringUtils.isBlank(serviceProviderName)) {
   serviceProviderName = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, page, categoryName,
     ProductConstants.SERVICE_PROVIDER_NAME);
  }
  
  if (StringUtils.isNotBlank(serviceProviderName)) {
   reviewMapProperties = GlobalConfigurationUtility.getMapFromConfiguration(configurationService, page, serviceProviderName);
   String[] excludedConfig = globalConfiguration.getCommonGlobalConfigValue("excludedSEOProperties").split(",");
   for (String excludeKey : excludedConfig) {
    if (reviewMapProperties.containsKey(excludeKey)) {
     reviewMapProperties.remove(excludeKey);
    }
   }
  }
  
  isRatingAndReviewEnabled = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, page, categoryName, ProductConstants.ENABLED);
  
  if (StringUtils.isNotBlank(componentName)) {
   isRatingAndReviewComponentEnabled = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, page, componentName,
     ProductConstants.IS_RATING_AND_REVIEW_ENABLED);
  }
  
  if (StringUtils.isNotBlank(isRatingAndReviewComponentEnabled)) {
   isRatingAndReviewEnabled = isRatingAndReviewComponentEnabled;
  }
  if (BAZAARVOICE.equalsIgnoreCase(serviceProviderName) || serviceProviderName.startsWith(BAZAARVOICE)) {
   serviceProviderName = BAZAARVOICE;
  } else if (KRITIQUE.equalsIgnoreCase(serviceProviderName) || serviceProviderName.startsWith(KRITIQUE)) {
   serviceProviderName = KRITIQUE;
  }
  reviewMapProperties.put(SERVICE_PROVIDER_NAME, serviceProviderName);
  reviewMapProperties.put(ProductConstants.ENABLED, isRatingAndReviewEnabled);
  LOGGER.debug("Review Map Properties : " + reviewMapProperties);
  return reviewMapProperties;
 }
 
 /**
  * Gets the product data map.
  * 
  * @param product
  *         the product
  * @param page
  *         the page
  * @param configurationService
  *         the configuration service
  * @param resources
  *         the resources
  * @param slingRequest
  * @param i18n
  * @return the product data map
  */
 public static Map<String, Object> getProductDataMap(UNIProduct product, Page page, ConfigurationService configurationService,
   SlingHttpServletRequest slingRequest, I18n i18n) {
  
  Map<String, Object> details = new HashMap<String, Object>();
  List<Map<String, Object>> imageList = new ArrayList<Map<String, Object>>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  
  String price = product.getPrice();
  if (price == null) {
   price = "";
  }
  String currency = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, page, ProductConstants.CURRENCY,
    ProductConstants.CURRENCY_SYMBOL);
  
  if ((price != null) && "0.00".equals(price)) {
   details.put(ProductConstants.PRICE, "");
  } else {
   details.put(ProductConstants.PRICE, currency.concat(price));
   details.put(ProductConstants.RETAIL, i18n.get(ProductConstants.RETAIL_LABEL_VALUE));
  }
  
  details.put(ProductConstants.UNIT, assignProductPropertyValue(product.getSize()));
  details.put(ProductConstants.SKUID, assignProductPropertyValue(product.getSKU()));
  details.put(ProductConstants.SHORT_IDENTIFIER_VALUE, assignProductPropertyValue(product.getShortIdentifierValue()));
  details.put(ProductConstants.SMART_PRODUCT_ID, assignProductPropertyValue(product.getSmartProductId()));
  details.put(ProductConstants.PRODUCT_SIZE, assignProductPropertyValue(product.getProductSize()));
  details.put(ProductConstants.LABEL_INSIGHT_ID, assignProductPropertyValue(product.getLabelInsightId()));
  
  details.put(ProductConstants.LONG_PAGE_DESCRIPTION, assignProductPropertyValue(product.getLongPageDescription()));
  details.put(ProductConstants.ABOUT_THIS_PRODUCT_TITLE, assignProductPropertyValue(product.getAboutThisProductTitle()));
  details.put(ProductConstants.ABOUT_THIS_PRODUCT_BULLETS, assignProductPropertyValue(product.getAboutThisProductBullets()));
  details.put(ProductConstants.ABOUT_THIS_PRODUCT_DESCRIPTION, assignProductPropertyValue(product.getAboutThisProductDescription()));
  details.put(ProductConstants.PERFECT_FOR_TITLE, assignProductPropertyValue(product.getPerfectForTitle()));
  details.put(ProductConstants.PERFECT_FOR_HEADINGS_AND_DESCRIPTION, assignProductPropertyValue(product.getPerfectForHeadingsAndDescription()));
  details.put(ProductConstants.HOW_TO_USE_TITLE, assignProductPropertyValue(product.getHowToUseTitle()));
  details.put(ProductConstants.HOW_TO_USE_DESCRIPTION, assignProductPropertyValue(product.getHowToUseDescription()));
  details.put(ProductConstants.TRY_THIS_TITLE, assignProductPropertyValue(product.getTryThisTitle()));
  details.put(ProductConstants.TRY_THIS_DESCTIPTION, assignProductPropertyValue(product.getTryThisDesctiption()));
  details.put(ProductConstants.INGREDIENTS_DISCLAIMER, assignProductPropertyValue(product.getIngredientsDisclaimer()));
  details.put(ProductConstants.SHORT_PRODUCT_DESCRIPTION, assignProductPropertyValue(product.getShortProductDescription()));
  details.put(ProductConstants.EAN_PARENT, assignProductPropertyValue(product.getEANparent()));
  details.put(ProductConstants.PRODUCT_IMAGE, assignProductPropertyValue(product.getProductImage()));
  details.put(ProductConstants.ALT_TEXT_IMAGES, assignProductPropertyValue(product.getAltTextImages()));
  details.put(ProductConstants.IMAGE_TYPE, assignProductPropertyValue(product.getImageType()));
  details.put(ProductConstants.PRODUCT_ID, assignProductPropertyValue(product.getUniqueID()));
  details.put(ProductConstants.IDENTIFIER_TYPE, assignProductPropertyValue(product.getIdentifierType()));
  details.put(ProductConstants.IDENTIFIER_VALUE, assignProductPropertyValue(product.getIdentifierValue()));
  details.put(ProductConstants.EAN, assignProductPropertyValue(product.getSKU()));
  details.put(ProductConstants.ILS_PRODUCT_NAME, assignProductPropertyValue(product.getIlsProductName()));
  details.put(ProductConstants.NAME, assignProductPropertyValue(product.getName()));
  details.put(NameConstants.PN_SHORT_TITLE, product.getTitle());
  details.put(ProductConstants.SHORT_PAGE_DESCRIPTION, assignProductPropertyValue(product.getShortPageDescription()));
  details.put(ProductConstants.INGREDIENTS, assignProductPropertyValue(product.getIngredients()));
  details.put(ProductConstants.INGREDIENTS_ARRAY_FLAG, false);
  details.put(ProductConstants.PRODUCT_TAG, assignProductPropertyValue(product.getCqTags()));
  
  boolean hasVideos = ProductHelper.getProductImagesList(product, page, configurationService, slingRequest, imageList);
  details.put(ProductConstants.IMAGES, imageList.toArray());
  
  if (ProductHelperExt.isCustomizableProduct(product, configurationService, page)) {
   details.put(ProductConstants.CUSTOM_IMAGE, getCustomProductImagesList(product.getCustomProduct(), page, slingRequest, configurationService));
  }
  details.put("hasVideos", hasVideos);
  // Adding details related to Nutrition Facts -- Start
  details = addNutrionDetails(details, product, page, configurationService, i18n);
  details.put(ProductConstants.STORE_LOCATOR_RESULT_URL, getStoreLocatorUrl(product, page, slingRequest));
  details.put(ProductConstants.DISABLE_BUY_IT_NOW, assignProductPropertyBooleanValue(product.disableBuyItNow(), page, configurationService));
  LOGGER.debug("Product Data Map Properties : " + details);
  return details;
 }
 
 private static String assignProductPropertyValue(String property) {
  
  String value = StringUtils.EMPTY;
  if (StringUtils.isNotBlank(property)) {
   value = property;
  }
  return value;
 }
 
 private static boolean assignProductPropertyBooleanValue(String property, Page currentPage, ConfigurationService configurationService) {
  String serviceProviderName = StringUtils.EMPTY;
  String enabled = StringUtils.EMPTY;
  try {
   serviceProviderName = configurationService.getConfigValue(currentPage, ProductConstants.SHOPNOW_MAP, ProductConstants.SERVICE_PROVIDER_NAME);
   enabled = configurationService.getConfigValue(currentPage, serviceProviderName, ProductConstants.ENABLED);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find serviceProviderName key in shopNow or " + serviceProviderName + " category", e);
   
  }
  boolean value = false;
  if (("On".equalsIgnoreCase(property) || "Yes".equalsIgnoreCase(property) || "True".equalsIgnoreCase(property))
    || !("True".equalsIgnoreCase(enabled))) {
   value = true;
  }
  return value;
 }
 
 /**
  * Gets the store locator url.
  * 
  * @param product
  *         the product
  * @param page
  *         the page
  * @param slingRequest
  *         the sling request
  * @return the store locator url
  */
 public static String getStoreLocatorUrl(UNIProduct product, Page page, SlingHttpServletRequest slingRequest) {
  
  String storeLocatorResultUrl = StringUtils.EMPTY;
  ConfigurationService configurationService = page.adaptTo(ConfigurationService.class);
  ResourceResolver resourceResolver = page.getContentResource().getResourceResolver();
  String ctaUrl = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, page, StoreLocatorConstants.GLOBAL_CONFIG_CAT,
    StoreLocatorConstants.CTA_URL);
  
  String productId = product.getUniqueID();
  if (StringUtils.isBlank(productId)) {
   productId = product.getSKU();
  }
  
  String fullCtaUrl = ComponentUtil.getFullURL(resourceResolver, ctaUrl, slingRequest);
  storeLocatorResultUrl = fullCtaUrl.replaceAll(".html", "." + productId + ".html");
  LOGGER.debug("result Url : " + storeLocatorResultUrl);
  return storeLocatorResultUrl;
 }
 
 /**
  * Adds the nutrion details.
  * 
  * @param productdetails
  *         the productdetails
  * @param product
  *         the product
  * @param page
  *         the page
  * @param configurationService
  *         the configuration service
  * @param slingRequest
  *         the sling request
  * @return the map
  */
 private static Map<String, Object> addNutrionDetails(Map<String, Object> productdetails, UNIProduct product, Page page,
   ConfigurationService configurationService, I18n i18n) {
  
  Map<String, Object> nutritionMap = new HashMap<String, Object>();
  
  List<Map<String, Object>> productNameList = new ArrayList<Map<String, Object>>();
  List<Map<String, Object>> allergyList = new ArrayList<Map<String, Object>>();
  List<Map<String, Object>> ingredientsList = new ArrayList<Map<String, Object>>();
  List<Map<String, Object>> nutritionalTableList = new ArrayList<Map<String, Object>>();
  
  if (productdetails != null) {
   
   nutritionMap = productdetails;
   if (product.isPack()) {
    List<UNIProduct> childProductList = product.getChildProducts();
    for (UNIProduct uniProduct : childProductList) {
     setProductNameList(productNameList, uniProduct);
     setAllergyList(allergyList, i18n, uniProduct);
     setIngredientsList(ingredientsList, i18n, uniProduct);
     setNutritionalTableList(page, configurationService, nutritionalTableList, i18n, uniProduct);
    }
   } else {
    setProductNameList(productNameList, product);
    setAllergyList(allergyList, i18n, product);
    setIngredientsList(ingredientsList, i18n, product);
    setNutritionalTableList(page, configurationService, nutritionalTableList, i18n, product);
   }
  }
  if (!productNameList.isEmpty()) {
   nutritionMap.put(ProductConstants.PRODUCTNAMES, productNameList.toArray());
  }
  if (!allergyList.isEmpty()) {
   nutritionMap.put(ProductConstants.ALLERGY, allergyList.toArray());
   nutritionMap.put(ProductConstants.SHOW_PRODUCTINFO_LABEL, true);
  }
  if (!ingredientsList.isEmpty()) {
   nutritionMap.put(ProductConstants.INGREDIENTS, ingredientsList.toArray());
   nutritionMap.put(ProductConstants.SHOW_PRODUCTINFO_LABEL, true);
   nutritionMap.put(ProductConstants.INGREDIENTS_ARRAY_FLAG, true);
  }
  if (!nutritionalTableList.isEmpty()) {
   nutritionMap.put(ProductConstants.NUTRITIONFACTS, nutritionalTableList.toArray());
   nutritionMap.put(ProductConstants.SHOW_PRODUCTINFO_LABEL, true);
  }
  
  return nutritionMap;
 }
 
 /**
  * Gets the product name map.
  * 
  * @param product
  *         the product
  * @return the product name map
  */
 private static Map<String, Object> getProductNameMap(UNIProduct product) {
  Map<String, Object> productNameMap = new HashMap<String, Object>();
  productNameMap.put(ProductConstants.NAME, product.getName());
  return productNameMap;
 }
 
 /**
  * Gets the allergy map.
  * 
  * @param product
  *         the product
  * @param i18n
  *         the i18n
  * @return the allergy map
  */
 private static Map<String, Object> getAllergyMap(UNIProduct product, I18n i18n) {
  
  Map<String, Object> allergyMap = new HashMap<String, Object>();
  
  if (StringUtils.isNotBlank(product.getAllergy())) {
   allergyMap.put(ProductConstants.LABEL, i18n.get(ProductConstants.PRODUCT_INFO_PREFIX + ProductConstants.ALLERGY));
   allergyMap.put(ProductConstants.PRODUCT_NAME, product.getName());
   allergyMap.put(ProductConstants.DESCRIPTION, product.getAllergy());
  }
  return allergyMap;
 }
 
 /**
  * Gets the ingredients map.
  * 
  * @param product
  *         the product
  * @param i18n
  *         the i18n
  * @return the ingredients map
  */
 private static Map<String, Object> getIngredientsMap(UNIProduct product, I18n i18n) {
  
  Map<String, Object> ingredientsMap = new HashMap<String, Object>();
  
  if (StringUtils.isNotBlank(product.getIngredients())) {
   ingredientsMap.put(ProductConstants.LABEL, i18n.get(ProductConstants.PRODUCT_INFO_PREFIX + ProductConstants.INGREDIENTS));
   ingredientsMap.put(ProductConstants.PRODUCT_NAME, product.getName());
   ingredientsMap.put(ProductConstants.DESCRIPTION, product.getIngredients());
  }
  
  return ingredientsMap;
 }
 
 /**
  * Gets the nutritional table map.
  * 
  * @param product
  *         the product
  * @param i18n
  *         the i18n
  * @param page
  *         the page
  * @param configurationService
  *         the configuration service
  * @return the nutritio table map
  */
 private static Map<String, Object> getNutritioTableMap(UNIProduct product, I18n i18n, Page page, ConfigurationService configurationService) {
  
  Map<String, Object> nutritionMap = new HashMap<String, Object>();
  
  Map<String, Object> nutritionDetailsMap = product.getNutritionalFacts();
  Map<String, String> nutritionConfigurations = GlobalConfigurationUtility.getMapFromConfiguration(configurationService, page,
    ProductConstants.NUTRITIONAL_DETAILS);
  
  if ((nutritionConfigurations != null) && (nutritionDetailsMap != null)) {
   nutritionMap.put(ProductConstants.LABEL, i18n.get(ProductConstants.PRODUCT_INFO_PREFIX + "Nutrition"));
   nutritionMap.put(ProductConstants.PRODUCT_NAME, product.getName());
   nutritionMap.put(ProductConstants.DETAILS, getNutritionDetailsList(i18n, nutritionConfigurations, nutritionDetailsMap).toArray());
   nutritionMap.put(ProductConstants.DISCLAIMER, getValuefromMap(nutritionDetailsMap, "Disclaimer"));
   nutritionMap.put(ProductConstants.FACT_TABLE, getNutritionFactsMap(i18n, nutritionConfigurations, nutritionDetailsMap, page).toArray());
  }
  
  return nutritionMap;
 }
 
 /**
  * Gets the nutrition details list.
  * 
  * @param i18n
  *         the i18n
  * @param nutritionConfigurations
  *         the nutrition configurations
  * @param nutritionDetailsMap
  *         the nutrition details map
  * 
  * @return the nutrition details list
  */
 private static List<Map<String, String>> getNutritionDetailsList(I18n i18n, Map<String, String> nutritionConfigurations,
   Map<String, Object> nutritionDetailsMap) {
  
  List<Map<String, String>> nutritionalDetails = new ArrayList<Map<String, String>>();
  
  String[] properties = StringUtils.split(getStringValuefromMap(nutritionConfigurations, "lines"), ",");
  for (String property : properties) {
   Map<String, String> lineMap = new HashMap<String, String>();
   String propertyValue = getValuefromMap(nutritionDetailsMap, property);
   String disclaimerTranslation = i18n.get(ProductConstants.NUTRITIONAL_FACTS_PREFIX + property);
   lineMap.put("text", String.format(disclaimerTranslation + " %s", propertyValue));
   nutritionalDetails.add(lineMap);
  }
  
  return nutritionalDetails;
 }
 
 /**
  * Gets the nutrition facts map.
  * 
  * @param i18n
  *         the i18n
  * @param nutritionConfigurations
  *         the nutrition configurations
  * @param nutritionDetailsMap
  *         the nutrition details map
  * 
  * @return the nutrition facts map
  */
 private static List<Map<String, Object>> getNutritionFactsMap(I18n i18n, Map<String, String> nutritionConfigurations,
   Map<String, Object> nutritionDetailsMap, Page page) {
  
  List<String> excludePropertiesList = Arrays.asList(HEADER, "lines");
  List<Map<String, Object>> tablesList = new ArrayList<Map<String, Object>>();
  
  Map<String, Object> rowMap = new HashMap<String, Object>();
  rowMap.put("row", getTableRowMap(i18n, HEADER, getStringValuefromMap(nutritionConfigurations, HEADER), null).toArray());
  tablesList.add(rowMap);
  
  for (String key : excludePropertiesList) {
   nutritionConfigurations.remove(key);
  }
  
  Set<String> nutritionKeys = nutritionConfigurations.keySet();
  String sortingOrder = GlobalConfigurationUtility.getValueFromConfiguration(page.adaptTo(ConfigurationService.class), page, "sortOrderCategory",
    "productNutrients");
  List<String> sortingOrderList = Collections.emptyList();
  if (StringUtils.isNotBlank(sortingOrder)) {
   sortingOrderList = Arrays.asList(sortingOrder.split(","));
  }
  
  createTableList(tablesList, sortingOrderList, nutritionDetailsMap, nutritionConfigurations, i18n);
  nutritionKeys.removeAll(sortingOrderList);
  createTableList(tablesList, nutritionKeys, nutritionDetailsMap, nutritionConfigurations, i18n);
  return tablesList;
 }
 
 /**
  * 
  * @param tablesList
  * @param nutritionKeys
  * @param nutritionDetailsMap
  * @param nutritionConfigurations
  * @param i18n
  */
 public static void createTableList(List<Map<String, Object>> tablesList, Collection<String> nutritionKeys, Map<String, Object> nutritionDetailsMap,
   Map<String, String> nutritionConfigurations, I18n i18n) {
  for (String key : nutritionKeys) {
   if (nutritionConfigurations.containsKey(key)) {
    List<String> list = getPIMValues(i18n, nutritionConfigurations.get(key), nutritionDetailsMap);
    if (!list.isEmpty()) {
     Map<String, Object> rowMap = new HashMap<String, Object>();
     rowMap.put("row", getTableRowMap(i18n, key, nutritionConfigurations.get(key), nutritionDetailsMap).toArray());
     tablesList.add(rowMap);
    }
   }
  }
 }
 
 /**
  * Check blank PIM values.
  * 
  * @param i18n
  *         the i 18 n
  * @param key
  *         the key
  * @param values
  *         the values
  * @param nutritionDetailsMap
  *         the nutrition details map
  * @return the list
  */
 private static List<String> getPIMValues(I18n i18n, String values, Map<String, Object> nutritionDetailsMap) {
  
  List<String> rowMapList = new ArrayList<String>();
  
  String[] colName = StringUtils.split(values, ",");
  if (colName != null) {
   if (nutritionDetailsMap != null) {
    for (String propVal : colName) {
     String valueFromMap = getValuefromMap(nutritionDetailsMap, propVal);
     if (StringUtils.isNotBlank(valueFromMap)) {
      rowMapList.add(valueFromMap);
     }
    }
   } else {
    
    for (String propVal : colName) {
     rowMapList.add(i18n.get(ProductConstants.PRODUCT_INFO_PREFIX + propVal).toString());
    }
   }
  }
  
  return rowMapList;
 }
 
 /**
  * Gets the valuefrom map.
  * 
  * @param map
  *         the map
  * @param key
  *         the key
  * @return the valuefrom map
  */
 private static String getValuefromMap(Map<String, Object> map, String key) {
  
  String value = StringUtils.EMPTY;
  if (map.containsKey(key)) {
   value = map.get(key).toString();
  }
  return value;
 }
 
 /**
  * Gets the string valuefrom map.
  * 
  * @param map
  *         the map
  * @param key
  *         the key
  * @return the string valuefrom map
  */
 private static String getStringValuefromMap(Map<String, String> map, String key) {
  
  String value = StringUtils.EMPTY;
  if (map.containsKey(key)) {
   value = map.get(key).toString();
  }
  return value;
 }
 
 /**
  * Gets the table row map.
  * 
  * @param i18n
  *         the i18n
  * @param key
  *         the key
  * @param values
  *         the values
  * @param nutritionDetailsMap
  *         the nutrition details map
  * @return the table row map
  */
 private static List<String> getTableRowMap(I18n i18n, String key, String values, Map<String, Object> nutritionDetailsMap) {
  List<String> rowMapList = new ArrayList<String>();
  
  String[] colName = StringUtils.split(values, ",");
  String rowName = i18n.get(ProductConstants.PRODUCT_INFO_PREFIX + key);
  if (colName != null) {
   rowMapList.add(rowName);
   if (nutritionDetailsMap != null) {
    for (String propVal : colName) {
     rowMapList.add(getValuefromMap(nutritionDetailsMap, propVal));
    }
   } else {
    
    for (String propVal : colName) {
     rowMapList.add(i18n.get(ProductConstants.PRODUCT_INFO_PREFIX + propVal).toString());
    }
   }
  }
  
  return rowMapList;
 }
 
 /**
  * Gets the product ratings map.
  * 
  * @param product
  *         the product
  * @param page
  *         the page
  * @param configurationService
  *         the configuration service
  * @param componentName
  *         the component name
  * @return the product ratings map
  */
 public static Map<String, Object> getProductRatingsMap(UNIProduct product, Page page, ConfigurationService configurationService, String componentName) {
  Map<String, Object> details = new HashMap<String, Object>();
  
  String uniqueId = product.getSKU();
  String entityType = ProductConstants.PRODUCT_MAP;
  String viewType = StringUtils.EMPTY;
  
  if (StringUtils.isBlank(uniqueId)) {
   uniqueId = StringUtils.EMPTY;
  }
  
  try {
   if (componentName != null) {
    viewType = configurationService.getConfigValue(page, ProductConstants.KQ_VIEW_TYPE, componentName);
   }
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Currency not found in global configurations", e);
  }
  
  details.put(ProductConstants.RATING_UNIQUE_ID, uniqueId);
  details.put(ProductConstants.RATING_IDENTIFIER_TYPE, ProductHelper.getProperty(product, ProductConstants.RATING_IDENTIFIER_TYPE));
  details.put(ProductConstants.RATING_IDENTIFIER_VALUE, ProductHelper.getProperty(product, ProductConstants.RATING_IDENTIFIER_VALUE));
  details.put(ProductConstants.RATING_ENTITY_TYPE, entityType);
  details.put(ProductConstants.RATING_VIEW_TYPE, viewType);
  
  LOGGER.debug("Product Ratings Map Properties : " + details);
  
  return details;
 }
 
 /**
  * Sets the modal product details.
  * 
  * @param currentProduct
  *         the current product
  * @param productProperties
  *         the product properties
  * @param i18n
  *         the i18n
  * @param page
  *         the page
  * @param slingRequest
  *         the sling request
  */
 private static void setModalProductDetails(UNIProduct currentProduct, Map<String, Object> productProperties, Page page, Map<String, Object> resources) {
  ConfigurationService configurationService = page.adaptTo(ConfigurationService.class);
  
  String currency = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, page, ProductConstants.CURRENCY,
    ProductConstants.CURRENCY_SYMBOL);
  
  String enableCustomizationBadges = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, page,
    UnileverConstants.EGIFTING_CONFIG_CAT, "enableCustomizationBadges");
  
  String customBodyClass = page.getProperties().containsKey(CUSTOM_BODY_CLASS) ? page.getProperties().get(CUSTOM_BODY_CLASS).toString()
    : StringUtils.EMPTY;
  
  boolean flag = ProductHelperExt.isCustomizableProduct(currentProduct, configurationService, page);
  CustomProduct customProduct = currentProduct.getCustomProduct();
  
  productProperties.put(ProductConstants.TITLE, currentProduct.getName());
  productProperties.put(PRODUCT_CATEGORY, getCategoryId(configurationService, currentProduct, page));
  productProperties.put(CUSTOM_BODY_CLASS, customBodyClass);
  productProperties.put(ProductConstants.ILS_PRODUCT_NAME, currentProduct.getIlsProductName());
  productProperties.put(ProductConstants.CATEGORY, currentProduct.getCategory());
  productProperties.put(ProductConstants.DESCRIPTION, currentProduct.getShortPageDescription());
  productProperties.put(ProductConstants.PRODUCT_ID, currentProduct.getSKU());
  productProperties.put(ProductConstants.CURRENCY_MAP, currency);
  productProperties.put("enableCustomizationBadges", Boolean.valueOf(enableCustomizationBadges));
  productProperties.put("isCustomizableProduct", flag);
  productProperties.put("personalizableLabel", BaseViewHelper.getI18n(resources).get(ProductConstants.PERSONALIZABLE));
  productProperties.put(ProductConstants.AWARDS, AwardHelper.getProductAwards(currentProduct, page, configurationService, resources).toArray());
  
  if (flag && customProduct != null) {
   productProperties.putAll(ProductHelperExt.getCustomiseDataMap(customProduct));
   productProperties.put(ProductConstants.GIFT_CONFIGURATOR,
     ProductHelperExt.getProductGiftMap(currentProduct, page, BaseViewHelper.getSlingRequest(resources)));
  }
 }
 
 /**
  * Get the category Id.
  * 
  * @param configurationService
  *         the configurationService
  * @param currentProduct
  *         the currentProduct
  * @param page
  *         the page
  * @return
  */
 public static String getCategoryId(ConfigurationService configurationService, UNIProduct currentProduct, Page page) {
  final Resource jcrResource = page.getContentResource();
  Tag[] tags = currentProduct.getTags(jcrResource.getResourceResolver());
  String[] categoryNamespace = getCategoryNamespace(configurationService, page);
  String categoryId = null;
  List<Tag> namespaceTags = new ArrayList<Tag>();
  
  for (String categoryName : categoryNamespace) {
   Tag namespaceTag = getMatchingTag(tags, StringUtils.trim(categoryName));
   if (namespaceTag != null) {
    namespaceTags.add(namespaceTag);
   }
  }
  
  if (!namespaceTags.isEmpty()) {
   Tag tag = namespaceTags.get(0);
   categoryId = tag.getTitle(BaseViewHelper.getLocale(page));
   
  }
  
  return categoryId;
 }
 
 private static Tag getMatchingTag(Tag[] tags, String namespace) {
  Tag matchingTag = null;
  if (tags != null) {
   for (Tag tag : tags) {
    if (StringUtils.startsWithIgnoreCase(tag.getLocalTagID(), namespace) && !StringUtils.equalsIgnoreCase(tag.getLocalTagID(), namespace)) {
     matchingTag = tag;
     break;
    }
   }
  }
  return matchingTag;
 }
 
 private static String[] getCategoryNamespace(ConfigurationService configurationService, Page page) {
  String[] namespace = ArrayUtils.EMPTY_STRING_ARRAY;
  try {
   Map<String, String> config = configurationService.getCategoryConfiguration(page, PRODUCT_CATEGORY);
   if (config.get("categoryNamespace") != null) {
    String categoryNamespace = (String) config.get("categoryNamespace");
    if (StringUtils.isNotBlank(categoryNamespace)) {
     namespace = StringUtils.split(categoryNamespace, ",");
    }
   }
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Could not get config for {}", "productCategory", e);
  }
  return namespace;
 }
 
 /**
  * Gets the product list with tag.
  * 
  * @param productPagePathList
  *         the product page path list
  * @param resources
  *         the resources
  * @param jsonNameSapce
  *         the json name sapce
  * @param ctaLabel
  *         the cta label
  * @return the product list with tag
  */
 public static List<Map<String, Object>> getProductListWithTag(List<String> productPagePathList, Map<String, Object> resources, String jsonNameSapce,
   String ctaLabel) {
  List<Map<String, Object>> productList = new ArrayList<Map<String, Object>>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  
  ResourceResolver resourceResolver = (ResourceResolver) resources.get(ProductConstants.RESOURCE_RESOLVER);
  SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) resources.get(ProductConstants.REQUEST);
  Resource compResource = slingRequest.getResource();
  
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  Page currentPage = pageManager.getContainingPage(compResource);
  I18n i18n = BaseViewHelper.getI18n(resources);
  for (String pagePath : productPagePathList) {
   String url = ComponentUtil.getFullURL(resourceResolver, pagePath, BaseViewHelper.getSlingRequest(resources));
   Page productPage = pageManager.getPage(pagePath);
   
   Product product = CommerceHelper.findCurrentProduct(productPage);
   UNIProduct currentProduct = null;
   if (product instanceof UNIProduct) {
    currentProduct = (UNIProduct) product;
   }
   
   Map<String, Object> linkTextMap = new HashMap<String, Object>();
   if (StringUtils.isBlank(ctaLabel)) {
    linkTextMap.put(ProductConstants.URL, url);
    linkTextMap.put(ProductConstants.LABEL, i18n.get(ProductConstants.LINK_TEXT_LABEL));
    
   } else {
    linkTextMap = getCrossaleLinkTextMap(linkTextMap, ctaLabel, currentProduct, resourceResolver, compResource.getValueMap(),
      BaseViewHelper.getSlingRequest(resources));
   }
   Map<String, Object> map = getProductMap(currentProduct, jsonNameSapce, resources, currentPage);
   LOGGER.debug("Product Page List{} Current Product {}", map);
   if (MapUtils.isNotEmpty(map)) {
    
    map.put(ProductConstants.TEXT_LINK, linkTextMap);
    map.put(ProductConstants.PRODUCT_TAG, getProductTagListProperties(productPage).toArray());
    map.put(ProductConstants.QUICK_VIEW_LABEL, i18n.get(ProductConstants.QUICK_VIEW));
    if (map.get(ProductConstants.SHOW_PRODUCTINFO_LABEL) != null) {
     map.put(ProductConstants.PRODUCT_INFOLABEL, i18n.get(ProductConstants.PRODUCT_INFO_LABEL));
     map.remove(ProductConstants.SHOW_PRODUCTINFO_LABEL);
    }
    Boolean isNewProduct = isNewProduct(productPage, currentPage);
    if (isNewProduct) {
     map.put(ProductConstants.ISNEWPRODUCT, i18n.get(ProductConstants.NEW_PRODUCT_LABEL));
    } else {
     map.put(ProductConstants.ISNEWPRODUCT, StringUtils.EMPTY);
    }
    productList.add(map);
   }
  }
  
  return productList;
 }
 
 /**
  * Gets the crossale link text map.
  * 
  * @param linkTextMap
  *         the link text map
  * @param ctaLabel
  *         the cta label
  * @param currentProduct
  *         the current product
  * @param resourceResolver
  *         the resource resolver
  * @param valueMap
  *         the value map
  * @return the crossale link text map
  */
 private static Map<String, Object> getCrossaleLinkTextMap(Map<String, Object> linkTextMap, String ctaLabel, UNIProduct currentProduct,
   ResourceResolver resourceResolver, ValueMap valueMap, SlingHttpServletRequest slingRequest) {
  String sku = currentProduct.getSKU();
  String url = MapUtils.getString(valueMap, "navigationURL");
  Map<String, String> urlParametersMap = new LinkedHashMap<String, String>();
  urlParametersMap.put(ProductConstants.EAN, sku);
  String navigationFullUrl = StringUtils.isNotBlank(url) ? addParametersToURL(ComponentUtil.getFullURL(resourceResolver, url, slingRequest),
    urlParametersMap) : StringUtils.EMPTY;
  linkTextMap.put(ProductConstants.URL, navigationFullUrl);
  linkTextMap.put(ProductConstants.LABEL, ctaLabel);
  return linkTextMap;
 }
 
 /**
  * Gets the navigationFullUrl.
  * 
  * @param URL
  *         the navigation URL
  * @param urlParametersMap
  * @return the navigationFullUrl
  */
 private static String addParametersToURL(String url, Map<String, String> urlParametersMap) {
  StringBuilder stringBuilder = new StringBuilder();
  Set<String> set = urlParametersMap.keySet();
  Iterator<String> iterator = set.iterator();
  String navigationFullUrl = StringUtils.EMPTY;
  int counter = 0;
  while (iterator.hasNext()) {
   String urlParameterKey = iterator.next();
   String urlParameterValue = urlParametersMap.get(urlParameterKey);
   if (counter == 0) {
    stringBuilder.append(urlParameterKey).append("=").append(urlParameterValue);
   } else {
    stringBuilder.append("&").append(urlParameterKey).append("=").append(urlParameterValue);
   }
   counter++;
  }
  if (stringBuilder.length() > 0) {
   if (StringUtils.contains(url, "?")) {
    navigationFullUrl = url + "&" + stringBuilder.toString();
   } else {
    navigationFullUrl = url + "?" + stringBuilder.toString();
   }
  }
  return navigationFullUrl;
 }
 
 /**
  * Gets the product tag list properties.
  * 
  * @param productPage
  *         the product page
  * @return the product tag list properties
  */
 private static List<Map<String, String>> getProductTagListProperties(Page productPage) {
  Tag[] tags = productPage.getTags();
  List<Map<String, String>> productTagListProperties = new ArrayList<Map<String, String>>();
  if (tags != null) {
   for (Tag record : tags) {
    if (record != null) {
     Map<String, String> tagProperties = new HashMap<String, String>();
     tagProperties.put(ProductConstants.TAG_ID, record.getTagID());
     tagProperties.put(ProductConstants.NAME, record.getName());
     tagProperties.put(ProductConstants.TITLE, record.getTitle(BaseViewHelper.getLocale(productPage)));
     tagProperties.put(ProductConstants.TAG_PATH, record.getPath());
     productTagListProperties.add(tagProperties);
    }
   }
  }
  return productTagListProperties;
 }
 
 /**
  * Checks if is new product.
  * 
  * @param productPage
  *         the product page
  * @param currentPage
  *         the current page
  * @return the boolean
  */
 public static Boolean isNewProduct(Page productPage, Page currentPage) {
  ConfigurationService configurationService = currentPage.adaptTo(ConfigurationService.class);
  boolean isNewProduct = false;
  UNIProduct currentProduct = (UNIProduct) CommerceHelper.findCurrentProduct(productPage);
  int configLaunchDateLimit = ProductConstants.DAYS_DIFF;
  try {
   String temp = configurationService.getConfigValue(currentPage, UnileverConstants.PRODUCT_CATEGORY,
     UnileverConstants.IS_NEW_PRODUCT_LAUNCH_DATE_LIMIT);
   configLaunchDateLimit = StringUtils.isNotBlank(temp) ? Integer.parseInt(temp) : configLaunchDateLimit;
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Configuration Not found for Category: " + UnileverConstants.PRODUCT_CATEGORY + " and key: "
     + UnileverConstants.IS_NEW_PRODUCT_LAUNCH_DATE_LIMIT);
   LOGGER.error(ExceptionUtils.getStackTrace(e));
  }
  
  if (currentProduct != null && currentProduct.getProductionDate() != null) {
   Date releaseDate = currentProduct.getProductionDate();
   long releaseTime = releaseDate.getTime();
   Date currentDate = new Date();
   long currentTime = currentDate.getTime();
   long diff = currentTime - releaseTime;
   long diffDays = diff / ProductConstants.MULTIPLICATION_CONSTANT;
   if (diffDays <= configLaunchDateLimit) {
    isNewProduct = true;
   }
  }
  return isNewProduct;
 }
 
 /**
  * Gets the product container tag map.
  * 
  * @param productPage
  *         the product page
  * @param currentProduct
  *         the current product
  * @return the product container tag map
  */
 public static Map<String, Object> getProductContainerTagMap(Page productPage, UNIProduct currentProduct) {
  Map<String, Object> map = new LinkedHashMap<String, Object>();
  
  Map<String, Object> productInfoMap = ComponentUtil.getContainerTagMap(productPage, ContainerTagConstants.PRODUCT_INFO, currentProduct.getName());
  productInfoMap.putAll(getProductOtherAttributesMap(currentProduct));
  map.put(ContainerTagConstants.PRODUCT_LOAD, productInfoMap);
  Map<String, Object> productClickMap = getProductClickMap(productPage, currentProduct);
  map.put(ContainerTagConstants.PRODUCT_CLICK, productClickMap);
  
  Map<String, Object> productQuickViewMap = getProductQuickViewMap(productPage, currentProduct);
  map.put(ContainerTagConstants.PRODUCT_QUICKVIEW, productQuickViewMap);
  
  Map<String, Object> productImpressionMap = getProductImpressionMap(productPage, currentProduct);
  
  map.put(ContainerTagConstants.PRODUCT_IMPRESSION, productImpressionMap);
  
  Map<String, Object> shopNowMap = getShopNowMap(productPage, currentProduct);
  map.put(ContainerTagConstants.SHOP_NOW, shopNowMap);
  
  Map<String, Object> ratingReview = ComponentUtil.getContainerTagMap(productPage, ContainerTagConstants.RATING_REVIEW, currentProduct.getName());
  map.put(ContainerTagConstants.RATING_REVIEW, ratingReview);
  
  map.put(ContainerTagConstants.DROPDOWN,
    ComponentUtil.getContainerTagMap(productPage, ContainerTagConstants.DROPDOWN, ContainerTagConstants.DROPDOWN));
  
  map.put(ContainerTagConstants.ZOOM, getZoomMap(productPage, currentProduct));
  
  return map;
  
 }
 
 /**
  * Gets the product click map.
  * 
  * @param productPage
  *         the product page
  * @param currentProduct
  *         the current product
  * @return the product click map
  */
 private static Map<String, Object> getProductClickMap(Page productPage, UNIProduct currentProduct) {
  return ComponentUtil.getContainerTagMap(productPage, ContainerTagConstants.PRODUCT_CLICK, getProductMapForAnalytics(productPage, currentProduct));
 }
 
 /**
  * Gets the product quick view map.
  * 
  * @param productPage
  *         the product page
  * @param currentProduct
  *         the current product
  * @return the product quick view map
  */
 private static Map<String, Object> getProductQuickViewMap(Page productPage, UNIProduct currentProduct) {
  return ComponentUtil.getContainerTagMap(productPage, ContainerTagConstants.PRODUCT_QUICKVIEW,
    getProductMapForAnalytics(productPage, currentProduct));
  
 }
 
 /**
  * Gets the zoom map.
  * 
  * @param productPage
  *         the product page
  * @param currentProduct
  *         the current product
  * @return the zoom map
  */
 private static Map<String, Object> getZoomMap(Page productPage, UNIProduct currentProduct) {
  Map<String, Object> containerTagvalueMap = ComponentUtil.getContainerTagMap(productPage, "productDetailZoom", ContainerTagConstants.ZOOM);
  containerTagvalueMap.putAll(getProductOtherAttributesMap(currentProduct));
  return containerTagvalueMap;
 }
 
 /**
  * Gets the product impression map.
  * 
  * @param productPage
  *         the product page
  * @param currentProduct
  *         the current product
  * @return the product impression map
  */
 private static Map<String, Object> getProductImpressionMap(Page productPage, UNIProduct currentProduct) {
  return ComponentUtil.getContainerTagMap(productPage, ContainerTagConstants.PRODUCT_IMPRESSION,
    getProductMapForAnalytics(productPage, currentProduct));
 }
 
 /**
  * Gets the shop now map.
  * 
  * @param productPage
  *         the product page
  * @param currentProduct
  *         the current product
  * @return the shop now map
  */
 public static Map<String, Object> getShopNowMap(Page productPage, UNIProduct currentProduct) {
  Map<String, Object> containerTagvalueMap = ComponentUtil.getContainerTagMap(productPage, ContainerTagConstants.SHOP_NOW,
    getProductMapForAnalytics(productPage, currentProduct));
  containerTagvalueMap.putAll(getProductOtherAttributesMap(currentProduct));
  return containerTagvalueMap;
 }
 
 /**
  * Gets the product map for analytics.
  * 
  * @param productPage
  *         the product page
  * @param currentProduct
  *         the current product
  * @return the product map for analytics
  */
 private static Map<String, Object> getProductMapForAnalytics(Page productPage, UNIProduct currentProduct) {
  Map<String, String> productInfoMap = new HashMap<String, String>();
  productInfoMap.put(ContainerTagConstants.PRODUCT_ID, currentProduct.getSKU());
  productInfoMap.put(ContainerTagConstants.PRODUCT_NAME, currentProduct.getName());
  productInfoMap.put(ContainerTagConstants.PRICE, currentProduct.getPrice());
  productInfoMap.put(ContainerTagConstants.BRAND, ComponentUtil.getBrandName(productPage));
  productInfoMap.put(ContainerTagConstants.QAUNTITY, currentProduct.getSize());
  
  Map<String, String> category = new HashMap<String, String>();
  category.put(ContainerTagConstants.PRIMARY_CATEGORY, currentProduct.getCategory());
  
  Map<String, String> attributes = new HashMap<String, String>();
  
  attributes.put(ContainerTagConstants.PRODUCT_VARIATIONS, getProductVarients(currentProduct));
  attributes.put(ContainerTagConstants.LIST_POSITION, "");
  
  Map<String, Object> product = new HashMap<String, Object>();
  product.put(ContainerTagConstants.PRODUCT_INFO, productInfoMap);
  product.put(ContainerTagConstants.CATEGORY, category);
  product.put(ContainerTagConstants.ATTRIBUTES, attributes);
  return product;
 }
 
 /**
  * Gets the product other attributes map.
  * 
  * @param currentProduct
  *         the current product
  * @return the product other attributes map
  */
 private static Map<String, Object> getProductOtherAttributesMap(UNIProduct currentProduct) {
  Map<String, Object> otherAttrsMap = new LinkedHashMap<String, Object>();
  otherAttrsMap.put(ContainerTagConstants.PRODUCT_ID, currentProduct.getSKU());
  otherAttrsMap.put(ContainerTagConstants.PRIMARY_CATEGORY, currentProduct.getCategory());
  otherAttrsMap.put(ContainerTagConstants.PRODUCT_VARIATIONS, getProductVarients(currentProduct));
  return otherAttrsMap;
  
 }
 
 /**
  * Gets the product varients.
  * 
  * @param currentProduct
  *         the current product
  * @return the product varients
  */
 public static String getProductVarients(UNIProduct currentProduct) {
  String variants = StringUtils.EMPTY;
  try {
   Iterator<Product> varientsItr = currentProduct.getVariants();
   while (varientsItr.hasNext()) {
    Product p = varientsItr.next();
    if (p instanceof UNIProduct) {
     UNIProduct variantProduct = (UNIProduct) p;
     variants = variants + variantProduct.getSize() + ",";
    }
   }
   variants = variants.substring(0, variants.lastIndexOf(","));
  } catch (CommerceException e) {
   LOGGER.error("Error in getting variants ", e);
  }
  return variants;
  
 }
 
 /**
  * Sets product name in list.
  * 
  * @param productNameList
  *         the product name list
  * @param uniProduct
  *         the uni product
  */
 private static void setProductNameList(List<Map<String, Object>> productNameList, UNIProduct uniProduct) {
  Map<String, Object> nameMap = getProductNameMap(uniProduct);
  if (!nameMap.isEmpty()) {
   productNameList.add(nameMap);
  }
 }
 
 /**
  * Sets Allergy in list.
  * 
  * @param allergyList
  *         the allergy list
  * @param i18n
  *         the i18n
  * @param uniProduct
  *         the uni product
  */
 private static void setAllergyList(List<Map<String, Object>> allergyList, I18n i18n, UNIProduct uniProduct) {
  Map<String, Object> allergyMap = getAllergyMap(uniProduct, i18n);
  if (!allergyMap.isEmpty()) {
   allergyList.add(allergyMap);
  }
 }
 
 /**
  * Sets ingredients in list.
  * 
  * @param ingredientsList
  *         the ingredients list
  * @param i18n
  *         the i18n
  * @param uniProduct
  *         the uni product
  */
 private static void setIngredientsList(List<Map<String, Object>> ingredientsList, I18n i18n, UNIProduct uniProduct) {
  
  Map<String, Object> ingredientsMap = getIngredientsMap(uniProduct, i18n);
  if (!ingredientsMap.isEmpty()) {
   ingredientsList.add(ingredientsMap);
  }
 }
 
 /**
  * Sets nutritional table in list.
  * 
  * @param page
  *         the page
  * @param configurationService
  *         the configuration service
  * @param nutritionalTableList
  *         the nutritional table list
  * @param i18n
  *         the i18n
  * @param uniProduct
  *         the uni product
  */
 private static void setNutritionalTableList(Page page, ConfigurationService configurationService, List<Map<String, Object>> nutritionalTableList,
   I18n i18n, UNIProduct uniProduct) {
  Map<String, Object> tableMap = getNutritioTableMap(uniProduct, i18n, page, configurationService);
  if (!tableMap.isEmpty()) {
   nutritionalTableList.add(tableMap);
  }
 }
 
 /**
  * Gets the product map.
  * 
  * @param uniProduct
  *         the uni product
  * @param componentName
  *         the component name
  * @param resources
  *         the resources
  * @param currentPage
  *         the current page
  * @param i18n
  *         the i18n
  * @param productRatingFlag
  *         the product rating flag
  * @return the product map
  */
 public static Map<String, Object> getProductMap(UNIProduct uniProduct, String componentName, Map<String, Object> resources, Page currentPage,
   boolean productRatingFlag) {
  /** The product properties. */
  Map<String, Object> productProperties = new LinkedHashMap<String, Object>();
  SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) resources.get(ProductConstants.REQUEST);
  I18n i18n = BaseViewHelper.getI18n(resources);
  if (uniProduct != null) {
   
   LOGGER.info("currentProduct data logged");
   ConfigurationService configurationService = currentPage.adaptTo(ConfigurationService.class);
   setModalProductDetails(uniProduct, productProperties, currentPage, resources);
   List<Map<String, Object>> variantList = new ArrayList<Map<String, Object>>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
   String productSkuId = uniProduct.getSKU();
   Map<String, Object> productData = getProductDataMap(uniProduct, currentPage, configurationService, slingRequest, i18n);
   getProductInfoLabel(productProperties, productData);
   variantList.add(productData);
   Map<String, Object> variants = null;
   try {
    Iterator<Product> iterator = uniProduct.getVariants();
    
    while (iterator.hasNext()) {
     
     UNIProduct productVariant = (UNIProduct) iterator.next();
     String productVariantSkuId = productVariant.getSKU();
     
     if (StringUtils.isNotBlank(productSkuId) && StringUtils.isNotBlank(productVariantSkuId) && !productSkuId.equals(productVariantSkuId)) {
      variants = getProductDataMap(productVariant, currentPage, configurationService, slingRequest, i18n);
      getProductInfoLabel(productProperties, variants);
      variantList.add(variants);
     }
    }
    
   } catch (CommerceException ce) {
    LOGGER.error("CommerceException while getting the product variants:", ce);
   }
   
   Map<String, Object> ratingsMap = getProductRatingsMap(uniProduct, currentPage, configurationService, componentName);
   Map<String, Object> containerTagMap = getProductContainerTagMap(currentPage, uniProduct);
   if (productRatingFlag) {
    productProperties.put(ProductConstants.PRODUCT_RATINGS_MAP, ratingsMap);
   }
   
   productProperties.put(ProductConstants.PRODUCT_DETAIL_MAP, variantList.toArray());
   
   productProperties.put(UnileverConstants.CONTAINER_TAG, containerTagMap);
   Boolean isStoreLocatorEnabled = StoreLocatorUtility.isStoreLocatorEnabled(currentPage, configurationService, componentName);
   if (isStoreLocatorEnabled) {
    productProperties.put("storeLocator", StoreLocatorUtility.getStoreLocatorMap(resources, configurationService, componentName));
   }
  }
  
  return productProperties;
  
 }
 
 /**
  * Checks if is price enabled
  * 
  * @param configurationService
  *         the configuration service
  * @param currentPage
  *         the current page
  * @return the boolean
  */
 public static Boolean isPriceEnabled(ConfigurationService configurationService, Page currentPage) {
  
  Boolean enabled = false;
  try {
   enabled = Boolean.parseBoolean(configurationService.getConfigValue(currentPage, ProductConstants.PRICE_CATEGORY, ProductConstants.ENABLED));
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find enabled key in price category", e);
  }
  
  return enabled;
  
 }
 
 /**
  * This method is used to get BIN setting for the product, defined in PIM sheet.
  * 
  * @param productPage
  *         the product page
  * @param currentPage
  *         the current page
  * @param configurationService
  *         the configuration service
  * @return displayBINFlag
  */
 public static boolean isDisableBuyItNow(Page productPage, Page currentPage, ConfigurationService configurationService) {
  UNIProduct product = null;
  String disableBuyItNow = StringUtils.EMPTY;
  String serviceProviderName = StringUtils.EMPTY;
  String enabled = StringUtils.EMPTY;
  boolean disableBuyItNowFlag = false;
  try {
   serviceProviderName = configurationService.getConfigValue(currentPage, ProductConstants.SHOPNOW_MAP, ProductConstants.SERVICE_PROVIDER_NAME);
   enabled = configurationService.getConfigValue(currentPage, serviceProviderName, ProductConstants.ENABLED);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find serviceProviderName key in shopNow or " + serviceProviderName + " category", e);
   
  }
  if (productPage != null) {
   product = ProductHelper.getUniProduct(productPage);
  }
  if (product != null) {
   disableBuyItNow = product.disableBuyItNow();
  }
  if (("On".equalsIgnoreCase(disableBuyItNow) || "Yes".equalsIgnoreCase(disableBuyItNow) || "True".equalsIgnoreCase(disableBuyItNow))
    || !("True".equalsIgnoreCase(enabled))) {
   disableBuyItNowFlag = true;
  }
  return disableBuyItNowFlag;
 }
}
