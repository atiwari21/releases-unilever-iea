/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.template.dto;

/**
 * The Class RecipeConfigDTO.
 */
public class RecipeConfigDTO {
 
 /** The timeout. */
 private int timeout;
 
 /** The end point url. */
 private String endPointUrl;
 
 /** The brand. */
 private String brand;
 
 /** The locale. */
 private String locale;
 
 private boolean isCalmenu;
 
 /**
  * @param isCalmenu
  *         the isCalmenu to set
  */
 public void setCalmenu(boolean isCalmenu) {
  this.isCalmenu = isCalmenu;
 }
 
 /**
  * Gets the timeout.
  * 
  * @return the timeout
  */
 public int getTimeout() {
  return timeout;
 }
 
 /**
  * Sets the timeout.
  * 
  * @param timeout
  *         the new timeout
  */
 public void setTimeout(int timeout) {
  this.timeout = timeout;
 }
 
 /**
  * Gets the end point url.
  * 
  * @return the end point url
  */
 public String getEndPointUrl() {
  return endPointUrl;
 }
 
 /**
  * Sets the end point url.
  * 
  * @param endPointUrl
  *         the new end point url
  */
 public void setEndPointUrl(String endPointUrl) {
  this.endPointUrl = endPointUrl;
 }
 
 /**
  * Gets the brand.
  * 
  * @return the brand
  */
 public String getBrand() {
  return brand;
 }
 
 /**
  * Sets the brand.
  * 
  * @param brand
  *         the new brand
  */
 public void setBrand(String brand) {
  this.brand = brand;
 }
 
 /**
  * Gets the locale.
  * 
  * @return the locale
  */
 public String getLocale() {
  return locale;
 }
 
 /**
  * Sets the locale.
  * 
  * @param locale
  *         the new locale
  */
 public void setLocale(String locale) {
  this.locale = locale;
 }
 
 /**
  * @return the isCalmenu
  */
 public boolean isCalmenu() {
  return isCalmenu;
 }
 
}
