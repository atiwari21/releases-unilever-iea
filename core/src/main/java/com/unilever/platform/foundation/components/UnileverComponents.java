/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.foundation.components;

/**
 * Home for all the constants related to Product components.
 * 
 */
public class UnileverComponents {
 
 /** login component path. */
 public static final String QUESTION_COMPONENT = "/apps/unilever-iea/components/question";
 
 /** turn to component path. */
 public static final String TURN_TO = "/apps/unilever-iea/components/turnTo";
 
 /** page Property component path. */
 public static final String PAGE_PROPERTY = "/apps/unilever-iea/components/pageProperty";
 
 /** login component path. */
 public static final String QUICK_POLL = "/apps/unilever-iea/components/quickPoll";
 
 /** login component path. */
 public static final String LOGIN_REGISTER = "/apps/unilever-iea/components/loginRegister";
 
 /** category reviews component path. */
 public static final String CATEGORY_REVIEWS = "/apps/unilever-iea/components/categoryReviews";
 
 /** virtual agent component path. */
 public static final String VIRTUAL_AGENT = "/apps/unilever-iea/components/virtualAgent";
 
 /** simple footer path. */
 public static final String SIMPLE_FOOTER = "/apps/unilever-iea/components/simpleFooter";
 
 /** call out button path. */
 public static final String CALL_OUT_BUTTON = "/apps/unilever-iea/components/callOutButton";
 
 /** anchor link component path. */
 public static final String ANCHOR_LINK = "/apps/unilever-iea/components/anchorLinkNavigation";
 
 /** store search component path. */
 public static final String STORE_SEARCH = "/apps/unilever-iea/components/storeSearch";
 
 /** anchor link navigation v2 component path. */
 public static final String ANCHOR_LINK_NAVIGATION_V2 = "/apps/unilever-iea/components/anchorLinkNavigationV2";
 
 /** social saring component path. */
 public static final String SOCIAL_SHARING = "/apps/unilever-iea/components/socialSharing";
 
 /** The Constant ARTICLE_LISTING. */
 public static final String ARTICLE_LISTING = "/apps/unilever-iea/components/articleListing";
 
 /** constant for product copy component. */
 public static final String PRODUCT_COPY = "/apps/unilever-iea/components/productCopy";
 
 /** constant for product awards component. */
 public static final String PRODUCT_AWARDS = "/apps/unilever-iea/components/productAwards";
 
 /** constant for related products component. */
 public static final String RELATED_PRODUCT = "/apps/unilever-iea/components/relatedProducts";
 
 /** The Constant PRODUCT_LISTING. */
 public static final String PRODUCT_LISTING = "/apps/unilever-iea/components/productListing";
 
 /** constant for home story component. */
 public static final String HOME_STORY = "/apps/unilever-iea/components/homeStory";
 
 /** constant for featured product component. */
 public static final String FEATURED_PRODUCT = "/apps/unilever-iea/components/featuredProduct";
 
 /** constant for featured product V2 component. */
 public static final String FEATURED_PRODUCT_V2 = "/apps/unilever-iea/components/featuredProductV2";
 
 /** constant for featured article component. */
 public static final String FEATURED_ARTICLE = "/apps/unilever-iea/components/featuredArticle";
 
 /** constant for featured Brand Tweet component. */
 public static final String FEATURED_BRAND_TWEET = "/apps/unilever-iea/components/featuredTwitterPost";
 
 /** constant for featured product range component. */
 public static final String FEATURED_PRODUCT_RANGE = "/apps/unilever-iea/components/featuredProductRange";
 
 /** constant for category overview component. */
 public static final String CATEGORY_OVERVIEW = "/apps/unilever-iea/components/categoryOverview";
 
 /** constant for product overview component. */
 public static final String PRODUCT_OVERVIEW = "/apps/unilever-iea/components/productOverview";
 
 /** constant for PRODUCT_IMAGES component. */
 public static final String PRODUCT_IMAGES = "/apps/unilever-iea/components/productImages";
 
 /** The Constant PAGE_DIVIDER. */
 public static final String PAGE_DIVIDER = "/apps/unilever-iea/components/pageDivider";
 /** The Constant PAGE_DIVIDER_V2. */
 public static final String PAGE_DIVIDER_V2 = "/apps/unilever-iea/components/pageDividerV2";
 /** constant for product overview component. */
 public static final String SITEMAP = "/apps/unilever-iea/components/sitemap";
 
 /** constant for hero component. */
 public static final String HERO = "/apps/unilever-iea/components/hero";
 
 public static final String HEROV2 = "/apps/unilever-iea/components/heroV2";
 
 /** constant for coupons component. */
 public static final String COUPONS = "/apps/unilever-iea/components/coupons";
 
 /** The Constant PRODUCT_TAG. */
 public static final String PRODUCT_TAG = "/apps/unilever-iea/components/productTags";
 
 /** The Constant MULTIPLE_RELATED_PRODUCT_BASE. */
 public static final String MULTIPLE_RELATED_PRODUCT_BASE = "/apps/unilever-iea/components/multipleRelatedProductsBase";
 
 /** The Constant MULTIPLE_RELATED_PRODUCT_ARTICLE. */
 public static final String MULTIPLE_RELATED_PRODUCT_ARTICLE = "/apps/unilever-iea/components/multipleRelatedProductsArticle";
 
 /** The Constant MULTIPLE_RELATED_ARTICLES_BASE. */
 public static final String MULTIPLE_RELATED_ARTICLES_BASE = "/apps/unilever-iea/components/multipleRelatedArticlesBase";
 
 /** constant for body copy steps component. */
 public static final String BODY_COPY_STEPS = "/apps/unilever-iea/components/bodyCopySteps";
 
 /** The Constant FOOTER. */
 public static final String FOOTER = "/apps/unilever-iea/components/footer";
 
 /** The Constant GLOBAL_NAVIGATION. */
 public static final String GLOBAL_NAVIGATION = "/apps/unilever-iea/components/globalNavigation";
 
 /** The Constant FEATURED_QUOTE. */
 public static final String FEATURED_QUOTE = "/apps/unilever-iea/components/featuredQuote";
 
 /** The Constant HEROLAUNCHVIDEO. */
 public static final String HEROLAUNCHVIDEO = "/apps/unilever-iea/components/heroLaunchVideo";
 
 /** The Constant MULTIPLE_RELATED_PRODUCT_ALTERNATE. */
 public static final String MULTIPLE_RELATED_PRODUCT_ALTERNATE = "/apps/unilever-iea/components/multipleRelatedProductAlternate";
 
 /** The Constant BODY_COPY. */
 public static final String BODY_COPY = "/apps/unilever-iea/components/bodyCopy";
 
 /** The Constant FEATURE BODY_COPY. */
 public static final String FEATURE_BODY_COPY = "/apps/unilever-iea/components/featurePhoneBodyCopy";
 /** The Constant SEARCH_RESULTS. */
 public static final String SEARCH_RESULTS = "/apps/unilever-iea/components/searchListing";
 
 /** The Constant CAMPAIGN_COMPONENT. */
 public static final String CAMPAIGN_COMPONENT = "/apps/unilever-iea/components/unilever-forms/formfileupload";
 
 /** The Constant IMAGE_FILE_REFERENCE. */
 public static final String IMAGE_FILE_REFERENCE = "fileReference";
 
 /** The Constant EXTENSION. */
 public static final String EXTENSION = "extension";
 
 /** The Constant FILENAME. */
 public static final String FILENAME = "fileName";
 
 /** The Constant IMAGE_ALT_TEXT. */
 public static final String IMAGE_ALT_TEXT = "altImage";
 
 /** The Constant IMAGE_ALT_TEXT_PROPERTY. */
 public static final String IMAGE_ALT_TEXT_PROPERTY = "alt";
 
 /** The Constant IMAGE_TITLE_PROPERTY. */
 public static final String IMAGE_TITLE_PROPERTY = "title";
 
 /** The Constant JSON_NAME_SPACE. */
 public static final String JSON_NAME_SPACE = "jsonNameSpace";
 
 /** The Constant URL. */
 public static final String URL = "url";
 
 /** The Constant IMAGE_PATH. */
 public static final String IMAGE_PATH = "path";
 
 /** The Constant WHATS_NEW. */
 public static final String WHATS_NEW = "/apps/unilever-iea/components/whatsNew";
 
 /** The Constant BODY_COPY_HEALINE. */
 public static final String BODY_COPY_HEALINE = "/apps/unilever-iea/components/bodyCopyHeadline";
 
 /** The Constant BREADCRUMB. */
 public static final String BREADCRUMB = "/apps/unilever-iea/components/breadcrumb";
 
 /** The Constant CAPTION. */
 public static final String CAPTION = "caption";
 
 /** The Constant PRODUCT_RANGE. */
 public static final String PRODUCT_RANGE = "/apps/unilever-iea/components/productRange";
 
 /** The Constant FEATURED_CATEGORY. */
 public static final String SPOTLIGHT = "/apps/unilever-iea/components/spotlight";
 
 /** The Constant LANG_SELECTOR. */
 public static final String LANG_SELECTOR = "/apps/unilever-iea/components/languageSelector";
 
 /** The Constant COUNTRY_SELECTOR. */
 public static final String COUNTRY_SELECTOR = "/apps/unilever-iea/components/countrySelector";
 
 /** The Constant CHECKBOX_COMPONENT. */
 public static final String CHECKBOX_COMPONENT = "/apps/unilever-iea/components/unilever-forms/formcheckbox";
 
 /** The Constant PRODUCT_CROSSELL. */
 public static final String PRODUCT_CROSSELL = "/apps/unilever-iea/components/productCrossell";
 
 /** The Constant SHOPPING_BASKET. */
 public static final String SHOPPING_BASKET = "/apps/unilever-iea/components/shoppingBasket";
 
 /** The Constant PURCHASE_CONFIRMATION. */
 public static final String PURCHASE_CONFIRMATION = "/apps/unilever-iea/components/purchaseConfirmation";
 
 /** The Constant GIFT_CONFIGURATOR. */
 public static final String GIFT_CONFIGURATOR = "/apps/unilever-iea/components/giftConfigurator";
 
 /** The Constant SIMPLE_HEADER. */
 public static final String SIMPLE_HEADER = "/apps/unilever-iea/components/simpleHeader";
 
 /** The Constant SIMPLE_HEADER. */
 public static final String ORDER_SUMMARY = "/apps/unilever-iea/components/orderSummary";
 
 /** The Constant PAYMENT_FORM. */
 public static final String PAYMENT_FORM = "/apps/unilever-iea/components/paymentForm";
 
 /** The Constant DYNAMIC_ERROR_MESSAGE. */
 public static final String DYNAMIC_ERROR_MESSAGE = "/apps/unilever-iea/components/dynamicErrorMessage";
 /** The Constant REVIEW component. */
 public static final String REVIEW = "/apps/unilever-iea/components/reviews";
 /** constant for olapic component. */
 public static final String OLAPIC = "/apps/unilever-iea/components/olapic";
 /** The Constant STORE_SEARCH_RESULTS. */
 public static final String STORE_SEARCH_RESULTS = "/apps/unilever-iea/components/buyInStoreSearchResult";
 /** The Constant STORE_SEARCH_RESULTS. */
 public static final String STORE_SEARCH_RESULTS_NEW = "/apps/unilever-iea/components/storeSearchResults";
 
 /** The Constant IFAQS. */
 public static final String IFAQS = "/apps/unilever-iea/components/ifaq";
 
 /** The Constant PRODUCT_TEASER. */
 public static final String PRODUCT_TEASER = "/apps/unilever-iea/components/productTeaser";
 
 /** constant for featured instagram image component. */
 public static final String FEATURED_INSTAGRAM = "/apps/unilever-iea/components/featuredInstagramPostTAB";
 
 /** constant for featured Tweet component. */
 public static final String FEATURED_TWEET = "/apps/unilever-iea/components/featuredTweetTAB";
 
 /** constant for Social Gallery Carousel component. */
 public static final String SOCIAL_GALLERY_CAROUSEL = "/apps/unilever-iea/components/socialGalleryCarouselThumbnailsTAB";
 
 /** The Constant SOCIAL_GALLERY_CAROUSEL_EXPANDED. */
 public static final String SOCIAL_GALLERY_CAROUSEL_EXPANDED = "/apps/unilever-iea/components/socialGalleryCarouselExpandedTAB";
 
 /** The Constant SOCIAL_GALLERY. */
 public static final String SOCIAL_GALLERY = "/apps/unilever-iea/components/socialGalleryTAB";
 
 /** The Constant SOCIAL_GALLERY. */
 public static final String SOCIAL_MEDIA_GALLERY = "/apps/unilever-iea/components/socialMediaGallery";
 
 /** The Constant MULTIPLE_RELATED_TWEETS. */
 public static final String MULTIPLE_RELATED_TWEETS = "/apps/unilever-iea/components/multipleRelatedTweetsTAB";
 
 /** The Constant MULTIPLE_RELATED_TWEETS_AREA. */
 public static final String MULTIPLE_RELATED_TWEETS_AREA = "/apps/unilever-iea/components/multipleRelatedTweets";
 
 /** The Constant SOCIAL_TAB_BASE. */
 public static final String SOCIAL_TAB_BASE = "/apps/unilever-iea/components/socialTabBase";
 
 /** The Constant BUY_IN_STORE_SEARCH. */
 public static final String BUY_IN_STORE_SEARCH = "/apps/unilever-iea/components/buyInStoreSearch";
 
 /** The Constant SOCIAL_COMMUNITY_STATS. */
 public static final String BRAND_SOCIAL_CHANNELS = "/apps/unilever-iea/components/brandSocialChannels";
 
 /** The Constant FEATURED_INSTAGRAM_POST. */
 public static final String FEATURED_INSTAGRAM_POST = "/apps/unilever-iea/components/featuredInstagramPost";
 
 /** The Constant SOCIAL_GALLERY. */
 public static final String SOCIAL_GALLERY_VIDEO = "/apps/unilever-iea/components/socialGallery";
 
 /** The Constant HTML_INJECTOR. */
 public static final String HTML_INJECTOR = "/apps/unilever-iea/components/htmlInjector";
 
 /** The Constant SMART_LABEL. */
 public static final String SMART_LABEL = "/apps/unilever-iea/components/smartLabel";
 
 /** The Constant END. */
 public static final String END = "/apps/unilever-iea/components/unilever-forms/end";
 
 /** The Constant END. */
 public static final String START_SECTION = "/apps/unilever-iea/components/startSection";
 
 /** The Constant END. */
 public static final String END_SECTION = "/apps/unilever-iea/components/endSection";
 
 /** The Constant SOCIAL_CAROUSEL. */
 public static final String SOCIAL_CAROUSEL = "/apps/unilever-iea/components/socialCarousel";
 
 /** The Constant CHECKOUT_SHOPPABLE. */
 public static final String CHECKOUT_SHOPPABLE = "/apps/unilever-iea/components/checkoutShoppable";
 
 /** The Constant CHECKOUT_SHOPPABLE. */
 public static final String SHOPPABLE_VIEW_BAG = "/apps/unilever-iea/components/shoppableViewBag";
 
 /** The Constant CHECKOUT_SHOPPABLE. */
 public static final String THANKYOU = "/apps/unilever-iea/components/thankYou";
 
 /** The Constant UNILEVERFORM. */
 public static final String UNILEVERFORM = "/apps/unilever-iea/components/unilever-forms/form";
 
 /** The Constant ZIP_FILE_DOWNLOAD. */
 public static final String ZIP_FILE_DOWNLOAD = "/apps/unilever-iea/components/zipFileDownload";
 
 /** The Constant IFRAME. */
 public static final String IFRAME = "/apps/unilever-iea/components/iframe";
 
 /** The Constant RECIPE_HERO. */
 public static final String RECIPE_HERO = "/apps/unilever-iea/components/recipeHero";
 
 /** The Constant RECIPE_COPY. */
 public static final String RECIPE_COPY = "/apps/unilever-iea/components/recipeCopy";
 
 /** The Constant ADAPTIVE_IMAGE. */
 public static final String ADAPTIVE_IMAGE_V2 = "/apps/unilever-iea/components/adaptiveImageV2";
 /** The Constant FEATURED_CONTENT. */
 public static final String FEATURED_CONTENT = "/apps/unilever-iea/components/featuredContent";
 /** The Constant RICH TEXT. */
 public static final String RICH_TEXT_V2 = "/apps/unilever-iea/components/richTextV2";
 
 /** The Constant ORDERED_LIST. */
 public static final String ORDERED_LIST = "/apps/unilever-iea/components/orderedList";
 
 /** The Constant ACCORDION. */
 public static final String ACCORDION = "/apps/unilever-iea/components/accordion";
 
 /** The Constant TABBED_CONTROL. */
 public static final String TABBED_CONTENT = "/apps/unilever-iea/components/tabbedContent";
 
 /** The Constant VIDEO_PLAYER_V2. */
 public static final String VIDEO_PLAYER_V2 = "/apps/unilever-iea/components/videoPlayerV2";
 
 /** The Constant HOTSPOT. */
 public static final String HOTSPOT = "/apps/unilever-iea/components/hotspot";
 
 /** The Constant GLOBAL_FOOTER_V2. */
 public static final String GLOBAL_FOOTER_V2 = "/apps/unilever-iea/components/globalFooterV2";
 
 /** The Constant RELATED ARTICLES. */
 public static final String RELATED_ARTICLES = "/apps/unilever-iea/components/relatedArticles";
 
 /** The Constant SEARCH_INPUT. */
 public static final String SEARCH_INPUT_V2 = "/apps/unilever-iea/components/search/searchInputV2";
 
 /** The Constant CAROUSEL. */
 public static final String CAROUSEL = "/apps/unilever-iea/components/carouselV2";
 
 /** constant for PRODUCT_RATING_OVERVIEW component. */
 public static final String PRODUCT_RATING_OVERVIEW = "/apps/unilever-iea/components/productRatingOverview";
 
 /** constant for COMMUNITY_PRODUCT_QA_OVERVIEW component. */
 public static final String COMMUNITY_PRODUCT_QA_OVERVIEW = "/apps/unilever-iea/components/communityProductQnaOverview";
 
 /** The Constant AD_CHOICE. */
 public static final String AD_CHOICES = "/apps/unilever-iea/components/adChoices";
 
 /** constant for PRODUCT_COPY_V2 component. */
 public static final String PRODUCT_COPY_V2 = "/apps/unilever-iea/components/productCopyV2";
 
 /** constant for PRODUCT_VARIANTS_AND_PRICE component. */
 public static final String PRODUCT_VARIANTS_AND_PRICE = "/apps/unilever-iea/components/productVariantsAndPrice";
 
 /** constant for BUY_IT_NOW component. */
 public static final String BUY_IT_NOW = "/apps/unilever-iea/components/buyItNow";
 
 /** constant for STORE_SEARCH_PDP component. */
 public static final String STORE_SEARCH_PDP = "/apps/unilever-iea/components/storeSearchPdp";
 
 /** constant for E_GIGTING_CONFIGURATOR component. */
 public static final String E_GIGTING_CONFIGURATOR = "/apps/unilever-iea/components/eGiftingConfigurator";
 
 /** constant for PRODUCT_AWARDS_OVERVIEW component. */
 public static final String PRODUCT_AWARDS_OVERVIEW = "/apps/unilever-iea/components/productAwardsOverview";
 /** constant for section navigation v2 component. */
 public static final String SECTION_NAVIGATION_V2 = "/apps/unilever-iea/components/sectionNavigationV2";
 
 /** The Constant CALL_TO_ACTION. */
 public static final String CALL_TO_ACTION = "/apps/unilever-iea/components/callToAction";
 
 /** The Constant QUOTE. */
 public static final String QUOTE = "/apps/unilever-iea/components/quote";
 
 /** The Constant TAGS. */
 public static final String TAGS = "/apps/unilever-iea/components/tags";
 
 /** The Constant PAGE LISTING V2. */
 public static final String PAGE_LISTING_V2 = "/apps/unilever-iea/components/pageListingV2";
 
 /** The Constant SMARTLABEL_V2. */
 public static final String SMARTLABEL_V2 = "/apps/unilever-iea/components/smartLabelV2";
 
 /** The Constant LIVE_CHAT_V2. */
 public static final String LIVE_CHAT_V2 = "/apps/unilever-iea/components/liveChatV2";
 /** The Constant PRODUCT LISTING V2. */
 public static final String PRODUCT_LISTING_V2 = "/apps/unilever-iea/components/productListingV2";
 
 /** The Constant SOCIAL_SHARE_PRINT. */
 public static final String SOCIAL_SHARE_PRINT = "/apps/unilever-iea/components/socialSharePrint";
 
 /** The Constant FORM_ELEMENT_V2. */
 public static final String FORM_ELEMENT_V2 = "/apps/unilever-iea/components/unilever-forms/formElementV2";
 
 /** The Constant FORM_HIDDEN. */
 public static final String FORM_HIDDEN = "/apps/unilever-iea/components/unilever-forms/formhidden";
 
 /** The Constant FORM ELEMENTS GROUP. */
 public static final String FORM_ELEMENTS_GROUP = "/apps/unilever-iea/components/formElementsGroup";
 
 /** The Constant BACK_TO_TOP_CTA. */
 public static final String BACK_TO_TOP_CTA = "/apps/unilever-iea/components/backToTopCta";
 
 /** The Constant PURCHASE_ORDER_CONFIRMATION. */
 public static final String PURCHASE_ORDER_CONFIRMATION = "/apps/unilever-iea/components/purchaseOrderConfirmation";
 
 /** The Constant FORM_V2. */
 public static final String FORM_V2 = "/apps/unilever-iea/components/unilever-forms/formV2";
 /** The Constant DIAGNOSTIC TOOL QUESTION. */
 public static final String DIAGNOSTIC_TOOL_QUESTION = "/apps/unilever-iea/components/diagnosticToolQuestion";
 /** The Constant DIAGNOSTIC TOOL RESULT SECTION. */
 public static final String DIAGNOSTIC_TOOL_RESULT_SECTION = "/apps/unilever-iea/components/diagnosticToolResultSection";
 /** The Constant DIAGNOSTIC TOOL. */
 public static final String DIAGNOSTIC_TOOL = "/apps/unilever-iea/components/diagnosticTool";
 /** The Constant SOCIAL_GALLERY_TAB_V2 */
 public static final String SOCIAL_GALLERY_TAB_V2 = "/apps/unilever-iea/components/socialGalleryTabV2";
 
 /** The Constant MEDIA_GALLERY_V2. */
 public static final String MEDIA_GALLERY_V2 = "/apps/unilever-iea/components/mediaGalleryV2";
 
 /** The Constant SEARCH_RESULTS. */
 public static final String SEARCH_RESULTS_V2 = "/apps/unilever-iea/components/searchListingV2";
 
 /** The Constant RECIPE_NUTRIENTS. */
 public static final String RECIPE_NUTRIENTS = "/apps/unilever-iea/components/recipeNutrients";
 
 /** The Constant RECIPE_COOKING_METHOD. */
 public static final String RECIPE_COOKING_METHOD = "/apps/unilever-iea/components/recipeCookingMethod";
 
 /** The Constant RECIPE_RELATED_PRODUCTS. */
 public static final String RECIPE_RELATED_PRODUCTS = "/apps/unilever-iea/components/recipeRelatedProducts";
 
 /** The Constant RECIPE_DIETARY_ATTRIBUTES. */
 public static final String RECIPE_DIETARY_ATTRIBUTES = "/apps/unilever-iea/components/recipeDietaryAttributes";
 
 /** The Constant RECIPE_ATTRIBUTES. */
 public static final String RECIPE_ATTRIBUTES = "/apps/unilever-iea/components/recipeAttributes";
 /**
  * Instantiates a new unilever components.
  */
 
 /** The Constant RECIPE_LISTING. */
 public static final String RECIPE_LISTING = "/apps/unilever-iea/components/recipeListing";
 
 /** The Constant RECIPE_RATING_OVERVIEW. */
 public static final String RECIPE_RATING_OVERVIEW = "/apps/unilever-iea/components/recipeRatingOverview";
 
 /** The Constant RECIPE_REVIEW. */
 public static final String RECIPE_REVIEW = "/apps/unilever-iea/components/recipeReviews";
 
 /** The Constant CONTENT_RESULTS_GRID. */
 public static final String CONTENT_RESULTS_GRID = "/apps/unilever-iea/components/contentResultsGrid";
 
 /** The Constant CAMPAIGN_OFFER_FLOW_WIDGET. */
 public static final String CAMPAIGN_OFFER_FLOW_WIDGET = "/apps/unilever-iea/components/campaignOfferFlowWidget";
 
 /** The Constant RESOURCE_LISTING. */
 public static final String RESOURCE_LISTING = "/apps/unilever-iea/components/resourceListing";
 
 /** The Constant ARTICLE_RATING_OVERVIEW component. */
 public static final String ARTICLE_RATING_OVERVIEW = "/apps/unilever-iea/components/articleRatingOverview";
 
 /** The Constant ARTICLE_REVIEWS component. */
 public static final String ARTICLE_REVIEWS = "/apps/unilever-iea/components/articleReviews";
 
 /** The Constant PRODUCT_NUTRIENTS. */
 public static final String PRODUCT_NUTRIENTS = "/apps/unilever-iea/components/productNutrients";
 
 /** login component path. */
 public static final String AUDIOPLAYER = "/apps/unilever-iea/components/audioPlayer";
 
 /** zoom modal component path. */
 public static final String ZOOM_MODAL = "/apps/unilever-iea/components/zoomModal";
 
 /** constant for reward redemption */
 public static final String REWARD_REDEMPTION = "/apps/unilever-iea/components/rewardRedemption";

 /** reward points component path. */
 public static final String REWARD_POINTS = "/apps/unilever-iea/components/rewardPoints";

 
 /**
  * Instantiates a new unilever components.
  */
 private UnileverComponents() {
  
 }
}
