/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.cache.ComponentViewAndViewHelperCache;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.utils.CollectionUtils;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.template.dto.Image;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * <p>
 * Responsible for reading author input for body copy steps component and generating a list of copy sections setting in pageContext for the view.
 * Input is manual
 * </p>
 */
@Component(description = "BodyCopyStepsViewHelperImpl", immediate = true, metatype = true, label = "BodyCopyStepsViewHelperImpl")
@Service(value = { BodyCopyStepsViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.BODY_COPY_STEPS, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class BodyCopyStepsViewHelperImpl extends BaseViewHelper {
 
 /**
  * Constant for retrieving multifield body copy headline.
  */
 private static final String HEADLINE = "copyStepHeadline";
 /**
  * Constant for retrieving multifield body copy description.
  */
 private static final String DESCRIPTION = "copyStepDescription";
 
 /** Constant for step label. */
 private static final String BODY_COPY_STEP = "step";
 
 /** Constant for headingText. */
 private static final String HEADING_TEXT = "headingText";
 
 /** Constant for subheadingText. */
 private static final String SUB_HEADING_TEXT = "subheadingText";
 
 /** Constant for text. */
 private static final String LONG_SUB_HEADING_TEXT = "text";
 
 /** Constant for heading label. */
 private static final String BODY_COPY_HEADING = "headline";
 
 /** Constant for description label. */
 private static final String BODY_COPY_DESCRIPTION = "description";
 
 /** Constant for description tips image. */
 private static final String TIPS_IMAGE_PATH = "image";
 
 /** Constant for description tips alt image text. */
 private static final String TIPS_IMAGE_ALT = "altImage";
 
 /** Constant for description IS_IMAGE_NEEDED. */
 private static final String IS_IMAGE_NEEDED = "isImageRequired";
 
 /**
  * logger object.
  **/
 private static final Logger LOGGER = LoggerFactory.getLogger(BodyCopyStepsViewHelperImpl.class);
 
 /** The component view and view helper cache. */
 @Reference
 ComponentViewAndViewHelperCache componentViewAndViewHelperCache;
 
 /**
  * The method processData overrides method is responsible validating author input and returning a list of sections containing rich text,text This
  * method converts the multiwidget input (a String Array) into sections of maps so that it can be accessed in JSP in consistent way.This method also
  * validates the link url provided by author.
  * 
  * @param content
  *         , - {@link Map} of all user input received as input <code>ViewHelper.onGetData</code> method
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return {@link Map} holding the data
  */
 @Override
 @SuppressWarnings("unchecked")
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  
  LOGGER.info("Body Copy Steps View Helper #processData called for processing fixed list.");
  Map<String, Object> bodyCopyStepsProperties = (Map<String, Object>) content.get(PROPERTIES);
  
  Map<String, Object> bodyCopyStepsMap = new LinkedHashMap<String, Object>();
  
  String jsonNameSpace = MapUtils.getString(content, "jsonNameSpace");
  ResourceResolver resourceResolver = (ResourceResolver) resources.get("RESOURCE_RESOLVER");
  
  SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) resources.get(ProductConstants.REQUEST);
  Resource currentResource = slingRequest.getResource();
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  Page page = pageManager.getContainingPage(currentResource);
  
  List<Map<String, Object>> bodyCopyStepsListFinal = new ArrayList<Map<String, Object>>();
  Map<String, Object> bodyCopyMap = new LinkedHashMap<String, Object>();
  String bodyCopyHeading = MapUtils.getString(bodyCopyStepsProperties, HEADLINE);
  Boolean isImageRequired = MapUtils.getBoolean(bodyCopyStepsProperties, IS_IMAGE_NEEDED, false);
  String headingText = MapUtils.getString(bodyCopyStepsProperties, HEADING_TEXT, StringUtils.EMPTY);
  String subheadingText = MapUtils.getString(bodyCopyStepsProperties, SUB_HEADING_TEXT, StringUtils.EMPTY);
  String text = MapUtils.getString(bodyCopyStepsProperties, LONG_SUB_HEADING_TEXT, StringUtils.EMPTY);
  bodyCopyMap.put("headingText", headingText);
  bodyCopyMap.put("subheadingText", subheadingText);
  bodyCopyMap.put("text", text);
  
  // checking body copy steps entry
  if (StringUtils.isNotEmpty(bodyCopyHeading)) {
   List<String> propertyNames = new ArrayList<String>();
   propertyNames.add(BODY_COPY_STEP);
   propertyNames.add(HEADLINE);
   propertyNames.add(DESCRIPTION);
   propertyNames.add(TIPS_IMAGE_PATH);
   propertyNames.add(TIPS_IMAGE_ALT);
   
   LOGGER.debug("Body Copy Steps component properties : " + bodyCopyStepsProperties);
   List<Map<String, String>> bodyCopySectionsList = CollectionUtils.convertMultiWidgetToList(bodyCopyStepsProperties, propertyNames);
   
   // preparing list of steps with headline and description
   List<Map<String, Object>> sectionsListFinal = prepareSectionList(bodyCopySectionsList, page, isImageRequired, resources);
   
   LOGGER.debug("The sections list being passed to the JSP after processing is :- " + sectionsListFinal);
   bodyCopyStepsListFinal.addAll(sectionsListFinal);
   
   bodyCopyMap.put("isImageRequired", isImageRequired);
   bodyCopyMap.put("steps", bodyCopyStepsListFinal.toArray());
   
   bodyCopyStepsMap.put(jsonNameSpace, bodyCopyMap);
  } else {
   List<Map<String, Object>> blankBodyCopyStepsListFinal = new ArrayList<Map<String, Object>>();
   
   bodyCopyStepsMap.put(jsonNameSpace, blankBodyCopyStepsListFinal);
  }
  
  return bodyCopyStepsMap;
 }
 
 /**
  * Change to empty string.
  * 
  * @param value
  *         the value
  * @return the string
  */
 // change null value to empty string
 public String changeToEmptyString(String value) {
  String newValue = value;
  if (StringUtils.isEmpty(newValue)) {
   newValue = "";
  }
  return newValue;
 }
 
 /**
  * This method is used to prepare section list based on the input provided by author.
  * 
  * @param sectionsList
  *         section list
  * @param page
  * @param isImageRequired
  * @return the list of maps
  */
 private List<Map<String, Object>> prepareSectionList(final List<Map<String, String>> sectionsList, Page page, Boolean isImageRequired,
   Map<String, Object> resources) {
  
  List<Map<String, Object>> finalSectionsList = new ArrayList<Map<String, Object>>();
  Map<String, Object> finalSectionsMap = null;
  for (Map<String, String> sectionMap : sectionsList) {
   String bodyCopyStep = sectionMap.get(BODY_COPY_STEP);
   String bodyCopyHeading = sectionMap.get(HEADLINE);
   String bodyCopyDescription = sectionMap.get(DESCRIPTION);
   String tipsimagepath = sectionMap.get(TIPS_IMAGE_PATH);
   String altImage = sectionMap.get(TIPS_IMAGE_ALT);
   
   Image image = new Image(tipsimagepath, altImage, altImage, page, getSlingRequest(resources));
   
   if (StringUtils.isNotEmpty(bodyCopyStep)) {
    finalSectionsMap = new LinkedHashMap<String, Object>();
    finalSectionsMap.put(BODY_COPY_STEP, bodyCopyStep);
    if (StringUtils.isNotEmpty(bodyCopyHeading)) {
     finalSectionsMap.put(BODY_COPY_HEADING, bodyCopyHeading);
    }
    if (StringUtils.isNotEmpty(bodyCopyDescription)) {
     finalSectionsMap.put(BODY_COPY_DESCRIPTION, bodyCopyDescription);
    }
    if (image != null && isImageRequired) {
     finalSectionsMap.put("image", image.convertToMap());
    }
    finalSectionsList.add(finalSectionsMap);
    
   }
  }
  
  return finalSectionsList;
 }
 
}
