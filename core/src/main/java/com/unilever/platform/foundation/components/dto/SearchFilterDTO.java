/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.components.dto;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

/**
 * The Class SearchFilterDTO.
 */
public class SearchFilterDTO {
 
 /** The filter header. */
 String filterHeader = StringUtils.EMPTY;
 
 /** The filter sub header. */
 String filterSubHeader = StringUtils.EMPTY;
 
 String clearAllFiltersKey = StringUtils.EMPTY;
 
 
 
 /** The filter options. */
 List<SearchFilterOptionsDTO> filterOptions = new ArrayList<SearchFilterOptionsDTO>();
 
 /**
  * Gets the filter header.
  * 
  * @return the filter header
  */
 public String getFilterHeader() {
  return filterHeader;
 }
 
 /**
  * Sets the filter header.
  * 
  * @param filterHeader
  *         the new filter header
  */
 public void setFilterHeader(String filterHeader) {
  this.filterHeader = filterHeader;
 }
 
 /**
  * Gets the sub filter header.
  * 
  * @return the sub filter header
  */
 public String getSubFilterHeader() {
  return filterSubHeader;
 }
 
 /**
  * Sets the sub filter header.
  * 
  * @param subFilterHeader
  *         the new sub filter header
  */
 public void setSubFilterHeader(String subFilterHeader) {
  this.filterSubHeader = subFilterHeader;
 }
 
 /**
  * Gets the filter options.
  * 
  * @return the filter options
  */
 public List<SearchFilterOptionsDTO> getFilterOptions() {
  return filterOptions;
 }
 
 /**
  * Sets the filter options.
  * 
  * @param filterOptions
  *         the new filter options
  */
 public void setFilterOptions(List<SearchFilterOptionsDTO> filterOptions) {
  this.filterOptions = filterOptions;
 }

 /**
  * @return the clearAllFilters
  */
 public String getClearAllFiltersKey() {
  return clearAllFiltersKey;
 }

 /**
  * @param clearAllFilters the clearAllFilters to set
  */
 public void setClearAllFiltersKey(String clearAllFilters) {
  this.clearAllFiltersKey = clearAllFilters;
 }
 
}
