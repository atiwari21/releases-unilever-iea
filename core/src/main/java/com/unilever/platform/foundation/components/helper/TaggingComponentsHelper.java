/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.components.helper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.aem.foundation.constants.CommonConstants;
import com.unilever.platform.aem.foundation.constants.SearchConstants;
import com.unilever.platform.aem.foundation.core.dto.SearchDTO;
import com.unilever.platform.aem.foundation.core.dto.TaggingSearchCriteria;
import com.unilever.platform.aem.foundation.core.helper.PageComparator;
import com.unilever.platform.aem.foundation.core.service.SearchParam;
import com.unilever.platform.aem.foundation.core.service.SearchService;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * The Class TaggingComponentsHelper.
 */
public class TaggingComponentsHelper {

	/** The Constant SLASH. */
	private static final String SLASH = "/";

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(ProductHelperExt.class);

	/** The Constant TWO. */
	private static final int TWO = 2;

	/**
	 * Instantiates a new tagging components helper.
	 */
	private TaggingComponentsHelper() {

	}

	/**
	 * Gets the list of pages based on tag criteria.
	 * 
	 * @param currentPage
	 *            the current page
	 * @param valueMap
	 *            the value map
	 * @param configMap
	 *            the config map
	 * @param propMap
	 *            the prop value list
	 * @param currentPageTagFlag
	 *            the current page tag flag
	 * @param searchService
	 *            the search service
	 * @param resourceResolver
	 *            the resource resolver
	 * @return list of pages
	 */
	public static List<String> getPagePathList(Page currentPage, Map<String, Object> valueMap,
			Map<String, String> configMap, Map<String, List<String>> propMap, boolean currentPageTagFlag,
			SearchService searchService, ResourceResolver resourceResolver) {
		int offset = 0, limit = CommonConstants.FIFTY;
		Map<String, Object> overridenConfig = GlobalConfigurationUtility.getOverriddenConfigMap(configMap, valueMap);
		String searchLimit = MapUtils.getString(configMap, UnileverConstants.LIMIT, StringUtils.EMPTY);

		try {
			limit = StringUtils.isNotBlank(searchLimit) ? Integer.parseInt(searchLimit) : CommonConstants.FIFTY;
		} catch (NumberFormatException nfe) {
			LOGGER.error("limit for page list must be number", nfe);
		}
		List<TaggingSearchCriteria> criteriaList = new ArrayList<TaggingSearchCriteria>();
		List<String> qualifiedTagIdListForSearch = new ArrayList<String>();
		if (currentPageTagFlag) {
			String[] includeNameSpaceList = StringUtils
					.isNotBlank(MapUtils.getString(overridenConfig, UnileverConstants.MATCHING_TAGS))
					? ComponentUtil.getPropertyValueArray(overridenConfig, UnileverConstants.MATCHING_TAGS)
							: new String[] {};
					for (String includeNameSpace : includeNameSpaceList) {
						qualifiedTagIdListForSearch = getQualifiedTagListFromPage(includeNameSpace, currentPage);
						updateCriteriaList(qualifiedTagIdListForSearch, criteriaList, SearchConstants.SEARCH_OR);
					}
		} else {
			qualifiedTagIdListForSearch = MapUtils.getObject(overridenConfig, UnileverConstants.MATCHING_TAGS) != null
					? ComponentUtil.getListFromStringArray(
							ComponentUtil.getPropertyValueArray(overridenConfig, UnileverConstants.MATCHING_TAGS))
							: new ArrayList<String>();
							updateCriteriaList(qualifiedTagIdListForSearch, criteriaList, SearchConstants.SEARCH_OR);
		}

		List<String> pageList = new ArrayList<String>();
		if (CollectionUtils.isNotEmpty(criteriaList)) {
			String[] excludeTagNameSpaces = StringUtils
					.isNotBlank(MapUtils.getString(overridenConfig, UnileverConstants.EXCLUSION_TAGS))
					? ComponentUtil.getPropertyValueArray(overridenConfig, UnileverConstants.EXCLUSION_TAGS)
							: new String[] {};
					List<String> excludeTagNameSpaceList = ComponentUtil.getListFromStringArray(excludeTagNameSpaces);
					String[] filterTagsNamespace = StringUtils
							.isNotBlank(MapUtils.getString(overridenConfig, UnileverConstants.INCLUSION_TAGS))
							? ComponentUtil.getPropertyValueArray(overridenConfig, UnileverConstants.INCLUSION_TAGS)
									: new String[] {};
							List<String> inclusionFilterNameSpace = ComponentUtil.getListFromStringArray(filterTagsNamespace);
							filterBrandLevelTags(inclusionFilterNameSpace);
							updateCriteriaList(inclusionFilterNameSpace, criteriaList, SearchConstants.SEARCH_AND);
							List<SearchDTO> list = searchService.getMultiTagResults(criteriaList, excludeTagNameSpaceList,
									currentPage.getPath(), propMap, new SearchParam(offset, limit, UnileverConstants.DESCENDING),
									resourceResolver);

							for (SearchDTO obj : list) {
								String page = obj.getResultNodePath();
								pageList.add(page);
							}
		}

		return pageList;
	}

	/**
	 * Updates the criteria List.
	 * 
	 * @param qualifiedTagIdListForSearch
	 *            the qualifiedTagIdListForSearch
	 * @param criteriaList
	 *            the criteriaList
	 * @param searchOperator
	 *            the criteriaList
	 */
	public static void updateCriteriaList(List<String> qualifiedTagIdListForSearch,
			List<TaggingSearchCriteria> criteriaList, String searchOperator) {
		if (CollectionUtils.isNotEmpty(qualifiedTagIdListForSearch)) {
			TaggingSearchCriteria criteria = new TaggingSearchCriteria();
			criteria.setTagList(qualifiedTagIdListForSearch);
			criteria.setOperator(searchOperator);
			criteriaList.add(criteria);
		}
	}

	/**
	 * Gets the qualified tag list from page.
	 * 
	 * @param includeNameSpace
	 *            the include name space
	 * @param currentPage
	 *            the current page
	 * @return the qualified tag list from page
	 */
	public static List<String> getQualifiedTagListFromPage(String includeNameSpace, Page currentPage) {
		return getQualifiedTagListFromPage(includeNameSpace, currentPage, false);
	}

	/**
	 * Gets the qualified tag list from page. This method fetch child tags of
	 * tags which are qualified.
	 * 
	 * @param includeNameSpace
	 *            the include name space
	 * @param currentPage
	 *            the current page
	 * @param childTagsFlag
	 *            the child tags flag
	 * @return the qualified tag list from page
	 */
	public static List<String> getQualifiedTagListFromPage(String includeNameSpace, Page currentPage,
			boolean childTagsFlag) {
		return getQualifiedTagListFromPage(includeNameSpace, currentPage, childTagsFlag, null);
	}

	/**
	 * Gets the qualified tag list from page. This method fetch child tags of
	 * tags which are qualified.
	 * 
	 * @param includeNameSpace
	 *            the include name space
	 * @param currentPage
	 *            the current page
	 * @param childTagsFlag
	 *            the child tags flag
	 * @param resources
	 *            the resources
	 * @return the qualified tag list from page
	 */
	public static List<String> getQualifiedTagListFromPage(String includeNameSpace, Page currentPage,
			boolean childTagsFlag, Map<String, Object> resources) {
		List<String> qualifiedTagListFromPage = new LinkedList<String>();
		TreeMap<Integer, Object> qualifiedTagListMapFromPage = new TreeMap<Integer, Object>();
		if (currentPage != null) {
			Tag[] pageTags = currentPage.getTags();
			for (Tag pageTag : pageTags) {
				String tagId = pageTag.getTagID();
				if (tagId.startsWith(includeNameSpace) && !childTagsFlag && resources == null) {
					qualifiedTagListFromPage.add(tagId);
				} else if (tagId.startsWith(includeNameSpace) && !childTagsFlag && resources != null) {
					TagManager tagManager = BaseViewHelper.getTagManager(resources);
					Tag includeNameSpaceTag = tagManager.resolve(includeNameSpace);
					Tag tag = tagManager.resolve(tagId);
					int tagIndex = ComponentUtil.getIndexFromIterator(includeNameSpaceTag.listAllSubTags(),
							(Object) tag);
					qualifiedTagListMapFromPage.put(tagIndex, tagId);
				} else if (tagId.startsWith(includeNameSpace) && childTagsFlag && resources != null) {
					String childTagId = getChildTagId(includeNameSpace, tagId);
					if (StringUtils.isNotBlank(childTagId)) {
						TagManager tagManager = BaseViewHelper.getTagManager(resources);
						Tag childTag = tagManager.resolve(childTagId);
						int tagIndex = ComponentUtil.getIndexFromIterator(childTag.getParent().listChildren(),
								(Object) childTag);
						qualifiedTagListMapFromPage.put(tagIndex, childTagId);
					}
				} else if (tagId.startsWith(includeNameSpace) && childTagsFlag && resources == null) {
					String childTagId = getChildTagId(includeNameSpace, tagId);
					if (StringUtils.isNotBlank(childTagId)) {
						qualifiedTagListFromPage.add(getChildTagId(includeNameSpace, tagId));
					}

				}
			}
			if (resources != null) {
				Collection<Object> qualifiedTags = qualifiedTagListMapFromPage.values();

				for (Object qualifiedTag : qualifiedTags) {
					qualifiedTagListFromPage.add(qualifiedTag.toString());
				}
			}

		}

		List<String> li2 = new LinkedList<String>(new LinkedHashSet<String>(qualifiedTagListFromPage));
		return li2;
	}

	/**
	 * This method extracts child tags based on includeNameSpace. If there is no
	 * child tag of includeNameSpace, includeNameSpace will be returned.
	 * 
	 * @param includeNameSpace
	 *            the base feature tags
	 * @param tagId
	 *            the tag id
	 * @return child tag id
	 */
	private static String getChildTagId(String includeNameSpace, String tagId) {
		String childTagId = StringUtils.EMPTY;
		String tempTagId = StringUtils.EMPTY;
		if (tagId.length() > includeNameSpace.length()) {
			tempTagId = tagId.substring(includeNameSpace.length() + 1);
		} else if (tagId.length() == includeNameSpace.length()) {
			childTagId = tagId;
		}
		if (tempTagId.contains(SLASH) && !tempTagId.isEmpty()) {
			childTagId = includeNameSpace + SLASH + tempTagId.substring(0, tempTagId.indexOf(SLASH));
		} else if (!tempTagId.contains(SLASH) && !tempTagId.isEmpty()) {
			childTagId = includeNameSpace + SLASH + tempTagId;
		}
		return childTagId;
	}

	/**
	 * Gets the qualified tag list from page.
	 * 
	 * @param includeNameSpaceList
	 *            the include name space list
	 * @param currentPage
	 *            the current page
	 * @return the qualified tag list from page
	 */
	public static List<String> getQualifiedTagListFromPage(String[] includeNameSpaceList, Page currentPage) {
		return getQualifiedTagListFromPage(includeNameSpaceList, currentPage, false);
	}

	/**
	 * Gets the qualified tag list from page.
	 * 
	 * @param includeNameSpaceList
	 *            the include name space list
	 * @param currentPage
	 *            the current page
	 * @param childTagsFlag
	 *            the child tags flag
	 * @return the qualified tag list from page
	 */
	public static List<String> getQualifiedTagListFromPage(String[] includeNameSpaceList, Page currentPage,
			boolean childTagsFlag) {
		return getQualifiedTagListFromPage(includeNameSpaceList, currentPage, childTagsFlag, null);
	}

	/**
	 * Gets the qualified tag list from page.
	 * 
	 * @param includeNameSpaceList
	 *            the include name space list
	 * @param currentPage
	 *            the current page
	 * @param childTagsFlag
	 *            the child tags flag
	 * @param resources
	 *            the resources
	 * @return the qualified tag list from page
	 */
	public static List<String> getQualifiedTagListFromPage(String[] includeNameSpaceList, Page currentPage,
			boolean childTagsFlag, Map<String, Object> resources) {
		List<String> qualifiedTagListFromPage = new LinkedList<String>();
		for (String includeNameSpace : includeNameSpaceList) {
			qualifiedTagListFromPage
			.addAll(getQualifiedTagListFromPage(includeNameSpace, currentPage, childTagsFlag, resources));

		}
		List<String> li2 = new LinkedList<String>(new LinkedHashSet<String>(qualifiedTagListFromPage));
		return li2;
	}

	/**
	 * Gets the tagged page list.
	 * 
	 * @param properties
	 *            the properties
	 * @param globalConfigMap
	 *            the global config map
	 * @param currentPage
	 *            the current page
	 * @param pageManager
	 *            the page manager
	 * @param propMap
	 *            the prop value list
	 * @param searchService
	 *            the search service
	 * @param resources
	 * @return the tagged page list
	 */
	public static List<String> getTaggedPageList(Map<String, Object> properties, Map<String, String> globalConfigMap,
			Page currentPage, PageManager pageManager, Map<String, List<String>> propMap, SearchService searchService,
			Map<String, Object> resources) {
		List<String> pagePathList = getSortedPagePathList(properties, currentPage, pageManager, globalConfigMap,
				propMap, currentPage.getContentResource().getResourceResolver(), searchService, resources);

		boolean overrideGlobalConfig = MapUtils.getBooleanValue(properties, "overrideGlobalConfig", false);
		int max = overrideGlobalConfig ? MapUtils.getIntValue(properties, UnileverConstants.LIMIT, 1)
				: MapUtils.getIntValue(globalConfigMap, "max", CommonConstants.FIFTY);

		List<String> pagePathSubList = pagePathList.size() > max ? pagePathList.subList(0, max) : pagePathList;
		return pagePathSubList;
	}

	/**
	 * Gets the sorted page path list.
	 * 
	 * @param properties
	 *            the properties
	 * @param currentPage
	 *            the current page
	 * @param pageManager
	 *            the page manager
	 * @param configMap
	 *            the config map
	 * @param propMap
	 *            the prop value list
	 * @param resourceResolver
	 *            the resource resolver
	 * @param searchService
	 *            the search service
	 * @param resources
	 * @return the sorted page path list
	 */
	public static List<String> getSortedPagePathList(Map<String, Object> properties, Page currentPage,
			PageManager pageManager, Map<String, String> configMap, Map<String, List<String>> propMap,
			ResourceResolver resourceResolver, SearchService searchService, Map<String, Object> resources) {
		List<String> pageList = new ArrayList<String>();
		boolean sortByDate = UnileverConstants.PRODUCT_LAUNCH_DATE.equals(
				MapUtils.getString(properties, UnileverConstants.ORDER_BY, UnileverConstants.PRODUCT_LAUNCH_DATE))
				? true : false;
		String includeSegmentRulesProp = MapUtils.getString(properties, "includeSegmentRules", StringUtils.EMPTY);
		boolean includeSegmentRules = ("true").equalsIgnoreCase(includeSegmentRulesProp) ? true : false;
		String tagBaseSearchType = MapUtils.getString(properties, UnileverConstants.TAG_SOURCE, StringUtils.EMPTY);

		Map<String, Object> overridenConfigMap = GlobalConfigurationUtility.getOverriddenConfigMap(configMap,
				properties);
		String[] includeNameSpaceList = StringUtils
				.isNotBlank(MapUtils.getString(overridenConfigMap, UnileverConstants.MATCHING_TAGS))
				? ComponentUtil.getPropertyValueArray(overridenConfigMap, UnileverConstants.MATCHING_TAGS)
						: new String[] {};
				String[] promotionTags = StringUtils
						.isNotBlank(MapUtils.getString(overridenConfigMap, UnileverConstants.PROMOTION_TAGS, StringUtils.EMPTY))
						? ComponentUtil.getPropertyValueArray(overridenConfigMap, UnileverConstants.PROMOTION_TAGS)
								: new String[] {};
						promotionTags = ComponentUtil.getUpdatedIncludedArrayFromResources(resources, promotionTags,
								includeSegmentRules);
						List<String> tagsList = getQualifiedTagListFromPage(includeNameSpaceList, currentPage);

						if (UnileverConstants.CURRENT_PAGE_TAGS.equals(tagBaseSearchType)) {
							pageList = getPagePathList(currentPage, properties, configMap, propMap, true, searchService,
									resourceResolver);

							Collections.sort(pageList, new PageComparator(tagsList, promotionTags, sortByDate, pageManager));
						} else if (UnileverConstants.SEARCH_TAGS.equals(tagBaseSearchType)) {
							pageList = getPagePathList(currentPage, properties, configMap, propMap, false, searchService,
									resourceResolver);

							tagsList = ComponentUtil.getListFromStringArray(
									StringUtils.isNotBlank(MapUtils.getString(overridenConfigMap, UnileverConstants.MATCHING_TAGS))
									? ComponentUtil.getPropertyValueArray(overridenConfigMap, UnileverConstants.MATCHING_TAGS)
											: new String[] {});

							Collections.sort(pageList, new PageComparator(tagsList, promotionTags, sortByDate, pageManager));
						}
						return pageList;
	}

	/**
	 * Returns the boolean value.
	 * 
	 * @param page
	 *            the page
	 * @param filters
	 *            the filters
	 * @return finalFlag
	 */
	public static boolean isEligbleFromFilter(Page page, String[] filters) {
		boolean finalFlag = true;
		if (page == null || filters == null || filters.length == 0) {
			return finalFlag;
		}
		Boolean[] flagArr = new Boolean[filters.length];
		Tag[] tags = page.getTags();

		for (int i = 0; i < filters.length; ++i) {
			String filter = filters[i];
			if (StringUtils.isBlank(filter)) {
				flagArr[i] = true;
				continue;
			}
			inner: for (Tag tag : tags) {
				String tagId = tag.getTagID();
				if (tagId.equalsIgnoreCase(filter)) {
					flagArr[i] = true;
					break inner;
				}
			}
		}

		for (Boolean flag : flagArr) {
			if (flag == null || !flag) {
				return false;
			}
		}
		return finalFlag;
	}

	/**
	 * 
	 * @param tagList
	 */
	public static void filterBrandLevelTags(List<String> tagList) {
		List<String> filteredTags = new ArrayList<String>();
		if (tagList != null) {
			for (String tagId : tagList) {
				if (StringUtils.split(tagId, "/").length < TWO) {
					filteredTags.add(tagId);
				}
			}
			tagList.removeAll(filteredTags);
		}
	}

}
