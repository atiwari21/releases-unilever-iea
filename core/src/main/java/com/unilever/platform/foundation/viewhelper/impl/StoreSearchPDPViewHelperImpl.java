/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;
import com.unilever.platform.foundation.viewhelper.constants.StoreLocatorConstants;

/**
 * This class prepares the data map for Store Search PDP.
 */
@Component(description = "StoreSearchPDPViewHelperImpl", immediate = true, metatype = true, label = "Store Search PDP ViewHelper")
@Service(value = { StoreSearchPDPViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.STORE_SEARCH_PDP, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class StoreSearchPDPViewHelperImpl extends BaseViewHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(StoreSearchPDPViewHelperImpl.class);
 
 private static final String PRODUCT_PAGE = "productPage";
 private static final String CTA_LABELS_MAP = "ctaLabels";
 
 /** The Constant STORE_LOCATER. */
 private static final String STORE_LOCATOR = "storeLocator";
 
 /** The Constant STORE_LOCATOR_URL. */
 private static final String STORE_LOCATOR_URL = "storeLocatorUrl";
 /** The Constant PRODUCT_UPC_CODE. */
 private static final String PRODUCT_UPC_CODE = "productUpcCode";
 /** The Constant POSTAL_CODE. */
 private static final String POSTAL_CODE = "postalCode";
 private static final String BUY_IN_STORE_LABEL = "buyInStore";
 private static final String POSTALCODE_FIELD_HELP_TEXT_LABEL = "postalCodeFieldHelpText";
 private static final String GPS_SHARE_LOCATION_LABEL = "gpsShareLocation";
 private static final String GO_CTA_LABEL = "goCta";
 private static final String ERROR_MSG = "errorMessage";
 
 @Reference
 ConfigurationService configurationService;
 
 @SuppressWarnings("unchecked")
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("Store Search PDP ViewHelper #processData called.");
  ResourceResolver resourceResolver = (ResourceResolver) resources.get(ProductConstants.RESOURCE_RESOLVER);
  Map<String, Object> productRatingOverviewProperties = (Map<String, Object>) content.get(PROPERTIES);
  SlingHttpServletRequest slingRequest = getSlingRequest(resources);
  Page currentPage = getCurrentPage(resources);
  I18n i18n = getI18n(resources);
  Map<String, Object> properties = new LinkedHashMap<String, Object>();
  Map<String, Object> storeLocatorProperties = new HashMap<String, Object>();
  UNIProduct product = null;
  String jsonNameSapce = getJsonNameSpace(content);
  String productPagePath = MapUtils.getString(productRatingOverviewProperties, PRODUCT_PAGE);
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  if (StringUtils.isNotBlank(productPagePath)) {
   product = ProductHelper.getUniProduct(pageManager.getPage(productPagePath));
  } else {
   product = ProductHelper.getUniProduct(currentPage, slingRequest);
  }
  if (product != null) {
   setStoreLocatorMap(currentPage, storeLocatorProperties, product, slingRequest);
  }
  setCtaLabelsMap(storeLocatorProperties, i18n);
  properties.put(STORE_LOCATOR, storeLocatorProperties);
  boolean geoCodeFlag = Boolean.parseBoolean(GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, "storeLocator",
    "geoCodeFlag"));
  
  properties.put("geoCodeFlag", geoCodeFlag);
  LOGGER.info("The Store Search PDP Overview is successfully generated");
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(jsonNameSapce, properties);
  return data;
 }
 
 /**
  * Sets the store locator map.
  * 
  * @param product
  *         the product
  * @param resources
  *         the resources
  * @param configurationService
  *         the configuration service
  */
 private void setStoreLocatorMap(Page currentPage, Map<String, Object> storeLocatorProperties, UNIProduct product,
   SlingHttpServletRequest slingRequest) {
  boolean isLocationBasedSearchEnabled = false;
  String storeLocatorUrl = StringUtils.EMPTY;
  String productUpcCode = StringUtils.EMPTY;
  String defaultRadiusForSearch = StringUtils.EMPTY;
  productUpcCode = product.getShortIdentifierValue();
  storeLocatorUrl = ProductHelper.getStoreLocatorUrl(product, currentPage, slingRequest);
  try {
   isLocationBasedSearchEnabled = Boolean.parseBoolean(configurationService.getConfigValue(currentPage, StoreLocatorConstants.GLOBAL_CONFIG_CAT,
     StoreLocatorConstants.IS_LOCATION_BASED_SEARCH_ENABLED));
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find isLocationBasedSearchEnabled key in storeLocator category", e);
  }
  defaultRadiusForSearch = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
    StoreLocatorConstants.GLOBAL_CONFIG_CAT, StoreLocatorConstants.DEFAULT_RADIOUS_FOR_SEARCH);
  String postalCodeRegEx = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, StoreLocatorConstants.REGEX,
    POSTAL_CODE);
  storeLocatorProperties.put(StoreLocatorConstants.REGEX, postalCodeRegEx);
  storeLocatorProperties.put(StoreLocatorConstants.IS_LOCATION_BASED_SEARCH_ENABLED, isLocationBasedSearchEnabled);
  storeLocatorProperties.put(STORE_LOCATOR_URL, storeLocatorUrl);
  storeLocatorProperties.put(PRODUCT_UPC_CODE, productUpcCode);
  storeLocatorProperties.put(StoreLocatorConstants.DEFAULT_RADIOUS_FOR_SEARCH, defaultRadiusForSearch);
  
 }
 
 /**
  * Sets the cta labels map.
  * 
  * @param storeLocatorProperties
  *         the storeLocatorProperties
  * @param i18n
  *         the i18n
  */
 private void setCtaLabelsMap(Map<String, Object> storeLocatorProperties, I18n i18n) {
  Map<String, Object> ctaLabelsMap = new HashMap<String, Object>();
  ctaLabelsMap.put(BUY_IN_STORE_LABEL, i18n.get(StoreLocatorConstants.BUY_IN_STORE_LABEL));
  ctaLabelsMap.put(POSTALCODE_FIELD_HELP_TEXT_LABEL, i18n.get(StoreLocatorConstants.POSTALCODE_FIELD_HELP_TEXT_LABEL));
  ctaLabelsMap.put(GPS_SHARE_LOCATION_LABEL, i18n.get(StoreLocatorConstants.GPS_SHARE_LOCATION_LABEL));
  ctaLabelsMap.put(GO_CTA_LABEL, i18n.get(StoreLocatorConstants.GO_CTA_LABEL));
  ctaLabelsMap.put(ERROR_MSG, i18n.get(StoreLocatorConstants.STORE_LOCATOR_POST_CODE_ERROR_MSG));
  storeLocatorProperties.put(CTA_LABELS_MAP, ctaLabelsMap);
 }
 
}
