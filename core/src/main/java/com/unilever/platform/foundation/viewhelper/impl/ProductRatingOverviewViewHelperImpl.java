/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.utils.AdaptiveUtil;
import com.unilever.platform.foundation.components.BazaarVoiceSEO;
import com.unilever.platform.foundation.components.KritiqueSEO;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.kritique.config.KritiqueReviewsCommentsService;
import com.unilever.platform.foundation.template.dto.KritiqueDTO;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * This class prepares the data map for Product rating overview.
 */
@Component(description = "ProductRatingOverviewViewHelperImpl", immediate = true, metatype = true, label = "Product Rating Overview ViewHelper")
@Service(value = { ProductRatingOverviewViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.PRODUCT_RATING_OVERVIEW, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class ProductRatingOverviewViewHelperImpl extends BaseViewHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ProductRatingOverviewViewHelperImpl.class);
 
 private static final String SEO = "SEO";
 private static final String ADAPTIVE_REVIEW_LABEL = "readReviews";
 private static final String ADAPTIVE_REVIEW_I18N = "productOverview.adaptiveReadReview";
 private static final String PRODUCT_PAGE = "productPage";
 private static final String RATING_REVIEWS = "ratingReviews";
 
 @Reference
 ConfigurationService configurationService;
 
 @Reference
 KritiqueSEO kqseoService;
 
 @Reference
 BazaarVoiceSEO bvseoService;
 
 @Reference
 KritiqueReviewsCommentsService kritiqueReviewsCommentsService;
 
 @SuppressWarnings("unchecked")
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("Product Rating Overview ViewHelper #processData called.");
  ResourceResolver resourceResolver = (ResourceResolver) resources.get(ProductConstants.RESOURCE_RESOLVER);
  Map<String, Object> productRatingOverviewProperties = (Map<String, Object>) content.get(PROPERTIES);
  SlingHttpServletRequest slingRequest = getSlingRequest(resources);
  Resource currentResource = slingRequest.getResource();
  String componentName = "";
  ValueMap componentProperties = currentResource.getValueMap();
  if (componentProperties != null) {
   componentName = componentProperties.get("componentName", String.class);
  }
  I18n i18n = getI18n(resources);
  Page currentPage = getCurrentPage(resources);
  Map<String, Object> properties = new LinkedHashMap<String, Object>();
  Map<String, Object> productProperties = new HashMap<String, Object>();
  UNIProduct product = null;
  String jsonNameSapce = getJsonNameSpace(content);
  String productPagePath = MapUtils.getString(productRatingOverviewProperties, PRODUCT_PAGE);
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  if (StringUtils.isNotBlank(productPagePath)) {
   product = ProductHelper.getUniProduct(pageManager.getPage(productPagePath));
  } else {
   product = ProductHelper.getUniProduct(currentPage, slingRequest);
  }
  String productPagePathInLogs = StringUtils.isNotBlank(productPagePath) ? productPagePath : currentPage.getPath();
  String serviceProvidername = StringUtils.EMPTY;
  String serviceProviderNameTemp = StringUtils.EMPTY;
  try {
   Resource resource = currentPage.getContentResource();
   InheritanceValueMap valueMap = new HierarchyNodeInheritanceValueMap(resource);
   if (valueMap != null) {
    serviceProviderNameTemp = (String) valueMap.getInherited(RATING_REVIEWS, StringUtils.EMPTY);
   }
   if (StringUtils.isNotBlank(serviceProviderNameTemp)) {
    serviceProvidername = serviceProviderNameTemp;
   } else {
    serviceProvidername = configurationService.getConfigValue(currentPage, ProductConstants.RATING_AND_REVIEWS,
      ProductConstants.SERVICE_PROVIDER_NAME);
   }
   Boolean isEnabled = Boolean.parseBoolean(configurationService.getConfigValue(currentPage, ProductConstants.RATING_AND_REVIEWS,
     ProductConstants.ENABLED));
   if (isEnabled) {
    Boolean enabled = Boolean.parseBoolean(configurationService.getConfigValue(currentPage, ProductConstants.PRODUCTOVERVIEW,
      ProductConstants.SEO_ENABLED));
    if (enabled) {
     if ("bazaarvoice".equalsIgnoreCase(serviceProvidername) || serviceProvidername.startsWith("bazaarvoice")) {
      productProperties.put(SEO, bvseoService.getSEOWithSDK(currentPage, configurationService, resources));
     } else if ("kritique".equalsIgnoreCase(serviceProvidername) || serviceProvidername.startsWith("kritique")) {
      productProperties.put(SEO, kqseoService.getSEOWithSDK(slingRequest, currentPage, configurationService));
     }
     
    }
    if (product != null) {
     productProperties.put(ProductConstants.PRODUCT_RATINGS_MAP,
       ProductHelper.getProductRatingsMap(product, currentPage, configurationService, componentName));
    }
    /* if it is feature phone, override ratings with feature phone ratings */
    if (AdaptiveUtil.isAdaptive(slingRequest)) {
     properties.put(ADAPTIVE_REVIEW_LABEL, i18n.get(ADAPTIVE_REVIEW_I18N));
     
     LOGGER.info("Feature phone view called and product page path is " + productPagePathInLogs);
     if (product != null) {
      KritiqueDTO kritiqueparams = new KritiqueDTO(currentPage, configurationService);
      kritiqueparams.setSiteProductId(product.getSKU());
      Map<String, Object> reviewMap = getKQMap(kritiqueparams);
      productProperties.put(ProductConstants.PRODUCT_RATINGS_MAP, reviewMap);
      LOGGER.debug("Product Rating map for productratingOverview of feature phone created succesfully");
     }
    }
   }
   
  } catch (Exception e) {
   LOGGER.error("could not find KQSEOenabled key in productOverview category", e);
  }
  
  if (product != null) {
   productProperties.put(ProductConstants.TITLE, product.getTitle());
  }
  
  productProperties.put(ProductConstants.REVIEW_MAP, ProductHelper.getReviewMapProperties(configurationService, currentPage));
  productProperties.put(ProductConstants.PRICE_ENABLED, ProductHelper.isPriceEnabled(configurationService, currentPage));
  properties.put(ProductConstants.PRODUCT_MAP, productProperties);
  LOGGER.info("The Product list corresponding to Product Overview is successfully generated");
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(jsonNameSapce, properties);
  return data;
 }
 
 /**
  * Gets the getKQMap map.
  * 
  * @param kritiqueparams
  *         the kritique parameter
  * @return the reviewMap
  */
 private Map<String, Object> getKQMap(KritiqueDTO kritiqueparams) {
  Map<String, Object> reviewMap = null;
  try {
   List<Map<String, Object>> kqMap = kritiqueReviewsCommentsService.getRatingsService(kritiqueparams, configurationService);
   if (!kqMap.isEmpty()) {
    reviewMap = kqMap.get(0);
   }
  } catch (Exception e) {
   LOGGER.error("Exception while getting kritique response ", e);
  }
  return reviewMap;
 }
 
}
