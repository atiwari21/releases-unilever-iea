/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.template.dto;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.ImageResource;
import com.day.cq.wcm.api.Page;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.ImageHelper;

/**
 * The Class Image.
 */
public class Image {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(Image.class);
 
 /** The Constant URL. */
 private static final String URL = "url";
 
 /** The Constant FILE_NAME. */
 private static final String FILE_NAME = "fileName";
 
 /** The Constant IS_NOT_ADAPTIVE_IMAGE. */
 private static final String IS_NOT_ADAPTIVE_IMAGE = "isNotAdaptiveImage";
 
 /** The Constant EXTENSION. */
 private static final String EXTENSION = "extension";
 
 /** The Constant ALT_IMAGE. */
 private static final String ALT_IMAGE = "altImage";
 
 /** The Constant TITLE. */
 private static final String TITLE = "title";
 
 /** The Constant PATH. */
 private static final String PATH = "path";
 
 /** The url. */
 private String url;
 
 /** The file name. */
 private String fileName;
 
 /** The is not adaptive image. */
 private boolean isNotAdaptiveImage;
 
 /** The extension. */
 private String extension;
 
 /** The alt image. */
 private String altImage;
 
 /** The title. */
 private String title;
 
 /** The path. */
 private String path;
 
 /**
  * Instantiates a new image.
  * 
  * @param fileReference
  *         the file reference
  * @param altImage
  *         the alt image
  * @param title
  *         the title
  * @param currentPage
  *         the current page
  * @param slingRequest
  *         the sling request
  */
 public Image(String fileReference, String altImage, String title, Page currentPage, SlingHttpServletRequest slingRequest) {
  LOGGER.debug("Inside constructor of Image");
  if (slingRequest != null) {
   this.url = StringUtils.isBlank(fileReference) ? StringUtils.EMPTY : ComponentUtil.getStaticFileFullPath(fileReference, currentPage, slingRequest);
  } else {
   this.url = StringUtils.isBlank(fileReference) ? StringUtils.EMPTY : ComponentUtil.getStaticFileFullPath(fileReference, currentPage);
  }
  this.fileName = StringUtils.EMPTY;
  this.isNotAdaptiveImage = false;
  this.extension = StringUtils.isBlank(fileReference) ? StringUtils.EMPTY : ImageHelper.getExtensionFromMimeType(fileReference.substring(
    fileReference.lastIndexOf('.') + 1, fileReference.length()));
  this.altImage = StringUtils.isBlank(altImage) ? fileName : altImage;
  this.title = StringUtils.isBlank(title) ? fileName : title;
  this.path = this.url;
 }
 
 /**
  * Instantiates a new image.
  * 
  * @param imageResource
  *         the image resource
  * @param currentPage
  *         the current page
  * @param httpServletRequest
  *         the http servlet request
  */
 public Image(ImageResource imageResource, Page currentPage, SlingHttpServletRequest httpServletRequest) {
  if (imageResource != null) {
   String fileRef = ComponentUtil.getStaticFileFullPath(imageResource.getFileReference(), currentPage, httpServletRequest);
   this.extension = ImageHelper.getFileExtension(fileRef);
   this.fileName = "";
   this.isNotAdaptiveImage = false;
   this.altImage = imageResource.getAlt();
   this.title = imageResource.getTitle();
   this.url = fileRef;
   this.path = fileRef;
   
  }
 }
 
 /**
  * Gets the url.
  * 
  * @return the url
  */
 public String getUrl() {
  return url;
 }
 
 /**
  * Sets the url.
  * 
  * @param url
  *         the new url
  */
 public void setUrl(String url) {
  this.url = url;
 }
 
 /**
  * Gets the file name.
  * 
  * @return the file name
  */
 public String getFileName() {
  return fileName;
 }
 
 /**
  * Sets the file name.
  * 
  * @param fileName
  *         the new file name
  */
 public void setFileName(String fileName) {
  this.fileName = fileName;
 }
 
 /**
  * Checks if is not adaptive image.
  * 
  * @return true, if is not adaptive image
  */
 public boolean isNotAdaptiveImage() {
  return isNotAdaptiveImage;
 }
 
 /**
  * Sets the not adaptive image.
  * 
  * @param isNotAdaptiveImage
  *         the new not adaptive image
  */
 public void setNotAdaptiveImage(boolean isNotAdaptiveImage) {
  this.isNotAdaptiveImage = isNotAdaptiveImage;
 }
 
 /**
  * Gets the extension.
  * 
  * @return the extension
  */
 public String getExtension() {
  return extension;
 }
 
 /**
  * Sets the extension.
  * 
  * @param extension
  *         the new extension
  */
 public void setExtension(String extension) {
  this.extension = extension;
 }
 
 /**
  * Gets the alt image.
  * 
  * @return the alt image
  */
 public String getAltImage() {
  return altImage;
 }
 
 /**
  * Sets the alt image.
  * 
  * @param altImage
  *         the new alt image
  */
 public void setAltImage(String altImage) {
  this.altImage = altImage;
 }
 
 /**
  * Gets the title.
  * 
  * @return the title
  */
 public String getTitle() {
  return title;
 }
 
 /**
  * Sets the title.
  * 
  * @param title
  *         the new title
  */
 public void setTitle(String title) {
  this.title = title;
 }
 
 /**
  * Gets the path.
  * 
  * @return the path
  */
 public String getPath() {
  return path;
 }
 
 /**
  * Sets the path.
  * 
  * @param path
  *         the new path
  */
 public void setPath(String path) {
  this.path = path;
 }
 
 /**
  * Convert to map.
  * 
  * @return the map
  */
 public Map<String, Object> convertToMap() {
  Map<String, Object> map = new LinkedHashMap<String, Object>();
  map.put(URL, this.url);
  map.put(FILE_NAME, this.fileName);
  map.put(IS_NOT_ADAPTIVE_IMAGE, this.isNotAdaptiveImage);
  map.put(EXTENSION, this.extension);
  map.put(ALT_IMAGE, this.altImage);
  map.put(TITLE, this.title);
  map.put(PATH, this.path);
  return map;
 }
 
}
