/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Session;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.engine.SlingRequestProcessor;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.contentsync.handler.util.RequestResponseFactory;
import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * This class prepares the data map for Login Register ComponentComponent It reads the values from the dialog and passes the info as a json.
 */

/**
 * @author rhariv
 * 
 */
@Component(description = "LoginRegister", immediate = true, metatype = true, label = "LoginRegister")
@Service(value = { LoginRegisterComponentViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.LOGIN_REGISTER, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class LoginRegisterComponentViewHelperImpl extends BaseViewHelper {
 
 private static final String CONFIGURATION_NOT_FOUND = "configuration not found";
 
 private static final String FULL_LOGIN_PATH = "fullLoginPath";
 
 private static final String IS_LOGIN = "isLoginPopup";
 
 private static final String IS_REGISTER = "isRegisterPopup";
 
 private static final String PROFILE_URL = "profileUrl";
 
 private static final String END_POINTS = "endPoints";
 
 private static final String LOGIN_REGISTER = "loginRegister";
 
 private static final String PROFILE = "profile";
 
 /** the constant resource */
 private static final String RESOURCE = "resource";
 
 /** the constant for ".html" */
 private static final String DOT_HTML = ".html";
 
 /** the constant get */
 private static final String GET = "GET";
 
 /** the constant loginUrl */
 private static final String LOGIN_URL = "loginUrl";
 
 /** the constant login cta label */
 private static final String LOGIN_CTA_LABEL = "loginCtaLabel";
 
 /** the constnt login page */
 private static final String LOGIN_PAGE = "loginPage";
 
 /** the constant register url */
 private static final String REGISTER_URL = "registerUrl";
 
 /** the constant html snippet */
 private static final String HTML_SNIPPET = "htmlSnippet";
 
 /** the constant cta label */
 private static final String CTA_LABEL = "ctaLabel";
 
 /** the constant register cta label */
 private static final String REGISTER_CTA_LABEL = "registerCtaLabel";
 
 /** the constant register page */
 private static final String REGISTER_PAGE = "registerPage";
 
 /** the constant register */
 private static final String REGISTER = "register";
 
 /** the constant login */
 private static final String LOGIN = "login";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(LoginRegisterComponentViewHelperImpl.class);
 
 private static final Object PROFILE_PAGE = "profilePage";
 
 private static final Object PROFILE_CTA_LABEL = "profileCTA";
 
 @Reference
 ConfigurationService configurationService;
 
 /** The request response factory. */
 @Reference
 private RequestResponseFactory requestResponseFactory;
 
 /** The request processor. */
 @Reference
 private SlingRequestProcessor requestProcessor;
 
 private static final String LOGIN_REGISTER_REDIRECTS = "loginRegisterRedirects";
 private static final String REDIRECTS_CONFIG = "redirectsConfig";
 
 private static final String LOGIN_REGISTER_LOGOUT_REDIRECTS = "loginRegisterLogoutRedirects";
 
 private static final String LOGOUT_REDIRECTS_CONFIG = "logoutRedirectsConfig";
 private static final String LOGIN_REGISTER_OR = "login.register.connectingstring";
 private static final String CONNECTING_STRING = "loginRegisterConnectingString";
 private static final String MESSAGE_WITH_URL = "anonymous.user.message";
 private static final String USER_MESSAGE = "anonymousUserMessage";
 private static final String LOGGED_IN_USER_MESSAGE_KEY = "loggedIn.user.message";
 private static final String LOGGED_IN_USER_MESSAGE = "loggedInUserMessage";
 private static final String LOGOUT_KEY = "login.register.logoutKey";
 private static final String LOGOUT_KEY_STRING = "logoutKey";
 
 /** The Constant IS_SECURE_PAGE. */
 private static final String IS_SECURE_PAGE = "isSecurePage";
 
 /** The Constant REDIRECT_URL. */
 private static final String REDIRECT_URL = "redirectUrl";
 
 /** The Constant TRUE. */
 private static final String TRUE = "true";
 
 /** The Constant REFERER. */
 private static final String REFERER = "referer";
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("Executing LoginRegisterComponentViewHelperImpl. Inside processData method ");
  Map<String, Object> properties = new HashMap<String, Object>();
  Map<String, Object> loginRegisterProperties = getProperties(content);
  /* reading properties of the component from the dialog */
  Map<String, Object> loginMap = getLoginDetails(resources, loginRegisterProperties);
  
  Map<String, Object> registerMap = getRegisterDetails(resources, loginRegisterProperties);
  I18n i18n = getI18n(resources);
  properties.put(LOGIN, loginMap);
  properties.put(REGISTER, registerMap);
  properties.put(PROFILE, getProfileDetails(resources, loginRegisterProperties));
  properties.put(END_POINTS, getUserEndPoints(resources));
  properties.put(REDIRECTS_CONFIG, getRedirects(resources));
  properties.put(LOGOUT_REDIRECTS_CONFIG, getLogoutRedirects(resources));
  properties.put(CONNECTING_STRING, i18n.get(LOGIN_REGISTER_OR));
  properties.put(USER_MESSAGE, i18n.get(MESSAGE_WITH_URL));
  properties.put(LOGGED_IN_USER_MESSAGE, i18n.get(LOGGED_IN_USER_MESSAGE_KEY));
  properties.put(LOGOUT_KEY_STRING, i18n.get(LOGOUT_KEY));
  Map<String, Object> data = new HashMap<String, Object>();
  setDispatcherNoCache(resources);
  data.put(getJsonNameSpace(content), properties);
  return data;
  
 }
 
 /**
  * @param content
  * @param resources
  * @return
  */
 private Map<String, Object> getRegisterDetails(Map<String, Object> resources, Map<String, Object> loginRegisterProperties) {
  String registerPage = null == loginRegisterProperties.get(REGISTER_PAGE) ? StringUtils.EMPTY : loginRegisterProperties.get(REGISTER_PAGE)
    .toString();
  String registerButton = null == loginRegisterProperties.get(REGISTER_CTA_LABEL) ? StringUtils.EMPTY : loginRegisterProperties.get(
    REGISTER_CTA_LABEL).toString();
  String path = null == getPageManager(resources).getContainingPage(registerPage) ? StringUtils.EMPTY : getPageManager(resources).getContainingPage(
    registerPage).getPath();
  path = ComponentUtil.getFullURL(getResourceResolver(resources), path, getSlingRequest(resources));
  boolean isRegister = MapUtils.getBoolean(loginRegisterProperties, IS_REGISTER, Boolean.FALSE);
  
  Map<String, Object> registerMap = new HashMap<String, Object>();
  
  registerMap.put(CTA_LABEL, registerButton);
  registerMap.put(IS_REGISTER, isRegister);
  if (isRegister) {
   registerMap.put(HTML_SNIPPET, getHtml(registerPage, resources));
  }
  registerMap.put(REGISTER_URL, path);
  return registerMap;
 }
 
 /**
  * @param content
  * @param resources
  * @return
  */
 private Map<String, String> getUserEndPoints(Map<String, Object> resources) {
  Map<String, String> endPointsConfigs = Collections.emptyMap();
  try {
   endPointsConfigs = configurationService.getCategoryConfiguration(getCurrentPage(resources), LOGIN_REGISTER);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error(CONFIGURATION_NOT_FOUND, e);
  }
  return endPointsConfigs;
 }
 
 /**
  * @param content
  * @param resources
  * @return
  */
 private Map<String, Object> getProfileDetails(Map<String, Object> resources, Map<String, Object> loginRegisterProperties) {
  String profilePage = null == loginRegisterProperties.get(PROFILE_PAGE) ? StringUtils.EMPTY : loginRegisterProperties.get(PROFILE_PAGE).toString();
  String profileButton = null == loginRegisterProperties.get(PROFILE_CTA_LABEL) ? StringUtils.EMPTY : loginRegisterProperties.get(PROFILE_CTA_LABEL)
    .toString();
  String path = null == getPageManager(resources).getContainingPage(profilePage) ? StringUtils.EMPTY : getPageManager(resources).getContainingPage(
    profilePage).getPath();
  path = ComponentUtil.getFullURL(getResourceResolver(resources), path, getSlingRequest(resources));
  Map<String, Object> profile = new HashMap<String, Object>();
  
  profile.put(CTA_LABEL, profileButton);
  profile.put(PROFILE_URL, path);
  return profile;
 }
 
 /**
  * @param content
  * @param resources
  * @return
  */
 private Map<String, Object> getLoginDetails(Map<String, Object> resources, Map<String, Object> loginRegisterProperties) {
  String loginPage = null == loginRegisterProperties.get(LOGIN_PAGE) ? StringUtils.EMPTY : loginRegisterProperties.get(LOGIN_PAGE).toString();
  String loginButton = null == loginRegisterProperties.get(LOGIN_CTA_LABEL) ? StringUtils.EMPTY : loginRegisterProperties.get(LOGIN_CTA_LABEL)
    .toString();
  String html = getHtml(loginPage, resources);
  String path = null == getPageManager(resources).getContainingPage(loginPage) ? StringUtils.EMPTY : getPageManager(resources).getContainingPage(
    loginPage).getPath();
  String fullPath = ComponentUtil.getFullURL(getResourceResolver(resources), path, getSlingRequest(resources));
  fullPath = StringUtils.replace(fullPath, DOT_HTML, ".hide" + DOT_HTML);
  path = ComponentUtil.getFullURL(getResourceResolver(resources), path, getSlingRequest(resources));
  boolean isLogin = MapUtils.getBoolean(loginRegisterProperties, IS_LOGIN, Boolean.FALSE);
  
  Map<String, Object> loginMap = new HashMap<String, Object>();
  
  loginMap.put(CTA_LABEL, loginButton);
  loginMap.put(IS_LOGIN, isLogin);
  if (isLogin) {
   loginMap.put(HTML_SNIPPET, html);
  }
  loginMap.put(LOGIN_URL, path);
  loginMap.put(FULL_LOGIN_PATH, fullPath);
  return loginMap;
 }
 
 /**
  * @param url
  * @param resources
  * @return
  */
 @SuppressWarnings("unchecked")
 private String getHtml(String url, Map<String, Object> resources) {
  String html = StringUtils.EMPTY;
  Session repositorySession = null;
  ByteArrayOutputStream out = new ByteArrayOutputStream();
  ResourceResolver resourceResolver = null;
  try {
   HttpServletRequest fakeRequest = requestResponseFactory.createRequest(GET, url + DOT_HTML, getSlingRequest(resources).getParameterMap());
   fakeRequest.setAttribute(RESOURCE, getResourceResolver(resources).getResource(url));
   HttpServletResponse fakeResponse = requestResponseFactory.createResponse(out);
   requestProcessor.processRequest(fakeRequest, fakeResponse, getResourceResolver(resources));
   html = out.toString();
   
  } catch (IOException ioexception) {
   LOGGER.error("Error while getting html content of the page ", ioexception);
  } catch (Exception exception) {
   LOGGER.error("Error while getting html content of the page ", exception);
  } finally {
   // logout if session is still valid
   if (repositorySession != null) {
    repositorySession.logout();
   }
   // close resource resolver
   if (resourceResolver != null) {
    resourceResolver.close();
   }
  }
  
  return html;
 }
 
 /**
  * Gets the redirects.
  * 
  * @param resources
  *         the resources
  * @param content
  *         the content
  * @return the redirects
  */
 private Map<String, String> getRedirects(Map<String, Object> resources) {
  Map<String, String> endPointsConfigs = Collections.emptyMap();
  try {
   endPointsConfigs = configurationService.getCategoryConfiguration(getCurrentPage(resources), LOGIN_REGISTER_REDIRECTS);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error(CONFIGURATION_NOT_FOUND, e);
  }
  return endPointsConfigs;
 }
 
 /**
  * Gets the mapped url.
  * 
  * @param resources
  *         the resources
  * @param pagePath
  *         the page path
  * @return the mapped url
  */
 private String getMappedUrl(Map<String, Object> resources, String pagePath) {
  ResourceResolver resourceResolver = getResourceResolver(resources);
  String localPagePath = pagePath;
  Resource resource = ComponentUtil.getResourceFromPath(resourceResolver, localPagePath);
  if (resource != null) {
   localPagePath = resourceResolver.map(localPagePath);
   PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
   Page page = pageManager.getContainingPage(resource);
   if (page == null) {
    return localPagePath;
   }
   
   if (!localPagePath.contains(com.unilever.platform.foundation.constants.CommonConstants.DOT_CHAR)) {
    localPagePath = localPagePath + com.unilever.platform.foundation.constants.CommonConstants.PAGE_EXTENSION;
   }
   if (localPagePath != null) {
    localPagePath = ComponentUtil.getEncodedOrDecodeUrl(localPagePath);
   }
   LOGGER.debug("Relative Path = " + localPagePath);
   
  }
  return localPagePath;
 }
 
 /**
  * Gets the logout redirects.
  * 
  * @param resources
  *         the resources
  * @param content
  *         the content
  * @return the logout redirects
  */
 private Map<String, String> getLogoutRedirects(Map<String, Object> resources) {
  String actualPageUrl = StringUtils.EMPTY;
  try {
   actualPageUrl = new URL(getSlingRequest(resources).getHeader(REFERER)).getPath();
  } catch (MalformedURLException e1) {
   LOGGER.warn("referer not found", e1.getMessage());
  }
  
  LOGGER.debug("actualPageUrl in LoginRegister(from referer) is {}", actualPageUrl);
  Page actualPage = getResourceResolver(resources).resolve(actualPageUrl).adaptTo(Page.class);
  
  if (actualPage == null) {
   LOGGER.debug("Page from referer in LoginRegister is null, fetching from request.getPathInfo");
   actualPage = getActualPage(resources);
  }
  
  LOGGER.debug("Current Page in LoginRegister {}", actualPage.getPath());
  Map<String, Object> pageProperties = actualPage.getProperties();
  String pagePath = actualPage.getPath();
  String isSecurePage = null == pageProperties.get(IS_SECURE_PAGE) ? StringUtils.EMPTY : pageProperties.get(IS_SECURE_PAGE).toString();
  String redirectUrl = null == pageProperties.get(REDIRECT_URL) ? StringUtils.EMPTY : pageProperties.get(REDIRECT_URL).toString();
  String mappedPagePath = getMappedUrl(resources, pagePath);
  String mappedRedirectUrl = StringUtils.isNotEmpty(redirectUrl) ? getMappedUrl(resources, redirectUrl) : StringUtils.EMPTY;
  Map<String, String> endPointsConfigs = new HashMap<String, String>();
  
  try {
   endPointsConfigs = configurationService.getCategoryConfiguration(actualPage, LOGIN_REGISTER_LOGOUT_REDIRECTS);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error(CONFIGURATION_NOT_FOUND, e);
  }
  if (TRUE.equals(isSecurePage) && StringUtils.isNotEmpty(mappedRedirectUrl)) {
   endPointsConfigs.put(mappedPagePath, mappedRedirectUrl);
  }
  return endPointsConfigs;
 }
 
 /**
  * Sets the dispacher no cache.
  * 
  * @param resources
  *         the resources
  */
 private void setDispatcherNoCache(Map<String, Object> resources) {
  SlingScriptHelper scriptHelper = (SlingScriptHelper) resources.get("sling");
  SlingHttpServletResponse slingResponse = scriptHelper.getResponse();
  final HttpServletResponse response = (HttpServletResponse) slingResponse;
  response.setHeader("Dispatcher", "no-cache");
 }
}
