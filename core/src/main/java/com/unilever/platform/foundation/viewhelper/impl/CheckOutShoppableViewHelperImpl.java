/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.WCMMode;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ShoppableHelper;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * <p>
 * Responsible for reading author input, I18N and configuration for shopping basket component and generate a map. Also read the site configuration for
 * maximum items configured for the site.
 * </p>
 */
@Component(description = "CheckOutShoppableViewHelperImpl", immediate = true, metatype = true, label = "CheckOutShoppableViewHelperImpl")
@Service(value = { CheckOutShoppableViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.CHECKOUT_SHOPPABLE, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class CheckOutShoppableViewHelperImpl extends BaseViewHelper {
 
 private static final String GO_BACK_LINK_TEXT = "gobackLinkText";
 private static final String TITLE = "title";
 private static final String SHOPPABLE_CONFIG = "shoppableConfig";
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(CheckOutShoppableViewHelperImpl.class);
 public static final String PUBLISHER_CHECKOUT_URL = "publisherCheckoutUrl";
 public static final String ORDER_COMPLETE_URL = "orderCompleteUrl";
 public static final String ORDER_COMPLETE_RETURN_TO_SITE_URL = "orderCompletereturnToSiteUrl";
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  LOGGER.debug("inside ShoppingBasketViewHelperImpl processData");
  Map<String, Object> checkoutProperties = (Map<String, Object>) content.get(PROPERTIES);
  Map<String, Object> checkoutMap = new HashMap<String, Object>();
  Map<String, Object> data = new HashMap<String, Object>();
  Map<String, String> checkoutConfigMap = new HashMap<String, String>();
  checkoutConfigMap = ShoppableHelper.getShoppableConfig(getResourceResolver(resources), configurationService, getCurrentPage(resources),
    getSlingRequest(resources));
  addApiToken(resources, checkoutConfigMap);
  checkoutMap.put(GO_BACK_LINK_TEXT, MapUtils.getString(checkoutProperties, GO_BACK_LINK_TEXT, StringUtils.EMPTY));
  checkoutMap.put(TITLE, MapUtils.getString(checkoutProperties, TITLE, StringUtils.EMPTY));
  checkoutMap.put(SHOPPABLE_CONFIG, checkoutConfigMap);
  data.put(getJsonNameSpace(content), checkoutMap);
  return data;
 }
 
 private void addApiToken(final Map<String, Object> resources, Map<String, String> checkoutConfigMap) {
  String apiTokenAuthor = checkoutConfigMap.get("apiTokenAuthor");
  checkoutConfigMap.remove(apiTokenAuthor);
  if (WCMMode.fromRequest(getSlingRequest(resources)) == WCMMode.PREVIEW || WCMMode.fromRequest(getSlingRequest(resources)) == WCMMode.EDIT
    || WCMMode.fromRequest(getSlingRequest(resources)) == WCMMode.DESIGN) {
   checkoutConfigMap.put("apiToken", apiTokenAuthor);
  }
 }
 
}
