/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.tagging.Tag;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.core.dto.SearchDTO;
import com.unilever.platform.aem.foundation.core.dto.TaggingSearchCriteria;
import com.unilever.platform.aem.foundation.core.service.SearchParam;
import com.unilever.platform.aem.foundation.core.service.SearchService;
import com.unilever.platform.foundation.components.SearchResultComparator;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.constants.CommonConstants;
import com.unilever.platform.foundation.constants.ContainerTagConstants;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.Article;
import com.unilever.platform.foundation.template.dto.Image;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.MultipleRelatedArticlesConstants;
import com.unilever.platform.foundation.viewhelper.constants.MultipleRelatedProductsConstants;

/**
 * <p>
 * Responsible for reading author input for multipleRelatedArticles component and get all the details of products which are tagged to the current page
 * and generate a product list. Also read the site configuration for maximum number configured for the articles.
 * </p>
 * .
 */
@Component(description = "MultipleRelatedArticlesBaseViewHelperImpl", immediate = true, metatype = true, label = "MultipleRelatedArticlesBaseViewHelperImpl")
@Service(value = { MultipleRelatedArticlesBaseViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.MULTIPLE_RELATED_ARTICLES_BASE, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class MultipleRelatedArticlesBaseViewHelperImpl extends BaseViewHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(MultipleRelatedArticlesBaseViewHelperImpl.class);
 
 @Reference
 ConfigurationService configurationService;
 
 /** The search service. */
 @Reference
 SearchService searchService;
 
 /**
  * The method processData overrides method is responsible validating author input and returning a list of articles, which are tagged to the current
  * page and maximum number for the links from site configuration to be displayed on the page.
  * 
  * @param content
  *         {@link Map} of all user input received as input
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return Map holding the result data.
  */
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  LOGGER.debug("Executing MultipleRelatedArticlesBaseViewHelperImpl.java. Inside processData method");
  Map<String, Object> data = new HashMap<String, Object>();
  Map<String, Object> properties = getProperties(content);
  Page currentPage = getCurrentPage(resources);
  String gloablConfigCatName = MapUtils.getString(properties, UnileverConstants.GLOBAL_CONFIGURATION_CATEGORY_NAME);
  LOGGER.debug("Global Configuration category for MultipleRelatedArticlesBaseViewHelperImpl = " + gloablConfigCatName);
  String jsonNameSpace = MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE);
  String title = MapUtils.getString(properties, UnileverConstants.TITLE, StringUtils.EMPTY);
  String backgroundImageAltText = MapUtils.getString(properties, MultipleRelatedArticlesConstants.BACKGROUND_IMAGE_ALT_TEXT, StringUtils.EMPTY);
  String backgroundImageFileReference = MapUtils.getString(properties, MultipleRelatedArticlesConstants.BACKGROUND_IMAGE, StringUtils.EMPTY);
  Image backgroundImage = new Image(backgroundImageFileReference, backgroundImageAltText, StringUtils.EMPTY, currentPage, getSlingRequest(resources));
  String contentType = MapUtils.getString(properties, UnileverConstants.CONTENT_TYPE, StringUtils.EMPTY);
  I18n i18n = getI18n(resources);
  String buttonText = i18n.get("multipleRelatedArticles.buttonText");
  String loadMoreButtonText = i18n.get("multipleRelatedArticles.loadMoreButtonText");
  
  String type = i18n.get("multipleRelatedArticles.type");
  
  Map<String, Object> multipleRelatedArticleMap = new HashMap<String, Object>();
  multipleRelatedArticleMap.put(MultipleRelatedArticlesConstants.TITLE, title);
  multipleRelatedArticleMap.put(MultipleRelatedArticlesConstants.BACKGROUND_IMAGE, backgroundImage.convertToMap());
  multipleRelatedArticleMap.put(MultipleRelatedArticlesConstants.BUTTON_TEXT, buttonText);
  multipleRelatedArticleMap.put(MultipleRelatedArticlesConstants.LOAD_MORE_BUTTON_TEXT, loadMoreButtonText);
  
  multipleRelatedArticleMap.put(MultipleRelatedArticlesConstants.TYPE, type);
  
  Map<String, String> configMap = ComponentUtil.getConfigMap(currentPage, gloablConfigCatName);
  LOGGER.debug("MultipleRelatedArticlesBaseViewHelperImpl configMap = " + configMap);
  String iconPath = MapUtils.getString(configMap, MultipleRelatedArticlesConstants.ICON_PATH, StringUtils.EMPTY);
  multipleRelatedArticleMap.put(MultipleRelatedArticlesConstants.ICON, iconPath);
  List<String> propValueList = new ArrayList<String>();
  if (StringUtils.isEmpty(contentType) || UnileverConstants.ARTICLE.equals(contentType)) {
   propValueList.add(UnileverConstants.ARTICLE);
  } else if (UnileverConstants.SOCIAL_ARTICLE.equals(contentType)) {
   propValueList.add(UnileverConstants.SOCIAL_ARTICLE);
  } else {
   propValueList.add(UnileverConstants.ARTICLE);
   propValueList.add(UnileverConstants.SOCIAL_ARTICLE);
  }
  List<SearchDTO> articlesResultList = getArticlesPagePathList(configMap, properties, propValueList, resources);
  
  LOGGER.debug("MultipleRelatedArticlesBaseViewHelperImpl articlesResultList = " + articlesResultList);
  List<String> articlesPagePathList = getSortedFinalListBasedOnComparator(articlesResultList, configMap, currentPage, resources, properties);
  LOGGER.debug("MultipleRelatedArticlesBaseViewHelperImpl articlesPagePathList after sorting = " + articlesPagePathList);
  List<Map<String, Object>> articleListData = getArticleListData(articlesPagePathList, currentPage, resources);
  LOGGER.debug("MultipleRelatedArticlesBaseViewHelperImpl articleListData  = " + articleListData);
  multipleRelatedArticleMap.put(MultipleRelatedArticlesConstants.ARTICLES, articleListData.toArray());
  multipleRelatedArticleMap.put(UnileverConstants.CONTENT_TYPE, contentType);
  Map<String, String> anchorLinkMap = ComponentUtil.getAnchorLinkMap(properties);
  multipleRelatedArticleMap.put(UnileverConstants.ANCHOR_NAVIGATION, anchorLinkMap);
  data.put(jsonNameSpace, multipleRelatedArticleMap);
  return data;
 }
 
 /**
  * Gets the sorted final list based on comparator.
  * 
  * @param articlesResultList
  *         the articles result list
  * @param configMap
  *         the config map
  * @param currentPage
  *         the current page
  * @return the sorted final list based on comparator
  */
 protected List<String> getSortedFinalListBasedOnComparator(List<SearchDTO> articlesResultList, Map<String, String> configMap, Page currentPage,
   final Map<String, Object> resources, Map<String, Object> properties) {
  int max = CommonConstants.THREE;
  try {
   max = StringUtils.isBlank(configMap.get(UnileverConstants.MAX)) ? CommonConstants.THREE : Integer.parseInt(configMap.get(UnileverConstants.MAX));
  } catch (NumberFormatException nfe) {
   LOGGER.error("Maximum article for page list must be number", nfe);
  }
  String[] include = !StringUtils.isBlank(configMap.get(UnileverConstants.INCLUDE_LIST)) ? configMap.get(UnileverConstants.INCLUDE_LIST).split(
    CommonConstants.COMMA_CHAR) : new String[] {};
  
  List<String> includeTagIdList = getIncludeTagIdList(currentPage, include);
  LOGGER.debug("Maximum articles for page list= " + max);
  String orderbyTagId = configMap.get(MultipleRelatedProductsConstants.ORDER_BY) != null ? configMap.get(MultipleRelatedProductsConstants.ORDER_BY)
    : StringUtils.EMPTY;
  String[] orderbyTagIdArr = StringUtils.isNotBlank(orderbyTagId) ? orderbyTagId.split(CommonConstants.COMMA_CHAR) : new String[] {};
  String includeSegmentRulesProp = MapUtils.getString(properties, "includeSegmentRules", StringUtils.EMPTY);
  boolean includeSegmentRules = ("true").equalsIgnoreCase(includeSegmentRulesProp) ? true : false;
  String[] featuredTags = ComponentUtil.getUpdatedIncludedArrayFromResources(resources, orderbyTagIdArr, includeSegmentRules);
  long startTime = System.currentTimeMillis();
  
  Collections.sort(articlesResultList, new SearchResultComparator(includeTagIdList, featuredTags, true));
  
  long stopTime = System.currentTimeMillis();
  
  LOGGER.info("Time elapsed in soring the result of MultipleRelatedArticles = " + (stopTime - startTime));
  List<SearchDTO> articlesResultListFinal = articlesResultList.size() > max ? articlesResultList.subList(0, max) : articlesResultList;
  return getPathList(articlesResultListFinal);
 }
 
 /**
  * Gets the article list data.
  * 
  * @param articlesPagePathList
  *         the articles page path list
  * @param gloablConfigCatName
  *         the gloabl config cat name
  * @param currentPage
  *         the current page
  * @return the article list data
  */
 protected List<Map<String, Object>> getArticleListData(List<String> articlesPagePathList, Page currentPage, Map<String, Object> resources) {
  List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
  Iterator<String> articlePagePathItr = articlesPagePathList.iterator();
  while (articlePagePathItr.hasNext()) {
   String articlePagePath = articlePagePathItr.next();
   Page articlePage = currentPage.getPageManager().getPage(articlePagePath);
   if (articlePage != null) {
    Article article = new Article();
    article.setData(articlePage, getSlingRequest(resources));
    Map<String, Object> map = article.convertToMap();
    @SuppressWarnings("unchecked")
    Map<String, Object> containerTagMap = (Map<String, Object>) map.get(UnileverConstants.CONTAINER_TAG);
    Map<String, Object> relatedArticleMap = ComponentUtil.getContainerTagMap(currentPage, ContainerTagConstants.RELATED_ARTICLE, article.getTitle());
    Map<String, Object> mayLikeMap = ComponentUtil.getContainerTagMap(currentPage, ContainerTagConstants.MAY_LIKE, article.getTitle());
    
    containerTagMap.put(ContainerTagConstants.RELATED_ARTICLE, relatedArticleMap);
    containerTagMap.put(ContainerTagConstants.MAY_LIKE, mayLikeMap);
    map.put(UnileverConstants.CONTAINER_TAG, containerTagMap);
    list.add(map);
   }
  }
  return list;
 }
 
 protected List<SearchDTO> getArticlesPagePathList(Map<String, String> configMap, Map<String, Object> properties, List<String> propValueList,
   Map<String, Object> resources) {
  LOGGER.debug("Inside getArticlesPagePathList of MultipleRelatedArticlesBaseViewHelperImpl");
  int offset = 0, limit = CommonConstants.TWENTY;
  Page currentPage = getCurrentPage(resources);
  try {
   limit = StringUtils.isBlank(configMap.get(UnileverConstants.LIMIT)) ? CommonConstants.TWENTY : Integer.parseInt(configMap
     .get(UnileverConstants.LIMIT));
  } catch (NumberFormatException nfe) {
   LOGGER.error("limit for page list must be number", nfe);
  }
  
  Map<String, List<String>> propMap = new HashMap<String, List<String>>();
  
  propMap.put(UnileverConstants.JCR_CONTENT_CONTENT_TYPE, propValueList);
  String[] include = !StringUtils.isBlank(configMap.get(UnileverConstants.INCLUDE_LIST)) ? configMap.get(UnileverConstants.INCLUDE_LIST).split(
    CommonConstants.COMMA_CHAR) : new String[] {};
  String[] exclude = !StringUtils.isBlank(configMap.get(UnileverConstants.EXCLUDE_LIST)) ? configMap.get(UnileverConstants.EXCLUDE_LIST).split(
    CommonConstants.COMMA_CHAR) : new String[] {};
  List<String> includeTagIdList = getIncludeTagIdList(currentPage, include);
  List<String> excludeTagIdList = Arrays.asList(exclude);
  LOGGER.debug("MultipleRelatedArticlesBaseViewHelperImpl includeTagIdList : " + includeTagIdList);
  LOGGER.debug("MultipleRelatedArticlesBaseViewHelperImpl excludeTagIdList : " + excludeTagIdList);
  
  String[] filters = ComponentUtil.getFilterTagsNamespace(properties, configMap);
  LOGGER.debug("MultipleRelatedArticlesBaseViewHelperImpl filters {}", filters);
  List<TaggingSearchCriteria> filteredTagList = ComponentUtil.getFilterTagList(currentPage, filters, includeTagIdList);
  LOGGER.debug("MultipleRelatedArticlesBaseViewHelperImpl filteredTagList {}", filteredTagList);
  
  String currentPagePath = currentPage.getPath();
  if (filteredTagList != null && !filteredTagList.isEmpty()) {
   return getSearchService().getMultiTagResults(filteredTagList, excludeTagIdList, currentPagePath, propMap,
     new SearchParam(offset, limit, UnileverConstants.DESCENDING), getResourceResolver(resources));
  } else {
   return new ArrayList<SearchDTO>();
  }
  
 }
 
 /**
  * Gets the articles page path list.
  * 
  * @param currentPage
  *         the current page
  * @param configMap
  *         the config map
  * @param properties
  * @return the articles page path list
  */
 protected List<SearchDTO> getArticlesPagePathList(Map<String, String> configMap, Map<String, Object> properties, Map<String, Object> resources) {
  List<String> propValueList = new ArrayList<String>();
  propValueList.add(UnileverConstants.ARTICLE);
  return getArticlesPagePathList(configMap, properties, propValueList, resources);
 }
 
 /**
  * Gets the path list.
  * 
  * @param list
  *         the list
  * @return the path list
  */
 protected List<String> getPathList(List<SearchDTO> list) {
  List<String> pathList = new ArrayList<String>();
  for (SearchDTO obj : list) {
   pathList.add(obj.getResultNodePath());
  }
  return pathList;
 }
 
 /**
  * Gets the include tag id list.
  * 
  * @param page
  *         the page
  * @param filterTagIds
  *         the filter tag ids
  * @return the include tag id list
  */
 protected List<String> getIncludeTagIdList(Page page, String[] filterTagIds) {
  List<String> filterTagList = Arrays.asList(filterTagIds);
  List<String> pageTagIdList = new ArrayList<String>();
  Tag[] pageTagList = page.getTags();
  if (pageTagList != null) {
   for (Tag tag : pageTagList) {
    if (tag != null) {
     String tagId = tag.getTagID();
     if (isValidToShow(tagId, filterTagList)) {
      pageTagIdList.add(tagId);
     }
    }
   }
  }
  LOGGER.debug("Valid Tag Id List, which are to be in the exclude list = " + pageTagIdList);
  return pageTagIdList;
 }
 
 // This method checks if the tagId is child of any Tag, which are in the
 // include list
 /**
  * Checks if is valid to show.
  * 
  * @param tagId
  *         the tag id
  * @param includeList
  *         the include list
  * @return true, if is valid to show
  */
 protected boolean isValidToShow(String tagId, List<String> includeList) {
  Iterator<String> itr = includeList.iterator();
  while (itr.hasNext()) {
   String parentTagId = itr.next();
   if (!StringUtils.isBlank(tagId) && tagId.startsWith(parentTagId)) {
    return true;
   }
  }
  return false;
 }
 
 /**
  * Gets the search service.
  * 
  * @return the search service
  */
 protected SearchService getSearchService() {
  
  return this.searchService;
 }
 
 /**
  * Gets the config service.
  * 
  * @return the config service
  */
 protected ConfigurationService getConfigService() {
  
  return this.configurationService;
 }
 
}
