/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.components;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.PageSortingHelper;
import com.unilever.platform.foundation.components.helper.ProductHelper;

/**
 * The Class SearchResultComparator.
 */
public class ProductResultComparator implements Comparator<Object> {
 
 /** The page manager. */
 private PageManager pageManager;
 
 /** The featuretag. */
 private String[] featuretag;
 
 /** The customizable tag. */
 private String customizableTag = StringUtils.EMPTY;
 
 /** The sort by date only. */
 private boolean sortByDateOnly = false;
 
 /** The is ascending. */
 private boolean isAscending = false;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ComponentUtil.class);
 
 /**
  * Instantiates a new search result comparator.
  * 
  * @param featuretag
  *         the featuretag
  * @param customizableTag
  *         the customizable tag
  * @param resourceResolver
  *         the resource resolver
  */
 public ProductResultComparator(String[] featuretag, String customizableTag, ResourceResolver resourceResolver) {
  super();
  this.pageManager = resourceResolver.adaptTo(PageManager.class);
  this.featuretag = featuretag != null?featuretag:new String[]{};
  this.customizableTag = customizableTag;
  
 }
 
 /**
  * Instantiates a new product result comparator.
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param sortByDateOnly
  *         the sort by date only
  * @param isAscending
  *         the is ascending
  */
 public ProductResultComparator(ResourceResolver resourceResolver, boolean sortByDateOnly, boolean isAscending) {
  super();
  this.pageManager = resourceResolver.adaptTo(PageManager.class);
  this.sortByDateOnly = sortByDateOnly;
  this.isAscending = isAscending;
  
 }
 
 @Override
 /*
  * (non-Javadoc)
  * 
  * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
  */
 public int compare(Object resultNodeObj1, Object resultNodeObj2) {
  
  int response = 0;
  
  Page productPage1 = pageManager.getPage((String) resultNodeObj1);
  Page productPage2 = pageManager.getPage((String) resultNodeObj2);
  
  UNIProduct currentProduct1 = ProductHelper.getUniProduct(productPage1);
  UNIProduct currentProduct2 = ProductHelper.getUniProduct(productPage2);
  
  if (currentProduct1 != null && currentProduct2 != null) {
   
   Date launchDate1 = getProductDate(currentProduct1);
   Date launchDate2 = getProductDate(currentProduct2);
   int dateCheck = launchDate2.compareTo(launchDate1);
   if (this.sortByDateOnly) {
    response = sortByDate(dateCheck);
   } else {
    boolean[] customizeTagArr = PageSortingHelper.pageHasTag(productPage1, productPage2, customizableTag);
    
    boolean[] featuredArr = PageSortingHelper.pageHasTagFromRequest(productPage1, productPage2, featuretag);
    
    if (customizeTagArr[0] != customizeTagArr[1]) {
     response = customizeTagArr[1] ? 1 : -1;
    } else if (featuredArr[0] != featuredArr[1]) {
     response = featuredArr[1] ? 1 : -1;
    } else if (response == 0) {
     response = dateCheck;
    }
   }
  }
  return response;
  
 }
 
 /**
  * Sort by date.
  * 
  * @param result
  *         the result
  * @return the int
  */
 private int sortByDate(int result) {
  int response = 0;
  if (!this.isAscending && result == -1) {
   response = 1;
  } else if (!this.isAscending && result == 1) {
   response = -1;
  } else {
   response = result;
  }
  return response;
 }
 
 /**
  * Get the date from unilever product and if the date is null sets default date to '01/01/1900'.
  * 
  * @param uniProduct
  *         the uni product
  * @return the product date
  */
 private Date getProductDate(UNIProduct uniProduct) {
  Date date = null;
  if (uniProduct != null) {
   date = uniProduct.getProductionDate();
   if (date == null) {
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    try {
     date = sdf.parse("01/01/1900");
    } catch (ParseException e) {
     LOGGER.warn("Could not format date");
    }
   }
  }
  
  return date;
 }
 
}
