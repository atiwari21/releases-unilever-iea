/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.constants.CommonConstants;
import com.unilever.platform.aem.foundation.core.service.SearchService;
import com.unilever.platform.aem.foundation.utils.AdaptiveUtil;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.LandingPageListHelper;
import com.unilever.platform.foundation.components.helper.ProductBundlingToBinUtil;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.components.helper.StoreLocatorUtility;
import com.unilever.platform.foundation.components.helper.TaggingComponentsHelper;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.kritique.config.KritiqueReviewsCommentsService;
import com.unilever.platform.foundation.template.dto.KritiqueDTO;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.MultipleRelatedProductsConstants;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;
import com.unilever.platform.foundation.viewhelper.constants.StoreLocatorConstants;
import com.unilever.platform.foundation.viewhelper.productoverview.config.ConfigureProductImageRenditionService;

/**
 * <p>
 * Responsible for reading author input for prduct copy component and generating
 * a list of collapsible sections setting in pageContext for the view. Input can
 * be manual or reading property attributes from product data.
 * </p>
 */
@Component(description = "ProductListingV2ViewHelperImpl", immediate = true, metatype = true, label = "ProductListingV2ViewHelperImpl")
@Service(value = { ProductListingV2ViewHelperImpl.class, ViewHelper.class })
@Properties({
		@Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.PRODUCT_LISTING_V2, propertyPrivate = true),

		@Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, 
		propertyPrivate = true) })
public class ProductListingV2ViewHelperImpl extends BaseViewHelper {

	private static final String ONE = "1";

	private static final String PRODUCT_LISTING_V2_FIND_OUT_MORE = "productListingV2.findOutMore";

	private static final String PRODUCT_NAVIGATION_LABEL = "productNavigationLabel";

	private static final String ERROR_MESSAGE = "errorMessage";

	private static final String GO_CTA = "goCta";

	private static final String GPS_SHARE_LOCATION = "gpsShareLocation";

	private static final String POSTAL_CODE_FIELD_HELP_TEXT = "postalCodeFieldHelpText";

	private static final String BUY_IN_STORE = "buyInStore";

	private static final String CTA_LABELS = "ctaLabels";

	/** The Constant PAGINATION. */
	private static final String PAGINATION = "pagination";

	/** The Constant STORE_LOCATOR. */
	private static final String STORE_LOCATOR = "storeLocator";

	/** The Constant PRODUCT_LISTING_RESET_FILTER_CTA. */
	private static final String PRODUCT_LISTING_RESET_FILTER_CTA = "productListing.resetFilterCta";

	/** The Constant RESET_FILTER_CTA. */
	private static final String RESET_FILTER_CTA = "resetFilterCta";

	/** The Constant PRODUCT_LISTING_NO_RESULTS_MESSAGE. */
	private static final String PRODUCT_LISTING_NO_RESULTS_MESSAGE = "productListing.noResultsMessage";

	/** The Constant STORE_SEARCH_CTA_LABEL. */
	private static final String STORE_SEARCH_CTA_LABEL = "storeSearch.ctaLabel";

	/** The Constant STORE_SEARCH_GPS_SHARE_LOCATION_CTA_LABEL. */
	private static final String STORE_SEARCH_GPS_SHARE_LOCATION_CTA_LABEL = "storeSearch.gpsShareLocationCtaLabel";

	/** The Constant STORE_SEARCH_POSTCODE_FIELD_HELP_TEXT. */
	private static final String STORE_SEARCH_POSTCODE_FIELD_HELP_TEXT = "storeSearch.postcodeFieldHelpText";

	/** The Constant STORE_SEARCH_HEADING_TEXT. */
	private static final String STORE_SEARCH_HEADING_TEXT = "storeSearch.headingText";

	/** The Constant PRODUCTS. */
	private static final String PRODUCTS = "products";

	/** The Constant PRODUCT_LIST. */
	private static final String PRODUCT_LIST = "productList";

	/** The Constant PRODUCT_PATH. */
	private static final String PRODUCT_PATH = "productPath";

	/** The Constant TEXT. */
	private static final String TEXT = "text";

	/** The Constant PAGINATION_CTA_LABEL. */
	private static final String PAGINATION_CTA_LABEL = "paginationCtaLabel";

	/** The Constant ZERO. */
	private static final int ZERO = 0;

	/** The Constant PAGINATION_TYPE. */
	private static final String PAGINATION_TYPE = "paginationType";

	/** The Constant PAGE_NO_PARAM_NAME. */
	private static final String PAGE_NO_PARAM_NAME = "pageNoParamName";

	/** The Constant FILTER_PARAM_NAME. */
	private static final String FILTER_PARAM_NAME = "filterParamName";

	/** The Constant NEXT_LABEL. */
	private static final String NEXT_LABEL = "nextLabel";

	/** The Constant PREVIOUS_LABEL. */
	private static final String PREVIOUS_LABEL = "previousLabel";

	/** The Constant LAST_LABEL. */
	private static final String LAST_LABEL = "lastLabel";

	/** The Constant FIRST_LABEL. */
	private static final String FIRST_LABEL = "firstLabel";

	/** The Constant SUB_HEADING_TEXT. */
	private static final String SUB_HEADING_TEXT = "subHeadingText";

	/** The Constant HEADING_TEXT. */
	private static final String HEADING_TEXT = "headingText";

	/** The Constant FILTER_CONFIG. */
	private static final String FILTER_CONFIG = "filterConfig";

	/** The Constant AJAX_URL. */
	private static final String AJAX_URL = "ajaxURL";

	/** The Constant DATA_JSON. */
	private static final String DATA_JSON = ".data.json";

	/** The Constant TOTAL_COUNT. */
	private static final String TOTAL_COUNT = "totalCount";

	/** The Constant TAGS. */
	private static final String TAGS = "tags";

	/** The Constant FIXED_LIST. */
	private static final String FIXED_LIST = "fixedList";

	/** The Constant GLOBAL_CONFIG. */
	private static final String GLOBAL_CONFIG = "productListingV2";

	/** The Constant ITEMS_PER_PAGE. */
	private static final String ITEMS_PER_PAGE = "itemsPerPage";

	/** The Constant FEATURE_TAGS. */
	private static final String FEATURE_TAGS = "featureTags";

	/** The Constant ZERO_STRING. */
	private static final String ZERO_STRING = "0";

	/** The Constant PN. */
	private static final String PN = "pn";

	/** The Constant FL. */
	private static final String FL = "fl";

	/** The Constant STATIC. */
	private static final String STATIC = "static";
	/** The Constant TEASER IMAGE FLAG. */
	private static final String PRODUCT_RATING = "productRating";
	/** The Constant TEASER COPY FLAG. */
	private static final String QUICK_VIEW = "quickView";
	/** The Constant TEASER LONG COPY. */
	private static final String BUY_IT_NOW = "buyItNow";
	/** The Constant OVERRIDE GLOBAL CONFIG. */
	private static final String OVERRIDE_GLOBAL_CONFIG = "overrideGlobalConfig";
	/** The Constant CONTAINER_TAG. */
	private static final String CONTAINER_TAG = "containertag";
	/** The Constant RATINGS. */
	private static final String RATINGS = "ratings";
	/** The Constant PROD_QUICK_VIEW. */
	private static final String PROD_QUICK_VIEW = "productQuickView";
	/**
	 * logger object.
	 */
	/** The kritique service. */
	@Reference
	KritiqueReviewsCommentsService kritiqueReviewsCommentsService;

	/** The configuration service. */
	@Reference
	ConfigurationService configurationService;

	/** The search service. */
	@Reference
	SearchService searchService;

	/** The configure product image rendition service. */
	@Reference
	ConfigureProductImageRenditionService configureProductImageRenditionService;

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(ProductListingV2ViewHelperImpl.class);

	/** The Constant GENERAL_CONFIG. */
	private static final String GENERAL_CONFIG = "generalConfig";

	/** The Constant BUILD_NAVIGATION_USING. */
	private static final String BUILD_LIST_USING = "buildListUsing";

	/*
	 * (non-Javadoc)
	 * 
	 * 
	 * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#
	 * processData (java.util.Map, java.util.Map)
	 */
	@Override
	public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
		LOGGER.debug("product Listing View Helper #processData called .");
		Map<String, Object> properties = getProperties(content);
		Map<String, Object> productListingMap = new LinkedHashMap<String, Object>();
		List<String> productsPagePathList = new LinkedList<String>();
		Page currentPage = getCurrentPage(resources);
		Map<String, String> globalConfigMap = ComponentUtil.getConfigMap(currentPage, GLOBAL_CONFIG);
		String buildNavigationUsing = MapUtils.getString(properties, BUILD_LIST_USING, StringUtils.EMPTY);
		int itemsPerPage = MapUtils.getIntValue(properties, ITEMS_PER_PAGE);
		itemsPerPage = (itemsPerPage == ZERO)
				? Integer.parseInt(MapUtils.getString(globalConfigMap, ITEMS_PER_PAGE, ZERO_STRING)) : itemsPerPage;
		I18n i18n = getI18n(resources);
		if (FIXED_LIST.equals(buildNavigationUsing)) {
			productsPagePathList = getFixedProducts(getPageManager(resources),
					getCurrentResource(resources));

		} else if (TAGS.equals(buildNavigationUsing)) {
			List<String> propValueList = new ArrayList<String>();
			propValueList.add(UnileverConstants.PRODUCT);
			Map<String, List<String>> propMap = new HashMap<String, List<String>>();
			propMap.put(UnileverConstants.JCR_CONTENT_CONTENT_TYPE, propValueList);
			productsPagePathList = TaggingComponentsHelper.getTaggedPageList(properties, globalConfigMap, currentPage,
					getPageManager(resources), propMap, searchService, resources);
		}
		if (StringUtils.isNotBlank(buildNavigationUsing)) {
			productsPagePathList = ProductBundlingToBinUtil.getProductsListForMultipleBuy(productsPagePathList,
					currentPage, configurationService);
			ProductBundlingToBinUtil.setMultiBuyProductsConfigProperties(properties, productListingMap, globalConfigMap,
			        isProductAttributeOrOverrideGlobal(properties, null, true, OVERRIDE_GLOBAL_CONFIG),
					configurationService, currentPage);
			ProductBundlingToBinUtil.setMultiBuyProductsCtaLabels(resources, productListingMap);
			productListingMap.put(PAGINATION, getPaginationData(i18n, properties, itemsPerPage));
			productListingMap.put(STORE_LOCATOR, getStoreLocatorMap(i18n, resources));
			productListingMap.put(GENERAL_CONFIG, getGeneralConfigDataMap(properties, i18n));
			productListingMap.put(STATIC, getStaticMap(i18n));
			setUpdatedPagesList(content, resources, productListingMap, productsPagePathList);
			productListingMap.put(ProductConstants.PRICE_ENABLED,
					ProductHelper.isPriceEnabled(configurationService, currentPage));
		}
		Map<String, Object> data = new HashMap<String, Object>();
		data.put(getJsonNameSpace(content), productListingMap);
		return data;
	}

	/**
	 * Gets the static map.
	 * 
	 * @param i18n
	 *            the i18n
	 * @return the static map
	 */
	private Map<String, Object> getStaticMap(I18n i18n) {
		Map<String, Object> staticMap = new LinkedHashMap<String, Object>();
		staticMap.put(PRODUCT_NAVIGATION_LABEL, i18n.get(PRODUCT_LISTING_V2_FIND_OUT_MORE));
		return staticMap;
	}

	/**
	 * Sets the updated pages list.
	 * 
	 * @param content
	 *            the content
	 * @param resources
	 *            the resources
	 * @param productListingMap
	 *            the product listing map
	 * @param productsPagePathList
	 *            the products page path list
	 */
	private void setUpdatedPagesList(Map<String, Object> content, Map<String, Object> resources,
			Map<String, Object> productListingMap, List<String> productsPagePathList) {
		Map<String, Object> properties = getProperties(content);
		Page currentPage = getCurrentPage(resources);
		SlingHttpServletRequest slingHttpServletRequest = getSlingRequest(resources);
		Map<String, String> globalConfigMap = ComponentUtil.getConfigMap(currentPage, GLOBAL_CONFIG);
		String[] filters = slingHttpServletRequest.getParameterValues(FL) != null
				? slingHttpServletRequest.getParameterValues(FL) : new String[] {};
		String[] featureTagNameSpaces = StringUtils.isNotBlank(MapUtils.getString(properties, FEATURE_TAGS))
				? ComponentUtil.getPropertyValueArray(properties, FEATURE_TAGS) : new String[] {};
		List<Map<String, Object>> pagePathList = setUpdatedProductMap(content, resources, productListingMap,
				productsPagePathList, globalConfigMap, filters, featureTagNameSpaces);
		Map<String, Object> filterMaps = LandingPageListHelper.getFilterProperties(properties, pagePathList,
				currentPage, getTagManager(resources), getCurrentResource(resources), GLOBAL_CONFIG);
		filterMaps.put(FILTER_PARAM_NAME, FL);
		filterMaps.put(PAGE_NO_PARAM_NAME, PN);
		String resPath = getCurrentResource(resources).getPath();

		String ajaxURL = getResourceResolver(resources).map(resPath + DATA_JSON);
		filterMaps.put(AJAX_URL, ajaxURL);
		int itemsPerPage = MapUtils.getIntValue(properties, ITEMS_PER_PAGE);
		itemsPerPage = (itemsPerPage == ZERO)
				? Integer.parseInt(MapUtils.getString(globalConfigMap, ITEMS_PER_PAGE, ZERO_STRING)) : itemsPerPage;
		filterMaps.put(ITEMS_PER_PAGE, itemsPerPage);
		productListingMap.put(FILTER_CONFIG, filterMaps);
	}

	/**
	 * Sets the updated product map.
	 * 
	 * @param content
	 *            the content
	 * @param resources
	 *            the resources
	 * @param productListingMap
	 *            the product listing map
	 * @param productsPagePathList
	 *            the products page path list
	 * @param globalConfigMap
	 *            the global config map
	 * @param filters
	 *            the filters
	 * @param featureTagNameSpaces
	 *            the feature tag name spaces
	 * @return the list
	 */
	private List<Map<String, Object>> setUpdatedProductMap(Map<String, Object> content, Map<String, Object> resources,
			Map<String, Object> productListingMap, List<String> productsPagePathList,
			Map<String, String> globalConfigMap, String[] filters, String[] featureTagNameSpaces) {
		Map<String, Object> properties = getProperties(content);
		SlingHttpServletRequest slingHttpServletRequest = getSlingRequest(resources);
		PageManager pageManager = getPageManager(resources);
		int itemsPerPage = MapUtils.getIntValue(properties, ITEMS_PER_PAGE);
		itemsPerPage = (itemsPerPage == ZERO)
				? Integer.parseInt(MapUtils.getString(globalConfigMap, ITEMS_PER_PAGE, ZERO_STRING)) : itemsPerPage;
		String pageNoStr = StringUtils.isNotBlank(slingHttpServletRequest.getParameter(PN))
				? slingHttpServletRequest.getParameter(PN) : ONE;
		int pageNo = ZERO;
		try {
			pageNo = Integer.parseInt(pageNoStr);
		} catch (NumberFormatException ne) {
			LOGGER.warn("Page Number is not a number", ne);
		}
		boolean overrdieGlobalConfig = isProductAttributeOrOverrideGlobal(properties, null, true, OVERRIDE_GLOBAL_CONFIG);
  boolean multiBuyEnabled = overrdieGlobalConfig?isProductAttributeOrOverrideGlobal(properties, null, true, "multiBuyEnabled"):
            isProductAttributeOrOverrideGlobal(null, globalConfigMap, false, "multiBuyEnabled");
  if(multiBuyEnabled){
   List<Map<String, Object>> localPagePathList = ProductHelper.getProductListWithTag(productsPagePathList, resources,
                GLOBAL_CONFIG, StringUtils.EMPTY);
   setMultiBuyEnabledProductCount(productListingMap, localPagePathList);
        }
		List<String> updatedPagePath = new LinkedList<String>();
		for (String pagePathItem : productsPagePathList) {
			Page listPage = pageManager.getContainingPage(pagePathItem);
			if (listPage != null && TaggingComponentsHelper.isEligbleFromFilter(listPage, filters)) {
				updatedPagePath.add(listPage.getPath());
			}
		}
		List<Map<String, Object>> pagePathList = ProductHelper.getProductListWithTag(updatedPagePath, resources,
		        GLOBAL_CONFIG, StringUtils.EMPTY);
		int pageCount = 0;
		for (Map<String, Object> updatedPageMap : pagePathList) {
			LandingPageListHelper.addFeatureTagsData(featureTagNameSpaces, updatedPageMap,
					pageManager.getContainingPage(updatedPagePath.get(pageCount)), resources);
			updatedPageMap.put(UnileverConstants.PAGE_PATH, updatedPagePath.get(pageCount));
			pageCount++;
		}
		setOverridenLandingAttribute(properties, globalConfigMap, pagePathList);
		productListingMap.put(TOTAL_COUNT, pagePathList.size());
		Map<String, Object> productMap = getProductMap(resources, productsPagePathList, properties, pagePathList,
				globalConfigMap);
		setProductList(productListingMap, itemsPerPage, pageNo, productMap);
		return pagePathList;
	}

    /**sets count of enabled multi buy configuration for each product
     * @param productListingMap
     * @param globalConfigMap
     * @param properties
     * @param pagePathList
     */
 private void setMultiBuyEnabledProductCount(Map<String, Object> productListingMap, List<Map<String, Object>> pagePathList) {
  int multiBuyEnabledProductsCount = ZERO;
  List<String> enabledMultiBuyProductIds = new ArrayList<String>();
		for(Map<String, Object> productListDataMap : pagePathList){
   if(productListDataMap.get(ProductConstants.PRODUCT_DETAIL_MAP) != null){
    Object[] productsDetailList = (Object[])productListDataMap.get(ProductConstants.PRODUCT_DETAIL_MAP);
    multiBuyEnabledProductsCount = getProductBundlingCount(multiBuyEnabledProductsCount,
            productsDetailList, productListDataMap, enabledMultiBuyProductIds);
		    }
		}
		productListingMap.put("multiBuyEnabledProductsCount", multiBuyEnabledProductsCount);
		productListingMap.put("multiBuyEnabledProductIds", enabledMultiBuyProductIds.toArray());
    }

    /**gets the number of multi buy enabled count
     * @param multiBuyEnabledProductsCount
     * @param productsDetailList
     * @param productListDataMap
     * @param enabledMultiBuyProductIds
     * @return
     */
    @SuppressWarnings("unchecked")
 private int getProductBundlingCount(int multiBuyEnabledProductsCount,
         Object[] productsDetailList, Map<String, Object> productListDataMap, List<String> enabledMultiBuyProductIds) {
  int localEnableMutiBuyCount = multiBuyEnabledProductsCount;
  if(productsDetailList!=null && productsDetailList.length>ZERO){
   Map<String, Object> productDetailsMap = (Map<String, Object>) productsDetailList[ZERO];
   if(!MapUtils.getBooleanValue(productDetailsMap, ProductConstants.DISABLE_BUY_IT_NOW, false)){
    localEnableMutiBuyCount++;
    enabledMultiBuyProductIds.add(MapUtils.getString(productListDataMap, ProductConstants.PRODUCT_ID, StringUtils.EMPTY));
 }
        }
  return localEnableMutiBuyCount;
    }

	/**
	 * Sets the product list.
	 * 
	 * @param productListingMap
	 *            the product listing map
	 * @param itemsPerPage
	 *            the items per page
	 * @param pageNo
	 *            the page no
	 * @param productMap
	 *            the product map
	 */
	@SuppressWarnings("unchecked")
	private void setProductList(Map<String, Object> productListingMap, int itemsPerPage, int pageNo,
			Map<String, Object> productMap) {
		List<Map<String, Object>> updatedProductList = new LinkedList<Map<String, Object>>();
		if (productMap.get(PRODUCT_LIST) != null) {
			updatedProductList = (List<Map<String, Object>>) productMap.get(PRODUCT_LIST);
		}
		List<Map<String, Object>> list = LandingPageListHelper.getSubListByPage(updatedProductList, pageNo,
				itemsPerPage);
		if (list != null) {
			productMap.replace(PRODUCT_LIST, list.toArray());
		} else {
   updatedProductList = new LinkedList<Map<String, Object>>();
			productMap.put(PRODUCT_LIST, updatedProductList.toArray());
		}
		productListingMap.put(PRODUCTS, productMap);
	}

	/**
	 * gets the general config data like heading,sub heading of component
	 * section.
	 * 
	 * @param properties
	 *            the properties
	 * @param i18n
	 * @return map
	 */
	protected Map<String, Object> getGeneralConfigDataMap(Map<String, Object> properties, I18n i18n) {
		Map<String, Object> generalConfigMap = new LinkedHashMap<String, Object>();
		generalConfigMap.put(HEADING_TEXT, MapUtils.getString(properties, HEADING_TEXT, StringUtils.EMPTY));
		generalConfigMap.put(SUB_HEADING_TEXT, MapUtils.getString(properties, TEXT, StringUtils.EMPTY));
		generalConfigMap.put(BUILD_LIST_USING, MapUtils.getString(properties, BUILD_LIST_USING, StringUtils.EMPTY));
		generalConfigMap.put(PRODUCT_NAVIGATION_LABEL, i18n.get(PRODUCT_LISTING_V2_FIND_OUT_MORE));
		return generalConfigMap;
	}

	/**
	 * returns the map which has static values like pagination labels.
	 * 
	 * @param i18n
	 *            the i18n
	 * @param properties
	 *            the properties
	 * @param itemsPerPage
	 *            the items per page
	 * @return map
	 */
	private Map<String, Object> getPaginationData(I18n i18n, Map<String, Object> properties, int itemsPerPage) {
		Map<String, Object> staticMap = new LinkedHashMap<String, Object>();
		staticMap.put(FIRST_LABEL, i18n.get("pageListing.pagination.first"));
		staticMap.put(LAST_LABEL, i18n.get("pageListing.pagination.last"));
		staticMap.put(PREVIOUS_LABEL, i18n.get("pageListing.pagination.last"));
		staticMap.put(NEXT_LABEL, i18n.get("pageListing.pagination.next"));
		Map<String, Object> paginationMap = new LinkedHashMap<String, Object>();
		String paginationType = MapUtils.getString(properties, PAGINATION_TYPE, StringUtils.EMPTY);
		String paginationCtaLabel = MapUtils.getString(properties, PAGINATION_CTA_LABEL, StringUtils.EMPTY);
		paginationMap.put(PAGINATION_TYPE, paginationType);
		paginationMap.put(PAGINATION_CTA_LABEL, paginationCtaLabel);
		paginationMap.put(ITEMS_PER_PAGE, itemsPerPage);
		paginationMap.put(STATIC, staticMap);
		return paginationMap;
	}

	/**
	 * returns the map which has store locator details and static values like
	 * result message keys.
	 * 
	 * @param i18n
	 *            the i18n
	 * @param currentPage
	 *            the current page
	 * @param resourceResolver
	 *            the resource resolver
	 * @param storeLocatorCategoryKey
	 *            the store locator category key
	 * @return map
	 */
	private Map<String, Object> getStoreLocatorMap(I18n i18n, Map<String, Object> resources) {
		Map<String, Object> storeLocatorConfigurationMap = StoreLocatorUtility
				.getStoreLocatorMap(getCurrentPage(resources), null, configurationService, getSlingRequest(resources));
		storeLocatorConfigurationMap.put(StoreLocatorConstants.STORE_LOCATOR_ENABLED, StoreLocatorUtility
				.isStoreLocatorEnabled(getCurrentPage(resources), configurationService, GLOBAL_CONFIG));
		Map<String, String> staticMap = new LinkedHashMap<String, String>();
		staticMap.put(BUY_IN_STORE, i18n.get(STORE_SEARCH_HEADING_TEXT));
		staticMap.put(POSTAL_CODE_FIELD_HELP_TEXT, i18n.get(STORE_SEARCH_POSTCODE_FIELD_HELP_TEXT));
		staticMap.put(GPS_SHARE_LOCATION, i18n.get(STORE_SEARCH_GPS_SHARE_LOCATION_CTA_LABEL));
		staticMap.put(GO_CTA, i18n.get(STORE_SEARCH_CTA_LABEL));
		staticMap.put(ERROR_MESSAGE, i18n.get(PRODUCT_LISTING_NO_RESULTS_MESSAGE));
		staticMap.put(RESET_FILTER_CTA, i18n.get(PRODUCT_LISTING_RESET_FILTER_CTA));
		storeLocatorConfigurationMap.put(CTA_LABELS, staticMap);
		return storeLocatorConfigurationMap;
	}

	/**
	 * returns list of product pages.
	 * 
	 * @param productListingProperties
	 *            the product listing properties
	 * @param resourceResolver
	 *            the resource resolver
	 * @param pageManager
	 * @return the fixed products
	 */
	public List<String> getFixedProducts(PageManager pageManager,Resource resource) {
		List<String> propertyNames = new ArrayList<String>();
		propertyNames.add(PRODUCT_PATH);
		List<Map<String, String>> prodSectionsList = ComponentUtil.getNestedMultiFieldProperties(resource, "sections");
		return prepareSectionList(prodSectionsList, pageManager);
	}

	/**
	 * returns list of product paths.
	 * 
	 * @param productSectionsList
	 *            the product sections list
	 * @param pageManager
	 *            the pageManager
	 * @return the list
	 */
	public List<String> prepareSectionList(List<Map<String, String>> productSectionsList, PageManager pageManager) {
		List<String> finalSectionsList = new ArrayList<String>();
		for (Map<String, String> sectionMap : productSectionsList) {
			String prodPath = sectionMap.get(PRODUCT_PATH);
			Page page = StringUtils.isNotBlank(prodPath) && pageManager != null
					? pageManager.getContainingPage(prodPath) : null;
			if (ProductHelper.getUniProduct(page) != null) {
				finalSectionsList.add(prodPath);
			}
		}

		return finalSectionsList;
	}

	/**
	 * Gets the product map.
	 * 
	 * @param resources
	 *            the resources
	 * @param productsPagePathList
	 *            the products page path list
	 * @param properties
	 *            the properties
	 * @param list
	 *            the list
	 * @param configMap
	 *            the config map
	 * @return the product map
	 */
	protected Map<String, Object> getProductMap(final Map<String, Object> resources, List<String> productsPagePathList,
			Map<String, Object> properties, List<Map<String, Object>> list, Map<String, String> configMap) {
		Map<String, Object> productMap = new HashMap<String, Object>();

		List<Map<String, Object>> productRatingPropertiesList = new ArrayList<Map<String, Object>>();
		Page currentPage = getCurrentPage(resources);
		if (AdaptiveUtil.isAdaptive(getSlingRequest(resources))) {

			String prodids = StringUtils.EMPTY;
			KritiqueDTO kritiqueparams = new KritiqueDTO(currentPage, configurationService);
			LOGGER.info("Feature phone view called and kritique api is going to be called");
			for (String pagePath : productsPagePathList) {

				if (ProductHelper.getUniProduct(getPageManager(resources).getPage(pagePath)) != null) {
					prodids += ProductHelper.getUniProduct(getPageManager(resources).getPage(pagePath)).getSKU() + ",";
				}
			}
			if (prodids != null) {
				kritiqueparams.setSiteProductId(prodids);
			}

			List<Map<String, Object>> kqresponse = kritiqueReviewsCommentsService.getRatingsService(kritiqueparams,
					configurationService);
			LOGGER.debug("Kritique response generated and size of the review list is " + kqresponse.size());
			productRatingPropertiesList = ComponentUtil.mapKritiqueProductRating(kqresponse, list);
			productMap.put(ProductConstants.PRODUCT_LIST_MAP, productRatingPropertiesList);

		} else {
			productMap.put(ProductConstants.PRODUCT_LIST_MAP, list);
		}

		Map<String, Object> staticMap = new HashMap<String, Object>();
		Map<String, Object> zoomMapProperties = new LinkedHashMap<String, Object>();
		staticMap.put(ProductConstants.PRODUCT_COUNT_LABEL,
				getI18n(resources).get(ProductConstants.PRODUCT_COUNT_LABEL));
		boolean overrideGlobalConfig = isProductAttributeOrOverrideGlobal(properties, configMap, true, OVERRIDE_GLOBAL_CONFIG);
		boolean buyItNowFlag = isProductAttributeOrOverrideGlobal(properties, configMap, overrideGlobalConfig, BUY_IT_NOW);
		Map<String, String> shopNowMap = (buyItNowFlag)
				? ProductHelper.getshopNowProperties(configurationService, currentPage,
						getI18n(resources), getResourceResolver(resources), getSlingRequest(resources))
				: null;
		productMap.put(ProductConstants.SHOPNOW_MAP, shopNowMap);
		productMap.put(ProductConstants.REVIEW_MAP,
				ProductHelper.getReviewMapProperties(configurationService, currentPage));
		String title = MapUtils.getString(properties, MultipleRelatedProductsConstants.TITLE, StringUtils.EMPTY);
		productMap.put(MultipleRelatedProductsConstants.TITLE, title);
		staticMap = ProductHelper.getStaticMapProperties(staticMap, zoomMapProperties, configurationService,
				getI18n(resources), currentPage);
		productMap.put(ProductConstants.STATIC_MAP, staticMap);
		return productMap;
	}

	/**
	 * setting enable for teaser title,copy,image and publish date.
	 * 
	 * @param properties
	 *            the properties
	 * @param configMap
	 *            the config map
	 * @param list
	 *            the list
	 */
	private void setOverridenLandingAttribute(Map<String, Object> properties, Map<String, String> configMap,
			List<Map<String, Object>> list) {
		boolean overrideGlobalConfig = isProductAttributeOrOverrideGlobal(properties, configMap, true, OVERRIDE_GLOBAL_CONFIG);
		boolean productRatingFlag = isProductAttributeOrOverrideGlobal(properties, configMap, overrideGlobalConfig, PRODUCT_RATING);
		boolean quickViewFlag = isProductAttributeOrOverrideGlobal(properties, configMap, overrideGlobalConfig, QUICK_VIEW);
		boolean buyItNowFlag = isProductAttributeOrOverrideGlobal(properties, configMap, overrideGlobalConfig, BUY_IT_NOW);
		setUpdatedProductListData(list, productRatingFlag, quickViewFlag, buyItNowFlag, RATINGS, PROD_QUICK_VIEW, ProductConstants.SHOPNOW_MAP);
	}

	/**
	 * returns boolean flag based on oveerriden global config flag.
	 * 
	 * @param properties
	 *            the properties
	 * @param configMap
	 *            the config map
	 * @param overrideGlobalConfig
	 *            the override global config
	 * @param productKey
	 *            the product key
	 * @return true, if is product attribute true
	 */
	private boolean isProductAttributeOrOverrideGlobal(Map<String, Object> properties, Map<String, String> configMap,
			boolean overrideGlobalConfig, String productKey) {
		if (overrideGlobalConfig) {
			return MapUtils.getBooleanValue(properties, productKey, false);
		} else {
			return MapUtils.getBooleanValue(configMap, productKey, false);
		}
	}

	/**
	 * sets the product attributes ratings,quick view and buy now enable or
	 * disable based on flag.
	 * 
	 * @param productListData
	 *            the product list data
	 * @param productFlag
	 *            the product flag
	 * @param quickViewFlag
	 *            the quick view flag
	 * @param keyToBeNull
	 *            the key to be null
	 */
	private void setUpdatedProductListData(List<Map<String, Object>> productListData, boolean productRatingFlag,
			boolean quickViewFlag, boolean buyItNowFlag, String ratingKey, String quickViewKey, String shopNowKey) {

  boolean[] flagKeys = {productRatingFlag, quickViewFlag, buyItNowFlag};
  String[] overridenKeys = {ratingKey, quickViewKey, shopNowKey};
  for(Map<String, Object> productListDataMap : productListData){
   for (int i=CommonConstants.ZERO;i<flagKeys.length;i++) {
    if(!flagKeys[i]){
     disableContainerTagMap(overridenKeys[i], productListDataMap);
     }
			}
			}

	}

	/**
	 * puts the map to be null for container tag in case of false flag.
	 * 
	 * @param keyToBeNull
	 *            the key to be null
	 * @param productListDataMap
	 *            the product list data map
	 */
	private void disableContainerTagMap(String keyToBeNull, Map<String, Object> productListDataMap) {
  String key = PROD_QUICK_VIEW.equals(keyToBeNull)?ProductConstants.QUICK_VIEW_LABEL:keyToBeNull;
  if(!StringUtils.equals(key, ProductConstants.SHOPNOW)){
   productListDataMap.put(key, null);
  }
  if(PROD_QUICK_VIEW.equals(keyToBeNull)){
   productListDataMap.put(ProductConstants.QUICK_VIEW_LABEL, StringUtils.EMPTY);
        }
  if (productListDataMap.get(CONTAINER_TAG) != null && !(RATINGS).equals(keyToBeNull)) {
            @SuppressWarnings("unchecked")
   Map<String, Object> conatinerDataMap = (Map<String, Object>) productListDataMap.get(CONTAINER_TAG);
   conatinerDataMap.put(keyToBeNull, null);
        }
    }
}
