/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.SocialTABHelper;
import com.unilever.platform.foundation.constants.CommonConstants;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.SocialTabQueryDTO;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * The Class MultipleRelatedTweetsAreaViewHelperImpl.
 */
@Component(description = "MultipleRelatedTweetsAreaViewHelperImpl", immediate = true, metatype = true, label = "Multiple Related Tweets Area View Helper Impl")
@Service(value = { MultipleRelatedTweetsAreaViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.MULTIPLE_RELATED_TWEETS_AREA, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class MultipleRelatedTweetsAreaViewHelperImpl extends SocialTabBaseViewHelperImpl {
 /**
  * Constant for title.
  */
 private static final String TITLE = "title";
 
 /**
  * Constant for Introduction Copy.
  */
 private static final String INTRO_COPY = "introCopy";
 
 /** The Constant ACCOUNT_ID. */
 private static final String ACCOUNT_ID = "accountId";
 
 /** The Constant SHOW_PROFILE_PIC. */
 private static final String SHOW_PROFILE_PIC = "showProfilePic";
 
 /** The Constant CTA_LABEL. */
 private static final String CTA_LABEL = "multipleRelatedTweets.joinTheConversation";
 
 /** The Constant CTA_URL. */
 private static final String CTA_URL = "ctaUrl";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(MultipleRelatedTweetsAreaViewHelperImpl.class);
 
 /** The Constant STRICT. */
 private static final String STRICT = "strict";
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.impl.SocialTabBaseViewHelperImpl#processData(java.util.Map, java.util.Map)
  */
 @SuppressWarnings("unchecked")
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.info("Multiple Related Tweets View Helper #processData called.");
  
  Map<String, Object> properties = getProperties(content);
  Page currentPage = getCurrentPage(resources);
  String jsonNameSpace = getJsonNameSpace(content);
  
  Map<String, Object> multipleRelatedTweetsMap = new LinkedHashMap<String, Object>();
  String accountId = getAccountId(properties);
  I18n i18n = getI18n(resources);
  
  Map<String, String> configMap = ComponentUtil.getConfigMap(currentPage, UnileverConstants.MULTIPLE_RELATED_TWEETS_CONFIG_CAT);
  int numberOfPosts = MapUtils.getIntValue(configMap, NUMBER_OF_POSTS);
  int delayTime = MapUtils.getIntValue(configMap, DELAY_TIME);
  if (configMap.containsKey(STRICT)) {
   boolean strict = MapUtils.getBooleanValue(configMap, STRICT);
   multipleRelatedTweetsMap.put(STRICT, strict);
  }
  
  SocialTabQueryDTO tabQuery = getSocialAggregatorQueryDataObj(resources, properties, currentPage, configurationService, numberOfPosts, 0);
  
  String ctaLabel = i18n.get(CTA_LABEL);
  
  String profilePicture = MapUtils.getString(properties, SHOW_PROFILE_PIC, StringUtils.EMPTY);
  String newWindow = MapUtils.getString(properties, OPEN_IN_NEW_WINDOW, StringUtils.EMPTY);
  
  String showProfilePicture = profilePicture != null && "true".equals(profilePicture) ? CommonConstants.BOOLEAN_TRUE : ProductConstants.FALSE;
  String openInNewWindow = newWindow != null && "true".equals(newWindow) ? CommonConstants.BOOLEAN_TRUE : ProductConstants.FALSE;
  
  multipleRelatedTweetsMap.put(TITLE, getTitle(properties));
  multipleRelatedTweetsMap.put(INTRO_COPY, getText(properties));
  multipleRelatedTweetsMap.put("delayTime", delayTime);
  multipleRelatedTweetsMap.put("cta", SocialTABHelper.showCtaMap(properties, ctaLabel, CTA_URL, openInNewWindow));
  multipleRelatedTweetsMap.put("showProfilePicture", Boolean.valueOf(showProfilePicture));
  
  // adding retrieval query parameters
  SocialTABHelper.addRetrievalQueryParametrsMap(multipleRelatedTweetsMap, tabQuery, true);
  Map<String, Object> retrivalQueryParams = (Map<String, Object>) multipleRelatedTweetsMap.get("retrivalQueryParams");
  
  Object[] socialChannelNamesMapArr = (Object[]) retrivalQueryParams.get("socialChannelNames");
  if (socialChannelNamesMapArr.length > 0) {
   Map<String, Object> socialChannelNamesMap = (Map<String, Object>) socialChannelNamesMapArr[0];
   if (accountId != null && !accountId.isEmpty()) {
    socialChannelNamesMap.put(ACCOUNT_ID, accountId);
   }
  }
  
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  data.put(jsonNameSpace, multipleRelatedTweetsMap);
  
  return data;
 }
 
}
