/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.SocialShareHelper;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * This class prepares the data map for Body Copy Headline Component It reads the values from the dialog and passes the info as a json.
 */

@Component(description = "BodyCopyHeadline", immediate = true, metatype = true, label = "Body Copy Headline")
@Service(value = { BodyCopyHeadlineViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.BODY_COPY_HEALINE, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class BodyCopyHeadlineViewHelperImpl extends BaseViewHelper {
 
 /** text */
 private static final String TEXT = "text";
 /** false */
 private static final String FALSE = "false";
 /** true */
 private static final String TRUE = "true";
 /** key used in map */
 private static final String SOCIAL_SHARING = "socialSharing";
 /** The Constant HEADLINE. */
 private static final String HEADLINE = "headline";
 /** The Constant SHORT_ARTICLE. */
 private static final String SHORT_ARTICLE = "shortArticle";
 /** The Constant SHOW_SOCIAL_MEDIA. */
 private static final String SHOW_SOCIAL_MEDIA = "showSocialMedia";
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(BodyCopyHeadlineViewHelperImpl.class);
 
 @Reference
 ConfigurationService configurationService;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @SuppressWarnings("unchecked")
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("Executing BodyCopyHeadlineViewHelperImpl. Inside processData method ");
  Map<String, Object> properties = new HashMap<String, Object>();
  ResourceResolver resourceResolver = (ResourceResolver) resources.get(ProductConstants.RESOURCE_RESOLVER);
  SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) resources.get(ProductConstants.REQUEST);
  Map<String, Object> productCopyProperties = (Map<String, Object>) content.get(PROPERTIES);
  Resource currentResource = slingRequest.getResource();
  if (currentResource != null) {
   LOGGER.info("Component found for current page {}", currentResource.getName());
   String showSocialMedia = checkNullValues(productCopyProperties.get(SHOW_SOCIAL_MEDIA));
   if (TRUE.equals(showSocialMedia)) {
    showSocialMedia = FALSE;
   } else {
    showSocialMedia = TRUE;
   }
   String title = checkNullValues(productCopyProperties.get(HEADLINE));
   String copyText = checkNullValues(productCopyProperties.get(TEXT));
   PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
   Page currentPage = pageManager.getContainingPage(currentResource);
   properties.put(UnileverConstants.PUBLISH_DATE, ComponentUtil.getPublishedDate(currentPage));
   properties.put(HEADLINE, title);
   properties.put(SHORT_ARTICLE, copyText);
   properties.put(SHOW_SOCIAL_MEDIA, showSocialMedia);
   properties.put(SOCIAL_SHARING, SocialShareHelper.addThisWidget(resources, configurationService));
   
  } else {
   LOGGER.info("Component cannot be found for current page");
  }
  
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE), properties);
  return data;
  
 }
 
 /**
  * Check null values.
  * 
  * @param key
  *         the key
  * @return the string
  */
 private String checkNullValues(Object key) {
  String value = "";
  if (null == key) {
   value = "";
  } else {
   value = key.toString();
  }
  
  return value;
  
 }
 
}
