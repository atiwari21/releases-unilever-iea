/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.Image;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * The Class DiagnosticToolQuestionViewHelperImpl.
 */
@Component(description = "DiagnosticToolQuestionViewHelperImpl", immediate = true, metatype = true, label = "DiagnosticToolQuestionViewHelperImpl")
@Service(value = { DiagnosticToolQuestionViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.DIAGNOSTIC_TOOL_QUESTION, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING,
  intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class DiagnosticToolQuestionViewHelperImpl extends BaseViewHelper {
 
 private static final String SURVEY_QUESTION_ID = "surveyQuestionId";

 private static final String SURVEY_ANSWER_ID = "surveyAnswerId";

 private static final String BACKGROUND_IMAGE_ALT_TEXT = "backgroundImageAltText";
 
 /** The Constant DIAGNOSTIC_TOOL_NEXT_QUESTION_CTA_LABEL. */
 private static final String DIAGNOSTIC_TOOL_NEXT_QUESTION_CTA_LABEL = "diagnosticTool.nextQuestionCtaLabel";
 
 /** The Constant GENERAL_CONFIG_DATA. */
 private static final String GENERAL_CONFIG_DATA = "generalConfigData";
 
 /** The Constant QUESTION_IDENTIFIER_NAME. */
 private static final String QUESTION_IDENTIFIER_NAME = "questionIdentifierName";
 
 /** The Constant RESPONSE_ID. */
 private static final String RESPONSE_ID = "responseId";
 
 /** The Constant QUESTION_ID. */
 private static final String QUESTION_ID = "questionId";
 
 /** The Constant SECTION_DIVIDER. */
 private static final String SECTION_DIVIDER = "sectionDivider";
 
 /** The Constant RESPONSES. */
 private static final String RESPONSES = "responses";
 
 /** The Constant RESPONSE_OPTION_JSON. */
 private static final String RESPONSE_OPTION_JSON = "responseOptionJson";
 
 /** The Constant TAG_NOT_REQUIRED. */
 private static final String TAG_NOT_REQUIRED = "tagNotRequired";
 
 /** The Constant CSS_STYLE. */
 private static final String CSS_STYLE = "cssStyle";
 
 /** The Constant MATCHING_TAG. */
 private static final String MATCHING_TAG = "matchingTag";
 
 /** The Constant FOREGROUND_IMAGE_ALT_TEXT_RESPONSE. */
 private static final String FOREGROUND_IMAGE_ALT_TEXT_RESPONSE = "foregroundImageAltTextResponse";
 
 /** The Constant FOREGROUND_IMAGE_RESPONSE. */
 private static final String FOREGROUND_IMAGE_RESPONSE = "foregroundImageResponse";
 
 /** The Constant BACKGROUND_IMAGE_RESPONSE. */
 private static final String BACKGROUND_IMAGE_RESPONSE = "backgroundImageResponse";
 
 /** The Constant BACKGROUND_IMAGE_ALT TEXT RESPONSE. */
 private static final String BACKGROUND_IMAGE_RESPONSE_ALT_TEXT = "backgroundImageResponseAltText";
 
 /** The Constant SELECTION_FEEDBACK_TEXT. */
 private static final String SELECTION_FEEDBACK_TEXT = "selectionFeedbackText";
 
 /** The Constant ADDITIONAL_DESCRIPTION_TEXT. */
 private static final String ADDITIONAL_DESCRIPTION_TEXT = "additionalDescriptionText";
 
 /** The Constant RESPONSE_SUBHEADING_TEXT. */
 private static final String RESPONSE_SUBHEADING_TEXT = "responseSubheadingText";
 
 /** The Constant RESPONSE_TEXT. */
 private static final String RESPONSE_TEXT = "responseText";
 
 /** The Constant RESPONSE_IMAGE_NOT_REQUIRED. */
 private static final String RESPONSE_IMAGE_NOT_REQUIRED = "responseImageNotRequired";
 
 /** The Constant DELAY_INTERVAL. */
 private static final String DELAY_INTERVAL = "delayInterval";
 
 /** The Constant SWITCH_TO_NEXT_QUESTION. */
 private static final String SWITCH_TO_NEXT_QUESTION = "switchToNextQuestion";
 
 /** The Constant NEXT_QUESTION_CTA_LABEL. */
 private static final String NEXT_QUESTION_CTA_LABEL = "nextQuestionCtaLabel";
 
 /** The Constant FOREGROUND_IMAGE_ALT_TEXT. */
 private static final String FOREGROUND_IMAGE_ALT_TEXT = "foregroundImageAltText";
 
 /** The Constant FOREGROUND_IMAGE. */
 private static final String FOREGROUND_IMAGE = "foregroundImage";
 
 /** The Constant BACKGROUND_IMAGE. */
 private static final String BACKGROUND_IMAGE = "backgroundImage";
 
 /** The Constant LONG_SUBHEADING_TEXT. */
 private static final String LONG_SUBHEADING_TEXT = "longSubheadingText";
 
 /** The Constant SHORT_SUBHEADING_TEXT. */
 private static final String SHORT_SUBHEADING_TEXT = "shortSubheadingText";
 
 /** The Constant HEADING_TEXT. */
 private static final String HEADING_TEXT = "headingText";
 
 /** The Constant QUESTION_TYPE. */
 private static final String QUESTION_TYPE = "questionType";
 /**
  * logger object.
  **/
 private static final Logger LOGGER = LoggerFactory.getLogger(DiagnosticToolQuestionViewHelperImpl.class);
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.info("calling diagnostic tool question view helper");
  Map<String, Object> diagnosticToolQuestionProperties = getProperties(content);
  String jsonNameSpace = getJsonNameSpace(content);
  Resource currentResource = getCurrentResource(resources);
  Page currentPage = getCurrentPage(resources);
  SlingHttpServletRequest slingHttpServletRequest = getSlingRequest(resources);
  String questionType = MapUtils.getString(diagnosticToolQuestionProperties, QUESTION_TYPE, StringUtils.EMPTY);
  Map<String, Object> diagnosticToolQuestionMap = new LinkedHashMap<String, Object>();
  if (StringUtils.isNotBlank(questionType)) {
   diagnosticToolQuestionMap = getQuestionMap(diagnosticToolQuestionProperties, currentResource, currentPage, slingHttpServletRequest, questionType);
  }
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  data.put(jsonNameSpace, diagnosticToolQuestionMap);
  return data;
 }
 
 /**
  * Gets the question map.
  * 
  * @param diagnosticToolQuestionProperties
  *         the diagnostic tool question properties
  * @param currentResource
  *         the current resource
  * @param currentPage
  *         the current page
  * @param slingHttpServletRequest
  *         the sling http servlet request
  * @param questionType
  *         the question type
  * @return the question map
  */
 public Map<String, Object> getQuestionMap(Map<String, Object> diagnosticToolQuestionProperties, Resource currentResource, Page currentPage,
   SlingHttpServletRequest slingHttpServletRequest, String questionType) {
  Map<String, Object> diagnosticToolQuestionMap = new LinkedHashMap<String, Object>();
  diagnosticToolQuestionMap.put(GENERAL_CONFIG_DATA, getCommonProperties(diagnosticToolQuestionProperties, currentPage, slingHttpServletRequest));
  diagnosticToolQuestionMap.put(QUESTION_TYPE, questionType);
  String displayType = MapUtils.getString(diagnosticToolQuestionProperties, UnileverConstants.VIEW_TYPE, StringUtils.EMPTY);
  diagnosticToolQuestionMap.put(UnileverConstants.DISPLAY_TYPE, displayType);
  if (!StringUtils.equals(SECTION_DIVIDER, questionType)) {
   List<Map<String, String>> responesListMap = ComponentUtil.getNestedMultiFieldProperties(currentResource, RESPONSE_OPTION_JSON);
   boolean imageNotRequired = MapUtils.getBooleanValue(diagnosticToolQuestionProperties, RESPONSE_IMAGE_NOT_REQUIRED, false);
   boolean tagNotRequired = MapUtils.getBooleanValue(diagnosticToolQuestionProperties, TAG_NOT_REQUIRED, false);
   diagnosticToolQuestionMap.put(RESPONSE_IMAGE_NOT_REQUIRED, imageNotRequired);
   diagnosticToolQuestionMap.put(TAG_NOT_REQUIRED, tagNotRequired);
   List<Map<String, Object>> sectionsListFinal = prepareSectionList(responesListMap, currentPage, imageNotRequired, tagNotRequired,
     slingHttpServletRequest, currentResource);
   LOGGER.debug("getting list of response lis and size of the list is " + sectionsListFinal.size());
   diagnosticToolQuestionMap.put(RESPONSES, sectionsListFinal.toArray());
  }
  return diagnosticToolQuestionMap;
 }
 
 /**
  * gets the common properties of component.
  * 
  * @param diagnosticToolQuestionProperties
  *         the diagnostic tool question properties
  * @param currentPage
  *         the current page
  * @param slingHttpServletRequest
  *         the sling http servlet request
  * @return map
  */
 public Map<String, Object> getCommonProperties(Map<String, Object> diagnosticToolQuestionProperties, Page currentPage,
   SlingHttpServletRequest slingHttpServletRequest) {
  Map<String, Object> diagnosticToolQuestionMap = new LinkedHashMap<String, Object>();
  String headingText = MapUtils.getString(diagnosticToolQuestionProperties, HEADING_TEXT, StringUtils.EMPTY);
  String shortSubheadingText = MapUtils.getString(diagnosticToolQuestionProperties, SHORT_SUBHEADING_TEXT, StringUtils.EMPTY);
  String longSubheadingText = MapUtils.getString(diagnosticToolQuestionProperties, LONG_SUBHEADING_TEXT, StringUtils.EMPTY);
  String backgroundImage = MapUtils.getString(diagnosticToolQuestionProperties, BACKGROUND_IMAGE, StringUtils.EMPTY);
  String backgroundImageAltText = MapUtils.getString(diagnosticToolQuestionProperties, BACKGROUND_IMAGE_ALT_TEXT, StringUtils.EMPTY);
  Map<String, Object> backgroundImageMap = getImageProperties(currentPage, slingHttpServletRequest, backgroundImage, backgroundImageAltText);
  String foregroundImage = MapUtils.getString(diagnosticToolQuestionProperties, FOREGROUND_IMAGE, StringUtils.EMPTY);
  String foregroundImageAlt = MapUtils.getString(diagnosticToolQuestionProperties, FOREGROUND_IMAGE_ALT_TEXT, StringUtils.EMPTY);
  Map<String, Object> foregroundImageMap = getImageProperties(currentPage, slingHttpServletRequest, foregroundImage, foregroundImageAlt);
  String nextQuestionCtaLabel = MapUtils.getString(diagnosticToolQuestionProperties, NEXT_QUESTION_CTA_LABEL, StringUtils.EMPTY);
  I18n i18n = BaseViewHelper.getI18n(slingHttpServletRequest, currentPage);
  nextQuestionCtaLabel = (StringUtils.isNotBlank(nextQuestionCtaLabel)) ? nextQuestionCtaLabel : i18n.get(DIAGNOSTIC_TOOL_NEXT_QUESTION_CTA_LABEL);
  String switchToNextQuestion = MapUtils.getString(diagnosticToolQuestionProperties, SWITCH_TO_NEXT_QUESTION, StringUtils.EMPTY);
  int delayInterval = Integer.parseInt(MapUtils.getString(diagnosticToolQuestionProperties, DELAY_INTERVAL, "0"));
  String questionIdentifierName = MapUtils.getString(diagnosticToolQuestionProperties, JcrConstants.JCR_TITLE, StringUtils.EMPTY);
  String questionId = MapUtils.getString(diagnosticToolQuestionProperties, QUESTION_ID);
  diagnosticToolQuestionMap.put(HEADING_TEXT, headingText);
  diagnosticToolQuestionMap.put(SHORT_SUBHEADING_TEXT, shortSubheadingText);
  diagnosticToolQuestionMap.put(LONG_SUBHEADING_TEXT, longSubheadingText);
  diagnosticToolQuestionMap.put(BACKGROUND_IMAGE, backgroundImageMap);
  diagnosticToolQuestionMap.put(FOREGROUND_IMAGE, foregroundImageMap);
  diagnosticToolQuestionMap.put(NEXT_QUESTION_CTA_LABEL, nextQuestionCtaLabel);
  diagnosticToolQuestionMap.put(SWITCH_TO_NEXT_QUESTION, switchToNextQuestion);
  diagnosticToolQuestionMap.put(DELAY_INTERVAL, delayInterval);
  diagnosticToolQuestionMap.put(QUESTION_IDENTIFIER_NAME, questionIdentifierName);
  diagnosticToolQuestionMap.put(QUESTION_ID, questionId);
  diagnosticToolQuestionMap.put(SURVEY_QUESTION_ID, MapUtils.getString(diagnosticToolQuestionProperties, SURVEY_QUESTION_ID, StringUtils.EMPTY));
  LOGGER.debug("getting common properties in a map and size of map is " + diagnosticToolQuestionMap.size());
  return diagnosticToolQuestionMap;
 }
 
 /**
  * This method is used to get image properties like path,alt text for image extension etc.
  * 
  * @param currentPage
  *         the current page
  * @param slingHttpServletRequest
  *         the sling http servlet request
  * @param fileReference
  *         the file reference
  * @param altImage
  *         the alt image
  * @return the image properties
  */
 
 public Map<String, Object> getImageProperties(Page currentPage, SlingHttpServletRequest slingHttpServletRequest, String fileReference,
   String altImage) {
  Map<String, Object> imageMap = new LinkedHashMap<String, Object>();
  Image image = new Image(fileReference, altImage, altImage, currentPage, slingHttpServletRequest);
  imageMap = image.convertToMap();
  return imageMap;
  
 }
 
 /**
  * Prepare section list.
  * 
  * @param sectionsList
  *         the sections list
  * @param currentPage
  *         the current page
  * @param imageNotRequired
  *         the image not required
  * @param tagNotRequired
  *         the tag not required
  * @param slingHttpServletRequest
  *         the sling http servlet request
  * @param currentResource
  *         the current resource
  * @return the list
  */
 public List<Map<String, Object>> prepareSectionList(List<Map<String, String>> sectionsList, Page currentPage, boolean imageNotRequired,
   boolean tagNotRequired, SlingHttpServletRequest slingHttpServletRequest, Resource currentResource) {
  List<Map<String, Object>> finalSectionsList = new ArrayList<Map<String, Object>>();
  Map<String, Object> currentPageProperties = (currentPage != null) ? currentResource.getValueMap() : new LinkedHashMap<String, Object>();
  String[] responseIdArray = ComponentUtil.getPropertyValueArray(currentPageProperties, RESPONSE_ID);
  int responseIdCount = 0;
  for (Map<String, String> sectionMap : sectionsList) {
   Map<String, Object> finalSectionsMap = new LinkedHashMap<String, Object>();
   String responseText = MapUtils.getString(sectionMap, RESPONSE_TEXT, StringUtils.EMPTY);
   String responseSubheadingText = MapUtils.getString(sectionMap, RESPONSE_SUBHEADING_TEXT, StringUtils.EMPTY);
   String additionalDescriptionText = MapUtils.getString(sectionMap, ADDITIONAL_DESCRIPTION_TEXT, StringUtils.EMPTY);
   String selectionFeedbackText = MapUtils.getString(sectionMap, SELECTION_FEEDBACK_TEXT, StringUtils.EMPTY);
   String backgroundImageResponseAltText = MapUtils.getString(sectionMap, BACKGROUND_IMAGE_RESPONSE_ALT_TEXT, StringUtils.EMPTY);
   String backgroundImageResponse = MapUtils.getString(sectionMap, BACKGROUND_IMAGE_RESPONSE, StringUtils.EMPTY);
   Map<String, Object> backgroundImageMap = getImageProperties(currentPage, slingHttpServletRequest, backgroundImageResponse,
     backgroundImageResponseAltText);
   String foregroundImageResponse = MapUtils.getString(sectionMap, FOREGROUND_IMAGE_RESPONSE, StringUtils.EMPTY);
   String foregroundImageAltTextResponse = MapUtils.getString(sectionMap, FOREGROUND_IMAGE_ALT_TEXT_RESPONSE, StringUtils.EMPTY);
   Map<String, Object> foregroundImageMap = getImageProperties(currentPage, slingHttpServletRequest, foregroundImageResponse,
     foregroundImageAltTextResponse);
   String matchingTag = MapUtils.getString(sectionMap, MATCHING_TAG, StringUtils.EMPTY);
   String[] multipleMatchingTags = matchingTag.split(UnileverConstants.COMMA);
   List<String> matchingTagList = new LinkedList<String>();
   for (String matchingTagItem : multipleMatchingTags) {
    matchingTagList.add(matchingTagItem);
   }
   String cssStyle = MapUtils.getString(sectionMap, CSS_STYLE, StringUtils.EMPTY);
   String responseId = MapUtils.getString(sectionMap, RESPONSE_ID, StringUtils.EMPTY);
   if (sectionMap.get(RESPONSE_ID) == null && responseIdArray[responseIdCount] != null) {
    responseId = responseIdArray[responseIdCount];
   }
   finalSectionsMap.put(RESPONSE_TEXT, responseText);
   finalSectionsMap.put(RESPONSE_SUBHEADING_TEXT, responseSubheadingText);
   finalSectionsMap.put(ADDITIONAL_DESCRIPTION_TEXT, additionalDescriptionText);
   finalSectionsMap.put(SELECTION_FEEDBACK_TEXT, selectionFeedbackText);
   finalSectionsMap.put(BACKGROUND_IMAGE, backgroundImageMap);
   setResponseAttributes(finalSectionsMap, FOREGROUND_IMAGE, foregroundImageMap, imageNotRequired, matchingTagList, true);
   setResponseAttributes(finalSectionsMap, MATCHING_TAG, foregroundImageMap, tagNotRequired, matchingTagList, false);
   finalSectionsMap.put(CSS_STYLE, cssStyle);
   finalSectionsMap.put(RESPONSE_ID, responseId);
   finalSectionsMap.put(SURVEY_ANSWER_ID, MapUtils.getString(sectionMap, SURVEY_ANSWER_ID, StringUtils.EMPTY));
   finalSectionsList.add(finalSectionsMap);
   responseIdCount++;
  }
  LOGGER.debug("created question tool responses and size of the responses is " + finalSectionsList.size());
  return finalSectionsList;
  
 }
 
 /**
  * Sets the response attributes.
  * 
  * @param finalSectionsMap
  *         the final sections map
  * @param key
  *         the key
  * @param imageMap
  *         the image map
  * @param responeFlag
  *         the respone flag
  * @param matchingTagList
  *         the matching tag list
  * @param isImage
  *         the is image
  */
 public void setResponseAttributes(Map<String, Object> finalSectionsMap, String key, Map<String, Object> imageMap, boolean responeFlag,
   List<String> matchingTagList, boolean isImage) {
  if (!responeFlag && isImage) {
   finalSectionsMap.put(key, imageMap);
  } else if (!responeFlag) {
   finalSectionsMap.put(key, matchingTagList);
  }
 }
}
