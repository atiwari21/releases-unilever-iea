/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.commons.collections.MapUtils;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.osgi.framework.Constants;

import com.sapient.platform.iea.aem.core.utils.CollectionUtils;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;

import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * The Class FormElementsGroupViewHelperImpl.
 */
@Component(description = "FormElementsGroupViewHelperImpl", immediate = true, metatype = true, label = "FormElementsGroupViewHelperImpl")
@Service(value = { FormElementsGroupViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.FORM_ELEMENTS_GROUP, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class FormElementsGroupViewHelperImpl extends BaseViewHelper {
 /**
  * Constant for retrieving form elements group id.
  */
 private static final String GROUP_ID = "groupId";
 /**
  * Constant for retrieving form elements group title.
  */
 private static final String GROUP_HEADING_TEXT = "groupHeadingText";
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @SuppressWarnings("unchecked")
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  Map<String, Object> propertiesValueMap = (Map<String, Object>) content.get(PROPERTIES);
  Map<String, Object> properties = CollectionUtils.getJcrPropertyAsMap(propertiesValueMap);
  Map<String, Object> finalMap = new LinkedHashMap<String, Object>();
  Map<String, Object> textValues = new LinkedHashMap<String, Object>();
  String jsonNameSpace = MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE);
  String id = MapUtils.getString(properties, GROUP_ID, null);
  String title = MapUtils.getString(properties, GROUP_HEADING_TEXT, null);
  textValues.put("id", id);
  textValues.put("text", title);
  finalMap.put(jsonNameSpace, textValues);
  return finalMap;
 }
 
}
