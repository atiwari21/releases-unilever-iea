/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.cache.ComponentViewAndViewHelperCache;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.servlets.GeneratePDF;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;
import com.unilever.platform.foundation.viewhelper.constants.PurchaseConfirmationConstant;

/**
 * <p>
 * Responsible for reading author input, I18N and configuration for purchase confirmation component and generate a map. Also read the site
 * configuration for return to gift page url configured for the site.
 * </p>
 */
@Component(description = "PurchaseConfirmationViewHelperImpl", immediate = true, metatype = true, label = "PurchaseConfirmationViewHelperImpl")
@Service(value = { PurchaseConfirmationViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.PURCHASE_CONFIRMATION, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class PurchaseConfirmationViewHelperImpl extends BaseViewHelper {
 
 private static final String USER_EMAIL_ID = "userEmailId";
 
 private static final String ORDER_ID = "orderId";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(PurchaseConfirmationViewHelperImpl.class);
 
 /** The component view and view helper cache. */
 /*
  * reference for cache object
  */
 @Reference
 ComponentViewAndViewHelperCache componentViewAndViewHelperCache;
 /*
  * reference for site configuration service
  */
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /**
  * The method processData overrides method is responsible validating author input and returning a map and maximum number for the items in shopping
  * basket for a site from site configuration .
  * 
  * @param content
  *         {@link Map} of all user input received as input
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return Map holding the result data.
  */
 @SuppressWarnings("unchecked")
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  LOGGER.debug("inside PurchaseConfirmationViewHelperImpl processData");
  Map<String, Object> data = new HashMap<String, Object>();
  Map<String, Object> dataMap = new HashMap<String, Object>();
  try {
   ResourceResolver resourceResolver = (ResourceResolver) resources.get(ProductConstants.RESOURCE_RESOLVER);
   SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) resources.get(ProductConstants.REQUEST);
   Resource compResource = slingRequest.getResource();
   PageManager pm = resourceResolver.adaptTo(PageManager.class);
   Page currentPage = pm.getContainingPage(compResource);
   Map<String, Object> properties = (Map<String, Object>) content.get(PROPERTIES);
   dataMap = new HashMap<String, Object>();
   dataMap.put("static", getStaticValuesMap(properties));
   dataMap.put("configs", getConfigs(currentPage));
   dataMap.put("orderDetail", getOrderDetailsMap(slingRequest));
   dataMap.put("returnButtonCTA", getReturnButtonMap(properties, resourceResolver, getSlingRequest(resources)));
   dataMap.put("downloadButtonCTA", getDownloadButtonMap(properties, resourceResolver));
   
   String jsonNameSpace = MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE);
   data.put(jsonNameSpace, dataMap);
  } catch (Exception e) {
   LOGGER.error("Error At PurchaseConfirmationViewHelperImpl " + ExceptionUtils.getStackTrace(e));
  }
  return data;
 }
 
 /**
  * Gets the static values map.
  * 
  * @param properties
  *         the properties
  * @return the static values map
  */
 private Map<String, String> getStaticValuesMap(Map<String, Object> properties) {
  Map<String, String> staticMap = new HashMap<String, String>();
  staticMap.put(PurchaseConfirmationConstant.THANK_YOU_LABEL, MapUtils.getString(properties, PurchaseConfirmationConstant.THANK_YOU_LABEL_FIELD, ""));
  staticMap.put(PurchaseConfirmationConstant.DISCLAIMER_STATEMENT,
    MapUtils.getString(properties, PurchaseConfirmationConstant.DISCLAIMER_STATEMENT_FIELD, ""));
  staticMap.put(PurchaseConfirmationConstant.ORDER_NO_STATEMENT,
    MapUtils.getString(properties, PurchaseConfirmationConstant.ORDER_NO_STATEMENT_FIELD, ""));
  staticMap.put(PurchaseConfirmationConstant.EMAIL_STATEMENT, MapUtils.getString(properties, PurchaseConfirmationConstant.EMAIL_STATEMENT_FIELD, ""));
  staticMap.put(PurchaseConfirmationConstant.DELIVERY_STATEMENT,
    MapUtils.getString(properties, PurchaseConfirmationConstant.DELIVERY_STATEMENT_FIELD, ""));
  
  staticMap.put(PurchaseConfirmationConstant.HELP_STATEMENT, MapUtils.getString(properties, PurchaseConfirmationConstant.HELP_STATEMENT_FIELD, ""));
  
  staticMap.put(PurchaseConfirmationConstant.TELEPHONE_STATEMENT,
    MapUtils.getString(properties, PurchaseConfirmationConstant.TELEPHONE_STATEMENT_FIELD, ""));
  staticMap.put(PurchaseConfirmationConstant.EMAIL_LABEL, MapUtils.getString(properties, PurchaseConfirmationConstant.EMAIL_LABEL_FIELD, ""));
  staticMap.put(PurchaseConfirmationConstant.TELEPHONE_NUMBER_FINE_PRINT,
    MapUtils.getString(properties, PurchaseConfirmationConstant.TELEPHONE_NUMBER_FINE_PRINT_FIELD, ""));
  return staticMap;
 }
 
 /**
  * Gets the configs.
  * 
  * @param currentPage
  *         the current page
  * @return the configs
  */
 private Map<String, String> getConfigs(Page currentPage) {
  String customerServicePhone = StringUtils.EMPTY;
  String customerServiceEmail = StringUtils.EMPTY;
  String deliveryTime = StringUtils.EMPTY;
  Map<String, String> configMap = new HashMap<String, String>();
  try {
   customerServicePhone = configurationService.getConfigValue(currentPage, UnileverConstants.EGIFTING_CONFIG_CAT, "customerServicePhone");
   customerServiceEmail = configurationService.getConfigValue(currentPage, UnileverConstants.EGIFTING_CONFIG_CAT, "customerServiceEmail");
   deliveryTime = configurationService.getConfigValue(currentPage, UnileverConstants.EGIFTING_CONFIG_CAT, "deliveryTime");
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Error in getting the configuration for  egifting ", e);
  }
  configMap.put(PurchaseConfirmationConstant.CUSTOMER_SERVICE_PHONE, customerServicePhone);
  configMap.put(PurchaseConfirmationConstant.CUSTOMER_SERVICE_EMAIL, customerServiceEmail);
  configMap.put(PurchaseConfirmationConstant.DELIVERY_TIME, deliveryTime);
  return configMap;
 }
 
 private Map<String, String> getOrderDetailsMap(SlingHttpServletRequest slingRequest) {
  String orderId = slingRequest.getParameter(ORDER_ID) != null ? slingRequest.getParameter(ORDER_ID) : "";
  String userEmailId = slingRequest.getParameter(USER_EMAIL_ID) != null ? slingRequest.getParameter(USER_EMAIL_ID) : "";
  Map<String, String> orderDetailsMap = new HashMap<String, String>();
  orderDetailsMap.put(ORDER_ID, orderId);
  orderDetailsMap.put(USER_EMAIL_ID, userEmailId);
  return orderDetailsMap;
 }
 
 /**
  * Gets the return button map.
  * 
  * @param properties
  *         the properties
  * @param resourceResolver
  *         the resource resolver
  * @return the return button map
  */
 private Map<String, String> getReturnButtonMap(Map<String, Object> properties, ResourceResolver resourceResolver,
   SlingHttpServletRequest slingRequest) {
  Map<String, String> returnButtonMap = new HashMap<String, String>();
  returnButtonMap.put(UnileverConstants.LABEL, MapUtils.getString(properties, PurchaseConfirmationConstant.RETURN_BUTTON_LABEL_FIELD, ""));
  
  String returnButtonPath = MapUtils.getString(properties, PurchaseConfirmationConstant.GIFTING_PAGE_PATH, null);
  
  String returnButtonURL = returnButtonPath != null ? ComponentUtil.getFullURL(resourceResolver, returnButtonPath, slingRequest) : "";
  returnButtonMap.put(UnileverConstants.URL, returnButtonURL);
  return returnButtonMap;
 }
 
 /**
  * Gets the download button map.
  * 
  * @param properties
  *         the properties
  * @return the download button map
  */
 private Map<String, String> getDownloadButtonMap(Map<String, Object> properties, ResourceResolver resourceResolver) {
  Map<String, String> downloadButtonMap = new HashMap<String, String>();
  downloadButtonMap.put(UnileverConstants.LABEL, MapUtils.getString(properties, PurchaseConfirmationConstant.DOWNLOAD_BUTTON_CTA_LABEL_FIELD, ""));
  String pdfUrl = resourceResolver.map(GeneratePDF.PATH);
  
  downloadButtonMap.put(UnileverConstants.URL, pdfUrl);
  return downloadButtonMap;
 }
}
