/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.Map;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.foundation.components.UnileverComponents;

/**
 * The class PageDividerViewHelperImplV2
 */
@Component(description = "PageDividerViewHelperImplV2", immediate = true, metatype = true, label = "Page Divider V2 ViewHelper")
@Service(value = { PageDividerViewHelperImplV2.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.PAGE_DIVIDER_V2, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class PageDividerViewHelperImplV2 extends com.unilever.platform.foundation.viewhelper.base.BaseViewHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(PageDividerViewHelperImplV2.class);
 
 /** The Constant ANCHOR_TITLE. */
 private static final String ANCHOR_TITLE = "anchorLinkTitle";
 /** The Constant ANCHOR_LINK_ID. */
 private static final String ANCHOR_LINK_ID = "anchorLinkId";
 /** The Constant GENERAL_CONFIG. */
 private static final String GENERAL_CONFIG = "generalConfig";
 /** The Constant RANDOM_NUMBER. */
 private static final String RANDOM_NUMBER = "randomNumber";
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper# processData (java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  
  LOGGER.info("The Page Divider Component View Helper is executing");
  String jsonNameSapce = MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE);
  Map<String, Object> pageDividerMap = new LinkedHashMap<String, Object>();
  Map<String, Object> properties = getProperties(content);
  Map<String, Object> generalConfigMap = new LinkedHashMap<String, Object>();
  String anchorLinkNavigationTitle = MapUtils.getString(properties, ANCHOR_TITLE, StringUtils.EMPTY);
  String anchorLinkId = anchorLinkNavigationTitle.trim().toLowerCase();
  anchorLinkId = "#" + anchorLinkId.replaceAll("\\s+", "-") + "-" + MapUtils.getString(properties, RANDOM_NUMBER, StringUtils.EMPTY);
  generalConfigMap.put(ANCHOR_TITLE, MapUtils.getString(properties, ANCHOR_TITLE, StringUtils.EMPTY).trim());
  generalConfigMap.put(ANCHOR_LINK_ID, anchorLinkId);
  pageDividerMap.put(GENERAL_CONFIG, generalConfigMap);
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(jsonNameSapce, pageDividerMap);
  return data;
 }
}
