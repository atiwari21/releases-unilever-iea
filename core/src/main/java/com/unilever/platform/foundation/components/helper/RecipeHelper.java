/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.components.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.RecipeConfigDTO;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * The Class RecipeHelper.
 */
public class RecipeHelper {
 
 private static final String RECIPE_DIETARY_ATTRIBUTES = "recipeDietaryAttributes";
 
 private static final String RECIPE_ATTRIBUTES = "recipeAttributes";
 
 private static final String COMPONENT_NAME = "componentName";
 
 private static final String DIETARY_ATTRIBUTES = "dietaryAttributes";
 
 /** The Constant COMMA_SEPARATED. */
 private static final String COMMA_SEPARATED = "\\s*,\\s*";
 
 /** The Constant SUFFIX. */
 private static final String SUFFIX = "suffix";
 
 /** The Constant LABEL. */
 private static final String LABEL = "label";
 
 /** The Constant ATTRIBUTE_NAME. */
 private static final String ATTRIBUTE_NAME = "attributeName";
 private static final String RATING_REVIEWS = "ratingReviews";
 private static final String ATTRIBUTES = "attributes";
 private static final String FILTER_DISPLAY_OPTION = "filterDisplayOption";
 private static final String RECIPE_LISTING = "recipeListing";
 /** The Constant TWO. */
 private static final int TWO = 2;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(RecipeHelper.class);

 /**
  * Gets the dietary attributes.
  * 
  * @param i18n
  *         the i18n
  * @param properties
  *         the recipe properties
  * @param globalConfigMap
  *         the global config map
  * @param prefix
  *         the prefix
  * @param postfix
  *         the postfix
  * @return the dietary attributes
  */
 
 private RecipeHelper() {
  
 }
 
 /**
  * Gets the attributes details.
  * 
  * @param i18n
  *         the i 18 n
  * @param properties
  *         the properties
  * @param globalConfigMap
  *         the global config map
  * @param prefix
  *         the prefix
  * @param postfix
  *         the postfix
  * @return the attributes details
  */
 public static List<Map<String, Object>> getAttributesDetails(I18n i18n, Map<String, Object> properties, Map<String, String> globalConfigMap,
   String prefix, String postfix) {
  
  LOGGER.info("Inside get dietary attribute method");
  
  List<Map<String, Object>> attributeList = new LinkedList<Map<String, Object>>();
  
  boolean overrideGlobalConfig = MapUtils.getBooleanValue(properties, UnileverConstants.OVERRIDE_GLOBAL_CONFIG, false);
  
  for (String key : globalConfigMap.keySet()) {
   String value = StringUtils.EMPTY;
   if (overrideGlobalConfig) {
    if (properties.containsKey(DIETARY_ATTRIBUTES) || properties.containsKey(ATTRIBUTES)) {
     
     value = getAttributeMapFromDialog(properties, key, value, false, true);
    }
   } else {
    value = MapUtils.getString(globalConfigMap, key);
   }
   
   if (StringUtils.isNotBlank(value)) {
    String[] attributeArray = value.split(COMMA_SEPARATED);
    Map<String, Object> attributeMap = getAttributeMap(prefix, postfix, i18n, key, attributeArray);
    if(MapUtils.isNotEmpty(attributeMap)){
    attributeList.add(attributeMap);
    }
   }
  }
  
  return attributeList;
 }
 
 /**
  * Gets the attribute map from dialog.
  * 
  * @param properties
  *         the properties
  * @param key
  *         the key
  * @param value
  *         the value
  * @param propertyFlag
  *         the propertyFlag
  * @param booleanSecondFlag
  *         the booleanSecondFlag
  * @return the attribute map from dialog
  */
 public static String getAttributeMapFromDialog(Map<String, Object> properties, String key, String value, boolean propertyFlag,
   boolean booleanSecondFlag) {
  
  Object dietaryAttributeArray = null;
  
  if (MapUtils.getString(properties, COMPONENT_NAME).equals(RECIPE_DIETARY_ATTRIBUTES)) {
   dietaryAttributeArray = MapUtils.getObject(properties, DIETARY_ATTRIBUTES);
  } else if (MapUtils.getString(properties, COMPONENT_NAME).equals(RECIPE_ATTRIBUTES)) {
   dietaryAttributeArray = MapUtils.getObject(properties, ATTRIBUTES);
  } else if (RECIPE_LISTING.equals(MapUtils.getString(properties, COMPONENT_NAME)) && propertyFlag && booleanSecondFlag) {
   dietaryAttributeArray = MapUtils.getObject(properties, "sortingOptions");
  } else if (RECIPE_LISTING.equals(MapUtils.getString(properties, COMPONENT_NAME)) && !propertyFlag && booleanSecondFlag) {
   dietaryAttributeArray = MapUtils.getObject(properties, "dietaryAttributes");
  } else if (RECIPE_LISTING.equals(MapUtils.getString(properties, COMPONENT_NAME)) && !propertyFlag && !booleanSecondFlag) {
   dietaryAttributeArray = MapUtils.getObject(properties, ATTRIBUTES);
  }
  
  String[] dietaryAttributeArrays = null;
  
  if (dietaryAttributeArray instanceof String) {
   dietaryAttributeArrays = new String[] { (String) dietaryAttributeArray };
   
  } else if (dietaryAttributeArray instanceof String[]) {
   dietaryAttributeArrays = (String[]) dietaryAttributeArray;
  }
  
  if (dietaryAttributeArrays != null && dietaryAttributeArrays.length > 0) {
   for (String dietaryAttribute : dietaryAttributeArrays) {
    String[] testValue = dietaryAttribute.split(COMMA_SEPARATED);
    if (testValue.length > 0 && key.equals(testValue[0])) {
     value = dietaryAttribute;
     break;
    }
    if (value.isEmpty()) {
     continue;
    } else {
     break;
    }
   }
  }
  return value;
 }
 
 /**
  * Gets the attribute list.
  * 
  * @param prefix
  *         the prefix
  * @param postfix
  *         the postfix
  * @param i18n
  *         the i18n
  * @param key
  *         the entry
  * @param attributeArray
  *         the attribute array
  * @return the attribute list
  */
 public static Map<String, Object> getAttributeMap(String prefix, String postfix, I18n i18n, String key, String[] attributeArray) {
  
  Map<String, Object> attributeMap = new LinkedHashMap<String, Object>();
  boolean flag = (attributeArray.length > 1) ? Boolean.valueOf(attributeArray[1]) : true;
  
   attributeMap.put(ATTRIBUTE_NAME, key);
   attributeMap.put(LABEL, i18n.get(prefix + attributeArray[0].substring(0, 1).toUpperCase() + attributeArray[0].substring(1) + postfix));
   attributeMap.put(SUFFIX, getSuffixValue(i18n, attributeArray));
  return attributeMap;
 }
 
 /**
  * Gets the suffix map.
  * 
  * @param i18n
  *         the i18n
  * @param attributeArray
  *         the attribute array
  * @return the suffix map
  */
 private static String getSuffixValue(I18n i18n, String[] attributeArray) {
  String value = StringUtils.EMPTY;
  
  if (attributeArray.length > TWO && attributeArray[TWO] != null && "P".equalsIgnoreCase(attributeArray[TWO])) {
   value = getValueByI18Key(i18n, "recipeAttributes.recipeServesSuffixText");
  } else if (attributeArray.length > TWO && attributeArray[TWO] != null && "T".equalsIgnoreCase(attributeArray[TWO])) {
   value = getValueByI18Key(i18n, "recipeAttributes.recipeTimeSuffixText");
  }
  
  return value;
 }
 
 /**
  * Gets the recipe configurations.
  * 
  * @param configurationService
  *         the configuration service
  * @param currentPage
  *         the current page
  * @param componentName
  *         the component name
  * @return the recipe configurations
  */
 public static RecipeConfigDTO getRecipeConfigurations(ConfigurationService configurationService, Page currentPage, String componentName) {
  
  RecipeConfigDTO recipeConfig = new RecipeConfigDTO();
  
  String endPointName = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, componentName,
    UnileverConstants.END_POINT);
  
  String recipeServiceUrl = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, UnileverConstants.RMS_END_POINT,
    endPointName);
  
  String brand = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, UnileverConstants.RMS_END_POINT,
    UnileverConstants.BRAND);
  
  String locale = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, UnileverConstants.RMS_END_POINT,
    UnileverConstants.LOCALE_NAME);
  
  int timeout = GlobalConfigurationUtility.getIntegerValueFromConfiguration(UnileverConstants.RMS_END_POINT, configurationService, currentPage,
    UnileverConstants.TIME_OUT);
  
  boolean isCalcmenu = Boolean.parseBoolean(GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
    "recipeNutrients", "isCalcmenu"));
  
  recipeConfig.setEndPointUrl(recipeServiceUrl);
  recipeConfig.setBrand(brand);
  recipeConfig.setLocale(locale);
  recipeConfig.setTimeout(timeout);
  recipeConfig.setCalmenu(isCalcmenu);
  
  return recipeConfig;
 }
 
 /**
  * Gets the product paths.
  * 
  * @param relatedProductIds
  *         the related product ids
  * @param productPropertiesList
  *         the product properties list
  * @param jsonNameSpace
  *         the json name space
  * @param resources
  *         the resources
  * @return the product paths
  */
 public static List<String> getProductPaths(String[] relatedProductIds, Map<String, Object> resources) {
  
  List<String> productPaths = new LinkedList<String>();
  Page currentPage = BaseViewHelper.getCurrentPage(resources);
  
  for (String productId : relatedProductIds) {
   if (StringUtils.isNotBlank(productId)) {
    UNIProduct uniProduct = ProductHelperExt.findProduct(currentPage, productId);
    if (uniProduct != null) {
     productPaths.add(uniProduct.getPath());
    } else {
     LOGGER.debug("UniProduct obj is null in getting from ProductHelperExt.findProduct");
    }
   }
  }
  return productPaths;
 }
 
 /**
  * Gets the child map.
  * 
  * @param dataMap
  *         the data map
  * @param childMapName
  *         the child map name
  * @return the child map
  */
 @SuppressWarnings("unchecked")
 public static Map<String, Object> getChildMap(Map<String, Object> dataMap, String childMapName) {
  
  if (MapUtils.isNotEmpty(dataMap) && dataMap.containsKey(childMapName)) {
   return (Map<String, Object>) dataMap.get(childMapName);
  }
  return MapUtils.EMPTY_MAP;
 }
 
 /**
  * Gets the value by i18 key.
  * 
  * @param i18n
  *         the i18n
  * @param key
  *         the key
  * @return the value by i18 key
  */
 public static String getValueByI18Key(I18n i18n, String key) {
  return i18n.get(key);
 }
 
 /**
  * Gets the review and rating map.
  * 
  * @param pageUrl
  *         the page url
  * @param categoryPageUrl
  *         the category page url
  * @param recipeId
  *         the recipe id
  * @param resources
  *         the resources
  * @return the review and rating map
  */
 public static Map<String, Object> getReviewAndRatingMap(String pageUrl, String categoryPageUrl, String recipeId,
   Map<String, Object> resources) {
  Map<String, Object> recipesMap = new LinkedHashMap<String, Object>();
  ResourceResolver resolver = BaseViewHelper.getResourceResolver(resources);
  ConfigurationService configurationService = resolver.adaptTo(ConfigurationService.class);
  Page currentPage = BaseViewHelper.getCurrentPage(resources);
  String serviceProvidername = StringUtils.EMPTY;
  String serviceProviderNameTemp = StringUtils.EMPTY;
  Resource resource = currentPage.getContentResource();
  InheritanceValueMap valueMap = new HierarchyNodeInheritanceValueMap(resource);
  if (valueMap != null) {
   serviceProviderNameTemp = (String) valueMap.getInherited(RATING_REVIEWS, StringUtils.EMPTY);
  }
  if (StringUtils.isNotBlank(serviceProviderNameTemp)) {
   serviceProvidername = serviceProviderNameTemp;
  } else {
   serviceProvidername = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, ProductConstants.RATING_AND_REVIEWS,
     ProductConstants.SERVICE_PROVIDER_NAME);
  }
  Boolean isEnabled = Boolean.parseBoolean(GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
    ProductConstants.RATING_AND_REVIEWS, ProductConstants.ENABLED));
  recipesMap.put("entityType", "recipe");
  recipesMap.put("entityUrl", pageUrl);
  recipesMap.put("categoryUrl", categoryPageUrl);
  recipesMap.put("templateName", "reviewCount");
  recipesMap.put("uniqueId", recipeId);
  recipesMap.put("serviceProviderName", serviceProvidername);
  recipesMap.put(ProductConstants.ENABLED, isEnabled);
  
  try {
   Map<String, String> serviceProviderPropertiesMap = configurationService.getCategoryConfiguration(currentPage, serviceProvidername);
   if (serviceProviderPropertiesMap != null) {
    for (String key : serviceProviderPropertiesMap.keySet()) {
     recipesMap.put(key, serviceProviderPropertiesMap.get(key));
    }
   }
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find serviceProviderNameReview category in configuration hence disabling the reviews.", e);
   recipesMap.put(ProductConstants.ENABLED, false);
  }
  return recipesMap;
  
 }
 
 /**
  * Gets the filter from global config.
  * 
  * @param configMap
  *         the config map
  * @return the filter from global config
  */
 public static List<Map<String, String>> getFilterFromGlobalConfig(Map<String, String> configMap) {
  List<Map<String, String>> globalConfigFiltersList = new LinkedList<Map<String, String>>();
  int filterCount = 1;
  Set<String> filterKeySet = configMap.keySet();
  for (int j = 0; j < filterCount; j++) {
   if (isExtraNumberOfFilters(filterCount, filterKeySet)) {
    filterCount++;
   }
  }
  Map<String, String> filteringMap = new LinkedHashMap<String, String>();
  
  for (int i = 1; i < filterCount; i++) {
   filteringMap = new LinkedHashMap<String, String>();
   String filterKeyHeading = getFilterKey("filterHeadingText", i);
   String filterKeySubHeading = getFilterKey("filterSubHeadingText", i);
   String filterDisplayOption = getFilterKey(FILTER_DISPLAY_OPTION, i);
   String filterDisplayFormat = getFilterKey(UnileverConstants.FILTER_DISPLAY_FORMAT, i);
   String filterDefaultValue = getFilterKey(UnileverConstants.FILTER_DEFAULT_VALUE, i);
   String filterCssStyleClass = getFilterKey(UnileverConstants.CSS_STYLE_CLASS, i);
   filteringMap.put(UnileverConstants.FILTER_HEADING, MapUtils.getString(configMap, filterKeyHeading, StringUtils.EMPTY));
   filteringMap.put(UnileverConstants.FILTER_SUB_HEADING, MapUtils.getString(configMap, filterKeySubHeading, StringUtils.EMPTY));
   filteringMap.put(FILTER_DISPLAY_OPTION, MapUtils.getString(configMap, filterDisplayOption, StringUtils.EMPTY));
   filteringMap.put(UnileverConstants.FILTER_DISPLAY_FORMAT, MapUtils.getString(configMap, filterDisplayFormat, StringUtils.EMPTY));
   filteringMap.put(UnileverConstants.FILTER_DEFAULT_VALUE, MapUtils.getString(configMap, filterDefaultValue, StringUtils.EMPTY));
   filteringMap.put(UnileverConstants.CSS_STYLE_CLASS, MapUtils.getString(configMap, filterCssStyleClass, StringUtils.EMPTY));
   globalConfigFiltersList.add(filteringMap);
  }
  return globalConfigFiltersList;
 }
 
 /**
  * Checks if is extra number of filters.
  * 
  * @param filterCount
  *         the filter count
  * @param filterKeySet
  *         the filter key set
  * @return true, if is extra number of filters
  */
 private static boolean isExtraNumberOfFilters(int filterCount, Set<String> filterKeySet) {
  boolean flag = false;
  if (filterKeySet.contains(UnileverConstants.FILTERS + UnileverConstants.DOT + String.valueOf(filterCount) + UnileverConstants.DOT
    + FILTER_DISPLAY_OPTION)) {
   flag = true;
  }
  return flag;
 }
 
 /**
  * Gets the filter key.
  * 
  * @param key
  *         the key
  * @param filterCount
  *         the filter count
  * @return the filter key
  */
 public static String getFilterKey(String key, int filterCount) {
  StringBuilder stringBuilder = new StringBuilder();
  return stringBuilder.append(UnileverConstants.FILTERS).append(UnileverConstants.DOT).append(String.valueOf(filterCount))
    .append(UnileverConstants.DOT).append(key).toString();
 }
 
 /**
  * To map converts JSON objects return by RMS to map.
  * 
  * @param object
  *         the object
  * @return the map
  * @throws JSONException
  *          the JSON exception
  */
 public static Map<String, Object> toMap(JSONObject object) throws JSONException {
  Map<String, Object> map = new HashMap<String, Object>();
  if (object != null) {
   Iterator<String> keysItr = object.keys();
   while (keysItr.hasNext()) {
    String key = keysItr.next();
    Object value = object.get(key);
    
    if (value instanceof JSONArray) {
     value = toList((JSONArray) value);
    } else if (value instanceof JSONObject) {
     value = toMap((JSONObject) value);
    }
    if (object.isNull(key)) {
     value = null;
    }
    map.put(key, value);
   }
  }
  return map;
 }
 
 /**
  * To list.
  * 
  * @param array
  *         the array
  * @return the object
  * @throws JSONException
  *          the JSON exception
  */
 public static Object toList(JSONArray array) throws JSONException {
  List<Object> list = new ArrayList<Object>();
  for (int i = 0; i < array.length(); i++) {
   Object value = array.get(i);
   if (value instanceof JSONArray) {
    value = toList((JSONArray) value);
   } else if (value instanceof JSONObject) {
    value = toMap((JSONObject) value);
   }
   list.add(value);
  }
  return list;
 }
 
}
