/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.components.helper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.tagging.Tag;
import com.day.cq.wcm.api.Page;

/**
 * The Class PageSortingHelper.
 */
public class PageSortingHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(PageSortingHelper.class);
 
 /**
  * Instantiates a new page sorting helper.
  */
 private PageSortingHelper() {
  
 }
 /**
  * Page has tag.
  * 
  * @param productPage1
  *         the product page1
  * @param productPage2
  *         the product page2
  * @param tagName
  *         the tag name
  * @return the boolean[]
  */
 public static boolean[] pageHasTag(Page productPage1, Page productPage2, String tagName) {
  boolean[] tagFlagArr = { false, false };
  
  try {
   Tag[] tagArr1 = productPage1.getTags();
   Tag[] tagArr2 = productPage2.getTags();
   for (int i = 0; i < tagArr1.length; ++i) {
    Tag tag = tagArr1[i];
    if (tag != null && tag.getTagID().startsWith(tagName)) {
     tagFlagArr[0] = true;
    }
   }
   for (int i = 0; i < tagArr2.length; ++i) {
    Tag tag = tagArr2[i];
    if (tag != null && tag.getTagID().startsWith(tagName)) {
     tagFlagArr[1] = true;
    }
   }
   
  } catch (Exception e) {
   LOGGER.error("Error while comparing ", e);
  }
  return tagFlagArr;
 }
 
 /**
  * Page has tag.
  * 
  * @param productPage1
  *         the product page1
  * @param productPage2
  *         the product page2
  * @param tagNames
  *         the tag names
  * @return the boolean[]
  */
 public static boolean[] pageHasTagFromRequest(Page productPage1, Page productPage2, String[] tagNames) {
  boolean[] tagFlagArr = { false, false };
  
  try {
   Tag[] tagArr1 = productPage1.getTags();
   Tag[] tagArr2 = productPage2.getTags();
   for (int j = 0; j < tagNames.length; j++) {
    tagFlagArr[0] = false;
    tagFlagArr[1] = false;
    for (int i = 0; i < tagArr1.length; ++i) {
     Tag tag = tagArr1[i];
     if (tag != null && tag.getTagID().startsWith(tagNames[j])) {
      tagFlagArr[0] = true;
      break;
     }
    }
    for (int i = 0; i < tagArr2.length; ++i) {
     Tag tag = tagArr2[i];
     if (tag != null && tag.getTagID().startsWith(tagNames[j])) {
      tagFlagArr[1] = true;
      break;
     }
    }
    if (tagFlagArr[0] != tagFlagArr[1]) {
     break;
    }
   }
   
  } catch (Exception e) {
   LOGGER.error("Error while comparing ", e);
  }
  return tagFlagArr;
 }
 
 
}
