/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.osgi.framework.Constants;

import com.day.cq.wcm.api.WCMMode;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * This class prepares the data map for start section.
 */

/**
 * @author mkathu
 * 
 */
@Component(description = "Start Section view helper", immediate = true, metatype = true, label = "Start Section ViewHelper")
@Service(value = { StartSectionViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.START_SECTION, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class StartSectionViewHelperImpl extends BaseViewHelper {
 
 private static final String IS_EDIT = "isEdit";
 private static final String IS_READ_ONLY = "isReadOnly";
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData (java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  Map<String, Object> jsonProperties = new HashMap<String, Object>();
  
  Map<String, Object> componentProperties = getProperties(content);
  jsonProperties.put(UnileverConstants.RANDOM_NUMBER, MapUtils.getString(componentProperties, UnileverConstants.RANDOM_NUMBER, StringUtils.EMPTY));
  jsonProperties.put(UnileverConstants.TITLE, MapUtils.getString(componentProperties, UnileverConstants.TITLE, StringUtils.EMPTY));
  jsonProperties.put("cssClass", MapUtils.getString(componentProperties, "cssClass", StringUtils.EMPTY));
  jsonProperties.put("elementType", MapUtils.getString(componentProperties, "elementType", StringUtils.EMPTY));
  jsonProperties.put("id", MapUtils.getString(componentProperties, "elementId", StringUtils.EMPTY));
  jsonProperties.put(IS_READ_ONLY, MapUtils.getBoolean(componentProperties, IS_READ_ONLY, Boolean.FALSE));
  boolean isPublish = (!(WCMMode.fromRequest(getSlingRequest(resources)) == WCMMode.DESIGN || WCMMode.fromRequest(getSlingRequest(resources)) == WCMMode.EDIT)) ? true
    : false;
  boolean isEdit = true;
  if (isPublish) {
   isEdit = false;
  }
  jsonProperties.put(IS_EDIT, isEdit);
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(getJsonNameSpace(content), jsonProperties);
  return data;
  
 }
 
}
