/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.foundation.viewhelper.impl;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.osgi.services.HttpClientBuilderFactory;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.commons.json.JSONObject;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.cache.ComponentViewAndViewHelperCache;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.core.helper.URLConnectionHelper;
import com.unilever.platform.aem.foundation.core.service.EGiftingConnectionService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.PaymentFormConstants;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * The Class ProductCrossellViewHelperImpl.
 */
@Component(description = "PaymentFormViewHelperImpl", immediate = true, metatype = true, label = "PaymentFormViewHelperImpl")
@Service(value = { PaymentFormViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.PAYMENT_FORM, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class PaymentFormViewHelperImpl extends BaseViewHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(PaymentFormViewHelperImpl.class);
 
 /** The component view and view helper cache. */
 @Reference
 ComponentViewAndViewHelperCache componentViewAndViewHelperCache;
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 @Reference
 EGiftingConnectionService eGiftingConnectionService;
 /** The factory. */
 @Reference
 HttpClientBuilderFactory factory;
 
 /*
  * reference for site configuration service
  */
 
 /**
  * The dcs post service.
  * 
  * @param content
  *         the content
  * @param resources
  *         the resources
  * @return the map
  */
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData (java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  LOGGER.debug("Executing PaymentFormViewHelperImpl.java. Inside processData method");
  Map<String, Object> data = new HashMap<String, Object>();
  SlingHttpServletRequest slingRequest = getSlingRequest(resources);
  Resource compResource = slingRequest.getResource();
  PageManager pageManager = getPageManager(resources);
  ResourceResolver resourceResolver = getResourceResolver(resources);
  Page currentPage = pageManager.getContainingPage(compResource);
  I18n i18n = getI18n(resources);
  String jsonNameSpace = getJsonNameSpace(content);
  Map<String, String> configMap = ComponentUtil.getConfigMap(currentPage, ProductConstants.E_GIFTING_CAT);
  Map<String, Object> paymentFormMap = new HashMap<String, Object>();
  paymentFormMap.put(PaymentFormConstants.FIELDS, getFieldsMap(resources));
  paymentFormMap.put(UnileverConstants.TITLE, i18n.get(PaymentFormConstants.PAYMENT_FORM_TITLE));
  paymentFormMap.put("clientToken", getClientToken(configMap));
  Map<String, String> urlMap = getRedirectURLMap(currentPage, resourceResolver, slingRequest);
  paymentFormMap.put("urlMap", urlMap);
  data.put(jsonNameSpace, paymentFormMap);
  return data;
 }
 
 /**
  * Gets the redirect url map.
  * 
  * @param currentPage
  *         the current page
  * @param resourceResolver
  *         the resource resolver
  * @return the redirect url map
  */
 private Map<String, String> getRedirectURLMap(Page currentPage, ResourceResolver resourceResolver, SlingHttpServletRequest slingRequest) {
  String purchaseConfirmationPageURL = "";
  String dyanamicErrorPageURL = "";
  try {
   String purchaseConfirmationPagePath = configurationService.getConfigValue(currentPage, UnileverConstants.EGIFTING_CONFIG_CAT,
     "purchaseConfirmationPage");
   purchaseConfirmationPageURL = ComponentUtil.getFullURL(resourceResolver, purchaseConfirmationPagePath, slingRequest);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Error in getting configuration for egfting", e);
  }
  try {
   String dyanamicErrorPagePath = configurationService.getConfigValue(currentPage, UnileverConstants.EGIFTING_CONFIG_CAT, "dyanamicErrorPage");
   dyanamicErrorPageURL = ComponentUtil.getFullURL(resourceResolver, dyanamicErrorPagePath, slingRequest);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Error in getting configuration for egfting", e);
  }
  Map<String, String> map = new HashMap<String, String>();
  map.put("purchaseConfirmationPageURL", purchaseConfirmationPageURL);
  map.put("dyanamicErrorPageURL", dyanamicErrorPageURL);
  return map;
 }
 
 /**
  * Gets the client token.
  * 
  * @param configMap
  *         the config map
  * 
  * @return the client token
  */
 private String getClientToken(Map<String, String> configMap) {
  String clientToken = "";
  String url = eGiftingConnectionService.getClientTokenURL();
  int timeout = eGiftingConnectionService.getTimeoutTime();
  LOGGER.debug("EGifting Transaction Service generate pdf URL: {}", url);
  LOGGER.debug("EGifting Transaction Service Timeout: {}", timeout);
  Map<String, String> map = new HashMap<String, String>();
  map.put("brand", configMap.get("brand"));
  map.put("locale", configMap.get("locale"));
  map.put("entity", configMap.get("entity"));
  try {
   HttpResponse httpResponse = URLConnectionHelper.getResponseFromGet(url, map, factory, timeout);
   if (httpResponse != null) {
    HttpEntity httpEntity = httpResponse.getEntity();
    if (httpEntity != null) {
     InputStream inputStream = httpEntity.getContent();
     
     BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
     StringBuilder response = new StringBuilder();
     String inputLine;
     while ((inputLine = in.readLine()) != null) {
      response.append(inputLine);
     }
     in.close();
     
     JSONObject jsonobj = new JSONObject(response.toString());
     clientToken = (String) jsonobj.get("clientToken");
    }
   }
  } catch (Exception e) {
   LOGGER.error("ERROR_WHILE_GENERATING_DCU_TOKEN", e);
  }
  return clientToken;
 }
 
 /**
  * Gets the fields map.
  * 
  * @param resources
  *         the resources
  * @return the fields map
  */
 private Map<String, Object> getFieldsMap(Map<String, Object> resources) {
  I18n i18n = getI18n(resources);
  
  Map<String, Object> fieldsMap = new HashMap<String, Object>();
  Map<String, Object> errormsgMap = new HashMap<String, Object>();
  fieldsMap.put(PaymentFormConstants.CARD_NUMBER_LABEL, i18n.get(PaymentFormConstants.PAYMENT_FORM_CARD_NUMBER_LABEL));
  fieldsMap.put(PaymentFormConstants.EXPIRY_DATE_LABEL, i18n.get(PaymentFormConstants.PAYMENT_FORM_EXPIRY_DATE_LABEL));
  fieldsMap.put(PaymentFormConstants.EXPIRY_DATE_YEAR_PLACEHOLDER, i18n.get(PaymentFormConstants.PAYMENT_FORM_EXPIRY_DATE_YEAR_PLACEHOLDER));
  fieldsMap.put(PaymentFormConstants.EXPIRY_DATE_MONTH_PLACEHOLDER, i18n.get(PaymentFormConstants.PAYMENT_FORM_EXPIRY_DATE_MONTH_PLACEHOLDER));
  fieldsMap.put(PaymentFormConstants.TOOL_TIP, i18n.get(PaymentFormConstants.PAYMENT_FORM_TOOL_TIP));
  fieldsMap.put(PaymentFormConstants.CVV_LABEL, i18n.get(PaymentFormConstants.PAYMENT_FORM_CVV_LABEL));
  fieldsMap.put(PaymentFormConstants.PAYPAL_BUTTON_LABEL, i18n.get(PaymentFormConstants.PAYMENT_FORM_PAYPAL_BUTTON_LABEL));
  fieldsMap.put(PaymentFormConstants.CARDPAY_BUTTON_LABEL, i18n.get(PaymentFormConstants.PAYMENT_FORM_CARDPAY_BUTTON_LABEL));
  
  errormsgMap.put(UnileverConstants.TITLE, i18n.get(PaymentFormConstants.PAYMENT_FORM_ERROR_TITLE));
  errormsgMap.put(UnileverConstants.DESCRIPTION, i18n.get(PaymentFormConstants.PAYMENT_FORM_ERROR_DESCRIPTION));
  
  fieldsMap.put(PaymentFormConstants.ERRORMSG, errormsgMap);
  return fieldsMap;
 }
 
}
