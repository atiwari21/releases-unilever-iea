/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.components;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.scripting.SlingBindings;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.variants.PageVariant;
import com.day.cq.wcm.api.variants.PageVariantsProvider;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.constants.UnileverConstants;

/**
 * The Class LanguageSelectorUtility.
 */
public final class LanguageSelectorUtility {
 
 /** The Constant QUESTION_MARK. */
 private static final String QUESTION_MARK = "?";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(LanguageSelectorUtility.class);
 
 /** The Constant LINK. */
 private static final String LINK = "link";
 
 /** The Constant LINK. */
 private static final String LANGUAGES_COUNT = "languagesCount";
 
 /** The Constant TITLE. */
 private static final String TITLE = "title";
 
 /** The Constant LANGUAGE_CODE. */
 private static final String LANGUAGE_CODE = "languageCode";
 
 /** The Constant SELECTED. */
 private static final String SELECTED = "selected";
 
 /** The bindings. */
 private static SlingBindings bindings;
 
 /** The script helper. */
 private static SlingScriptHelper scriptHelper;
 
 /** The page variants provider. */
 private static PageVariantsProvider pageVariantsProvider;
 
 /** The Constant LANGUAGE_SELECTOR_CATEGORY. */
 private static final String LANGUAGE_SELECTOR_CATEGORY = "languageSelector";
 
 /** The Constant LANGUAGE_SELECTOR_CATEGORY. */
 private static final String FOOTER_CATEGORY = "footer";
 
 /** The Constant EXCLUDED_PAGES_LIST. */
 private static final String EXCLUDED_PAGES_LIST = "excludedPagesList";
 
 /** enabled key used in map. */
 private static final String FOOTER_LANGUAGE_SELCTOR_ENABLED = "languageSelectorEnabled";
 
 /** isEnabled key used in map. */
 public static final String IS_ENABLED = "isEnabled";
 
 /** The Constant LANGUAGES. */
 private static final String LANGUAGES = "languages";
 
 /** The Constant LANGUAGE_NAME. */
 private static final String LANGUAGE_NAME = "languageName";
 
 /**
  * Instantiates a new language selector utility.
  */
 private LanguageSelectorUtility() {
  
 }
 
 /**
  * Gets the page variants.
  * 
  * @param page
  *         the page
  * @param slingRequest
  *         the sling request
  * @return the page variants
  */
 private static List<PageVariant> getPageVariants(Page page, SlingHttpServletRequest slingRequest) {
  
  bindings = (SlingBindings) slingRequest.getAttribute(SlingBindings.class.getName());
  scriptHelper = bindings.getSling();
  pageVariantsProvider = scriptHelper.getService(PageVariantsProvider.class);
  List<PageVariant> variantsList = null;
  
  if (pageVariantsProvider != null) {
   variantsList = pageVariantsProvider.getVariants(page, slingRequest);
  }
  
  return variantsList;
 }
 
 /**
  * Gets the languages by country.
  * 
  * @param currentPage
  *         the current page
  * @param slingRequest
  *         the sling request
  * @param resourceResolver
  *         the resource resolver
  * @param configurationService
  *         the configuration service
  * @return the languages by country
  */
 public static List<Map<String, String>> getLanguagesByCountry(Page currentPage, SlingHttpServletRequest slingRequest,
   ResourceResolver resourceResolver, ConfigurationService configurationService) {
  
  List<Map<String, String>> languageList = new ArrayList<Map<String, String>>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  List<PageVariant> variantPages = null;
  
  try {
   variantPages = getPageVariants(currentPage, slingRequest);
   if (variantPages != null) {
    String country = currentPage.getLanguage(true).getCountry();
    PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
    String[] excludedList = getExcludedUrls(currentPage, configurationService);
    int count = GlobalConfigurationUtility.getIntegerValueFromConfiguration(LANGUAGE_SELECTOR_CATEGORY, configurationService, currentPage,
      LANGUAGES_COUNT);
    for (PageVariant variant : variantPages) {
     if (languageList.size() < count) {
      Page page = pageManager.getPage(variant.getPath());
      Map<String, String> langPagesMap = getLanguagePagesMap(page, currentPage, country, excludedList, resourceResolver, slingRequest);
      if (langPagesMap != null) {
       languageList.add(langPagesMap);
      }
     }
    }
   }
  } catch (Exception e) {
   LOGGER.error("Error while fetching languages for same country : " + e);
  }
  
  return languageList;
 }
 
 /**
  * Gets the language pages map.
  * 
  * @param page
  *         the page
  * @param currentPage
  *         the current page
  * @param country
  *         the country
  * @param excludedList
  *         the excluded list
  * @param resourceResolver
  *         the resource resolver
  * @param slingRequest
  *         the sling request
  * @return the language pages map
  */
 private static Map<String, String> getLanguagePagesMap(Page page, Page currentPage, String country, String[] excludedList,
   ResourceResolver resourceResolver, SlingHttpServletRequest slingRequest) {
  Boolean exclude = false;
  Map<String, String> langPagesMap = null;
  
  if ((page != null) && (country.equals(page.getLanguage(true).getCountry()))) {
   
   for (String exPage : excludedList) {
    if (page.getPath().startsWith(exPage)) {
     exclude = true;
     break;
    }
   }
   if (!exclude) {
    langPagesMap = new HashMap<String, String>();
    langPagesMap.put(TITLE, page.getTitle());
    langPagesMap.put(LINK, getLink(resourceResolver, page, slingRequest));
    langPagesMap.put(LANGUAGE_CODE, page.getLanguage(true).getLanguage());
    langPagesMap.put(LANGUAGE_NAME, page.getLanguage(true).getDisplayLanguage());
    if (page.equals(currentPage)) {
     langPagesMap.put(SELECTED, "true");
    } else {
     langPagesMap.put(SELECTED, "false");
    }
   }
  }
  return langPagesMap;
 }
 
 /**
  * Returns link after adding selector and suffix, if request contain any.
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param page
  *         the page
  * @param slingRequest
  *         the sling request
  * @return link
  */
 private static String getLink(ResourceResolver resourceResolver, Page page, SlingHttpServletRequest slingRequest) {
  String link = ComponentUtil.getFullURL(resourceResolver, page.getPath(), slingRequest);
  RequestPathInfo requestPathInfo = slingRequest.getRequestPathInfo();
  String resourcePath = StringUtils.EMPTY;
  String suffix = slingRequest.getQueryString();
  String selector = requestPathInfo.getSelectorString();
  String extension = requestPathInfo.getExtension();
  if (StringUtils.isNotBlank(extension)) {
   resourcePath = getURLInitialsFromLink(link, extension);
  }
  
  if (StringUtils.isNotBlank(selector) && StringUtils.isNotBlank(suffix)) {
   link = resourcePath + UnileverConstants.DOT + selector + UnileverConstants.DOT + extension + QUESTION_MARK + suffix;
  } else if (StringUtils.isNotBlank(selector) && StringUtils.isBlank(suffix)) {
   link = resourcePath + UnileverConstants.DOT + selector + UnileverConstants.DOT + extension;
  } else if (StringUtils.isBlank(selector) && StringUtils.isNotBlank(suffix)) {
   link = link.concat(QUESTION_MARK + suffix);
  }
  
  return link;
 }
 
 /**
  * This method returns the part of URL before extension. e.g.: http://localhost:4502/editor.html/content/whitelabel/en_gb/home.html For this link
  * this method will return http://localhost:4502/editor.html/content/whitelabel/en_gb/home
  * 
  * @param link
  *         the full URL of the page
  * @param extension
  *         extension of the page
  * @return the part of URL before extension
  */
 private static String getURLInitialsFromLink(String link, String extension) {
  String initialOfLink = StringUtils.EMPTY;
  String[] split = link.split(UnileverConstants.DOT + extension);
  initialOfLink = split[0];
  return initialOfLink;
 }
 
 /**
  * Gets the excluded urls.
  * 
  * @param currentPage
  *         the current page
  * @param configurationService
  *         the configuration service
  * @return the excluded urls
  */
 public static String[] getExcludedUrls(Page currentPage, ConfigurationService configurationService) {
  
  String[] excludedPagesList = {};
  String excludedPages = null;
  
  excludedPages = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, LANGUAGE_SELECTOR_CATEGORY,
    EXCLUDED_PAGES_LIST);
  if (excludedPages != null) {
   excludedPagesList = excludedPages.split(",");
  }
  
  return excludedPagesList;
 }
 
 /**
  * Returns the languages list corresponding to current page's country along with the language.
  * 
  * @param currentPage
  *         the current page
  * @param slingRequest
  *         the sling request
  * @param resourceResolver
  *         the resource resolver
  * @param configurationService
  *         the configuration service
  * @return the languages
  */
 public static Map<String, Object> getLanguages(Page currentPage, SlingHttpServletRequest slingRequest, ResourceResolver resourceResolver,
   ConfigurationService configurationService) {
  Map<String, Object> languageSelector = new HashMap<String, Object>();
  languageSelector.put(LANGUAGES, LanguageSelectorUtility.getLanguagesByCountry(currentPage, slingRequest, resourceResolver, configurationService)
    .toArray());
  String isEnabled = "true";
  try {
   isEnabled = configurationService.getConfigValue(currentPage, LANGUAGE_SELECTOR_CATEGORY, FOOTER_LANGUAGE_SELCTOR_ENABLED);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Unable to find excluded Pages List Category ", e);
  }
  
  languageSelector.put(IS_ENABLED, BooleanUtils.toBoolean(isEnabled));
  return languageSelector;
  
 }
 
 /**
  * Returns true/false based on if the language selector enabled in footer.
  * 
  * @param currentPage
  *         the current page
  * @param configurationService
  *         the configuration service
  * @return true/false
  */
 public static boolean isLanguageSelectorEnabledInFooter(Page currentPage, ConfigurationService configurationService) {
  String isEnabled = "true";
  try {
   isEnabled = configurationService.getConfigValue(currentPage, FOOTER_CATEGORY, FOOTER_LANGUAGE_SELCTOR_ENABLED);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Unable to find excluded Pages List Category ", e);
  }
  return BooleanUtils.toBoolean(isEnabled);
 }
 
}
