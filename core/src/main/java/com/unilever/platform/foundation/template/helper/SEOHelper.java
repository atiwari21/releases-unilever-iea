/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.template.helper;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.designer.Design;
import com.day.cq.wcm.api.designer.Designer;
import com.day.cq.wcm.api.variants.PageVariant;
import com.day.cq.wcm.api.variants.PageVariantsProvider;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.constants.CommonConstants;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.HrefLangTag;
import com.unilever.platform.foundation.template.dto.MetaTag;

/**
 * The Class SEOHelper.
 */
public final class SEOHelper {
 
 private static final String FAVICON_IPAD_NAME = "favicon-ipad.png";
 
 private static final String FAVICON_ICO_NAME = "favicon.ico";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(SEOHelper.class);
 
 /** The Constant META_TAG_CONTENT. */
 private static final String META_TAG_CONTENT = "metaTagContent";
 
 /** The Constant META_TAG_NAME. */
 private static final String META_TAG_NAME = "metaTagName";
 
 /** The Constant HREF_LANG. */
 private static final String HREF_LANG = "hreflang";
 
 /** The Constant OPEN_GRAPH. */
 private static final String OPEN_GRAPH = "openGraph";
 
 /** The Constant OG_TYPE. */
 private static final String OG_TYPE = "og_type";
 
 /** The Constant OG_SITE_NAME. */
 private static final String OG_SITE_NAME = "og:site_name";
 
 /** The Constant TWO. */
 private static final int TWO = 2;
 
 /**
  * The Enum OG_TYPE.
  */
 enum OG_TYPE_VALUES {
  
  /** The article. */
  ARTICLE_VAL,
  
  /** The product. */
  PRODUCT_VAL,
  
  /** The website. */
  WEBSITE_VAL;
 }
 
 /**
  * Instantiates a new SEO helper.
  */
 private SEOHelper() {
  
 }
 
 /**
  * Gets the value.
  * 
  * @param val
  *         the val
  * @return the value
  */
 public static String getValue(OG_TYPE_VALUES val) {
  String returnVal = "website";
  switch (val) {
   case ARTICLE_VAL:
    returnVal = "article";
    break;
   case PRODUCT_VAL:
    returnVal = "product";
    break;
   default:
    returnVal = "website";
    break;
  }
  return returnVal;
 }
 
 /**
  * Sets the full url.
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param page
  *         the page
  * @param request
  *         the request
  */
 private static void setFullURL(ResourceResolver resourceResolver, Page page, HttpServletRequest request) {
  LOGGER.debug("Inside setFullURL of SEOHelper");
  String fullURL = ComponentUtil.getFullURL(resourceResolver, page.getPath());
  request.setAttribute("fullURL", ComponentUtil.getURLWithUpdatedProtocol(fullURL, request));
  String queryString = request.getQueryString();
  try {
   URI fullURI = new URI(fullURL);
   if (StringUtils.isNotBlank(queryString)) {
    if (StringUtils.isNotBlank(fullURI.getQuery())) {
     fullURL += "&" + queryString;
    } else {
     fullURL += "?" + queryString;
    }
   }
  } catch (URISyntaxException e) {
   LOGGER.error("error while creating uri", e);
  }
  
  request.setAttribute("canonicalPath", ComponentUtil.getURLWithUpdatedProtocol(fullURL, request));
  
 }
 
 /**
  * Sets the other meta tag.
  * 
  * @param prop
  *         the prop
  * @param request
  *         the request
  */
 private static void setOtherMetaTag(ValueMap prop, HttpServletRequest request) {
  LOGGER.debug("Inside setOtherMetaTag of SEOHelper");
  List<MetaTag> list = new ArrayList<MetaTag>();
  try {
   String[] metaTagContent = ComponentUtil.getPropertyValueArray(prop, META_TAG_CONTENT);
   String[] metaTagName = ComponentUtil.getPropertyValueArray(prop, META_TAG_NAME);
   LOGGER.debug("metaTagContent = " + metaTagContent + " metaTagName = " + metaTagName);
   int len = metaTagContent != null ? metaTagContent.length : 0;
   for (int i = 0; i < len; ++i) {
    MetaTag tag = new MetaTag();
    tag.setName(metaTagName[i]);
    tag.setContent(metaTagContent[i]);
    list.add(tag);
   }
  } catch (Exception e) {
   LOGGER.error("Error in getting the other Meta tags List ", e);
  }
  request.setAttribute("otherMetaList", list);
 }
 
 /**
  * Sets the language.
  * 
  * @param page
  *         the page
  * @param request
  *         the request
  */
 private static void setLanguage(Page page, HttpServletRequest request) {
  LOGGER.debug("Inside setLanguage of SEOHelper");
  request.setAttribute("lang", page.getLanguage(true).getLanguage());
 }
 
 /**
  * Sets the href lang list.
  * 
  * @param page
  *         the page
  * @param request
  *         the request
  * @param resourceResolver
  *         the resource resolver
  * @param sling
  *         the sling
  */
 private static void setHREFLangList(Page page, HttpServletRequest request, ResourceResolver resourceResolver, SlingScriptHelper sling) {
  LOGGER.debug("Inside setHREFLangList of SEOHelper");
  List<HrefLangTag> list = new ArrayList<HrefLangTag>();
  try {
   String pageName = page.getName();
   final PageVariantsProvider p = sling.getService(PageVariantsProvider.class);
   if (p == null) {
    throw new IllegalStateException("Missing PageVariantsProvider service");
   }
   for (PageVariant v : p.getVariants(page, sling.getRequest())) {
    String path = v.getPath();
    String variantName = path.substring(path.lastIndexOf(CommonConstants.SLASH_CHAR) + 1, path.length());
    if (variantName.equals(pageName) && path.indexOf("/global") == -1) {
     HrefLangTag hrefLangTag = new HrefLangTag();
     hrefLangTag.setTitle(v.getTitle());
     String url = ComponentUtil.getFullURL(resourceResolver, path);
     hrefLangTag.setUrl(ComponentUtil.getURLWithUpdatedProtocol(url, request));
     String[] hrefAttribute = v.getAttributes().get(HREF_LANG) != null ? v.getAttributes().get(HREF_LANG).split("_") : new String[] {};
     String hrefLang = "";
     if (hrefAttribute.length == TWO) {
      hrefLang = new Locale(hrefAttribute[0], hrefAttribute[1]).toLanguageTag();
      hrefLangTag.setHrefLang(hrefLang);
     } else if (hrefAttribute.length == 1) {
      hrefLang = new Locale(hrefAttribute[0]).toLanguageTag();
      hrefLangTag.setHrefLang(hrefLang);
     }
     
     list.add(hrefLangTag);
    }
   }
  } catch (Exception e) {
   LOGGER.error("Error in getting the hreflang List ", e);
  }
  request.setAttribute("hrefLangList", list);
 }
 
 /**
  * Sets the seo attributes.
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param currentPage
  *         the current page
  * @param request
  *         the request
  * @param sling
  *         the sling
  */
 public static void setSEOAttributes(ResourceResolver resourceResolver, Page currentPage, HttpServletRequest request, SlingScriptHelper sling) {
  if (currentPage != null) {
   setFullURL(resourceResolver, currentPage, request);
   setOtherMetaTag(currentPage.getProperties(), request);
   setLanguage(currentPage, request);
   setHREFLangList(currentPage, request, resourceResolver, sling);
   setSiteName(currentPage, resourceResolver, request);
   
   // make a call to setFavIcon method to set the favicon, ipadFavIcon.
   setFavIcon(resourceResolver, currentPage, request);
   // end
   String templatePath = currentPage.getProperties().get(NameConstants.PN_TEMPLATE, "");
   
   if (templatePath.endsWith(UnileverConstants.HOME_PAGE_TEMPLATE_NAME)) {
    request.setAttribute("isHomePage", true);
   }
   setSocialShareImage(currentPage, request, sling.getRequest());
   
   setSocialTags(templatePath, currentPage, request);
   
   setCookiePolicyFlag(currentPage, request);
  }
  
 }
 
 /**
  * Sets the cookie policy flag.
  * 
  * @param currentPage
  *         the current page
  * @param request
  *         the request
  */
 private static void setCookiePolicyFlag(Page currentPage, HttpServletRequest request) {
  ConfigurationService configurationService = currentPage.adaptTo(ConfigurationService.class);
  String iscookiePopupEnabled = null;
  try {
   iscookiePopupEnabled = configurationService.getConfigValue(currentPage, "cookiePopup", "isEnabled");
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Configuration not found for cookiePopup", e);
  }
  request.setAttribute("isCookiePopupEnabled", iscookiePopupEnabled);
  
 }
 
 /**
  * Sets the social tags.
  * 
  * @param templatePath
  *         the template path
  * @param currentPage
  *         the current page
  * @param request
  *         the request
  */
 private static void setSocialTags(String templatePath, Page currentPage, HttpServletRequest request) {
  if (templatePath.endsWith(UnileverConstants.ARTICLE_TEMPLATE_NAME)) {
   request.setAttribute(OG_TYPE, getValue(OG_TYPE_VALUES.ARTICLE_VAL));
  } else if (templatePath.endsWith(UnileverConstants.PRODUCT_TEMPLATE_NAME)) {
   request.setAttribute(OG_TYPE, getValue(OG_TYPE_VALUES.PRODUCT_VAL));
  } else {
   request.setAttribute(OG_TYPE, getValue(OG_TYPE_VALUES.WEBSITE_VAL));
  }
  ConfigurationService configurationService = currentPage.adaptTo(ConfigurationService.class);
  String ogSiteName = "";
  try {
   ogSiteName = configurationService.getConfigValue(currentPage, OPEN_GRAPH, OG_SITE_NAME);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Couldn't find og:site_name from configuration", e);
  }
  request.setAttribute("ogSiteName", ogSiteName);
  
  ValueMap properties = currentPage.getContentResource().getValueMap();
  
  String jcrTitle = MapUtils.getString(properties, JcrConstants.JCR_TITLE, "");
  String jcrDesc = MapUtils.getString(properties, JcrConstants.JCR_DESCRIPTION, "");
  String ogTitle = MapUtils.getString(properties, "ogTitle", jcrTitle);
  String ogDescription = MapUtils.getString(properties, "ogDescription", jcrDesc);
  String twitterTitle = MapUtils.getString(properties, "twitterTitle", jcrTitle);
  String twitterDescription = MapUtils.getString(properties, "twitterDescription", jcrDesc);
  
  request.setAttribute("ogTitle", ogTitle);
  request.setAttribute("ogDescription", ogDescription);
  request.setAttribute("twitterTitle", twitterTitle);
  request.setAttribute("twitterDescription", twitterDescription);
 }
 
 /**
  * Sets the social share image.
  * 
  * @param currentPage
  *         the current page
  * @param request
  *         the request
  */
 private static void setSocialShareImage(Page currentPage, HttpServletRequest request, SlingHttpServletRequest httpServletRequest) {
  String socialShareImagePath = StringUtils.EMPTY;
  ValueMap properties = currentPage.getProperties();
  socialShareImagePath = properties.get(UnileverConstants.TEASER_IMAGE, "");
  if (StringUtils.isBlank(socialShareImagePath)) {
   Resource imgRes = currentPage.getContentResource("image");
   if (imgRes != null) {
    ValueMap imgProperties = imgRes.getValueMap();
    if (imgProperties != null) {
     socialShareImagePath = imgProperties.get("fileReference", String.class);
    }
   }
  }
  if (!StringUtils.isBlank(socialShareImagePath)) {
   socialShareImagePath = ComponentUtil.getStaticFileFullPath(socialShareImagePath, currentPage, httpServletRequest);
  }
  request.setAttribute("socialShareImage", socialShareImagePath);
 }
 
 /**
  * Sets the site name.
  * 
  * @param page
  *         the page
  * @param resourceResolver
  *         the resource resolver
  * @param request
  *         the request
  */
 private static void setSiteName(Page page, ResourceResolver resourceResolver, HttpServletRequest request) {
  String relPath = page.getPath();
  String fullURL = ComponentUtil.getFullURL(resourceResolver, relPath);
  fullURL = ComponentUtil.getURLWithUpdatedProtocol(fullURL, request);
  String domainName = fullURL.split(relPath).length > 0 ? fullURL.split(relPath)[0] : "";
  request.setAttribute("domainName", domainName);
 }
 
 /**
  * Sets the site name.
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param page
  *         the page
  * @param request
  *         the request
  */
 private static void setFavIcon(ResourceResolver resourceResolver, Page currentPage, HttpServletRequest request) {
  Designer designer = resourceResolver.adaptTo(Designer.class);
  Design currentDesign = designer.getDesign(currentPage);
  String favIconPath = StringUtils.EMPTY;
  String favIconIPADPath = StringUtils.EMPTY;
  if (currentDesign != null) {
   String currentDesignPath = currentDesign.getPath();
   Resource resource = resourceResolver.getResource(currentDesignPath);
   Resource localeResource = resource.getChild(StringUtils.lowerCase(currentPage.getLanguage(false).toString()));
   if (localeResource != null) {
    String localeResourcePath = localeResource.getPath();
    if (resourceResolver.getResource(localeResourcePath + UnileverConstants.FORWARD_SLASH + FAVICON_ICO_NAME) != null) {
     favIconPath = localeResourcePath + UnileverConstants.FORWARD_SLASH + FAVICON_ICO_NAME;
    }
    if (resourceResolver.getResource(localeResourcePath + UnileverConstants.FORWARD_SLASH + FAVICON_IPAD_NAME) != null) {
     favIconIPADPath = localeResourcePath + UnileverConstants.FORWARD_SLASH + FAVICON_IPAD_NAME;
    }
   }
   
   favIconPath = StringUtils.isNotBlank(favIconPath) ? favIconPath : (currentDesignPath + UnileverConstants.FORWARD_SLASH + FAVICON_ICO_NAME);
   favIconIPADPath = StringUtils.isNotBlank(favIconIPADPath) ? favIconIPADPath : currentDesignPath + UnileverConstants.FORWARD_SLASH
     + FAVICON_IPAD_NAME;
   
  }
  request.setAttribute("favIconPath", favIconPath);
  request.setAttribute("favIconIPADPath", favIconIPADPath);
 }
}
