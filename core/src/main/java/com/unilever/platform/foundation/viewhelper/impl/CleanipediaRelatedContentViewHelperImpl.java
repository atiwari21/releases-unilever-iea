/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.template.dto.Image;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * This class prepares the data map for Anchor Link.
 */
@Component(description = "CleanipediaRelatedContent", immediate = true, metatype = true, label = "CleanipediaRelatedContentViewHelperImpl")
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = "/apps/unilever-iea/components/cleanipediaRelatedContent", propertyPrivate = true), })
public class CleanipediaRelatedContentViewHelperImpl extends BaseViewHelper {
 
 private static final String CLEANIPEDIA_CONFIGURATION = "cleanipediaConfiguration";
 
 private static final String PAIRED_CONTENT_JSON = "pairedContentJson";
 
 private static final String RELATED_HEADING_TEXT = "relatedHeadingText";
 
 private static final String FALSE = "false";
 
 private static final String PAIRED_CONTENT = "pairedContent";
 
 private static final String RELATED_CONTENT = "relatedContent";
 
 private static final String CONTENT_QUERY = "contentQuery";
 
 private static final String HEADING_TEXT = "headingText";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(CleanipediaRelatedContentViewHelperImpl.class);
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("Executing CleanipediaRelatedContentViewHelperImpl.java. Inside processData method");
  Map<String, Object> properties = getProperties(content);
  Map<String, Object> cleanipediaContentMap = new LinkedHashMap<String, Object>();
  
  Resource currentResource = getCurrentResource(resources);
  Page currentPage = getCurrentPage(resources);
  I18n i18n = getI18n(resources);
  
  Boolean isPairedContent = Boolean.parseBoolean(MapUtils.getString(properties, PAIRED_CONTENT, FALSE));
  Boolean isRelatedContent = Boolean.parseBoolean(MapUtils.getString(properties, RELATED_CONTENT, FALSE));
  cleanipediaContentMap.put(CLEANIPEDIA_CONFIGURATION, getRetrivalParams(configurationService, currentPage));
  
  if (isPairedContent) {
   Map<String, Object> pairedContentMap = new LinkedHashMap<String, Object>();
   pairedContentMap.put(HEADING_TEXT, MapUtils.getString(properties, HEADING_TEXT, StringUtils.EMPTY));
   pairedContentMap.put(PAIRED_CONTENT, (getArticleList(currentResource, currentPage, getSlingRequest(resources))).toArray());
   cleanipediaContentMap.put(PAIRED_CONTENT, pairedContentMap);
  }
  if (isRelatedContent) {
   Map<String, Object> relatedContentMap = new LinkedHashMap<String, Object>();
   relatedContentMap.put(HEADING_TEXT, MapUtils.getString(properties, RELATED_HEADING_TEXT, StringUtils.EMPTY));
   relatedContentMap.put(CONTENT_QUERY, MapUtils.getString(properties, CONTENT_QUERY, StringUtils.EMPTY));
   cleanipediaContentMap.put(RELATED_CONTENT, relatedContentMap);
  }
  cleanipediaContentMap.put("readMoreCtaLabel", i18n.get("cleanipediaRelatedContent.readMoreCtaLabel"));
  LOGGER.info("The Cleanipedia Map is successfully generated");
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(getJsonNameSpace(content), cleanipediaContentMap);
  return data;
 }
 
 private List<Map<String, Object>> getArticleList(Resource currentResource, Page page, SlingHttpServletRequest request) {
  List<Map<String, Object>> articleMapList = new ArrayList<Map<String, Object>>();
  List<Map<String, String>> list = ComponentUtil.getNestedMultiFieldProperties(currentResource, PAIRED_CONTENT_JSON);
  for (Map<String, String> map : list) {
   Map<String, Object> articleMap = new LinkedHashMap<String, Object>();
   articleMap.put("articleId", MapUtils.getString(map, "articleId", StringUtils.EMPTY));
   articleMap.put("articleDescription", MapUtils.getString(map, "articleDescription", StringUtils.EMPTY));
   articleMap.put("articleHeading", MapUtils.getString(map, "articleHeading", StringUtils.EMPTY));
   String imageUrl = MapUtils.getString(map, "articleImage", StringUtils.EMPTY);
   String alt = MapUtils.getString(map, "imageAltText", StringUtils.EMPTY);
   Image image = new Image(imageUrl, alt, "", page, request);
   if (null != image) {
    Map<String, Object> imageMap = image.convertToMap();
    articleMap.put("image", imageMap);
   }
   articleMapList.add(articleMap);
  }
  LOGGER.debug("Paired article list size : " + articleMapList.size());
  return articleMapList;
  
 }
 
 private Map<String, String> getRetrivalParams(ConfigurationService configurationService, Page page) {
  Map<String, String> retrivalParamMap = new LinkedHashMap<String, String>();
  try {
   retrivalParamMap = configurationService.getCategoryConfiguration(page, "cleanipediaArticles");
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find cleanipediaArticles category", e);
  }
  return retrivalParamMap;
 }
}
