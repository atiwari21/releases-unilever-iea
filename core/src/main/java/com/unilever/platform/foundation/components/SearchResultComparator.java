/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.components;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.jcr.RepositoryException;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unilever.platform.aem.foundation.core.dto.SearchDTO;

/**
 * The Class SearchResultComparator.
 */
public class SearchResultComparator implements Comparator<Object> {
 
 /**
  * The Class DataDTO.
  */
 class DataDTO {
  
  /** The count. */
  int count;
  
  /** The date. */
  Date date;
  
  /** The tag flag. */
  boolean tagFlag;
  
  /**
   * Gets the count.
   * 
   * @return the count
   */
  public int getCount() {
   return count;
  }
  
  /**
   * Sets the count.
   * 
   * @param count
   *         the new count
   */
  public void setCount(int count) {
   this.count = count;
  }
  
  /**
   * Gets the date.
   * 
   * @return the date
   */
  public Date getDate() {
   return date;
  }
  
  /**
   * Sets the date.
   * 
   * @param date
   *         the new date
   */
  public void setDate(Date date) {
   this.date = date;
  }
  
  /**
   * Checks if is tag flag.
   * 
   * @return true, if is tag flag
   */
  public boolean isTagFlag() {
   return tagFlag;
  }
  
  /**
   * Sets the tag flag.
   * 
   * @param tagFlag
   *         the new tag flag
   */
  public void setTagFlag(boolean tagFlag) {
   this.tagFlag = tagFlag;
  }
  
 }
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(SearchResultComparator.class);
 
 /** The Constant DEFAULT_DATE_STRING. */
 private static final String DEFAULT_DATE_STRING = "1900/01/01";
 
 /** The Constant DATE_FORMAT. */
 private static final String DATE_FORMAT = "yyyy/MM/dd";
 
 /** The tags list. */
 private List<String> tagsList;
 
 /** The featuretag. */
 private String[] featuretag;
 
 /** The sort by date. */
 private boolean sortByDate;
 
 /** The default date. */
 private static Date defaultDate;
 static {
  DateFormat df = new SimpleDateFormat(DATE_FORMAT);
  try {
   defaultDate = df.parse(DEFAULT_DATE_STRING);
  } catch (ParseException e) {
   LOGGER.warn("Date ParseException", e);
  }
 }
 
 /**
  * Instantiates a new search result comparator.
  * 
  * @param tagsCollection
  *         the tags collection
  * @param featuretag
  *         the featuretag
  * @param sortByDate
  *         the sort by date
  */
 public SearchResultComparator(List<String> tagsCollection, String[] featuretag, boolean sortByDate) {
  super();
  tagsList = tagsCollection;
  this.featuretag = featuretag != null?featuretag:new String[]{};
  this.sortByDate = sortByDate;
 }
 
 @Override
 /*
  * (non-Javadoc)
  * 
  * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
  */
 public int compare(Object resultNodeObj1, Object resultNodeObj2) {
  SearchDTO resultNode1 = (SearchDTO) resultNodeObj1;
  SearchDTO resultNode2 = (SearchDTO) resultNodeObj2;
  
  DataDTO dataDTO1 = new DataDTO();
  DataDTO dataDTO2 = new DataDTO();
  boolean featured1 = false;
  boolean featured2 = false;
  for (int i = 0; i < featuretag.length; i++) {
   featured1 = false;
   featured2 = false;
   dataDTO1 = getDataObj(resultNode1, i);
   dataDTO2 = getDataObj(resultNode2, i);
   if (dataDTO1.isTagFlag() != dataDTO2.isTagFlag()) {
    featured1 = dataDTO1.isTagFlag();
    featured2 = dataDTO2.isTagFlag();
    break;
   }
  }
  if(featuretag.length<1){
   dataDTO1 = getDataObj(resultNode1, 0);
   dataDTO2 = getDataObj(resultNode2, 0);
  }
  int res = dataDTO2.getCount() - dataDTO1.getCount();
  if (res == 0) {
   if (this.sortByDate && featured1 == featured2) {
    res = dataDTO2.getDate().compareTo(dataDTO1.getDate());
   } else if (featured1 != featured2) {
    if (featured1) {
     res = -1;
    } else if (featured2) {
     res = 1;
    }
   }
  }
  return res;
 }
 
 /**
  * Gets the data obj.
  * 
  * @param resultNode
  *         the result node
  * @return the data obj
  */
 private DataDTO getDataObj(SearchDTO resultNode, int tagFlagCount) {
  DataDTO dataDTO = new DataDTO();
  dataDTO.setTagFlag(false);
  int count = 0;
  try {
   for (Value tag1 : resultNode.getResultNodeTags()) {
    
    if (tagsList.contains(tag1.getString())) {
     count++;
    }
    
    if (featuretag.length>tagFlagCount && tag1.getString().startsWith(featuretag[tagFlagCount])) {
     dataDTO.setTagFlag(true);
    }
   }
  } catch (ValueFormatException e) {
   LOGGER.error(ExceptionUtils.getStackTrace(e));
  } catch (IllegalStateException e) {
   LOGGER.error(ExceptionUtils.getStackTrace(e));
  } catch (RepositoryException e) {
   LOGGER.error(ExceptionUtils.getStackTrace(e));
  }
  Date publishDate = resultNode.getReplicationTime() != null ? resultNode.getReplicationTime() : defaultDate;
  dataDTO.setCount(count);
  dataDTO.setDate(publishDate);
  return dataDTO;
 }
 
}
