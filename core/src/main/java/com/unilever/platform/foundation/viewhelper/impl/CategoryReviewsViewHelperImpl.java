/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.integration.RatingHelper;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * This class prepares the data map for Virtual Agent ComponentComponent It reads the values from the dialog and passes the info as a json.
 */

@Component(description = "CategoryReview", immediate = true, metatype = true, label = "Category Review")
@Service(value = { CategoryReviewsViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.CATEGORY_REVIEWS, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class CategoryReviewsViewHelperImpl extends BaseViewHelper {
 
 /** constant category reviews */
 private static final String CATEGORY_REVIEWS = "categoryReviews";
 
 /** constant category List */
 private static final String CATEGORY_LIST = "categoryList";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(CategoryReviewsViewHelperImpl.class);
 
 @Reference
 private ConfigurationService configurationService;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData (java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  LOGGER.debug("Executing CategoryReviewsViewHelperImpl. Inside processData method ");
  Map<String, Object> properties = new HashMap<String, Object>();
  Map<String, Object> ratingproperties = new HashMap<String, Object>();
  List<Map<String, Object>> categoryListproperties = new ArrayList<Map<String, Object>>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  ratingproperties.put(ProductConstants.PRODUCT_RATINGS_MAP, getRatingMAps(resources));
  categoryListproperties.add(ratingproperties);
  properties.put(ProductConstants.REVIEW_MAP, getReviewMap(resources));
  properties.put(CATEGORY_LIST, categoryListproperties.toArray());
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE), properties);
  return data;
  
 }
 
 private Map<String, String> getRatingMAps(final Map<String, Object> resources) {
  String uniqueId = RatingHelper.getCategoryId(configurationService, getCurrentPage(resources));
  String viewType = StringUtils.EMPTY;
  String entityType = StringUtils.EMPTY;
  Map<String, String> ratingsMap = new HashMap<String, String>();
  try {
   viewType = configurationService.getConfigValue(getCurrentPage(resources), ProductConstants.KQ_VIEW_TYPE, CATEGORY_REVIEWS);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find view type in KritiqueViewType category", e);
  }
  try {
   entityType = configurationService.getConfigValue(getCurrentPage(resources), CATEGORY_REVIEWS, ProductConstants.RATING_ENTITY_TYPE);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find entity type config.", e);
  }
  uniqueId = RatingHelper.getProductFamilyPrefix(configurationService, getCurrentPage(resources)) + uniqueId;
  ratingsMap.put(ProductConstants.RATING_ENTITY_TYPE, entityType);
  ratingsMap.put(ProductConstants.RATING_VIEW_TYPE, viewType);
  ratingsMap.put(ProductConstants.RATING_UNIQUE_ID, uniqueId);
  
  return ratingsMap;
 }
 
 private Map<String, String> getReviewMap(final Map<String, Object> resources) {
  String isEnabled = StringUtils.EMPTY;
  Map<String, String> reviewMap = ProductHelper.getReviewMapProperties(configurationService, getCurrentPage(resources));
  try {
   isEnabled = configurationService.getConfigValue(getCurrentPage(resources), CATEGORY_REVIEWS, ProductConstants.ENABLED);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find isRatingAndReviewEnabled key in categoryReviews category", e);
  }
  reviewMap.put(ProductConstants.ENABLED, isEnabled);
  return reviewMap;
 }
 
}
