/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.template.dto;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfiguration;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * The Class KritiqueDTO.
 */
public class KritiqueDTO {
 
 /** The Constant TEN. */
 private static final int TEN = 10;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(KritiqueDTO.class);
 
 /** The api key. */
 private String apiKey;
 
 /** The brand id. */
 private long brandId;
 
 /** The is syndication on. */
 private boolean isSyndicationOn;
 
 /** The locale id. */
 private long localeId;
 
 /** The no of records. */
 private int noOfRecords;
 
 /** The site product id. */
 private String siteProductId;
 
 /** The site source. */
 private String siteSource;
 
 /** The api path. */
 private String apiPath;
 
 /** The pageno. */
 private int pageno;
 
 /** The sortby. */
 private String sortby;
 
 /** The Referer. */
 private String referer;
 
 /** The site entity Source Id. */
 private String entitySourceId;
 /**
  * Instantiates a new kritique dto.
  * 
  * @param page
  *         the page
  * @param configurationService
  *         the configuration service
  * @param configCategory
  *         the config category
  * @param configKey
  *         the config key
  */
 public KritiqueDTO(Page page, ConfigurationService configurationService) {
  
  try {
   
   GlobalConfiguration globalConfiguration = page.adaptTo(GlobalConfiguration.class);
   this.apiKey = ComponentUtil.getProperteryFromConfig(page, configurationService, ProductConstants.KQ_KEY, ProductConstants.API_KEY);
   this.brandId = Long.parseLong(ComponentUtil
     .getProperteryFromConfig(page, configurationService, ProductConstants.KQ_KEY, ProductConstants.BRAND_ID));
   this.isSyndicationOn = Boolean.parseBoolean(ComponentUtil.getProperteryFromConfig(page, configurationService, ProductConstants.KQ_KEY,
     ProductConstants.SYNDICATION));
   this.localeId = Long.parseLong(ComponentUtil.getProperteryFromConfig(page, configurationService, ProductConstants.KQ_KEY,
     ProductConstants.LOCALE_ID));
   this.noOfRecords = Integer.parseInt(ComponentUtil.getProperteryFromConfig(page, configurationService, ProductConstants.KQ_KEY,
     ProductConstants.RECORDS_COUNT));
   this.siteProductId = StringUtils.EMPTY;
   this.entitySourceId = StringUtils.EMPTY;
   this.siteSource = ComponentUtil.getProperteryFromConfig(page, configurationService, ProductConstants.KQ_KEY, ProductConstants.SITE_SOURCE);
   this.apiPath = globalConfiguration.getConfigValue(ProductConstants.KQ_API_BASE_PATH);
   
   this.pageno = Integer.parseInt(ComponentUtil.getProperteryFromConfig(page, configurationService, ProductConstants.KQ_KEY,
     ProductConstants.PAGE_NUMBER));
   this.sortby = ComponentUtil.getProperteryFromConfig(page, configurationService, ProductConstants.KQ_KEY, ProductConstants.SORT_BY);
   this.referer = ComponentUtil.getProperteryFromConfig(page, configurationService, ProductConstants.KQ_KEY, ProductConstants.REFERER);
  } catch (Exception e) {
   LOGGER.error("Configuration not found in global configurations", e);
  }
 }
 
 /**
  * Instantiates a new kritique dto.
  */
 public KritiqueDTO() {
  
  this.apiKey = StringUtils.EMPTY;
  this.brandId = 0;
  this.isSyndicationOn = false;
  this.localeId = 0;
  this.noOfRecords = TEN;
  this.siteProductId = StringUtils.EMPTY;
  this.siteSource = StringUtils.EMPTY;
  this.apiPath = StringUtils.EMPTY;
  this.pageno = 1;
  this.sortby = StringUtils.EMPTY;
  this.referer = StringUtils.EMPTY;
  this.entitySourceId = StringUtils.EMPTY;
 }
 
 /**
  * Gets the api key.
  * 
  * @return the api key
  */
 public String getApiKey() {
  return apiKey;
 }
 
 /**
  * Sets the api key.
  * 
  * @param apiKey
  *         the new api key
  */
 public void setApiKey(String apiKey) {
  this.apiKey = apiKey;
 }
 
 /**
  * Gets the brand id.
  * 
  * @return the brand id
  */
 public long getBrandId() {
  return brandId;
 }
 
 /**
  * Sets the brand id.
  * 
  * @param brandId
  *         the new brand id
  */
 public void setBrandId(long brandId) {
  this.brandId = brandId;
 }
 
 /**
  * Checks if is syndication on.
  * 
  * @return true, if is syndication on
  */
 public boolean isSyndicationOn() {
  return isSyndicationOn;
 }
 
 /**
  * Sets the syndication on.
  * 
  * @param isSyndicationOn
  *         the new syndication on
  */
 public void setSyndicationOn(boolean isSyndicationOn) {
  this.isSyndicationOn = isSyndicationOn;
 }
 
 /**
  * Gets the locale id.
  * 
  * @return the locale id
  */
 public long getLocaleId() {
  return localeId;
 }
 
 /**
  * Sets the locale id.
  * 
  * @param localeId
  *         the new locale id
  */
 public void setLocaleId(long localeId) {
  this.localeId = localeId;
 }
 
 /**
  * Gets the no of records.
  * 
  * @return the no of records
  */
 public int getNoOfRecords() {
  return noOfRecords;
 }
 
 /**
  * Sets the no of records.
  * 
  * @param noOfRecords
  *         the new no of records
  */
 public void setNoOfRecords(int noOfRecords) {
  this.noOfRecords = noOfRecords;
 }
 
 /**
  * Gets the site product id.
  * 
  * @return the site product id
  */
 public String getSiteProductId() {
  return siteProductId;
 }
 
 /**
  * Sets the site product id.
  * 
  * @param siteProductId
  *         the new site product id
  */
 public void setSiteProductId(String siteProductId) {
  this.siteProductId = siteProductId;
 }
 
 /**
  * Gets the site source.
  * 
  * @return the site source
  */
 public String getSiteSource() {
  return siteSource;
 }
 
 /**
  * Sets the site source.
  * 
  * @param siteSource
  *         the new site source
  */
 public void setSiteSource(String siteSource) {
  this.siteSource = siteSource;
 }
 
 /**
  * Gets the api path.
  * 
  * @return the api path
  */
 public String getApiPath() {
  return apiPath;
 }
 
 /**
  * Sets the api path.
  * 
  * @param apiPath
  *         the new api path
  */
 public void setApiPath(String apiPath) {
  this.apiPath = apiPath;
 }
 
 /**
  * Gets the pageno.
  * 
  * @return the pageno
  */
 public int getPageno() {
  return pageno;
 }
 
 /**
  * Sets the pageno.
  * 
  * @param pageno
  *         the new pageno
  */
 public void setPageno(int pageno) {
  this.pageno = pageno;
 }
 
 /**
  * Gets the sortby.
  * 
  * @return the sortby
  */
 public String getSortby() {
  return sortby;
 }
 
 /**
  * Sets the sortby.
  * 
  * @param sortby
  *         the new sortby
  */
 public void setSortby(String sortby) {
  this.sortby = sortby;
 }
 
 /**
  * Gets the referer.
  * 
  * @return the referer
  */
 public String getReferer() {
  return referer;
 }
 
 /**
  * Sets the referer.
  * 
  * @param sortby
  *         the new referer
  */
 public void setReferer(String referer) {
  this.referer = referer;
 }
 
 public String getEntitySourceId() {
  return entitySourceId;
 }
 
 public void setEntitySourceId(String entitySourceId) {
  this.entitySourceId = entitySourceId;
 }
 
}
