/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * <p>
 * The goal of this component is to display ad choice in footer. Ad choice label is the property from the dialog which will be displayed in this
 * component. Client Id, pId and isEnabled are the properties which will be coming from global configurations.
 * 
 * </p>
 */
@Component(description = "AdChoiceViewHelperImpl", immediate = true, metatype = true, label = "Ad Choice")
@Service(value = { AdChoiceViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.AD_CHOICES, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class AdChoiceViewHelperImpl extends BaseViewHelper {
 
 /** constant for ad choice label */
 private static final String AD_CHOICE_LABEL = "label";
 
 /** ad choice field label */
 private static final String AD_CHOICE_FIELD_LABEL = "adChoicesLabel";
 
 /** The Constant GLOBAL_CONFIG_CAT. */
 private static final String GLOBAL_CONFIG_CAT = "adchoice";
 
 /** constant for ad choice client id */
 private static final String AD_CHOICE_CLIENT_ID = "clientId";
 
 /** constant for ad choice PID */
 private static final String AD_CHOICE_PID = "pId";
 
 /** constant to check if the ad choice is enabled */
 private static final String IS_ENABLED = "isEnabled";
 
 /**
  * logger object.
  **/
 private static final Logger LOGGER = LoggerFactory.getLogger(GlobalFooterViewHelperImpl.class);
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /**
  * The method processData overrides method is responsible validating author input and returning a list of sections containing rich text,text This
  * method converts the multiwidget input (a String Array) into sections of maps so that it can be accessed in JSP in consistent way.This method also
  * validates the link url provided by author.
  * 
  * @param content
  *         , - {@link Map} of all user input received as input <code>ViewHelper.onGetData</code> method
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return {@link Map} holding the data
  */
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  
  LOGGER.debug("AdChoiceViewHelperImpl #processData called for processing fixed list.");
  Map<String, Object> jsonProperties = getProperties(content);
  
  Page currentPage = getCurrentPage(resources);
  
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(getJsonNameSpace(content), getAdChoiceDetails(currentPage, jsonProperties));
  
  return data;
  
 }
 
 private Map<String, Object> getAdChoiceDetails(Page currentPage, Map<String, Object> jsonProperties) {
  
  Map<String, Object> adChoiceMap = new HashMap<String, Object>();
  String isEnabled = StringUtils.EMPTY;
  String adChoicePId = StringUtils.EMPTY;
  String adChoiceClientId = StringUtils.EMPTY;
  String adChoiceLabel = MapUtils.getString(jsonProperties, AD_CHOICE_FIELD_LABEL, StringUtils.EMPTY);
  try {
   isEnabled = configurationService.getConfigValue(currentPage, GLOBAL_CONFIG_CAT, IS_ENABLED);
   adChoicePId = configurationService.getConfigValue(currentPage, GLOBAL_CONFIG_CAT, AD_CHOICE_PID);
   adChoiceClientId = configurationService.getConfigValue(currentPage, GLOBAL_CONFIG_CAT, AD_CHOICE_CLIENT_ID);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find  key for adChoice category." + "The following keys are needed-isEnabled,adChoicePId,adChoiceClientId", e);
  }
  String enabledTrim = isEnabled.trim();
  if ("yes".equalsIgnoreCase(enabledTrim) || "true".equalsIgnoreCase(enabledTrim)) {
   adChoiceMap.put(IS_ENABLED, true);
  } else {
   adChoiceMap.put(IS_ENABLED, false);
  }
  adChoiceMap.put(AD_CHOICE_PID, adChoicePId);
  adChoiceMap.put(AD_CHOICE_CLIENT_ID, adChoiceClientId);
  adChoiceMap.put(AD_CHOICE_LABEL, adChoiceLabel);
  return adChoiceMap;
 }
}
