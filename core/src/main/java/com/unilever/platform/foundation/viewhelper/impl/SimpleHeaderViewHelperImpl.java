/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.cache.ComponentViewAndViewHelperCache;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.ImageHelper;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * <p>
 * Responsible for reading author input for CountryLanguage Selector component and generating a list of Link objects and setting in pageContext for
 * the view.
 * </p>
 */
@Component(description = "SimpleHeaderViewHelperImpl", immediate = true, metatype = true, label = "SimpleHeaderViewHelperImpl")
@Service(value = { SimpleHeaderViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.SIMPLE_HEADER, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class SimpleHeaderViewHelperImpl extends BaseViewHelper {
 
 /** The Constant MAIN_LOGO. */
 private static final String MAIN_LOGO = "mainLogo";
 
 /** The Constant ALT_TEXT. */
 private static final String ALT_TEXT = "alttext";
 
 /** The Constant FILENAME. */
 private static final String FILENAME = "fileName";
 
 /** The Constant FILE_REFERENCE. */
 private static final String FILE_REFERENCE = "fileReference";
 
 /** The Constant EXTENSION. */
 private static final String EXTENSION = "extension";
 
 /** The Constant TARGET. */
 private static final String TARGET = "target";
 
 /** The Constant IMAGE_URL. */
 private static final String IMAGE_URL = "imageUrl";
 
 /** The Constant BRAND_LOGO_IMAGE. */
 private static final String BRAND_LOGO_IMAGE = "brandLogoImage";
 
 /** The Constant BRAND_ALT_TEXT. */
 private static final String BRAND_LOGO_ALT_TEXT = "brandLogoAltText";
 
 /** The Constant EXPIRE_TIMER_NOTIFICATION. */
 private static final String EXPIRE_TIMER_NOTIFICATION = "expireTimerNotification";
 
 /** The Constant TIMER_NOTIFICATION_TEXT. */
 private static final String TIMER_NOTIFICATION_TEXT = "timerNotificationText";
 
 /** The Constant EXPIRATION_MESSAGE_TEXT. */
 private static final String EXPIRATION_MESSAGE_TEXT = "expirationMessageText";
 
 /** The Constant EXPIRATION_TIMER. */
 private static final String EXPIRATION_TIMER = "expireTimer";
 
 /** The Constant MINUTES. */
 private static final String MINUTES = "minutesLabel";
 
 /** The Constant ERROR_PAGE_URL. */
 private static final String ERROR_PAGE_URL = "errorPageUrl";
 
 /**
  * Logger for the GlobalNavigationViewHelperImpl.
  */
 private static final Logger LOGGER = LoggerFactory.getLogger(GlobalNavigationViewHelperImpl.class);
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /*
  * reference for cache object
  */
 
 /** The component view and view helper cache. */
 @Reference
 ComponentViewAndViewHelperCache componentViewAndViewHelperCache;
 
 @Override
 /*
  * (non-Javadoc)
  * 
  * @see com.sapient.platform.iea.aem.core.viewhelper.impl.DefaultBaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  LOGGER.debug("Entering #processData of Simple Header View Helper.");
  Map<String, Object> headerProperties = new LinkedHashMap<String, Object>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  
  headerProperties.put(MAIN_LOGO, getBrandLogoProperties(content, resources));
  headerProperties.put(EXPIRE_TIMER_NOTIFICATION, getExpireTimerNotification(content, resources));
  LOGGER.info("The Navigation list corresponding to Simple Header is successfully generated");
  Map<String, Object> finalData = new HashMap<String, Object>();
  finalData.put(getJsonNameSpace(content), headerProperties);
  
  return finalData;
  
 }
 
 /**
  * Gets the expire timer notification.
  * 
  * @param properties
  *         the properties
  * @param i18n
  *         the i18n
  * @return the expire timer notification
  */
 private Map<String, Object> getExpireTimerNotification(final Map<String, Object> content, final Map<String, Object> resources) {
  
  Map<String, Object> expireNotificationProperties = new LinkedHashMap<String, Object>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  Map<String, Object> properties = getProperties(content);
  String notificationText = MapUtils.getString(properties, TIMER_NOTIFICATION_TEXT, StringUtils.EMPTY);
  String messageText = MapUtils.getString(properties, EXPIRATION_MESSAGE_TEXT, StringUtils.EMPTY);
  String notificationTimer = MapUtils.getString(properties, EXPIRATION_TIMER, StringUtils.EMPTY);
  String errorPageFullUrl = StringUtils.EMPTY;
  
  try {
   String errorPagePath = configurationService.getConfigValue(getCurrentPage(resources), UnileverConstants.EGIFTING_CONFIG_CAT, "dyanamicErrorPage");
   errorPageFullUrl = ComponentUtil.getFullURL(getResourceResolver(resources), errorPagePath, getSlingRequest(resources));
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Could not find the configuration for  egifting ", e);
  }
  
  expireNotificationProperties.put(TIMER_NOTIFICATION_TEXT, notificationText);
  expireNotificationProperties.put(EXPIRATION_TIMER, notificationTimer);
  expireNotificationProperties.put(EXPIRATION_MESSAGE_TEXT, messageText);
  expireNotificationProperties.put(MINUTES, getI18n(resources).get("simpleHeader.minutesLabel"));
  expireNotificationProperties.put(ERROR_PAGE_URL, errorPageFullUrl);
  
  LOGGER.debug("Expiration Timer Notification Properties map corresponding to Simple Header is: " + expireNotificationProperties);
  return expireNotificationProperties;
 }
 
 /**
  * Gets the brand logo properties.
  * 
  * @param properties
  *         the properties
  * @param currentPage
  *         the current page
  * @param resourceResolver
  *         the resource resolver
  * @return the brand logo properties
  */
 private Map<String, Object> getBrandLogoProperties(final Map<String, Object> content, final Map<String, Object> resources) {
  
  Map<String, Object> brandLogoProperties = new LinkedHashMap<String, Object>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  Map<String, Object> properties = getProperties(content);
  Page currentPage = getCurrentPage(resources);
  ResourceResolver resourceResolver = getResourceResolver(resources);
  String brandLogoImagePath = MapUtils.getString(properties, BRAND_LOGO_IMAGE, StringUtils.EMPTY);
  brandLogoImagePath = ComponentUtil.getStaticFileFullPath(brandLogoImagePath, currentPage, getSlingRequest(resources));
  String brandLogoAltText = MapUtils.getString(properties, BRAND_LOGO_ALT_TEXT, StringUtils.EMPTY);
  brandLogoProperties.put(ALT_TEXT, brandLogoAltText);
  brandLogoProperties.put(EXTENSION, ImageHelper.getExtensionFromMimeType(ImageHelper.getFileExtension(brandLogoImagePath)));
  brandLogoProperties.put(FILENAME, ImageHelper.fetchImageFileName(brandLogoImagePath));
  brandLogoProperties.put(FILE_REFERENCE, brandLogoImagePath);
  brandLogoProperties.put(IMAGE_URL, brandLogoImagePath);
  String homePageUrl = ComponentUtil.getFullURL(resourceResolver, currentPage.getPath(), getSlingRequest(resources));
  if (StringUtils.isNotBlank(homePageUrl)) {
   homePageUrl = StringUtils.substringBefore(homePageUrl, resourceResolver.map(currentPage.getPath()));
  }
  if (StringUtils.isEmpty(homePageUrl)) {
   homePageUrl = UnileverConstants.FORWARD_SLASH;
  }
  brandLogoProperties.put(TARGET, homePageUrl);
  LOGGER.debug("Brand Logo Properties map corresponding to Simple Header is: " + brandLogoProperties);
  
  return brandLogoProperties;
 }
 
}
