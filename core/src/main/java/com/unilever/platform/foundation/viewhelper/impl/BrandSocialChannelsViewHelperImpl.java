/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.SocialTABHelper;

/**
 * The Class SocialCommunityStatsViewHelperImpl.
 */
@Component(description = "brandSocialChannels", immediate = true, metatype = true, label = "Brand Social Channels")
@Service(value = { BrandSocialChannelsViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.BRAND_SOCIAL_CHANNELS, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class BrandSocialChannelsViewHelperImpl extends SocialTabBaseViewHelperImpl {
 
 /**
  * Constant for title.
  */
 private static final String TITLE_TEXT = "titleText";
 
 /**
  * Constant for Introduction Copy.
  */
 private static final String COPY_TEXT = "copyText";
 
 /** The Constant CATEGORY. */
 private static final String SOCIAL_CHANNELS = "socialChannels";
 
 private static final String SOCIAL_CHANNEL_NAMES = "socialChannelNames";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(BrandSocialChannelsViewHelperImpl.class);
 
 /** The configuration service. */
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.impl.SocialTabBaseViewHelperImpl#processData(java.util.Map, java.util.Map)
  */
 @Reference
 ConfigurationService configurationService;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.impl.SocialTabBaseViewHelperImpl#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  
  LOGGER.info("Brand Social Channels View Helper #processData called for processing fixed list.");
  
  Map<String, Object> properties = getProperties(content);
  Page currentPage = getCurrentPage(resources);
  String jsonNameSpace = getJsonNameSpace(content);
  
  Map<String, Object> brandSocialChannelsMap = new LinkedHashMap<String, Object>();
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  List<String> channelList = new ArrayList<String>();
  
  String sectionTitle = getTitle(properties);
  String introCopy = getText(properties);
  
  brandSocialChannelsMap.put(TITLE_TEXT, sectionTitle);
  brandSocialChannelsMap.put(COPY_TEXT, introCopy);
  
  String channels = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, jsonNameSpace, SOCIAL_CHANNEL_NAMES);
  
  if (StringUtils.isNotBlank(channels)) {
   channelList = Arrays.asList(channels.split(","));
  }
  
  addSocialChannelsMap(brandSocialChannelsMap, channelList, currentPage, configurationService);
  
  data.put(jsonNameSpace, brandSocialChannelsMap);
  
  return data;
 }
 
 /**
  * Adds the social channels map.
  * 
  * @param requestServletUrlMap
  *         the request servlet url map
  * @param socialChannels
  *         the social channels
  * @param currentPage
  *         the current page
  * @param configurationService
  *         the configuration service
  */
 private static void addSocialChannelsMap(Map<String, Object> requestServletUrlMap, List<String> socialChannels, Page currentPage,
   ConfigurationService configurationService) {
  List<Map<String, Object>> socialChannelsList = new ArrayList<Map<String, Object>>();
  
  for (String channel : socialChannels) {
   socialChannelsList.add(SocialTABHelper.getSocialChannelMap(channel.trim(), currentPage, configurationService));
  }
  requestServletUrlMap.put(SOCIAL_CHANNELS, socialChannelsList.toArray());
 }
 
}
