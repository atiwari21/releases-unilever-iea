/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sapient.platform.iea.aem.core.cache.ComponentViewAndViewHelperCache;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * <p>
 * Responsible for reading author input for Dynamic Error Message component and displaying dynamic error message along with the title and link in
 * pageContext for the view.
 * </p>
 */
@Component(description = "ErrorPageViewHelperImpl", immediate = true, metatype = true, label = "ErrorPageViewHelperImpl")
@Service(value = { DynamicErrorMessageViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.DYNAMIC_ERROR_MESSAGE, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class DynamicErrorMessageViewHelperImpl extends BaseViewHelper {
 
 /** The Constant ERROR_MESSAGE_TITLE. */
 private static final String ERROR_MESSAGE_TITLE = "errorTitle";
 
 /** The Constant ERROR_MESSAGE_LINK_LABEL. */
 private static final String ERROR_MESSAGE_LINK_LABEL = "linkLabel";
 
 /** The Constant ERROR_MESSAGE_LINK_URL. */
 private static final String ERROR_MESSAGE_LINK_URL = "linkUrl";
 
 /** The Constant DYNAMIC_ERROR_MESSAGE. */
 private static final String DYNAMIC_ERROR_MESSAGE = "dynamicErrorMessage";
 
 /**
  * Logger for the DynamicErrorMessageViewHelperImpl.
  */
 private static final Logger LOGGER = LoggerFactory.getLogger(DynamicErrorMessageViewHelperImpl.class);
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /*
  * reference for cache object
  */
 
 /** The component view and view helper cache. */
 @Reference
 ComponentViewAndViewHelperCache componentViewAndViewHelperCache;
 
 @Override
 /*
  * (non-Javadoc)
  * 
  * @see com.sapient.platform.iea.aem.core.viewhelper.impl.DefaultBaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @SuppressWarnings("unchecked")
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  LOGGER.debug("Entering #processData of Dynamic Error Message View Helper.");
  Map<String, Object> properties = (Map<String, Object>) content.get(PROPERTIES);
  SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) resources.get(ProductConstants.REQUEST);
  ResourceResolver resourceResolver = (ResourceResolver) resources.get(ProductConstants.RESOURCE_RESOLVER);
  Map<String, Object> errorMessageProperties = new LinkedHashMap<String, Object>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  
  errorMessageProperties.put(DYNAMIC_ERROR_MESSAGE, slingRequest.getParameter("errorMsg"));
  
  String errorTitle = MapUtils.getString(properties, ERROR_MESSAGE_TITLE, StringUtils.EMPTY);
  String linkLabel = MapUtils.getString(properties, ERROR_MESSAGE_LINK_LABEL, StringUtils.EMPTY);
  String linkUrl = MapUtils.getString(properties, ERROR_MESSAGE_LINK_URL, StringUtils.EMPTY);
  
  errorMessageProperties.put(UnileverConstants.TITLE, errorTitle);
  errorMessageProperties.put(UnileverConstants.LABEL, linkLabel);
  errorMessageProperties.put(UnileverConstants.URL, ComponentUtil.getFullURL(resourceResolver, linkUrl, slingRequest));
  
  String jsonNameSpace = MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE);
  
  Map<String, Object> finalData = new HashMap<String, Object>();
  finalData.put(jsonNameSpace, errorMessageProperties);
  
  return finalData;
 }
 
}
