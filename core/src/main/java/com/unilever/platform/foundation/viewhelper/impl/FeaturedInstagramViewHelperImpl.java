/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.SocialShareHelper;
import com.unilever.platform.foundation.components.helper.SocialTABHelper;
import com.unilever.platform.foundation.constants.CommonConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * <p>
 * Responsible for Fetching Image and Caption related to the Instagram.
 * 
 * </p>
 */
@Component(description = "FeaturedInstagramViewHelperImpl", immediate = true, metatype = true, label = "Featured Instagram Component")
@Service(value = { FeaturedInstagramViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.FEATURED_INSTAGRAM, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class FeaturedInstagramViewHelperImpl extends BaseViewHelper {
 
 /** The Constant CTA_URL. */
 private static final String CTA_URL = "ctaUrl";
 
 /** Constant for instagramImage Featured Instagram. */
 private static final String INSTAGRAM_IMAGE = "instagramImage";
 
 /** The Constant SOCIAL_SHARING. */
 private static final String SOCIAL_SHARING = "socialSharing";
 
 /** The Constant CTA_LABEL. */
 private static final String CTA_LABEL = "ctaLabel";
 
 /** The Constant CTA_LABEL_KEY. */
 private static final String CTA_LABEL_KEY = "featuredInstagramPostTab.ctaLabel";
 
 /** The Constant READ_LESS_TEXT. */
 private static final String READ_LESS_TEXT = "featuredInstagramPostTab.readLessLabelText";
 
 /** The Constant READ_MORE_TEXT. */
 private static final String READ_MORE_TEXT = "featuredInstagramPostTab.readMoreLabelText";
 
 /** The Constant OPEN_IN_NEW_WINDOW. */
 public static final String OPEN_IN_NEW_WINDOW = "openInNewWindow";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(FeaturedInstagramViewHelperImpl.class);
 
 @Reference
 ConfigurationService configurationService;
 
 /**
  * The method processData overrides method is responsible validating author input and returning a map.
  * 
  * @param content
  *         {@link Map} of all user input received as input
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return Map holding the result data.
  */
 
 @SuppressWarnings("unchecked")
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  
  LOGGER.info("Featured Instagram View Helper #processData called for processing fixed list.");
  Map<String, Object> properties = getProperties(content);
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  Map<String, Object> ctaMap = new LinkedHashMap<String, Object>();
  
  ResourceResolver resourceResolver = getResourceResolver(resources);
  String jsonNameSpace = getJsonNameSpace(content);
  I18n i18n = getI18n(resources);
  Page currentPage = getCurrentPage(resources);
  
  String ctaLabel = i18n.get(CTA_LABEL_KEY);
  String readMoreText = i18n.get(READ_MORE_TEXT);
  String readLessText = i18n.get(READ_LESS_TEXT);
  String newWindow = MapUtils.getString(properties, OPEN_IN_NEW_WINDOW, StringUtils.EMPTY);
  String socialSharing = MapUtils.getString(properties, SOCIAL_SHARING, StringUtils.EMPTY);
  String isSocialSharing = socialSharing != null && ("true".equals(socialSharing) || "yes".equals(socialSharing)) ? CommonConstants.BOOLEAN_TRUE
    : ProductConstants.FALSE;
  
  String imgPath = MapUtils.getString(properties, INSTAGRAM_IMAGE, StringUtils.EMPTY);
  Map<String, Object> featuredInstagramImageMap = SocialTABHelper.getImageMapWithMetadata(imgPath, currentPage, resourceResolver,
    getSlingRequest(resources));
  String openInNewWindow = newWindow != null && "true".equals(newWindow) ? CommonConstants.BOOLEAN_TRUE : ProductConstants.FALSE;
  
  featuredInstagramImageMap.put("isSocialSharing", Boolean.valueOf(isSocialSharing));
  if (Boolean.valueOf(isSocialSharing)) {
   featuredInstagramImageMap.put(SOCIAL_SHARING, SocialShareHelper.addThisWidget(resources, configurationService));
  }
  
  ctaMap.put(CTA_LABEL, ctaLabel);
  ctaMap.put(CTA_URL, MapUtils.getString(properties, CTA_URL, StringUtils.EMPTY));
  ctaMap.put(OPEN_IN_NEW_WINDOW, Boolean.valueOf(openInNewWindow));
  featuredInstagramImageMap.put("cta", ctaMap);
  featuredInstagramImageMap.put("isSocialSharing", Boolean.valueOf(isSocialSharing));
  featuredInstagramImageMap.put("readMoreText", readMoreText);
  featuredInstagramImageMap.put("readLessText", readLessText);
  
  data.put(jsonNameSpace, featuredInstagramImageMap);
  return data;
 }
}
