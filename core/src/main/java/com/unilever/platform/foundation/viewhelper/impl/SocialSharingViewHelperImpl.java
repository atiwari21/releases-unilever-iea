/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.sling.api.SlingConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.SocialShareHelper;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * This class prepares the data map for Social Sharing.
 */
@Component(description = "SocialSharing", immediate = true, metatype = true, label = "SocialSharingViewHelperImpl")
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.SOCIAL_SHARING, propertyPrivate = true), })
public class SocialSharingViewHelperImpl extends BaseViewHelper {
 
 /** The LOGGER instance. */
 private static final Logger LOGGER = LoggerFactory.getLogger(SocialSharingViewHelperImpl.class);
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 /*
  * (Javadoc) This method calls the static method addThisWidget od the SocialShareHelper class. It recieves the social media details in a map and
  * forwards it as json
  * 
  * Confluence page- https://tools.sapient.com/confluence/display/UU/CP-29-A-Social-Sharing
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  Map<String, Object> properties = new HashMap<String, Object>();
  Map<String, Object> pageProperties = getProperties(content);
  LOGGER.debug("Executing SocialSharingViewHelperImpl. Inside processData method");
  Map<String, Object> data = new HashMap<String, Object>();
  boolean overrideGlobalConfig = MapUtils.getBooleanValue(pageProperties, "overrideGlobalConfig", false);
  if (overrideGlobalConfig) {
   properties = SocialShareHelper.addThisWidget(resources, configurationService, true, pageProperties);
   properties.put("prefix", MapUtils.getString(pageProperties, "prefixText", StringUtils.EMPTY));
  } else {
   properties = SocialShareHelper.addThisWidget(resources, configurationService);
  }
  LOGGER.info("Configured widget Properties: " + properties);
  data.put(MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE), properties);
  return data;
  
 }
 
}
