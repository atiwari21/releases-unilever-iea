/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.constants;

/**
 * This class holds all the constants that are used in the form elements.
 * 
 */
public final class FormElementConstants {
 
 /**
  * Constant for constraint message.
  */
 public static final String CONSTRAINT_MESSAGE = "constraintMessage";
 
 /** The Constant CONSTRAINT_MSG. */
 public static final String CONSTRAINT_MSG = "msg";
 /**
  * Constant for constraint regex pattern.
  */
 public static final String CONSTRAINT_REGEX = "pattern";
 /**
  * Constant for holding the radio keys.
  */
 public static final String DATA_RADIO_KEY = "dataKeyRadio";
 
 /**
  * Constant for holding the radio values.
  */
 public static final String DATA_RADIO_VALUE = "dataValueRadio";
 
 /**
  * Constant for holding the dropdown keys.
  */
 public static final String DATA_SELECTION_KEY = "dataKey";
 
 /**
  * Constant for holding the dropdown values.
  */
 public static final String DATA_SELECTION_VALUE = "dataValue";
 
 /**
  * Constant for holding date format of date element.
  */
 public static final String DATE_FORMAT = "dateFormat";
 
 /**
  * Constant for default values of elements like textfield, radio and drop down.
  */
 public static final String DEFAULT_VALUE = "defaultValue";
 /**
  * Identifier for holding dropdown options map.
  */
 public static final String DROPDOWN_OPTIONS = "dropdownOptions";
 
 /**
  * Constant for form element description.
  */
 public static final String ELEMENT_DESCRIPTION = "description";
 
 /**
  * Constant for form element id.
  */
 public static final String ELEMENT_ID = "name";
 /**
  * Constant for form element label/title.
  */
 public static final String ELEMENT_LABEL = "label";
 /**
  * Constant for hide title identifier.
  */
 public static final String HIDE_TITLE = "hideTitle";
 /**
  * Constant for checking if multiple items can be selected form the drop down list.
  */
 public static final String MULTI_SELECT = "multiSelection";
 /**
  * Constant for textfield multi value.
  */
 public static final String MULTIVALUE = "multivalue";
 /**
  * Constant for holding the dynamic load path option for dropdown and radio button form elements.
  */
 public static final String OPTIONS_LOAD_PATH = "optionsLoadPath";
 /**
  * Identifier for radio options.
  */
 public static final String RADIO_OPTIONS = "radioOptions";
 /**
  * Constant for checking if the field is andatory or not.
  */
 public static final String REQUIRED_FIELD = "required";
 
 /**
  * Constant for required field message.
  */
 public static final String REQUIRED_MESSAGE = "msg";
 
 public static final String RULES = "rules";
 
 /**
  * Constant for type of form element.
  */
 public static final String VIEW_TYPE = "viewType";
 
 /**
  * Constant for checking if the help text is required or not.
  */
 public static final String SHOW_HELP_TEXT = "showHelpText";
 
 public static final String SHOW_HELP_IMAGE = "showHelpImage";
 
 public static final String HELP_IMAGE = "helpImage";
 
 public static final String CHECKED = "checked";
 
 public static final String OPEN_IN_PARENT_WINDOW = "openInParentWindow";
 
 public static final String CHECKBOX_OPTIONS = "checkboxOptions";
 
 public static final String DATA_CHECKBOX_KEY = "dataKeyMultiCheckBox";
 
 public static final String DATA_CHECKBOX_VALUE = "dataValueMultiCheckBox";
 
 public static final String CHECKED_INDEX = "checkedIndex";
 
 /**
  * Private Constructor.
  */
 private FormElementConstants() {
  
 }
 
}
