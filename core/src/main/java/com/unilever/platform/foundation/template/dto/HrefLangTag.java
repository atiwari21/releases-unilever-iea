/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.template.dto;

/**
 * The Class HrefLangTag.
 */
public class HrefLangTag {
 
 /** The title. */
 private String title;
 
 /** The href lang. */
 private String hrefLang;
 
 /** The url. */
 private String url;
 
 /**
  * Gets the title.
  * 
  * @return the title
  */
 public String getTitle() {
  return title;
 }
 
 /**
  * Sets the title.
  * 
  * @param title
  *         the new title
  */
 public void setTitle(String title) {
  this.title = title;
 }
 
 /**
  * Gets the href lang.
  * 
  * @return the href lang
  */
 public String getHrefLang() {
  return hrefLang;
 }
 
 /**
  * Sets the href lang.
  * 
  * @param hrefLang
  *         the new href lang
  */
 public void setHrefLang(String hrefLang) {
  this.hrefLang = hrefLang;
 }
 
 /**
  * Gets the url.
  * 
  * @return the url
  */
 public String getUrl() {
  return url;
 }
 
 /**
  * Sets the url.
  * 
  * @param url
  *         the new url
  */
 public void setUrl(String url) {
  this.url = url;
 }
 
}
