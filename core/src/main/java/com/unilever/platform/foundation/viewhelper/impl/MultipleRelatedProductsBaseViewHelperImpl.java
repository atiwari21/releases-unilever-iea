/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.api.Product;
import com.adobe.cq.commerce.common.CommerceHelper;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.aem.foundation.core.dto.SearchDTO;
import com.unilever.platform.aem.foundation.core.dto.TaggingSearchCriteria;
import com.unilever.platform.aem.foundation.core.service.SearchParam;
import com.unilever.platform.aem.foundation.core.service.SearchService;
import com.unilever.platform.aem.foundation.utils.AdaptiveUtil;
import com.unilever.platform.foundation.components.SearchResultComparator;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.ProductBundlingToBinUtil;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.constants.CommonConstants;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.kritique.config.KritiqueReviewsCommentsService;
import com.unilever.platform.foundation.template.dto.KritiqueDTO;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.MultipleRelatedProductsConstants;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * <p>
 * Responsible for reading author input for multipleRelatedProductsAlternate component and get all the details of products which are tagged to the
 * current page and generate a product list. Also read the site configuration for maximum number configured for the product list.
 * </p>
 * .
 */
@Component(description = "MultipleRelatedProductsBaseViewHelperImpl",
immediate = true, metatype = true, label = "MultipleRelatedProductsBaseViewHelperImpl")
@Service(value = { MultipleRelatedProductsBaseViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.MULTIPLE_RELATED_PRODUCT_BASE, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING,
  intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class MultipleRelatedProductsBaseViewHelperImpl extends BaseViewHelper {
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(MultipleRelatedProductsBaseViewHelperImpl.class);
 /** The kritique service. */
 @Reference
 KritiqueReviewsCommentsService kritiqueReviewsCommentsService;
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The search service. */
 @Reference
 SearchService searchService;
 
 /**
  * The method processData overrides method is responsible validating author input and returning a list of products, which are tagged to the current
  * page and maximum number for the links from site configuration to be displayed on the page.
  * 
  * @param content
  *         {@link Map} of all user input received as input
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return Map holding the result data.
  */
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  LOGGER.debug("Executing MultipleRelatedProductsBaseViewHelperImpl.java. Inside processData method");
  Map<String, Object> data = new HashMap<String, Object>();
  String gloablConfigCatName = MapUtils.getString(getProperties(content), UnileverConstants.GLOBAL_CONFIGURATION_CATEGORY_NAME);
  LOGGER.debug("Global Configuration category for MultipleRelatedProductsBaseViewHelperImpl = " + gloablConfigCatName);
  String jsonNameSpace = MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE);
  Map<String, String> configMap = ComponentUtil.getConfigMap(getCurrentPage(resources), gloablConfigCatName);
  
  LOGGER.debug("MultipleRelatedProductsBaseViewHelperImpl configMap = " + configMap);
  List<SearchDTO> productsResultList = getProductPagePathList(resources, configMap, getProperties(content));
  LOGGER.debug("MultipleRelatedProductsBaseViewHelperImpl productsResultList = " + productsResultList);
  List<String> productsPagePathList = getSortedFinalListBasedOnComparator(resources, productsResultList, configMap, getProperties(content));
  productsPagePathList = ProductBundlingToBinUtil.getProductsListForMultipleBuy(productsPagePathList,
          getCurrentPage(resources), configurationService);
  LOGGER.debug("MultipleRelatedProductsBaseViewHelperImpl productsPagePathList after sorting = " + productsPagePathList);
  List<Map<String, Object>> list = ProductHelper.getProductList(productsPagePathList, resources, jsonNameSpace);
  Map<String, Object> productMap = getProductMap(resources, productsPagePathList, getProperties(content), list);
  String randomNumber = MapUtils.getString(getProperties(content), UnileverConstants.RANDOM_NUMBER, StringUtils.EMPTY);
  if(StringUtils.isNotBlank(randomNumber)){
   ProductBundlingToBinUtil.setMultiBuyProductsConfigProperties(getProperties(content), productMap, configMap,
          MapUtils.getBooleanValue(getProperties(content), UnileverConstants.OVERRIDE_GLOBAL_CONFIG, false),
          configurationService, getCurrentPage(resources));
   ProductBundlingToBinUtil.setMultiBuyProductsCtaLabels(resources, productMap);
   Map<String,String> shopNowProperties = ProductHelper.getshopNowProperties(configurationService, getCurrentPage(resources),
           getI18n(resources), getResourceResolver(resources), getSlingRequest(resources));
   productMap.put(ProductConstants.SHOPNOW, shopNowProperties);
  }
  data.put(jsonNameSpace, productMap);
  return data;
 }
 
 /**
  * Gets the product map.
  * 
  * @param productsPagePathList
  * @param currentPage
  * @param slingRequest
  *         the sling request
  * @param properties
  *         the properties
  * @param configMap
  *         the config map
  * @param list
  *         the list
  * @param pageManager
  * 
  * @return the product map
  */
 protected Map<String, Object> getProductMap(final Map<String, Object> resources, List<String> productsPagePathList, Map<String, Object> properties,
   List<Map<String, Object>> list) {
  Map<String, Object> productMap = new HashMap<String, Object>();
  
  getAdaptiveRating(resources, productsPagePathList, list, productMap);
  
  Map<String, String> staticMap = new HashMap<String, String>();
  staticMap.put(ProductConstants.PRODUCT_COUNT_LABEL, getI18n(resources).get(ProductConstants.PRODUCT_COUNT_LABEL));
  productMap.put(ProductConstants.REVIEW_MAP, ProductHelper.getReviewMapProperties(configurationService, getCurrentPage(resources)));
  String title = properties.get(MultipleRelatedProductsConstants.TITLE) != null ? (String) properties.get(MultipleRelatedProductsConstants.TITLE)
    : StringUtils.EMPTY;
  productMap.put(MultipleRelatedProductsConstants.TITLE, title);
  productMap.put(ProductConstants.STATIC_MAP, staticMap);
  Map<String, String> anchorLinkMap = ComponentUtil.getAnchorLinkMap(properties);
  productMap.put(UnileverConstants.ANCHOR_NAVIGATION, anchorLinkMap);
  return productMap;
 }
 
 /**
  * @param productsPagePathList
  * @param pageManager
  * @param currentPage
  * @param slingRequest
  * @param list
  * @param productMap
  */
 private void getAdaptiveRating(final Map<String, Object> resources, List<String> productsPagePathList, List<Map<String, Object>> list,
   Map<String, Object> productMap) {
  List<Map<String, Object>> productRatingPropertiesList = new ArrayList<Map<String, Object>>();
  if (AdaptiveUtil.isAdaptive(getSlingRequest(resources))) {
   
   String prodids = StringUtils.EMPTY;
   KritiqueDTO kritiqueparams = new KritiqueDTO(getCurrentPage(resources), configurationService);
   LOGGER.info("Feature phone view called and kritique api is going to be called");
   for (String pagePath : productsPagePathList) {
    
    Page productPage = getPageManager(resources).getPage(pagePath);
    UNIProduct product = null;
    Product pageProduct = CommerceHelper.findCurrentProduct(productPage);
    if (pageProduct instanceof UNIProduct) {
     product = (UNIProduct) pageProduct;
    }
    
    prodids = setProductId(prodids, product);
   }
   if(prodids != null){
    kritiqueparams.setSiteProductId(prodids);
   }
   
   List<Map<String, Object>> kqresponse = kritiqueReviewsCommentsService.getRatingsService(kritiqueparams, configurationService);
   LOGGER.debug("Kritique response generated and size of the review list is " + kqresponse.size());
   productRatingPropertiesList = ComponentUtil.mapKritiqueProductRating(kqresponse, list);
   productMap.put(ProductConstants.PRODUCT_LIST_MAP, productRatingPropertiesList.toArray());
   
  } else {
   productMap.put(ProductConstants.PRODUCT_LIST_MAP, list.toArray());
  }
    }
    
    /**
     * sets product id to kritiqueservice
     * 
     * @param prodids
     * @param product
     * @param kritiqueparams
     */
 private String setProductId(String prodids, UNIProduct product) {
  String productIds = prodids;
  if (product != null) {
   productIds += product.getSKU() + ",";
        }
  return productIds;
    }
 
 /**
  * Gets the sorted final list based on comparator.
  * 
  * @param productsResultList
  *         the products result list
  * @param configMap
  *         the config map
  * @param currentPage
  *         the current page
  * @param tagManager
  * @return the sorted final list based on comparator
  */
 protected List<String> getSortedFinalListBasedOnComparator(final Map<String, Object> resources, List<SearchDTO> productsResultList,
   Map<String, String> configMap, Map<String, Object> properties) {
  int max = CommonConstants.FOUR;
  try {
   max = StringUtils.isBlank(configMap.get(UnileverConstants.MAX)) ? CommonConstants.FOUR : Integer.parseInt(configMap.get(UnileverConstants.MAX));
  } catch (NumberFormatException nfe) {
   LOGGER.error("Maximum article for page list must be number", nfe);
  }
  String[] includeSortList = getIncludeExcludeList(configMap, MultipleRelatedProductsConstants.INCLUDE_SORT_LIST, true);
  String[] exclude = getIncludeExcludeList(configMap, UnileverConstants.EXCLUDE_LIST, true);
  List<String> includeTagIdList = getIncludeTagIdList(resources, includeSortList, exclude);
  LOGGER.debug("Maximum articles for page list= " + max);
  String[] orderbyTagIdArr = getIncludeExcludeList(configMap, MultipleRelatedProductsConstants.ORDER_BY, false);
  String includeSegmentRulesProp = MapUtils.getString(properties, "includeSegmentRules", StringUtils.EMPTY);
  boolean includeSegmentRules = ("true").equalsIgnoreCase(includeSegmentRulesProp) ? true : false;
  String[] featuredTags = ComponentUtil.getUpdatedIncludedArrayFromResources(resources, orderbyTagIdArr, includeSegmentRules);
  long startTime = System.currentTimeMillis();
  
  Collections.sort(productsResultList, new SearchResultComparator(includeTagIdList, featuredTags, true));
  
  long stopTime = System.currentTimeMillis();
  
  LOGGER.info("Time elapsed in soring the result of MultipleRelatedArticles = " + (stopTime - startTime));
  List<SearchDTO> productsResultListFinal = productsResultList.size() > max ? productsResultList.subList(0, max) : productsResultList;
  return getPathList(productsResultListFinal);
 }
 
 /**
  * Gets the product page path list.
  * 
  * @param currentPage
  *         the current page
  * @param configMap
  *         the config map
  * @param properties
  * @param tagManager
  * @param filterTagsNamespace
  *         the filter tags namespace
  * @return the product page path list
  */
 protected List<SearchDTO> getProductPagePathList(final Map<String, Object> resources,
         Map<String, String> configMap, Map<String, Object> properties) {
  LOGGER.debug("Inside getProductPagePathList of MultipleRelatedProductsBaseViewHelperImpl");
  int offset = 0, limit = CommonConstants.TWENTY;
  Page currentPage = getCurrentPage(resources);
  try {
   limit = StringUtils.isBlank(configMap.get(UnileverConstants.LIMIT)) ? CommonConstants.TWENTY : Integer.parseInt(configMap
     .get(UnileverConstants.LIMIT));
  } catch (NumberFormatException nfe) {
   LOGGER.error("limit for page list must be number ", nfe);
  }
  Map<String, List<String>> propMap = new HashMap<String, List<String>>();
  List<String> propValueList = new ArrayList<String>();
  propValueList.add(UnileverConstants.PRODUCT);
  propMap.put(UnileverConstants.JCR_CONTENT_CONTENT_TYPE, propValueList);
  String[] include = getIncludeExcludeList(configMap, UnileverConstants.INCLUDE_LIST, true);
  String[] exclude = getIncludeExcludeList(configMap, UnileverConstants.EXCLUDE_LIST, true);
  
  String[] filters = ComponentUtil.getFilterTagsNamespace(properties, configMap);
  LOGGER.debug("MultipleRelatedProductsBaseViewHelperImpl filters " + filters);
  List<TaggingSearchCriteria> filteredTagList = ComponentUtil
    .getFilterTagList(currentPage, filters, getIncludeTagIdList(resources, include, exclude));
  LOGGER.debug("MultipleRelatedProductsBaseViewHelperImpl filteredTagList " + filteredTagList);
  
  String currentPagePath = currentPage.getPath();
  if (filteredTagList != null && !filteredTagList.isEmpty()) {
   return getSearchService().getMultiTagResults(filteredTagList, getExcludeTagIdList(currentPage, exclude), currentPagePath, propMap,
     new SearchParam(offset, limit, UnileverConstants.DESCENDING), getResourceResolver(resources));
  } else {
   return new ArrayList<SearchDTO>();
  }
    }
    
    /**
     * gets string array from global config
     * 
     * @param configMap
     * @param key
     * @return
     */
 private String[] getIncludeExcludeList(Map<String, String> configMap, String key, boolean nullBased) {
  String[] stringArray = new String[] {};
  Map<String, String> localConfigMap = configMap;
  String localKey = key;
  if (nullBased) {
   stringArray = !StringUtils.isBlank(localConfigMap.get(localKey)) ? localConfigMap.get(localKey).split(CommonConstants.COMMA_CHAR)
                    : new String[] {};
        } else if (!nullBased) {
   stringArray = StringUtils.isNotBlank(localConfigMap.get(localKey)) ? localConfigMap.get(localKey).split(CommonConstants.COMMA_CHAR)
                    : new String[] {};
        }
  return stringArray;
    }

 /**
  * Gets the path list.
  * 
  * @param list
  *         the list
  * @return the path list
  */
 protected List<String> getPathList(List<SearchDTO> list) {
  List<String> pathList = new ArrayList<String>();
  for (SearchDTO obj : list) {
   pathList.add(obj.getResultNodePath());
  }
  return pathList;
 }
 
 /**
  * Gets the include tag id list.
  * 
  * @param page
  *         the page
  * @param includeParentTagIds
  *         the include parent tag ids
  * @param exclude
  *         the exclude
  * @param tagManager
  * @return the include tag id list
  */
 protected List<String> getIncludeTagIdList(final Map<String, Object> resources, String[] includeParentTagIds, String[] exclude) {
  List<String> filterTagList = new ArrayList<String>();
  List<String> excludeList = Arrays.asList(exclude);
  for (String includeParentTagId : includeParentTagIds) {
   filterTagList = getIncludeFilterTags(filterTagList, excludeList, includeParentTagId, getTagManager(resources));
  }
  List<String> pageTagIdList = getExtractedIncludeExcludeList(getCurrentPage(resources), filterTagList);
  LOGGER.debug("Valid Tag Id List, which are to be in the include list = " + pageTagIdList);
  return pageTagIdList;
 }
 
 /**
  * Gets the include filter tags.
  * 
  * @param filterTagList
  *         the filter tag list
  * @param excludeList
  *         the exclude list
  * @param includeParentTagId
  *         the include parent tag id
  * @param tagManager
  * @return the include filter tags
  */
 protected List<String> getIncludeFilterTags(List<String> filterTagList, List<String> excludeList, String includeParentTagId, TagManager tagManager) {
  Tag includeParentTag = tagManager.resolve(includeParentTagId);
  if (includeParentTag != null) {
   Iterator<Tag> childItr = includeParentTag.listChildren();
   while (childItr.hasNext()) {
    Tag tag = childItr.next();
    setFilterTagList(filterTagList, excludeList, tag);
   }
  }
  return filterTagList;
    }
    
    /**
     * sets filter tag list
     * 
     * @param filterTagList
     * @param excludeList
     * @param tag
     */
 private void setFilterTagList(List<String> filterTagList, List<String> excludeList, Tag tag) {
  if (tag != null) {
   String childTagId = tag.getTagID();
   if (getValidToShow(childTagId, null, excludeList)) {
    filterTagList.add(childTagId);
            }
        }
    }

 /**
  * Gets the exclude tag id list.
  * 
  * @param page
  *         the page
  * @param filterTagIds
  *         the filter tag ids
  * @return the exclude tag id list
  */
 protected List<String> getExcludeTagIdList(Page page, String[] filterTagIds) {
  List<String> pageTagIdList = getExtractedIncludeExcludeList(page, Arrays.asList(filterTagIds));
  LOGGER.debug("Valid Tag Id List, which are to be in the exclude list = " + pageTagIdList);
  return pageTagIdList;
    }
    
    /**
     * gets page tag id list
     * 
     * @param page
     * @param filterTagList
     * @return
     */
 private List<String> getExtractedIncludeExcludeList(Page page, List<String> filterTagList) {
  List<String> localFilterTagList = filterTagList;
  List<String> pageTagIdList = new ArrayList<String>();
  Tag[] pageTagList = page.getTags();
  if (pageTagList != null) {
   for (Tag tag : pageTagList) {
    setPageTagIdList(localFilterTagList, pageTagIdList, tag);
       }
      }
  return pageTagIdList;
    }
    
    /**
     * sets page tag id list
     * 
     * @param localFilterTagList
     * @param pageTagIdList
     * @param tag
     */
 private void setPageTagIdList(List<String> localFilterTagList, List<String> pageTagIdList, Tag tag) {
  if (tag != null) {
   String tagId = tag.getTagID();
   if (isValidToShow(tagId, localFilterTagList)) {
    pageTagIdList.add(tagId);
            }
        }
    }

 /**
  * This method checks if the tagId is child of any Tag, which are in the include list
  * 
  * @param tagId
  *         the tag id
  * @param includeList
  *         the include list
  * @return true, if is valid to show
  */
 protected boolean isValidToShow(String tagId, List<String> includeList) {
  if (includeList != null) {
   Iterator<String> itr = includeList.iterator();
   while (itr.hasNext()) {
    String parentTagId = itr.next();
    if(getValidToShow(tagId, parentTagId, null)){
     return true;
    }
   }
  }
  return false;
    }
    
    /**
     * returns true if tag valid to be shown
     * 
     * @param tagId
     * @param parentTagId
     * @param validToShow
     * @return
     */
 private boolean getValidToShow(String tagId, String parentTagId, List<String> list) {
  boolean isValidToShow = false;
  if (!StringUtils.isBlank(tagId) && StringUtils.isNotBlank(parentTagId) && tagId.startsWith(parentTagId) && list == null) {
   isValidToShow = true;
        } else if (StringUtils.isNotBlank(tagId) && list != null && !list.contains(tagId)) {
   isValidToShow = true;
        }
  return isValidToShow;
    }

 /**
  * Gets the search service.
  * 
  * @return the search service
  */
 protected SearchService getSearchService() {
  
  return this.searchService;
 }
 
 /**
  * Gets the config service.
  * 
  * @return the config service
  */
 protected ConfigurationService getConfigService() {
  
  return this.configurationService;
 }
 
}
