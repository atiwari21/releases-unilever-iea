/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.Collections;

import javax.jcr.Session;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.jcr.resource.JcrResourceConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.workflow.exec.WorkflowData;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.metadata.MetaDataMap;
import com.day.cq.workflow.WorkflowException;

/**
 * The Class ConfigPageWorkflowProcessHelperImpl.
 */
@Component
@Service
@Properties({ @Property(name = "ServiceDescription", value = "Unilever Config Page Workflow Process implementation."),
  @Property(name = "ServiceVendor", value = "Adobe"), @Property(name = "process.label", value = "Unilever Config Page Approval Workflow Process") })
public class ConfigPageWorkflowProcessHelperImpl implements WorkflowProcess {
 
 /** The Constant RETURN_VALUETO_ECMA_SCRIPT. */
 private static final String RETURN_VALUETO_ECMA_SCRIPT = "returnValuetoEcmaScript";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ConfigPageWorkflowProcessHelperImpl.class);
 
 /** The resource resolver factory. */
 @Reference
 private ResourceResolverFactory resourceResolverFactory;
 /**
  * Constant for WORKFLOW_TEMPLATE.
  */
 private static final String WORKFLOW_TEMPLATE = "/apps/unilever-aem/templates/platformConfigTemplate";
 
 /** The Constant WORKFLOW_NOT_APPLICABLE. */
 private static final String WORKFLOW_NOT_APPLICABLE = "This workflow is not applicable to the payload template :  ";
 
 /** The Constant WORKFLOW_TEMPLATE_USED. */
 private static final String WORKFLOW_TEMPLATE_USED = "Template used for this payload is : ";
 
 /** The Constant JCR_PATH_NOT_FOUND. */
 private static final String JCR_PATH_NOT_FOUND = "No JCR path is found";
 
 /** The Constant ERR_GETTING_RESOURCE. */
 private static final String ERR_GETTING_RESOURCE = "Error in getting resource resolver";
 
 /** The Constant TEMPLATE. */
 private static final String TEMPLATE = com.day.cq.wcm.api.NameConstants.NN_TEMPLATE;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.day.cq.workflow.exec.WorkflowProcess#execute(com.day.cq.workflow.exec.WorkItem, com.day.cq.workflow.WorkflowSession,
  * com.day.cq.workflow.metadata.MetaDataMap)
  */
 @Override
 public void execute(WorkItem workItem, WorkflowSession workflowSession, MetaDataMap metaDataMap) throws WorkflowException {
  
  ResourceResolver resourceResolver = null;
  try {
   resourceResolver = getResourceResolver(workflowSession.getSession());
  } catch (Exception e) {
   LOGGER.error(ERR_GETTING_RESOURCE, e);
  }
  WorkflowData workflowData = workItem.getWorkflowData();
  
  if ("JCR_PATH".equals(workflowData.getPayloadType()) && null != resourceResolver) {
   String path = workflowData.getPayload().toString();
   Resource currentResource = resourceResolver.getResource(path);
   PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
   Page page = pageManager.getContainingPage(currentResource);
   
   if (page.getProperties().get(TEMPLATE, "").equalsIgnoreCase(WORKFLOW_TEMPLATE)) {
    LOGGER.info(WORKFLOW_TEMPLATE_USED + page.getProperties().get(TEMPLATE, ""));
    workItem.getWorkflow().getMetaDataMap().put(RETURN_VALUETO_ECMA_SCRIPT, "true");
    workflowData.getMetaDataMap().put(RETURN_VALUETO_ECMA_SCRIPT, "true");
    
   } else {
    LOGGER.info(WORKFLOW_NOT_APPLICABLE + page.getProperties().get(TEMPLATE, ""));
    workItem.getWorkflow().getMetaDataMap().put(RETURN_VALUETO_ECMA_SCRIPT, "false");
    workflowData.getMetaDataMap().put(RETURN_VALUETO_ECMA_SCRIPT, "false");
   }
  } else {
   LOGGER.info(JCR_PATH_NOT_FOUND);
  }
  
 }
 
 /**
  * Gets the resource resolver.
  * 
  * @param session
  *         the session
  * @return the resource resolver
  * @throws LoginException
  *          the login exception
  */
 private ResourceResolver getResourceResolver(Session session) throws LoginException {
  return resourceResolverFactory.getResourceResolver(Collections.<String, Object> singletonMap(JcrResourceConstants.AUTHENTICATION_INFO_SESSION,
    session));
 }
 
}
