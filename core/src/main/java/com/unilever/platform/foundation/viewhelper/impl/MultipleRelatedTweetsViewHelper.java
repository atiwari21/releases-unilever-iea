/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.LinkedHashMap;
import java.util.Map;

import com.day.cq.i18n.I18n;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.SocialTABHelper;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.SocialTabQueryDTO;

/**
 * The Class MultipleRelatedTweetsViewHelper.
 */
@Component(description = "MultipleRelatedTweetsViewHelper", immediate = true, metatype = true, label = "MultipleRelatedTweetsViewHelper")
@Service(value = { MultipleRelatedTweetsViewHelper.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.MULTIPLE_RELATED_TWEETS, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class MultipleRelatedTweetsViewHelper extends SocialTabBaseViewHelperImpl {
 
 /**
  * Constant for title.
  */
 private static final String TITLE = "title";
 
 /**
  * Constant for Introduction Copy.
  */
 private static final String INTRO_COPY = "introCopy";
 
 /** The Constant CTA_URL. */
 private static final String CTA_URL = "ctaUrl";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(MultipleRelatedTweetsViewHelper.class);
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.impl.SocialTabBaseViewHelperImpl#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.info("Multiple Related Tweets View Helper #processData called.");
  
  Map<String, Object> properties = getProperties(content);
  Resource currentResource = getCurrentResource(resources);
  ResourceResolver resolver = getResourceResolver(resources);
  Page currentPage = getCurrentPage(resources);
  I18n i18n = getI18n(resources);
  String jsonNameSpace = getJsonNameSpace(content);
  
  SocialTabQueryDTO tabQuery = new SocialTabQueryDTO();
  Map<String, Object> multipleRelatedTweetsMap = new LinkedHashMap<String, Object>();
  
  int numberOfPosts = Integer.parseInt(GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
    UnileverConstants.MULTIPLE_RELATED_TWEETS_TAB_CONFIG_CAT, NUMBER_OF_POSTS));
  int delayTime = Integer.parseInt(GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
    UnileverConstants.MULTIPLE_RELATED_TWEETS_TAB_CONFIG_CAT, DELAY_TIME));
  
  tabQuery.setBrandNameParameter(getBrandNameParameter(configurationService, currentPage));
  tabQuery.setConfigurationService(configurationService);
  tabQuery.setHashTags(getHashTags(currentResource));
  tabQuery.setMaxImages(0);
  tabQuery.setNoOfPost(numberOfPosts);
  tabQuery.setPage(currentPage);
  tabQuery.setPagePath(resolver.map(currentPage.getPath()));
  tabQuery.setSocialChannels(getSocialChannels(properties));
  
  multipleRelatedTweetsMap.put(TITLE, getTitle(properties));
  multipleRelatedTweetsMap.put(INTRO_COPY, getText(properties));
  multipleRelatedTweetsMap.put("delayTime", delayTime);
  multipleRelatedTweetsMap.put("ctaLabel", i18n.get("multipleRelatedTweetsTAB.joinTheConversation"));
  multipleRelatedTweetsMap.put("ctaUrl", MapUtils.getString(properties, CTA_URL, StringUtils.EMPTY));
  
  // adding retrieval query parameters
  SocialTABHelper.addRetrievalQueryParametrsMap(multipleRelatedTweetsMap, tabQuery);
  
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  data.put(jsonNameSpace, multipleRelatedTweetsMap);
  
  return data;
 }
}
