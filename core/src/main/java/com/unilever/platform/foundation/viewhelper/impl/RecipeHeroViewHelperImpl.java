/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.http.osgi.services.HttpClientBuilderFactory;
import org.apache.sling.api.SlingConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.RMSJson;
import com.unilever.platform.foundation.components.helper.RecipeHelper;
import com.unilever.platform.foundation.template.dto.RecipeConfigDTO;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * The Class RecipeHeroViewHelperImpl.
 */
@Component(description = "RecipeHeroViewHelperImpl", immediate = true, metatype = true, label = "Recipe Hero Component")
@Service(value = { RecipeHeroViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.RECIPE_HERO, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class RecipeHeroViewHelperImpl extends BaseViewHelper {
 
 /** The Constant VIDEO_PLAY_CTA. */
 private static final String VIDEO_PLAY_CTA = "videoPlayCta";
 
 /** The Constant RECIPE_HERO_VIDEO_PLAY_CTA. */
 private static final String RECIPE_HERO_VIDEO_PLAY_CTA = "recipeHero.videoPlayCta";
 
 /** The Constant VIDEO_CONTROLS. */
 private static final String VIDEO_CONTROLS = "videoControls";
 
 /** The Constant RELATED_VIDEOS_AT_END. */
 private static final String RELATED_VIDEOS_AT_END = "relatedVideosAtEnd";
 
 /** The Constant LOOP_VIDEO_PLAYBACK. */
 private static final String LOOP_VIDEO_PLAYBACK = "loopVideoPlayback";
 
 /** The Constant MUTE_VIDEO_AUDIO. */
 private static final String MUTE_VIDEO_AUDIO = "muteVideoAudio";
 
 /** The Constant HIDE_VIDEO_CONTROLS. */
 private static final String HIDE_VIDEO_CONTROLS = "hideVideoControls";
 
 /** The Constant AUTO_PLAY. */
 private static final String AUTO_PLAY = "autoplay";
 
 /** The Constant DISPLAY_IN_OVERLAY. */
 private static final String DISPLAY_IN_OVERLAY = "displayInOverlay";
 
 /** The Constant RECIPE_VIDEO. */
 private static final String RECIPE_VIDEO = "recipeVideo";
 
 /** The Constant RECIPE_IMAGE. */
 private static final String RECIPE_IMAGE = "recipeImage";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(RecipeHeroViewHelperImpl.class);
 
 @Reference
 HttpClientBuilderFactory factory;
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  
  LOGGER.info("Recipe Hero View Helper #processData called for processing fixed list.");
  
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  Map<String, Object> recipesMap = new LinkedHashMap<String, Object>();
  
  Map<String, Object> dialogProperties = getProperties(content);
  Page currentPage = getCurrentPage(resources);
  String jsonNameSpace = getJsonNameSpace(content);
  
  I18n i18n = getI18n(resources);
  
  RecipeConfigDTO recipeConfigDTO = RecipeHelper.getRecipeConfigurations(configurationService, currentPage, jsonNameSpace);
  
  boolean isVideoEnabled = MapUtils.getBoolean(dialogProperties, RECIPE_VIDEO, false);
  boolean isImageEnabled = MapUtils.getBoolean(dialogProperties, RECIPE_IMAGE, false);
  
  RMSJson.getRecipeJsonFromPage(resources, dialogProperties, recipesMap, recipeConfigDTO, factory);
  RMSJson.setRMSServiceDownKey(resources, recipesMap, configurationService, i18n);
  recipesMap.put(RECIPE_IMAGE, isImageEnabled);
  recipesMap.put(RECIPE_VIDEO, isVideoEnabled);
  if (isVideoEnabled) {
   Map<String, Object> videoControlsConfigMap = getVideoControlConfigMap(dialogProperties);
   
   recipesMap.put(VIDEO_CONTROLS, videoControlsConfigMap);
   recipesMap.put(VIDEO_PLAY_CTA, RecipeHelper.getValueByI18Key(i18n, RECIPE_HERO_VIDEO_PLAY_CTA));
  }
  
  data.put(jsonNameSpace, recipesMap);
  return data;
 }
 
 /**
  * Gets the video control config map.
  * 
  * @param resources
  *         the resources
  * @param properties
  *         the recipe properties
  * @return the video control config map
  */
 private Map<String, Object> getVideoControlConfigMap(Map<String, Object> properties) {
  
  LOGGER.info("Inside get Video Control Config Map");
  Map<String, Object> videoControlsFinalConfigMap = new LinkedHashMap<String, Object>();
  
  videoControlsFinalConfigMap.put(DISPLAY_IN_OVERLAY, MapUtils.getBoolean(properties, DISPLAY_IN_OVERLAY, false));
  videoControlsFinalConfigMap.put(AUTO_PLAY, MapUtils.getBoolean(properties, AUTO_PLAY, false));
  videoControlsFinalConfigMap.put(HIDE_VIDEO_CONTROLS, MapUtils.getBoolean(properties, HIDE_VIDEO_CONTROLS, false));
  videoControlsFinalConfigMap.put(MUTE_VIDEO_AUDIO, MapUtils.getBoolean(properties, MUTE_VIDEO_AUDIO, false));
  videoControlsFinalConfigMap.put(LOOP_VIDEO_PLAYBACK, MapUtils.getBoolean(properties, LOOP_VIDEO_PLAYBACK, false));
  videoControlsFinalConfigMap.put(RELATED_VIDEOS_AT_END, MapUtils.getBoolean(properties, RELATED_VIDEOS_AT_END, false));
  
  return videoControlsFinalConfigMap;
 }
}
