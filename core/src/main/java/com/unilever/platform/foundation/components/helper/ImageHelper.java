/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.components.helper;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.ImageResource;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.servlets.UnileverImageRenditionsServlet;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * This class contains all the methods related to image.
 * 
 * @author nbhart
 * 
 */
public final class ImageHelper {
 
 /**
  * logger object.
  */
 private static final Logger LOGGER = LoggerFactory.getLogger(ImageHelper.class);
 
 /**
  * Instantiates a new image helper.
  */
 private ImageHelper() {
 }
 
 /**
  * This method checks if the panel has a desktop image uploaded or dam asset image and accordingly the values are set in the panel map.
  * 
  * @param resource
  *         the resource
  * @param componentMap
  *         the component map
  * @param fileReference
  *         the file reference
  * @param page
  *         the page
  * @param slingRequest
  */
 public static void addImageParameters(Resource resource, Map<String, Object> componentMap, final String fileReference, Page page,
   SlingHttpServletRequest slingRequest) {
  List<Map<String, Object>> variantList = new ArrayList<Map<String, Object>>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  Resource imageResource = resource.getChild("file");
  if (imageResource != null) {
   componentMap.put(UnileverComponents.IMAGE_FILE_REFERENCE, ComponentUtil.getStaticFileFullPath(fileReference, page, slingRequest));
   Map<String, Object> renditionMap = new HashMap<String, Object>();
   Map<String, Object> renditionMapThumbnail = new HashMap<String, Object>();
   renditionMap.put("zoom", ComponentUtil.getStaticFileFullPath(fileReference, page, slingRequest));
   addAdaptiveParameters(imageResource, renditionMap);
   renditionMapThumbnail.put("thumbnail", ComponentUtil.getStaticFileFullPath(fileReference, page, slingRequest));
   addAdaptiveParameters(imageResource, renditionMapThumbnail);
   variantList.add(renditionMapThumbnail);
   variantList.add(renditionMap);
   componentMap.put(UnileverComponents.FILENAME, StringUtils.EMPTY);
   addAdaptiveParameters(imageResource, componentMap);
   componentMap.put(DamConstants.RENDITIONS_FOLDER, variantList);
   
  } else {
   ValueMap valueMap = resource.adaptTo(ValueMap.class);
   String filePath = ComponentUtil.getStaticFileFullPath(MapUtils.getString(valueMap, UnileverComponents.IMAGE_FILE_REFERENCE, StringUtils.EMPTY),
     page, slingRequest);
   String altText = getAltText(resource);
   if (StringUtils.isNotBlank(filePath)) {
    
    componentMap.putAll(getImageMap(filePath, page, filePath, altText, altText, slingRequest));
    
   }
  }
 }
 
 /**
  * This is the method to add the adaptive parameters.
  * 
  * @param imageResource
  *         the image resource
  * @param panelMap
  *         the panel map
  */
 public static void addAdaptiveParameters(Resource imageResource, Map<String, Object> panelMap) {
  
  if (imageResource.adaptTo(ValueMap.class).get(UnileverComponents.IMAGE_FILE_REFERENCE) != null) {
   String fileReference = StringUtils.EMPTY;
   
   try {
    fileReference = URLEncoder.encode((String) imageResource.adaptTo(ValueMap.class).get(UnileverComponents.IMAGE_FILE_REFERENCE), "UTF-8");
   } catch (UnsupportedEncodingException uee) {
    LOGGER.error("UnsupportedEncodingException while encoding the file reference: ", uee);
   }
   if (StringUtils.isNotBlank(fileReference)) {
    panelMap.put(UnileverComponents.EXTENSION, getExtensionFromMimeType(getFileExtension(fileReference)));
   }
   
  } else if (imageResource.hasChildren()) {
   Resource file = imageResource.getChild(JcrConstants.JCR_CONTENT);
   panelMap.put(UnileverComponents.EXTENSION, getExtensionFromMimeType((String) file.adaptTo(ValueMap.class).get(JcrConstants.JCR_MIMETYPE)));
   
  }
 }
 
 /**
  * Adds the zoom image parameters.
  * 
  * @param resource
  *         the resource
  * @param componentMap
  *         the component map
  * @param fileReference
  *         the file reference
  * @param page
  *         the page
  * @param zoomImageSize
  *         the zoom image size
  * @param slingRequest
  */
 public static void addZoomImageParameters(Resource resource, Map<String, Object> componentMap, final String fileReference, Page page,
   String zoomImageSize, SlingHttpServletRequest slingRequest) {
  ValueMap valueMap = resource.adaptTo(ValueMap.class);
  String filePath = MapUtils.getString(valueMap, UnileverComponents.IMAGE_FILE_REFERENCE, StringUtils.EMPTY);
  String altText = getAltText(resource);
  String extension = StringUtils.EMPTY;
  if (StringUtils.isNotBlank(filePath)) {
   if (StringUtils.isNotEmpty(filePath)) {
    extension = filePath.substring(filePath.lastIndexOf(CommonConstants.FULL_STOP, filePath.length()) + 1);
   }
   String url = fileReference + CommonConstants.FULL_STOP + UnileverImageRenditionsServlet.SELECTOR + CommonConstants.FULL_STOP + zoomImageSize
     + CommonConstants.FULL_STOP + extension;
   url = ComponentUtil.getStaticFileFullPath(url, page, slingRequest);
   componentMap.putAll(getImageMap(fileReference, page, fileReference, altText, altText, slingRequest));
   componentMap.put(UnileverComponents.URL, url);
  }
 }
 
 /**
  * This method is used to return the valid extension corresponding to the mime type.
  * 
  * @param mimeType
  *         This is the mimeType String.
  * @return The valid extension.
  */
 public static String getExtensionFromMimeType(String mimeType) {
  
  String extension = StringUtils.EMPTY;
  if (StringUtils.isNotEmpty(mimeType)) {
   if (StringUtils.containsIgnoreCase(mimeType, UnileverConstants.JPEG) || StringUtils.containsIgnoreCase(mimeType, UnileverConstants.JPG)) {
    extension = UnileverConstants.JPG;
   } else if (StringUtils.containsIgnoreCase(mimeType, UnileverConstants.GIF)) {
    extension = UnileverConstants.GIF;
   } else if (StringUtils.containsIgnoreCase(mimeType, UnileverConstants.PNG)) {
    extension = UnileverConstants.PNG;
   }
  }
  return extension;
 }
 
 /**
  * This method is used to fetch the image name.
  * 
  * @param fileReference
  *         the image path
  * @return the file name
  */
 public static String fetchImageFileName(String fileReference) {
  String fileName = StringUtils.EMPTY;
  if (StringUtils.isNotEmpty(fileReference) && StringUtils.contains(fileReference, UnileverConstants.FORWARD_SLASH)
    && StringUtils.contains(fileReference, CommonConstants.FULL_STOP)) {
   fileName = fileReference.substring(fileReference.lastIndexOf(UnileverConstants.FORWARD_SLASH) + 1,
     fileReference.lastIndexOf(CommonConstants.FULL_STOP));
  }
  return fileName;
 }
 
 /**
  * Gets the file extension.
  * 
  * @param fileReference
  *         the file reference
  * @return the file extension
  */
 public static String getFileExtension(String fileReference) {
  String extn = null;
  if (StringUtils.isNotBlank(fileReference) && StringUtils.contains(fileReference, CommonConstants.FULL_STOP)) {
   extn = fileReference.substring(fileReference.lastIndexOf(CommonConstants.FULL_STOP) + 1, fileReference.length());
  } else {
   extn = StringUtils.EMPTY;
  }
  return extn;
 }
 
 /**
  * Gets the image map.
  * 
  * @param fileReference
  *         the file reference
  * @param currentPage
  *         the current page
  * @param resourcePath
  *         the resource path
  * @param title
  *         the title
  * @param altTxt
  *         the alt txt
  * @param slingRequest
  * 
  * @return the image map
  */
 public static Map<String, Object> getImageMap(final String fileReference, final Page currentPage, String resourcePath, String title, String altTxt,
   SlingHttpServletRequest slingRequest) {
  ConfigurationService configurationService = currentPage.adaptTo(ConfigurationService.class);
  Map<String, Object> componentMap = new HashMap<String, Object>();
  if (currentPage != null) {
   
   String resourceFullPath = ComponentUtil.getStaticFileFullPath(resourcePath, currentPage, slingRequest);
   
   if (StringUtils.isNotBlank(fileReference)) {
    componentMap.put(UnileverComponents.URL, ComponentUtil.getStaticFileFullPath(fileReference, currentPage, slingRequest));
    componentMap.put(UnileverComponents.FILENAME, StringUtils.EMPTY);
    componentMap.put(ProductConstants.ISNOTADAPTIVEIMAGE, ProductConstants.FALSE);
    try {
     componentMap.put(ProductConstants.ISNOTADAPTIVEIMAGE,
       configurationService.getConfigValue(currentPage, "ImageAdaptivity", "isNotAdaptiveImageEnabled"));
    } catch (ConfigurationNotFoundException e) {
     LOGGER.error("could not find isNotAdaptiveImageEnabled key in ImageAdaptivity category", e);
    }
    
    componentMap.put(UnileverComponents.EXTENSION, getFileExtension(fileReference));
    String altTextTemp = altTxt;
    String titleTemp = title;
    if (StringUtils.isBlank(altTxt) || StringUtils.isBlank(title)) {
     if (StringUtils.isBlank(altTxt)) {
      altTextTemp = title;
     } else {
      titleTemp = altTxt;
     }
    }
    componentMap.put(UnileverComponents.IMAGE_ALT_TEXT, StringUtils.defaultString(altTextTemp, StringUtils.EMPTY));
    componentMap.put(UnileverComponents.IMAGE_TITLE_PROPERTY, StringUtils.defaultString(titleTemp, StringUtils.EMPTY));
    componentMap.put(UnileverComponents.IMAGE_PATH, StringUtils.defaultString(resourceFullPath, StringUtils.EMPTY));
   }
  }
  return componentMap;
 }
 
 /**
  * Gets the alt text from image resource or title of the resource if the alt text is empty.
  * 
  * @param resource
  *         the resource
  * @return the alt text
  */
 private static String getAltText(Resource resource) {
  ValueMap valueMap = resource.adaptTo(ValueMap.class);
  String altText = MapUtils.getString(valueMap, ProductConstants.IMAGE_ALT_TEXT, StringUtils.EMPTY);
  if (resource instanceof ImageResource && StringUtils.isEmpty(altText)) {
   ImageResource imageAsset = (ImageResource) resource;
   
   altText = imageAsset.getTitle();
  }
  return altText;
 }
}
