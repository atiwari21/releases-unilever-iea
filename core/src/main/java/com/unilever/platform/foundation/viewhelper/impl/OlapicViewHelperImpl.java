/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * This class prepares the data map for Olapic.
 */
@Component(description = "OlapicViewHelperImpl", immediate = true, metatype = true, label = "Olapic ViewHelper")
@Service(value = { OlapicViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = "/apps/unilever-iea/components/olapic", propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class OlapicViewHelperImpl extends BaseViewHelper {
 
 /**
  * logger object.
  */
 @Reference
 ConfigurationService configurationService;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(OlapicViewHelperImpl.class);
 
 /** The Constant OLAPIC_CATEGORY. */
 public static final String OLAPIC_CATEGORY = "olapic";
 
 /** The Constant IS_ENABLED. */
 public static final String IS_ENABLED = "IsEnabled";
 
 /** The Constant SOURCE. */
 public static final String SOURCE = "src";
 
 /** The Constant API_KEY. */
 public static final String API_KEY = "apiKey";
 
 /** The Constant INSTANCE_ID. */
 public static final String INSTANCE_ID = "instanceid";
 
 /** The Constant LANGUAGE. */
 private static final String LANGUAGE = "lang";
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper# processData (java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("Olapic View Helper #processData called .");
  ResourceResolver resourceResolver = (ResourceResolver) resources.get(ProductConstants.RESOURCE_RESOLVER);
  SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) resources.get(ProductConstants.REQUEST);
  Resource currentResource = slingRequest.getResource();
  
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  Page currentPage = pageManager.getContainingPage(currentResource);
  Map<String, Object> olapicProperties = (Map<String, Object>) content.get(PROPERTIES);
  Map<String, Object> properties = new LinkedHashMap<String, Object>();
  String jsonNameSapce = MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE);
  
  try {
   properties.put(IS_ENABLED, Boolean.parseBoolean(configurationService.getConfigValue(currentPage, OLAPIC_CATEGORY, IS_ENABLED)));
   properties.put(SOURCE, configurationService.getConfigValue(currentPage, OLAPIC_CATEGORY, SOURCE));
   properties.put(API_KEY, configurationService.getConfigValue(currentPage, OLAPIC_CATEGORY, API_KEY));
   properties.put(INSTANCE_ID, getProperty(olapicProperties, INSTANCE_ID));
   properties.put(LANGUAGE, currentPage.getLanguage(false));
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find key in olapic category", e);
  }
  
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(jsonNameSapce, properties);
  return data;
 }
 
 /**
  * Gets the property.
  * 
  * @param heroProperties
  *         the hero properties
  * @param propertyName
  *         the property name
  * @return the property
  */
 public static String getProperty(Map<String, Object> heroProperties, String propertyName) {
  String propertyValue = StringUtils.EMPTY;
  if (heroProperties != null && StringUtils.isNotBlank(propertyName)) {
   propertyValue = MapUtils.getString(heroProperties, propertyName);
  }
  
  if (StringUtils.isBlank(propertyValue)) {
   propertyValue = StringUtils.EMPTY;
  }
  
  return propertyValue;
 }
}
