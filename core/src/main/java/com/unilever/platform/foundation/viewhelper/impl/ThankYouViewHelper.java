/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.SocialCommunityHelper;
import com.unilever.platform.foundation.components.helper.TurnToHelper;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * The Class ThankYouViewHelper.
 */
@Component(description = "ThankYou Viewhelper Impl", immediate = true, metatype = true, label = "ThankYou helper Impl")
@Service(value = { ThankYouViewHelper.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.THANKYOU, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class ThankYouViewHelper extends BaseViewHelper {
 
 /** The Constant EMAIL_LABEL. */
 private static final String EMAIL_LABEL = "emailLabel";
 
 /** The Constant TITLE. */
 private static final String TITLE = "title";
 
 /** The Constant SUCESS_MESSAGE. */
 private static final String SUCESS_MESSAGE = "sucessMessage";
 private static final String FAILURE_MESSAGE = "failureMessage";
 
 /** The Constant ORDER_NUMBER_LABEL. */
 private static final String ORDER_NUMBER_LABEL = "orderNumberLabel";
 private static final String THANK_YOU_IMG_URL = "imageUrl";
 private static final String DESCRIPTION = "description";
 private static final String SHOPPABLE = "shoppable";
 private static final String ENTITY = "entity";
 private static final String FEED_TO = "feedTo";
 private static final String TURN_TO_CONFIG = "turnToConfig";
 private static final String TRANSACTION_SERVICE_URL = "transactionServiceUrl";
 private static final String CAMPAIGN = "campaign";
 private static final String BRAND = "brand";
 private static final String SERVICE_BRAND_NAME = "serviceBrandName";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ThankYouViewHelper.class);
 @Reference
 ConfigurationService configurationService;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @Override
 @SuppressWarnings("unchecked")
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  
  LOGGER.debug("thank You View Helper #processData called for processing fixed list.");
  Map<String, Object> thankYouProperties = (Map<String, Object>) content.get(PROPERTIES);
  Map<String, Object> thankYouMap = new LinkedHashMap<String, Object>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  Page currentPage = getCurrentPage(resources);
  thankYouMap.put(TITLE, MapUtils.getString(thankYouProperties, TITLE, StringUtils.EMPTY));
  thankYouMap.put(SUCESS_MESSAGE, MapUtils.getString(thankYouProperties, SUCESS_MESSAGE, StringUtils.EMPTY));
  thankYouMap.put(ORDER_NUMBER_LABEL, MapUtils.getString(thankYouProperties, ORDER_NUMBER_LABEL, StringUtils.EMPTY));
  thankYouMap.put(EMAIL_LABEL, MapUtils.getString(thankYouProperties, EMAIL_LABEL, StringUtils.EMPTY));
  String thankYouImageUrl = MapUtils.getString(thankYouProperties, THANK_YOU_IMG_URL, StringUtils.EMPTY);
  Map<String, Object> imageMap = SocialCommunityHelper.getImageMapFromPath(resources, getCurrentPage(resources), StringUtils.EMPTY, thankYouImageUrl);
  thankYouMap.put("image", imageMap);
  thankYouMap.put(FAILURE_MESSAGE, MapUtils.getString(thankYouProperties, FAILURE_MESSAGE, StringUtils.EMPTY));
  String entity = StringUtils.EMPTY;
  String campaign = StringUtils.EMPTY;
  String description = StringUtils.EMPTY;
  String feedTo = StringUtils.EMPTY;
  String transactionServiceUrl = StringUtils.EMPTY;
  String serviceBrandName = StringUtils.EMPTY;
  try {
   entity = configurationService.getConfigValue(currentPage, SHOPPABLE, ENTITY);
   campaign = configurationService.getConfigValue(currentPage, SHOPPABLE, CAMPAIGN);
   serviceBrandName = configurationService.getConfigValue(currentPage, BRAND, SERVICE_BRAND_NAME);
   description = configurationService.getConfigValue(currentPage, SHOPPABLE, DESCRIPTION);
   feedTo = configurationService.getConfigValue(currentPage, SHOPPABLE, FEED_TO);
   transactionServiceUrl = configurationService.getConfigValue(currentPage, SHOPPABLE, TRANSACTION_SERVICE_URL);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.debug("Could not find entity or description or turnTo key in category shoppable:" + e);
  }
  getResourceResolver(resources).map(transactionServiceUrl);
  thankYouMap.put(ENTITY, entity);
  thankYouMap.put(CAMPAIGN, campaign);
  thankYouMap.put(DESCRIPTION, description);
  thankYouMap.put(FEED_TO, feedTo);
  thankYouMap.put("serviceBrandName", serviceBrandName);
  thankYouMap.put(TURN_TO_CONFIG, TurnToHelper.getTurnToConfig(currentPage, configurationService, null));
  thankYouMap.put(TRANSACTION_SERVICE_URL, transactionServiceUrl);
  LOGGER.debug("thankYou Map Properties : " + thankYouMap.size());
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(getJsonNameSpace(content), thankYouMap);
  
  return data;
  
 }
}
