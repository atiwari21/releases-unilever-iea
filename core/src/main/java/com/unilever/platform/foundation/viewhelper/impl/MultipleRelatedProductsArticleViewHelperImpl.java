/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.api.Product;
import com.adobe.cq.commerce.common.CommerceHelper;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.aem.foundation.utils.AdaptiveUtil;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.ProductBundlingToBinUtil;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.kritique.config.KritiqueReviewsCommentsService;
import com.unilever.platform.foundation.template.dto.KritiqueDTO;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.MultipleRelatedProductsConstants;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * <p>
 * Responsible for reading author input for multipleRelatedProductsArticle component and get all the details of products which are selected by author
 * in dialog and then generate a product list.
 * </p>
 * .
 */
@Component(description = "MultipleRelatedProductsArticleViewHelperImpl",
immediate = true, metatype = true, label = "MultipleRelatedProductsArticleViewHelperImpl")
@Service(value = { MultipleRelatedProductsArticleViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.MULTIPLE_RELATED_PRODUCT_ARTICLE, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING,
  intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class MultipleRelatedProductsArticleViewHelperImpl extends BaseViewHelper {
 private static final String GLOBAL_CONFIG_CATEGORY = "multipleRelatedProductsArticle";

/** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(MultipleRelatedProductsArticleViewHelperImpl.class);
 
 private static final String ADD_PRODUCT = "addProduct";
 /*
  * reference for site configuration service
  */
 
 /** The kritique service. */
 @Reference
 KritiqueReviewsCommentsService kritiqueReviewsCommentsService;
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /**
  * The method processData overrides method is responsible validating author input and based on the input provided by author, returning a list of
  * products to be displayed on the page.
  * 
  * @param content
  *         {@link Map} of all user input received as input
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return Map holding the result data.
  */
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  LOGGER.debug("Inside MultipleRelatedProductsArticleViewHelperImpl view helper");
  Map<String, Object> data = new HashMap<String, Object>();
  Map<String, Object> properties = getProperties(content);
  
  List<String> productPagePathList = generateProductPathList(getResourceResolver(resources), getCurrentResource(resources));
  productPagePathList = ProductBundlingToBinUtil.getProductsListForMultipleBuy(productPagePathList, getCurrentPage(resources), configurationService);
  LOGGER.debug("Fetching details for list of product paths configured manually");
  List<Map<String, Object>> list = ProductHelper.getProductList(productPagePathList, resources, getJsonNameSpace(content));
  LOGGER.info("Final list of products related to product paths configured manually : " + list.size());
  Map<String, Object> productMap = getAdaptiveRating(resources, productPagePathList, list);
  Map<String, String> staticMap = new HashMap<String, String>();
  staticMap.put(ProductConstants.PRODUCT_COUNT_LABEL, getI18n(resources).get(ProductConstants.PRODUCT_COUNT_LABEL));
  productMap.put(ProductConstants.REVIEW_MAP, ProductHelper.getReviewMapProperties(configurationService, getCurrentPage(resources)));
  productMap.put(MultipleRelatedProductsConstants.TITLE, MapUtils.getString(properties, MultipleRelatedProductsConstants.TITLE, StringUtils.EMPTY));
  productMap.put(ProductConstants.STATIC_MAP, staticMap);
  Map<String, String> anchorLinkMap = ComponentUtil.getAnchorLinkMap(properties);
  productMap.put(UnileverConstants.ANCHOR_NAVIGATION, anchorLinkMap);
  String randomNumber = MapUtils.getString(properties, UnileverConstants.RANDOM_NUMBER, StringUtils.EMPTY);
  if(StringUtils.isNotBlank(randomNumber)){
   Map<String,String> configMap = ComponentUtil.getConfigMap(getCurrentPage(resources), GLOBAL_CONFIG_CATEGORY);
   ProductBundlingToBinUtil.setMultiBuyProductsConfigProperties(properties, productMap, configMap,
           MapUtils.getBooleanValue(properties, UnileverConstants.OVERRIDE_GLOBAL_CONFIG, false), configurationService, getCurrentPage(resources));
   ProductBundlingToBinUtil.setMultiBuyProductsCtaLabels(resources, productMap);
   Map<String,String> shopNowProperties = ProductHelper.getshopNowProperties(configurationService, getCurrentPage(resources),
           getI18n(resources), getResourceResolver(resources), getSlingRequest(resources));
   productMap.put(ProductConstants.SHOPNOW, shopNowProperties);
  }
  data.put(getJsonNameSpace(content), productMap);
  return data;
 }
 
 /**
  * @param slingRequest
  * @param pageManager
  * @param currentPage
  * @param productPagePathList
  * @param list
  * @return
  */
 private Map<String, Object> getAdaptiveRating(final Map<String, Object> resources, List<String> productPagePathList, List<Map<String, Object>> list){
  List<Map<String, Object>> productRatingPropertiesList = new ArrayList<Map<String, Object>>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  Map<String, Object> productMap = new HashMap<String, Object>();
  if (AdaptiveUtil.isAdaptive(getSlingRequest(resources))) {
   
   String prodids = StringUtils.EMPTY;
   KritiqueDTO kritiqueparams = new KritiqueDTO(getCurrentPage(resources), configurationService);
   LOGGER.info("Feature phone view called and kritique api is going to be called");
   for (String pagePath : productPagePathList) {
    UNIProduct product = null;
    Product pageProduct = CommerceHelper.findCurrentProduct(getPageManager(resources).getPage(pagePath));
    if (pageProduct instanceof UNIProduct) {
     product = (UNIProduct) pageProduct;
    }
    
    prodids = setProductId(prodids, product, pageProduct);
   }
   
   if(prodids != null){
    kritiqueparams.setSiteProductId(prodids);
   }
   
   List<Map<String, Object>> kqresponse = kritiqueReviewsCommentsService.getRatingsService(kritiqueparams, configurationService);
   LOGGER.debug("Kritique response generated and size of the review list is " + kqresponse.size());
   productRatingPropertiesList = ComponentUtil.mapKritiqueProductRating(kqresponse, list);
   productMap.put(ProductConstants.PRODUCT_LIST_MAP, productRatingPropertiesList.toArray());
   
  } else {
   productMap.put(ProductConstants.PRODUCT_LIST_MAP, list.toArray());
  }
  return productMap;
 }

/**sets product id to kritiqueservice
 * @param prodids
 * @param product
 * @param kritiqueparams
 */
 private String setProductId(String prodids, UNIProduct product, Product pageProduct) {
  String productIds = prodids;
  if(product != null && pageProduct != null){
   productIds += product.getSKU() + ",";
    }
  return productIds;
}
 
 /**
  * Generate product path list.
  * 
  * @param content
  *         the content
  * @return the list
  */
 private List<String> generateProductPathList(ResourceResolver resourceResolver, Resource currentResource) {
  
  List<Map<String, String>> relatedProductsSectionList = new ArrayList<Map<String, String>>();
  List<String> relatedList = new ArrayList<String>();
  List<String> propertyNames = new ArrayList<String>();
  propertyNames.add(ADD_PRODUCT);
  LOGGER.debug("multiple related products article properties : " + propertyNames);
  
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  
  List<Map<String, String>> list = ComponentUtil.getNestedMultiFieldProperties(currentResource, "panelsRelated");
  
  if (list != null) {
   for (Map<String, String> map : list) {
    Map<String, String> relatedProductsPropertiesMap = new LinkedHashMap<String, String>();
    String addProduct = StringUtils.defaultString(map.get(ADD_PRODUCT), StringUtils.EMPTY);
    Page page = pageManager.getPage(addProduct);
    setRelatedProductSectionList(relatedProductsSectionList, relatedProductsPropertiesMap, addProduct, page);
   }
   LOGGER.info("Manually configured product paths fetched successfully: " + relatedProductsSectionList.size());
   Iterator<Map<String, String>> iterator = relatedProductsSectionList.iterator();
   while (iterator.hasNext()) {
    Map<String, String> var = (Map<String, String>) iterator.next();
    String product = var.get(ADD_PRODUCT);
    if (product != null) {
     relatedList.add(product);
    }
   }
  }
  return relatedList;
    }
    
    /**
     * sets relatedProductsSectionList
     * 
     * @param relatedProductsSectionList
     * @param relatedProductsPropertiesMap
     * @param addProduct
     * @param page
     */
 private void setRelatedProductSectionList(List<Map<String, String>> relatedProductsSectionList, Map<String, String> relatedProductsPropertiesMap,
            String addProduct, Page page) {
  if (page != null) {
   relatedProductsPropertiesMap.put(ADD_PRODUCT, addProduct);
   if (relatedProductsPropertiesMap != null && !relatedProductsPropertiesMap.isEmpty()) {
    relatedProductsSectionList.add(relatedProductsPropertiesMap);
            }
        }
    }

}
