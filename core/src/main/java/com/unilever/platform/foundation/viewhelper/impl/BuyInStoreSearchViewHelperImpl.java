/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.components.helper.StoreLocatorUtility;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.Image;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;
import com.unilever.platform.foundation.viewhelper.constants.StoreLocatorConstants;

/**
 * The Class WhereToBuyViewHelperImpl.
 */
@Component(description = "BuyInStoreSearchViewHelperImpl", immediate = true, metatype = true, label = "BuyInStoreSearchViewHelperImpl")
@Service(value = { BuyInStoreSearchViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.BUY_IN_STORE_SEARCH, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class BuyInStoreSearchViewHelperImpl extends BaseViewHelper {
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(BuyInStoreSearchViewHelperImpl.class);
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The Constant WHERE_TO_BUY. */
 private static final String WHERE_TO_BUY = "whereToBuy";
 
 /** The Constant CHOOSE_PRODUCT_LABEL. */
 private static final String CHOOSE_PRODUCT_LABEL = "chooseProductLabel";
 
 /** The Constant CHOOSE_CATEGORY_LABEL. */
 
 private static final String CHOOSE_CATEGORY_LABEL = "chooseCategoryLabel";
 
 /** The Constant CATEGORIES. */
 private static final String CATEGORIES = "categories";
 
 /** The Constant CATEGORY_PAGE_URL. */
 private static final String CATEGORY_PAGE_URL = "categoryPageUrl";
 
 /** The Constant NAME. */
 private static final String NAME = "name";
 
 /** The Constant PAGE_PATH. */
 private static final String PAGE_PATH = "pagePath";
 
 /** The Constant IMAGE. */
 private static final String IMAGE = "image";
 
 /** The Constant SUB_CATGORIES. */
 private static final String SUB_CATGORIES = "subCategory";
 
 /** The Constant CHOOSE_CATEGORY_LABEL_KEY. */
 private static final String CHOOSE_CATEGORY_LABEL_KEY = "whereToBuy.chooseCategoryLabel";
 
 /** The Constant CHOOSE_PRODUCT_LABEL_KEY. */
 private static final String CHOOSE_PRODUCT_LABEL_KEY = "whereToBuy.chooseProductLabel";
 
 /** The Constant CATEGORY_TEMPLATE. */
 private static final String CATEGORY_TEMPLATE = "categoryTemplate";
 
 /** The Constant PRODUCT_DATA_PATH. */
 private static final String PRODUCT_DATA_PATH = "productDataPath";
 
 /** The Constant STORE_LOCATER. */
 private static final String STORE_LOCATOR = "storeLocator";
 
 /** The Constant CURRENT_PAGE. */
 private static final String CURRENT_PAGE = "currentPage";
 
 /** The Constant ZIP_CODE_LABEL. */
 private static final String ZIP_CODE_LABEL = "chooseZipCodeLabel";
 
 /** The Constant ZIP_CODE_LABEL_KEY. */
 private static final String ZIP_CODE_LABEL_KEY = "whereToBuy.chooseZipCodeLabel";
 
 public static final String COUNTRY_CODE = "countryCode";
 
 /** The Constant SELECT_LABEL. */
 public static final String SELECT_LABEL = "whereToBuy.selectLabel";
 
 /** The Constant CONTENT_TYPE. */
 private static final String CONTENT_TYPE = "contentType";
 
 /** The Constant ZERO. */
 private static final int ZERO = 0;
 
 /** The Constant CATEGORY_CONTENT_TYPE. */
 private static final String CATEGORY_CONTENT_TYPE = "categoryContentType";
 
 /**
  * The method processData overrides method is responsible validating author input and returning a list of product catogories
  * 
  * @param content
  *         {@link Map} of all user input received as input
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return Map holding the result data.
  */
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  LOGGER.debug("BuyInStoreSearch View Helper #processData called for processing fixed list.");
  
  Map<String, Object> categoryMapProps = new HashMap<String, Object>();
  I18n i18n = getI18n(resources);
  Map<String, String> whereToBuyConfigurations = GlobalConfigurationUtility.getMapFromConfiguration(configurationService, getCurrentPage(resources),
    WHERE_TO_BUY);
  
  List<Map<String, Object>> categoryPageList = getCategoryList(content, resources, whereToBuyConfigurations);
  LOGGER.info("The number of category page list defined is " + categoryPageList.size());
  categoryMapProps.put(CHOOSE_CATEGORY_LABEL, i18n.get(CHOOSE_CATEGORY_LABEL_KEY));
  categoryMapProps.put(CHOOSE_PRODUCT_LABEL, i18n.get(CHOOSE_PRODUCT_LABEL_KEY));
  categoryMapProps.put(ZIP_CODE_LABEL, i18n.get(ZIP_CODE_LABEL_KEY));
  categoryMapProps.put(CURRENT_PAGE, getCurrentPage(resources).getPath());
  String buyOnlineTitleText = MapUtils.getString(getProperties(content), "buyOnlineTitleText", "");
  categoryMapProps.put("buyOnlineTitleText", buyOnlineTitleText);
  
  String productDataPath = whereToBuyConfigurations.get(PRODUCT_DATA_PATH);
  String servletPath = StringUtils.isNotBlank(productDataPath) ? getResourceResolver(resources).map(productDataPath) : "";
  
  categoryMapProps.put(PRODUCT_DATA_PATH, servletPath);
  categoryMapProps.put(ProductConstants.REVIEW_MAP, ProductHelper.getReviewMapProperties(configurationService, getCurrentPage(resources)));
  Boolean isStoreLocatorEnabled = StoreLocatorUtility.isStoreLocatorEnabled(getCurrentPage(resources), configurationService,
    getJsonNameSpace(content));
  if (isStoreLocatorEnabled) {
   categoryMapProps.put(STORE_LOCATOR, StoreLocatorUtility.getStoreLocatorMap(resources, configurationService, getJsonNameSpace(content)));
  }
  categoryMapProps.put(CATEGORIES, categoryPageList.toArray());
  categoryMapProps.put(StoreLocatorConstants.GEO_CODE_API, GlobalConfigurationUtility.getValueFromConfiguration(configurationService,
    getCurrentPage(resources), StoreLocatorConstants.GLOBAL_CONFIG_CAT, StoreLocatorConstants.GEO_CODE_API));
  categoryMapProps.put("selectLabel", i18n.get(SELECT_LABEL));
  
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(getJsonNameSpace(content), categoryMapProps);
  LOGGER.debug("The Buy In store final data map returned is " + data.size());
  return data;
 }
 
 /**
  * Gets the category list.
  * 
  * @param content
  *         the content
  * @param resources
  *         the resources
  * @param whereToBuyConfigurations
  *         the where to buy configurations
  * @return the category list
  */
 private List<Map<String, Object>> getCategoryList(Map<String, Object> content, final Map<String, Object> resources,
   Map<String, String> whereToBuyConfigurations) {
  List<Map<String, Object>> categoryList = new ArrayList<Map<String, Object>>();
  List<String> categoryPageName = new ArrayList<String>();
  categoryPageName.add(CATEGORY_PAGE_URL);
  List<Map<String, String>> categoryPageList = com.sapient.platform.iea.aem.core.utils.CollectionUtils.convertMultiWidgetToList(
    getProperties(content), categoryPageName);
  int size = categoryPageList != null ? categoryPageList.size() : 0;
  List<String> list = new ArrayList<String>();
  for (int i = 0; i < size; i++) {
   Map<String, String> categoryPageMap = categoryPageList.get(i);
   String pageUrl = categoryPageMap.get(CATEGORY_PAGE_URL).toString();
   Page categoryPage = getPageManager(resources).getPage(pageUrl);
   Map<String, Object> categoryMap = new LinkedHashMap<String, Object>();
   categoryList.add(getCategoryMap(categoryPage, whereToBuyConfigurations, categoryMap, resources));
  }
  LOGGER.debug("categories map list size : " + list.size());
  
  return categoryList;
 }
 
 /**
  * Gets the category map.
  * 
  * @param page
  *         the page
  * @param whereToBuyConfigurations
  *         the where to buy configurations
  * @param categoryMap
  *         the category map
  * @param resources
  * @return the category map
  */
 private Map<String, Object> getCategoryMap(Page page, Map<String, String> whereToBuyConfigurations, Map<String, Object> categoryMap,
   Map<String, Object> resources) {
  if (page != null && isCategoryPage(page, whereToBuyConfigurations)) {
   categoryMap.put(NAME, page.getTitle());
   categoryMap.put(PAGE_PATH, page.getPath());
   ValueMap pageProperties = page.getProperties();
   String teaserImagePath = pageProperties.get(UnileverConstants.TEASER_IMAGE, null);
   
   if (StringUtils.isNotBlank(teaserImagePath)) {
    String teaserImageAlt = pageProperties.get(UnileverConstants.TEASER_IMAGE_ALT_TEXT, "");
    categoryMap.put(IMAGE, new Image(teaserImagePath, teaserImageAlt, "", getCurrentPage(resources), getSlingRequest(resources)));
   }
   
   Iterator<Page> categoryPageIterator = page.listChildren();
   List<Map<String, Object>> categoryMapList = new ArrayList<Map<String, Object>>();
   while (categoryPageIterator.hasNext()) {
    Page categoryPage = categoryPageIterator.next();
    if (isCategoryPage(categoryPage, whereToBuyConfigurations)) {
     Map<String, Object> childCategoryMap = new LinkedHashMap<String, Object>();
     categoryMapList.add(getCategoryMap(categoryPage, whereToBuyConfigurations, childCategoryMap, resources));
    }
   }
   if (CollectionUtils.isNotEmpty(categoryMapList)) {
    categoryMap.put(SUB_CATGORIES, categoryMapList.toArray());
   }
  }
  return categoryMap;
 }
 
 /**
  * Checks if is category page.
  * 
  * @param categoryPage
  *         the category page
  * @param categoryListingConfigurations
  *         the category listing configurations
  * @return the boolean
  */
 protected Boolean isCategoryPage(Page categoryPage, Map<String, String> categoryListingConfigurations) {
  String[] categoryContentTypes = StringUtils.split(getStringValuefromMap(categoryListingConfigurations, CATEGORY_CONTENT_TYPE), ",");
  if (categoryContentTypes.length > ZERO) {
   for (String categoryContentType : categoryContentTypes) {
    ValueMap pageProperties = categoryPage != null ? categoryPage.getProperties() : null;
    String contentType = MapUtils.getString(pageProperties, CONTENT_TYPE, null);
    if (contentType != null && contentType.equals(categoryContentType.trim())) {
     return true;
    }
   }
   return false;
  }
  
  String[] properties = StringUtils.split(getStringValuefromMap(categoryListingConfigurations, CATEGORY_TEMPLATE), ",");
  for (String property : properties) {
   ValueMap pageProperties = categoryPage != null ? categoryPage.getProperties() : null;
   String templateName = MapUtils.getString(pageProperties, NameConstants.PN_TEMPLATE, null);
   if (templateName != null && templateName.endsWith(property)) {
    return true;
   }
  }
  return false;
 }
 
 /**
  * Gets the string value from map.
  * 
  * @param map
  *         the map
  * @param key
  *         the key
  * @return the string value from map
  */
 private static String getStringValuefromMap(Map<String, String> map, String key) {
  
  String value = StringUtils.EMPTY;
  if (map.containsKey(key)) {
   value = map.get(key).toString();
  }
  return value;
 }
 
}
