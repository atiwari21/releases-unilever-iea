/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.servlet;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.io.JSONWriter;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.aem.foundation.constants.CommonConstants;

/**
 * The Class SocialShareServlet.
 */
@SlingServlet(label = "Unilever Social Share Servlet to Fetch social share options list", resourceTypes = { CommonConstants.DEFAULT_SERVLET_NAME }, methods = { HttpConstants.METHOD_GET }, selectors = { "addThisOptions" }, extensions = { "json" })
@Properties({
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.foundation.servlet.SocialShareServlet", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "Unilever Social Share Servlet to Fetch social share options list", propertyPrivate = false),

})
public class SocialShareServlet extends SlingAllMethodsServlet {
 
 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = 1L;
 
 /** The Constant ADD_THIS_OPTIONS. */
 private static final String ADD_THIS_OPTIONS = "addThisOptions";
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(SocialShareServlet.class);
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache. sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected final void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws IOException {
  
  String path = request.getRequestPathInfo().getResourcePath();
  ResourceResolver resourceResolver = request.getResourceResolver();
  
  Writer out = response.getWriter();
  JSONWriter writer = new JSONWriter(out);
  
  response.setContentType("application/json");
  response.setCharacterEncoding("UTF-8");
  
  Resource currentResource = resourceResolver.getResource(path);
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  Page page = pageManager.getContainingPage(currentResource);
  String[] pageSelectors = request.getRequestPathInfo().getSelectors();
  String globalConfigCategory = pageSelectors.length > 1 ? pageSelectors[1] : ADD_THIS_OPTIONS;
  if (page != null) {
   Map<String, String> addThisOptionsMap = GlobalConfigurationUtility.getMapFromConfiguration(configurationService, page, globalConfigCategory);
   
   try {
    writer.array();
    addAllAddThisOptions(addThisOptionsMap, writer);
    writer.endArray();
   } catch (JSONException e) {
    LOGGER.error("RepositoryException occurred ", e);
   }
  }
 }
 
 /**
  * Adds the writer obj.
  * 
  * @param writer
  *         the writer
  * @param key
  *         the key
  * @param value
  *         the value
  * @throws JSONException
  *          the JSON exception
  */
 private void addWriterObj(JSONWriter writer, String key, String value) throws JSONException {
  writer.object();
  writer.key("text").value(key.trim());
  writer.key("value").value(value.trim());
  writer.endObject();
 }
 
 /**
  * Adds the all add this options.
  * 
  * @param addThisOptionsMap
  *         the add this options map
  * @param writer
  *         the writer
  * @throws JSONException
  *          the JSON exception
  */
 private void addAllAddThisOptions(Map<String, String> addThisOptionsMap, JSONWriter writer) throws JSONException {
  if (MapUtils.isNotEmpty(addThisOptionsMap)) {
   for (String key : addThisOptionsMap.keySet()) {
    addWriterObj(writer, key, addThisOptionsMap.get(key));
   }
  }
 }
 
}
