/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.kritique.config;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.template.dto.KritiqueDTO;

/**
 * @author mkathu
 * 
 */
@Component(immediate = true, label = "Kritique Reviews and Comments Service", description = "Kritique Reviews and Comments Service", metatype = true)
@Service(value = KritiqueReviewsCommentsService.class)
public class KritiqueReviewsCommentsServiceImpl implements KritiqueReviewsCommentsService {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(KritiqueReviewsCommentsServiceImpl.class);
 
 /** The Constant NO_OF_RECORDS. */
 private static final String NO_OF_RECORDS = "NoOfRecords";
 
 /** The Constant PAGE_NUMBER. */
 private static final String PAGE_NUMBER = "PageNumber";
 
 /** The Constant SORT_BY. */
 private static final String SORT_BY = "SortBy";
 
 /** The Constant SITE_PRODUCT_ID. */
 private static final String SITE_PRODUCT_ID = "SiteProductId";
 
 /** The Constant IS_SYNDICATION_ON. */
 private static final String IS_SYNDICATION_ON = "IsSyndicationOn";
 
 /** The Constant SITE_SOURCE. */
 private static final String SITE_SOURCE = "siteSource";
 
 /** The Constant LOCALE_ID. */
 private static final String LOCALE_ID = "LocaleId";
 
 /** The Constant BRAND_ID. */
 private static final String BRAND_ID = "BrandId";
 
 /** The Constant AMPERSAND. */
 private static final String AMPERSAND = "&";
 
 /** The Constant EQUALS. */
 private static final String EQUALS = "=";
 
 private static final int DEAFULT_CONNECTION_TIMEOUT = 2000;
 
 private static final int KRITIQUE_RESPONSE = 200;
 
 private static final String REVIEWS = "/Reviews?";
 
 private static final String RATINGS = "/Rating?";
 
 private static final String AVERAGE_RATING = "AverageRating";
 
 String apiKey = "";
 
 /** The Constant ENTITY_SOURCE_ID. */
 private static final String ENTITY_SOURCE_ID = "EntitySourceId";
 
 /** The Constant RATING_ARTICLE. */
 private static final String RATING_ARTICLE = "/Rating/Article?";
 
 /** The Constant REVIEWS_ARTICLE. */
 private static final String REVIEWS_ARTICLE = "/Reviews/Article?";
 
 /** The Constant ID_TYPE. */
 private static final String ID_TYPE = "IdType";
 
 @Reference
 org.apache.http.osgi.services.HttpClientBuilderFactory factory;
 
 @Override
 public Map<String, Object> getReviewsService(KritiqueDTO kritiquedto, ConfigurationService configurationService) {
  Map<String, Object> reviewmap = new HashMap<String, Object>();
  List<Map<String, Object>> output = new ArrayList<Map<String, Object>>();
  String serviceURL = StringUtils.EMPTY;
  
  StringBuilder stringBuilder = new StringBuilder();
  stringBuilder.append(kritiquedto.getApiPath() + REVIEWS);
  stringBuilder.append(AMPERSAND).append(BRAND_ID).append(EQUALS).append(kritiquedto.getBrandId());
  stringBuilder.append(AMPERSAND).append(LOCALE_ID).append(EQUALS).append(kritiquedto.getLocaleId());
  stringBuilder.append(AMPERSAND).append(SITE_SOURCE).append(EQUALS).append(kritiquedto.getSiteSource());
  stringBuilder.append(AMPERSAND).append(IS_SYNDICATION_ON).append(EQUALS).append(kritiquedto.isSyndicationOn());
  stringBuilder.append(AMPERSAND).append(SITE_PRODUCT_ID).append(EQUALS).append(kritiquedto.getSiteProductId());
  stringBuilder.append(AMPERSAND).append(NO_OF_RECORDS).append(EQUALS).append(kritiquedto.getNoOfRecords());
  stringBuilder.append(AMPERSAND).append(PAGE_NUMBER).append(EQUALS).append(kritiquedto.getPageno());
  stringBuilder.append(AMPERSAND).append(SORT_BY).append(EQUALS).append(kritiquedto.getSortby());
  serviceURL = stringBuilder.toString();
  
  output = getKritiqueResponse(serviceURL, kritiquedto.getApiKey(), kritiquedto.getReferer());
  if (!output.isEmpty()) {
   reviewmap = output.get(0);
  }
  return reviewmap;
 }
 
 @Override
 public List<Map<String, Object>> getRatingsService(KritiqueDTO kritiquedto, ConfigurationService configurationService) {
  
  List<Map<String, Object>> output = new ArrayList<Map<String, Object>>();
  String serviceURL = StringUtils.EMPTY;
  
  StringBuilder stringBuilder = new StringBuilder();
  stringBuilder.append(kritiquedto.getApiPath() + RATINGS);
  stringBuilder.append(AMPERSAND).append(BRAND_ID).append(EQUALS).append(kritiquedto.getBrandId());
  stringBuilder.append(AMPERSAND).append(LOCALE_ID).append(EQUALS).append(kritiquedto.getLocaleId());
  stringBuilder.append(AMPERSAND).append(SITE_SOURCE).append(EQUALS).append(kritiquedto.getSiteSource());
  stringBuilder.append(AMPERSAND).append(IS_SYNDICATION_ON).append(EQUALS).append(kritiquedto.isSyndicationOn());
  stringBuilder.append(AMPERSAND).append(SITE_PRODUCT_ID).append(EQUALS).append(kritiquedto.getSiteProductId());
  serviceURL = stringBuilder.toString();
  output = getKritiqueResponse(serviceURL, kritiquedto.getApiKey(), kritiquedto.getReferer());
  
  return output;
  
 }
 
 @Override
 public String getSEORatingsService(KritiqueDTO kritiquedto) {
  return null;
 }
 
 /**
  * Call Kritique Service.
  * 
  * @param serviceUrl
  *         the serviceUrl
  * @return the Map
  * 
  * 
  */
 private List<Map<String, Object>> getKritiqueResponse(String serviceURL, String apiKey, String referrer) {
  HttpGet requestService = null;
  List<Map<String, Object>> kqresponse = new ArrayList<Map<String, Object>>();
  
  try {
   RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(DEAFULT_CONNECTION_TIMEOUT)
     .setConnectionRequestTimeout(DEAFULT_CONNECTION_TIMEOUT).setSocketTimeout(DEAFULT_CONNECTION_TIMEOUT).build();
   
   HttpClient httpclient = factory.newBuilder().disableAutomaticRetries().disableConnectionState().disableRedirectHandling()
     .setDefaultRequestConfig(requestConfig).build();
   
   requestService = new HttpGet(serviceURL);
   requestService.setHeader("Content-Type", "application/json");
   requestService.setHeader("X-ApiKey", apiKey);
   requestService.setHeader("Referer", referrer);
   
   HttpResponse response = httpclient.execute(requestService);
   StatusLine statusLine = response.getStatusLine();
   int statuscode = statusLine.getStatusCode();
   if (statuscode == KRITIQUE_RESPONSE) {
    HttpEntity entity = response.getEntity();
    InputStream content = entity.getContent();
    if (serviceURL.contains("/Article") || serviceURL.contains("/article")) {
     kqresponse = convertArticleStreamToMap(content);
    } else {
     kqresponse = convertStreamToMap(content);
    }
   }
   
  } catch (Exception e) {
   LOGGER.error("Error in getting KQ Response : ", e);
   return kqresponse;
  } finally {
   if (requestService != null) {
    requestService.releaseConnection();
   }
  }
  
  return kqresponse;
 }
 
 /**
  * Convert stream to map.
  * 
  * @param input
  *         the input
  * @return the Map
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  * 
  */
 @SuppressWarnings("unchecked")
 private static List<Map<String, Object>> convertStreamToMap(InputStream input) throws IOException {
  
  BufferedReader reader = new BufferedReader(new InputStreamReader(input));
  StringBuilder out = new StringBuilder();
  Map<String, Object> result = new HashMap<String, Object>();
  List<Map<String, Object>> finalresult = new ArrayList<Map<String, Object>>();
  
  String line;
  while ((line = reader.readLine()) != null) {
   out.append(line);
  }
  JSONObject object = new JSONObject(out.toString());
  JSONArray products = object.getJSONArray("Products");
  if (products != null) {
   for (int i = 0; i < products.length(); i++) {
    JSONObject conf = products.getJSONObject(i);
    Double val1 = conf.getDouble(AVERAGE_RATING);
    int newval = (int) Math.round(val1);
    conf.put(AVERAGE_RATING, String.valueOf(newval));
    result = new ObjectMapper().readValue(conf.toString(), HashMap.class);
    finalresult.add(result);
    
   }
  }
  
  return finalresult;
 }
 
 @Override
 public Map<String, Object> getArticleReviewsService(KritiqueDTO kritiquedto, ConfigurationService configurationService) {
  Map<String, Object> reviewmap = new HashMap<String, Object>();
  List<Map<String, Object>> output = new ArrayList<Map<String, Object>>();
  String serviceURL = StringUtils.EMPTY;
  
  StringBuilder stringBuilder = new StringBuilder();
  stringBuilder.append(kritiquedto.getApiPath() + REVIEWS_ARTICLE);
  stringBuilder.append(AMPERSAND).append(ENTITY_SOURCE_ID).append(EQUALS).append(kritiquedto.getEntitySourceId());
  stringBuilder.append(AMPERSAND).append(ID_TYPE).append(EQUALS);
  stringBuilder.append(AMPERSAND).append(NO_OF_RECORDS).append(EQUALS).append(kritiquedto.getNoOfRecords());
  stringBuilder.append(AMPERSAND).append(PAGE_NUMBER).append(EQUALS).append(kritiquedto.getPageno());
  stringBuilder.append(AMPERSAND).append(SORT_BY).append(EQUALS).append(kritiquedto.getSortby());
  stringBuilder.append(AMPERSAND).append(IS_SYNDICATION_ON).append(EQUALS).append(kritiquedto.isSyndicationOn());
  stringBuilder.append(AMPERSAND).append(SITE_SOURCE).append(EQUALS).append(kritiquedto.getSiteSource());
  stringBuilder.append(AMPERSAND).append(BRAND_ID).append(EQUALS).append(kritiquedto.getBrandId());
  stringBuilder.append(AMPERSAND).append(LOCALE_ID).append(EQUALS).append(kritiquedto.getLocaleId());
  serviceURL = stringBuilder.toString();
  
  output = getKritiqueResponse(serviceURL, kritiquedto.getApiKey(), kritiquedto.getReferer());
  if (!output.isEmpty()) {
   reviewmap = output.get(0);
  }
  return reviewmap;
 }
 
 @Override
 public List<Map<String, Object>> getArticleRatingsService(KritiqueDTO kritiquedto, ConfigurationService configurationService) {
  List<Map<String, Object>> output = new ArrayList<Map<String, Object>>();
  String serviceURL = StringUtils.EMPTY;
  
  StringBuilder stringBuilder = new StringBuilder();
  stringBuilder.append(kritiquedto.getApiPath() + RATING_ARTICLE);
  stringBuilder.append(AMPERSAND).append(ENTITY_SOURCE_ID).append(EQUALS).append(kritiquedto.getEntitySourceId());
  stringBuilder.append(AMPERSAND).append(ID_TYPE).append(EQUALS);
  stringBuilder.append(AMPERSAND).append(IS_SYNDICATION_ON).append(EQUALS).append(kritiquedto.isSyndicationOn());
  stringBuilder.append(AMPERSAND).append(SITE_SOURCE).append(EQUALS).append(kritiquedto.getSiteSource());
  stringBuilder.append(AMPERSAND).append(BRAND_ID).append(EQUALS).append(kritiquedto.getBrandId());
  stringBuilder.append(AMPERSAND).append(LOCALE_ID).append(EQUALS).append(kritiquedto.getLocaleId());
  
  serviceURL = stringBuilder.toString();
  output = getKritiqueResponse(serviceURL, kritiquedto.getApiKey(), kritiquedto.getReferer());
  
  return output;
 }
 
 /**
  * Convert stream to map.
  * 
  * @param input
  *         the input
  * @return the Map
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  * 
  */
 @SuppressWarnings("unchecked")
 private static List<Map<String, Object>> convertArticleStreamToMap(InputStream input) throws IOException {
  
  BufferedReader reader = new BufferedReader(new InputStreamReader(input));
  StringBuilder out = new StringBuilder();
  Map<String, Object> result = new HashMap<String, Object>();
  List<Map<String, Object>> finalresult = new ArrayList<Map<String, Object>>();
  
  String line;
  while ((line = reader.readLine()) != null) {
   out.append(line);
  }
  JSONObject object = new JSONObject(out.toString());
  JSONArray entities = object.getJSONArray("Entities");
  if (entities != null) {
   for (int i = 0; i < entities.length(); i++) {
    JSONObject conf = entities.getJSONObject(i);
    Double val1 = conf.getDouble(AVERAGE_RATING);
    int newval = (int) Math.round(val1);
    conf.put(AVERAGE_RATING, String.valueOf(newval));
    result = new ObjectMapper().readValue(conf.toString(), HashMap.class);
    finalresult.add(result);
    
   }
  }
  
  return finalresult;
 }
}
