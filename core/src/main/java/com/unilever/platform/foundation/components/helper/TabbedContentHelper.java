/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.components.helper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class TabbedContentHelper
 * 
 */
@Service(value = { TabbedContentHelper.class })
public final class TabbedContentHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(TabbedContentHelper.class);
 
 /**
  * Instantiates a new social tab helper.
  */
 private TabbedContentHelper() {
  LOGGER.info("Tabbed Control Helper Constructor Called.");
 }
 
 /**
  * @param childNodesIterator
  *         the childNodesIterator
  * @param resourceResolver
  *         the resourceResolver
  * @param slideRandomNumbersOrigArray
  *         the slideRandomNumbersOrigArray
  */
 public static void deleteUnwantedNodes(Iterator<Resource> childNodesIterator, ResourceResolver resourceResolver, String[] slideRandomNumbersOrigArray) {
  String[] randomNumberArray = getValidValues(slideRandomNumbersOrigArray);
  
  while (childNodesIterator.hasNext()) {
   Resource child = (Resource) childNodesIterator.next();
   
   if (!randomNumberMatcher(tokenizePath(child.getPath()), randomNumberArray)) {
    try {
     resourceResolver.delete(child);
     resourceResolver.commit();
    } catch (PersistenceException e) {
     LOGGER.error("Error in deleting the node " + e.getMessage() + " caught exception " + e);
    }
   }
  }
 }
 
 /**
  * @param string
  *         the string
  * @return tokenize path
  */
 public static String tokenizePath(String string) {
  String[] splitValue = string.split("par_tab_");
  String tokenizedValue = null;
  
  if ((splitValue != null) && (splitValue.length > 1)) {
   tokenizedValue = splitValue[1];
  }
  return tokenizedValue;
 }
 
 /**
  * @param slideRandomNumbersOrigArray
  *         the slideRandomNumbersOrigArray
  * @return the valid values
  */
 public static String[] getValidValues(String[] slideRandomNumbersOrigArray) {
  List<String> randomNumberArray = new ArrayList<String>();
  for (int i = 0; i < slideRandomNumbersOrigArray.length; i++) {
   if ((slideRandomNumbersOrigArray[i] != null) || (!StringUtils.isEmpty(slideRandomNumbersOrigArray[i]))) {
    randomNumberArray.add(slideRandomNumbersOrigArray[i]);
   }
  }
  return (String[]) randomNumberArray.toArray(new String[randomNumberArray.size()]);
 }
 
 /**
  * @param nodeRandomNumber
  *         the nodeRandomNumber
  * @param randomNumberArray
  *         the randomNumberArray
  * @return boolean
  */
 public static boolean randomNumberMatcher(String nodeRandomNumber, String[] randomNumberArray) {
  for (int i = 0; i < randomNumberArray.length; i++) {
   if (randomNumberArray[i].equals(nodeRandomNumber)) {
    return true;
   }
  }
  return false;
 }
 
}
