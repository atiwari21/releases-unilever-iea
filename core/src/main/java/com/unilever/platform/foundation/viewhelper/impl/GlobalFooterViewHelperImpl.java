/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.utils.CollectionUtils;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.components.LanguageSelectorUtility;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.constants.ContainerTagConstants;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * <p>
 * The goal of this component is to display global footer on all pages. The component will be configured by editing sections in the dialog at root
 * level page once and then will get displayed on all children pages. Quote copy text, Brand logo image,Social sharing links,Footer links, Local
 * market identification,Copyright text, Unilever logo image are the properties which will be displayed in this component.
 * 
 * </p>
 */
@Component(description = "GlobalFooterViewHelper Viewhelper Impl", immediate = true, metatype = true, label = "GlobalFooterViewHelper")
@Service(value = { GlobalFooterViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.FOOTER, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class GlobalFooterViewHelperImpl extends BaseViewHelper {
 
 /**
  * Constant for retrieving footer component property.
  */
 
 /** constant for ad choice label */
 private static final String AD_CHOICE_LABEL = "label";
 
 /** ad choice field label */
 private static final String AD_CHOICE_FIELD_LABEL = "adChoiceLabel";
 
 /** constant for ad choice config key */
 private static final String AD_CHOICE = "adChoice";
 
 /** constant for ad choice config key */
 private static final String AD_CHOICE_CAT = "adchoice";
 
 /** constant for ad choice client id */
 private static final String AD_CHOICE_CLIENT_ID = "clientId";
 
 /** constant for ad choice PID */
 private static final String AD_CHOICE_PID = "pId";
 
 /** constant to check if the ad choice is enabled */
 private static final String IS_ENABLED = "isEnabled";
 
 /** The Constant QUOTES. */
 private static final String QUOTES = "quotes";
 
 /** The Constant CUTE_QUOTES. */
 private static final String CUTE_QUOTES = "cuteQuote";
 
 /** The Constant BRAND_LOGO_LINK. */
 private static final String BRAND_LOGO_LINK = "brandLogoLink";
 
 /** The Constant BRAND_ALT_TEXT. */
 private static final String BRAND_ALT_TEXT = "brandAltText";
 
 /** The Constant BRAND_LOGO_ICON. */
 private static final String BRAND_LOGO_ICON = "brandLogoIcon";
 
 /** The Constant ALT. */
 private static final String ALT = "alt";
 
 /** The Constant ALT_IMAGE. */
 private static final String ALT_IMAGE = "altImage";
 
 /** The Constant LOGO. */
 private static final String LOGO = "logo";
 
 /** The Constant FOOTER_LINKS. */
 private static final String FOOTER_LINKS = "footerLinks";
 
 /** The Constant COMPANY. */
 private static final String COMPANY = "company";
 
 /** The Constant FLAG_SVG. */
 private static final String FLAG_SVG = "flag";
 
 /** The Constant URL. */
 private static final String IMAGE_URL = "imageUrl";
 
 /** The Constant FOOTER_INFO. */
 private static final String FOOTER_INFO = "footerInfo";
 
 /** The Constant ID. */
 private static final String ICON_IMAGE = "iconImage";
 /** The Constant PREVIEW_IMAGE_URL. */
 private static final String SOCIAL_LINK = "socialLink";
 
 /** The Constant SOCIAL_TITLE. */
 private static final String SOCIAL_TITLE = "socialTitle";
 
 /** The Constant ALT_TEXT. */
 private static final String ALT_TEXT = "altText";
 
 /** The Constant ICON_NAME. */
 private static final String ICON_NAME = "iconName";
 
 /** The Constant SOCIAL_NAVIGATION. */
 private static final String SOCIAL_NAVIGATION = "socialNavigation";
 
 /** The Constant SOCIAL_LINKS. */
 private static final String SOCIAL_LINKS = "socialLinks";
 
 /** The Constant LINK_LABEL. */
 private static final String LINK_LABEL = "linkLabel";
 
 /** The Constant LABEL. */
 private static final String LABEL = "label";
 
 /** The Constant LINK_URL. */
 private static final String LINK_URL = "linkUrl";
 
 /** The Constant LINK. */
 private static final String LINK = "link";
 
 /** The Constant TARGET. */
 private static final String TARGET = "target";
 
 /** The Constant WINDOW_TYPE. */
 private static final String WINDOW_TYPE = "windowType";
 
 /** The Constant OPEN_IN_NEW_WINDOW. */
 private static final String OPEN_IN_NEW_WINDOW = "openInNewWindow";
 
 /** The Constant COPY_RIGHT_TEXT. */
 private static final String COPY_RIGHT_TEXT = "copyrightText";
 /** The Constant COPY_RIGHT_TEXT. */
 private static final String TEXT = "text";
 
 /** The Constant COUNTRY_SELECTOR_LABEL. */
 private static final String COUNTRY_SELECTOR_LABEL = "countrySelectorLabel";
 
 /** The Constant COUNTRY_SELECTOR. */
 private static final String COUNTRY_SELECTOR = "countrySelector";
 
 /** The Constant COUNTRY_SELECTOR. */
 private static final String COUNTRY_INFO = "countryInfo";
 
 /** The Constant COUNTRY_SELECTOR_LINK. */
 private static final String COUNTRY_SELECTOR_LINK = "countrySelectorLink";
 
 /** The Constant MAIN_LOGO. */
 private static final String MAIN_LOGO = "mainLogo";
 
 /** The Constant COMPANY_LOGO. */
 private static final String COMPANY_LOGO = "companyLogo";
 
 /** The Constant COMPANY_ALT_TEXT. */
 private static final String COMPANY_ALT_TEXT = "companyAltText";
 
 /** The Constant UNILEVER_GLOBAL_URL. */
 private static final String UNILEVER_GLOBAL_URL = "unileverGlobalUrl";
 
 /** The Constant GLOBAL_URL_INFO. */
 private static final String GLOBAL_URL_INFO = "globalUrlInformation";
 
 /** The Constant HTML_EXTENSION. */
 private static final String HTML_EXTENSION = ".html";
 
 /** The Constant HTML_FULL_MENU_EXTENTION. */
 private static final String HTML_FULL_MENU_EXTENTION = ".fullmenu.html";
 
 /** The Constant URL. */
 private static final String URL = "url";
 
 /** The Constant Nav_Bar. */
 private static final String NAV_BAR = "navbar";
 
 /** The Constant back to top. */
 private static final String BACK_TO_TOP = "backtop";
 
 /** The Constant feature cute quote. */
 private static final String FEATURE_CUTE_QUOTE = "featureCuteQuote";
 
 /** The Constant LANGUAGE_SELECTOR. */
 private static final String LANGUAGE_SELECTOR = "languageSelector";
 /** The Constant LINK_LABEL. */
 private static final String COUNTRY_CONFIGURATION = "countryConfiguration";
 
 /** The Constant LINK_LABEL. */
 private static final String CODE = "code";
 /** The Constant CONTEXT_LABEL. */
 private static final String CONTEXT_LABEL = "contextLabel";
 /** The Constant CONTEXT_TYPE. */
 private static final String CONTEXT_TYPE = "contextType";
 /**
  * logger object.
  **/
 private static final Logger LOGGER = LoggerFactory.getLogger(GlobalFooterViewHelperImpl.class);
 /** The Constant LEGAL_TEXT. */
 private static final String LEGAL_TEXT = "legalText";
 
 /** The Constant IS_LINK_EXTERNAL. */
 private static final String IS_LINK_EXTERNAL = "isLinkExternal";
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /**
  * The resource resolver.
  * 
  * @param content
  *         the content
  * @param resources
  *         the resources
  * @return the map
  */
 
 /**
  * The method processData overrides method is responsible validating author input and returning a list of sections containing rich text,text This
  * method converts the multiwidget input (a String Array) into sections of maps so that it can be accessed in JSP in consistent way.This method also
  * validates the link url provided by author.
  * 
  * @param content
  *         , - {@link Map} of all user input received as input <code>ViewHelper.onGetData</code> method
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return {@link Map} holding the data
  */
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  
  LOGGER.debug("footer View Helper #processData called for processing fixed list.");
  Map<String, Object> props = getProperties(content);
  Map<String, Object> footerProperties = new LinkedHashMap<String, Object>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  
  ResourceResolver resolver = getResourceResolver(resources);
  SlingHttpServletRequest request = getSlingRequest(resources);
  
  Page currentPage = getCurrentPage(resources);
  Page actualPage = getActualPage(resources);
  
  I18n i18n = getI18n(resources);
  String fullMenuUrl = ComponentUtil.getFullURL(resolver, currentPage.getPath(), request);
  
  Map<String, Object> navBarMap = new HashMap<String, Object>();
  Map<String, Object> languageMap = new HashMap<String, Object>();
  
  if (actualPage != null) {
   languageMap = LanguageSelectorUtility.getLanguages(actualPage, request, resolver, configurationService);
  } else {
   languageMap = LanguageSelectorUtility.getLanguages(currentPage, request, resolver, configurationService);
  }
  
  if (!StringUtils.isEmpty(fullMenuUrl) && (fullMenuUrl.contains(HTML_EXTENSION))) {
   fullMenuUrl = fullMenuUrl.replace(HTML_EXTENSION, HTML_FULL_MENU_EXTENTION);
  }
  
  navBarMap.put(LABEL, i18n.get("globalNavigation.menu"));
  navBarMap.put(URL, fullMenuUrl);
  String legalText = MapUtils.getString(props, LEGAL_TEXT, StringUtils.EMPTY);
  languageMap.put(LanguageSelectorUtility.IS_ENABLED,
    LanguageSelectorUtility.isLanguageSelectorEnabledInFooter(getCurrentPage(resources), configurationService));
  
  footerProperties.put(QUOTES, getCuteQuoteProperties(props));
  footerProperties.put(MAIN_LOGO, getMainLogoProperties(props, resolver, request));
  footerProperties.put(SOCIAL_NAVIGATION, getSocialNavigationProperties(props, getCurrentResource(resources)));
  footerProperties.put(FOOTER_LINKS, getfooterProperties(resolver, getCurrentResource(resources), request, currentPage));
  
  footerProperties.put(NAV_BAR, navBarMap);
  footerProperties.put(BACK_TO_TOP, i18n.get("footer.backToTop"));
  footerProperties.put(FEATURE_CUTE_QUOTE, getRandomCuteQuoteProperty(props));
  
  footerProperties.put(AD_CHOICE, getAdChoiceDetails(currentPage, props));
  footerProperties.put(LANGUAGE_SELECTOR, languageMap);
  footerProperties.put(COMPANY, getCompanyProperties(props, currentPage, resources));
  footerProperties.put(LEGAL_TEXT, legalText);
  LOGGER.info("The Footer Map is successfully generated");
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(getJsonNameSpace(content), footerProperties);
  
  return data;
  
 }
 
 /**
  * Gets the main logo properties.
  * 
  * @param properties
  *         the properties
  * @param currentPage
  *         the current page
  * @param analyticsCategory
  *         the analytics category
  * @param analyticsAction
  *         the analytics action
  **/
 private Map<String, Object> getAdChoiceDetails(Page currentPage, Map<String, Object> properties) {
  
  Map<String, Object> adChoiceMap = new HashMap<String, Object>();
  String isEnabled = StringUtils.EMPTY;
  String adChoicePId = StringUtils.EMPTY;
  String adChoiceClientId = StringUtils.EMPTY;
  String adChoiceLabel = MapUtils.getString(properties, AD_CHOICE_FIELD_LABEL) != null ? MapUtils.getString(properties, AD_CHOICE_FIELD_LABEL)
    : StringUtils.EMPTY;
  try {
   isEnabled = configurationService.getConfigValue(currentPage, AD_CHOICE_CAT, IS_ENABLED);
   adChoicePId = configurationService.getConfigValue(currentPage, AD_CHOICE_CAT, AD_CHOICE_PID);
   adChoiceClientId = configurationService.getConfigValue(currentPage, AD_CHOICE_CAT, AD_CHOICE_CLIENT_ID);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find  key for adChoice category." + "The following keys are needed-isEnabled,adChoicePId,adChoiceClientId", e);
  }
  if ("yes".equalsIgnoreCase(isEnabled.trim()) || "true".equalsIgnoreCase(isEnabled.trim())) {
   adChoiceMap.put(IS_ENABLED, true);
  } else {
   adChoiceMap.put(IS_ENABLED, false);
  }
  adChoiceMap.put(AD_CHOICE_PID, adChoicePId);
  adChoiceMap.put(AD_CHOICE_CLIENT_ID, adChoiceClientId);
  adChoiceMap.put(AD_CHOICE_LABEL, adChoiceLabel);
  return adChoiceMap;
 }
 
 /**
  * Gets the main logo properties.
  * 
  * @param properties
  *         the properties
  * @param resourceResolver
  *         the resource resolver
  * @return the main logo properties
  */
 private Map<String, Object> getMainLogoProperties(Map<String, Object> properties, ResourceResolver resourceResolver,
   SlingHttpServletRequest slingRequest) {
  Map<String, Object> mainLogoProperties = new LinkedHashMap<String, Object>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  String brandLogoIcon = MapUtils.getString(properties, BRAND_LOGO_ICON, StringUtils.EMPTY);
  String brandLogoAlt = MapUtils.getString(properties, BRAND_ALT_TEXT, StringUtils.EMPTY);
  String brandLogoLink = MapUtils.getString(properties, BRAND_LOGO_LINK, StringUtils.EMPTY);
  brandLogoLink = ComponentUtil.getFullURL(resourceResolver, brandLogoLink, slingRequest);
  mainLogoProperties.put(BRAND_LOGO_ICON, brandLogoIcon);
  mainLogoProperties.put(TARGET, brandLogoLink);
  mainLogoProperties.put(ALT, brandLogoAlt);
  LOGGER.debug("Main Logo Properties Map : " + mainLogoProperties);
  return mainLogoProperties;
  
 }
 
 /**
  * Gets the cute quote properties.
  * 
  * @param properties
  *         the properties
  * @return the cute quote properties
  */
 private List<String> getCuteQuoteProperties(Map<String, Object> properties) {
  List<String> cuteQuotePropertyName = new ArrayList<String>();
  cuteQuotePropertyName.add(CUTE_QUOTES);
  LOGGER.debug("cute quotes properties : " + cuteQuotePropertyName);
  List<Map<String, String>> cuteQuoteSectionList = CollectionUtils.convertMultiWidgetToList(properties, cuteQuotePropertyName);
  int size = cuteQuoteSectionList.size();
  List<String> list = new ArrayList<String>();
  for (int i = 0; i < size; i++) {
   Map<String, String> cuteQuoteMap = cuteQuoteSectionList.get(i);
   list.add(cuteQuoteMap.get("cuteQuote").toString());
  }
  LOGGER.debug("Cute Quote Properties List size : " + list.size());
  return list;
 }
 
 /**
  * Gets the cute quote property from list of cute quotes randomly.
  * 
  * @param properties
  *         the properties
  * @return the cute quote property
  */
 private String getRandomCuteQuoteProperty(Map<String, Object> properties) {
  String featurephoneCuteQuote = StringUtils.EMPTY;
  List<String> list = getCuteQuoteProperties(properties);
  if (org.apache.commons.collections.CollectionUtils.isNotEmpty(list)) {
   int i = new SecureRandom().nextInt(list.size());
   featurephoneCuteQuote = list.get(i).isEmpty() ? StringUtils.EMPTY : list.get(i);
  }
  LOGGER.debug("feature cute quote for adaptive is " + featurephoneCuteQuote);
  return featurephoneCuteQuote;
 }
 
 /**
  * Gets the logo properties.
  * 
  * @param properties
  *         the properties
  * @param currentPage
  *         the current page
  * @param analyticsCategory
  *         the analytics category
  * @param analyticsAction
  *         the analytics action
  * @return the logo properties
  */
 private Map<String, Object> getLogoProperties(Map<String, Object> properties) {
  String companyLogo = MapUtils.getString(properties, COMPANY_LOGO, StringUtils.EMPTY);
  String companyAltText = MapUtils.getString(properties, COMPANY_ALT_TEXT, StringUtils.EMPTY);
  String unileverGlobalUrl = MapUtils.getString(properties, UNILEVER_GLOBAL_URL, StringUtils.EMPTY);
  String globalUrlInformation = MapUtils.getString(properties, GLOBAL_URL_INFO, StringUtils.EMPTY);
  Map<String, Object> logoProperties = new LinkedHashMap<String, Object>();
  logoProperties.put(IMAGE_URL, companyLogo);
  logoProperties.put(ALT_IMAGE, companyAltText);
  logoProperties.put(TARGET, unileverGlobalUrl);
  if (StringUtils.isBlank(globalUrlInformation)) {
   globalUrlInformation = companyAltText;
  }
  LOGGER.debug("Unilever Logo Properties map : " + logoProperties);
  return logoProperties;
 }
 
 /**
  * Gets the company properties.
  * 
  * @param properties
  *         the properties
  * 
  * @return the company properties
  */
 private Map<String, Object> getCompanyProperties(Map<String, Object> properties, Page currentPage, Map<String, Object> resources) {
  Map<String, Object> companyProperties = new LinkedHashMap<String, Object>();
  Map<String, Object> copyProperties = new LinkedHashMap<String, Object>();
  copyProperties.put(TEXT,
    ComponentUtil.getCopyrightSignWithYear() + UnileverConstants.SPACE + MapUtils.getString(properties, COPY_RIGHT_TEXT, StringUtils.EMPTY));
  copyProperties.put(LOGO, getLogoProperties(properties));
  
  companyProperties.put(COUNTRY_SELECTOR, getCountryProperties(properties, currentPage, resources));
  
  companyProperties.put("copyright", copyProperties);
  LOGGER.debug("Copyright Properties map : " + companyProperties);
  return companyProperties;
 }
 
 /**
  * Gets the country properties.
  * 
  * @param properties
  *         the properties
  * @return the country properties
  */
 private Map<String, Object> getCountryProperties(Map<String, Object> properties, Page currentPage, Map<String, Object> resources) {
  Map<String, Object> countryProperties = new LinkedHashMap<String, Object>();
  String countrySelectorLink = MapUtils.getString(properties, COUNTRY_SELECTOR_LINK, StringUtils.EMPTY);
  String countrySelectorFullLink = ComponentUtil.getFullURL(getResourceResolver(resources), countrySelectorLink);
  countryProperties.put(TARGET, countrySelectorFullLink);
  countryProperties.put("countryLabel", MapUtils.getString(properties, COUNTRY_SELECTOR_LABEL, StringUtils.EMPTY));
  countryProperties.put(FLAG_SVG,
    GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, COUNTRY_CONFIGURATION, CODE));
  String countryInfo = MapUtils.getString(properties, COUNTRY_INFO, StringUtils.EMPTY);
  if (StringUtils.isBlank(countryInfo)) {
   countryInfo = COUNTRY_SELECTOR_LABEL;
  }
  LOGGER.debug("Country Selector Properties map : " + countryProperties);
  return countryProperties;
  
 }
 
 /**
  * Gets the footer properties.
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param currentResource
  *         the current resource
  * @param currentPage
  * @return the footer properties
  */
 private Map<String, Object> getfooterProperties(ResourceResolver resourceResolver, Resource currentResource, SlingHttpServletRequest slingRequest,
   Page currentPage) {
  List<Map<String, Object>> footerList = new ArrayList<Map<String, Object>>();
  
  List<String> footerFieldPropertyName = new ArrayList<String>();
  footerFieldPropertyName.add(LINK_LABEL);
  footerFieldPropertyName.add(LINK_URL);
  footerFieldPropertyName.add(FOOTER_INFO);
  footerFieldPropertyName.add(WINDOW_TYPE);
  
  LOGGER.debug("footer field properties : " + footerFieldPropertyName);
  
  List<Map<String, String>> list = ComponentUtil.getNestedMultiFieldProperties(currentResource, "footerJson");
  
  for (Map<String, String> map : list) {
   Map<String, Object> footerMap = new LinkedHashMap<String, Object>();
   footerMap.put(LABEL, StringUtils.defaultString(map.get(LINK_LABEL), StringUtils.EMPTY));
   String linkUrl = StringUtils.defaultString(map.get(LINK_URL), StringUtils.EMPTY);
   linkUrl = ComponentUtil.getFullURL(resourceResolver, linkUrl, slingRequest);
   String contextLabel = StringUtils.defaultString(map.get(CONTEXT_LABEL), StringUtils.EMPTY);
   String contextType = StringUtils.defaultString(map.get(CONTEXT_TYPE), StringUtils.EMPTY);
   footerMap.put(LINK, linkUrl);
   String fullUrl = ComponentUtil.getFullURL(resourceResolver, linkUrl, slingRequest);
   footerMap.put(IS_LINK_EXTERNAL, ComponentUtil.isUrlExternal(resourceResolver, slingRequest, currentPage, fullUrl));
   footerMap.put(OPEN_IN_NEW_WINDOW, StringUtils.defaultString(map.get(WINDOW_TYPE), StringUtils.EMPTY));
   footerMap.put(CONTEXT_LABEL, contextLabel);
   footerMap.put(CONTEXT_TYPE, contextType);
   String footerInfo = StringUtils.defaultString(map.get(FOOTER_INFO), StringUtils.EMPTY);
   if (StringUtils.isBlank(footerInfo)) {
    footerInfo = StringUtils.defaultString(map.get(LINK_LABEL), StringUtils.EMPTY);
   }
   footerList.add(footerMap);
  }
  
  // adding list to list of map
  Map<String, Object> footerLinkProperties = new LinkedHashMap<String, Object>();
  footerLinkProperties.put("linksList", footerList.toArray());
  LOGGER.debug("Footer Link Navigation Properties : " + footerLinkProperties.size());
  return footerLinkProperties;
 }
 
 /**
  * Gets the social navigation properties.
  * 
  * @param properties
  *         the properties
  * 
  * @param currentResource
  *         the current resource
  * @return the social navigation properties
  */
 private Map<String, Object> getSocialNavigationProperties(Map<String, Object> properties, Resource currentResource) {
  
  String socialTitle = MapUtils.getString(properties, SOCIAL_TITLE, StringUtils.EMPTY);
  List<Map<String, Object>> socialList = new ArrayList<Map<String, Object>>();
  
  List<Map<String, String>> list = ComponentUtil.getNestedMultiFieldProperties(currentResource, "socialShareJson");
  for (Map<String, String> map : list) {
   Map<String, Object> socialProperties = new LinkedHashMap<String, Object>();
   socialProperties.put(ICON_NAME, StringUtils.defaultString(map.get(ICON_NAME), StringUtils.EMPTY));
   socialProperties.put(ICON_IMAGE, StringUtils.defaultString(map.get(ICON_IMAGE), StringUtils.EMPTY));
   socialProperties.put(TARGET, StringUtils.defaultString(map.get(SOCIAL_LINK), StringUtils.EMPTY));
   socialProperties.put(ALT, StringUtils.defaultString(map.get(ALT_TEXT), StringUtils.EMPTY));
   socialProperties.put(UnileverConstants.CONTAINER_TAG, getSocialLinkContainerTag(StringUtils.defaultString(map.get(ICON_NAME), StringUtils.EMPTY)));
   socialList.add(socialProperties);
  }
  
  // adding list to list of map
  Map<String, Object> socialNavigationProperties = new LinkedHashMap<String, Object>();
  socialNavigationProperties.put(SOCIAL_TITLE, socialTitle);
  socialNavigationProperties.put(SOCIAL_LINKS, socialList.toArray());
  LOGGER.debug("Social share link Properties : " + socialNavigationProperties.size());
  return socialNavigationProperties;
 }
 
 /**
  * Gets the social link container tag.
  * 
  * @param iconName
  *         the icon name
  * @return the social link container tag
  */
 private Map<String, Object> getSocialLinkContainerTag(String iconName) {
  Map<String, Object> map = new HashMap<String, Object>();
  map.put(ContainerTagConstants.ACTION, iconName);
  map.put(ContainerTagConstants.LABEL, iconName);
  map.put(ContainerTagConstants.CATEGORY, "referral");
  map.put(ContainerTagConstants.TYPE, "trackEvent");
  return map;
 }
 
}
