/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.search.QueryBuilder;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.aem.foundation.core.service.SearchService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.components.helper.ProductHelperExt;
import com.unilever.platform.foundation.components.helper.StoreLocatorUtility;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.Image;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;
import com.unilever.platform.foundation.viewhelper.constants.StoreLocatorConstants;

/**
 * The Class StoreSearchViewHelperImpl.
 * 
 * @author mkathu
 */
@Component(description = "StoreSearchViewHelperImpl", immediate = true, metatype = true, label = "StoreSearchViewHelperImpl")
@Service(value = { StoreSearchViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.STORE_SEARCH, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class StoreSearchViewHelperImpl extends BaseViewHelper {
 /** The Constant SEARCH_IPP. */
 private static final String SEARCH_IPP = "searchIPP";
 
 /** The Constant SUGGESTION_IPP. */
 private static final String SUGGESTION_IPP = "suggestionIPP";
 
 /** The Constant SEARCH_RETIVAL_HANDLER. */
 private static final String SEARCH_RETIVAL_HANDLER = "searchRetrivalHandler";
 
 /** The Constant FIVE. */
 private static final int FIVE = 5;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(StoreSearchViewHelperImpl.class);
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The builder. */
 @Reference
 private QueryBuilder builder;
 
 /** The builder. */
 @Reference
 private SearchService searchservice;
 
 /** The Constant STORE_SEARCH. */
 private static final String STORE_SEARCH = "storeSearch";
 
 /** The Constant PRODUCT. */
 private static final String PRODUCT = "product";
 
 /** The Constant PRODUCT_COMMERCE_PATH. */
 private static final String PRODUCT_COMMERCE_PATH = "jcr:content/product/cq:commerceType";
 
 /** The Constant CATEGORIES. */
 private static final String CATEGORIES = "categories";
 
 /** The Constant CATEGORY_PAGE_URL. */
 private static final String CATEGORY_PAGE_URL = "categoryPageUrl";
 
 /** The Constant NAME. */
 private static final String NAME = "name";
 
 /** The Constant PAGE_PATH. */
 private static final String PAGE_PATH = "pagePath";
 
 /** The Constant PRODUCTS. */
 private static final String PRODUCTS = "products";
 
 /** The Constant IMAGE. */
 private static final String IMAGE = "image";
 
 /** The Constant SUB_CATGORIES. */
 private static final String SUB_CATGORIES = "subCategory";
 
 /** The Constant CATEGORY_TEMPLATE. */
 private static final String CATEGORY_TEMPLATE = "categoryTemplate";
 
 /** The Constant PRODUCT_DATA_PATH. */
 private static final String PRODUCT_DATA_PATH = "productDataPath";
 
 /** The Constant STORE_LOCATER. */
 private static final String STORE_LOCATOR = "storeLocator";
 
 /** The Constant CURRENT_PAGE. */
 private static final String CURRENT_PAGE = "currentPage";
 
 /** The Constant COUNTRY_CODE. */
 public static final String COUNTRY_CODE = "countryCode";
 /** The Constant CATEGORY_SELECT_DEFAULT_LABEL. */
 private static final String CATEGORY_SELECT_DEFAULT_LABEL = "storeSearch.categorySelectDefaultLabel";
 
 /** The Constant CATEGORY_CATEGORY_SELECT_DEFAULT_KEY. */
 private static final String CATEGORY_SELECT_DEFAULT_KEY = "categorySelectDefaultLabel";
 /** The Constant PRODUCt_LIST_HEADING_LABEL. */
 private static final String PRODUCT_LIST_HEADING_LABEL = "storeSearch.productListHeading";
 /** The Constant PRODUCt_LIST_HEADING_LABEL. */
 private static final String PRODUCT_LIST_HEADING_KEY = "productListHeading";
 
 /** The Constant SHOW_MORE_CTA_LABEL. */
 private static final String SHOW_MORE_CTA_LABEL = "storeSearch.showMoreCtaLabel";
 /** The Constant SHOW_LESS_CTA_LABEL. */
 private static final String SHOW_LESS_CTA_LABEL = "storeSearch.showLessCtaLabel";
 /** The Constant SHOW_LESS_CTA_KEY. */
 private static final String SHOW_LESS_CTA_KEY = "showLessCtaLabel";
 /** The Constant STORE_FINDER_HEADING. */
 private static final String STORE_FINDER_HEADING = "storeSearch.storeFinderHeading";
 /** The Constant STORE_FINDER_KEY. */
 private static final String STORE_FINDER_KEY = "storeFinderHeading";
 /** The Constant STORE_FINDER_CTA_LABEL. */
 private static final String STORE_FINDER_CTA_LABEL = "storeSearch.storeFinderCtaLabel";
 /** The Constant STORE_FINDER_CTA_KEY. */
 private static final String STORE_FINDER_CTA_KEY = "storeFinderCtaLabel";
 /** The Constant ZIPCODE_FIELD_HELP_TEXT. */
 private static final String ZIPCODE_FIELD_HELP_TEXT = "storeSearch.zipcodeField.helpText";
 /** The Constant ZIPCODE_FIELD_HELP_KEY. */
 private static final String ZIPCODE_FIELD_HELP_KEY = "zipcodeFieldText";
 /** The Constant SEARCH_RADIUS_PREFIX_TEXT. */
 private static final String SEARCH_RADIUS_PREFIX_TEXT = "storeSearch.searchRadiusField.prefixText";
 /** The Constant SEARCH_RADIUS_PREFIX_KEY. */
 private static final String SEARCH_RADIUS_PREFIX_KEY = "searchRadiusprefix";
 /** The Constant SEARCH_RADIUS_METRICS_LABEL. */
 private static final String SEARCH_RADIUS_METRICS_LABEL = "storeSearch.searchRadiusField.metricsLabel";
 /** The Constant SEARCH_RADIUS_METRICS_KEY. */
 private static final String SEARCH_RADIUS_METRICS_KEY = "searchRadiusmetrics";
 /** The Constant BUYONLINE_PREFIX_TEXT. */
 private static final String BUYONLINE_PREFIX_TEXT = "storeSearch.buyOnline.prefixText";
 /** The Constant BUYONLINE_PREFIX_KEY. */
 private static final String BUYONLINE_PREFIX_KEY = "buyOnlineprefix";
 /** The Constant BUYONLINE_CTA_LABEL. */
 private static final String BUYONLINE_CTA_LABEL = "storeSearch.buyOnline.ctaLabel";
 /** The Constant BUYONLINE_CTA_KEY. */
 private static final String BUYONLINE_CTA_KEY = "buyOnlinectaLabel";
 /** The Constant CHOOSE_CATEGORY_LABEL_KEY. */
 private static final String CHOOSE_CATEGORY_LABEL_KEY = "storeSearch.chooseCategoryLabel";
 /** The Constant CHOOSE_CATEGORY_LABEL. */
 private static final String CHOOSE_CATEGORY_LABEL = "chooseCategoryLabel";
 
 /** The Constant CHOOSE_PRODUCT_LABEL_KEY. */
 private static final String CHOOSE_PRODUCT_LABEL_KEY = "storeSearch.chooseProductLabel";
 /** The Constant CHOOSE_PRODUCT_LABEL. */
 private static final String CHOOSE_PRODUCT_LABEL = "chooseProductLabel";
 /** The Constant ZIP_CODE_LABEL_KEY. */
 private static final String ZIP_CODE_LABEL_KEY = "storeSearch.chooseZipCodeLabel";
 /** The Constant ZIP_CODE_LABEL. */
 private static final String ZIP_CODE_LABEL = "chooseZipCodeLabel";
 
 /** The Constant SHOW_MORE_CTA_KEY. */
 private static final String SHOW_MORE_CTA_KEY = "showMoreCtaLabel";
 
 /** The Constant CONTENT_TYPE. */
 private static final String CONTENT_TYPE = "contentType";
 
 /** The Constant ZERO. */
 private static final int ZERO = 0;
 
 /** The Constant CATEGORY_CONTENT_TYPE. */
 private static final String CATEGORY_CONTENT_TYPE = "categoryContentType";
 
 /** The Constant HUNDRED. */
 private static final int HUNDRED = 100;
 /**
  * The method processData overrides method is responsible validating author input and returning a list of product catogories.
  * 
  * @param content
  *         {@link Map} of all user input received as input
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return Map holding the result data.
  */
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  LOGGER.debug("StoreSearch View Helper #processData called for processing fixed list.");
  
  Map<String, Object> categoryMapProps = new HashMap<String, Object>();
  List<Map<String, Object>> categoryPageList = new ArrayList<Map<String, Object>>();
  List<Map<String, Object>> productList = new ArrayList<Map<String, Object>>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  Page currentPage = getCurrentPage(resources);
  I18n i18n = getI18n(resources);
  ResourceResolver resourceResolver = getResourceResolver(resources);
  Map<String, String> storeSearchConfigurations = GlobalConfigurationUtility.getMapFromConfiguration(configurationService, currentPage, STORE_SEARCH);
  
  int loadMoreCount = MapUtils.getIntValue(storeSearchConfigurations, "loadMoreCount", FIVE);
  
  categoryMapProps.put(CURRENT_PAGE, currentPage.getPath());
  String titleText = MapUtils.getString(getProperties(content), "headingText", StringUtils.EMPTY);
  categoryMapProps.put("title", titleText);
  String subTitleText = MapUtils.getString(getProperties(content), "subHeadingText", StringUtils.EMPTY);
  categoryMapProps.put("subTitle", subTitleText);
  categoryMapProps.put("loadMoreCount", loadMoreCount);
  Boolean isProductHide = MapUtils.getBoolean(getProperties(content), "isProductCategory", false);
  if (isProductHide) {
   categoryMapProps.put("hideProductCategoryFlag", isProductHide);
   List<Page> productPages = getProductPages(resources);
   for (Page productPage : productPages) {
    if (productPage != null) {
     Map<String, Object> productProperties = ProductHelperExt.getProductTeaserData(productPage, currentPage, getSlingRequest(resources));
     productList.add(productProperties);
    }
   }
   LOGGER.info("The number of product page list defined is " + productList.size());
   
  } else {
   categoryPageList = getCategoryList(content, resources, storeSearchConfigurations);
   LOGGER.info("The number of category page list defined is " + categoryPageList.size());
  }
  
  String productDataPath = storeSearchConfigurations.get(PRODUCT_DATA_PATH);
  String servletPath = StringUtils.isNotBlank(productDataPath) ? getResourceResolver(resources).map(productDataPath) : "";
  
  categoryMapProps.put(PRODUCT_DATA_PATH, servletPath);
  categoryMapProps.put(ProductConstants.REVIEW_MAP, ProductHelper.getReviewMapProperties(configurationService, currentPage));
  
  categoryMapProps.put(STORE_LOCATOR, StoreLocatorUtility.getfieldsMap(currentPage, configurationService, i18n));
  categoryMapProps.put(StoreLocatorConstants.DEFAULT_RADIOUS_FOR_SEARCH, GlobalConfigurationUtility.getValueFromConfiguration(configurationService,
    getCurrentPage(resources), StoreLocatorConstants.GLOBAL_CONFIG_CAT, StoreLocatorConstants.DEFAULT_RADIOUS_FOR_SEARCH));
  
  categoryMapProps.put("staticText", getStaticMap(i18n));
  categoryMapProps.put(PRODUCTS, productList.toArray());
  
  categoryMapProps.put(CATEGORIES, categoryPageList.toArray());
  if (categoryPageList.isEmpty()) {
   categoryMapProps.put("isCategoryListEmpty", true);
  } else {
   categoryMapProps.put("isCategoryListEmpty", false);
  }
  categoryMapProps.put(StoreLocatorConstants.GEO_CODE_API, GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
    StoreLocatorConstants.GLOBAL_CONFIG_CAT, StoreLocatorConstants.GEO_CODE_API));
  
  categoryMapProps.put(SEARCH_RETIVAL_HANDLER,
    resourceResolver.map(MapUtils.getString(storeSearchConfigurations, SEARCH_RETIVAL_HANDLER, StringUtils.EMPTY)));
  categoryMapProps.put(SUGGESTION_IPP, MapUtils.getIntValue(storeSearchConfigurations, SUGGESTION_IPP, FIVE));
  categoryMapProps.put(SEARCH_IPP, MapUtils.getIntValue(storeSearchConfigurations, SEARCH_IPP, HUNDRED));
  categoryMapProps.put("contentType", UnileverConstants.PRODUCT);
  boolean geoCodeFlag = Boolean.parseBoolean(GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, "storeLocator",
    "geoCodeFlag"));
  
  categoryMapProps.put("geoCodeFlag", geoCodeFlag);
  
  String storeLocatorResultUrl = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
    StoreLocatorConstants.GLOBAL_CONFIG_CAT, StoreLocatorConstants.CTA_URL);
  categoryMapProps.put("storeLocatorResultUrl", ComponentUtil.getFullURL(resourceResolver, storeLocatorResultUrl, getSlingRequest(resources)));
  
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(getJsonNameSpace(content), categoryMapProps);
  LOGGER.debug("The Store Search final data map returned is " + data.size());
  return data;
 }
 
 /**
  * Gets the static map.
  * 
  * @param i18n
  *         the i18n
  * @return the static map
  */
 private Map<String, Object> getStaticMap(I18n i18n) {
  Map<String, Object> staticText = new HashMap<String, Object>();
  staticText.put(CATEGORY_SELECT_DEFAULT_KEY, i18n.get(CATEGORY_SELECT_DEFAULT_LABEL));
  staticText.put(PRODUCT_LIST_HEADING_KEY, i18n.get(PRODUCT_LIST_HEADING_LABEL));
  staticText.put(SHOW_MORE_CTA_KEY, i18n.get(SHOW_MORE_CTA_LABEL));
  staticText.put(SHOW_LESS_CTA_KEY, i18n.get(SHOW_LESS_CTA_LABEL));
  staticText.put(STORE_FINDER_KEY, i18n.get(STORE_FINDER_HEADING));
  staticText.put(STORE_FINDER_CTA_KEY, i18n.get(STORE_FINDER_CTA_LABEL));
  staticText.put(ZIPCODE_FIELD_HELP_KEY, i18n.get(ZIPCODE_FIELD_HELP_TEXT));
  staticText.put(SEARCH_RADIUS_PREFIX_KEY, i18n.get(SEARCH_RADIUS_PREFIX_TEXT));
  staticText.put(SEARCH_RADIUS_METRICS_KEY, i18n.get(SEARCH_RADIUS_METRICS_LABEL));
  staticText.put(BUYONLINE_PREFIX_KEY, i18n.get(BUYONLINE_PREFIX_TEXT));
  staticText.put(BUYONLINE_CTA_KEY, i18n.get(BUYONLINE_CTA_LABEL));
  staticText.put(CHOOSE_CATEGORY_LABEL, i18n.get(CHOOSE_CATEGORY_LABEL_KEY));
  staticText.put(ZIP_CODE_LABEL, i18n.get(ZIP_CODE_LABEL_KEY));
  staticText.put(CHOOSE_PRODUCT_LABEL, i18n.get(CHOOSE_PRODUCT_LABEL_KEY));
  staticText.put("ctaLabel", i18n.get("storeSearch.ctaLabel"));
  staticText.put("getDirectionLabel", i18n.get("storeSearch.getDirectionLabel"));
  staticText.put("gpsShareLocationCtaLabel", i18n.get("storeSearch.gpsShareLocationCtaLabel"));
  staticText.put("getDirectionLabel", i18n.get("storeSearch.getDirectionLabel"));
  staticText.put("headingText", i18n.get("storeSearch.headingText"));
  staticText.put("productSelectCtaLabel", i18n.get("storeSearch.productSelectCtaLabel"));
  staticText.put("showAllCtaLabel", i18n.get("storeSearch.showAllCtaLabel"));
  staticText.put("zipInputFieldLabel", i18n.get("storeSearch.zipcodeField.zipInputFieldLabel"));
  staticText.put("searchOptionHeadingText", i18n.get("storeSearch.searchOptionHeadingText"));
  staticText.put("browseOptionHeadingText", i18n.get("storeSearch.browseOptionHeadingText"));
  staticText.put("searchInputFieldLabel", i18n.get("storeSearch.productSearch.searchInputFieldLabel"));
  staticText.put("searchInputFieldHelpText", i18n.get("storeSearch.productSearch.searchInputFieldHelpText"));
  staticText.put("searchCtaLabel", i18n.get("storeSearch.productSearch.searchCtaLabel"));
  staticText.put("noSearchResultsMessagetext", i18n.get("storeSearch.productSearch.noSearchResultsMessagetext"));
  staticText.put("changeProductSelectionLabel", i18n.get("storeSearch.productSearch.changeProductSelectionLabel"));
  staticText.put("categorySelectLevelOneLabel", i18n.get("storeSearch.categorySelectLevelOneLabel"));
  staticText.put("categorySelectLevelTwoLabel", i18n.get("storeSearch.categorySelectLevelTwoLabel"));
  staticText.put("categorySelectLevelThreeLabel", i18n.get("storeSearch.categorySelectLevelThreeLabel"));
  
  return staticText;
 }
 
 /**
  * Gets the category list.
  * 
  * @param content
  *         the content
  * @param resources
  *         the resources
  * @param storeSearchConfigurations
  *         the storeSearch configurations
  * @return the category list
  */
 private List<Map<String, Object>> getCategoryList(Map<String, Object> content, final Map<String, Object> resources,
   Map<String, String> storeSearchConfigurations) {
  List<Map<String, Object>> categoryList = new ArrayList<Map<String, Object>>();
  List<String> categoryPageName = new ArrayList<String>();
  categoryPageName.add(CATEGORY_PAGE_URL);
  List<Map<String, String>> categoryPageList = com.sapient.platform.iea.aem.core.utils.CollectionUtils.convertMultiWidgetToList(
    getProperties(content), categoryPageName);
  int size = categoryPageList != null ? categoryPageList.size() : 0;
  List<String> list = new ArrayList<String>();
  for (int i = 0; i < size; i++) {
   Map<String, String> categoryPageMap = categoryPageList.get(i);
   String pageUrl = categoryPageMap.get(CATEGORY_PAGE_URL).toString();
   Page categoryPage = getPageManager(resources).getPage(pageUrl);
   Map<String, Object> categoryMap = new LinkedHashMap<String, Object>();
   categoryList.add(getCategoryMap(categoryPage, storeSearchConfigurations, categoryMap, resources));
  }
  LOGGER.debug("categories map list size : " + list.size());
  
  return categoryList;
 }
 
 /**
  * Gets the category map.
  * 
  * @param page
  *         the page
  * @param storeSearchConfigurations
  *         the store Search configurations
  * @param categoryMap
  *         the category map
  * @param resources
  *         the resources
  * @return the category map
  */
 private Map<String, Object> getCategoryMap(Page page, Map<String, String> storeSearchConfigurations, Map<String, Object> categoryMap,
   Map<String, Object> resources) {
  
  if (page != null && isCategoryPage(page, storeSearchConfigurations)) {
   categoryMap.put(NAME, page.getTitle());
   categoryMap.put(PAGE_PATH, page.getPath());
   ValueMap pageProperties = page.getProperties();
   String teaserImagePath = pageProperties.get(UnileverConstants.TEASER_IMAGE, null);
   
   if (StringUtils.isNotBlank(teaserImagePath)) {
    String teaserImageAlt = pageProperties.get(UnileverConstants.TEASER_IMAGE_ALT_TEXT, "");
    categoryMap.put(IMAGE, new Image(teaserImagePath, teaserImageAlt, "", getCurrentPage(resources), getSlingRequest(resources)));
   }
   
   Iterator<Page> categoryPageIterator = page.listChildren();
   List<Map<String, Object>> categoryMapList = new ArrayList<Map<String, Object>>();
   while (categoryPageIterator.hasNext()) {
    Page categoryPage = categoryPageIterator.next();
    if (isCategoryPage(categoryPage, storeSearchConfigurations)) {
     Map<String, Object> childCategoryMap = new LinkedHashMap<String, Object>();
     categoryMapList.add(getCategoryMap(categoryPage, storeSearchConfigurations, childCategoryMap, resources));
    }
   }
   if (CollectionUtils.isNotEmpty(categoryMapList)) {
    categoryMap.put(SUB_CATGORIES, categoryMapList.toArray());
   }
  }
  return categoryMap;
 }
 
 /**
  * Checks if is category page.
  * 
  * @param categoryPage
  *         the category page
  * @param categoryListingConfigurations
  *         the category listing configurations
  * @return the boolean
  */
 protected Boolean isCategoryPage(Page categoryPage, Map<String, String> categoryListingConfigurations) {
  String[] categoryContentTypes = StringUtils.split(getStringValuefromMap(categoryListingConfigurations, CATEGORY_CONTENT_TYPE), ",");
  boolean isCategoryPage = false;
  if (categoryContentTypes.length > ZERO) {
   for (String categoryContentType : categoryContentTypes) {
    ValueMap pageProperties = categoryPage != null ? categoryPage.getProperties() : null;
    String contentType = MapUtils.getString(pageProperties, CONTENT_TYPE, null);
    if (contentType != null && contentType.equals(categoryContentType.trim())) {
     isCategoryPage = true;
    }
   }
   return isCategoryPage;
  }
  
  String[] properties = StringUtils.split(getStringValuefromMap(categoryListingConfigurations, CATEGORY_TEMPLATE), ",");
  for (String property : properties) {
   ValueMap pageProperties = categoryPage != null ? categoryPage.getProperties() : null;
   String templateName = MapUtils.getString(pageProperties, NameConstants.PN_TEMPLATE, null);
   if (templateName != null && templateName.endsWith(property)) {
    isCategoryPage = true;
   }
  }
  return isCategoryPage;
 }
 
 /**
  * Gets the product pages.
  * 
  * @param resources
  *         the resources
  * @return the product pages
  */
 private List<Page> getProductPages(Map<String, Object> resources) {
  List<Map<String, String>> pageCategoryList = getPageCategoryMap(getCurrentResource(resources));
  List<Page> productPageList = new ArrayList<Page>();
  for (Map<String, String> map : pageCategoryList) {
   String path = StringUtils.EMPTY;
   path = StringUtils.defaultString(map.get("categoryPageUrl"), StringUtils.EMPTY);
   if (StringUtils.isNotEmpty(path)) {
    Session session = null;
    try {
     session = getCurrentPage(resources).adaptTo(Node.class).getSession();
     productPageList = searchservice.getServicePages(session, path, PRODUCT_COMMERCE_PATH, PRODUCT, builder);
    } catch (RepositoryException e) {
     LOGGER.debug("Exception in getting session" + session + e);
    }
   }
  }
  
  return productPageList;
 }
 
 /**
  * Gets the getPageCategoryMap.
  * 
  * @param currentResource
  *         the current resource
  * @return the getPageCategoryMap
  */
 private List<Map<String, String>> getPageCategoryMap(Resource currentResource) {
  return ComponentUtil.getNestedMultiFieldProperties(currentResource, "pageCategoryJson");
 }
 
 /**
  * Gets the string value from map.
  * 
  * @param map
  *         the map
  * @param key
  *         the key
  * @return the string value from map
  */
 private static String getStringValuefromMap(Map<String, String> map, String key) {
  
  String value = StringUtils.EMPTY;
  if (map.containsKey(key)) {
   value = map.get(key).toString();
  }
  return value;
 }
 
}
