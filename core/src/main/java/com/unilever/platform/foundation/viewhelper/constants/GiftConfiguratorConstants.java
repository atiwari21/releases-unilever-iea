/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.constants;

/**
 * The Class GiftConfiguratorConstants.
 */
public class GiftConfiguratorConstants {
 /** The Constant GLOBAL_CONFIG_CAT. */
 public static final String GLOBAL_CONFIG_CAT = "giftConfigurator";
 
 /** The Constant GIFT_WRAP_LABEL. */
 public static final String GIFT_WRAP_LABEL = "giftWrapLabel";
 
 /** The Constant CTA. */
 public static final String CTA = "cta";
 
 /** The Constant GIFT_FEILDS. */
 public static final String GIFT_FEILDS = "giftFeilds";
 
 /** The Constant NAME. */
 public static final String NAME = "name";
 
 /** The Constant INPUT_LIMIT. */
 public static final String INPUT_LIMIT = "inputLimit";
 
 /** The Constant CHARACTER_COUNT. */
 public static final String CHARACTER_COUNT = "characterCount";
 
 /** The Constant GIFTING_WRAP_ENABLE. */
 public static final String GIFTING_WRAP_ENABLE = "giftingWrapEnable";
 
 /** The Constant MAX_GIFT_WRAPS. */
 public static final String MAX_GIFT_WRAPS = "maxGiftsWraps";
 
 /** The Constant NAVIGATION_URL_KEY. */
 public static final String NAVIGATION_URL_KEY = "navigationURL";
 
 /** The Constant NAVIGATION_URL. */
 public static final Object NAVIGATION_URL = "navigationUrl";
 
 /** The Constant GIFT_CONFIGURATOR_CTALABEL. */
 public static final String GIFT_CONFIGURATOR_CTALABEL = "giftConfigurator.ctaLabel";
 
 /** The Constant GIFT_CONFIGURATOR_RECIEPENT_LABEL. */
 public static final String LABEL = "Label";
 
 /** The Constant GIFT_CONFIGURATOR_RECIPIENT_ERRORMSG. */
 public static final String ERRORMSG = "Errormsg";
 
 /** The Constant GIFT_CONFIGURATOR_RECIPIENT_DEFAULT_VALUE. */
 public static final String DEFAULT_VALUE = "DefaultValue";
 
 /** The Constant GIFT_CONFIGURATOR_GIFTWRAP_LABEL1. */
 public static final String GIFT_CONFIGURATOR_GIFTWRAP_LABEL = "giftConfigurator.giftWrapLabel";
 
 /** The Constant RECIEPENT_FIELD_NAME. */
 public static final String FIELD_NAME = "FieldName";
 
 /** The Constant RECIEPENT_FIELD_INPUT_LIMIT. */
 public static final String RECIEPENT_FIELD_INPUT_LIMIT = "recipientFieldInputLimit";
 
 /** The Constant GREETING_FIELD_INPUT_LIMIT. */
 public static final String GREETING_FIELD_INPUT_LIMIT = "greetingFieldInputLimit";
 
 /** The Constant TWENTY_FIVE. */
 public static final int TWENTY_FIVE = 25;
 
 /** The Constant SUB_TOTAL_LABEl. */
 public static final String SUB_TOTAL_LABEL = "subTotal";
 
 /** The Constant EXCLUSIVE_GIFT_WRAP_LABEL. */
 public static final String EXCLUSIVE_GIFT_WRAP_LABEL = "exclusiveGiftWrap";
 
 /** The Constant EXCLUSIVE_GIFT_WRAP_KEY. */
 public static final String EXCLUSIVE_GIFT_WRAP_KEY = "giftConfigurator.exclusiveGiftWrap";
 
 /** The Constant SUB_TOTAL. */
 public static final String SUB_TOTAL = "giftConfigurator.subTotal";
 
 /* constructor */
 private GiftConfiguratorConstants() {
  
 }
 
}
