/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;

import org.apache.commons.collections.MapUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.constants.CommonConstants;
import com.unilever.platform.aem.foundation.core.service.SearchService;
import com.unilever.platform.foundation.components.bean.DiagnosticToolQuestion;
import com.unilever.platform.foundation.components.bean.DiagnosticToolQuestion.Answer;
import com.unilever.platform.foundation.components.bean.DiagnosticToolResultSection;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * The Class DiagnosticToolResponse.
 */
@SlingServlet(resourceTypes = CommonConstants.DEFAULT_SERVLET_NAME, selectors = { "diagnostictoolresponse" }, methods = { HttpConstants.METHOD_GET,
  HttpConstants.METHOD_POST }, extensions = "json")
@Properties(value = {
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.foundation.servlet.DiagnosticToolResponse", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "Diagnostic Tool Response", propertyPrivate = false) })
public class DiagnosticToolResponse extends SlingAllMethodsServlet {
 
 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = -3945033031400825585L;
 
 /** The serach service. */
 @Reference
 SearchService serachService;
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(DiagnosticToolResponse.class);
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingAllMethodsServlet#doPost(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doPost(SlingHttpServletRequest slingRequest, SlingHttpServletResponse slingResponse) throws ServletException, IOException {
  StringBuilder jb = new StringBuilder();
  String line = null;
  try {
   BufferedReader reader = slingRequest.getReader();
   while ((line = reader.readLine()) != null) {
    jb.append(line);
   }
  } catch (Exception e) {
   LOGGER.error("Error while reading request", e);
  }
  printResponse(slingRequest, slingResponse, jb.toString());
  
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doGet(SlingHttpServletRequest slingRequest, SlingHttpServletResponse slingResponse) throws ServletException, IOException {
  String jsonStr = slingRequest.getParameter(NameConstants.NN_PARAMS);
  printResponse(slingRequest, slingResponse, jsonStr);
  
 }
 
 /**
  * Prints the response.
  * 
  * @param slingRequest
  *         the sling request
  * @param slingResponse
  *         the sling response
  * @param jsonStr
  *         the json str
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  */
 private void printResponse(SlingHttpServletRequest slingRequest, SlingHttpServletResponse slingResponse, String jsonStr) throws IOException {
  JSONObject jsonObj = null;
  try {
   jsonObj = new JSONObject(jsonStr);
  } catch (Exception exception) {
   LOGGER.error("parameter is not in valid json format", exception);
  }
  ServletOutputStream out = slingResponse.getOutputStream();
  Resource diagnosticToolResource = slingRequest.getResource();
  Map<String, Object> finalMap = new HashMap<String, Object>();
  Map<String, Object> dataMap = new HashMap<String, Object>();
  if (jsonObj != null) {
   ResourceResolver resourceResolver = slingRequest.getResourceResolver();
   Page currentPage = resourceResolver.adaptTo(PageManager.class).getContainingPage(diagnosticToolResource);
   Map<String, DiagnosticToolQuestion> questionsMap = getAllQuestionsMap(diagnosticToolResource, resourceResolver);
   setAnsweredQuestionsList(questionsMap, jsonObj);
   List<Map<String, Object>> sectionList = getSectionList(diagnosticToolResource, slingRequest, resourceResolver, questionsMap, currentPage);
   
   I18n i18n = BaseViewHelper.getI18n(slingRequest, currentPage);
   Map<String, String> shopNowProperties = ProductHelper
     .getshopNowProperties(configurationService, currentPage, i18n, resourceResolver, slingRequest);
   
   Map<String, String> reviewMapProperties = ProductHelper.getReviewMapProperties(configurationService, currentPage);
   finalMap.put(ProductConstants.SHOPNOW_MAP, shopNowProperties);
   finalMap.put(ProductConstants.REVIEW_MAP, reviewMapProperties);
   finalMap.put("sections", sectionList);
   finalMap.put("static", getStaticMap(i18n));
   
  } else {
   finalMap.put("error", "error in parameter json");
  }
  dataMap.put("data", finalMap);
  
  Gson gson = new GsonBuilder().serializeNulls().create();
  String dataStr = new String(gson.toJson(dataMap).getBytes("utf-8"), "iso-8859-1");
  out.print(dataStr);
 }
 
 /**
  * Gets the static map.
  * 
  * @param i18n
  *         the i18n
  * @return the static map
  */
 private Map<String, String> getStaticMap(I18n i18n) {
  Map<String, String> staticMap = new HashMap<String, String>();
  staticMap.put("readLabel", i18n.get("diagnosticTool.readLabel"));
  return staticMap;
 }
 
 /**
  * Gets the section list.
  * 
  * @param diagnosticToolResource
  *         the diagnostic tool resource
  * @param slingRequest
  *         the sling request
  * @param resourceResolver
  *         the resource resolver
  * @param questionsMap
  *         the questions map
  * @param currentPage
  *         the current page
  * @return the section list
  */
 private List<Map<String, Object>> getSectionList(Resource diagnosticToolResource, SlingHttpServletRequest slingRequest,
   ResourceResolver resourceResolver, Map<String, DiagnosticToolQuestion> questionsMap, Page currentPage) {
  List<Map<String, String>> sectionList = ComponentUtil.getNestedMultiFieldProperties(diagnosticToolResource, "resultSections");
  List<Map<String, Object>> resultSectionList = new ArrayList<Map<String, Object>>();
  List<String> tagIdList = getTagIdList(questionsMap);
  Iterator<Map<String, String>> itr = sectionList.iterator();
  String socialShareSectionPath = MapUtils.getString(diagnosticToolResource.getValueMap(), "personalisedTextFrom");
  while (itr.hasNext()) {
   Map<String, String> sectionProps = itr.next();
   String resultSectionPath = MapUtils.getString(sectionProps, "resultSectionPath", "");
   Resource resultSectionResource = resourceResolver.getResource(resultSectionPath);
   boolean socialSharePersonalisedTextFlag = false;
   if (resultSectionPath.equals(socialShareSectionPath)) {
    socialSharePersonalisedTextFlag = true;
   }
   if (resultSectionResource != null) {
    
    DiagnosticToolResultSection section = new DiagnosticToolResultSection(resultSectionResource, currentPage, slingRequest, tagIdList, serachService,
      questionsMap, socialSharePersonalisedTextFlag);
    
    ObjectMapper m = new ObjectMapper();
    @SuppressWarnings("unchecked")
    Map<String, Object> props = m.convertValue(section, Map.class);
    resultSectionList.add(props);
   }
  }
  return resultSectionList;
 }
 
 /**
  * Gets the tag id list.
  * 
  * @param questionsMap
  *         the questions map
  * @return the tag id list
  */
 private List<String> getTagIdList(Map<String, DiagnosticToolQuestion> questionsMap) {
  Iterator<String> quesItr = questionsMap.keySet().iterator();
  List<String> tagIdList = new ArrayList<String>();
  while (quesItr.hasNext()) {
   String questionId = quesItr.next();
   DiagnosticToolQuestion question = questionsMap.get(questionId);
   if (!question.isTagNotRequired()) {
    List<Answer> selectedAnswerList = question.getSelectedAnswerList() != null ? question.getSelectedAnswerList() : new ArrayList<Answer>();
    Iterator<Answer> ansItr = selectedAnswerList.iterator();
    while (ansItr.hasNext()) {
     Answer ans = ansItr.next();
     tagIdList.addAll(Arrays.asList(ans.getTagIds()));
    }
   }
  }
  return tagIdList;
 }
 
 /**
  * Gets the questions list.
  * 
  * @param questionsMap
  *         the questions map
  * @param jsonObj
  *         the json obj
  * @return the questions list
  */
 private void setAnsweredQuestionsList(Map<String, DiagnosticToolQuestion> questionsMap, JSONObject jsonObj) {
  
  JSONArray questionsArr = jsonObj.getJSONArray("data");
  for (int i = 0; i < questionsArr.length(); ++i) {
   JSONObject questionJSON = questionsArr.getJSONObject(i);
   String questionId = questionJSON.getString("questionId");
   JSONArray answerIds = questionJSON.getJSONArray("answers");
   DiagnosticToolQuestion question = questionsMap.get(questionId);
   if (question != null) {
    Map<String, Answer> answerMap = question != null ? question.getAllAnswerMap() : null;
    List<Answer> selectedAnswerList = new ArrayList<Answer>();
    for (int j = 0; j < answerIds.length(); ++j) {
     JSONObject answerObj = answerIds.getJSONObject(j);
     String answerId = answerObj.getString("id");
     if (answerMap != null && answerMap.containsKey(answerId)) {
      selectedAnswerList.add(answerMap.get(answerId));
     }
    }
    question.setSelectedAnswerList(selectedAnswerList);
   }
  }
  
 }
 
 /**
  * Gets the questions map.
  * 
  * @param diagnosticToolResource
  *         the diagnostic tool resource
  * @param resourceResolver
  *         the resource resolver
  * @return the questions map
  */
 private Map<String, DiagnosticToolQuestion> getAllQuestionsMap(Resource diagnosticToolResource, ResourceResolver resourceResolver) {
  List<Map<String, String>> questionsList = ComponentUtil.getNestedMultiFieldProperties(diagnosticToolResource, "questions");
  Map<String, DiagnosticToolQuestion> map = new TreeMap<String, DiagnosticToolQuestion>();
  Iterator<Map<String, String>> itr = questionsList.iterator();
  while (itr.hasNext()) {
   Map<String, String> questionProps = itr.next();
   String questionPath = MapUtils.getString(questionProps, "questionPath", "");
   Resource questionResource = resourceResolver.getResource(questionPath);
   if (questionResource != null) {
    DiagnosticToolQuestion question = new DiagnosticToolQuestion(questionResource);
    map.put(question.getId(), question);
   }
  }
  return map;
 }
 
}
