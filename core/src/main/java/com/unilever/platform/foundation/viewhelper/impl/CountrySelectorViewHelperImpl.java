/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.dam.DAMAssetUtility;
import com.unilever.platform.foundation.components.LanguageSelectorUtility;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * The Class CountrySelectorViewHelperImpl.
 */
@Component(description = "Language Viewhelper Impl", immediate = true, metatype = true, label = "LanguageViewHelper")
@Service(value = { CountrySelectorViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.COUNTRY_SELECTOR, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class CountrySelectorViewHelperImpl extends BaseViewHelper {
 
 private static final String MAIN_LOGO = "mainLogo";
 
 private static final String STATIC = "static";
 
 /** The Constant COUNTRY_SELECTOR_SEARCH_CTA_LABEL_TEXT. */
 private static final String COUNTRY_SELECTOR_SEARCH_CTA_LABEL_TEXT = "countrySelector.SearchCtaLabelText";
 
 /** The Constant COUNTRY_SELECTOR_SEARCH_INPUT_PLACEHOLDER_TEXT. */
 private static final String COUNTRY_SELECTOR_SEARCH_INPUT_PLACEHOLDER_TEXT = "countrySelector.SearchInputPlaceholderText";
 
 /** The Constant SEARCH_CTA_LABEL_TEXT. */
 private static final String SEARCH_CTA_LABEL_TEXT = "searchCtaLabelText";
 
 /** The Constant SEARCH_INPUT_PLACEHOLDER_TEXT. */
 private static final String SEARCH_INPUT_PLACEHOLDER_TEXT = "searchInputPlaceholderText";
 
 /** The Constant SLOGAN. */
 private static final String SLOGAN = "slogan";
 
 /** The Constant COUNTRIES. */
 private static final String COUNTRIES = "countries";
 
 /** The Constant TYPE. */
 private static final String TYPE = "type";
 
 /** The Constant EXTERNAL_TYPE. */
 private static final String EXTERNAL_TYPE = "externalType";
 
 /** The Constant INTERNAL_TYPE. */
 private static final String INTERNAL_TYPE = "internalType";
 
 /** The Constant INTERNAL_LANGUAGE. */
 private static final String INTERNAL_LANGUAGE = "internalLanguage";
 
 /** The Constant COUNTRY_NAME. */
 private static final String COUNTRY_NAME = "countryName";
 
 /** The Constant COUNTRY_CODE. */
 private static final String COUNTRY_CODE = "countryCode";
 
 /** The Constant LANGUAGE_CODE. */
 private static final String LANGUAGE_CODE = "languageCode";
 
 /** The Constant LANGUAGE_TITLE. */
 private static final String LANGUAGE_TITLE = "languageTitle";
 
 /** The Constant LANGUAGE_LINK. */
 private static final String LANGUAGE_LINK = "languagesLink";
 
 /** The Constant COUNTRY_SELECTOR. */
 private static final String EXTERNAL_JSON = "externalJson";
 
 /** The Constant BRAND_LOGO. */
 private static final String BRAND_LOGO = "brandLogo";
 
 /** The Constant BRAND_QUOTE. */
 private static final String BRAND_QUOTE = "brandQuote";
 
 /** The Constant BRANDS. */
 private static final String BRANDS = "brands";
 
 /** The Constant BRAND_NAME. */
 private static final String BRAND_NAME = "brandName";
 
 /** The Constant BRAND_JSON. */
 private static final String BRAND_JSON = "brandJson";
 
 /** The Constant REGION. */
 private static final String REGION = "regions";
 
 /** The Constant CODE. */
 private static final String CODE = "code";
 /** The Constant NAMES. */
 private static final String NAMES = "name";
 
 /** The Constant REGION_NAME. */
 private static final String REGION_NAME = "regionName";
 
 /** The Constant LANGUAGES. */
 private static final String LANGUAGES = "languages";
 
 /** The Constant LABEL. */
 private static final String LABEL = "label";
 
 /** The Constant PATH. */
 private static final String PATH = "path";
 
 /** The Constant TITLE. */
 private static final String TITLE = "title";
 
 /** The Constant LINK. */
 private static final String LINK = "link";
 
 /** The Constant SELECTED. */
 private static final String SELECTED = "selected";
 
 private static final String DISPLAY_COUNTRY_FLAGS = "displayCountryFlags";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(CountrySelectorViewHelperImpl.class);
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /**
  * The method processData overrides method is responsible validating author input and returning a list of sections containing Map, This method
  * converts the multiwidget input (a String Array) into sections of maps so that it can be accessed in JSP in consistent way.This method also
  * validates the link url provided by author.
  * 
  * @param content
  *         , - {@link Map} of all user input received as input <code>ViewHelper.onGetData</code> method
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return {@link Map} holding the data
  */
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  
  LOGGER.debug("Country Language Selector View Helper #processData called for processing fixed list.");
  @SuppressWarnings("unchecked")
  Map<String, Object> contentProperties = (Map<String, Object>) content.get(PROPERTIES);
  Map<String, Object> countrySelector = new LinkedHashMap<String, Object>();
  String brandLogo = MapUtils.getString(contentProperties, BRAND_LOGO, StringUtils.EMPTY);
  String brandQuote = MapUtils.getString(contentProperties, BRAND_QUOTE, StringUtils.EMPTY);
  countrySelector.put(BRAND_LOGO, StringUtils.defaultString(brandLogo, StringUtils.EMPTY));
  countrySelector.put(MAIN_LOGO, DAMAssetUtility.getImageMap(brandLogo, getCurrentPage(resources), getSlingRequest(resources)));
  countrySelector.put(BRAND_QUOTE, StringUtils.defaultString(brandQuote, StringUtils.EMPTY));
  countrySelector.put(BRANDS, getBrandProperties(resources).toArray());
  countrySelector.put(STATIC, setStaticValues(resources));
  LOGGER.info("The country selector Properties Map is successfully generated");
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(getJsonNameSpace(content), countrySelector);
  return data;
 }
 
 /**
  * Gets the static values.
  * 
  * @param resources
  *         the resources
  * @return the static values
  */
 private Map<String, Object> setStaticValues(Map<String, Object> resources) {
  
  Map<String, Object> staticMap = new HashMap<String, Object>();
  I18n i18n = getI18n(resources);
  staticMap.put(SEARCH_INPUT_PLACEHOLDER_TEXT, i18n.get(COUNTRY_SELECTOR_SEARCH_INPUT_PLACEHOLDER_TEXT));
  staticMap.put(SEARCH_CTA_LABEL_TEXT, i18n.get(COUNTRY_SELECTOR_SEARCH_CTA_LABEL_TEXT));
  
  return staticMap;
 }
 
 /**
  * Gets the brand properties.
  * 
  * @param currentResource
  *         the current resource
  * @param slingRequest
  *         the sling request
  * @param resourceResolver
  *         the resource resolver
  * @param properties
  *         the properties
  * @return the brand properties
  */
 private List<Map<String, Object>> getBrandProperties(final Map<String, Object> resources) {
  List<Map<String, Object>> brandList = new ArrayList<Map<String, Object>>();
  
  List<Map<String, String>> primaryList = ComponentUtil.getNestedMultiFieldProperties(getCurrentResource(resources), BRAND_JSON);
  for (Map<String, String> map : primaryList) {
   Map<String, Object> brandMap = new LinkedHashMap<String, Object>();
   brandMap.put(LABEL, StringUtils.defaultString(map.get(BRAND_NAME), StringUtils.EMPTY));
   brandMap.put(SLOGAN, StringUtils.defaultString(map.get(SLOGAN), StringUtils.EMPTY));
   String path = StringUtils.defaultString(map.get(PATH), StringUtils.EMPTY);
   brandMap.put(REGION, getRegions(resources, path).toArray());
   brandList.add(brandMap);
  }
  LOGGER.debug("brandList Size logged: " + brandList.size());
  return brandList;
 }
 
 /**
  * Gets the regions.
  * 
  * @param resources
  *         the resources
  * @param path
  *         the path
  * @return the regions
  */
 public List<Map<String, Object>> getRegions(final Map<String, Object> resources, String path) {
  
  List<Map<String, Object>> regionList = new ArrayList<Map<String, Object>>();
  Page page = getPageManager(resources).getPage(path);
  Iterator<Page> it = null;
  if (page != null) {
   it = page.listChildren();
   
   while (it.hasNext()) {
    Page regionPage = it.next();
    Map<String, Object> regionMap = new LinkedHashMap<String, Object>();
    regionMap.put(REGION_NAME, regionPage.getTitle() != null ? regionPage.getTitle() : StringUtils.EMPTY);
    regionMap.put(COUNTRIES, getCountries(resources, regionPage).toArray());
    regionList.add(regionMap);
   }
  }
  LOGGER.debug("regionList Size logged: " + regionList.size());
  return regionList;
 }
 
 /**
  * Gets the countries.
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param regionPage
  *         the region page
  * @param slingRequest
  *         the sling request
  * @param configurationService
  *         the configuration service
  * @return the countries
  */
 private List<Map<String, Object>> getCountries(final Map<String, Object> resources, Page regionPage) {
  List<Map<String, Object>> countryList = new ArrayList<Map<String, Object>>();
  
  Iterator<Page> regionIterator = regionPage.listChildren();
  while (regionIterator.hasNext()) {
   Page countryPage = regionIterator.next();
   Iterator<Resource> countryIterator = null;
   Resource resource = countryPage.getContentResource();
   if (resource != null) {
    countryIterator = resource.listChildren();
    countryList.addAll(getLanguagesForCountry(resources, countryIterator));
   }
  }
  LOGGER.debug("countryList size logged : " + countryList.size());
  
  return countryList;
 }
 
 private List<Map<String, Object>> getLanguagesForCountry(final Map<String, Object> resources, Iterator<Resource> countryIterator) {
  Resource countryResource;
  String type;
  List<Map<String, Object>> countryList = new ArrayList<Map<String, Object>>();
  while (countryIterator != null && countryIterator.hasNext()) {
   Map<String, Object> languageMap = null;
   countryResource = countryIterator.next();
   ValueMap properties = countryResource.adaptTo(ValueMap.class);
   
   if (properties != null) {
    type = properties.get(TYPE, String.class);
    
    if (StringUtils.equalsIgnoreCase(type, EXTERNAL_TYPE)) {
     languageMap = getExternalMap(countryResource);
    } else {
     languageMap = getInternalMap(resources, countryResource);
    }
    
    if (languageMap != null && !languageMap.isEmpty()) {
     countryList.add(languageMap);
    }
   }
   
  }
  return countryList;
 }
 
 /**
  * Gets the internal map.
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param slingRequest
  *         the sling request
  * @param configurationService
  *         the configuration service
  * @param countryList
  *         the country list
  * @param countryResource
  *         the resource1
  * @return the internal map
  */
 private Map<String, Object> getInternalMap(final Map<String, Object> resources, Resource countryResource) {
  
  Map<String, Object> countryMap = new LinkedHashMap<String, Object>();
  List<Map<String, String>> languagesList = new ArrayList<Map<String, String>>();
  ValueMap valueMap = countryResource.getValueMap();
  String path = null;
  Locale locale = null;
  String currentCountryCode = null;
  String displayCountry = null;
  if (valueMap != null) {
   path = (String) valueMap.get(INTERNAL_LANGUAGE);
  }
  if (StringUtils.isNotBlank(path)) {
   PageManager pageMgr = getPageManager(resources);
   if (pageMgr != null) {
    Page currentPage = pageMgr.getPage(path);
    if (currentPage != null) {
     SlingHttpServletRequest slingRequest = getSlingRequest(resources);
     ResourceResolver resourceResolver = getResourceResolver(resources);
     locale = currentPage.getLanguage(false);
     currentCountryCode = locale.getCountry();
     displayCountry = locale.getDisplayCountry();
     languagesList = LanguageSelectorUtility.getLanguagesByCountry(currentPage, slingRequest, resourceResolver, configurationService);
    }
    countryMap.put(NAMES, StringUtils.defaultString(displayCountry, StringUtils.EMPTY));
    countryMap.put(DISPLAY_COUNTRY_FLAGS, MapUtils.getBoolean(countryResource.getValueMap(), DISPLAY_COUNTRY_FLAGS, false));
    countryMap.put(CODE, StringUtils.defaultString(currentCountryCode, StringUtils.EMPTY));
    countryMap.put(TYPE, INTERNAL_TYPE);
   }
  }
  countryMap.put(LANGUAGES, languagesList.toArray());
  return countryMap;
 }
 
 /**
  * Gets the external map.
  * 
  * @param countryList
  *         the country list
  * @param countryResource
  *         the resource1
  * @return the external map
  */
 private static Map<String, Object> getExternalMap(Resource countryResource) {
  Map<String, Object> countryMap = new LinkedHashMap<String, Object>();
  countryMap.put(NAMES, StringUtils.defaultString((String) countryResource.getValueMap().get(COUNTRY_NAME), StringUtils.EMPTY));
  countryMap.put(DISPLAY_COUNTRY_FLAGS, MapUtils.getBoolean(countryResource.getValueMap(), DISPLAY_COUNTRY_FLAGS, false));
  countryMap.put(CODE, StringUtils.defaultString((String) countryResource.getValueMap().get(COUNTRY_CODE), StringUtils.EMPTY));
  countryMap.put(TYPE, EXTERNAL_TYPE);
  Boolean isSelected = false;
  List<Map<String, String>> primaryList = ComponentUtil.getNestedMultiFieldProperties(countryResource, EXTERNAL_JSON);
  List<Map<String, Object>> languagesList = new ArrayList<Map<String, Object>>();
  if (CollectionUtils.isNotEmpty(primaryList)) {
   for (Map<String, String> map : primaryList) {
    if (map != null) {
     Map<String, Object> extLanguagesMap = new LinkedHashMap<String, Object>();
     extLanguagesMap.put(LINK, StringUtils.defaultString(map.get(LANGUAGE_LINK), StringUtils.EMPTY));
     extLanguagesMap.put(TITLE, StringUtils.defaultString(map.get(LANGUAGE_TITLE), StringUtils.EMPTY));
     extLanguagesMap.put(LANGUAGE_CODE, StringUtils.defaultString(map.get(LANGUAGE_CODE), StringUtils.EMPTY));
     extLanguagesMap.put(SELECTED, isSelected);
     languagesList.add(extLanguagesMap);
    }
   }
  }
  countryMap.put(LANGUAGES, languagesList.toArray());
  return countryMap;
  
 }
}
