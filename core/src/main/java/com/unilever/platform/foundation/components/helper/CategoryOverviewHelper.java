/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.components.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.Image;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * The Class CategoryOverviewHelper.
 * 
 * @author nbhart
 */
public final class CategoryOverviewHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(CategoryOverviewHelper.class);
 
 /**
  * Identifier for link constant.
  */
 private static final String LINK = "link";
 
 /**
  * Identifier for menu link URL.
  */
 public static final String MENU_LINK = "url";
 
 /**
  * Identifier for menu label.
  */
 public static final String MENU_LABEL = "label";
 
 /**
  * Identifier for opening in new window.
  */
 public static final String OPEN_IN_NEW_WINDOW = "openInNewWindow";
 
 /**
  * Identifier for configured categoryOverview links count, i.e. the number of menu links configured under each column.
  */
 public static final String CATEGORY_OVERVIEW_LINKS = "linksList";
 
 /**
  * Identifier for css class.
  */
 public static final String CLASS = "class";
 
 /**
  * Identifier for css class.
  */
 public static final String CSS_CLASS = "cssClass";
 
 /**
  * Instantiates a new category overview helper.
  */
 private CategoryOverviewHelper() {
  
 }
 
 /**
  * Convert to list map.
  * 
  * @param jsonString
  *         the json string
  * @param resources
  *         the resources
  * @return the list
  */
 public static List<Map<String, Object>> convertToListMap(String jsonString, Map<String, Object> resources) {
  
  List<Map<String, Object>> menuMapList = new ArrayList<Map<String, Object>>();
  List<Map<String, String>> listMap = ComponentUtil.convertJsonArrayToListOfStringMap(jsonString);
  
  for (Map<String, String> map : listMap) {
   Map<String, Object> menuMap = new HashMap<String, Object>();
   
   // this condition adds only those menu items for which label
   // and link both are configured
   if (StringUtils.isNotEmpty(map.get(MENU_LABEL)) || StringUtils.isNotEmpty(map.get(MENU_LINK))) {
    LOGGER.debug("Both menu label and menu link are configured");
    String newWindow = map.get(OPEN_IN_NEW_WINDOW);
    String openInNewWindow = newWindow != null && ("[true]".equals(newWindow) || "[yes]".equals(newWindow)) ? "Yes" : "No";
    menuMap.put(MENU_LABEL, map.get(MENU_LABEL));
    String linkUrl = map.get(MENU_LINK);
    
    Page page = BaseViewHelper.getPageManager(resources).getPage(linkUrl);
    SlingHttpServletRequest slingRequest = BaseViewHelper.getSlingRequest(resources);
    if (page != null) {
     String teaserImagePath = MapUtils.getString(page.getProperties(), UnileverConstants.TEASER_IMAGE, "");
     String teaserImageAltText = MapUtils.getString(page.getProperties(), UnileverConstants.TEASER_IMAGE_ALT_TEXT, "");
     String teaserTitle = MapUtils.getString(page.getProperties(), UnileverConstants.TEASER_TITLE, "");
     Image teaserImage = new Image(teaserImagePath, teaserImageAltText, teaserTitle, BaseViewHelper.getCurrentPage(resources), slingRequest);
     menuMap.put("teaserImage", teaserImage.convertToMap());
    }
    
    menuMap.put(MENU_LINK, ComponentUtil.getFullURL(BaseViewHelper.getResourceResolver(resources), linkUrl, slingRequest));
    menuMap.put(OPEN_IN_NEW_WINDOW, openInNewWindow);
    String cssClass = MapUtils.getString(map, "class", "");
    menuMap.put("cssClass", cssClass);
    
    LOGGER.debug("The menuMap prepared is" + menuMap);
    Map<String, Object> linkMap = new HashMap<String, Object>();
    linkMap.put(LINK, menuMap);
    menuMapList.add(linkMap);
   }
  }
  LOGGER.debug("The number of menu items for this column is" + menuMapList.size());
  return menuMapList;
 }
}
