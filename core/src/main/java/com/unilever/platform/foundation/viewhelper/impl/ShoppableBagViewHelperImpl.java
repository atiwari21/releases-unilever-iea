/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.WCMMode;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ShoppableHelper;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * <p>
 * Responsible for reading author input, I18N and configuration for shopping basket component and generate a map. Also read the site configuration for
 * maximum items configured for the site.
 * </p>
 */
@Component(description = "ShoppableBagViewHelperImpl", immediate = true, metatype = true, label = "ShoppableBagViewHelperImpl")
@Service(value = { ShoppableBagViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.SHOPPABLE_VIEW_BAG, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class ShoppableBagViewHelperImpl extends BaseViewHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ShoppableBagViewHelperImpl.class);
 
 /** The Constant STATIC_TEXT. */
 private static final String STATIC_TEXT = "staticText";
 
 /** The Constant SHOPPABLE_CONFIG. */
 private static final String SHOPPABLE_CONFIG = "shoppableConfig";
 
 /** The Constant SHOP_NOW. */
 private static final String SHOP_NOW = "shopNow";
 /** The Constant CURRENCY_SYMBOL. */
 public static final String CURRENCY_SYMBOL = "currencySymbol";
 
 /** The Constant CURRENCY. */
 public static final String CURRENCY = "currency";
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  LOGGER.debug("inside ShoppingBasketViewHelperImpl processData");
  Map<String, Object> data = new HashMap<String, Object>();
  Map<String, Object> dataMap = new HashMap<String, Object>();
  Map<String, String> shoppableConfigMap = new HashMap<String, String>();
  Map<String, String> shopNowConfigMap = new HashMap<String, String>();
  String currencySymbol = StringUtils.EMPTY;
  try {
   shopNowConfigMap = configurationService.getCategoryConfiguration(getCurrentPage(resources), SHOP_NOW);
   currencySymbol = configurationService.getConfigValue(getCurrentPage(resources), CURRENCY, CURRENCY_SYMBOL);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.debug("Could not find category for Shoppable or shopNow", e);
  }
  shoppableConfigMap = ShoppableHelper.getShoppableConfig(getResourceResolver(resources), configurationService, getCurrentPage(resources),
    getSlingRequest(resources));
  addApiToken(resources, shoppableConfigMap);
  shoppableConfigMap.put(CURRENCY_SYMBOL, currencySymbol);
  dataMap.put(STATIC_TEXT, ShoppableHelper.getBagSummaryMap(resources));
  dataMap.put(SHOPPABLE_CONFIG, shoppableConfigMap);
  dataMap.put(SHOP_NOW, shopNowConfigMap);
  data.put(getJsonNameSpace(content), dataMap);
  return data;
 }
 
 private void addApiToken(final Map<String, Object> resources, Map<String, String> shoppableConfigMap) {
  String apiTokenAuthor = shoppableConfigMap.get("apiTokenAuthor");
  shoppableConfigMap.remove(apiTokenAuthor);
  if (WCMMode.fromRequest(getSlingRequest(resources)) == WCMMode.PREVIEW || WCMMode.fromRequest(getSlingRequest(resources)) == WCMMode.EDIT
    || WCMMode.fromRequest(getSlingRequest(resources)) == WCMMode.DESIGN) {
   shoppableConfigMap.put("apiToken", apiTokenAuthor);
  }
 }
 
}
