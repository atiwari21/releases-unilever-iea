/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * This class prepares the data map for Html Injector.
 */

/**
 * @author mkathu
 * 
 */
@Component(description = "Html Injector view helper", immediate = true, metatype = true, label = "Html Injector ViewHelper")
@Service(value = { HtmlInjectorViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.HTML_INJECTOR, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class HtmlInjectorViewHelperImpl extends BaseViewHelper {
 
 /** used as a key in json. */
 private static final String ENABLED = "enabled";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(HtmlInjectorViewHelperImpl.class);
 
 /*
  * responsible for reading the values from the dialog and creating a json.
  * 
  * *
  */
 /*
  * (non-Javadoc)
  * 
  * com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData (java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("executing htmlInjector. Inside processData method");
  Map<String, Object> properties = new HashMap<String, Object>();
  Map<String, Object> htmlinjectorProperties = getProperties(content);
  properties.put(ENABLED, MapUtils.getBoolean(htmlinjectorProperties, ENABLED, false));
  properties.put(UnileverConstants.TITLE, MapUtils.getString(htmlinjectorProperties, UnileverConstants.TITLE, StringUtils.EMPTY));
  properties.put(UnileverConstants.HTML_SNIPPET, MapUtils.getString(htmlinjectorProperties, UnileverConstants.HTML_SNIPPET, StringUtils.EMPTY));
  LOGGER.info("Fetched the result from the component.");
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE), properties);
  return data;
  
 }
 
}
