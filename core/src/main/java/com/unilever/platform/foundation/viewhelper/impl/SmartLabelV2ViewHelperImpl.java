/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * This class prepared the data map for SmartLabelV2 component.
 * 
 */
@Component(description = "SmartLabelV2", immediate = true, metatype = true, label = "SmartLabelV2ViewHelperImpl")
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.SMARTLABEL_V2, propertyPrivate = true), })
public class SmartLabelV2ViewHelperImpl extends BaseViewHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(SmartLabelV2ViewHelperImpl.class);
 
 /** The Constant HEADING_TEXT. */
 private static final String HEADING_TEXT = "headingText";
 /** The Constant SUBHEADING_TEXT. */
 private static final String SUBHEADING_TEXT = "subheadingText";
 /** The Constant CTA_LABEL. */
 private static final String CTA_LABEL = "ctaLabel";
 
 /** The Constant PRODUCT. */
 private static final String PRODUCT = "product";
 
 /** The Constant DISPLAY_PRODUCT_VARIENT_OPTION. */
 private static final String DISPLAY_PRODUCT_VARIENT_OPTION = "displayProductVarientOption";
 
 /** The Constant SMART_LABEL_V2. */
 private static final String SMART_LABEL_V2 = "smartLabelV2";
 
 /** The Constant BASE_URL. */
 private static final String BASE_URL = "baseUrl";
 
 private static final String DYNAMIC_URL = "dynamicURL";
 
 private static final String DYNAMIC_URL_ENABLED = "dynamicURLEnabled";
 
 private static final String ENTITY = "entity";
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /**
  * This is overriden method from base view helper which prepares data map by calling all required methods.
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("Executing SmartLabelV2ViewHelperImpl.java. Inside processData method");
  
  Map<String, Object> smartLabelV2Map = new LinkedHashMap<String, Object>();
  Map<String, Object> properties = getProperties(content);
  Map<String, Object> productProperties = new HashMap<String, Object>();
  Map<String, String> configMap = new LinkedHashMap<String, String>();
  Map<String, Object> dynamicURLMap = new LinkedHashMap<String, Object>();
  
  PageManager pageManager = getPageManager(resources);
  Page currentPage = getCurrentPage(resources);
  SlingHttpServletRequest slingRequest = getSlingRequest(resources);
  
  try {
   configMap = configurationService.getCategoryConfiguration(currentPage, SMART_LABEL_V2);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Exception in configurationService inside process data method of LiveCahtV2viewhelperImpl.java" + e);
  }
  
  dynamicURLMap.put("enabled", MapUtils.getBoolean(configMap, DYNAMIC_URL_ENABLED, false));
  dynamicURLMap.put("apiURL", MapUtils.getString(configMap, DYNAMIC_URL, StringUtils.EMPTY));
  
  String componentName = MapUtils.getString(properties, "componentName", StringUtils.EMPTY);
  
  smartLabelV2Map.put("dynamicURL", dynamicURLMap);
  
  smartLabelV2Map.put("headingText", MapUtils.getString(properties, HEADING_TEXT, StringUtils.EMPTY));
  smartLabelV2Map.put("subheadingText", MapUtils.getString(properties, SUBHEADING_TEXT, StringUtils.EMPTY));
  smartLabelV2Map.put("ctaLabel", MapUtils.getString(properties, CTA_LABEL, StringUtils.EMPTY));
  smartLabelV2Map.put("displayProductVarientOption", MapUtils.getBoolean(properties, DISPLAY_PRODUCT_VARIENT_OPTION, false));
  smartLabelV2Map.put("baseUrl", MapUtils.getString(configMap, BASE_URL, StringUtils.EMPTY));
  smartLabelV2Map.put(ENTITY, MapUtils.getString(configMap, ENTITY, "smartlabel"));
  
  String productPath = MapUtils.getString(properties, PRODUCT, StringUtils.EMPTY);
  UNIProduct product = null;
  
  if (StringUtils.isNotBlank(productPath)) {
   Page productPage = pageManager.getPage(productPath);
   product = ProductHelper.getUniProduct(productPage, slingRequest);
  } else {
   product = ProductHelper.getUniProduct(currentPage, slingRequest);
  }
  
  productProperties = ProductHelper.getProductMap(product, componentName, resources, currentPage);
  
  if (productProperties.isEmpty()) {
   productProperties = null;
  }
  
  smartLabelV2Map.put(ProductConstants.PRODUCT_MAP, productProperties);
  
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  data.put(getJsonNameSpace(content), smartLabelV2Map);
  
  return data;
 }
 
}
