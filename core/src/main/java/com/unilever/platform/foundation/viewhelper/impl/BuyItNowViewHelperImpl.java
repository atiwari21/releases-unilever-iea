/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.components.helper.ProductHelperExt;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * This class prepares the data map for Product Images.
 */
@Component(description = "BuyItNowViewHelperImpl", immediate = true, metatype = true, label = "Buy It Now ViewHelper")
@Service(value = { BuyItNowViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.BUY_IT_NOW, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class BuyItNowViewHelperImpl extends BaseViewHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(BuyItNowViewHelperImpl.class);
 
 private static final String PRODUCT_PAGE = "productPage";
 
 @Reference
 ConfigurationService configurationService;
 
 @SuppressWarnings("unchecked")
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("Buy It Now ViewHelper #processData called.");
  ResourceResolver resourceResolver = (ResourceResolver) resources.get(ProductConstants.RESOURCE_RESOLVER);
  Map<String, Object> productRatingOverviewProperties = (Map<String, Object>) content.get(PROPERTIES);
  SlingHttpServletRequest slingRequest = getSlingRequest(resources);
  Page currentPage = getCurrentPage(resources);
  I18n i18n = getI18n(resources);
  Map<String, Object> properties = new LinkedHashMap<String, Object>();
  Map<String, Object> productProperties = new HashMap<String, Object>();
  UNIProduct product = null;
  String jsonNameSapce = getJsonNameSpace(content);
  String productPagePath = MapUtils.getString(productRatingOverviewProperties, PRODUCT_PAGE);
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  if (StringUtils.isNotBlank(productPagePath)) {
   product = ProductHelper.getUniProduct(pageManager.getPage(productPagePath));
  } else {
   product = ProductHelper.getUniProduct(currentPage, slingRequest);
  }
  if (product != null) {
   properties.put(ProductConstants.SHOPNOW_MAP,
     ProductHelper.getshopNowProperties(configurationService, currentPage, i18n, getResourceResolver(resources), slingRequest));
   productProperties = ProductHelperExt.getProductTeaserData(product, currentPage, slingRequest, false);
   productProperties.put(ProductConstants.PRICE_ENABLED, ProductHelper.isPriceEnabled(configurationService, currentPage));
   properties.put(ProductConstants.PRODUCT_MAP, productProperties);
  } else {
   properties.put(ProductConstants.SHOPNOW_MAP, new HashMap<String, String>());
   properties.put(ProductConstants.PRODUCT_MAP, new HashMap<String, Object>());
   LOGGER.info("No Product is available");
  }
  LOGGER.info("The Buy It Now Overview is successfully generated");
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(jsonNameSapce, properties);
  return data;
 }
 
}
