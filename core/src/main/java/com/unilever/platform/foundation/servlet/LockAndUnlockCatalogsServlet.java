/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Writer;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.servlet.ServletException;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.io.JSONWriter;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unilever.platform.foundation.constants.CommonConstants;

/**
 * This class sets the lock and unlock to product catalogs based on catalog selection handled for single and multiple selection catalogs.
 */
@SlingServlet(paths = { "/bin/lockAndUnlock" }, methods = { HttpConstants.METHOD_GET, HttpConstants.METHOD_POST })
@Properties(value = {
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.aem.foundation.servlets.LockAndUnlockCatalogsServlet", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "updates the content's lock and unlock properties ", propertyPrivate = false) })
public class LockAndUnlockCatalogsServlet extends SlingAllMethodsServlet {
 
 private static final String USER = "user";
 
 private static final String STATUS = "status";
 
 private static final String RESPONSE_MESSAGE = "responseMessage";
 
 /** The Constant LOCK_OWNER. */
 private static final String LOCK_OWNER = "lockOwner";
 
 /** The Constant TWO. */
 private static final int TWO = 2;
 
 /** The Constant ZERO. */
 private static final int ZERO = 0;
 
 /** The Constant ONE. */
 private static final int ONE = 1;
 
 /** The Constant LOCKED. */
 private static final String LOCKED = "locked";
 
 /** The Constant PATHS. */
 private static final String PATHS = "paths";
 
 /** The Constant LOCK. */
 private static final String LOCK = "lock";
 
 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = -782708002977083726L;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(LockAndUnlockCatalogsServlet.class);
 
 /** The Constant TWO_HUNDRED. */
 private static final int TWO_HUNDRED = 200;
 
 /** The Constant TWO_HUNDRED_ONE. */
 private static final int TWO_HUNDRED_ONE = 201;
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingAllMethodsServlet#doPost(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
  Writer out = response.getWriter();
  JSONWriter writer = new JSONWriter(out);
  StringBuilder requestObjectBuilder = new StringBuilder();
  String line = null;
  try {
   BufferedReader reader = request.getReader();
   while ((line = reader.readLine()) != null) {
    requestObjectBuilder.append(line);
   }
  } catch (Exception e) {
   LOGGER.error("Error while reading request", e);
  }
  String requestObjectString = requestObjectBuilder.toString();
  if (StringUtils.isNotBlank(requestObjectString)) {
   requestObjectString = getValueWithoutQuote(requestObjectString, requestObjectString.length(), ONE, ONE);
   String[] keyValuePairs = requestObjectString.split(LOCK);
   Map<String, Object> map = getMapWithKeyValue(keyValuePairs);
   String productCatalogPaths = MapUtils.getString(map, PATHS);
   String productCatalogPathsWithoutArray = getValueWithoutQuote(productCatalogPaths, productCatalogPaths.length(), ONE, ONE);
   String[] splitPathsByComma = productCatalogPathsWithoutArray.split(CommonConstants.COMMA_CHAR);
   String lockProperty = MapUtils.getString(map, LOCK);
   ResourceResolver resourceResolver = request.getResource().getResourceResolver();
   setLockPostStatusData(writer, splitPathsByComma, lockProperty, resourceResolver);
   resourceResolver.commit();
  }
 }
 
 /**
  * Sets the post status data.
  * 
  * @param writer
  *         the writer
  * @param splitPathsByComma
  *         the split paths by comma
  * @param lockProperty
  *         the lock property
  * @param resourceResolver
  *         the resource resolver
  */
 private void setLockPostStatusData(JSONWriter writer, String[] splitPathsByComma, String lockProperty, ResourceResolver resourceResolver) {
  for (String catalogPath : splitPathsByComma) {
   String catalogPagePath = getValueWithoutQuote(catalogPath, catalogPath.length(), ONE, ONE);
   LOGGER.debug("product catalog path", catalogPagePath);
   Resource resource = resourceResolver.getResource(catalogPagePath);
   Node catalogNode = resource.adaptTo(Node.class);
   try {
    writer.object();
    Session session = catalogNode.getSession();
    if (Boolean.valueOf(getValueWithoutQuote(lockProperty, lockProperty.length(), ONE, ONE))) {
     catalogNode.setProperty(LOCKED, true);
     catalogNode.setProperty(LOCK_OWNER, session.getUserID());
     writer.key(STATUS).value(TWO_HUNDRED);
    } else {
     catalogNode.setProperty(LOCKED, (Value) null);
     catalogNode.setProperty(LOCK_OWNER, (Value) null);
     writer.key(STATUS).value(TWO_HUNDRED_ONE);
    }
    writer.endObject();
   } catch (RepositoryException e) {
    LOGGER.error("Error while getting resource node", e);
   } catch (JSONException e) {
    LOGGER.error("Error while parsing JSON object", e);
   }
  }
 }
 
 /**
  * Gets the value without quote.
  * 
  * @param value
  *         the value
  * @param length
  *         the length
  * @param startIndex
  *         the start index
  * @param endIndex
  *         the end index
  * @return the value without quote
  */
 private String getValueWithoutQuote(String value, int length, int withOutQuoteIndexFromStart, int withOutQuoteIndexFromLast) {
  String updatedValue = StringUtils.EMPTY;
  if (length > TWO) {
   updatedValue = value.substring(withOutQuoteIndexFromStart, length - withOutQuoteIndexFromLast);
  }
  return updatedValue;
 }
 
 /**
  * Gets the map with key value.
  * 
  * @param keyValuePairs
  *         the key value pairs
  * @return the map with key value
  */
 private Map<String, Object> getMapWithKeyValue(String[] keyValuePairs) {
  int pathCount = ZERO;
  Map<String, Object> map = new LinkedHashMap<>();
  for (String pair : keyValuePairs) {
   String[] entry = pair.split(CommonConstants.COLON_CHAR);
   String key = entry[ZERO].trim().length() > ONE ? entry[ZERO].trim() : LOCK;
   key = (pathCount == ZERO) ? getValueWithoutQuote(key, key.length(), ONE, ONE) : key;
   String keyVal = entry[ONE].trim().split(CommonConstants.COMMA_CHAR).length > ONE ? getValueWithoutQuote(entry[ONE].trim(), entry[ONE].trim()
     .length(), ZERO, TWO) : entry[ONE].trim();
   map.put(key, keyVal);
   pathCount++;
  }
  return map;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws ServletException, IOException {
  Writer out = response.getWriter();
  JSONWriter writer = new JSONWriter(out);
  
  ResourceResolver resourceResolver = request.getResource().getResourceResolver();
  String catalogPagePath = request.getParameter(PATHS);
  Resource resource = StringUtils.isNotBlank(catalogPagePath) ? resourceResolver.getResource(catalogPagePath) : null;
  LOGGER.debug("product catalog path", catalogPagePath);
  String userId = StringUtils.EMPTY;
  if (resource != null) {
   Node catalogNode = resource.adaptTo(Node.class);
   try {
    userId = (catalogNode.hasProperty(LOCK_OWNER)) ? catalogNode.getProperty(LOCK_OWNER).getString() : userId;
    writer.object();
    if (catalogNode.hasProperty(LOCKED)) {
     writer.key(RESPONSE_MESSAGE).value("It was Locked already");
     writer.key(STATUS).value(TWO_HUNDRED_ONE);
    } else {
     writer.key(RESPONSE_MESSAGE).value("It is being processed for lock");
     writer.key(STATUS).value(TWO_HUNDRED);
    }
    writer.key(USER).value(userId);
    writer.endObject();
   } catch (RepositoryException e) {
    LOGGER.error("Error while getting resource node", e);
   } catch (JSONException e) {
    LOGGER.error("Error while parsing JSON object", e);
   }
  }
  
 }
}
