/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.constants;

/**
 * The Class GiftConfiguratorConstants.
 */
public class StoreLocatorConstants {
 /** The Constant GLOBAL_CONFIG_CAT. */
 public static final String GLOBAL_CONFIG_CAT = "storeLocator";
 
 /** The Constant STORE_LOCATOR_ENABLED. */
 public static final String STORE_LOCATOR_ENABLED = "storeLocatorEnabled";
 
 /** The Constant IS_ENABLED_COMPONENT_LIST. */
 public static final String IS_ENABLED_COMPONENT_LIST = "isEnabledComponentList";
 
 /** The Constant TRUE. */
 public static final String TRUE = "true";
 
 /** The Constant CTA_URL. */
 public static final String CTA_URL = "ctaUrl";
 
 /** The Constant IS_LOCATION_BASED_SEARCH_ENABLED. */
 public static final String IS_LOCATION_BASED_SEARCH_ENABLED = "isLocationBasedSearchEnabled";
 
 /** The Constant BUY_IN_STORE_CTA. */
 public static final String BUY_IN_STORE_CTA = "buyInStoreCta";
 
 /** The Constant POST_CODE_TOOLTIP. */
 public static final String POST_CODE_TOOLTIP = "postCodeToolTip";
 
 /** The Constant FIELDS. */
 public static final String FIELDS = "fields";
 
 /** The Constant DEFAULT_VALUE. */
 public static final String DEFAULT_VALUE = "defaultValue";
 
 /** The Constant ERROR_MSG. */
 public static final String ERROR_MSG = "errormsg";
 
 /** The Constant OPTIONS. */
 public static final String OPTIONS = "options";
 
 /** The Constant PRODUCT_VARIENT. */
 public static final String PRODUCT_VARIENT = "productVarient";
 
 /** The Constant POST_CODE. */
 public static final String POST_CODE = "postCode";
 
 /** The Constant DISTANCE_WITHIN. */
 public static final String DISTANCE_WITHIN = "distanceWithin";
 
 /** The Constant STORE_LOCATOR_BUY_IN_STORE_CTA. */
 public static final String STORE_LOCATOR_BUY_IN_STORE_CTA = "storeLocator.buyInStoreCta";
 
 /** The Constant STORE_LOCATOR_POST_CODE_TOOLTIP. */
 public static final String STORE_LOCATOR_POST_CODE_TOOLTIP = "storeLocator.postCodeToolTip";
 
 /** The Constant STORE_LOCATOR_PRODUCT_VARIENT_LABEL. */
 public static final String STORE_LOCATOR_PRODUCT_VARIENT_LABEL = "storeLocator.productVarientLabel";
 
 /** The Constant STORE_LOCATOR_POST_CODE_LABEL. */
 public static final String STORE_LOCATOR_POST_CODE_LABEL = "storeLocator.postCodeLabel";
 
 /** The Constant STORE_LOCATOR_POST_CODE_DEFAULT_VALUE. */
 public static final String STORE_LOCATOR_POST_CODE_DEFAULT_VALUE = "storeLocator.postCodeDefaultValue";
 
 /** The Constant STORE_LOCATOR_POST_CODE_ERROR_MSG. */
 public static final String STORE_LOCATOR_POST_CODE_ERROR_MSG = "storeLocator.postCodeErrormsg";
 
 /** The Constant STORE_LOCATOR_DISTANCE_WITHIN_LABEL. */
 public static final String STORE_LOCATOR_DISTANCE_WITHIN_LABEL = "storeLocator.distanceWithinLabel";
 
 /** The Constant NO_OF_STORES. */
 public static final String NO_OF_STORES = "noOfStores";
 
 /** The Constant LOAD_MORE_DISPLAY_COUNT. */
 public static final String LOAD_MORE_DISPLAY_COUNT = "loadMoreDisplayCount";
 
 /** The Constant STORE_SEARCH_RESULTS. */
 public static final String STORE_SEARCH_RESULTS = "storeSearchResults";
 
 /** The Constant SERVICE_URL. */
 public static final String SERVICE_URL = "serviceUrl";
 
 public static final String SERVICE_URL_KEY = "storeLocatorServiceURL";
 
 /** The Constant SEARCH_DESCRIPTION. */
 public static final String SEARCH_DESCRIPTION = "searchDescription";
 
 /** The Constant ANOTHER_PRODUCT_LABEL. */
 public static final String ANOTHER_PRODUCT_LABEL = "anotherProductLabel";
 
 /** The Constant SELECTED_PRODUCT_LABEL. */
 public static final String SELECTED_PRODUCT_LABEL = "selectedProductLabel";
 
 /** The Constant BUY_ONLINE_CTA. */
 public static final String BUY_ONLINE_CTA = "buyOnlineCta";
 
 /** The Constant SHOW_ME_STORES_CTA. */
 public static final String SHOW_ME_STORES_CTA = "showMeStoresCta";
 
 /** The Constant LOAD_MORE_CTA. */
 public static final String LOAD_MORE_CTA = "loadMoreCta";
 
 /** The Constant STORE_SEARCH_RESULTS_SEARCH_DESCRIPTION. */
 public static final String STORE_SEARCH_RESULTS_SEARCH_DESCRIPTION = "storeSearchResults.searchDescription";
 
 /** The Constant STORE_SEARCH_RESULTS_ANOTHER_PRODUCT_LABEL. */
 public static final String STORE_SEARCH_RESULTS_ANOTHER_PRODUCT_LABEL = "storeSearchResults.anotherProductLabel";
 
 /** The Constant STORE_SEARCH_RESULTS_SELECTED_PRODUCT_LABEL. */
 public static final String STORE_SEARCH_RESULTS_SELECTED_PRODUCT_LABEL = "storeSearchResults.selectedProductLabel";
 
 /** The Constant STORE_SEARCH_RESULTS_BUY_ONLINE_CTA. */
 public static final String STORE_SEARCH_RESULTS_BUY_ONLINE_CTA = "storeSearchResults.buyOnlineCta";
 
 /** The Constant STORE_SEARCH_RESULTS_SHOW_ME_STORES_CTA. */
 public static final String STORE_SEARCH_RESULTS_SHOW_ME_STORES_CTA = "storeSearchResults.showMeStoresCta";
 
 /** The Constant STORE_SEARCH_RESULTS_LOAD_MORE_CTA. */
 public static final String STORE_SEARCH_RESULTS_LOAD_MORE_CTA = "storeSearchResults.loadMoreCta";
 
 /** The Constant COLUMN_ONE. */
 public static final String COLUMN_ONE = "columnOne";
 
 /** The Constant COLUMN_TWO. */
 public static final String COLUMN_TWO = "columnTwo";
 
 /** The Constant COLUMN_THREE. */
 public static final String COLUMN_THREE = "columnThree";
 
 /** The Constant COLUMN_FOUR. */
 public static final String COLUMN_FOUR = "columnFour";
 
 /** The Constant COLUMN_ONE. */
 public static final String COLUMN_STORE = "columnStore";
 
 /** The Constant COLUMN_TWO. */
 public static final String COLUMN_PHONE = "columnPhone";
 
 /** The Constant COLUMN_THREE. */
 public static final String COLUMN_ADDRESS = "columnAddress";
 
 /** The Constant COLUMN_FOUR. */
 public static final String COLUMN_DISTANCE = "columnDistance";
 
 /** The Constant STORE_SEARCH_RESULTS_COLUMN_ONE. */
 public static final String STORE_SEARCH_RESULTS_COLUMN_ONE = "storeSearchResults.columnOne";
 
 /** The Constant STORE_SEARCH_RESULTS_COLUMN_TWO. */
 public static final String STORE_SEARCH_RESULTS_COLUMN_TWO = "storeSearchResults.columnTwo";
 
 /** The Constant STORE_SEARCH_RESULTS_COLUMN_THREE. */
 public static final String STORE_SEARCH_RESULTS_COLUMN_THREE = "storeSearchResults.columnThree";
 
 /** The Constant STORE_SEARCH_RESULTS_COLUMN_FOUR. */
 public static final String STORE_SEARCH_RESULTS_COLUMN_FOUR = "storeSearchResults.columnFour";
 
 /** The Constant STORE_SEARCH_RESULTS_COLUMN_ONE. */
 public static final String STORE_SEARCH_RESULTS_COLUMN_STORE = "storeSearchResults.storeList.storeColumnHeader";
 
 /** The Constant STORE_SEARCH_RESULTS_COLUMN_TWO. */
 public static final String STORE_SEARCH_RESULTS_COLUMN_PHONE = "storeSearchResults.storeList.phoneColumnHeader";
 
 /** The Constant STORE_SEARCH_RESULTS_COLUMN_THREE. */
 public static final String STORE_SEARCH_RESULTS_COLUMN_ADDRESS = "storeSearchResults.storeList.addressColumnHeader";
 
 /** The Constant STORE_SEARCH_RESULTS_COLUMN_FOUR. */
 public static final String STORE_SEARCH_RESULTS_COLUMN_DISTANCE = "storeSearchResults.storeList.distanceColumnHeader";
 
 /** The Constant TABLE_HEADING. */
 public static final String TABLE_HEADING = "tableHeading";
 
 /** The Constant DISTANCE_WITHIN_OPTIONS. */
 public static final String DISTANCE_WITHIN_OPTIONS = "distanceWithinOptions";
 
 /** The Constant OPTIONS_LABEL. */
 public static final String OPTIONS_LABEL = "optionsLabel";
 
 /** The Constant STORE_LOCATOR_DISTANCE_WITHIN_OPTIONS_LABEL. */
 public static final String STORE_LOCATOR_DISTANCE_WITHIN_OPTIONS_LABEL = "storeLocator.distanceWithinOptionsLabel";
 
 /** The Constant PRINT_LABEL. */
 public static final String PRINT_LABEL = "printLabel";
 
 /** The Constant STORE_SEARCH_RESULTS_PRINT_LABEL. */
 public static final String STORE_SEARCH_RESULTS_PRINT_LABEL = "storeSearchResults.printLabel";
 
 /** The Constant REGEX. */
 public static final String REGEX = "regex";
 
 /** The Constant TEN. */
 public static final int TEN = 10;
 
 public static final int HUNDRED = 100;
 
 /** The Constant SHARE_LOCATION. */
 public static final String SHARE_LOCATION = "shareLocation";
 
 /** The Constant STORE_SEARCH_RESULTS_SHARE_LOCATION. */
 public static final String STORE_SEARCH_RESULTS_SHARE_LOCATION = "storeSearchResults.shareLocation";
 
 /** The Constant MAP_API. */
 public static final String MAP_API = "mapApi";
 
 public static final String MAP_IT_KEY = "storeSearchResults.mapit";
 
 /** The Constant STORE_SEARCH_RESULTS_HEADING_TEXT */
 public static final String HEADING_TEXT = "headingText";
 
 /** The Constant STORE_SEARCH_RESULTS_ZIPCODE_HELPTEXT. */
 public static final String ZIPCODE_HELPTEXT = "zipCodeHelpText";
 
 /** The Constant STORE_SEARCH_RESULTS_SEARCHRADIUS_PREFIX. */
 public static final String SEARCHRADIUS_PREFIX = "searchRadiusPrefix";
 
 /** The Constant STORE_SEARCH_RESULTS_SEARCH_RADIUS_METRICS. */
 public static final String SEARCHRADIUS_METRICS = "searchRadiusMetrics";
 
 /** The Constant STORE_SEARCH_RESULTS_STORELIST_DIRECTION_CTALABEL. */
 public static final String STORELIST_DIRECTION_CTALABEL = "directionCtaLabel";
 
 /** The Constant STORE_SEARCH_RESULTS_STORELIST_SHOWMORE_CTALABEL. */
 public static final String SHOWMORE_CTALABEL = "showMoreCtaLabel";
 
 /** The Constant STORE_SEARCH_RESULTS_STORELIST_BUYONLINE_PREFIX. */
 public static final String BUYONLINE_PREFIX = "buyOnlinePrefixText";
 
 /** The Constant STORE_SEARCH_RESULTS_STORELIST_BUYONLINE_PREFIX. */
 public static final String SUMMARY_TEXT = "summaryText";
 
 /** The Constant STORE_SEARCH_RESULTS_HEADING_TEXT */
 public static final String STORE_SEARCH_RESULTS_HEADING_TEXT = "storeSearchResults.headingText";
 
 /** The Constant STORE_SEARCH_RESULTS_ZIPCODE_HELPTEXT. */
 public static final String STORE_SEARCH_RESULTS_ZIPCODE_HELPTEXT = "storeSearchResults.zipcodeField.helpText";
 
 /** The Constant STORE_SEARCH_RESULTS_SEARCHRADIUS_PREFIX. */
 public static final String STORE_SEARCH_RESULTS_SEARCHRADIUS_PREFIX = "storeSearchResults.searchRadiusField.prefixText";
 
 /** The Constant STORE_SEARCH_RESULTS_SEARCH_RADIUS_METRICS. */
 public static final String STORE_SEARCH_RESULTS_SEARCH_RADIUS_METRICS = "storeSearchResults.searchRadiusField.metricsLabel";
 
 /** The Constant STORE_SEARCH_RESULTS_STORELIST_DIRECTION_CTALABEL. */
 public static final String STORE_SEARCH_RESULTS_STORELIST_DIRECTION_CTALABEL = "storeSearchResults.storeList.getDirectionCtaLabel";
 
 /** The Constant STORE_SEARCH_RESULTS_STORELIST_SHOWMORE_CTALABEL. */
 public static final String STORE_SEARCH_RESULTS_STORELIST_SHOWMORE_CTALABEL = "storeSearchResults.storeList.showMoreCtaLabel";
 
 /** The Constant STORE_SEARCH_RESULTS_STORELIST_BUYONLINE_PREFIX. */
 public static final String STORE_SEARCH_RESULTS_STORELIST_BUYONLINE_PREFIX = "storeSearchResults.storeList.buyOnlinePrefixText";
 
 /** The Constant STORE_SEARCH_RESULTS_STORELIST_BUYONLINE_PREFIX. */
 public static final String STORE_SEARCH_RESULTS_STORELIST_SUMMARY_TEXT = "storeSearchResults.storeList.summaryText";
 
 /** The Constant BUY_IN_STORE_LABEL. */
 public static final String BUY_IN_STORE_LABEL = "storeSearch.headingText";
 
 /** The Constant POSTALCODE_FIELD_HELP_TEXT_LABEL. */
 public static final String POSTALCODE_FIELD_HELP_TEXT_LABEL = "storeSearch.postcodeFieldHelpText";
 
 /** The Constant GPS_SHARE_LOCATION_LABEL. */
 public static final String GPS_SHARE_LOCATION_LABEL = "storeSearch.gpsShareLocationCtaLabel";
 
 /** The Constant GO_CTA_LABEL. */
 public static final String GO_CTA_LABEL = "storeSearch.ctaLabel";
 
 /** The Constant DEFAULT_RADIOUS_FOR_SEARCH. */
 public static final String DEFAULT_RADIOUS_FOR_SEARCH = "defaultRadiusForSearch";
 
 /** The Constant GEO_CODE_API. */
 public static final String GEO_CODE_API = "geoCodeApi";
 
 /**
  * Instantiates a new store locator constants.
  */
 /* constructor */
 private StoreLocatorConstants() {
  
 }
 
}
