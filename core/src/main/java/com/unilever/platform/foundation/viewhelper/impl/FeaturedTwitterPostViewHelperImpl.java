/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.SocialTABHelper;
import com.unilever.platform.foundation.constants.CommonConstants;
import com.unilever.platform.foundation.template.dto.SocialTabQueryDTO;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * <p>
 * Responsible for Fetching Image and Caption related to the Twitter.
 * 
 * </p>
 */
@Component(description = "FeaturedTwitterPostViewHelperImpl", immediate = true, metatype = true, label = "Featured Brand Tweet Component")
@Service(value = { FeaturedTwitterPostViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.FEATURED_BRAND_TWEET, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class FeaturedTwitterPostViewHelperImpl extends SocialTabBaseViewHelperImpl {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(FeaturedTwitterPostViewHelperImpl.class);
 
 /** The Constant TWEET_DETAILS. */
 private static final String TWEET_DETAILS = "tweetDetails";
 
 /** The Constant TWEET_ID. */
 private static final String POST_ID = "postId";
 
 /** The Constant SHOW_PROFILE_PIC. */
 private static final String SHOW_PROFILE_PIC = "showProfilePic";
 
 /** The Constant SHOW_PROFILE_PICTURE. */
 private static final String SHOW_PROFILE_PICTURE = "showProfilePicture";
 
 /** The Constant CTA. */
 private static final String CTA = "cta";
 
 /** The Constant CTA_LABEL. */
 private static final String CTA_LABEL = "ctaLabel";
 
 /** The Constant CTA_LABEL_KEY. */
 private static final String CTA_LABEL_KEY = "featuredTwitterPost.ctaLabel";
 
 /** The Constant CTA_URL. */
 private static final String CTA_URL = "ctaUrl";
 
 /** The Constant ROTATION_PERIOD. */
 private static final String ROTATION_PERIOD = "rotationPeriod";
 
 /** The Constant START_DATE. */
 private static final String START_DATE = "startDate";
 
 /** The Constant ONE. */
 private static final String ONE = "1";
 
 /** The Constant FEATURED_TWITTER_POST_CAT. */
 private static final String FEATURED_TWITTER_POST_CAT = "featuredTwitterPost";
 
 /** The Constant ZERO. */
 private static final Integer ZERO = 0;
 
 /** The Constant STRICT. */
 private static final String STRICT = "strict";
 
 /**
  * The method processData overrides method is responsible validating author input and returning a map
  * 
  * @param content
  *         {@link Map} of all user input received as input
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return Map holding the result data.
  */
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.impl.SocialTabBaseViewHelperImpl#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  
  LOGGER.info("Featured Twitter Post View Helper #processData called for processing fixed list.");
  
  Map<String, Object> featuredTwitterPostMap = new LinkedHashMap<String, Object>();
  Map<String, Object> properties = getProperties(content);
  Page currentPage = getCurrentPage(resources);
  
  String jsonNameSpace = getJsonNameSpace(content);
  
  String rotationPeriod = MapUtils.getString(properties, ROTATION_PERIOD, ONE);
  GregorianCalendar startDate = (GregorianCalendar) MapUtils.getObject(properties, START_DATE);
  
  String dateString = StringUtils.EMPTY;
  if (startDate != null) {
   dateString = format.format(startDate.getTime());
  }
  
  featuredTwitterPostMap.put(ROTATION_PERIOD, rotationPeriod);
  featuredTwitterPostMap.put(START_DATE, dateString);
  
  // Adding data related to each tweet
  addTweetDetails(resources, featuredTwitterPostMap);
  
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  
  Map<String, String> configMap = ComponentUtil.getConfigMap(currentPage, FEATURED_TWITTER_POST_CAT);
  int numberOfPosts = MapUtils.getInteger(configMap, NUMBER_OF_POSTS, ZERO);
  int maxImages = MapUtils.getInteger(configMap, MAXIMUM_IMAGES, ZERO);
  if (configMap.containsKey(STRICT)) {
   boolean strict = MapUtils.getBooleanValue(configMap, STRICT);
   featuredTwitterPostMap.put(STRICT, strict);
  }
  SocialTabQueryDTO tabQuery = getSocialAggregatorQueryDataObj(resources, properties, currentPage, configurationService, numberOfPosts, maxImages);
  
  SocialTABHelper.addRetrievalQueryParametrsMap(featuredTwitterPostMap, tabQuery, true);
  
  data.put(jsonNameSpace, featuredTwitterPostMap);
  
  return data;
  
 }
 
 /**
  * Adds the tweet details.
  * 
  * @param resources
  *         the resources
  * @param featuredTwitterPostMap
  *         the featured twitter post map
  */
 private static void addTweetDetails(Map<String, Object> resources, Map<String, Object> featuredTwitterPostMap) {
  
  List<Map<String, Object>> tweetDetailsMapList = new ArrayList<Map<String, Object>>();
  List<Map<String, String>> tweetDetailsList = ComponentUtil.getNestedMultiFieldProperties(getCurrentResource(resources), TWEET_DETAILS);
  
  Map<String, Object> ctaMap = null;
  I18n i18n = getI18n(resources);
  String ctaLabel = i18n.get(CTA_LABEL_KEY);
  
  for (Map<String, String> map : tweetDetailsList) {
   ctaMap = new LinkedHashMap<String, Object>();
   String postId = MapUtils.getString(map, POST_ID, StringUtils.EMPTY);
   String ctaUrl = MapUtils.getString(map, CTA_URL, StringUtils.EMPTY);
   String newWindow = MapUtils.getString(map, OPEN_IN_NEW_WINDOW, StringUtils.EMPTY);
   String profilePic = MapUtils.getString(map, SHOW_PROFILE_PIC, StringUtils.EMPTY);
   String showProfilePic = profilePic != null && "[true]".equals(profilePic) ? CommonConstants.BOOLEAN_TRUE : ProductConstants.FALSE;
   String openInNewWindow = newWindow != null && "[true]".equals(newWindow) ? CommonConstants.BOOLEAN_TRUE : ProductConstants.FALSE;
   
   ctaMap.put(CTA_LABEL, ctaLabel);
   ctaMap.put(CTA_URL, ctaUrl);
   ctaMap.put(OPEN_IN_NEW_WINDOW, Boolean.valueOf(openInNewWindow));
   
   Map<String, Object> tweetDetailsMap = new LinkedHashMap<String, Object>();
   tweetDetailsMap.put(CTA, ctaMap);
   tweetDetailsMap.put(SHOW_PROFILE_PICTURE, Boolean.valueOf(showProfilePic));
   tweetDetailsMap.put(POST_ID, postId);
   
   tweetDetailsMapList.add(tweetDetailsMap);
  }
  featuredTwitterPostMap.put(TWEET_DETAILS, tweetDetailsMapList.toArray());
 }
}
