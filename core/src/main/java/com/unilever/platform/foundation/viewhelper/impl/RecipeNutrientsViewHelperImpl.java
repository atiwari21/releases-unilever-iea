/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.http.osgi.services.HttpClientBuilderFactory;
import org.apache.sling.api.SlingConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.RMSJson;
import com.unilever.platform.foundation.components.helper.RecipeHelper;
import com.unilever.platform.foundation.template.dto.RecipeConfigDTO;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * The Class RecipeNutrientsViewHelperImpl.
 */
@Component(description = "RecipeNutrientsViewHelperImpl", immediate = true, metatype = true, label = "Recipe Nutrients Component")
@Service(value = { RecipeNutrientsViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.RECIPE_NUTRIENTS, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class RecipeNutrientsViewHelperImpl extends BaseViewHelper {
 
 private static final String TOTAL_RESULTS = "totalResults";
 
 private static final String RESULTS_TO_SHOW = "resultsToShow";
 
 /** The Constant NUTRIENTS_PER_SERVING_COLUMN_HEADING. */
 private static final String NUTRIENTS_PER_SERVING_COLUMN_HEADING = "nutrientsPerServingColumnHeading";
 
 /** The Constant NUTRIENTS_COLUMN_HEADING. */
 private static final String NUTRIENTS_COLUMN_HEADING = "nutrientsColumnHeading";
 
 /** The Constant DESCRIPTION_COLUMN_HEADING. */
 private static final String DESCRIPTION_COLUMN_HEADING = "descriptionColumnHeading";
 
 /** The Constant NUTRIENTS_CTA_LABEL. */
 private static final String NUTRIENTS_CTA_LABEL = "nutrientsCtaLabel";
 
 /** The Constant RECIPE_NUTRIENTS_NUTRIENTS_PER_SERVING_COLUMN_HEADING. */
 private static final String RECIPE_NUTRIENTS_NUTRIENTS_PER_SERVING_COLUMN_HEADING = "recipeNutrients.nutrientsPerServingColumnHeading";
 
 /** The Constant RECIPE_NUTRIENTS_NUTRIENTS_COLUMN_HEADING. */
 private static final String RECIPE_NUTRIENTS_NUTRIENTS_COLUMN_HEADING = "recipeNutrients.nutrientsColumnHeading";
 
 /** The Constant RECIPE_NUTRIENTS_DESCRIPTION_COLUMN_HEADING. */
 private static final String RECIPE_NUTRIENTS_DESCRIPTION_COLUMN_HEADING = "recipeNutrients.descriptionColumnHeading";
 
 /** The Constant RECIPE_NUTRIENTS_NUTRIENTS_CTA_LABEL. */
 private static final String RECIPE_NUTRIENTS_NUTRIENTS_CTA_LABEL = "recipeNutrients.nutrientsCtaLabel";
 
 /** The Constant IS_SHOW_NUTRIENTS. */
 private static final String IS_SHOW_NUTRIENTS = "isShowNutrients";
 
 /** The Constant IS_SHOW_NUTRIENTS_PER_SERVING. */
 private static final String IS_SHOW_NUTRIENTS_PER_SERVING = "isShowNutrientsPerServing";
 
 /** The Constant NUTRIENTS. */
 private static final String NUTRIENTS = "nutrients";
 
 /** The Constant NUTRIENTS_PER_SERVING. */
 private static final String NUTRIENTS_PER_SERVING = "nutrientsPerServing";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(FeaturedInstagramPostViewHelperImpl.class);
 
 /** The factory. */
 @Reference
 HttpClientBuilderFactory factory;
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  
  LOGGER.info("Recipe Hero View Helper #processData called for processing fixed list.");
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  Map<String, Object> recipesMap = new LinkedHashMap<String, Object>();
  
  I18n i18n = getI18n(resources);
  
  Map<String, Object> dialogProperties = getProperties(content);
  
  Page currentPage = getCurrentPage(resources);
  String jsonNameSpace = getJsonNameSpace(content);
  Map<String, String> recipeNutrientsMap = ComponentUtil.getConfigMap(currentPage, "recipeNutrients");
  
  RecipeConfigDTO recipeConfigDTO = RecipeHelper.getRecipeConfigurations(configurationService, currentPage, jsonNameSpace);
  
  getI18Value(i18n, recipesMap);
  RMSJson.getRecipeJsonFromPage(resources, dialogProperties, recipesMap, recipeConfigDTO, factory);
  RMSJson.setRMSServiceDownKey(resources, recipesMap, configurationService, i18n);
  recipesMap.put(IS_SHOW_NUTRIENTS, MapUtils.getBoolean(dialogProperties, NUTRIENTS, false));
  recipesMap.put(IS_SHOW_NUTRIENTS_PER_SERVING, MapUtils.getBoolean(dialogProperties, NUTRIENTS_PER_SERVING, false));
  recipesMap.put(RESULTS_TO_SHOW, MapUtils.getInteger(recipeNutrientsMap, TOTAL_RESULTS, 0));
  recipesMap.put("isCalcMenuEnabled", recipeConfigDTO.isCalmenu());
  data.put(jsonNameSpace, recipesMap);
  return data;
 }
 
 /**
  * Gets the i18 value.
  * 
  * @param resources
  *         the resources
  * @param recipesMap
  *         the recipes map
  * @return the i18 value
  */
 private void getI18Value(I18n i18n, Map<String, Object> recipesMap) {
  
  String nutrientsCtaLabel = i18n.get(RECIPE_NUTRIENTS_NUTRIENTS_CTA_LABEL);
  String descriptionColumnHeading = i18n.get(RECIPE_NUTRIENTS_DESCRIPTION_COLUMN_HEADING);
  String nutrientsColumnHeading = i18n.get(RECIPE_NUTRIENTS_NUTRIENTS_COLUMN_HEADING);
  String nutrientsPerServingColumnHeading = i18n.get(RECIPE_NUTRIENTS_NUTRIENTS_PER_SERVING_COLUMN_HEADING);
  
  recipesMap.put(NUTRIENTS_CTA_LABEL, nutrientsCtaLabel);
  recipesMap.put(DESCRIPTION_COLUMN_HEADING, descriptionColumnHeading);
  recipesMap.put(NUTRIENTS_COLUMN_HEADING, nutrientsColumnHeading);
  recipesMap.put(NUTRIENTS_PER_SERVING_COLUMN_HEADING, nutrientsPerServingColumnHeading);
 }
 
}
