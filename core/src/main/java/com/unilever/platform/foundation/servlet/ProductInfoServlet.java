/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONArray;
import org.json.JSONObject;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.components.helper.ProductHelperExt;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * The Class ProductInfoServlet.
 */
@SlingServlet(paths = { "/bin/public/cms/product" }, selectors = { "data" }, methods = { HttpConstants.METHOD_GET, HttpConstants.METHOD_POST })
@Properties(value = {
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.foundation.servlet.ProductInfoServlet", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "Product Data Servlet", propertyPrivate = false) })
public class ProductInfoServlet extends SlingAllMethodsServlet {
 
 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = 3536163301726542594L;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ProductInfoServlet.class);
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingAllMethodsServlet#doPost(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doPost(SlingHttpServletRequest slingRequest, SlingHttpServletResponse slingResponse) throws ServletException, IOException {
  slingResponse.setStatus(SlingHttpServletResponse.SC_INTERNAL_SERVER_ERROR);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doGet(SlingHttpServletRequest slingRequest, SlingHttpServletResponse slingResponse) throws ServletException, IOException {
  LOGGER.info("Inside doGet of ProductInfoServlet");
  slingResponse.setHeader("Content-Type", "application/json; charset=UTF-8");
  JSONObject dataObj = new JSONObject();
  PageManager pageManager = slingRequest.getResourceResolver().adaptTo(PageManager.class);
  ServletOutputStream out = slingResponse.getOutputStream();
  String currentPagePath = slingRequest.getParameter("cppt");
  
  String categoryPath = slingRequest.getParameter("cpt");
  List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
  if (StringUtils.isNotBlank(categoryPath) && StringUtils.isNotBlank(currentPagePath)) {
   Page subcategoryPage = pageManager.getPage(categoryPath);
   Page currentPage = pageManager.getPage(currentPagePath);
   if (subcategoryPage != null && currentPage != null) {
    dataObj.put(
      ProductConstants.SHOPNOW_MAP,
      ProductHelper.getshopNowProperties(slingRequest.getResourceResolver().adaptTo(ConfigurationService.class), currentPage,
        BaseViewHelper.getI18n(slingRequest, currentPage), slingRequest.getResourceResolver(), slingRequest));
    Iterator<Page> itr = subcategoryPage.listChildren();
    while (itr.hasNext()) {
     Page productPage = itr.next();
     Map<String, Object> map = ProductHelperExt.getProductTeaserData(productPage, currentPage, slingRequest);
     list.add(map);
    }
   }
  }
  
  JSONArray arr = new JSONArray(list);
  JSONObject data = new JSONObject();
  dataObj.put("products", arr);
  data.put("data", dataObj);
  String dataStr = new String(data.toString().getBytes("utf-8"), "iso-8859-1");
  out.print(dataStr);
  
 }
}
