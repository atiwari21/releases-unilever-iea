/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.cache.ComponentViewAndViewHelperCache;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.utils.CollectionUtils;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.utils.AdaptiveUtil;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * <p>
 * Responsible for reading author input for product copy component and generating a list of collapsible sections setting in pageContext for the view.
 * Input can be manual or reading property attributes from product data.
 * </p>
 */
@Component(description = "ProductCopyV2ViewHelperImpl", immediate = true, metatype = true, label = "ProductCopyV2ViewHelperImpl")
@Service(value = { ProductCopyV2ViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.PRODUCT_COPY_V2, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class ProductCopyV2ViewHelperImpl extends BaseViewHelper {
 
 private static final String SUB_HEADING = "subHeading";
 private static final String PRODUCT_COPY_SUB_TITLE = "productCopySubTitle";
 /**
  * Constant for retrieving product copy source text.
  */
 private static final String PRODUCT_COPY_SOURCE_TEXT = "text";
 /**
  * Constant for retrieving product copy attribute property .
  */
 private static final String PRODUCT_COPY_PROPERTY_NAME = "productCopyPropertyName";
 
 /** Constant for product copy title. */
 private static final String PRODUCT_COPY_TITLE = "productCopyTitle";
 
 /** Constant for heading label. */
 private static final String PRODUCT_COPY_HEADING = "headline";
 
 /** Constant forgetting adaptive attribute topLink. */
 private static final String TOP_LINK_I18N = "productCopy.topLink";
 
 /** Constant for description label. */
 private static final String PRODUCT_COPY_DESCRIPTION = "description";
 
 /** Constant for description label. */
 private static final String TOP_LINK = "topLink";
 
 /** The Constant PRODUCT_PATH. */
 private static final String PRODUCT_PATH = "product";
 /**
  * logger object.
  **/
 private static final Logger LOGGER = LoggerFactory.getLogger(ProductCopyV2ViewHelperImpl.class);
 
 /** The component view and view helper cache. */
 @Reference
 ComponentViewAndViewHelperCache componentViewAndViewHelperCache;
 
 /**
  * The method processData overrides method is responsible validating author input and returning a list of sections containing rich text,text This
  * method converts the multiwidget input (a String Array) into sections of maps so that it can be accessed in JSP in consistent way.This method also
  * validates the link url provided by author.
  * 
  * @param content
  *         , - {@link Map} of all user input received as input <code>ViewHelper.onGetData</code> method
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return {@link Map} holding the data
  */
 @SuppressWarnings("unchecked")
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  
  LOGGER.info("Product Copy View Helper #processData called for processing fixed list.");
  
  Map<String, Object> productCopyMap = new LinkedHashMap<String, Object>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  
  Map<String, Object> productCopyProperties = (Map<String, Object>) content.get(PROPERTIES);
  String jsonNameSpace = MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE);
  
  String productCopyTitle = MapUtils.getString(productCopyProperties, PRODUCT_COPY_TITLE);
  String productCopySubTitle = MapUtils.getString(productCopyProperties, PRODUCT_COPY_SUB_TITLE);
  
  productCopyMap = populateProductData(resources, productCopyProperties, jsonNameSpace, productCopyTitle, productCopySubTitle);
  
  return productCopyMap;
 }
 
 /**
  * Gets the i18n top link.
  * 
  * @param slingRequest
  *         the sling request
  * @return the i18n top link
  */
 private String getI18nTopLink(SlingHttpServletRequest slingRequest) {
  I18n i18n = new I18n(slingRequest);
  return i18n.get(TOP_LINK_I18N);
 }
 
 /**
  * This method is used to retrieve product copy description in a map using product copy attributes.
  * 
  * @param resources
  *         the resources
  * @param productCopyProperties
  *         the product copy properties
  * @param jsonNameSpace
  *         the json name space
  * @param productCopyProductTitle
  *         the product copy product title
  * @return product copy map
  */
 private Map<String, Object> populateProductData(final Map<String, Object> resources, Map<String, Object> productCopyProperties,
   String jsonNameSpace, String productCopyProductTitle, String productCopyProductSubTitle) {
  SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) resources.get(ProductConstants.REQUEST);
  UNIProduct uniProduct = getProductManual(resources, productCopyProperties);
  List<String> productCopyPropertyName = new ArrayList<String>();
  productCopyPropertyName.add(PRODUCT_COPY_PROPERTY_NAME);
  LOGGER.debug("created product Copy component properties Map,size of the Map is :" + productCopyProperties.size());
  String adaptiveTopLinkData = getI18nTopLink(slingRequest);
  // adding list to list of map
  List<Map<String, String>> productCopyPropertiesList = CollectionUtils.convertMultiWidgetToList(productCopyProperties, productCopyPropertyName);
  
  Map<String, Object> productCopyProductProperties = new LinkedHashMap<String, Object>();
  populateProductCopyProperties(productCopyProductTitle, productCopyProductSubTitle, resources, uniProduct, adaptiveTopLinkData,
    productCopyPropertiesList, productCopyProductProperties);
  
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(jsonNameSpace, productCopyProductProperties);
  return data;
 }
 
 /**
  * Populate product copy properties.
  * 
  * @param productCopyProductTitle
  *         the product copy product title
  * @param resources
  *         the resources
  * @param uniProduct
  *         the uni product
  * @param adaptiveTopLinkData
  *         the adaptive top link data
  * @param productCopyPropertiesList
  *         the product copy properties list
  * @param productCopyProductProperties
  *         the product copy product properties
  */
 private void populateProductCopyProperties(String productCopyProductTitle, String productCopyProductSubTitle, Map<String, Object> resources,
   UNIProduct uniProduct, String adaptiveTopLinkData, List<Map<String, String>> productCopyPropertiesList,
   Map<String, Object> productCopyProductProperties) {
  List<Map<String, Object>> productCopyDetailList = new ArrayList<Map<String, Object>>();
  int propertyLength = 0;
  int propertyListLength = productCopyPropertiesList.size();
  if (uniProduct != null) {
   Map<String, Object> productCopyProductDetails = null;
   productCopyProductProperties.put(PRODUCT_COPY_HEADING, productCopyProductTitle);
   productCopyProductProperties.put(SUB_HEADING, productCopyProductSubTitle);
   
   // creating list of product copy attributes which will look for product
   
   for (propertyLength = 0; propertyLength < propertyListLength; propertyLength++) {
    productCopyProductDetails = new LinkedHashMap<String, Object>();
    Map<String, String> map = productCopyPropertiesList.get(propertyLength);
    String propertyNameKey = map.values().iterator().next();
    String productPropertyAttribute = ProductHelper.getProperty(uniProduct, propertyNameKey);
    if (StringUtils.isNotEmpty(productPropertyAttribute)) {
     productCopyProductDetails.put(PRODUCT_COPY_SOURCE_TEXT, productPropertyAttribute);
    }
    if (MapUtils.isNotEmpty(productCopyProductDetails)) {
     productCopyDetailList.add(productCopyProductDetails);
    }
   }
   LOGGER.debug("created list of product properties for product copy,size of the list is " + productCopyDetailList.size());
   productCopyProductProperties.put(PRODUCT_COPY_DESCRIPTION, productCopyDetailList.toArray());
   if (AdaptiveUtil.isAdaptive(getSlingRequest(resources)) && StringUtils.isNotBlank(adaptiveTopLinkData)) {
    productCopyProductProperties.put(TOP_LINK, adaptiveTopLinkData);
   }
  } else {
   productCopyProductProperties.put(PRODUCT_COPY_DESCRIPTION, productCopyDetailList.toArray());
   
   LOGGER.info("Product cannot be found for current page {}", getCurrentResource(resources).getPath());
   
  }
  
 }
 
 /**
  * Gets the uniproduct.
  * 
  * @param resources
  *         the resources
  * @param productCopyProperties
  *         the product copy properties
  * 
  * @return uniproduct
  */
 
 private UNIProduct getProductManual(Map<String, Object> resources, Map<String, Object> productCopyProperties) {
  String productPagePath = MapUtils.getString(productCopyProperties, PRODUCT_PATH);
  ResourceResolver resourceResolver = getResourceResolver(resources);
  SlingHttpServletRequest slingRequest = getSlingRequest(resources);
  UNIProduct uniProduct = null;
  Page currentPage = getCurrentPage(resources);
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  if (StringUtils.isNotBlank(productPagePath)) {
   uniProduct = ProductHelper.getUniProduct(pageManager.getPage(productPagePath));
  } else {
   uniProduct = ProductHelper.getUniProduct(currentPage, slingRequest);
  }
  
  return uniProduct;
 }
}
