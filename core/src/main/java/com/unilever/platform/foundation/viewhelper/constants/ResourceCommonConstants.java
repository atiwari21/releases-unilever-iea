/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
/**
 * © Confidential property of Unilever
 * Do not reproduce or re-distribute without the express written consent of Unilever
 *
 */
package com.unilever.platform.foundation.viewhelper.constants;

/**
 * Home for all the constants related to Product.
 * 
 * 
 * 
 */
public class ResourceCommonConstants {
    
    /** The Constant NON_ALPHA_NUMERIC_REGEX. */
 public static final String NON_ALPHA_NUMERIC_REGEX = "[^a-zA-Z0-9]";
    
    /** The Constant SPACE_REGEX. */
 public static final String SPACE_REGEX = "\\s";
    
    /** The Constant UNDERSCORE. */
 public static final String UNDERSCORE = "_";
    
    /** The Constant ZERO. */
 public static final int ZERO = 0;
    
    /** The Constant ONE. */
 public static final int ONE = 1;
    
    /** The Constant TWO. */
 public static final int TWO = 2;
    /**
     * Instantiates a new product constants.
     */
 private ResourceCommonConstants() {
        
    }
    
}
