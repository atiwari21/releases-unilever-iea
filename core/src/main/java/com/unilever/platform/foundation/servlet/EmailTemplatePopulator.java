/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.servlet;

import java.io.IOException;

import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.io.JSONWriter;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unilever.platform.foundation.emailtemplate.config.EmailTemplateConfigService;

/**
 * The Class EmailTemplatePopulator.
 */
@SlingServlet(label = "Unilever Email Template List populator", resourceTypes = { "/apps/unilever-iea/pagecomponents/emailPage" }, methods = { HttpConstants.METHOD_GET }, selectors = { "jsons.templateTypes" }, extensions = { "json" })
@Properties({
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.foundation.servlet.EmailTemplatePopulator", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "Unilever Email template servlet to populate template list", propertyPrivate = false),

})
public class EmailTemplatePopulator extends SlingAllMethodsServlet {
 
 /** The email template config service. */
 
 @Reference
 EmailTemplateConfigService emailTemplateConfigService;
 
 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = -7336062439904163757L;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(EmailTemplatePopulator.class);
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
  String[] templates = emailTemplateConfigService.getEmailTemplates();
  JSONWriter writer = new JSONWriter(response.getWriter());
  try {
   writer.array();
   
   for (String template : templates) {
    writer.object();
    writer.key("text").value(template);
    writer.key("value").value(template);
    writer.endObject();
   }
   writer.endArray();
  } catch (JSONException e) {
   LOGGER.error("Error while parsing JSON", e);
  }
  
 }
 
}
