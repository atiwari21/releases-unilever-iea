/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.SearchHelper;
import com.unilever.platform.foundation.template.dto.Image;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * The Class SearchInputV2ViewHelperImpl.
 */
@Component(description = "SearchInputV2ViewHelperImpl", immediate = true, metatype = true, label = "SearchInputV2ViewHelperImpl")
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.SEARCH_INPUT_V2, propertyPrivate = true), })
public class SearchInputV2ViewHelperImpl extends BaseViewHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(SearchInputV2ViewHelperImpl.class);
 
 /** The Constant SEARCH_LABEL_TEXT. */
 private static final String SEARCH_LABEL_TEXT = "searchLabelText";
 
 /** The Constant SEARCH_HEADING_TEXT. */
 public static final String SEARCH_HEADING_TEXT = "searchHeadingText";
 
 /** The Constant SEARCH_INPUT_PLACEHOLDER_TEXT. */
 public static final String SEARCH_INPUT_PLACEHOLDER_TEXT = "searchInputPlaceholderText";
 
 /** The Constant CTA_LABEL. */
 public static final String CTA_LABEL = "ctaLabel";
 
 /** The Constant SEARCH_RESULT_PAGE. */
 private static final String SEARCH_RESULT_PAGE = "searchResultPage";
 
 /** The Constant DISABLE_AUTO_SUGGEST. */
 private static final String DISABLE_AUTO_SUGGEST = "disableAutoSuggest";
 
 /** The Constant DISABLE_AUTO_COMPLETE_JSON_LABEL. */
 private static final String DISABLE_AUTO_COMPLETE_JSON_LABEL = "disableAutoComplete";
 
 /** The Constant AUTO_SUGGEST_RESULT_COUNT. */
 private static final String AUTO_SUGGEST_RESULT_COUNT = "autoSuggestResultCount";
 
 /** The Constant SEARCH_UNDER. */
 private static final String SEARCH_UNDER = "searchUnder";
 
 /** The Constant INCLUDE_CONTENT_TYPE. */
 private static final String INCLUDE_CONTENT_TYPE = "includeContentType";
 
 /** The Constant INCLUDE_CONTENT_TYPE. */
 private static final String INCLUDE_CONTENT_TYPE_JSON_LABEL = "includeContentType";
 
 /** The Constant MIN_CHAR_TO_TRIGGER_SEARCH. */
 private static final String MIN_CHAR_TO_TRIGGER_SEARCH = "minCharToTriggerAutoSuggest";
 
 /** The Constant MIN_CHAR_TO_TRIGGER_SEARCH_JSON_LABEL. */
 private static final String MIN_CHAR_TO_TRIGGER_SEARCH_JSON_LABEL = "minCharToTriggerAutoSuggest";
 
 /** The Constant CONTENT_TYPE. */
 private static final String CONTENT_TYPE = "contentType";
 
 /** The Constant DISABLE_CONTENT_TYPE_FILTER. */
 private static final Object DISABLE_CONTENT_TYPE_FILTER = "disableContentTypeFilter";
 
 /** The Constant TWO. */
 private static final int TWO = 2;
 
 /** The Constant SUB_BRAND_NAME. */
 private static final String SUB_BRAND_NAME = "subBrandName";
 
 
 /**
  * This class prepared the data map for Search Input component.
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("Executing AdaptiveImageViewHelperImpl.java. Inside processData method");
  
  Page currentPage = getCurrentPage(resources);
  ResourceResolver resourceResolver = getResourceResolver(resources);
  Resource resource = getCurrentResource(resources);
  
  Map<String, Object> finalMap = new LinkedHashMap<String, Object>();
  Map<String, Object> searchImageMap = new LinkedHashMap<String, Object>();
  Map<String, Object> properties = getProperties(content);
  Map<String, String> configMap = ComponentUtil.getConfigMap(currentPage, SearchHelper.SEARCH_INPUT);
  Map<String, String> contentTypeMap = ComponentUtil.getConfigMap(currentPage, CONTENT_TYPE);
  
  Boolean overrideGlobalConfig = MapUtils.getBoolean(properties, "overrideGlobalConfig", false);
  
  // If override configuration is set to true, get overriden config map or else put values from dialog
  if (overrideGlobalConfig) {
   finalMap = GlobalConfigurationUtility.getOverriddenConfigMap(configMap, properties);
  } else {
   finalMap.put(MIN_CHAR_TO_TRIGGER_SEARCH_JSON_LABEL, MapUtils.getInteger(configMap, MIN_CHAR_TO_TRIGGER_SEARCH, SearchHelper.THREE));
   finalMap.put(SearchHelper.DISABLE_AUTO_SUGGEST_JSON_LABEL, MapUtils.getBoolean(configMap, DISABLE_AUTO_SUGGEST, false));
   finalMap.put(DISABLE_AUTO_COMPLETE_JSON_LABEL, MapUtils.getBoolean(configMap, SearchHelper.DISABLE_AUTO_COMPLETE, false));
   finalMap.put(SearchHelper.AUTO_SUGGEST_RESULT_COUNT_JSON_LABEL, MapUtils.getInteger(configMap, AUTO_SUGGEST_RESULT_COUNT, SearchHelper.FIVE));
   finalMap.put(SearchHelper.DISABLE_CONTENT_TYPE_FILTER_JSON_LABEL, MapUtils.getBoolean(configMap, DISABLE_CONTENT_TYPE_FILTER, false));
   finalMap.put(SearchHelper.SEARCH_SUGGESTION_HANDLER_JSON_LABEL, MapUtils.getString(configMap, SearchHelper.SEARCH_SUGGESTION_HANDLER, ""));
   finalMap.put(SearchHelper.REQUEST_SERVLET_JSON_LABEL, MapUtils.getString(configMap, SearchHelper.REQUEST_SERVLET, ""));
  }
  
  searchImageMap.put("searchLabelText", MapUtils.getString(properties, SEARCH_LABEL_TEXT, ""));
  searchImageMap.put("searchHeadingText", MapUtils.getString(properties, SEARCH_HEADING_TEXT, ""));
  searchImageMap.put("searchInputPlaceholderText", MapUtils.getString(properties, SEARCH_INPUT_PLACEHOLDER_TEXT, ""));
  searchImageMap.put("ctaLabel", MapUtils.getString(properties, CTA_LABEL, ""));
  searchImageMap.put("inLabel", getI18n(resources).get("searchInputV2.inLabel"));
  searchImageMap.put("imageAltText", MapUtils.getString(properties, "logoImageAltText", StringUtils.EMPTY));
  
  String searchResultPage = MapUtils.getString(properties, SEARCH_RESULT_PAGE, "");
  String searchresultsPage = resourceResolver.map(searchResultPage);
  if (StringUtils.isNotEmpty(searchresultsPage)) {
   searchresultsPage += ".html";
  }
  searchImageMap.put("searchResultPage", searchresultsPage);
  
  // If disable content type filter is set true, then no includeContentType will appear in JSON. If it is set to false, it will first read from
  // dialog. If not availablr in dialog then send all default value.
  boolean subContentType = false;
  List<Map<String, Object>> includeContentType = new ArrayList<Map<String, Object>>();
  if (!MapUtils.getBoolean(finalMap, DISABLE_CONTENT_TYPE_FILTER, false)) {
   if (properties.containsKey(INCLUDE_CONTENT_TYPE)) {
    // read values from dialog.
    subContentType = true;
    includeContentType = getContentTypeFromDialog(ComponentUtil.getNestedMultiFieldProperties(resource, INCLUDE_CONTENT_TYPE),
      BaseViewHelper.getI18n(resources));
   } else {
    // send all default value to JSON from contentType configuration.
    includeContentType = getContentType(contentTypeMap, BaseViewHelper.getI18n(resources));
   }
  }
  String sortingOrder = GlobalConfigurationUtility.getValueFromConfiguration(currentPage.adaptTo(ConfigurationService.class), currentPage, "sortOrderCategory", "contentTypeOrder");
  if(StringUtils.isNotBlank(sortingOrder)){
   List<String> sortingOrderList = Arrays.asList(sortingOrder.split(","));
   includeContentType=ComponentUtil.sortListByConfigOrder(includeContentType, "contentType", sortingOrderList);
  }
  searchImageMap.put(INCLUDE_CONTENT_TYPE_JSON_LABEL, includeContentType.toArray());
  // Read logo image path and put it in image map.
  String imgPath = MapUtils.getString(properties, SearchHelper.SEARCH_LOGO_IMAGE, "");
  String logoImgAltText = MapUtils.getString(properties, "logoImageAltText", StringUtils.EMPTY);
  Image image = new Image(imgPath, logoImgAltText, StringUtils.EMPTY, currentPage, getSlingRequest(resources));
  
  searchImageMap.put("searchLogoImage", image.convertToMap());
  
  searchImageMap.put(SearchHelper.DISABLE_AUTO_SUGGEST_JSON_LABEL, MapUtils.getBoolean(finalMap, DISABLE_AUTO_SUGGEST, false));
  searchImageMap.put(DISABLE_AUTO_COMPLETE_JSON_LABEL, MapUtils.getBoolean(finalMap, SearchHelper.DISABLE_AUTO_COMPLETE, false));
  searchImageMap.put(SearchHelper.AUTO_SUGGEST_RESULT_COUNT_JSON_LABEL, MapUtils.getInteger(finalMap, AUTO_SUGGEST_RESULT_COUNT, SearchHelper.FIVE));
  searchImageMap.put("searchUnder", MapUtils.getString(properties, SEARCH_UNDER, ""));
  searchImageMap.put(MIN_CHAR_TO_TRIGGER_SEARCH_JSON_LABEL, MapUtils.getInteger(finalMap, MIN_CHAR_TO_TRIGGER_SEARCH, SearchHelper.THREE));
  searchImageMap.put(SearchHelper.REQUEST_SERVLET_JSON_LABEL, resourceResolver.map(MapUtils.getString(finalMap, SearchHelper.REQUEST_SERVLET, "")));
  searchImageMap.put(SearchHelper.SEARCH_SUGGESTION_HANDLER_JSON_LABEL,
    resourceResolver.map(MapUtils.getString(finalMap, SearchHelper.SEARCH_SUGGESTION_HANDLER, "")));
  if (CollectionUtils.isEmpty(includeContentType)) {
   searchImageMap.put(SearchHelper.DISABLE_CONTENT_TYPE_FILTER_JSON_LABEL, true);
  } else {
   searchImageMap.put(SearchHelper.DISABLE_CONTENT_TYPE_FILTER_JSON_LABEL, MapUtils.getBoolean(finalMap, DISABLE_CONTENT_TYPE_FILTER, false));
  }
  searchImageMap.put("isSubContentType", subContentType);
  if (subContentType) {
   searchImageMap.put("subContentType", getIncludedContents(ComponentUtil.getNestedMultiFieldProperties(resource, INCLUDE_CONTENT_TYPE)));
  }
  searchImageMap.put("searchInputPath", getCurrentResource(resources).getPath());
  boolean hideSubBrand = MapUtils.getBooleanValue(properties, "hideSubBrand");
  searchImageMap.put("hideSubBrand", hideSubBrand);
  searchImageMap.put("subBrandMap", getSubBrandMap(resources, properties));
  
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  data.put(getJsonNameSpace(content), searchImageMap);
  
  return data;
 }
 
 /**
  * This method returns content type that has to be included in result. It returns list of indivisual contentType maps containing single content type.
  * 
  * @param configMap
  *         The global configuration map.
  * @return contentTypeMapList The list containing contentType maps.
  */
 private List<Map<String, Object>> getContentType(Map<String, String> configMap, I18n i18n) {
  LOGGER.debug("Executing AdaptiveImageViewHelperImpl.java. Inside getContentType method");
  
  // Extract all keys from configMap.
  Set<String> contentTypeSet = configMap.keySet();
  Iterator<String> iterator = contentTypeSet.iterator();
  List<Map<String, Object>> contentTypeMapList = new ArrayList<Map<String, Object>>();
  
  // Iterate through each key and put it in contentTypeMap.
  while (iterator.hasNext()) {
   Map<String, Object> contentTypeMap = new LinkedHashMap<String, Object>();
   String contentName = iterator.next();
   String i18nLabel = String.format(SearchHelper.CONTENT_TYPE_FORMAT, StringUtils.deleteWhitespace(contentName));
   contentTypeMap.put(CONTENT_TYPE, i18n.get(i18nLabel));
   contentTypeMapList.add(contentTypeMap);
  }
  
  return contentTypeMapList;
 }
 
 /**
  * 
  * @param contentMap
  * @param i18n
  * @return
  */
 private List<Map<String, Object>> getContentTypeFromDialog(List<Map<String, String>> contentMap, I18n i18n) {
  
  List<Map<String, Object>> contentTypeMapList = new ArrayList<Map<String, Object>>();
  for (Map<String, String> keys : contentMap) {
   Map<String, Object> contentTypeMap = new LinkedHashMap<String, Object>();
   String contentName = (String) keys.get(CONTENT_TYPE);
   String i18nLabel = String.format(SearchHelper.CONTENT_TYPE_FORMAT, StringUtils.deleteWhitespace(contentName));
   contentTypeMap.put(CONTENT_TYPE, i18n.get(i18nLabel));
   contentTypeMapList.add(contentTypeMap);
  }
  Map<String, Object> everythingMap = new HashMap<String, Object>();
  String i18neverythingLabel = String.format(SearchHelper.CONTENT_TYPE_FORMAT, StringUtils.deleteWhitespace("everything"));
  everythingMap.put(CONTENT_TYPE, i18n.get(i18neverythingLabel));
  contentTypeMapList.add(everythingMap);
  return contentTypeMapList;
 }
 
 /**
  * 
  * @param contentMap
  * @return
  */
 private String getIncludedContents(List<Map<String, String>> contentMap) {
  String formatForInput = "%s,";
  String includeContents = StringUtils.EMPTY;
  for (Map<String, String> keys : contentMap) {
   String contentName = (String) keys.get(CONTENT_TYPE);
   includeContents += String.format(formatForInput, contentName);
  }
  includeContents = StringUtils.substring(includeContents, 0, includeContents.length() - 1);
  return includeContents;
 }
 
 /**
  * 
  * @param resources
  * @param properties
  * @return
  */
 
 public Map<String, Object> getSubBrandMap(Map<String, Object> resources, Map<String, Object> properties) {
  Map<String, Object> subBrandMap = new HashMap<String, Object>();
  ResourceResolver resolver = getResourceResolver(resources);
  TagManager manager = resolver.adaptTo(TagManager.class);
  Page currentPage = getCurrentPage(resources);
  boolean selectSubBrand = MapUtils.getBooleanValue(properties, "selectSubBrand");
  String selectedBrand = StringUtils.EMPTY;
  String[] subBrandName = properties.get(SUB_BRAND_NAME) != null ? (String[]) properties.get(SUB_BRAND_NAME) : ArrayUtils.EMPTY_STRING_ARRAY;
  if (!ArrayUtils.isEmpty(subBrandName)) {
   selectedBrand = subBrandName[0];
  }
  if (selectSubBrand) {
   Tag[] pageTagSet = manager.getTags(currentPage.getContentResource());
   Set<String> pageTagsSet = new HashSet<String>();
   for (Tag pageTag : pageTagSet) {
    pageTagsSet.add(pageTag.getTagID());
   }
   List<Map<String, String>> subBrandsList = ComponentUtil.getNestedMultiFieldProperties(getCurrentResource(resources), "subBrands");
   Map<String, String[]> data = new LinkedHashMap<String, String[]>();
   for (Map<String, String> subBrand : subBrandsList) {
    String tagsRelation = MapUtils.getString(subBrand, "tagsRelation");
    String inclusionTag = MapUtils.getString(subBrand, "inclusionTags");
    String matchingTag = MapUtils.getString(subBrand, "matchingTag");
    String exclusionTag = MapUtils.getString(subBrand, "exclusionTags");
    if (subBrandSelected(pageTagsSet, inclusionTag, "AND", manager, resolver, false, false)
      && subBrandSelected(pageTagsSet, matchingTag, tagsRelation, manager, resolver, false, true)
      && subBrandSelected(pageTagsSet, exclusionTag, "AND", manager, resolver, true, false)) {
     data.put(MapUtils.getString(subBrand, SUB_BRAND_NAME), new String[] { inclusionTag, matchingTag });
    }
   }
   String subBrand = getSubBrandByScore(data, manager, pageTagsSet);
   if (StringUtils.isNotBlank(subBrand)) {
    selectedBrand = subBrand;
   }
  }
  subBrandMap.put("selectedSubBrand", selectedBrand);
  subBrandMap.put("selectSubBrand", selectSubBrand);
  StringBuilder selectedSubBrandClass = new StringBuilder();
  subBrandMap.put("subBrands", SearchHelper.getSubBrandProperties(subBrandName, resources, StringUtils.EMPTY, selectedSubBrandClass, selectedBrand));
  subBrandMap.put("selectedSubBrandClass", selectedSubBrandClass);
  
  return subBrandMap;
 }
 
 /**
  * 
  * @param pageTags
  * @param exclusionTags
  * @param tagsRelation
  * @param manager
  * @param resolver
  * @param select
  * @param children
  * @return
  */
 public boolean subBrandSelected(Set<String> pageTags, String exclusionTags, String tagsRelation, TagManager manager, ResourceResolver resolver,
   boolean select, boolean children) {
  boolean selected = true;
  List<Set<String>> tagsSetList = getTagSet(exclusionTags, children, resolver);
  if (CollectionUtils.isEmpty(tagsSetList)) {
   return selected;
  }
  for (Set<String> tagsSet : tagsSetList) {
   Set<String> pageTagsSet = new HashSet<String>();
   pageTagsSet.addAll(pageTags);
   if (StringUtils.equals(tagsRelation, "AND")) {
    selected = Boolean.logicalAnd(selected, pageTagsSet.removeAll(tagsSet));
   } else {
    if (pageTagsSet.removeAll(tagsSet)) {
     break;
    }
   }
  }
  if (select) {
   return !selected;
  }
  return selected;
 }
 
 private String getSubBrandByScore(Map<String, String[]> data, TagManager manager, Set<String> pageTagsSet) {
  String subBrand = StringUtils.EMPTY;
  int score = 0;
  for (Entry<String, String[]> entry : data.entrySet()) {
   String[] tagArr = entry.getValue();
   Set<String> tagSet = new HashSet<String>();
   for (int i = 0; i < tagArr.length; i++) {
    String tag = tagArr[i];
    if (StringUtils.isNotBlank(tag)) {
     for (String perTag : tag.split(",")) {
      if (StringUtils.isNotBlank(perTag)) {
       Tag tagName = manager.resolve(perTag);
       if (tagName != null) {
        if (i % TWO == 0) {
         tagSet.addAll(getTagSet(tagName, false));
        } else {
         tagSet.addAll(getTagSet(tagName, true));
        }
        
       }
      }
     }
    }
   }
   if (CollectionUtils.isNotEmpty(tagSet)) {
    @SuppressWarnings("unchecked")
    Collection<String> intersectionSet = CollectionUtils.intersection(pageTagsSet, tagSet);
    if (intersectionSet != null && intersectionSet.size() > score) {
     score = intersectionSet.size();
     subBrand = entry.getKey();
    }
   }
  }
  
  return subBrand;
 }
 
 private List<Set<String>> getTagSet(String exclusionTags, boolean children, ResourceResolver resolver) {
  TagManager manager = resolver.adaptTo(TagManager.class);
  List<Set<String>> tagsSetList = new ArrayList<Set<String>>();
  if (StringUtils.isNotEmpty(exclusionTags)) {
   for (String exclusionTag : exclusionTags.split(",")) {
    Set<String> tagSet = null;
    Tag tagName = manager.resolve(exclusionTag);
    if (tagName != null) {
     tagSet = getTagSet(tagName, children);
     tagsSetList.add(tagSet);
    }
    
   }
  }
  
  return tagsSetList;
 }
 
 private Set<String> getTagSet(Tag tag, boolean children) {
  Set<String> tagSet = new HashSet<String>();
  tagSet.add(tag.getTagID());
  if (children) {
   Iterator<Tag> subTags = tag.listAllSubTags();
   while (subTags.hasNext()) {
    tagSet.add(subTags.next().getTagID());
   }
  }
  return tagSet;
  
 }
}
