/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;

import com.sapient.platform.iea.aem.core.cache.ComponentViewAndViewHelperCache;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.sapient.platform.iea.aem.utils.CommonUtils;
import com.sapient.platform.iea.aem.utils.constants.CommonConstants;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * <p>
 * HotspotViewHelperImpl is the helper for the hotspot component and is deployed as a OSGI service in felix container.
 * <p>
 * 
 * <p>
 * This class extends {BaseViewHelper} and honors the application contract by implementing the processData method which has logic around getting the
 * image map data entered by author and sends it back.
 * <p>
 */
@Component(description = "HotspotViewHelperImpl", immediate = true, metatype = true, label = "HotspotViewHelperImpl")
@Service(value = { HotspotViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.HOTSPOT, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class HotspotViewHelperImpl extends BaseViewHelper {
 
 /*
  * reference for cache object
  */
 @Reference
 ComponentViewAndViewHelperCache componentViewAndViewHelperCache;
 
 /**
  * Constant to hold the imageMap value.
  */
 public static final String IMAGE_MAP = "imageMap";
 /**
  * This is the constant to hold the identifier for left coord.
  * 
  */
 public static final String COORD_LEFT = "coordLeft";
 /**
  * This is the constant to hold the identifier for top coord.
  * 
  */
 public static final String COORD_TOP = "coordTop";
 
 /**
  * Constant to hold the left value.
  */
 public static final String LEFT = "left";
 
 /**
  * Constant to hold the top value.
  */
 public static final String TOP = "top";
 /**
  * Constant to hold the title value.
  */
 public static final String TITLE = "title";
 
 /**
  * Constant to hold the description value.
  */
 public static final String DESCRIPTION = "description";
 /**
  * Constant to hold the image SRC value.
  */
 public static final String IMAGE_SRC = "imageSRC";
 
 /**
  * Constant to hold the hotspots value.
  */
 public static final String HOTSPOT = "hotspot";
 /**
  * Constant to hold the hotspotsJSON value.
  */
 public static final String HOTSPOTS = "hotspots";
 
 /**
  * Constant to hold the cardViewFileReference value.
  */
 public static final String FILE_REFERENCE = "fileReference";
 
 /**
  * Constant to hold the width limit.
  */
 public static final int SCALE_LIMIT = 1280;
 
 /**
  * Constant to hold the position key value.
  */
 public static final String POSITION = "position";
 
 /**
  * Constant to hold the 100.00f.
  */
 public static final float DOUBLE_HUNDRED = 100.00f;
 
 /**
  * The method processData overrides method com.sapient.cq.viewhelper.impl.ViewHelperImpl#processData(java.util.Map, java.util.Map) and is responsible
  * to get the image map properties entered by the author and convert it into json object. This json is then put into a map dataMap and is sent back
  * to the view for appropriate rendition. It also sends the data coming from ingestion in the map.
  * 
  * @param content
  *         map of all data/properties in request and will hold any <key,value> that needs to be sent back to the view.
  * @param resources
  *         Contains Day CQ objects like sling,resourceResolver.
  * @return dataMap The map contains the values which will be used to create the JSON required by the view.
  */
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  
  @SuppressWarnings("unchecked")
  Map<String, Object> properties = (Map<String, Object>) content.get(PROPERTIES);
  
  String jsonNameSpace = MapUtils.getString(content, "jsonNameSpace");
  
  String imageMapValues = MapUtils.getString(properties, IMAGE_MAP);
  Map<String, Object> dataMap = new HashMap<String, Object>();
  Map<String, Object> hotspotMap = new HashMap<String, Object>();
  if (StringUtils.isNotBlank(imageMapValues)) {
   List<Map<String, Object>> hotspotJsonList = formatImageMapValues(imageMapValues, resources, content);
   dataMap.put(HOTSPOTS, hotspotJsonList.toArray());
  } else {
   
   dataMap.put(HOTSPOTS, new ArrayList<Map<String, Object>>());
  }
  
  String extension = StringUtils.EMPTY;
  if ((MapUtils.getString(properties, FILE_REFERENCE) != null) && (!MapUtils.getString(properties, FILE_REFERENCE).isEmpty())) {
   extension = MapUtils.getString(properties, FILE_REFERENCE).substring(MapUtils.getString(properties, FILE_REFERENCE).lastIndexOf(".") + 1);
  }
  dataMap.put("background", properties.get(FILE_REFERENCE));
  dataMap.put("extension", extension);
  dataMap.put("bgTitle", MapUtils.getString(properties, TITLE, StringUtils.EMPTY));
  dataMap.put("bgAltText", MapUtils.getString(properties, "alt", StringUtils.EMPTY));
  hotspotMap.put(jsonNameSpace, dataMap);
  return hotspotMap;
  
 }
 
 /**
  * This method is used to tokenize the string which is created by the author in the image map.
  * 
  * @param imageMapValues
  *         The image map data from the CRX
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @param content
  *         - map of all data/properties in request and will hold any <key,value> that needs to be sent back to the view
  * @return hotspotsMapList The List of maps which contains the individually tokenized final values of the imagemap which will be used to create the
  *         JSON required by the view.
  */
 private List<Map<String, Object>> formatImageMapValues(final String imageMapValues, final Map<String, Object> resources,
   final Map<String, Object> content) {
  
  StringTokenizer tokenizer = new StringTokenizer(imageMapValues, "¶");
  List<Map<String, Object>> valuesList = new LinkedList<Map<String, Object>>();
  while (tokenizer.hasMoreTokens()) {
   String imageMapString = tokenizer.nextToken();
   valuesList.add(constructHotSpots(imageMapString, resources, content));
  }
  return valuesList;
 }
 
 /**
  * This method is used to tokenize the individual elements contained in a single set of imageMap as tokenized in the formatIMageMapValues method.
  * 
  * @param imageHotspots
  *         imageHotspots is the list which contains the tokens of the image map values.
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @param content
  *         - map of all data/properties in request and will hold any <key,value> that needs to be sent back to the view
  * @return imageHotspotsMap - Map containing the values of the iamgeMap.
  */
 private Map<String, Object> constructHotSpots(final String imageHotspots, final Map<String, Object> resources, final Map<String, Object> content) {
  
  String patternStr = "^[^(]*\\(([^,]*),([^,]*)[^\"§]*[^\\§]*\\§([^\\§]*)\\§([^\\§]*)\\§([^]]*)";
  
  Pattern pattern = Pattern.compile(patternStr);
  Matcher matcher = pattern.matcher(imageHotspots);
  boolean matchFound = matcher.find();
  List<String> imageMapValues = new ArrayList<String>(matcher.groupCount());
  if (matchFound) {
   // Get all groups for this match
   for (int i = 0; i <= matcher.groupCount(); i++) {
    // removing quotes & storing in list.
    imageMapValues.add(matcher.group(i).replaceAll("\"", ""));
   }
  }
  
  return createHostspotMap(imageMapValues, resources, content);
  
 }
 
 /**
  * This method is used to create a map of all the values of the image map created by the author.
  * 
  * @param imageMapValues
  *         imageMapValues are the values of the imageMap which author creates.
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @param content
  *         - map of all data/properties in request and will hold any <key,value> that needs to be sent back to the view
  * @return hotspotValuesMap - map containing all the values provided by the author for the image map.
  */
 private Map<String, Object> createHostspotMap(final List<String> imageMapValues, final Map<String, Object> resources,
   final Map<String, Object> content) {
  
  ResourceResolver resourceResolver = (ResourceResolver) resources.get("RESOURCE_RESOLVER");
  Map<String, Object> hotspotValuesMap = new LinkedHashMap<String, Object>();
  Map<String, Object> positionValueMap = new HashMap<String, Object>();
  // i is used to avoid the Magic number SONAR violation
  int i = 1;
  int coordLeft = Integer.parseInt(imageMapValues.get(i));
  int coordTop = Integer.parseInt(imageMapValues.get(++i));
  hotspotValuesMap.put(IMAGE_SRC, imageMapValues.get(++i));
  hotspotValuesMap.put(DESCRIPTION, imageMapValues.get(++i));
  hotspotValuesMap.put(TITLE, imageMapValues.get(++i));
  @SuppressWarnings("unchecked")
  String imageFileRef = MapUtils.getString((Map<String, Object>) content.get(PROPERTIES), FILE_REFERENCE);
  
  int imageWidth = CommonUtils.getImageDimension(resourceResolver, imageFileRef, CommonConstants.IMAGE_WIDTH);
  int imageLength = CommonUtils.getImageDimension(resourceResolver, imageFileRef, CommonConstants.IMAGE_LENGTH);
  
  Map<String, Float> scaledCoordMap = scaleValue(coordTop, coordLeft, imageWidth, imageLength);
  float left = scaledCoordMap.get(COORD_LEFT).floatValue();
  float top = scaledCoordMap.get(COORD_TOP).floatValue();
  positionValueMap.put(LEFT, getPercentage(left, imageWidth));
  positionValueMap.put(TOP, getPercentage(top, imageLength));
  hotspotValuesMap.put(POSITION, positionValueMap);
  
  return hotspotValuesMap;
  
 }
 
 /**
  * This method is used to calculate the percentage of the a value with respect to the base value provided.
  * 
  * @param scaledValue
  *         The value for which the percentage needs to be calculated.
  * @param baseValue
  *         The vase value against which the percentage needs to be calculated.
  * @return prcntge The percentage calculated is returned.
  */
 public float getPercentage(final float scaledValue, final Integer baseValue) {
  final float fractionNumber = DOUBLE_HUNDRED;
  float prcntage = 0;
  
  if (baseValue != null && baseValue > 0) {
   prcntage = (scaledValue * fractionNumber) / baseValue;
  }
  return prcntage;
  
 }
 
 /**
  * 
  * CQ has a img.get.java which scales any image to scale it to 1280 px breadth-wise which causes problem when we want to create hot-spot on a large
  * image For e.g. 2140 px. wide. To resolve this, the below method will be used to scale the left value of the coordinate to satisfy a very specific
  * case where breadth can exceed 1280.(default width of a web rendition created by CQ using parbase's img.get.java. In normal case it returns the
  * normal value and scale it to a bigger value accordingly if breadth exceeds 1280.
  * 
  * @param value
  * @return scaledValue
  */
 private Map<String, Float> scaleValue(final Integer coordTop, final Integer coordLeft, final Integer imageWidth, final Integer imageHeight) {
  
  float topScaledValue = 0.0f;
  float leftScaledValue = 0.0f;
  Map<String, Float> scaledCoordMap = new HashMap<String, Float>();
  
  if (imageWidth > SCALE_LIMIT) {
   leftScaledValue = ((float) coordLeft / SCALE_LIMIT) * imageWidth;
   scaledCoordMap.put(COORD_LEFT, leftScaledValue);
   
   topScaledValue = ((float) coordTop / getAbsoluteScaledImageHeight(imageWidth, imageHeight)) * imageHeight;
   scaledCoordMap.put(COORD_TOP, topScaledValue);
   
  } else {
   
   scaledCoordMap.put(COORD_LEFT, (Float) coordLeft.floatValue());
   scaledCoordMap.put(COORD_TOP, (Float) coordTop.floatValue());
  }
  
  return scaledCoordMap;
 }
 
 /**
  * When CQ scales the image breadth wise it also scales the height of the image which is visible in the dialog. This is done by img.get.java. The
  * below function is used to calculate the reduced height of the image wrt to scaled width. This method will be only required when the image width is
  * scalable.
  * 
  * For e.g. 1280 is the factor by which the width gets reduced so based on that below calculation is done. <br/>
  * The scaling factor is calculated as :- <br/>
  * <code>1280/imageWidth</code> <br/>
  * 
  * It is multiplied by the imageHeight to get the absolute height of the image which will them be used to scale the top coordinate.
  * 
  * @param imageWidth
  * @param imageHeight
  * @return
  */
 private float getAbsoluteScaledImageHeight(final Integer imageWidth, final Integer imageHeight) {
  return (SCALE_LIMIT / (float) imageWidth) * imageHeight;
 }
}
