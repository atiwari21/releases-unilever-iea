/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.utils.CollectionUtils;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.ProductBundlingToBinUtil;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.Image;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * The Class DiagnosticToolResultSectionViewHelperImpl.
 */
@Component(description = "DiagnosticToolResultSectionViewHelperImpl", immediate = true, metatype = true, label = "DiagnosticToolResultSectionViewHelperImpl")
@Service(value = { DiagnosticToolResultSectionViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.DIAGNOSTIC_TOOL_RESULT_SECTION, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class DiagnosticToolResultSectionViewHelperImpl extends BaseViewHelper {
 
 /**
 * The constant DIAGNOSTIC_TOOL_RESULT_SECTION
 */
private static final String DIAGNOSTIC_TOOL_RESULT_SECTION = "diagnosticToolResultSection";

/** The Constant ARTICLE_LIST_HEADING. */
 private static final String ARTICLE_LIST_HEADING = "articleListHeading";
 
 /** The Constant ARTICLE_LIST_SECTIONS. */
 private static final String ARTICLE_LIST_SECTIONS = "articleListSections";
 
 /** The Constant PRODUCT_LIST_HEADING. */
 private static final String PRODUCT_LIST_HEADING = "productListHeading";
 
 /** The Constant PRODUCT_LIST_SECTIONS. */
 private static final String PRODUCT_LIST_SECTIONS = "productListSections";
 
 /** The Constant LIST_HEADING. */
 private static final String LIST_HEADING = "listHeading";
 
 /** The Constant PERSONALISED_RESPONSE. */
 private static final String PERSONALISED_RESPONSE = "personalisedResponse";
 
 /** The Constant ARTICLES_SECTION. */
 private static final String ARTICLES_SECTION = "articlesSection";
 
 /** The Constant PRODUCTS_SECTION. */
 private static final String PRODUCTS_SECTION = "productsSection";
 
 /** The Constant ARTICLES_SECTION_HEADING. */
 private static final String ARTICLES_SECTION_HEADING = "articlesSectionHeading";
 
 /** The Constant PRODUCTS_SECTION_HEADING. */
 private static final String PRODUCTS_SECTION_HEADING = "productsSectionHeading";
 
 /** The Constant DEFAULT_IMAGE_RESPONSE. */
 private static final String DEFAULT_IMAGE_RESPONSE = "defaultImageResponse";
 
 /** The Constant DEFAULT_IMAGE_RESPONSE. */
 private static final String DEFAULT_IMAGE_RESPONSE_ALT_TEXT = "defaultImageResponseAltText";
 
 /** The Constant DEFAULT_TEXT_RESPONSE. */
 private static final String DEFAULT_TEXT_RESPONSE = "defaultTextResponse";
 
 /** The Constant SUMMARY_OF_SELECTED_RESPONSES_FLAG. */
 private static final String SUMMARY_OF_SELECTED_RESPONSES_FLAG = "summaryOfSelectedResponsesFlag";
 
 /** The Constant ARTICLES_FLAG. */
 private static final String ARTICLES_FLAG = "articlesFlag";
 
 /** The Constant PRODUCTS_FLAG. */
 private static final String PRODUCTS_FLAG = "productsFlag";
 
 /** The Constant PERSONALISED_TEXT_FLAG. */
 private static final String PERSONALISED_TEXT_FLAG = "personalisedTextFlag";
 
 /** The Constant SECONDARY_CTA. */
 private static final String SECONDARY_CTA = "secondaryCTA";
 
 /** The Constant PRIMARY_CTA. */
 private static final String PRIMARY_CTA = "primaryCTA";
 
 /** The Constant SUMMARY_OF_SELECTED_RESPONSES. */
 private static final String SUMMARY_OF_SELECTED_RESPONSES = "summaryOfSelectedResponses";
 
 /** The Constant ARTICLES. */
 private static final String ARTICLES = "articles";
 
 /** The Constant PRODUCTS. */
 private static final String PRODUCTS = "products";
 
 /** The Constant PERSONALISED_TEXT. */
 private static final String PERSONALISED_TEXT = "personalisedText";
 
 /** The Constant SECONDARY_OPEN_IN_NEW_WINDOW. */
 private static final String SECONDARY_OPEN_IN_NEW_WINDOW = "secondaryOpenInNewWindow";
 
 /** The Constant SECONDARY_CTA_URL. */
 private static final String SECONDARY_CTA_URL = "secondaryCtaUrl";
 
 /** The Constant SECONDARY_CTA_LABEL. */
 private static final String SECONDARY_CTA_LABEL = "secondaryCtaLabel";
 
 /** The Constant PRIMARY_CTA_URL. */
 private static final String PRIMARY_CTA_URL = "primaryCtaUrl";
 
 /** The Constant PRIMARY_CTA_LABEL. */
 private static final String PRIMARY_CTA_LABEL = "primaryCtaLabel";
 
 /** The Constant SUBHEADING_TEXT. */
 private static final String SUBHEADING_TEXT = "subheadingText";
 
 /** The Constant GENERAL_CONFIG_DATA. */
 private static final String GENERAL_CONFIG_DATA = "generalConfiguration";
 
 /** The Constant BACKGROUND_IMAGE. */
 private static final String BACKGROUND_IMAGE = "backgroundImage";
 
 /** The Constant BACKGROUND_IMAGE_ALT_TEXT. */
 private static final String BACKGROUND_IMAGE_ALT_TEXT = "backgroundImageAltText";
 
 /** The Constant LONG_SUBHEADING_TEXT. */
 private static final String LONG_SUBHEADING_TEXT = "longSubheadingText";
 
 /** The Constant HEADING_TEXT. */
 private static final String HEADING_TEXT = "headingText";
 
 /** The Constant QUESTION_TYPE. */
 private static final String QUESTION_TYPE = "questionType";
 
 /** The diagnostic tool question view helper impl. */
 @Reference
 DiagnosticToolQuestionViewHelperImpl diagnosticToolQuestionViewHelperImpl;
 /**
  * logger object.
  **/
 private static final Logger LOGGER = LoggerFactory.getLogger(DiagnosticToolResultSectionViewHelperImpl.class);
 @Reference
 ConfigurationService configurationService;
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.info("calling DiagnosticToolResultSectionViewHelperImpl helper class inside pocess data");
  ResourceResolver resourceResolver = getResourceResolver(resources);
  Page currentPage = getCurrentPage(resources);
  SlingHttpServletRequest slingHttpServletRequest = getSlingRequest(resources);
  Map<String, Object> componentProperties = getProperties(content);
  String randomNumber = MapUtils.getString(componentProperties, UnileverConstants.RANDOM_NUMBER, StringUtils.EMPTY);
  String jsonNameSpace = getJsonNameSpace(content);
  Map<String, Object> diagnosticToolResultSectionMap = new LinkedHashMap<String, Object>();
  if (StringUtils.isNotBlank(randomNumber)) {
   diagnosticToolResultSectionMap = getResultSectionMap(resourceResolver, currentPage, slingHttpServletRequest, componentProperties);
   String displayType = MapUtils.getString(componentProperties, UnileverConstants.VIEW_TYPE, StringUtils.EMPTY);
   diagnosticToolResultSectionMap.put(UnileverConstants.DISPLAY_TYPE, displayType);
  }
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  data.put(jsonNameSpace, diagnosticToolResultSectionMap);
  return data;
 }
 
 /**
  * Gets the result section map.
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param currentPage
  *         the current page
  * @param slingHttpServletRequest
  *         the sling http servlet request
  * @param componentProperties
  *         the component properties
  * @return the result section map
  */
 public Map<String, Object> getResultSectionMap(ResourceResolver resourceResolver, Page currentPage, SlingHttpServletRequest slingHttpServletRequest,
   Map<String, Object> componentProperties) {
  Map<String, Object> diagnosticToolResultSectionMap = new LinkedHashMap<String, Object>();
  diagnosticToolResultSectionMap.put(GENERAL_CONFIG_DATA,
    getCommonProperties(componentProperties, currentPage, slingHttpServletRequest, resourceResolver));
  setArticleProductAndDefaultAttributes(currentPage, slingHttpServletRequest, componentProperties, diagnosticToolResultSectionMap);
  return diagnosticToolResultSectionMap;
 }
 
 /**
  * set the default attributes like default image , text response ,article and product default details.
  * 
  * @param currentPage
  *         the current page
  * @param slingHttpServletRequest
  *         the sling http servlet request
  * @param componentProperties
  *         the component properties
  * @param diagnosticToolResultSectionMap
  *         the diagnostic tool result section map
  */
 private void setArticleProductAndDefaultAttributes(Page currentPage, SlingHttpServletRequest slingHttpServletRequest,
            Map<String, Object> componentProperties, Map<String, Object> diagnosticToolResultSectionMap) {
  String defaultTextResponse = MapUtils.getString(componentProperties, DEFAULT_TEXT_RESPONSE, StringUtils.EMPTY);
  String defaultImageResponsePath = MapUtils.getString(componentProperties, DEFAULT_IMAGE_RESPONSE, StringUtils.EMPTY);
   String defaultImageResponseAltText = MapUtils.getString(componentProperties, DEFAULT_IMAGE_RESPONSE_ALT_TEXT, StringUtils.EMPTY);
  Map<String, Object> defaultImageMap = getImageProperties(currentPage, slingHttpServletRequest, defaultImageResponsePath,
    defaultImageResponseAltText);
  Map<String, Object> personalisedResponseMap = new LinkedHashMap<String, Object>();
  String productsSectionHeading = MapUtils.getString(componentProperties, PRODUCTS_SECTION_HEADING, StringUtils.EMPTY);
  String articlesSectionHeading = MapUtils.getString(componentProperties, ARTICLES_SECTION_HEADING, StringUtils.EMPTY);
        diagnosticToolResultSectionMap.put(
                PRODUCTS_SECTION,
                getProductOrArticleMap(productsSectionHeading, PRODUCTS, PRODUCT_LIST_SECTIONS, PRODUCT_LIST_HEADING, componentProperties,
                        currentPage, slingHttpServletRequest));
        diagnosticToolResultSectionMap.put(
                ARTICLES_SECTION,
                getProductOrArticleMap(articlesSectionHeading, ARTICLES, ARTICLE_LIST_SECTIONS, ARTICLE_LIST_HEADING, componentProperties,
                        currentPage, slingHttpServletRequest));
  personalisedResponseMap.put(UnileverConstants.TEXT, defaultTextResponse);
  personalisedResponseMap.put(UnileverConstants.IMAGE, defaultImageMap);
  diagnosticToolResultSectionMap.put(PERSONALISED_RESPONSE, personalisedResponseMap);
 }
 
 /**
  * gets the details of products or articles section.
  * 
  * @param heading
  *         the heading
  * @param propertyName
  *         the property name
  * @param key
  *         the key
  * @param productOrArticleListHeading
  *         the product or article list heading
  * @param componentProperties
  *         the component properties
  * @return map of product section or article section details
  */
 private Map<String, Object> getProductOrArticleMap(String heading, String propertyName, String key, String productOrArticleListHeading,
   Map<String, Object> componentProperties, Page currentPage, SlingHttpServletRequest slingHttpServletRequest) {
     Map<String, String> configMap = ComponentUtil.getConfigMap(currentPage, DIAGNOSTIC_TOOL_RESULT_SECTION);
     List<String> propertyNames = new LinkedList<String>();
  propertyNames.add(productOrArticleListHeading);
  List<Map<String, String>> productOrArticleSections = CollectionUtils.convertMultiWidgetToList(componentProperties, propertyNames);
  String[] jsonProperties = ComponentUtil.getPropertyValueArray(componentProperties, propertyName);
  Map<String, Object> landingMap = new LinkedHashMap<String, Object>();
  List<Map<String, Object>> productsOrArticlesBlankArray = new LinkedList<Map<String, Object>>();
  List<Map<String, Object>> listSections = new LinkedList<Map<String, Object>>();
  for (Map<String, String> attributeMap : productOrArticleSections) {
   String attributeListHeading = MapUtils.getString(attributeMap, productOrArticleListHeading, StringUtils.EMPTY);
   if (jsonProperties.length > 0) {
    landingMap.put(LIST_HEADING, attributeListHeading);
    landingMap.put(propertyName, productsOrArticlesBlankArray);
    listSections.add(landingMap);
   } else {
    break;
   }
  }
  Map<String, Object> productsOrArticlesMap = new LinkedHashMap<String, Object>();
        productsOrArticlesMap.put(UnileverConstants.HEADING, heading);
        setMultiBuyProductData(propertyName, componentProperties, currentPage, slingHttpServletRequest, configMap, productsOrArticlesMap);
  productsOrArticlesMap.put(key, listSections.toArray());
  return productsOrArticlesMap;
    }
    
    /**
     * SETS MULTI BUY PRODUCT CONFIG DAATA
     * 
     * @param propertyName
     * @param componentProperties
     * @param currentPage
     * @param slingHttpServletRequest
     * @param configMap
     * @param productsOrArticlesMap
     */
    private void setMultiBuyProductData(String propertyName, Map<String, Object> componentProperties, Page currentPage,
            SlingHttpServletRequest slingHttpServletRequest, Map<String, String> configMap, Map<String, Object> productsOrArticlesMap) {
        if (PRODUCTS.equals(propertyName)) {
            ProductBundlingToBinUtil.setMultiBuyProductsConfigProperties(componentProperties, productsOrArticlesMap, configMap,
                    MapUtils.getBooleanValue(componentProperties, UnileverConstants.OVERRIDE_GLOBAL_CONFIG, true),configurationService,currentPage);
            ProductBundlingToBinUtil.setMultiBuyProductsCtaLabels(slingHttpServletRequest, productsOrArticlesMap, currentPage);
        }
    }
 
 /**
  * Prepare questions list.
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param currentPage
  *         the current page
  * @param slingHttpServletRequest
  *         the sling http servlet request
  * @param questionSummaryList
  *         the question summary list
  * @param question
  *         the question
  * @return the list
  */
 public List<Map<String, Object>> prepareQuestionsList(ResourceResolver resourceResolver, Page currentPage,
   SlingHttpServletRequest slingHttpServletRequest, List<Map<String, String>> questionSummaryList, String question) {
  List<Map<String, Object>> questionSummaryFinalList = new LinkedList<Map<String, Object>>();
  for (Map<String, String> questionItem : questionSummaryList) {
   String questionItemPath = MapUtils.getString(questionItem, question, StringUtils.EMPTY);
   Resource questionResource = (StringUtils.isNotBlank(questionItemPath)) ? resourceResolver.getResource(questionItemPath) : null;
   Map<String, Object> diagnosticToolQuestionProperties = (questionResource != null) ? questionResource.getValueMap()
     : new LinkedHashMap<String, Object>();
   String questionType = MapUtils.getString(diagnosticToolQuestionProperties, QUESTION_TYPE, StringUtils.EMPTY);
   if (StringUtils.isNotBlank(questionType)) {
    Map<String, Object> diagnosticToolQuestionMap = new LinkedHashMap<String, Object>();
    diagnosticToolQuestionMap = (questionResource != null) ? diagnosticToolQuestionViewHelperImpl.getQuestionMap(diagnosticToolQuestionProperties,
      questionResource, currentPage, slingHttpServletRequest, questionType) : new LinkedHashMap<String, Object>();
    questionSummaryFinalList.add(diagnosticToolQuestionMap);
   }
  }
  LOGGER.debug("List processed for questions list and size of the list is " + questionSummaryFinalList.size());
  return questionSummaryFinalList;
 }
 
 /**
  * gets the common properties of component.
  * 
  * @param componentProperties
  *         the component properties
  * @param currentPage
  *         the current page
  * @param slingHttpServletRequest
  *         the sling http servlet request
  * @param resourceResolver
  *         the resource resolver
  * @return map
  */
 public Map<String, Object> getCommonProperties(Map<String, Object> componentProperties, Page currentPage,
   SlingHttpServletRequest slingHttpServletRequest, ResourceResolver resourceResolver) {
  Map<String, Object> diagnosticToolResultSectionMap = new LinkedHashMap<String, Object>();
  String headingText = MapUtils.getString(componentProperties, HEADING_TEXT, StringUtils.EMPTY);
  String subheadingText = MapUtils.getString(componentProperties, SUBHEADING_TEXT, StringUtils.EMPTY);
  String longSubheadingText = MapUtils.getString(componentProperties, LONG_SUBHEADING_TEXT, StringUtils.EMPTY);
  String backgroundImagePath = MapUtils.getString(componentProperties, BACKGROUND_IMAGE, StringUtils.EMPTY);
  String backgroundImageAltText = MapUtils.getString(componentProperties, BACKGROUND_IMAGE_ALT_TEXT, StringUtils.EMPTY);
  Map<String, Object> backgroundImageMap = getImageProperties(currentPage, slingHttpServletRequest, backgroundImagePath, backgroundImageAltText);
  String primaryCtaLabel = MapUtils.getString(componentProperties, PRIMARY_CTA_LABEL, StringUtils.EMPTY);
  String primaryCtaUrl = MapUtils.getString(componentProperties, PRIMARY_CTA_URL, StringUtils.EMPTY);
  String primaryCtaUrlFull = ComponentUtil.getFullURL(resourceResolver, primaryCtaUrl, slingHttpServletRequest);
  boolean primaryCtaOpenInNewWindow = MapUtils.getBooleanValue(componentProperties, "primaryOpenInNewWindow", false);
  Map<String, Object> primaryCtaMap = getNavigationLinkMap(primaryCtaUrlFull, primaryCtaLabel, primaryCtaOpenInNewWindow);
  String secondaryCtaLabel = MapUtils.getString(componentProperties, SECONDARY_CTA_LABEL, StringUtils.EMPTY);
  String secondaryCtaUrl = MapUtils.getString(componentProperties, SECONDARY_CTA_URL, StringUtils.EMPTY);
  String secondaryCtaUrlFull = ComponentUtil.getFullURL(resourceResolver, secondaryCtaUrl, slingHttpServletRequest);
  boolean secondaryCtaOpenInNewWindow = MapUtils.getBooleanValue(componentProperties, SECONDARY_OPEN_IN_NEW_WINDOW, false);
  Map<String, Object> secondaryCtaMap = getNavigationLinkMap(secondaryCtaUrlFull, secondaryCtaLabel, secondaryCtaOpenInNewWindow);
  boolean personalisedText = MapUtils.getBoolean(componentProperties, PERSONALISED_TEXT, false);
  boolean products = MapUtils.getBoolean(componentProperties, PRODUCTS_FLAG, false);
  boolean articles = MapUtils.getBoolean(componentProperties, ARTICLES_FLAG, false);
  boolean summaryOfSelectedResponses = MapUtils.getBoolean(componentProperties, SUMMARY_OF_SELECTED_RESPONSES, false);
  String sectionIdentifierName = MapUtils.getString(componentProperties, JcrConstants.JCR_TITLE, StringUtils.EMPTY);
  diagnosticToolResultSectionMap.put(HEADING_TEXT, headingText);
  diagnosticToolResultSectionMap.put(SUBHEADING_TEXT, subheadingText);
  diagnosticToolResultSectionMap.put(LONG_SUBHEADING_TEXT, longSubheadingText);
  diagnosticToolResultSectionMap.put(BACKGROUND_IMAGE, backgroundImageMap);
  diagnosticToolResultSectionMap.put(PRIMARY_CTA, primaryCtaMap);
  diagnosticToolResultSectionMap.put(SECONDARY_CTA, secondaryCtaMap);
  diagnosticToolResultSectionMap.put("sectionIdentifierName", sectionIdentifierName);
  diagnosticToolResultSectionMap.put(PERSONALISED_TEXT_FLAG, personalisedText);
  diagnosticToolResultSectionMap.put(PRODUCTS_FLAG, products);
  diagnosticToolResultSectionMap.put(ARTICLES_FLAG, articles);
  diagnosticToolResultSectionMap.put(SUMMARY_OF_SELECTED_RESPONSES_FLAG, summaryOfSelectedResponses);
  LOGGER.debug("getting common properties in a map and size of map is " + diagnosticToolResultSectionMap.size());
  return diagnosticToolResultSectionMap;
 }
 
 /**
  * returns cta url details map.
  * 
  * @param url
  *         the url
  * @param ctaLabel
  *         the cta label
  * @param openInNewWindow
  *         the open in new window
  * @return the navigation link map
  */
 private Map<String, Object> getNavigationLinkMap(String url, String ctaLabel, boolean openInNewWindow) {
  Map<String, Object> navigationMap = new LinkedHashMap<String, Object>();
  navigationMap.put(UnileverConstants.URL, url);
  navigationMap.put(UnileverConstants.LABEL, ctaLabel);
  navigationMap.put(UnileverConstants.OPEN_IN_NEW_WINDOW, openInNewWindow);
  return navigationMap;
 }
 
 /**
  * This method is used to get image properties like path,alt text for image extension etc.
  * 
  * @param currentPage
  *         the current page
  * @param slingHttpServletRequest
  *         the sling http servlet request
  * @param fileReference
  *         the file reference
  * @param altImage
  *         the alt image
  * @return the image properties
  */
 
 public Map<String, Object> getImageProperties(Page currentPage, SlingHttpServletRequest slingHttpServletRequest, String fileReference,
   String altImage) {
  Map<String, Object> imageMap = new LinkedHashMap<String, Object>();
  Image image = new Image(fileReference, altImage, altImage, currentPage, slingHttpServletRequest);
  imageMap = image.convertToMap();
  return imageMap;
  
 }
}
