/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.aem.foundation.core.helper.URLConnectionHelper;
import com.unilever.platform.aem.foundation.core.service.SearchService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.LandingPageListHelper;
import com.unilever.platform.foundation.constants.TaggingComponentsConstants;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * The Class RelatedArticlesViewHelperImpl.
 * 
 * @author spra26
 */

@Component(description = "relatedArticles Viewhelper Impl", immediate = true, metatype = true, label = "relatedArticles helper Impl")
@Service(value = { RelatedArticlesViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.RELATED_ARTICLES, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class RelatedArticlesViewHelperImpl extends BaseViewHelper {
 
 /** The Constant ARTICLE_PROVIDER. */
 private static final String ARTICLE_PROVIDER = "articleProvider";
 
 /** The Constant RELATED_ARTICLE_READ_TIME_LABEL. */
 private static final String RELATED_ARTICLE_READ_TIME_LABEL = "relatedArticle.readTimeLabel";
 
 /** The Constant RELATED_ARTICLE_READ_TIME_SUFFIX_TEXT. */
 private static final String RELATED_ARTICLE_READ_TIME_SUFFIX_TEXT = "relatedArticle.readTimeSuffixText";
 
 /** The Constant RELATED_ARTICLE_FEATURED_ARTICLE_LABEL_TEXT. */
 private static final String RELATED_ARTICLE_FEATURED_ARTICLE_LABEL_TEXT = "relatedArticles.featuredArticleLabeltext";
 
 /** The Constant RELATED_ARTICLE_LATEST_ARTICLE_LABEL_TEXT. */
 private static final String RELATED_ARTICLE_LATEST_ARTICLE_LABEL_TEXT = "relatedArticles.latestArticleLabeltext";
 
 /** The Constant READ_ARTICLE_FEATURED_ARTICLE_LABEL_TEXT. */
 private static final String READ_ARTICLE_FEATURED_ARTICLE_LABEL_TEXT = "featuredArticleLabeltext";
 
 /** The Constant READ_ARTICLE_LATEST_ARTICLE_LABEL_TEXT. */
 private static final String READ_ARTICLE_LATEST_ARTICLE_LABEL_TEXT = "latestArticleLabeltext";
 
 /** The Constant READ_TIME_LABEL. */
 private static final String READ_TIME_LABEL = "readTimeLabel";
 
 /** The Constant READ_TIME_SUFFIX_TEXT. */
 private static final String READ_TIME_SUFFIX_TEXT = "readTimeSuffixText";
 
 /** The Constant GENERAL_CONFIG. */
 private static final String GENERAL_CONFIG = "generalConfig";
 
 /** The Constant BUILD_NAVIGATION_USING. */
 private static final String BUILD_NAVIGATION_USING = "buildNavigationUsing";
 
 /** The Constant RELATED_ARTICLES_CATEGORY. */
 private static final String RELATED_ARTICLES_CATEGORY = "relatedArticles";
 
 /** The Constant FEATURE_TAGS. */
 private static final String FEATURE_TAGS = "featureTags";
 
 /** The search service. */
 @Reference
 SearchService searchService;
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @SuppressWarnings("unchecked")
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  Map<String, Object> properties = getProperties(content);
  
  Map<String, Object> relatedArticlesMap = new LinkedHashMap<String, Object>();
  Map<String, Object> generalConfigMap = new HashMap<String, Object>();
  
  ResourceResolver resourceResolver = getResourceResolver(resources);
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  
  Resource compResource = getCurrentResource(resources);
  Page currentPage = pageManager.getContainingPage(compResource);
  Map<String, String> configMap = ComponentUtil.getConfigMap(getCurrentPage(resources), RELATED_ARTICLES_CATEGORY);
  String allThingsHairUrl = MapUtils.getString(configMap, "allThingsHairUrl", StringUtils.EMPTY);
  
  Map<String, String> globalConfigMap = ComponentUtil.getConfigMap(currentPage, RELATED_ARTICLES_CATEGORY);
  
  String articleProvider = MapUtils.getString(globalConfigMap, ARTICLE_PROVIDER);
  
  String articleProviderConfigUrl = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, articleProvider,
    "articleProviderUrl");
  
  StringBuilder uri = null;
  StringBuilder articleProviderUrl = null;
  
  String[] featureTagNameSpaces = StringUtils.isNotBlank(MapUtils.getString(properties, FEATURE_TAGS)) ? ComponentUtil.getPropertyValueArray(
    properties, FEATURE_TAGS) : new String[] {};
  
  relatedArticlesMap.put(GENERAL_CONFIG, getGeneralConfigDataMap(properties, generalConfigMap, resources));
  Map<String, Object> pagesMap = new HashMap<String, Object>();
  if (generalConfigMap.get(BUILD_NAVIGATION_USING).equals(TaggingComponentsConstants.FIXED_LIST)) {
   pagesMap
     .put("articleList", LandingPageListHelper.getPagesList(properties, resources, globalConfigMap, featureTagNameSpaces, null, true).toArray());
   
  } else if ("tags".equals(generalConfigMap.get(BUILD_NAVIGATION_USING))) {
   List<Map<String, Object>> landingPageListData = LandingPageListHelper.getTaggedPageDataList(resources, globalConfigMap, properties, searchService,
     true);
   
   pagesMap.put("articleList", landingPageListData.toArray());
  } else {
   
   uri = new StringBuilder(allThingsHairUrl);
   articleProviderUrl = getAllThingsHairFields(resources, properties, articleProviderConfigUrl);
  }
  
  Map<String, Object> tempMap = MapUtils.getMap(relatedArticlesMap, GENERAL_CONFIG);
  if (uri != null) {
   tempMap.put("requestServletUrl", resourceResolver.map(uri.toString()));
  } else {
   tempMap.put("requestServletUrl", uri);
  }
  tempMap.put("articleProviderUrl", articleProviderUrl);
  relatedArticlesMap.put("articles", pagesMap);
  
  relatedArticlesMap.put("includeInResults", getFlagMap(properties, globalConfigMap));
  
  getI18Values(resources, relatedArticlesMap);
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(getJsonNameSpace(content), relatedArticlesMap);
  return data;
 }
 
 /**
  * Gets the flag map.
  * 
  * @param properties
  *         the properties
  * @param globalConfigMap
  *         the global config map
  * @return the flag map
  */
 private Map<String, Boolean> getFlagMap(Map<String, Object> properties, Map<String, String> globalConfigMap) {
  Map<String, Boolean> flagMap = new HashMap<String, Boolean>();
  Map<String, Object> tempMap = new HashMap<String, Object>();
  boolean overrideGlobalConfig = MapUtils.getBooleanValue(properties, UnileverConstants.OVERRIDE_GLOBAL_CONFIG, false);
  
  if (overrideGlobalConfig) {
   tempMap.putAll(properties);
  } else {
   tempMap.putAll(globalConfigMap);
  }
  flagMap.put("teaserImage", MapUtils.getBoolean(tempMap, TaggingComponentsConstants.TEASER_IMAGE_FLAG, false));
  flagMap.put("teaserCopy", MapUtils.getBoolean(tempMap, TaggingComponentsConstants.TEASER_COPY_FLAG, false));
  flagMap.put("teaserLongCopy", MapUtils.getBoolean(tempMap, TaggingComponentsConstants.TEASER_LONG_COPY_FLAG, false));
  flagMap.put("publishDate", MapUtils.getBoolean(tempMap, TaggingComponentsConstants.TEASER_PUBLISH_DATE_FLAG, false));
  return flagMap;
 }
 
 /**
  * Gets the all things hair fields.
  * 
  * @param resources
  *         the resources
  * @param properties
  *         the properties
  * @param articleProviderUrl
  *         the article provider url
  * @return the all things hair fields
  */
 private StringBuilder getAllThingsHairFields(Map<String, Object> resources, Map<String, Object> properties, String articleProviderUrl) {
  String buildListUsing = MapUtils.getString(properties, "buildListUsing");
  Map<String, String> queryMap = new HashMap<String, String>();
  StringBuilder str = new StringBuilder();
  
  if (articleProviderUrl != null && StringUtils.isNotEmpty(articleProviderUrl)) {
   if ("articleSearch".equals(buildListUsing)) {
    str = getArticleSearchData(properties, articleProviderUrl, queryMap, resources);
    
   } else {
//  For "articleIds".equals(buildListUsing)
    str = getArticleIdData(properties, articleProviderUrl, resources); 
   }
  }
  return str;
  
 }
 
 /**
  * Sets the article id data.
  * 
  * @param properties
  *         the properties
  * @param allThingsHairUrl
  *         the all things hair url
  * @param queryMap
  *         the query map
  * @param resources
  *         the resources
  * @return the string builder
  */
 private StringBuilder getArticleIdData(Map<String, Object> properties, String allThingsHairUrl,
   Map<String, Object> resources) {
  
  String[] articleId = ComponentUtil.getPropertyValueArray(properties, "articleId");
  String articleProvider = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, getCurrentPage(resources), RELATED_ARTICLES_CATEGORY,
    ARTICLE_PROVIDER);
  Map<String, String> configMap = ComponentUtil.getConfigMap(getCurrentPage(resources), articleProvider);
  StringBuilder str = new StringBuilder(allThingsHairUrl);
  
  for (String article : articleId) {
   str = getUrlWithQuery(str, article, MapUtils.getString(configMap, "articleId"));
  }
  return str;
 }
 
 /**
  * Sets the article search data.
  * 
  * @param properties
  *         the properties
  * @param allThingsHairUrl
  *         the all things hair url
  * @param queryMap
  *         the query map
  * @param resources
  *         the resources
  * @return the string builder
  */
 private StringBuilder getArticleSearchData(Map<String, Object> properties, String allThingsHairUrl, Map<String, String> queryMap,
   Map<String, Object> resources) {
  String searchKeyword = MapUtils.getString(properties, "searchKeyword", StringUtils.EMPTY);
  
  boolean overrideGlobalConfig = MapUtils.getBooleanValue(properties, "overrideGlobalConfig", false);
  String limitResultCount = StringUtils.EMPTY;
  if (overrideGlobalConfig) {
   limitResultCount = MapUtils.getString(properties, "limit");
  } else {
   limitResultCount = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, getCurrentPage(resources), RELATED_ARTICLES_CATEGORY, "max");
  }
  String[] limitResultCategories = ComponentUtil.getPropertyValueArray(properties, "limitResults");
  String[] excludeArticle = ComponentUtil.getPropertyValueArray(properties, "excludeArticle");
  String articleProvider = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, getCurrentPage(resources), RELATED_ARTICLES_CATEGORY,
    ARTICLE_PROVIDER);
  Map<String, String> configMap = ComponentUtil.getConfigMap(getCurrentPage(resources), articleProvider);
  
  queryMap.put(MapUtils.getString(configMap, "searchKeyword"), searchKeyword);
  queryMap.put(MapUtils.getString(configMap, "limitResultCount"), limitResultCount);
  
  StringBuilder str = URLConnectionHelper.buildUrlWithQueryParam(allThingsHairUrl, queryMap);
  
  for (String category : limitResultCategories) {
   str = getUrlWithQuery(str, category, MapUtils.getString(configMap, "limitCategories"));
  }
  for (String exclude : excludeArticle) {
   str = getUrlWithQuery(str, exclude, MapUtils.getString(configMap, "excludeArticleIds"));
  }
  return str;
 }
 
 /**
  * Gets the url with query.
  * 
  * @param str
  *         the str
  * @param value
  *         the value
  * @param key
  *         the key
  * @return the url with query
  */
 private StringBuilder getUrlWithQuery(StringBuilder str, String value, String key) {
  Map<String, String> queryMap;
  queryMap = new HashMap<String, String>();
  queryMap.put(key, value);
  StringBuilder str1 = URLConnectionHelper.buildUrlWithQueryParam(str.toString(), queryMap);
  return str1;
 }
 
 /**
  * Gets the i 18 values.
  * 
  * @param resources
  *         the resources
  * @param pageListingMap
  *         the page listing map
  * @return the i 18 values
  */
 @SuppressWarnings("unchecked")
 private void getI18Values(Map<String, Object> resources, Map<String, Object> pageListingMap) {
  
  I18n i18n = getI18n(resources);
  Map<String, Object> generalConfigMap = MapUtils.getMap(pageListingMap, "generalConfig", MapUtils.EMPTY_MAP);
  
  generalConfigMap.put(READ_TIME_SUFFIX_TEXT, i18n.get(RELATED_ARTICLE_READ_TIME_SUFFIX_TEXT));
  generalConfigMap.put(READ_TIME_LABEL, i18n.get(RELATED_ARTICLE_READ_TIME_LABEL));
  generalConfigMap.put(READ_ARTICLE_FEATURED_ARTICLE_LABEL_TEXT, i18n.get(RELATED_ARTICLE_FEATURED_ARTICLE_LABEL_TEXT));
  generalConfigMap.put(READ_ARTICLE_LATEST_ARTICLE_LABEL_TEXT, i18n.get(RELATED_ARTICLE_LATEST_ARTICLE_LABEL_TEXT));
 }
 
 /**
  * Gets the general config data map.
  * 
  * @param properties
  *         the properties
  * @param configMap
  *         the config map
  * @return generalConfigMap
  */
 protected Map<String, Object> getGeneralConfigDataMap(Map<String, Object> properties, Map<String, Object> configMap, Map<String, Object> resources) {
  Map<String, Object> generalConfigMap = new HashMap<String, Object>();
  
  generalConfigMap.put("headingText", MapUtils.getString(properties, "headingText"));
  generalConfigMap.put("subHeadingText", MapUtils.getString(properties, "subHeadingText"));
  generalConfigMap.put("ctaLabelText", MapUtils.getString(properties, "ctaLabelText"));
  
  generalConfigMap.put("longSubHeadingText", MapUtils.getString(properties, "longSubHeadingText"));
  generalConfigMap.put("readAllArticleCtaLabel", MapUtils.getString(properties, "readAllArticleCtaLabel"));
  generalConfigMap.put("readAllArticlesCtaUrl",
    ComponentUtil.getFullURL(getResourceResolver(resources), MapUtils.getString(properties, "readAllArticlesCtaUrl"), getSlingRequest(resources)));
  generalConfigMap.put("openInNewWindow", MapUtils.getBoolean(properties, "openInNewWindow", false));
  
  generalConfigMap.put(BUILD_NAVIGATION_USING, MapUtils.getString(properties, BUILD_NAVIGATION_USING));
  
  configMap.put(BUILD_NAVIGATION_USING, MapUtils.getString(properties, BUILD_NAVIGATION_USING));
  return generalConfigMap;
 }
}
