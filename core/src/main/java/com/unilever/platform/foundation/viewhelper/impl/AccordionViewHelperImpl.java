/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sapient.platform.iea.aem.core.cache.ComponentViewAndViewHelperCache;
import com.sapient.platform.iea.aem.core.utils.CollectionUtils;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.sapient.platform.iea.aem.utils.RandomNumberUtils;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.TabbedContentHelper;
import com.unilever.platform.foundation.constants.CommonConstants;
import com.unilever.platform.foundation.constants.TaggingComponentsConstants;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * This class prepares the data map for Anchor Link.
 */
@Component(description = "AccordionViewHelperImpl", immediate = true, metatype = true, label = "Accordion ViewHelper Impl")
@Service({ AccordionViewHelperImpl.class, ViewHelper.class })
@Properties({
  @org.apache.felix.scr.annotations.Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.ACCORDION, propertyPrivate = true),
  @org.apache.felix.scr.annotations.Property(name = Constants.SERVICE_RANKING, intValue = { 4000 }, propertyPrivate = true) })
public class AccordionViewHelperImpl extends BaseViewHelper {
 private static final String KEY_CURRENT_CONTENT_PAGE_PATH = "KEY_CURRENT_CONTENT_PAGE_PATH";
 private static final int HUNDRED = 100;
 private static final String LINKS = "links";
 private static final String PANEL_LIST = "panelList";
 @Reference
 ComponentViewAndViewHelperCache componentViewAndViewHelperCache;
 private static final String MULTIPLE_OPEN_ITEMS = "multipleOpenItems";
 private static final String RANDOM_NUMBER = "randomNumber";
 private static final String EXPANDED_BY_DEFAULT = "expandedByDefault";
 private static final String PANEL_HEADING = "panelHeading";
 private static final String CSS_STYLE_PANEL = "cssStylePanel";
 private static final Logger LOGGER = LoggerFactory.getLogger(AccordionViewHelperImpl.class);
 
 public AccordionViewHelperImpl() {
 }
 
 /*
  * (Javadoc)
  * 
  * This helper class reads all the data from the anchor link component.It takes the logo, alt text and It fetcher "mobile view text" from the global
  * configsUsing the logo field, it extracts the extension, path, name of the file and passesit in the json
  * 
  * For details on json and component structure,please visit https://tools.sapient .com/confluence/display/UU/CP-03+Anchor+Link+Navigation
  */
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  @SuppressWarnings("unchecked")
  Map<String, Object> properties = (Map<String, Object>) content.get(PROPERTIES);
  ResourceResolver resourceResolver = getResourceResolver(resources);
  String[] slideRandomNumbersOrigArray = null;
  Object randomNumberObject = properties.get(UnileverConstants.RANDOM_NUMBER);
  if (randomNumberObject instanceof ArrayList) {
   Object[] randomNumberArray = ((ArrayList) randomNumberObject).toArray(new String[HUNDRED]);
   slideRandomNumbersOrigArray = CollectionUtils.convertObjectArrayToStringArray(randomNumberArray);
  } else {
   slideRandomNumbersOrigArray = CollectionUtils.convertObjectArrayToStringArray(randomNumberObject);
  }
  if (content.containsKey(KEY_CURRENT_CONTENT_PAGE_PATH)) {
   Iterator<Resource> childNodesIterator = resourceResolver.listChildren(resourceResolver.getResource((String) content
     .get(KEY_CURRENT_CONTENT_PAGE_PATH)));
   TabbedContentHelper.deleteUnwantedNodes(childNodesIterator, resourceResolver, slideRandomNumbersOrigArray);
  }
  
  List<String> propertyNames = new ArrayList<String>();
  propertyNames.add(PANEL_HEADING);
  propertyNames.add(CSS_STYLE_PANEL);
  propertyNames.add(RANDOM_NUMBER);
  propertyNames.add(EXPANDED_BY_DEFAULT);
  LOGGER.info("After property names");
  LOGGER.debug("After property names");
  List<Map<String, String>> primaryListNormal = CollectionUtils.convertMultiWidgetToList(properties, propertyNames);
  List<String> randomnumberlist = new LinkedList<String>();
  for (Map<String, String> normalMap : primaryListNormal) {
   randomnumberlist.add(MapUtils.getString(normalMap, UnileverConstants.RANDOM_NUMBER, StringUtils.EMPTY));
  }
  List<Map<String, String>> primaryList = ComponentUtil.getNestedMultiFieldProperties(getCurrentResource(resources), LINKS);
  List<Map<String, String>> updatedPrimaryList = new LinkedList<Map<String, String>>();
  Map<String, String> accordionNewMap = new LinkedHashMap<String, String>();
  int randomNumberCount = 0;
  for (Map<String, String> accordionMap : primaryList) {
   accordionNewMap = new LinkedHashMap<String, String>();
   accordionNewMap.put(PANEL_HEADING, MapUtils.getString(accordionMap, PANEL_HEADING, StringUtils.EMPTY));
   accordionNewMap.put(CSS_STYLE_PANEL, MapUtils.getString(accordionMap, CSS_STYLE_PANEL, StringUtils.EMPTY));
   accordionNewMap.put(RANDOM_NUMBER, randomnumberlist.get(randomNumberCount));
   if (TaggingComponentsConstants.TRUE.equals(MapUtils.getString(accordionMap, EXPANDED_BY_DEFAULT))) {
    accordionNewMap.put(EXPANDED_BY_DEFAULT, CommonConstants.BOOLEAN_TRUE);
   } else {
    accordionNewMap.put(EXPANDED_BY_DEFAULT, CommonConstants.BOOLEAN_FALSE);
   }
   updatedPrimaryList.add(accordionNewMap);
   randomNumberCount++;
  }
  
  String[] slideRandomNumbersNewArray = RandomNumberUtils.createRandomNumberArray(updatedPrimaryList, UnileverConstants.RANDOM_NUMBER);
  LOGGER.info("After slideRandomNumbersNewArray");
  LOGGER.debug("After slideRandomNumbersNewArray");
  if (content.get(KEY_CURRENT_CONTENT_PAGE_PATH) != null) {
   RandomNumberUtils.writeRandomNumberToNode((String) content.get(KEY_CURRENT_CONTENT_PAGE_PATH), slideRandomNumbersOrigArray,
     slideRandomNumbersNewArray, resourceResolver, UnileverConstants.RANDOM_NUMBER);
  }
  LOGGER.info("After writeRandomNumberToNode");
  LOGGER.debug("After writeRandomNumberToNode");
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  Map<String, Object> accordionMap = new LinkedHashMap<String, Object>();
  accordionMap.put(PANEL_LIST, updatedPrimaryList.toArray());
  
  accordionMap.put(MULTIPLE_OPEN_ITEMS, MapUtils.getBoolean(properties, MULTIPLE_OPEN_ITEMS, false));
  data.put(getJsonNameSpace(content), accordionMap);
  return data;
 }
}
