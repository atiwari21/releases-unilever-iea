/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.AwardHelper;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * This class prepares the data map for Product Awards Overview.
 */
@Component(description = "ProductAwardsOverviewViewHelperImpl", immediate = true, metatype = true, label = "Product Awards Overview ViewHelper")
@Service(value = { ProductAwardsOverviewViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.PRODUCT_AWARDS_OVERVIEW, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class ProductAwardsOverviewViewHelperImpl extends BaseViewHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ProductAwardsOverviewViewHelperImpl.class);
 private static final String PRODUCT_PAGE = "productPage";
 private static final String HIDE_CTA = "hideCta";
 private static final String PRODUCT_AWARDS_DETAILS_MAP = "productAwardsDetail";
 private static final String NUMBER_OF_PRIMARY_AWARDS_TO_DISPLAY = "numberOfPrimaryAwardsToDisplay";
 private static final String NUMBER_OF_SECONDARY_AWARDS_TO_DISPLAY = "numberOfSecondaryAwardsToDisplay";
 private static final String TOTAL_AWARDS_TO_DISPLAY = "totalAwardsToDisplay";
 private static final String VIEW_ALL_CTA = "productAwardsOverview.viewAllCtaLabel";
 private static final String AWARD_CONFIG_CAT = "awardConfig";
 private static final String VIEW_ALL_CTA_LABEL = "viewAllCta";
 private static final String HIDE_CTA_FLAG = "hideCtaForAllAwards";
 
 @Reference
 ConfigurationService configurationService;
 
 @SuppressWarnings("unchecked")
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("e Gifting Configurator View Helper #processData called .");
  ResourceResolver resourceResolver = (ResourceResolver) resources.get(ProductConstants.RESOURCE_RESOLVER);
  Map<String, Object> productAwardsOverviewProperties = (Map<String, Object>) content.get(PROPERTIES);
  SlingHttpServletRequest slingRequest = getSlingRequest(resources);
  Page currentPage = getCurrentPage(resources);
  Map<String, Object> properties = new LinkedHashMap<String, Object>();
  Map<String, Object> productAwardsDetailMap = new HashMap<String, Object>();
  UNIProduct product = null;
  String jsonNameSapce = getJsonNameSpace(content);
  String productPagePath = MapUtils.getString(productAwardsOverviewProperties, PRODUCT_PAGE);
  String hideCtaForAllAwards = MapUtils.getString(productAwardsOverviewProperties, HIDE_CTA);
  boolean hideCta = Boolean.parseBoolean(hideCtaForAllAwards);
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  if (StringUtils.isNotBlank(productPagePath)) {
   product = ProductHelper.getUniProduct(pageManager.getPage(productPagePath));
  } else {
   product = ProductHelper.getUniProduct(currentPage, slingRequest);
  }
  
  if (product != null) {
   productAwardsDetailMap.put(ProductConstants.AWARDS, AwardHelper.getProductAwards(product, currentPage, configurationService, resources).toArray());
  } else {
   productAwardsDetailMap.put(ProductConstants.AWARDS, new ArrayList<Map<String, Object>>().toArray());
   LOGGER.info("No Product is available for current page");
  }
  productAwardsDetailMap.put(HIDE_CTA_FLAG, hideCta);
  productAwardsDetailMap.put(VIEW_ALL_CTA_LABEL, getI18n(resources).get(VIEW_ALL_CTA));
  setGlobalConfigAttributes(currentPage, productAwardsDetailMap, productAwardsOverviewProperties);
  properties.put(PRODUCT_AWARDS_DETAILS_MAP, productAwardsDetailMap);
  LOGGER.info("The e Gifting Configurator map is successfully generated");
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(jsonNameSapce, properties);
  return data;
 }
 
 /**
  * Sets the product gift attributes.
  * 
  * @param uniProduct
  *         the uni product
  * @param currentPage
  *         the current page
  * @param slingRequest
  *         the sling request
  * @return the product gift map
  */
 private void setGlobalConfigAttributes(Page currentPage, Map<String, Object> productAwardsDetailMap,
   Map<String, Object> productAwardsOverviewProperties) {
  boolean overrideGlobalConfig = MapUtils.getBoolean(productAwardsOverviewProperties, UnileverConstants.OVERRIDE_GLOBAL_CONFIG) != null ? MapUtils
    .getBoolean(productAwardsOverviewProperties, UnileverConstants.OVERRIDE_GLOBAL_CONFIG) : false;
  
  int numberOfPrimaryAwardsToDisplay;
  int numberOfSecondaryAwardsToDisplay;
  int totalAwardsToDisplay;
  boolean hideCtaForAllAwards = false;
  
  if (overrideGlobalConfig) {
   numberOfPrimaryAwardsToDisplay = MapUtils.getIntValue(productAwardsOverviewProperties, NUMBER_OF_PRIMARY_AWARDS_TO_DISPLAY, 0);
   numberOfSecondaryAwardsToDisplay = MapUtils.getIntValue(productAwardsOverviewProperties, NUMBER_OF_SECONDARY_AWARDS_TO_DISPLAY, 0);
   totalAwardsToDisplay = MapUtils.getIntValue(productAwardsOverviewProperties, TOTAL_AWARDS_TO_DISPLAY, 0);
   hideCtaForAllAwards = MapUtils.getBoolean(productAwardsOverviewProperties, HIDE_CTA) != null ? MapUtils.getBoolean(
     productAwardsOverviewProperties, HIDE_CTA) : false;
  } else {
   numberOfPrimaryAwardsToDisplay = GlobalConfigurationUtility.getIntegerValueFromConfiguration(AWARD_CONFIG_CAT, configurationService, currentPage,
     NUMBER_OF_PRIMARY_AWARDS_TO_DISPLAY);
   numberOfSecondaryAwardsToDisplay = GlobalConfigurationUtility.getIntegerValueFromConfiguration(AWARD_CONFIG_CAT, configurationService,
     currentPage, NUMBER_OF_SECONDARY_AWARDS_TO_DISPLAY);
   totalAwardsToDisplay = GlobalConfigurationUtility.getIntegerValueFromConfiguration(AWARD_CONFIG_CAT, configurationService, currentPage,
     TOTAL_AWARDS_TO_DISPLAY);
   hideCtaForAllAwards = Boolean.parseBoolean(GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
     AWARD_CONFIG_CAT, HIDE_CTA_FLAG));
  }
  productAwardsDetailMap.put(NUMBER_OF_PRIMARY_AWARDS_TO_DISPLAY, numberOfPrimaryAwardsToDisplay);
  productAwardsDetailMap.put(NUMBER_OF_SECONDARY_AWARDS_TO_DISPLAY, numberOfSecondaryAwardsToDisplay);
  productAwardsDetailMap.put(HIDE_CTA_FLAG, hideCtaForAllAwards);
  productAwardsDetailMap.put(TOTAL_AWARDS_TO_DISPLAY, totalAwardsToDisplay);
 }
 
}
