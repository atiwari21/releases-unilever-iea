/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.components.dto;

import java.util.List;
import java.util.Map;

/**
 * The Class SearchFilterOptionsDTO.
 */
public class SearchFilterOptionsDTO {
 
 /** The content type. */
 private Map<String, String> contentType;
 
 /** The configure options. */
 private List<Map<String, Object>> configureOptions;
 
 /**
  * Gets the configure options.
  * 
  * @return the configure options
  */
 public List<Map<String, Object>> getConfigureOptions() {
  return configureOptions;
 }
 
 /**
  * Sets the configure options.
  * 
  * @param configureOptions
  *         the configure options
  */
 public void setConfigureOptions(List<Map<String, Object>> configureOptions) {
  this.configureOptions = configureOptions;
 }
 
 /**
  * Gets the content type.
  * 
  * @return the content type
  */
 public String getContentType() {
  return contentType.keySet().iterator().next();
 }
 
 /**
  * Sets the content type.
  * 
  * @param contentType
  *         the content type
  */
 public void setContentType(Map<String, String> contentType) {
  this.contentType = contentType;
 }
 
 /**
  * Gets the content type value.
  * 
  * @param contentTypeKey
  *         the content type key
  * @return the content type value
  */
 public String getContentTypeValue(String contentTypeKey) {
  return contentType.get(contentTypeKey);
 }
 
}
