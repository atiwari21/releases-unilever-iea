/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * <p>
 * This class prepares the data map for Purchase Order Confirmation Component It reads the values from the dialog and passes the info as a Map.
 * </p>
 */
@Component(description = "PurchaseOrderConfirmation", immediate = true, metatype = true, label = "PurchaseOrderConfirmationViewHelperImpl")
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.PURCHASE_ORDER_CONFIRMATION, propertyPrivate = true), })
public class PurchaseOrderConfirmationViewHelperImpl extends BaseViewHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(PurchaseOrderConfirmationViewHelperImpl.class);
 
 /** The Constant HEADING_TEXT. */
 private static final String HEADING_TEXT = "headingText";
 
 /** The Constant ORDER_CONFIRMATION_MESSAGE. */
 private static final String ORDER_CONFIRMATION_MESSAGE = "text";
 
 /** The Constant CTA_LABEL. */
 private static final String CTA_LABEL = "ctaLabel";
 
 /** The Constant CTA_URL. */
 private static final String CTA_URL = "ctaUrl";
 
 /** The Constant OPEN_IN_NEW_WINDOW. */
 private static final String OPEN_IN_NEW_WINDOW = "openInNewWindow";
 
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("Executing PurchaseOrderConfirmationViewHelperImpl.java. Inside processData method");
  
  Map<String, Object> purchaseOrderConfirmationMap = new LinkedHashMap<String, Object>();
  Map<String, Object> properties = getProperties(content);
  ResourceResolver resourceResolver = getResourceResolver(resources);
  
  purchaseOrderConfirmationMap.put("headingText", MapUtils.getString(properties, HEADING_TEXT, StringUtils.EMPTY));
  purchaseOrderConfirmationMap.put("orderConfirmationMessage", MapUtils.getString(properties, ORDER_CONFIRMATION_MESSAGE, StringUtils.EMPTY));
  purchaseOrderConfirmationMap.put("ctaLabel", MapUtils.getString(properties, CTA_LABEL, StringUtils.EMPTY));
  purchaseOrderConfirmationMap.put("ctaUrl",
    ComponentUtil.getFullURL(resourceResolver, MapUtils.getString(properties, CTA_URL, StringUtils.EMPTY), getSlingRequest(resources)));
  purchaseOrderConfirmationMap.put("openInNewWindow", MapUtils.getString(properties, OPEN_IN_NEW_WINDOW, StringUtils.EMPTY));
  
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  data.put(getJsonNameSpace(content), purchaseOrderConfirmationMap);
  
  return data;
 }
 
}
