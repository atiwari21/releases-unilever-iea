/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.constants;

/**
 * The Class QuickPollConstants.
 */
public class QuickPollConstants {
 /** The Constant POST_POLL_URL. */
 public static final String POST_POLL_URL = "postPollUrl";
 
 /** The Constant GET_POLL_URL. */
 public static final String GET_POLL_URL = "getPollUrl";
 
 /** The Constant QUICK_POLL_SUBMIT_LABEL. */
 public static final String QUICK_POLL_SUBMIT_LABEL = "quickPoll.SubmitLabel";
 
 /** The Constant QUICK_POLL_READ_LESS_LABEL. */
 public static final String QUICK_POLL_READ_LESS_LABEL = "quickPoll.readLessLabel";
 
 /** The Constant QUICK_POLL_READ_MORE_LABEL. */
 public static final String QUICK_POLL_READ_MORE_LABEL = "quickPoll.readMoreLabel";
 
 /** The Constant QUESTION_PATH. */
 public static final String QUESTION_PATH = "questionPath";
 
 /** The Constant QUICK_POLL_HEADLINE. */
 public static final String QUICK_POLL_HEADLINE = "quickPollHeadline";
 
 /** The Constant FILE_REFERENCE. */
 public static final String FILE_REFERENCE = "fileReference";
 
 /** The Constant IMAGE. */
 public static final String IMAGE = "image";
 
 /** The Constant OPEN_IN_NEW_WINDOW. */
 public static final String OPEN_IN_NEW_WINDOW = "openInNewWindow";
 
 /** The Constant RECOMMENDED_CONTENT. */
 public static final String RECOMMENDED_CONTENT = "recommendedContent";
 
 /** The Constant DATA_PRESENTATION. */
 public static final String DATA_PRESENTATION = "dataPresentation";
 
 /** The Constant RESPONSE_SUB_HEADLINE. */
 public static final String RESPONSE_SUB_HEADLINE = "responseSubHeadline";
 
 /** The Constant RESPONSE_HEADLINE. */
 public static final String RESPONSE_HEADLINE = "responseHeadline";
 
 /** The Constant RESPONSE_COPY. */
 public static final String RESPONSE_COPY = "responseCopy";
 
 /** The Constant RECOMMENDED_INTRO_TEXT. */
 public static final String RECOMMENDED_INTRO_TEXT = "recommendedIntroText";
 
 /** The Constant RECOMMENDED_CONTENT_LIMIT. */
 public static final String RECOMMENDED_CONTENT_LIMIT = "recommendedContentLimit";
 
 /** The Constant MAX_RECOMMENDED_CONTENT. */
 public static final String MAX_RECOMMENDED_CONTENT = "maxRecommendedContent";
 
 /** The Constant TRUE. */
 public static final String TRUE = "[true]";
 
 /** The Constant YES. */
 public static final String YES = "yes";
 
 /** The Constant OVERRIDE_CONFIGURATIONS. */
 public static final String OVERRIDE_CONFIGURATIONS = "overrideConfigurations";
 
 /** The Constant QUICK_POLL. */
 public static final String QUICK_POLL = "quickPoll";
 
 /** The Constant QUICK_POLL. */
 public static final String RESULT_CHART_TYPE = "resultChartType";
 
 /**
  * Instantiates a new quick poll constants.
  */
 private QuickPollConstants() {
  
 }
}
