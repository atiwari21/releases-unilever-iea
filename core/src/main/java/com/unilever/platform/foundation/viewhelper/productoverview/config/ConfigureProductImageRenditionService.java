/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.productoverview.config;

/**
 * The Interface ConfigureProductImageRenditionService.
 */
public interface ConfigureProductImageRenditionService {
 
 /**
  * Gets the zoom width.
  * 
  * @return the zoom width
  */
 public String getZoomWidth();
 
 /**
  * Gets the thumbnail width.
  * 
  * @return the thumbnail width
  */
 public String getThumbnailWidth();
}
