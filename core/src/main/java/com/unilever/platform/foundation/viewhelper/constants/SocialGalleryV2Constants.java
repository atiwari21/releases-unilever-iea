/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.constants;

/**
 * This class holds all the constants that are used in the form elements.
 * 
 */
public final class SocialGalleryV2Constants {
 
 public static final String OPEN_IN_NEW_WINDOW_TWITTER = "openInNewWindowTwitter";
 
 public static final String SOCIAL_GALLERY_VIEW_PRODUCT_CTA_LABEL = "socialGalleryV2.viewProductCtaLabel";
 
 public static final String VIEW_PRODUCT_CTA_LABEL = "viewProductCtaLabel";
 
 public static final String SOCIAL_GALLERY_VIEW_PRODUCT_HEADING_TEXT = "socialGalleryV2.viewProductHeadingText";
 
 public static final String VIEW_PRODUCT_HEADING_TEXT = "viewProductHeadingText";
 
 public static final String VIEW_ARTICLE_CTA_LABEL = "viewArticleCtaLabel";
 
 public static final String SOCIAL_GALLERY_VIEW_ARTICLE_CTA_LABEL = "socialGalleryV2.viewArticleCtaLabel";
 
 public static final String VIEW_ARTICLE_HEADING_TEXT = "viewArticleHeadingText";
 
 public static final String SOCIAL_GALLERY_VIEW_ARTICLE_HEADING_TEXT = "socialGalleryV2.viewArticleHeadingText";
 
 public static final String SOCIAL_POST_LIKES_SUFFIX = "socialPostLikesSuffix";
 
 public static final String SOCIAL_GALLERY_SOCIAL_POST_LIKES_SUFFIX = "socialGalleryV2.socialPostLikesSuffix";
 
 public static final String SOCIAL_GALLERY_PAGINATION_CTA_LABEL = "socialGalleryV2.paginationCtaLabel";
 
 public static final String TEASER_TITLE = "teaserTitle";
 
 public static final String TEASER_ALT_TEXT = "teaserAltText";
 
 public static final String LINK_POST_TO_INSTAGRAM = "linkPostToInstagram";
 
 /** The Constant LONG_SUBHEADING_TEXT. */
 public static final String LONG_SUBHEADING_TEXT = "longSubheadingText";
 
 /** The Constant SUBHEADING_TEXT_KEY. */
 public static final String SUBHEADING_TEXT_KEY = "subheadingText";
 
 /** The Constant ACCOUNT_ID_KEY. */
 public static final String ACCOUNT_ID_KEY = "accountId";
 
 /** The Constant IMAGE. */
 public static final String IMAGE = "image";
 
 /** The Constant TEASER_IMAGE. */
 public static final String TEASER_IMAGE = "teaserImage";
 
 /** The Constant ASSOCIATED_PAGE. */
 public static final String ASSOCIATED_PAGE = "associatedPage";
 
 /** The Constant POST_LINKED_TO_URL. */
 public static final String POST_LINKED_TO_URL = "postLinkedToUrl";
 
 /** The Constant EXPANDABLE_PANEL_FLAG. */
 public static final String EXPANDABLE_PANEL_FLAG = "expandablePanelFlag";
 
 /** The Constant LIMIT_RESULTS_TO. */
 public static final String LIMIT_RESULTS_TO = "limitResultsTo";
 
 /** The Constant LIMIT. */
 public static final String LIMIT = "limit";
 
 /** The Constant EXPANDABLE_PANEL. */
 public static final String EXPANDABLE_PANEL = "expandablePanel";
 
 /** The Constant STATIC. */
 public static final String STATIC = "static";
 
 /** The Constant PAGINATION_CTA_LABEL. */
 public static final String PAGINATION_CTA_LABEL = "paginationCtaLabel";
 
 /** The Constant ITEMS_PER_PAGE2. */
 public static final String ITEMS_PER_PAGE2 = "itemsPerPage";
 
 /** The Constant PAGINATION_TYPE. */
 public static final String PAGINATION_TYPE = "paginationType";
 
 /** The Constant NAME_KEY. */
 public static final String NAME_KEY = "name";
 
 /** The Constant SOCIAL_CHANNEL_NAMES. */
 public static final String SOCIAL_CHANNEL_NAMES = "socialChannelNames";
 
 /** The Constant ID_KEY. */
 public static final String ID_KEY = "Id";
 
 /** The Constant HASH_TAG_INSTAGRAM. */
 public static final String HASH_TAG_INSTAGRAM = "hashTagInstagram";
 
 /** The Constant HASH_TAG_TWITTER. */
 public static final String HASH_TAG_TWITTER = "hashTagTwitter";
 
 /** The Constant HASH_TAGS_KEY. */
 public static final String HASH_TAGS_KEY = "hashTags";
 
 /** The Constant HASH_TAG_FACEBOOK. */
 public static final String HASH_TAG_FACEBOOK = "hashTagFacebook";
 
 /** The Constant AUTOMATIC. */
 public static final String AUTOMATIC = "automatic";
 
 /** The Constant REQUEST_SERVLET_URL. */
 public static final String REQUEST_SERVLET_URL = "requestServletUrl";
 
 /** The Constant BRAND_NAME_PARAMETER. */
 public static final String BRAND_NAME_PARAMETER = "brandNameParameter";
 
 /** The Constant NUMBER_OF_POSTS_KEY. */
 public static final String NUMBER_OF_POSTS_KEY = "numberOfPosts";
 
 /** The Constant OPEN_IN_NEW_WINDOW_INSTAGRAM. */
 public static final String OPEN_IN_NEW_WINDOW_INSTAGRAM = "openInNewWindowInstagram";
 
 /** The Constant CTA_LABEL_CUSTOM_INSTAGRAM. */
 public static final String CTA_LABEL_CUSTOM_INSTAGRAM = "ctaLabelCustomInstagram";
 
 /** The Constant HEADING_TEXT_CUSTOM_INSTAGRAM. */
 public static final String HEADING_TEXT_CUSTOM_INSTAGRAM = "headingTextCustomInstagram";
 
 /** The Constant LINK_TYPE_INSTAGRAM. */
 public static final String LINK_TYPE_INSTAGRAM = "linkTypeInstagram";
 
 /** The Constant POST_ID_INSTAGRAM. */
 public static final String POST_ID_INSTAGRAM = "postIdInstagram";
 
 /** The Constant LINK_TYPE_FACEBOOK. */
 public static final String LINK_TYPE_FACEBOOK = "linkTypeFacebook";
 
 /** The Constant LINK_TYPE_KEY. */
 public static final String LINK_TYPE_KEY = "linkType";
 
 /** The Constant GLOBAL_CONFIG. */
 public static final String GLOBAL_CONFIG = "socialGalleryV2";
 
 /** The Constant ALL. */
 public static final String ALL = "all";
 
 /** The Constant PAGE_NUMBER. */
 public static final String PAGE_NUMBER = "pageNumber";
 
 /** The Constant ZERO. */
 public static final int ZERO = 0;
 
 /** The Constant CTA_LABEL. */
 public static final String CTA_LABEL = "ctaLabel";
 
 /** The Constant CTA_URL. */
 public static final String CTA_URL = "ctaUrl";
 
 /** The Constant CTA_LABEL_KEY. */
 public static final String CTA_LABEL_KEY = "ctaLabel";
 
 /** The Constant ITEMS_PER_PAGE. */
 public static final String ITEMS_PER_PAGE = ITEMS_PER_PAGE2;
 
 /** The Constant FIXED_LIST. */
 public static final String FIXED_LIST = "manual";
 
 /** The Constant AUTOMATED_LIST. */
 public static final String AUTOMATED_LIST = AUTOMATIC;
 
 /** The Constant SOCIAL_CHANNELS_FIXED. */
 public static final String SOCIAL_CHANNELS_FIXED = "socialChannelsFixed";
 
 /** The Constant POST_ID_FACEBOOK. */
 public static final String POST_ID_FACEBOOK = "postIdFacebook";
 
 /** The Constant POST_ID_FIXED. */
 public static final String POST_ID_FIXED = "postIdFixed";
 
 /** The Constant LINK_POST_TO_FIXED. */
 public static final String LINK_POST_TO_FIXED = "linkPostToFixed";
 
 /** The Constant LINK_TYPE_FIXED. */
 public static final String LINK_TYPE_FIXED = "linkTypeFixed";
 
 /** The Constant HEADING_TEXT_CUSTOM_FIXED. */
 public static final String HEADING_TEXT_CUSTOM_FIXED = "headingTextCustomFixed";
 
 /** The Constant CTA_LABEL_CUSTOM_FIXED. */
 public static final String CTA_LABEL_CUSTOM_FIXED = "ctaLabelCustomFixed";
 
 /** The Constant OPEN_IN_NEW_WINDOW_FIXED. */
 public static final String OPEN_IN_NEW_WINDOW_FIXED = "openInNewWindowFixed";
 
 /** The Constant OPEN_IN_NEW_WINDOW. */
 public static final String OPEN_IN_NEW_WINDOW = "openInNewWindow";
 
 /** The Constant OPEN_IN_NEW_WINDOW_KEY. */
 public static final String OPEN_IN_NEW_WINDOW_KEY = "openInNewWindow";
 
 /** The Constant FACEBOOK. */
 public static final String FACEBOOK = "facebook";
 
 /** The Constant TWITTER. */
 public static final String TWITTER = "twitter";
 
 /** The Constant INSTAGRAM. */
 public static final String INSTAGRAM = "instagram";
 
 /** The Constant SOCIAL_POST_TO_DISPLAY_JSON. */
 public static final String SOCIAL_POST_TO_DISPLAY_JSON = "socialPostsToDisplayJson";
 
 /** The Constant LINK_POST_TO_FACEBOOK. */
 public static final String LINK_POST_TO_FACEBOOK = "linkPostToFacebook";
 
 /** The Constant LINK_TYPE. */
 public static final String LINK_TYPE = LINK_TYPE_KEY;
 
 /** The Constant HEADING_TEXT_CUSTOM_FACEBOOK. */
 public static final String HEADING_TEXT_CUSTOM_FACEBOOK = "headingTextCustomFacebook";
 
 /** The Constant CTA_LABEL_CUSTOM_FACEBOOK. */
 public static final String CTA_LABEL_CUSTOM_FACEBOOK = "ctaLabelCustomFacebook";
 
 /** The Constant OPEN_IN_NEW_WINDOW_FACEBOOK. */
 public static final String OPEN_IN_NEW_WINDOW_FACEBOOK = "openInNewWindowFacebook";
 
 /** The Constant PAGE_LISTING_PAGINATION_NEXT. */
 public static final String PAGE_LISTING_PAGINATION_NEXT = "pageListing.pagination.next";
 
 /** The Constant NEXT_LABEL. */
 public static final String NEXT_LABEL = "nextLabel";
 
 /** The Constant PAGE_LISTING_PAGINATION_PREVIOUS. */
 public static final String PAGE_LISTING_PAGINATION_PREVIOUS = "pageListing.pagination.previous";
 
 /** The Constant PREVIOUS_LABEL. */
 public static final String PREVIOUS_LABEL = "previousLabel";
 
 /** The Constant PAGE_LISTING_PAGINATION_LAST. */
 public static final String PAGE_LISTING_PAGINATION_LAST = "pageListing.pagination.last";
 
 /** The Constant LAST_LABEL. */
 public static final String LAST_LABEL = "lastLabel";
 
 /** The Constant PAGE_LISTING_PAGINATION_FIRST. */
 public static final String PAGE_LISTING_PAGINATION_FIRST = "pageListing.pagination.first";
 
 /** The Constant FIRST_LABEL. */
 public static final String FIRST_LABEL = "firstLabel";
 
 /** The Constant HEADING_TEXT. */
 public static final String HEADING_TEXT = "headingText";
 
 /** The Constant SUB_HEADING_TEXT. */
 public static final String SUB_HEADING_TEXT = SUBHEADING_TEXT_KEY;
 
 /** The Constant TEXT. */
 public static final String TEXT = "text";
 
 /** The Constant HEADING_TEXT_KEY. */
 public static final String HEADING_TEXT_KEY = "headingText";
 
 /** The Constant SOCIAL_CHANNELS. */
 public static final String SOCIAL_CHANNELS = "socialChannels";
 
 /** The Constant SOCIAL_CONFIGURATION_KEY. */
 public static final String SOCIAL_CONFIGURATION_KEY = "socialConfiguration";
 
 /** The Constant POST_ID_KEY. */
 public static final String POST_ID_KEY = "postId";
 
 /** The Constant AGGREGATION_MODE_KEY. */
 public static final String AGGREGATION_MODE_KEY = "aggregationMode";
 
 /** The Constant AGGREGATION_MODE. */
 public static final String AGGREGATION_MODE = "aggregationMode";
 
 /** The Constant CUSTOM. */
 public static final String CUSTOM = "custom";
 
 /** The Constant TRUE. */
 public static final String TRUE = "[true]";
 
 /**
  * public Constructor.
  */
 private SocialGalleryV2Constants() {
  
 }
 
}
