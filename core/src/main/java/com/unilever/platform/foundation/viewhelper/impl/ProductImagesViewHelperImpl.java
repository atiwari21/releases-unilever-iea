/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.components.helper.ProductHelperExt;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * This class prepares the data map for Product Images.
 */
@Component(description = "ProductImagesViewHelperImpl", immediate = true, metatype = true, label = "Product Images ViewHelper")
@Service(value = { ProductImagesViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.PRODUCT_IMAGES, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class ProductImagesViewHelperImpl extends BaseViewHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ProductImagesViewHelperImpl.class);
 private static final String PRODUCT_PAGE = "productPage";
 
 @Reference
 ConfigurationService configurationService;
 
 @SuppressWarnings("unchecked")
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("Product Images View Helper #processData called .");
  ResourceResolver resourceResolver = (ResourceResolver) resources.get(ProductConstants.RESOURCE_RESOLVER);
  Map<String, Object> productImagesProperties = (Map<String, Object>) content.get(PROPERTIES);
  SlingHttpServletRequest slingRequest = getSlingRequest(resources);
  I18n i18n = getI18n(resources);
  Page currentPage = getCurrentPage(resources);
  Map<String, Object> zoomProperties = new HashMap<String, Object>();
  Map<String, Object> properties = new LinkedHashMap<String, Object>();
  Map<String, Object> productProperties = new HashMap<String, Object>();
  UNIProduct product = null;
  String jsonNameSapce = getJsonNameSpace(content);
  String productPagePath = MapUtils.getString(productImagesProperties, PRODUCT_PAGE);
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  if (StringUtils.isNotBlank(productPagePath)) {
   product = ProductHelper.getUniProduct(pageManager.getPage(productPagePath));
  } else {
   product = ProductHelper.getUniProduct(currentPage, slingRequest);
  }
  if (product != null) {
   productProperties = ProductHelperExt.getProductTeaserData(product, currentPage, slingRequest, false);
   productProperties.put(ProductConstants.PRICE_ENABLED, ProductHelper.isPriceEnabled(configurationService, currentPage));
  } else {
   productProperties.put(ProductConstants.IMAGES, null);
   LOGGER.info("No Product is available for current page");
  }
  
  if (hasProductImagesConfiguration(configurationService, currentPage)) {
   properties.put(ProductConstants.BACKGROUND_IMAGE, GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
     ProductConstants.PRODUCT_IMAGES, ProductConstants.BACKGROUND_IMAGE));
   properties.put(ProductConstants.ZOOM_MAP, getZoomMapProperties(zoomProperties, configurationService, i18n, currentPage));
  } else {
   properties.put(ProductConstants.BACKGROUND_IMAGE, GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
     ProductConstants.PRODUCTOVERVIEW, ProductConstants.BACKGROUND_IMAGE));
   properties.put(ProductConstants.ZOOM_MAP, ProductHelper.getZoomMapProperties(zoomProperties, configurationService, i18n, currentPage));
  }
  
  properties.put(ProductConstants.PRODUCT_MAP, productProperties);
  
  LOGGER.info("The Images list corresponding to Product images is successfully generated");
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(jsonNameSapce, properties);
  return data;
 }
 
 private Map<String, Object> getZoomMapProperties(Map<String, Object> zoomMapProperties, ConfigurationService configurationService, I18n i18n,
   Page page) {
  zoomMapProperties.put(ProductConstants.LABEL, i18n.get(ProductConstants.ZOOM_LABEL));
  
  try {
   zoomMapProperties.put(ProductConstants.ENABLED,
     configurationService.getConfigValue(page, ProductConstants.PRODUCT_IMAGES, ProductConstants.PRODUCT_IMAGES_ZOOM_ENABLED));
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find isProductOverviewZoomEnabled key in Zoom category", e);
   zoomMapProperties.put(ProductConstants.ENABLED, ProductConstants.FALSE);
  }
  LOGGER.debug("Zoom Map Properties : " + zoomMapProperties);
  return zoomMapProperties;
 }
 
 private boolean hasProductImagesConfiguration(ConfigurationService configurationService, Page page) {
  boolean exists = false;
  try {
   configurationService.getCategoryConfiguration(page, ProductConstants.PRODUCT_IMAGES);
   exists = true;
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("product images configuration does not exist",e);
  }
  return exists;
 }
}
