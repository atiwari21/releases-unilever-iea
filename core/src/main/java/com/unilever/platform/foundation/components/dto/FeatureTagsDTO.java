/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.foundation.components.dto;

import java.lang.reflect.Constructor;

public class FeatureTagsDTO {
 
 private String title;
 private String tagID;
 private String url;
 
 /**
  * {@link Constructor}
  * @param tagTitle
  * @param tagID
  * @param url
  */
 public FeatureTagsDTO(String tagTitle, String tagID, String url) {
  super();
  this.title = tagTitle;
  this.tagID = tagID;
  this.url = url;
 }
 
 
 /**
  * @return the tagTitle
  */
 public String getTitle() {
  return title;
 }
 /**
  * @param tagTitle the tagTitle to set
  */
 public void setTitle(String tagTitle) {
  this.title = tagTitle;
 }
 /**
  * @return the tagID
  */
 public String getTagID() {
  return tagID;
 }
 /**
  * @param tagID the tagID to set
  */
 public void setTagID(String tagID) {
  this.tagID = tagID;
 }
 /**
  * @return the url
  */
 public String getUrl() {
  return url;
 }
 /**
  * @param url the url to set
  */
 public void setUrl(String url) {
  this.url = url;
 }
 
 
 
 
}
