/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * The Class SocialSharePrintViewHelperImpl.
 */
@Component(description = "SocialSharePrintViewHelperImpl", immediate = true, metatype = true, label = "Social Share Print")
@Service(value = { SocialSharePrintViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.SOCIAL_SHARE_PRINT, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class SocialSharePrintViewHelperImpl extends BaseViewHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(SocialSharePrintViewHelperImpl.class);
 
 /** The Constant PRINT_CTA_ICON. */
 private static final String PRINT_CTA_ICON = "printctaicon";
 
 /** The Constant PRINT_TITLE. */
 private static final String PRINT_TITLE = "printTitle";
 
 /** The Constant SHARE_CTA_ICAON. */
 private static final String SHARE_CTA_ICAON = "sharectaicon";
 
 /** The Constant SHARE_TITLE. */
 private static final String SHARE_TITLE = "shareTitle";
 
 /** The Constant ADD_THIS_OPTIONS. */
 private static final String ADD_THIS_OPTIONS = "addThisOptions";
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("Executing SocialSharePrintViewHelperImpl. Inside processData method ");
  Map<String, Object> properties = getProperties(content);
  Map<String, Object> socialSharePrintProperties = new HashMap<String, Object>();
  
  String printctaicon = MapUtils.getString(properties, PRINT_CTA_ICON, StringUtils.EMPTY);
  String printTitle = MapUtils.getString(properties, PRINT_TITLE, StringUtils.EMPTY);
  String sharectaicon = MapUtils.getString(properties, SHARE_CTA_ICAON, StringUtils.EMPTY);
  String shareTitle = MapUtils.getString(properties, SHARE_TITLE, StringUtils.EMPTY);
  
  List<String> addThisOptionsList = getAllAddThisOptions(properties);
  socialSharePrintProperties.put("addThisOptions", addThisOptionsList.toArray());
  
  socialSharePrintProperties.put("printctaicon", printctaicon);
  socialSharePrintProperties.put("printTitle", printTitle);
  socialSharePrintProperties.put("sharectaicon", sharectaicon);
  socialSharePrintProperties.put("shareTitle", shareTitle);
  
  LOGGER.debug("Social Share Print properties Map: " + socialSharePrintProperties.size());
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(getJsonNameSpace(content), socialSharePrintProperties);
  return data;
  
 }
 
 /**
  * Gets the all add this options.
  * 
  * @param properties
  *         the properties
  * @return the all add this options
  */
 private List<String> getAllAddThisOptions(Map<String, Object> properties) {
  List<String> list = new ArrayList<String>();
  Object obj = MapUtils.getObject(properties, ADD_THIS_OPTIONS);
  if (obj instanceof String[]) {
   for (String channel : (String[]) obj) {
    list.add(channel);
   }
  } else if (obj instanceof String) {
   list.add((String) obj);
   
  }
  return list;
 }
 
}
