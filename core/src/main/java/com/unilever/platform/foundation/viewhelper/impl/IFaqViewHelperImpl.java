/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.foundation.viewhelper.impl;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.core.service.FaqConfigService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * <p>
 * The goal of this component is to implement IFAQ.
 * 
 * </p>
 */

@Component(description = "IFaqViewHelperImpl Viewhelper Impl", immediate = true, metatype = true, label = "IFaqViewHelperImpl")
@Service(value = { IFaqViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.IFAQS, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class IFaqViewHelperImpl extends BaseViewHelper {
 
 private static final String FAQ_DIFFERENT_QUES_CTA = "contactUsCTA";
 
 private static final String FAQ_DIFFERENT_QUES_CTA_LABEL = "faq.differentQuesCTA";
 
 private static final String REQ_PARAM = "faqsiteid";
 
 /** the constant SERVICE_URL. */
 private static final String SERVICE_URL = "serviceUrl";
 
 /** the constant FAQ_SITE_ID. */
 private static final String FAQ_SITE_ID = "faqSiteID";
 
 /** the constant FAQ_CONFIG. */
 private static final String FAQ_CONFIG = "faqConfig";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(IFaqViewHelperImpl.class);
 
 @Reference
 FaqConfigService faqConfigService;
 
 /**
  * creates the result map containing aggregated list of questions and answers returned by the service. {@inheritDoc}
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.info("IFAQ View Helper #processData() method called..");
  Map<String, Object> faqCollection = new LinkedHashMap<String, Object>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  I18n i18n = getI18n(resources);
  Map<String, Object> faq = new LinkedHashMap<String, Object>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  faqCollection.put(FAQ_DIFFERENT_QUES_CTA, i18n.get(FAQ_DIFFERENT_QUES_CTA_LABEL));
  faqCollection.put(SERVICE_URL, getFullFaqUrl(resources));
  faq.put(getJsonNameSpace(content), faqCollection);
  return faq;
  
 }
 
 /**
  * Reads comma separated list of faqsiteIDs from the siteConfig.
  * 
  * @param resources
  * @return url with the request paramaters.
  */
 private String getFullFaqUrl(Map<String, Object> resources) {
  Page currentPage = getCurrentPage(resources);
  ConfigurationService configurationService = currentPage.adaptTo(ConfigurationService.class);
  String url = StringUtils.EMPTY;
  try {
   url = faqConfigService.getWebServiceUrl();
   String faqSiteID = configurationService.getConfigValue(currentPage, FAQ_CONFIG, FAQ_SITE_ID);
   LOGGER.info("comma separated faq site IDs are:", faqSiteID);
   if (StringUtils.isNotEmpty(url) && StringUtils.isNotEmpty(faqSiteID)) {
    url = url + "?" + REQ_PARAM + "=" + faqSiteID;
   }
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find faqSiteID key in configuration faqConfig", e);
  }
  return url;
  
 }
 
}
