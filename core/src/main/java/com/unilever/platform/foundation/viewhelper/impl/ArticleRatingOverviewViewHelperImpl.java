/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.utils.AdaptiveUtil;
import com.unilever.platform.foundation.components.BazaarVoiceSEO;
import com.unilever.platform.foundation.components.KritiqueSEO;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ArticleHelper;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.kritique.config.KritiqueReviewsCommentsService;
import com.unilever.platform.foundation.template.dto.Article;
import com.unilever.platform.foundation.template.dto.KritiqueDTO;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ArticleConstants;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * The Class ArticleRatingOverviewViewHelperImpl.
 */
@Component(description = "Article Rating Overview Component", immediate = true, metatype = true, label = "ArticleRatingOverviewViewHelperImpl")
@Service({ ArticleRatingOverviewViewHelperImpl.class, ViewHelper.class })
@Properties({
  @org.apache.felix.scr.annotations.Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = { UnileverComponents.ARTICLE_RATING_OVERVIEW }, propertyPrivate = true),
  @org.apache.felix.scr.annotations.Property(name = Constants.SERVICE_RANKING, intValue = { 5000 }, propertyPrivate = true) })
public class ArticleRatingOverviewViewHelperImpl extends BaseViewHelper {
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The kritique service. */
 @Reference
 KritiqueReviewsCommentsService kritiqueReviewsCommentsService;
 
 @Reference
 KritiqueSEO kqseoService;
 
 @Reference
 BazaarVoiceSEO bvseoService;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ArticleRatingOverviewViewHelperImpl.class);
 private static final String RATING_REVIEWS = "ratingReviews";
 private static final String ADAPTIVE_REVIEW_LABEL = "readReviews";
 private static final String ADAPTIVE_REVIEW_I18N = "productOverview.adaptiveReadReview";
 private static final String SEO = "SEO";
 private static final String COMPONENT_NAME = "componentName";
 
 @Override
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData (java.util.Map, java.util.Map)
  */
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("Article Rating Overview ViewHelper #processData called.");
  ResourceResolver resourceResolver = (ResourceResolver) resources.get(ProductConstants.RESOURCE_RESOLVER);
  Map<String, Object> articleRatingOverviewProperties = getProperties(content);
  SlingHttpServletRequest slingRequest = getSlingRequest(resources);
  Resource currentResource = slingRequest.getResource();
  Article article = new Article();
  String componentName = StringUtils.EMPTY;
  ValueMap componentProperties = currentResource.getValueMap();
  if (componentProperties != null) {
   componentName = componentProperties.get(COMPONENT_NAME, String.class);
  }
  I18n i18n = getI18n(resources);
  Page currentPage = getCurrentPage(resources);
  Map<String, Object> properties = new LinkedHashMap<String, Object>();
  Map<String, Object> articleProperties = new HashMap<String, Object>();
  
  String jsonNameSapce = getJsonNameSpace(content);
  String articlePagePath = MapUtils.getString(articleRatingOverviewProperties, ArticleConstants.ARTICLE_PAGE);
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  Page articlePage = pageManager.getPage(articlePagePath);
  String contentType = (String) currentPage.getProperties().getOrDefault(UnileverConstants.CONTENT_TYPE, StringUtils.EMPTY);
  Page finalArticlePage = articlePage != null ? articlePage : ArticleHelper.getAlternateArticlePage(contentType, currentPage, slingRequest,
    configurationService, resourceResolver);
  
  if (finalArticlePage != null) {
   article.setData(finalArticlePage, getSlingRequest(resources));
  }
  
  String serviceProvidername = StringUtils.EMPTY;
  String serviceProviderNameTemp = StringUtils.EMPTY;
  try {
   Resource resource = currentPage.getContentResource();
   InheritanceValueMap valueMap = new HierarchyNodeInheritanceValueMap(resource);
   if (valueMap != null) {
    serviceProviderNameTemp = (String) valueMap.getInherited(RATING_REVIEWS, StringUtils.EMPTY);
   }
   if (StringUtils.isNotBlank(serviceProviderNameTemp)) {
    serviceProvidername = serviceProviderNameTemp;
   } else {
    serviceProvidername = configurationService.getConfigValue(currentPage, ArticleConstants.ARTICLE_RATING_AND_REVIEWS,
      ProductConstants.SERVICE_PROVIDER_NAME);
   }
   Boolean isEnabled = Boolean.parseBoolean(configurationService.getConfigValue(currentPage, ArticleConstants.ARTICLE_RATING_AND_REVIEWS,
     ArticleConstants.ENABLED));
   if (isEnabled) {
    Boolean enabled = Boolean.parseBoolean(configurationService.getConfigValue(currentPage, ArticleConstants.ARTICLE_RATING_AND_REVIEWS,
      ArticleConstants.SEO_ENABLED));
    if (enabled) {
     if ("bazaarvoice".equalsIgnoreCase(serviceProvidername) || serviceProvidername.startsWith("bazaarvoice")) {
      articleProperties.put(SEO, bvseoService.getSEOWithSDK(currentPage, configurationService, resources));
     } else if ("kritique".equalsIgnoreCase(serviceProvidername) || serviceProvidername.startsWith("kritique")) {
      articleProperties.put(SEO, kqseoService.getSEOWithSDK(slingRequest, currentPage, configurationService));
     }
     
    }
    if (finalArticlePage != null) {
     articleProperties.put(ArticleConstants.ARTICLE_RATINGS_MAP,
       ArticleHelper.getArticleRatingsMap(article, currentPage, configurationService, componentName, finalArticlePage));
    } else {
     articleProperties.put(ArticleConstants.ARTICLE_RATINGS_MAP, new HashMap<String, Object>());
    }
    /* if it is feature phone, override ratings with feature phone ratings */
    if (AdaptiveUtil.isAdaptive(slingRequest)) {
     properties.put(ADAPTIVE_REVIEW_LABEL, i18n.get(ADAPTIVE_REVIEW_I18N));
     LOGGER.info("Feature phone view called and article page path is " + articlePagePath);
     if (finalArticlePage != null) {
      KritiqueDTO kritiqueparams = new KritiqueDTO(currentPage, configurationService);
      String entitiySourceId = (String) finalArticlePage.getProperties().getOrDefault(UnileverConstants.UNIQUE_PAGE_ID, StringUtils.EMPTY);
      kritiqueparams.setEntitySourceId(entitiySourceId);
      Map<String, Object> reviewMap = getKQMap(kritiqueparams);
      articleProperties.put(ArticleConstants.ARTICLE_RATINGS_MAP, reviewMap);
      LOGGER.debug("Article Rating map for articleRatingOverview of feature phone created succesfully");
     }
    }
   }
   
  } catch (Exception e) {
   LOGGER.error("could not find KQSEOenabled key in productOverview category", e);
  }
  articleProperties.put(ProductConstants.REVIEW_MAP, ArticleHelper.getReviewMapProperties(configurationService, currentPage));
  properties.put(ArticleConstants.ARTICLE_MAP, articleProperties);
  LOGGER.info("The Product list corresponding to Product Overview is successfully generated");
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(jsonNameSapce, properties);
  return data;
 }
 
 /**
  * Gets the getKQMap map.
  * 
  * @param kritiqueparams
  *         the kritique parameter
  * @return the reviewMap
  */
 private Map<String, Object> getKQMap(KritiqueDTO kritiqueparams) {
  Map<String, Object> reviewMap = null;
  try {
   List<Map<String, Object>> kqMap = kritiqueReviewsCommentsService.getArticleRatingsService(kritiqueparams, configurationService);
   if (!kqMap.isEmpty()) {
    reviewMap = kqMap.get(0);
   }
  } catch (Exception e) {
   LOGGER.error("Exception while getting kritique response ", e);
  }
  return reviewMap;
 }
 
}
