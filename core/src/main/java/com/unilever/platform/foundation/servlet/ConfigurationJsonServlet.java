/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.servlet;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.io.JSONWriter;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.aem.foundation.constants.CommonConstants;
import com.unilever.platform.foundation.constants.UnileverConstants;

/**
 * The Class SocialChannelServlet.
 */
@SlingServlet(label = "Unilever uasge contrext Servlet to Fetch analytics context list", resourceTypes = { CommonConstants.DEFAULT_SERVLET_NAME }, methods = { HttpConstants.METHOD_GET }, selectors = { "configurejson" }, extensions = { "json" })
@Properties({
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.foundation.components.AnalyticsUsageContextServlet", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "Unilever uasge contrext Servlet to Fetch analytics context list", propertyPrivate = false),

})
public class ConfigurationJsonServlet extends SlingAllMethodsServlet {
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationJsonServlet.class);
 
 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = 1L;
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache. sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected final void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws IOException {
  
  String path = request.getRequestPathInfo().getResourcePath();
  ResourceResolver resourceResolver = request.getResourceResolver();
  
  Writer out = response.getWriter();
  JSONWriter writer = new JSONWriter(out);
  
  response.setContentType("application/json");
  response.setCharacterEncoding("UTF-8");
  
  Resource currentResource = resourceResolver.getResource(path);
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  Page page = pageManager.getContainingPage(currentResource);
  String[] pageSelectors = request.getRequestPathInfo().getSelectors();
  String globalConfigCategory = pageSelectors.length > 1 ? pageSelectors[1] : StringUtils.EMPTY;
  if (page != null) {
   Map<String, String> usageContextMap = GlobalConfigurationUtility.getMapFromConfiguration(configurationService, page, globalConfigCategory);
   
   try {
    writer.array();
    addAllUsageTypes(usageContextMap, writer);
    writer.endArray();
   } catch (JSONException e) {
    LOGGER.error("RepositoryException occurred ", e);
   }
  }
 }
 
 /**
  * Adds the writer obj.
  * 
  * @param writer
  *         the writer
  * @param value
  *         the value
  * @throws JSONException
  *          the JSON exception
  */
 private void addWriterObj(JSONWriter writer, String value) throws JSONException {
  writer.object();
  writer.key("text").value(value.trim());
  writer.key("value").value(value.trim());
  writer.endObject();
 }
 
 /**
  * Adds the all channels.
  * 
  * @param channelsMap
  *         the channels map
  * @param writer
  *         the writer
  * @throws JSONException
  *          the JSON exception
  */
 private void addAllUsageTypes(Map<String, String> usageContextMap, JSONWriter writer) throws JSONException {
  if (MapUtils.isNotEmpty(usageContextMap)) {
   addWriterObj(writer, UnileverConstants.PLEASE_SELECT);
   for (String key : usageContextMap.keySet()) {
    addWriterObj(writer, usageContextMap.get(key));
   }
  }
 }
 
}
