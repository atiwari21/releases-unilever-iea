/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.components.helper;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;
import com.unilever.platform.foundation.viewhelper.impl.SocialSharingViewHelperImpl;

/**
 * The Class SocialShareHelper.
 * 
 * @author rhariv A common social share class.
 */
public final class SocialShareHelper {
 
 private static final int ZERO = 0;
 private static final String FACEBOOK = "facebook";
 /** name */
 private static final String NAME = "name";
 /** social media key used in map */
 private static final String SOCIAL_MEDIA = "socialMedia";
 /** headline key used in map */
 private static final String HEADLINE = "headline";
 /** enabled key used in map */
 private static final String ENABLED = "enabled";
 /** isEnabled key used in map */
 private static final String IS_ENABLED = "isEnabled";
 /** i18n key */
 private static final String ADDTHIS_SOCIAL_SHARE_TITLE_I18N = "addthis.social.share.title";
 /** The Constant SOCIAL_SHARING_CATEGORY. */
 private static final String SOCIAL_SHARING_CATEGORY = "addThisWidget";
 private static final int INTEGER_TWO = 2;
 private static final int INTEGER_ONE = 1;
 /** The Constant SOCIAL_SHARING_CATEGORY CHINA. */
 private static final String SOCIAL_SHARING_CATEGORY_CHINA = "jiaThisWidget";
 /** i18n key china */
 private static final String ADDTHIS_SOCIAL_SHARE_TITLE_I18N_CHINA = "socialSharingChina.headline";
 /** china locale flag constant */
 private static final String CHINA_SOCIAL_MEDIA_FLAG = "chineseLocaleFlag";
 /** global config jiaThisLocale constant */
 private static final String JIA_THIS_LOCALE = "jiaThisLocale";
 /** global config jiaThisLocale key localeconstant */
 private static final String JIA_THIS_LOCALE_KEY = "locale";
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(SocialSharingViewHelperImpl.class);
 /** The Constant PDF_SERVICE_URL */
 private static final String PDF_SERVICE_URL = "pdfServiceUrl";
 
 private static final String SERVICE_CODE = "serviceCode";
 
 /** The Constructor */
 private SocialShareHelper() {
  
 }
 
 /**
  * Adds the this widget.
  * 
  * @param resources
  *         the resources
  * @param configurationService
  *         the configuration service
  * @return the map
  */
 public static Map<String, Object> addThisWidget(Map<String, Object> resources, ConfigurationService configurationService) {
  return addThisWidget(resources, configurationService, null);
 }
 
 /**
  * Adds the this widget.
  * 
  * @param resources
  *         the resources
  * @param configurationService
  *         the configuration service
  * @param override
  * @param properties
  * @return the map
  */
 public static Map<String, Object> addThisWidget(Map<String, Object> resources, ConfigurationService configurationService, boolean override,
   Map<String, Object> properties) {
  return addThisWidget(resources, configurationService, null, override, properties);
 }
 
 /**
  * Called from SocialSharingViewHelperImpl class. Reads the social media details from the config files . Passes it in a map
  * 
  * @param content
  *         the content
  * @param resources
  *         the resources
  * @param configurationService
  *         the configuration service
  * @param addThisWidgetOptions
  * @return properties map
  */
 public static Map<String, Object> addThisWidget(Map<String, Object> resources, ConfigurationService configurationService,
   Map<String, String> addThisWidgetOptions) {
  LOGGER.debug("Executing SocialShareHelper.java");
  Map<String, String> localAddThisWidgetOptions = addThisWidgetOptions;
  Map<String, Object> properties = new HashMap<String, Object>();
  if ((resources.get(ProductConstants.REQUEST)) != null) {
   ResourceResolver resourceResolver = (ResourceResolver) resources.get(ProductConstants.RESOURCE_RESOLVER);
   SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) resources.get(ProductConstants.REQUEST);
   Resource currentResource = slingRequest.getResource();
   PageManager pm = resourceResolver.adaptTo(PageManager.class);
   Page currentPage = pm.getContainingPage(currentResource);
   
   List<Object> socialShareList = new ArrayList<Object>();
   
   String title = StringUtils.EMPTY;
   String i18nSocialShare = StringUtils.EMPTY;
   String socialShareCategory = StringUtils.EMPTY;
   I18n i18n = BaseViewHelper.getI18n(resources);
   boolean chineseLocaleFlag = GlobalConfigurationUtility.isJiaThisLocaleEnabled(currentPage, configurationService, JIA_THIS_LOCALE,
     JIA_THIS_LOCALE_KEY);
   i18nSocialShare = chineseLocaleFlag ? ADDTHIS_SOCIAL_SHARE_TITLE_I18N_CHINA : ADDTHIS_SOCIAL_SHARE_TITLE_I18N;
   socialShareCategory = chineseLocaleFlag ? SOCIAL_SHARING_CATEGORY_CHINA : SOCIAL_SHARING_CATEGORY;
   // getting social share options from config
   try {
    if (MapUtils.isEmpty(localAddThisWidgetOptions)) {
     localAddThisWidgetOptions = configurationService.getCategoryConfiguration(currentPage, socialShareCategory);
     LOGGER.debug("Configured Options are :" + localAddThisWidgetOptions);
    }
   } catch (ConfigurationNotFoundException e) {
    LOGGER.error("Configuration for social links Not found:", e);
   }
   // preparing social share list based on social share options
   getSocialShareList(localAddThisWidgetOptions, socialShareList, currentPage, resourceResolver, slingRequest, ArrayUtils.EMPTY_STRING_ARRAY);
   title = i18n.get(i18nSocialShare);
   properties.put(HEADLINE, title);
   properties.put(IS_ENABLED, MapUtils.getBoolean(localAddThisWidgetOptions, ENABLED, Boolean.TRUE));
   properties.put(SOCIAL_MEDIA, socialShareList);
   properties.put(CHINA_SOCIAL_MEDIA_FLAG, chineseLocaleFlag);
   properties.put(PDF_SERVICE_URL, MapUtils.getString(localAddThisWidgetOptions, PDF_SERVICE_URL, StringUtils.EMPTY));
   LOGGER.debug("No of social channels fetched: {}", socialShareList.size());
   
  } else {
   LOGGER.info("Component cannot be found for current page");
  }
  
  return properties;
  
 }
 
 /**
  * Called from SocialSharingViewHelperImpl class. Reads the social media details from the config files . Passes it in a map
  * 
  * @param pageProperties
  *         the pageProperties
  * @param resources
  *         the resources
  * @param configurationService
  *         the configuration service
  * @param addThisWidgetOptions
  * @param overridden
  * @return properties map
  */
 public static Map<String, Object> addThisWidget(Map<String, Object> resources, ConfigurationService configurationService,
   Map<String, String> addThisWidgetOptions, boolean overridden, Map<String, Object> pageProperties) {
  LOGGER.debug("Executing SocialShareHelper.java");
  Map<String, String> localAddThisWidgetOptions = addThisWidgetOptions;
  Map<String, Object> properties = new HashMap<String, Object>();
  if ((resources.get(ProductConstants.REQUEST)) != null) {
   ResourceResolver resourceResolver = (ResourceResolver) resources.get(ProductConstants.RESOURCE_RESOLVER);
   SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) resources.get(ProductConstants.REQUEST);
   Resource currentResource = slingRequest.getResource();
   PageManager pm = resourceResolver.adaptTo(PageManager.class);
   Page currentPage = pm.getContainingPage(currentResource);
   
   List<Object> socialShareList = new ArrayList<Object>();
   
   String title = StringUtils.EMPTY;
   String i18nSocialShare = StringUtils.EMPTY;
   String socialShareCategory = StringUtils.EMPTY;
   I18n i18n = BaseViewHelper.getI18n(resources);
   String provider = MapUtils.getString(pageProperties, "provider", StringUtils.EMPTY);
   String[] serviceCodes = ArrayUtils.EMPTY_STRING_ARRAY;
   if (MapUtils.getObject(pageProperties, SERVICE_CODE) instanceof String[]) {
    serviceCodes = (String[]) MapUtils.getObject(pageProperties, SERVICE_CODE);
   } else if (MapUtils.getObject(pageProperties, SERVICE_CODE) instanceof String) {
    String serviceCode = MapUtils.getString(pageProperties, SERVICE_CODE);
    serviceCodes = new String[] { serviceCode };
   }
   boolean chineseLocaleFlag = false;
   if (StringUtils.equalsIgnoreCase(provider, "jiaThis")) {
    chineseLocaleFlag = GlobalConfigurationUtility.isJiaThisLocaleEnabled(currentPage, configurationService, JIA_THIS_LOCALE, JIA_THIS_LOCALE_KEY);
    i18nSocialShare = chineseLocaleFlag ? ADDTHIS_SOCIAL_SHARE_TITLE_I18N_CHINA : ADDTHIS_SOCIAL_SHARE_TITLE_I18N;
    socialShareCategory = chineseLocaleFlag ? SOCIAL_SHARING_CATEGORY_CHINA : SOCIAL_SHARING_CATEGORY;
   } else if (StringUtils.equalsIgnoreCase(provider, "addThis")) {
    i18nSocialShare = ADDTHIS_SOCIAL_SHARE_TITLE_I18N;
    socialShareCategory = SOCIAL_SHARING_CATEGORY;
   }
   // getting social share options from config
   try {
    if (MapUtils.isEmpty(localAddThisWidgetOptions)) {
     localAddThisWidgetOptions = configurationService.getCategoryConfiguration(currentPage, socialShareCategory);
     LOGGER.debug("Configured Options are :" + localAddThisWidgetOptions);
    }
   } catch (ConfigurationNotFoundException e) {
    LOGGER.error("Configuration for social links Not found:", e);
   }
   // preparing social share list based on social share options
   getSocialShareList(localAddThisWidgetOptions, socialShareList, currentPage, resourceResolver, slingRequest, serviceCodes);
   title = i18n.get(i18nSocialShare);
   properties.put(HEADLINE, title);
   properties.put(IS_ENABLED, MapUtils.getBoolean(localAddThisWidgetOptions, ENABLED, Boolean.TRUE));
   properties.put(SOCIAL_MEDIA, socialShareList);
   properties.put(CHINA_SOCIAL_MEDIA_FLAG, chineseLocaleFlag);
   properties.put(PDF_SERVICE_URL, MapUtils.getString(localAddThisWidgetOptions, PDF_SERVICE_URL, StringUtils.EMPTY));
   LOGGER.debug("No of social channels fetched: {}", socialShareList.size());
   
  } else {
   LOGGER.info("Component cannot be found for current page");
  }
  
  return properties;
  
 }
 
 /**
  * Gets the list of of maps social media links and container maps .
  * 
  * @param configurationService
  *         the configuration service
  * @param currentPage
  *         the current page
  * @param addthisWidgetOptions
  *         the addthis widget options
  * @param socialShareList
  *         the social share list
  * @param currentPage
  * @return the social share list
  */
 private static void getSocialShareList(Map<String, String> addthisWidgetOptions, List<Object> socialShareList, Page currentPage,
   ResourceResolver resourceResolver, SlingHttpServletRequest slingRequest, String[] sourceCodes) {
  LOGGER.debug("Fetching the social channels");
  Map<String, Object> socialShareMap;
  Set<String> set = addthisWidgetOptions.keySet();
  Iterator<String> itr = set.iterator();
  while (itr.hasNext()) {
   socialShareMap = new HashMap<String, Object>();
   String socialLink = itr.next();
   if (!StringUtils.equalsIgnoreCase(socialLink, ENABLED) && !StringUtils.equalsIgnoreCase(socialLink, PDF_SERVICE_URL)) {
    String name = addthisWidgetOptions.get(socialLink);
    String[] names = name.split("#");
    String newName = name;
    String url = ComponentUtil.getFullURL(resourceResolver, currentPage.getPath(), slingRequest);
    StringBuilder stringBuilder = new StringBuilder();
    String encodedUrl = StringUtils.EMPTY;
    String currentPageTitle = currentPage.getTitle();
    try {
     url = URLEncoder.encode(url, "UTF-8");
     currentPageTitle = URLEncoder.encode(currentPageTitle, "UTF-8");
    } catch (UnsupportedEncodingException e) {
     LOGGER.error("Error encoding current page", e);
    }
    if (newName.contains("#") && names.length > INTEGER_ONE) {
     name = names[ZERO];
     encodedUrl = stringBuilder.append(names[INTEGER_ONE]).append(url).append("&text=").append(currentPageTitle).append("&Subject=")
       .append(currentPageTitle).append("&body=").append(url).toString();
     String appId = (name.equalsIgnoreCase(FACEBOOK) && names.length > INTEGER_TWO) ? names[INTEGER_TWO] : StringUtils.EMPTY;
     socialShareMap.put(NAME, name);
     socialShareMap.put("url", encodedUrl);
     if (name.equalsIgnoreCase(FACEBOOK) && StringUtils.isNotBlank(appId)) {
      socialShareMap.put("appId", appId);
     }
    } else {
     socialShareMap.put(NAME, newName);
    }
    if (names.length > INTEGER_TWO && !names[ZERO].equalsIgnoreCase(FACEBOOK)) {
     socialShareMap.put("socialMediaName", names[INTEGER_TWO]);
    }
    if (ArrayUtils.isEmpty(sourceCodes) || ArrayUtils.contains(sourceCodes, name)) {
     Map<String, Object> containerTagMap = ComponentUtil.getContainerTagMap(currentPage, socialLink, name);
     socialShareMap.put(UnileverConstants.CONTAINER_TAG, containerTagMap);
     socialShareList.add(socialShareMap);
    }
   }
   
  }
 }
 
}
