/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.jcr.Session;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.http.osgi.services.HttpClientBuilderFactory;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.i18n.I18n;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.components.helper.ProductHelperExt;
import com.unilever.platform.foundation.components.helper.RMSJson;
import com.unilever.platform.foundation.components.helper.RecipeHelper;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.RecipeConfigDTO;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * The Class RecipeCopyViewHelperImpl.
 */
@Component(description = "RecipeRelatedProductsViewHelperImpl", immediate = true, metatype = true, label = "Recipe Related Products Component")
@Service(value = { RecipeRelatedProductsViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.RECIPE_RELATED_PRODUCTS, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class RecipeRelatedProductsViewHelperImpl extends BaseViewHelper {
 
 /** The Constant PAGE_PATH. */
 private static final String PAGE_PATH = "pagePath";
 
 /** The Constant RECIPE_RELATED_PRODUCTS. */
 private static final String RECIPE_RELATED_PRODUCTS = "recipeRelatedProducts";
 
 /** The Constant SECOND_PROPERTY_VALUE. */
 private static final String SECOND_PROPERTY_VALUE = "2_property.value";
 
 /** The Constant SECOND_PROPERTY. */
 private static final String SECOND_PROPERTY = "2_property";
 
 /** The Constant FIRST_PROPERTY_VALUE. */
 private static final String FIRST_PROPERTY_VALUE = "1_property.value";
 
 /** The Constant FIRST_PROPERTY. */
 private static final String FIRST_PROPERTY = "1_property";
 
 /** The Constant PAGE_URL. */
 private static final String PAGE_URL = "pageUrl";
 
 /** The Constant PAGE_DESCRIPTION. */
 private static final String PAGE_DESCRIPTION = "pageDesc";
 
 /** The Constant PRODUCT_PRODUCT_DATA. */
 private static final String PRODUCT_PRODUCT_DATA = "jcr:content/product/productData";
 
 /** The Constant COMMA_SEPARATED. */
 private static final String COMMA_SEPARATED = "\\s*,\\s*";
 
 /** The Constant RELATED_PRODUCTS. */
 private static final String RELATED_PRODUCTS = "relatedProducts";
 
 /** The Constant RELATED_DATA. */
 private static final String RELATED_DATA = "relatedData";
 
 /** The Constant RECIPES. */
 private static final String RECIPES = "recipes";
 
 /** The Constant PRODUCT_CTA_LABEL. */
 private static final String PRODUCT_CTA_LABEL = "productCtaLabel";
 
 /** The Constant RECIPE_RELATED_PRODUCTS_VIEW_PRODUCT_CTA_LABEL. */
 private static final String RECIPE_RELATED_PRODUCTS_VIEW_PRODUCT_CTA_LABEL = "recipeRelatedProducts.viewProductCtaLabel";
 
 /** The Constant RECIPE_COPY_SUB_TITLE. */
 private static final String RECIPE_RELATED_PRODUCTS_SUB_TITLE = "recipeRelatedProductsSubTitle";
 
 /** The Constant RECIPE_COPY_TITLE. */
 private static final String RECIPE_RELATED_PRODUCTS_TITLE = "recipeRelatedProductsTitle";
 
 /** The Constant PRODUCT_COMMERCE_PATH. */
 private static final String PRODUCT_COMMERCE_PATH = "jcr:content/product/cq:commerceType";
 
 /** The Constant PRODUCT. */
 private static final String PRODUCT = "product";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(FeaturedInstagramPostViewHelperImpl.class);
 
 /** The builder. */
 @Reference
 private QueryBuilder builder;
 
 /** The factory. */
 @Reference
 HttpClientBuilderFactory factory;
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  
  LOGGER.info("Recipe Copy View Helper #processData called for processing fixed list.");
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  Map<String, Object> recipesMap = new LinkedHashMap<String, Object>();
  
  I18n i18n = getI18n(resources);
  
  Page currentPage = getCurrentPage(resources);
  String jsonNameSpace = getJsonNameSpace(content);
  ResourceResolver resourceResolver = getResourceResolver(resources);
  
  Map<String, Object> dialogProperties = getProperties(content);
  
  String recipeCopyTitle = MapUtils.getString(dialogProperties, RECIPE_RELATED_PRODUCTS_TITLE);
  String recipeCopySubTitle = MapUtils.getString(dialogProperties, RECIPE_RELATED_PRODUCTS_SUB_TITLE);
  
  RecipeConfigDTO recipeConfigDTO = RecipeHelper.getRecipeConfigurations(configurationService, currentPage, jsonNameSpace);
  
  List<Map<String, Object>> finalProductPropertiesList = new LinkedList<Map<String, Object>>();
  
  RMSJson.getRecipeJsonFromPage(resources, dialogProperties, recipesMap, recipeConfigDTO, factory);
  RMSJson.setRMSServiceDownKey(resources, recipesMap, configurationService, i18n);
  List<String> productPaths = RecipeHelper.getProductPaths(getRelatedProductIds(recipesMap), resources);
  
  List<Page> getProductPages = getProductPages(resourceResolver.adaptTo(Session.class), productPaths, currentPage, finalProductPropertiesList,
    jsonNameSpace, resources);
  
  setPagePropertiesInMap(resourceResolver, finalProductPropertiesList, getProductPages, getSlingRequest(resources));
  
  recipesMap.put(RECIPE_RELATED_PRODUCTS_TITLE, recipeCopyTitle);
  recipesMap.put(RECIPE_RELATED_PRODUCTS_SUB_TITLE, recipeCopySubTitle);
  recipesMap.put(PRODUCT_CTA_LABEL, RecipeHelper.getValueByI18Key(i18n, RECIPE_RELATED_PRODUCTS_VIEW_PRODUCT_CTA_LABEL));
  
  recipesMap.put(UnileverConstants.PRODUCT, finalProductPropertiesList.toArray());
  
  data.put(jsonNameSpace, recipesMap);
  return data;
 }
 
 /**
  * Sets the page properties in map.
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param finalProductPropertiesList
  *         the final product properties list
  * @param getProductPages
  *         the get product pages
  */
 private void setPagePropertiesInMap(ResourceResolver resourceResolver, List<Map<String, Object>> finalProductPropertiesList,
   List<Page> getProductPages, SlingHttpServletRequest slingRequest) {
  for (int productProperty = 0; productProperty < finalProductPropertiesList.size(); productProperty++) {
   
   Map<String, Object> propertyMap = finalProductPropertiesList.get(productProperty);
   
   Page page = null;
   if (getProductPages.size() > productProperty && !getProductPages.isEmpty()) {
    page = getProductPages.get(productProperty);
   }
   LOGGER.debug("Error is propertyMap ---->" + propertyMap);
   if (page != null) {
    String url = ComponentUtil.getFullURL(resourceResolver, page.getPath(), slingRequest);
    ValueMap properties = page.getProperties();
    propertyMap.put(PAGE_URL, url);
    propertyMap.put(PAGE_DESCRIPTION, properties.get(JcrConstants.JCR_DESCRIPTION));
   } else {
    propertyMap.put(PAGE_URL, StringUtils.EMPTY);
    propertyMap.put(PAGE_DESCRIPTION, StringUtils.EMPTY);
   }
   LOGGER.debug("Error is page---->" + page);
  }
 }
 
 /**
  * Gets the product properties.
  * 
  * @param recipesMap
  *         the recipes map
  * @return the product properties
  */
 public static String[] getRelatedProductIds(Map<String, Object> recipesMap) {
  
  String[] relatedProductIds = ArrayUtils.EMPTY_STRING_ARRAY;
  
  Map<String, Object> dataMap = RecipeHelper.getChildMap(recipesMap, RECIPES);
  Map<String, Object> relatedDataMap = RecipeHelper.getChildMap(dataMap, RELATED_DATA);
  
  if (relatedDataMap != null && !relatedDataMap.isEmpty() && relatedDataMap.containsKey(RELATED_PRODUCTS)) {
   
   Object relatedProducts = relatedDataMap.get(RELATED_PRODUCTS);
   if (relatedProducts != null) {
    relatedProductIds = (String[]) ((String) relatedProducts).split(COMMA_SEPARATED);
   }
   
  }
  return relatedProductIds;
 }
 
 /**
  * Gets the product pages.
  * 
  * @param session
  *         the session
  * @param productPropertiesList
  *         the product properties list
  * @param currentPage
  *         the current page
  * @param finalProductPropertiesList
  *         the final product properties list
  * @param jsonNameSpace
  *         the json name space
  * @param resources
  *         the resources
  * @return the product pages
  */
 private List<Page> getProductPages(Session session, List<String> productPropertiesList, Page currentPage,
   List<Map<String, Object>> finalProductPropertiesList, String jsonNameSpace, Map<String, Object> resources) {
  
  List<Page> productPageList = new ArrayList<Page>();
  String path = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, RECIPE_RELATED_PRODUCTS, PAGE_PATH);
  
  productPageList = getProductPages(session, path, productPropertiesList, finalProductPropertiesList, jsonNameSpace, resources);
  
  return productPageList;
 }
 
 /**
  * Gets the pages.
  * 
  * @param session
  *         the session
  * @param path
  *         the path
  * @param productPaths
  *         the page path
  * @param finalProductPropertiesList
  *         the final product properties list
  * @param jsonNameSpace
  *         the json name space
  * @param resources
  *         the resources
  * @return the pages
  */
 public List<Page> getProductPages(Session session, String path, List<String> productPaths, List<Map<String, Object>> finalProductPropertiesList,
   String jsonNameSpace, Map<String, Object> resources) {
  
  Map<String, String> map = new HashMap<String, String>();
  List<Page> productPages = new ArrayList<Page>();
  
  for (String productPath : productPaths) {
   List<Page> pageList = new ArrayList<Page>();
   
   map.put("path", path);
   map.put("type", NameConstants.NT_PAGE);
   map.put(FIRST_PROPERTY, PRODUCT_COMMERCE_PATH);
   map.put(FIRST_PROPERTY_VALUE, PRODUCT);
   
   map.put(SECOND_PROPERTY, PRODUCT_PRODUCT_DATA);
   map.put(SECOND_PROPERTY_VALUE, productPath);
   
   if (builder != null) {
    pageList = getResultfromQuery(session, map, builder);
    
    if (!pageList.isEmpty()) {
     UNIProduct uniProduct = ProductHelperExt.getProductByPath(getResourceResolver(resources), productPath);
     finalProductPropertiesList.add(ProductHelper.getProductMap(uniProduct, jsonNameSpace, resources, getCurrentPage(resources)));
    }
    
    productPages.addAll(pageList);
   }
  }
  return productPages;
 }
 
 /**
  * Gets the resultfrom query.
  * 
  * @param session
  *         the session
  * @param map
  *         the map
  * @param builder
  *         the builder
  * @return the resultfrom query
  */
 public static List<Page> getResultfromQuery(Session session, Map<String, String> map, QueryBuilder builder) {
  
  com.day.cq.search.Query query = null;
  SearchResult result = null;
  List<Page> pageList = new ArrayList<Page>();
  
  query = builder.createQuery(PredicateGroup.create(map), session);
  query.setHitsPerPage(0);
  if (query != null) {
   result = query.getResult();
   
   Iterator<Resource> rsIterator = result.getResources();
   while (rsIterator.hasNext()) {
    Page page = rsIterator.next().adaptTo(Page.class);
    if (page != null) {
     pageList.add(page);
    }
   }
  }
  return pageList;
 }
}
