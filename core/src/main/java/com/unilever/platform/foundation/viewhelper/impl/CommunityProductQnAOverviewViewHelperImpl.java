/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.BazaarVoiceSEO;
import com.unilever.platform.foundation.components.KritiqueSEO;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.components.helper.ProductHelperExt;
import com.unilever.platform.foundation.components.helper.TurnToHelper;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * This class prepares the data map for Community Product Q&A Overview.
 */
@Component(description = "CommunityProductQnAOverviewViewHelperImpl", immediate = true, metatype = true, label = "Community Product Q&A Overview ViewHelper")
@Service(value = { CommunityProductQnAOverviewViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.COMMUNITY_PRODUCT_QA_OVERVIEW, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class CommunityProductQnAOverviewViewHelperImpl extends BaseViewHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(CommunityProductQnAOverviewViewHelperImpl.class);
 private static final String PRODUCT_PAGE = "productPage";
 private static final String COMMUNITY_PRODUCT_QA = "communityProductQA";
 
 @Reference
 ConfigurationService configurationService;
 
 @Reference
 KritiqueSEO kqseoService;
 
 @Reference
 BazaarVoiceSEO bvseoService;
 
 @SuppressWarnings("unchecked")
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("CommunityProductQnAOverviewViewHelperImpl #processData called.");
  ResourceResolver resourceResolver = (ResourceResolver) resources.get(ProductConstants.RESOURCE_RESOLVER);
  Map<String, Object> communityProductQnAOverviewProperties = (Map<String, Object>) content.get(PROPERTIES);
  SlingHttpServletRequest slingRequest = getSlingRequest(resources);
  Map<String, Object> properties = new LinkedHashMap<String, Object>();
  Map<String, String> serviceProviderData = new HashMap<String, String>();
  Map<String, Object> productProperties = new HashMap<String, Object>();
  UNIProduct product = null;
  Page currentPage = getCurrentPage(resources);
  String jsonNameSapce = getJsonNameSpace(content);
  String productPagePath = MapUtils.getString(communityProductQnAOverviewProperties, PRODUCT_PAGE);
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  if (StringUtils.isNotBlank(productPagePath)) {
   product = ProductHelper.getUniProduct(pageManager.getPage(productPagePath));
  } else {
   product = ProductHelper.getUniProduct(currentPage, slingRequest);
  }
  if (product != null) {
   productProperties = ProductHelperExt.getProductTeaserData(product, currentPage, slingRequest, false);
   productProperties.put(ProductConstants.PRICE_ENABLED, ProductHelper.isPriceEnabled(configurationService, currentPage));
  }
  properties.put(ProductConstants.PRODUCT_MAP, productProperties);
  try {
   String serviceProviderName = configurationService.getConfigValue(currentPage, ProductConstants.COMMUNITY_PRODUCT_QA_CAT,
     ProductConstants.SERVICE_PROVIDER_NAME);
   serviceProviderData = configurationService.getCategoryConfiguration(currentPage, serviceProviderName);
   if (product != null && serviceProviderName != null) {
    if ("turnTo".equals(serviceProviderName)) {
     properties.put(COMMUNITY_PRODUCT_QA, TurnToHelper.getTurnToConfig(currentPage, configurationService, product.getSKU()));
    } else {
     properties.put(COMMUNITY_PRODUCT_QA, serviceProviderData);
    }
   } else {
    properties.put(COMMUNITY_PRODUCT_QA, new HashMap<String, String>());
    LOGGER.info("No Product is available for current page");
   }
  } catch (Exception e) {
   LOGGER.error("could not find communityProductQnA category", e);
  }
  LOGGER.info("Community Product QnA Overview JSON successfully generated");
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(jsonNameSapce, properties);
  return data;
 }
 
}
