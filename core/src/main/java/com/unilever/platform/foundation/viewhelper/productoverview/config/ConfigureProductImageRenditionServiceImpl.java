/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.productoverview.config;

import java.util.Dictionary;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unilever.platform.foundation.components.helper.ImageHelper;

/**
 * The Class ConfigureProductImageRenditionServiceImpl.
 */
@Component(immediate = true, label = "Product Image Configuration Service", description = "Product Configuration Service", metatype = true)
@Service(value = ConfigureProductImageRenditionService.class)
public class ConfigureProductImageRenditionServiceImpl implements ConfigureProductImageRenditionService {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ImageHelper.class);
 
 /** The Constant IMAGE_ZOOM_WIDTH. */
 @Property(label = "Image Zoom Width", value = "700", description = "Enter image size u want")
 public static final String IMAGE_ZOOM_WIDTH = "image.zoom.width";
 
 /** The Constant IMAGE_THUMBNAIL_WIDTH. */
 @Property(label = "Image Thumbnail Width", value = "400", description = "Enter the required image size")
 public static final String IMAGE_THUMBNAIL_WIDTH = "image_thumbnail_width";
 
 /** The zoom width. */
 private String zoomWidth;
 
 /** The thumbnail width. */
 private String thumbnailWidth;
 
 /** The props. */
 @SuppressWarnings("rawtypes")
 Dictionary props = null;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.productoverview.config.ConfigureProductImageRenditionService#getZoomWidth()
  */
 @Override
 public String getZoomWidth() {
  return zoomWidth;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.productoverview.config.ConfigureProductImageRenditionService#getThumbnailWidth()
  */
 @Override
 public String getThumbnailWidth() {
  
  return thumbnailWidth;
 }
 
 /**
  * Activate.
  * 
  * @param context
  *         the context
  */
 @Activate
 protected void activate(ComponentContext context) {
  try {
   
   props = context.getProperties();
   
   zoomWidth = (String) props.get(IMAGE_ZOOM_WIDTH).toString();
   thumbnailWidth = (String) props.get(IMAGE_THUMBNAIL_WIDTH).toString();
  } catch (Exception e) {
   LOGGER.error(e.getMessage(), e);
  }
 }
 
}
