/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.aem.foundation.constants.CommonConstants;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.DiagnosticToolHelper;
import com.unilever.platform.foundation.components.helper.ProductBundlingToBinUtil;
import com.unilever.platform.foundation.components.helper.SocialShareHelper;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.Image;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * The Class DiagnosticToolViewHelperImpl.
 */
@Component(description = "DiagnosticToolViewHelperImpl", immediate = true, metatype = true, label = "DiagnosticToolViewHelperImpl")
@Service(value = { DiagnosticToolViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.DIAGNOSTIC_TOOL, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING,
  intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class DiagnosticToolViewHelperImpl extends BaseViewHelper {
    
 private static final String DIAGNOSTIC_TOOL_SEPARATION_TEXT = "diagnosticTool.separationText";

 private static final String SIGN_UP_OPEN_IN_NEW_WINDOW = "signUpOpenInNewWindow";

 private static final String PAGE_URL_SIGN_UP = "pageUrlSignUp";

 private static final String CTA_LABEL_SIGN_UP = "ctaLabelSignUp";

 private static final String INTRODUCTION_SIGN_UP = "introductionSignUp";

 private static final String OVERRIDE_CONFIG_FOR_SIGN_UP = "overrideConfigForSignUp";

 private static final String ENABLE_SIGN_UP = "enableSignUp";

 private static final String NON_USER_INFO = "nonUserInfo";

 private static final String USER_INFO = "userInfo";

 private static final String RESPONSE_TEXT = "responseText";

 private static final String USER_OPEN_IN_NEW_WINDOW = "userOpenInNewWindow";

 private static final String PROFILE_URL = "profileUrl";

 private static final String CTA_LABEL_USER = "ctaLabelUser";

 private static final String RESPONSE_TEXT_USER = "responseTextUser";

 private static final String INTRODUCTION_TEXT_USER = "introductionTextUser";

 private static final String NON_USER_OPEN_IN_NEW_WINDOW = "nonUserOpenInNewWindow";

 private static final String LOGIN_URL = "loginUrl";

 private static final String CTA_LABEL_NON_USER = "ctaLabelNonUser";

 private static final String INTRODUCTION_TEXT_NON_USER = "introductionTextNonUser";

 private static final String OVERRIDE_CONFIG_FOR_REGISTRATION = "overrideConfigForRegistration";

 private static final String ENABLE_LOGIN_REGISTER = "enableLoginRegister";

 private static final String SAVE_TO_PROFILE_URL = "saveToProfileUrl";

 private static final String ENABLE_REGISTRATION = "enableRegistration";

 private static final String OVERRIDE_CONFIG_FOR_EMAIL = "overrideConfigForEmail";

 private static final String SIGN_UP_CONFIG = "signUpConfig";

 private static final String LOGIN_REGISTER_CONFIG = "loginRegisterConfig";

    /**
     * The constanr DIAGNOSTIC_TOOL
     */
 private static final String DIAGNOSTIC_TOOL = "diagnosticTool";

/** The Constant ENTITY. */
 private static final String ENTITY = "entity";
 
 /** The Constant HUNDRED. */
 private static final int HUNDRED = 100;
 
 /** The Constant EMAIL_CONFIG. */
 private static final String EMAIL_CONFIG = "emailConfig";
 
 /** The Constant EMAIL_FAILURE_MSG. */
 private static final String EMAIL_FAILURE_MSG = "emailFailureMsg";
 
 /** The Constant CTA_URL. */
 private static final String CTA_URL = "ctaURL";
 
 /** The Constant CTA_LABEL. */
 private static final String CTA_LABEL = "ctaLabel";
 
 /** The Constant EMAIL_SUCCESS_MSG. */
 private static final String EMAIL_SUCCESS_MSG = "emailSuccessMsg";
 
 /** The Constant EMAIL_PREFIX. */
 private static final String EMAIL_PREFIX = "emailPrefix";
 
 /** The Constant ENABLE_EMAIL. */
 private static final String ENABLE_EMAIL = "enableEmail";
 
 /** The Constant SPLIT_BY_DOT. */
 private static final String SPLIT_BY_DOT = "\\.";
 
 /** The Constant STATIC. */
 private static final String STATIC = "static";
 
 /** The Constant DIAGNOSTIC_TOOL_PAGE_XOF_Y_TEXT. */
 private static final String DIAGNOSTIC_TOOL_PAGE_XOF_Y_TEXT = "diagnosticTool.pageXofYText";
 
 /** The Constant DIAGNOSTIC_TOOL_EMAIL_FORM_CTA_LABEL. */
 private static final String DIAGNOSTIC_TOOL_EMAIL_FORM_CTA_LABEL = "diagnosticTool.emailFormCtaLabel";
 
 /** The Constant DIAGNOSTIC_TOOL_EMAIL_FORM_PLACEHOLDER_TEXT. */
 private static final String DIAGNOSTIC_TOOL_EMAIL_FORM_PLACEHOLDER_TEXT = "diagnosticTool.emailFormPlaceholderText";
 
 /** The Constant DIAGNOSTIC_TOOL_READ_ARTICLE_CTA_LABEL. */
 private static final String DIAGNOSTIC_TOOL_READ_ARTICLE_CTA_LABEL = "diagnosticTool.readArticleCtaLabel";
 
 /** The Constant DIAGNOSTIC_TOOL_BUY_IT_NOW_CTA_LABEL. */
 private static final String DIAGNOSTIC_TOOL_BUY_IT_NOW_CTA_LABEL = "diagnosticTool.buyItNowCtaLabel";
 
 /** The Constant DIAGNOSTIC_TOOL_EXIT_QUESTIONNAIRE_CTA_LABEL. */
 private static final String DIAGNOSTIC_TOOL_EXIT_QUESTIONNAIRE_CTA_LABEL = "diagnosticTool.exitQuestionnaireCtaLabel";
 
 /** The Constant DIAGNOSTIC_TOOL_RESTART_QUESTIONNAIRE_CTA_LABEL. */
 private static final String DIAGNOSTIC_TOOL_RESTART_QUESTIONNAIRE_CTA_LABEL = "diagnosticTool.restartQuestionnaireCtaLabel";
 
 /** The Constant DIAGNOSTIC_TOOL_FINISH_QUESTIONNAIRE_CTA_LABEL. */
 private static final String DIAGNOSTIC_TOOL_FINISH_QUESTIONNAIRE_CTA_LABEL = "diagnosticTool.finishQuestionnaireCtaLabel";
 
 /** The Constant DIAGNOSTIC_TOOL_PREVIOUS_QUESTION_CTA_LABEL. */
 private static final String DIAGNOSTIC_TOOL_PREVIOUS_QUESTION_CTA_LABEL = "diagnosticTool.previousQuestionCtaLabel";
 
 /** The Constant DIAGNOSTIC_TOOL_NEXT_QUESTION_CTA_LABEL. */
 private static final String DIAGNOSTIC_TOOL_NEXT_QUESTION_CTA_LABEL = "diagnosticTool.nextQuestionCtaLabel";
 
 /** The Constant SEND_EMAIL_URL. */
 private static final String SEND_EMAIL_URL = "sendEmailUrl";
 
 /** The Constant POST_RESPONSE_URL. */
 private static final String POST_RESPONSE_URL = "postResponseUrl";
 
 /** The Constant GET_RESPONSE_URL. */
 private static final String GET_RESPONSE_URL = "getResponseUrl";
 
 /** The Constant POST_URL. */
 private static final String POST_URL = "postUrl";
 
 /** The Constant QUESTION_PATH. */
 private static final String QUESTION_PATH = "questionPath";
 
 /** The Constant GENERAL_CONFIG_DATA. */
 private static final String GENERAL_CONFIG_DATA = "generalConfigData";
 
 /** The Constant INTRODUCTION_PANEL. */
 private static final String INTRODUCTION_PANEL = "introductionPanel";
 
 /** The Constant QUESTIONS. */
 private static final String QUESTIONS = "questions";
 
 /** The Constant INTRO_PANEL_START_CTA_LABEL. */
 private static final String INTRO_PANEL_START_CTA_LABEL = "introPanelStartCtaLabel";
 
 /** The Constant INTRO_PANEL_FOREGROUND_IMAGE_ALT_TEXT. */
 private static final String INTRO_PANEL_FOREGROUND_IMAGE_ALT_TEXT = "introPanelForegroundImageAltText";
 
 /** The Constant INTRO_PANEL_FOREGROUND_IMAGE. */
 private static final String INTRO_PANEL_FOREGROUND_IMAGE = "introPanelForegroundImage";
 
 /** The Constant INTRO_PANEL_BACKGROUND_IMAGE_ALT_TEXT. */
 private static final String INTRO_PANEL_BACKGROUND_IMAGE_ALT_TEXT = "introPanelBackgroundImageAltText";
 
 /** The Constant INTRO_PANEL_BACKGROUND_IMAGE. */
 private static final String INTRO_PANEL_BACKGROUND_IMAGE = "introPanelBackgroundImage";
 
 /** The Constant INTRO_PANEL_LONG_SUBHEADING_TEXT. */
 private static final String INTRO_PANEL_LONG_SUBHEADING_TEXT = "introPanelLongSubheadingText";
 
 /** The Constant INTRO_PANEL_SHORT_SUBHEADING_TEXT. */
 private static final String INTRO_PANEL_SHORT_SUBHEADING_TEXT = "introPanelShortSubheadingText";
 
 /** The Constant INTRO_PANEL_HEADING_TEXT. */
 private static final String INTRO_PANEL_HEADING_TEXT = "introPanelHeadingText";
 
 /** The Constant PROGRESS_INDICATOR. */
 private static final String PROGRESS_INDICATOR = "progressIndicator";
 
 /** The Constant INCLUDE_INTRO_PANEL. */
 private static final String INCLUDE_INTRO_PANEL = "includeIntroPanel";
 
 /** The Constant RETURN_USER_TEXT. */
 private static final String RETURN_USER_TEXT = "returnUserText";
 
 /** The Constant GET_RESPONSE_DATA. */
 private static final String GET_RESPONSE_DATA = ".diagnostictoolresponse";
 
 /** The Constant DOT_JSON. */
 private static final String DOT_JSON = ".json";
 
 /** The Constant PRODUCT_SUMMARY. */
 private static final String PRODUCT_SUMMARY = "productSummary";
 
 /** The Constant PRODUCT_SUMMARY_BACKGROUND_IMAGE. */
 private static final String PRODUCT_SUMMARY_BACKGROUND_IMAGE = "productSummaryBackgroundImage";
 
 /** The Constant PRODUCT_SUMMARY_BACKGROUND_IMAGE_ALT_TEXT. */
 private static final String PRODUCT_SUMMARY_BACKGROUND_IMAGE_ALT_TEXT = "productSummaryBackgroundImageAltText";
 
 /** The Constant PRODUCT_SUMMARY_LONG_SUBHEADING_TEXT. */
 private static final String PRODUCT_SUMMARY_LONG_SUBHEADING_TEXT = "productSummaryLongSubheadingText";
 
 /** The Constant PRODUCT_SUMMARY_SHORT_SUBHEADING_TEXT. */
 private static final String PRODUCT_SUMMARY_SHORT_SUBHEADING_TEXT = "productSummaryShortSubheadingText";
 
 /** The Constant PRODUCT_SUMMARY_HEADING_TEXT. */
 private static final String PRODUCT_SUMMARY_HEADING_TEXT = "productSummaryHeadingText";
 
 /** The diagnostic tool question view helper impl. */
 @Reference
 DiagnosticToolQuestionViewHelperImpl diagnosticToolQuestionViewHelperImpl;
 
 /** The diagnostic tool result section view helper impl. */
 @Reference
 DiagnosticToolResultSectionViewHelperImpl diagnosticToolResultSectionViewHelperImpl;
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 /**
  * logger object.
  **/
 private static final Logger LOGGER = LoggerFactory.getLogger(DiagnosticToolViewHelperImpl.class);
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.info("calling DiagnosticToolViewHelperImpl helper class inside pocess data");
  ResourceResolver resourceResolver = getResourceResolver(resources);
  Resource currentResource = getCurrentResource(resources);
  Page currentPage = getCurrentPage(resources);
  I18n i18n = getI18n(resources);
  SlingHttpServletRequest slingHttpServletRequest = getSlingRequest(resources);
  Map<String, Object> componentProperties = getProperties(content);
  String jsonNameSpace = getJsonNameSpace(content);
  String componentName = getComponentName(currentResource, slingHttpServletRequest);
  Map<String, Object> diagnosticToolMap = new LinkedHashMap<String, Object>();
  String userText = MapUtils.getString(componentProperties, RETURN_USER_TEXT, StringUtils.EMPTY);
  String progressIndicatorType = MapUtils.getString(componentProperties, PROGRESS_INDICATOR, StringUtils.EMPTY);
  if (StringUtils.isNotBlank(progressIndicatorType)) {
   diagnosticToolMap.put(GENERAL_CONFIG_DATA, getGeneralConfigMapData(userText, progressIndicatorType));
   setIntroPanelMap(currentPage, slingHttpServletRequest, componentProperties, diagnosticToolMap);
   setQuestions(resourceResolver, currentResource, currentPage, slingHttpServletRequest, diagnosticToolMap, progressIndicatorType,
     i18n.get(DIAGNOSTIC_TOOL_PAGE_XOF_Y_TEXT));
   setAndPostResponseUrls(resourceResolver, currentResource, currentPage, componentName, diagnosticToolMap);
   diagnosticToolMap.put(PRODUCT_SUMMARY, getProductSummaryData(currentPage, slingHttpServletRequest, componentProperties));
   diagnosticToolMap.put(STATIC, getStaticI18nKeys(i18n));
   Map<String, String> configMap = ComponentUtil.getConfigMap(currentPage, DIAGNOSTIC_TOOL);
   setOverrideLoginSignUpEmail(resourceResolver, slingHttpServletRequest, componentProperties, diagnosticToolMap, configMap);
   boolean enableSocialSharing = MapUtils.getBooleanValue(componentProperties, "enableSocialSharing", false);
   Map<String, Object> socialSharingMap = new LinkedHashMap<String, Object>();
   socialSharingMap.put("enableSocialSharing", enableSocialSharing);
   if (enableSocialSharing) {
    Map<String, String> addThisOptionsConfig = null;
    try {
     addThisOptionsConfig = configurationService.getCategoryConfiguration(currentPage, "diagnosticToolAddThis");
    } catch (ConfigurationNotFoundException e) {
     LOGGER.error(e.getMessage() + e);
    }
    
    socialSharingMap.put("socialSharing", SocialShareHelper.addThisWidget(resources, configurationService, addThisOptionsConfig));
    String socialOptionsPrefix = MapUtils.getString(componentProperties, "socialOptionsPrefix", StringUtils.EMPTY);
    String sharingPrefix = MapUtils.getString(componentProperties, "sharingPrefix", StringUtils.EMPTY);
    String personalisedTextFrom = MapUtils.getString(componentProperties, "personalisedTextFrom", StringUtils.EMPTY);
    String sharingSuffix = MapUtils.getString(componentProperties, "sharingSuffix", StringUtils.EMPTY);
    String sharingImage = MapUtils.getString(componentProperties, "sharingImage", StringUtils.EMPTY);
    String sharingImageAltText = MapUtils.getString(componentProperties, "sharingImageAltText", StringUtils.EMPTY);
    Map<String, Object> sharingImageMap = getImageProperties(currentPage, slingHttpServletRequest, sharingImage, sharingImageAltText);
    String sharingTitle = MapUtils.getString(componentProperties, "sharingTitle", StringUtils.EMPTY);
    socialSharingMap.put("socialOptionsPrefix", socialOptionsPrefix);
    socialSharingMap.put("sharingPrefix", sharingPrefix);
    socialSharingMap.put("personalisedTextFrom", personalisedTextFrom);
    socialSharingMap.put("sharingSuffix", sharingSuffix);
    socialSharingMap.put("sharingImageMap", sharingImageMap);
    socialSharingMap.put("sharingTitle", sharingTitle);
   }
   diagnosticToolMap.put(ENTITY, MapUtils.getString(configMap, ENTITY, StringUtils.EMPTY));
   diagnosticToolMap.put("socialSharing", socialSharingMap);
   ProductBundlingToBinUtil.setMultiBuyProductsConfigProperties(componentProperties, diagnosticToolMap, configMap,
                    MapUtils.getBooleanValue(componentProperties, UnileverConstants.OVERRIDE_GLOBAL_CONFIG, true),configurationService,currentPage);
   ProductBundlingToBinUtil.setMultiBuyProductsCtaLabels(resources, diagnosticToolMap);
  }
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  data.put(jsonNameSpace, diagnosticToolMap);
  return data;
 }

/**sets map for login register,sign up and email
 * @param resourceResolver
 * @param slingHttpServletRequest
 * @param componentProperties
 * @param diagnosticToolMap
 * @param configMap
 */
 private void setOverrideLoginSignUpEmail(ResourceResolver resourceResolver, SlingHttpServletRequest slingHttpServletRequest,
        Map<String, Object> componentProperties, Map<String, Object> diagnosticToolMap, Map<String, String> configMap) {
  boolean overrideConfigForLoginRegister = MapUtils.getBooleanValue(componentProperties, OVERRIDE_CONFIG_FOR_REGISTRATION, false);
  Map<String, Object> mapOverriden = DiagnosticToolHelper.getOverriddenConfigMap(configMap,
          componentProperties, true);
  Map<String, Object> mapNotOverriden = DiagnosticToolHelper.getOverriddenConfigMap(configMap,
          componentProperties, false);
  Map<String, Object> overridenConfigMap = overrideConfigForLoginRegister?mapOverriden:mapNotOverriden;
  diagnosticToolMap.put(LOGIN_REGISTER_CONFIG,
               getLoginRegisterConfigData(resourceResolver,
                       componentProperties, slingHttpServletRequest, overridenConfigMap, overrideConfigForLoginRegister, configMap));
  boolean overrideConfigForSignUp = MapUtils.getBooleanValue(componentProperties, OVERRIDE_CONFIG_FOR_SIGN_UP, false);
  Map<String, Object> overridenSignUpConfigMap = overrideConfigForSignUp?mapOverriden:mapNotOverriden;
  diagnosticToolMap
       .put(SIGN_UP_CONFIG, getSignUpConfigData(resourceResolver,
               componentProperties, slingHttpServletRequest, overridenSignUpConfigMap, overrideConfigForSignUp));
  boolean overrideConfigForEmail = MapUtils.getBooleanValue(componentProperties, OVERRIDE_CONFIG_FOR_EMAIL, false);
  Map<String, Object> overridenEmailConfigMap = overrideConfigForEmail?mapOverriden:mapNotOverriden;
  diagnosticToolMap
         .put(EMAIL_CONFIG, getEmailConfigData(resourceResolver,
                 componentProperties, slingHttpServletRequest, overridenEmailConfigMap, overrideConfigForEmail, configMap));
}
 
 /**
  * Gets the email config data.
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param currentPage
  *         the current page
  * @param componentProperties
  *         the component properties
  * @param jsonNameSpace
  *         the json name space
  * @param slingRequest
  *         the sling request
  * @param configMap
  *         the configMap
  * @return the email config data
  */
 private Map<String, Object> getEmailConfigData(ResourceResolver resourceResolver, Map<String, Object> componentProperties,
   SlingHttpServletRequest slingRequest, Map<String, Object> overridenConfigMap, boolean overrideConfigForEmail, Map<String, String> configMap) {
  boolean enableEmail = MapUtils.getBooleanValue(componentProperties, ENABLE_EMAIL, false);
  Map<String, Object> emailMap = new LinkedHashMap<String, Object>();
  emailMap.put(ENABLE_EMAIL, enableEmail);
  emailMap.put(OVERRIDE_CONFIG_FOR_EMAIL, overrideConfigForEmail);
  if(enableEmail){
   String emailConfigUrl = MapUtils.getString(configMap, SEND_EMAIL_URL, StringUtils.EMPTY);
   String sendEmailUrl = resourceResolver.map(emailConfigUrl);
   emailMap.put(SEND_EMAIL_URL, sendEmailUrl);
   String emailPrefix = MapUtils.getString(overridenConfigMap, EMAIL_PREFIX, StringUtils.EMPTY);
   String emailSuccessMsg = MapUtils.getString(overridenConfigMap, EMAIL_SUCCESS_MSG, StringUtils.EMPTY);
   String ctaLabel = MapUtils.getString(overridenConfigMap, CTA_LABEL, StringUtils.EMPTY);
   String ctaURL = MapUtils.getString(overridenConfigMap, CTA_URL, StringUtils.EMPTY);
   boolean openInNewWindow = MapUtils.getBooleanValue(overridenConfigMap, UnileverConstants.OPEN_IN_NEW_WINDOW, false);
   String emailFailureMsg = MapUtils.getString(overridenConfigMap, EMAIL_FAILURE_MSG, StringUtils.EMPTY);
   String ctaFullUrl = ComponentUtil.getFullURL(resourceResolver, ctaURL, slingRequest);
   emailMap.put(EMAIL_PREFIX, emailPrefix);
   emailMap.put(EMAIL_SUCCESS_MSG, emailSuccessMsg);
   emailMap.put(EMAIL_FAILURE_MSG, emailFailureMsg);
   emailMap.put(UnileverConstants.URL, ctaFullUrl);
   emailMap.put(UnileverConstants.CTA_LABEL, ctaLabel);
   emailMap.put(UnileverConstants.OPEN_IN_NEW_WINDOW, openInNewWindow);
  }
  return emailMap;
 }
 
 /**
  * Gets the sign up config data.
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param currentPage
  *         the current page
  * @param componentProperties
  *         the component properties
  * @param jsonNameSpace
  *         the json name space
  * @param slingRequest
  *         the sling request
  * @param configMap
  *         the configMap
  * @return the email config data
  */
 private Map<String, Object> getSignUpConfigData(ResourceResolver resourceResolver, Map<String, Object> componentProperties,
    SlingHttpServletRequest slingRequest,  Map<String, Object> overridenConfigMap, boolean overrideConfigForSignUp) {
  boolean enableSignUp = MapUtils.getBooleanValue(componentProperties, ENABLE_SIGN_UP, false);
  Map<String, Object> signUpMap = new LinkedHashMap<String, Object>();
  signUpMap.put(ENABLE_SIGN_UP, enableSignUp);
  signUpMap.put(OVERRIDE_CONFIG_FOR_SIGN_UP, overrideConfigForSignUp);
  if(enableSignUp){
   String intorductionTextSignUp = MapUtils.getString(overridenConfigMap, INTRODUCTION_SIGN_UP, StringUtils.EMPTY);
   String ctaLabelSignUp = MapUtils.getString(overridenConfigMap, CTA_LABEL_SIGN_UP, StringUtils.EMPTY);
   String pageUrlSignUp = MapUtils.getString(overridenConfigMap, PAGE_URL_SIGN_UP, StringUtils.EMPTY);
   boolean signUpOpenInNewWindow = MapUtils.getBoolean(overridenConfigMap, SIGN_UP_OPEN_IN_NEW_WINDOW, false);
   String signUpPageFullUrl = ComponentUtil.getFullURL(resourceResolver, pageUrlSignUp, slingRequest);
   signUpMap.put(UnileverConstants.HEADING, intorductionTextSignUp);
   signUpMap.put(UnileverConstants.URL, signUpPageFullUrl);
   signUpMap.put(UnileverConstants.CTA_LABEL, ctaLabelSignUp);
   signUpMap.put(UnileverConstants.OPEN_IN_NEW_WINDOW, signUpOpenInNewWindow);
  }
  return signUpMap;
 }
 
 /**
  * Gets the login register config data.
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param currentPage
  *         the current page
  * @param componentProperties
  *         the component properties
  * @param jsonNameSpace
  *         the json name space
  * @param slingRequest
  *         the sling request
  * @param configMap
  *         the configMap
  * @return the email config data
  */
 private Map<String, Object> getLoginRegisterConfigData(ResourceResolver resourceResolver, Map<String, Object> componentProperties,
    SlingHttpServletRequest slingRequest,  Map<String, Object> overridenConfigMap, boolean overrideLoginRegisterConfig, Map<String,
    String> configMap) {
  boolean enableRegistration = MapUtils.getBooleanValue(componentProperties, ENABLE_REGISTRATION, false);
  Map<String, Object> loginRegisterMap = new LinkedHashMap<String, Object>();
  loginRegisterMap.put(ENABLE_LOGIN_REGISTER, enableRegistration);
  loginRegisterMap.put(OVERRIDE_CONFIG_FOR_REGISTRATION, overrideLoginRegisterConfig);
  if(enableRegistration){
   String saveToProfileConfigUrl = MapUtils.getString(configMap, SAVE_TO_PROFILE_URL, StringUtils.EMPTY);
   String saveToProfileUrl = resourceResolver.map(saveToProfileConfigUrl);
   String introductionTextNonUser = MapUtils.getString(overridenConfigMap, INTRODUCTION_TEXT_NON_USER, StringUtils.EMPTY);
   String ctaLabelNonUser = MapUtils.getString(overridenConfigMap, CTA_LABEL_NON_USER, StringUtils.EMPTY);
   String loginUrl = MapUtils.getString(overridenConfigMap, LOGIN_URL, StringUtils.EMPTY);
   boolean nonUserOpenInNewWindow = MapUtils.getBoolean(overridenConfigMap, NON_USER_OPEN_IN_NEW_WINDOW, false);
   String introductionTextUser = MapUtils.getString(overridenConfigMap, INTRODUCTION_TEXT_USER, StringUtils.EMPTY);
   String responseTextUser = MapUtils.getString(overridenConfigMap, RESPONSE_TEXT_USER, StringUtils.EMPTY);
   String ctaLabelUser = MapUtils.getString(overridenConfigMap, CTA_LABEL_USER, StringUtils.EMPTY);
   String profileUrl = MapUtils.getString(overridenConfigMap, PROFILE_URL, StringUtils.EMPTY);
   boolean userOpenInNewWindow = MapUtils.getBoolean(overridenConfigMap, USER_OPEN_IN_NEW_WINDOW, false);
   String profileFullUrl = ComponentUtil.getFullURL(resourceResolver, profileUrl, slingRequest);
   String loginFullUrl = ComponentUtil.getFullURL(resourceResolver, loginUrl, slingRequest);
   Map<String, Object> nonUserMap = new LinkedHashMap<String, Object>();
   nonUserMap.put(UnileverConstants.HEADING, introductionTextNonUser);
   nonUserMap.put(UnileverConstants.CTA_LABEL, ctaLabelNonUser);
   nonUserMap.put(UnileverConstants.URL, loginFullUrl);
   nonUserMap.put(UnileverConstants.OPEN_IN_NEW_WINDOW, nonUserOpenInNewWindow);
   Map<String, Object> userMap = new LinkedHashMap<String, Object>();
   userMap.put(UnileverConstants.HEADING, introductionTextUser);
   userMap.put(RESPONSE_TEXT, responseTextUser);
   userMap.put(UnileverConstants.CTA_LABEL, ctaLabelUser);
   userMap.put(UnileverConstants.URL, profileFullUrl);
   userMap.put(UnileverConstants.OPEN_IN_NEW_WINDOW, userOpenInNewWindow);
   loginRegisterMap.put(USER_INFO, userMap);
   loginRegisterMap.put(NON_USER_INFO, nonUserMap);
   loginRegisterMap.put(SAVE_TO_PROFILE_URL, saveToProfileUrl);
  }
  return loginRegisterMap;
 }
 
 /**
  * gets the map of keys used for diagnostic tool different cta labels.
  * 
  * @param i18n
  *         the i 18 n
  * @return the static I 18 n keys
  */
 private Map<String, Object> getStaticI18nKeys(I18n i18n) {
  Map<String, Object> staticMap = new LinkedHashMap<String, Object>();
  staticMap.put(getI18nLabelFromKey(DIAGNOSTIC_TOOL_NEXT_QUESTION_CTA_LABEL), i18n.get(DIAGNOSTIC_TOOL_NEXT_QUESTION_CTA_LABEL));
  staticMap.put(getI18nLabelFromKey(DIAGNOSTIC_TOOL_PREVIOUS_QUESTION_CTA_LABEL), i18n.get(DIAGNOSTIC_TOOL_PREVIOUS_QUESTION_CTA_LABEL));
  staticMap.put(getI18nLabelFromKey(DIAGNOSTIC_TOOL_FINISH_QUESTIONNAIRE_CTA_LABEL), i18n.get(DIAGNOSTIC_TOOL_FINISH_QUESTIONNAIRE_CTA_LABEL));
  staticMap.put(getI18nLabelFromKey(DIAGNOSTIC_TOOL_RESTART_QUESTIONNAIRE_CTA_LABEL), i18n.get(DIAGNOSTIC_TOOL_RESTART_QUESTIONNAIRE_CTA_LABEL));
  staticMap.put(getI18nLabelFromKey(DIAGNOSTIC_TOOL_EXIT_QUESTIONNAIRE_CTA_LABEL), i18n.get(DIAGNOSTIC_TOOL_EXIT_QUESTIONNAIRE_CTA_LABEL));
  staticMap.put(getI18nLabelFromKey(DIAGNOSTIC_TOOL_BUY_IT_NOW_CTA_LABEL), i18n.get(DIAGNOSTIC_TOOL_BUY_IT_NOW_CTA_LABEL));
  staticMap.put(getI18nLabelFromKey(DIAGNOSTIC_TOOL_READ_ARTICLE_CTA_LABEL), i18n.get(DIAGNOSTIC_TOOL_READ_ARTICLE_CTA_LABEL));
  staticMap.put(getI18nLabelFromKey(DIAGNOSTIC_TOOL_SEPARATION_TEXT), i18n.get(DIAGNOSTIC_TOOL_SEPARATION_TEXT));
  staticMap.put(getI18nLabelFromKey(DIAGNOSTIC_TOOL_EMAIL_FORM_PLACEHOLDER_TEXT), i18n.get(DIAGNOSTIC_TOOL_EMAIL_FORM_PLACEHOLDER_TEXT));
  staticMap.put(getI18nLabelFromKey(DIAGNOSTIC_TOOL_EMAIL_FORM_CTA_LABEL), i18n.get(DIAGNOSTIC_TOOL_EMAIL_FORM_CTA_LABEL));
  staticMap.put(getI18nLabelFromKey(DIAGNOSTIC_TOOL_PAGE_XOF_Y_TEXT), i18n.get(DIAGNOSTIC_TOOL_PAGE_XOF_Y_TEXT));
  return staticMap;
 }
 
 /**
  * returns json attribute for i18n key.
  * 
  * @param i18nKey
  *         the i 18 n key
  * @return the i 18 n label from key
  */
 private String getI18nLabelFromKey(String i18nKey) {
  String[] splitI18nKey = i18nKey.split(SPLIT_BY_DOT);
  return splitI18nKey[CommonConstants.ONE];
 }
 
 /**
  * put response data nad post data urls.
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param currentResource
  *         the current resource
  * @param currentPage
  *         the current page
  * @param jsonNameSpace
  *         the json name space
  * @param diagnosticToolMap
  *         the diagnostic tool map
  */
 private void setAndPostResponseUrls(ResourceResolver resourceResolver, Resource currentResource, Page currentPage, String jsonNameSpace,
   Map<String, Object> diagnosticToolMap) {
  String resPath = currentResource.getPath();
  String getResponseUrl = resourceResolver.map(resPath + GET_RESPONSE_DATA + DOT_JSON);
  String getPostUrlFromConfig = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, jsonNameSpace, POST_URL);
  String postResponseUrl = resourceResolver.map(getPostUrlFromConfig);
  diagnosticToolMap.put(GET_RESPONSE_URL, getResponseUrl);
  diagnosticToolMap.put(POST_RESPONSE_URL, postResponseUrl);
  diagnosticToolMap.put("requestParamName", NameConstants.NN_PARAMS);
 }
 
 /**
  * gets the selected questions and place them in a map.
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param currentResource
  *         the current resource
  * @param currentPage
  *         the current page
  * @param slingHttpServletRequest
  *         the sling http servlet request
  * @param diagnosticToolMap
  *         the diagnostic tool map
  * @param indicatorType
  *         the indicator type
  * @param i18Key
  *         the i 18 key
  */
 private void setQuestions(ResourceResolver resourceResolver, Resource currentResource, Page currentPage,
   SlingHttpServletRequest slingHttpServletRequest, Map<String, Object> diagnosticToolMap, String indicatorType, String i18Key) {
  LOGGER.info("entering into DiagnosticToolResultSectionViewHelperImpl to get questions");
  List<Map<String, String>> questionsList = ComponentUtil.getNestedMultiFieldProperties(currentResource, QUESTIONS);
  List<Map<String, Object>> questionSummaryFinalList = diagnosticToolResultSectionViewHelperImpl.prepareQuestionsList(resourceResolver, currentPage,
    slingHttpServletRequest, questionsList, QUESTION_PATH);
  if ("xOfy".equals(indicatorType)) {
   int count = 0;
   for (Map<String, Object> question : questionSummaryFinalList) {
    question.put("stepIndicator", String.valueOf(count + 1 + " " + i18Key + " " + questionSummaryFinalList.size()));
    count++;
   }
  } else if ("percent".equals(indicatorType)) {
   float count = 0;
   for (Map<String, Object> question : questionSummaryFinalList) {
    float percent = ((count + 1) / questionSummaryFinalList.size()) * HUNDRED;
    question.put("stepIndicator", String.valueOf(percent).split(SPLIT_BY_DOT)[0] + "%");
    count++;
   }
  }
  diagnosticToolMap.put("totalCount", questionSummaryFinalList.size());
  LOGGER.debug("List processed for questions list and size of the list is " + questionSummaryFinalList.size());
  diagnosticToolMap.put(QUESTIONS, questionSummaryFinalList.toArray());
 }
 
 /**
  * returns introduction panel details.
  * 
  * @param currentPage
  *         the current page
  * @param slingHttpServletRequest
  *         the sling http servlet request
  * @param componentProperties
  *         the component properties
  * @param diagnosticToolMap
  *         the diagnostic tool map
  */
 private void setIntroPanelMap(Page currentPage, SlingHttpServletRequest slingHttpServletRequest, Map<String, Object> componentProperties,
   Map<String, Object> diagnosticToolMap) {
  boolean includeIntroPanel = MapUtils.getBooleanValue(componentProperties, INCLUDE_INTRO_PANEL, false);
  Map<String, Object> introPanelMap = null;
  if (includeIntroPanel) {
   introPanelMap = new LinkedHashMap<String, Object>();
   String introPanelHeadingText = MapUtils.getString(componentProperties, INTRO_PANEL_HEADING_TEXT, StringUtils.EMPTY);
   String introPanelShortSubheadingText = MapUtils.getString(componentProperties, INTRO_PANEL_SHORT_SUBHEADING_TEXT, StringUtils.EMPTY);
   String introPanelLongSubheadingText = MapUtils.getString(componentProperties, INTRO_PANEL_LONG_SUBHEADING_TEXT, StringUtils.EMPTY);
   String introPanelBackgroundImage = MapUtils.getString(componentProperties, INTRO_PANEL_BACKGROUND_IMAGE, StringUtils.EMPTY);
   String introPanelBackgroundImageAltText = MapUtils.getString(componentProperties, INTRO_PANEL_BACKGROUND_IMAGE_ALT_TEXT,
     StringUtils.EMPTY);
   Map<String, Object> introPanelBackgroundImageMap = getImageProperties(currentPage, slingHttpServletRequest, introPanelBackgroundImage,
     introPanelBackgroundImageAltText);
   String introPanelForegroundImage = MapUtils.getString(componentProperties, INTRO_PANEL_FOREGROUND_IMAGE, StringUtils.EMPTY);
   String introPanelForegroundImageAltText = MapUtils.getString(componentProperties, INTRO_PANEL_FOREGROUND_IMAGE_ALT_TEXT, StringUtils.EMPTY);
   Map<String, Object> introPanelForegroundImageMap = getImageProperties(currentPage, slingHttpServletRequest, introPanelForegroundImage,
     introPanelForegroundImageAltText);
   String introPanelStartCtaLabel = MapUtils.getString(componentProperties, INTRO_PANEL_START_CTA_LABEL, StringUtils.EMPTY);
   introPanelMap.put(UnileverConstants.HEADING, introPanelHeadingText);
   introPanelMap.put(UnileverConstants.SHORT_SUB_HEADING, introPanelShortSubheadingText);
   introPanelMap.put(UnileverConstants.LONG_SUB_HEADING, introPanelLongSubheadingText);
   introPanelMap.put(UnileverConstants.BACKGROUND_IMAGE, introPanelBackgroundImageMap);
   introPanelMap.put(UnileverConstants.FOREGROUND_IMAGE, introPanelForegroundImageMap);
   introPanelMap.put(UnileverConstants.CTA_LABEL, introPanelStartCtaLabel);
   LOGGER.debug("processed introduction panel details in a map and size of map is " + introPanelMap.size());
  }
  diagnosticToolMap.put(INTRODUCTION_PANEL, introPanelMap);
 }
 
 /**
  * This method is used to get image properties like path,alt text for image extension etc.
  * 
  * @param currentPage
  *         the current page
  * @param slingHttpServletRequest
  *         the sling http servlet request
  * @param fileReference
  *         the file reference
  * @param altImage
  *         the alt image
  * @return the image properties
  */
 
 public Map<String, Object> getImageProperties(Page currentPage, SlingHttpServletRequest slingHttpServletRequest, String fileReference,
   String altImage) {
  Map<String, Object> imageMap = new LinkedHashMap<String, Object>();
  Image image = new Image(fileReference, altImage, altImage, currentPage, slingHttpServletRequest);
  imageMap = image.convertToMap();
  return imageMap;
  
 }
 
 /**
  * returns general configuration tab data.
  * 
  * @param userText
  *         the user text
  * @param indicatorType
  *         the indicator type
  * @return the general config map data
  */
 private Map<String, Object> getGeneralConfigMapData(String userText, String indicatorType) {
  Map<String, Object> generalConfigMap = new LinkedHashMap<String, Object>();
  generalConfigMap.put(RETURN_USER_TEXT, userText);
  generalConfigMap.put(PROGRESS_INDICATOR, indicatorType);
  return generalConfigMap;
 }
 
 /**
  * returns product summary details.
  * 
  * @param currentPage
  *         the current page
  * @param slingHttpServletRequest
  *         the sling http servlet request
  * @param componentProperties
  *         the component properties
  * @return the product summary data
  */
 private Map<String, Object> getProductSummaryData(Page currentPage, SlingHttpServletRequest slingHttpServletRequest,
   Map<String, Object> componentProperties) {
  Map<String, Object> productSummaryMap = new LinkedHashMap<String, Object>();
  String productSummaryHeadingText = MapUtils.getString(componentProperties, PRODUCT_SUMMARY_HEADING_TEXT, StringUtils.EMPTY);
  String productSummaryShortSubheadingText = MapUtils.getString(componentProperties, PRODUCT_SUMMARY_SHORT_SUBHEADING_TEXT, StringUtils.EMPTY);
  String productSummaryLongSubheadingText = MapUtils.getString(componentProperties, PRODUCT_SUMMARY_LONG_SUBHEADING_TEXT, StringUtils.EMPTY);
  String productSummaryBackgroundImage = MapUtils.getString(componentProperties, PRODUCT_SUMMARY_BACKGROUND_IMAGE, StringUtils.EMPTY);
  String productSummaryBackgroundImgAltText = MapUtils.getString(componentProperties, PRODUCT_SUMMARY_BACKGROUND_IMAGE_ALT_TEXT, StringUtils.EMPTY);
  Map<String, Object> productSummaryBackgroundImageMap = getImageProperties(currentPage, slingHttpServletRequest, productSummaryBackgroundImage,
    productSummaryBackgroundImgAltText);
  productSummaryMap.put(UnileverConstants.HEADING, productSummaryHeadingText);
  productSummaryMap.put(UnileverConstants.SHORT_SUB_HEADING, productSummaryShortSubheadingText);
  productSummaryMap.put(UnileverConstants.LONG_SUB_HEADING, productSummaryLongSubheadingText);
  productSummaryMap.put(UnileverConstants.BACKGROUND_IMAGE, productSummaryBackgroundImageMap);
  LOGGER.debug("processed product summary details in a map and size of map is " + productSummaryMap.size());
  return productSummaryMap;
 }
}
