/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.cache.ComponentViewAndViewHelperCache;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.utils.CollectionUtils;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.template.dto.Image;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * <p>
 * Responsible for reading author input for body copy steps component and generating a list of copy sections setting in pageContext for the view.
 * Input is manual
 * </p>
 */
@Component(description = "OrderedListViewHelperImpl", immediate = true, metatype = true, label = "OrderedListViewHelperImpl")
@Service(value = { OrderedListViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.ORDERED_LIST, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class OrderedListViewHelperImpl extends BaseViewHelper {
 
 /** The Constant LIST_ITEMS. */
 private static final String LIST_ITEMS = "listItems";
 
 /** The Constant IS_IMAGE_REQUIRED. */
 private static final String IS_IMAGE_REQUIRED = "isImageRequired";
 
 /** The Constant IMAGE_MAP. */
 private static final String IMAGE_MAP = "image";
 
 /** The Constant ITEM_COPY. */
 private static final String ITEM_COPY = "itemCopy";
 
 /** The Constant ITEM_HEADING. */
 private static final String ITEM_HEADING = "itemHeading";
 
 /** The Constant ITEM_NUMBER. */
 private static final String ITEM_NUMBER = "itemNumber";
 /**
  * Constant for retrieving multifield body copy headline.
  */
 private static final String HEADLINE = "copyStepHeadline";
 /**
  * Constant for retrieving multifield body copy description.
  */
 private static final String DESCRIPTION = "copyStepDescription";
 
 /** Constant for step label. */
 private static final String ORDERED_LIST_STEP = "step";
 
 /** Constant for description tips image. */
 private static final String TIPS_IMAGE_PATH = IMAGE_MAP;
 
 /** Constant for description tips alt image text. */
 private static final String TIPS_IMAGE_ALT = "altImage";
 
 /** Constant for description IS_IMAGE_NEEDED. */
 private static final String IS_IMAGE_NEEDED = IS_IMAGE_REQUIRED;
 
 /** The Constant SHORT_SUBHEADING_TEXT. */
 private static final String SHORT_SUBHEADING_TEXT = "shortSubheadingText";
 
 /** The Constant LONG_SUBHEADING_TEXT. */
 private static final String LONG_SUBHEADING_TEXT = "longSubheadingText";
 
 /** The Constant HEADING_TEXT. */
 private static final String HEADING_TEXT = "headingText";
 
 /**
  * logger object.
  **/
 private static final Logger LOGGER = LoggerFactory.getLogger(OrderedListViewHelperImpl.class);
 
 /** The component view and view helper cache. */
 @Reference
 ComponentViewAndViewHelperCache componentViewAndViewHelperCache;
 
 /**
  * The method processData overrides method is responsible validating author input and returning a list of sections containing rich text,text This
  * method converts the multiwidget input (a String Array) into sections of maps so that it can be accessed in JSP in consistent way.This method also
  * validates the link url provided by author.
  * 
  * @param content
  *         , - {@link Map} of all user input received as input <code>ViewHelper.onGetData</code> method
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return {@link Map} holding the data
  */
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  
  LOGGER.info("OrderedListViewHelperImpl #processData called for processing fixed list.");
  Map<String, Object> orderedListProperties = getProperties(content);
  
  Map<String, Object> orderedListFinalMap = new LinkedHashMap<String, Object>();
  
  Page page = getCurrentPage(resources);
  
  List<Map<String, Object>> orderedListFinalList = new ArrayList<Map<String, Object>>();
  Boolean isImageRequired = MapUtils.getBoolean(orderedListProperties, IS_IMAGE_NEEDED, false);
  
  // checking ordered list entry
  List<String> propertyNames = getOrderedListComponentProperties(orderedListProperties);
  List<Map<String, String>> bodyCopySectionsList = CollectionUtils.convertMultiWidgetToList(orderedListProperties, propertyNames);
  
  // preparing list of steps with headline and description
  orderedListFinalList = prepareSectionList(bodyCopySectionsList, page, isImageRequired, getSlingRequest(resources));
  
  LOGGER.debug("The sections list being passed to the JSP after processing is :- " + orderedListFinalList);
  
  Map<String, Object> orderedListPropertyMap = new LinkedHashMap<String, Object>();
  orderedListPropertyMap.put(IS_IMAGE_REQUIRED, isImageRequired);
  orderedListPropertyMap.put(LIST_ITEMS, orderedListFinalList.toArray());
  orderedListPropertyMap.put(HEADING_TEXT, MapUtils.getString(orderedListProperties, HEADING_TEXT, StringUtils.EMPTY));
  orderedListPropertyMap.put(SHORT_SUBHEADING_TEXT, MapUtils.getString(orderedListProperties, SHORT_SUBHEADING_TEXT, StringUtils.EMPTY));
  orderedListPropertyMap.put(LONG_SUBHEADING_TEXT, MapUtils.getString(orderedListProperties, LONG_SUBHEADING_TEXT, StringUtils.EMPTY));
  
  orderedListFinalMap.put(getJsonNameSpace(content), orderedListPropertyMap);
  return orderedListFinalMap;
 }
 
 /**
  * Gets the ordered list component properties.
  * 
  * @param orderedListProperties
  *         the ordered list properties
  * @return the ordered list component properties
  */
 private List<String> getOrderedListComponentProperties(Map<String, Object> orderedListProperties) {
  List<String> propertyNames = new ArrayList<String>();
  propertyNames.add(ORDERED_LIST_STEP);
  propertyNames.add(HEADLINE);
  propertyNames.add(DESCRIPTION);
  propertyNames.add(TIPS_IMAGE_PATH);
  propertyNames.add(TIPS_IMAGE_ALT);
  
  LOGGER.debug("Ordered List component properties : " + orderedListProperties);
  return propertyNames;
 }
 
 /**
  * Prepare section list.
  * 
  * @param sectionsList
  *         the sections list
  * @param page
  *         the page
  * @param isImageRequired
  *         the is image required
  * @param slingHttpServletRequest
  * @return the list
  */
 private List<Map<String, Object>> prepareSectionList(final List<Map<String, String>> sectionsList, Page page, Boolean isImageRequired,
   SlingHttpServletRequest slingHttpServletRequest) {
  
  List<Map<String, Object>> finalSectionsList = new ArrayList<Map<String, Object>>();
  Map<String, Object> finalSectionsMap = null;
  for (Map<String, String> sectionMap : sectionsList) {
   String orderedListStep = sectionMap.get(ORDERED_LIST_STEP);
   String orderedListHeading = sectionMap.get(HEADLINE);
   orderedListHeading = trimUnwantedTags(orderedListHeading);
   String orderedListDescription = sectionMap.get(DESCRIPTION);
   orderedListDescription = trimUnwantedTags(orderedListDescription);
   String tipsimagepath = sectionMap.get(TIPS_IMAGE_PATH);
   String altImage = sectionMap.get(TIPS_IMAGE_ALT);
   
   Image image = new Image(tipsimagepath, altImage, StringUtils.EMPTY, page, slingHttpServletRequest);
   
   if (StringUtils.isNotEmpty(orderedListStep)) {
    finalSectionsMap = new LinkedHashMap<String, Object>();
    finalSectionsMap.put(ITEM_NUMBER, orderedListStep);
    if (StringUtils.isNotEmpty(orderedListHeading)) {
     finalSectionsMap.put(ITEM_HEADING, orderedListHeading);
    }
    if (StringUtils.isNotEmpty(orderedListDescription)) {
     finalSectionsMap.put(ITEM_COPY, orderedListDescription);
    }
    if (image != null && isImageRequired) {
     finalSectionsMap.put(IMAGE_MAP, image.convertToMap());
    }
    finalSectionsList.add(finalSectionsMap);
    
   }
  }
  
  return finalSectionsList;
 }
 
 /**
  * This method removes the unwanted p tags in case multifield has been kept empty.
  * 
  * @param orderedListText
  *         the string ordered list text.
  * @return trimmed ordered list text.
  */
 private String trimUnwantedTags(String orderedListText) {
  String orderedListTextTrimmed = orderedListText;
  Document doc = Jsoup.parse(orderedListTextTrimmed);
  Elements content = doc.getElementsByTag("p");
  boolean isContentEmpty = content.text().isEmpty();
  if (isContentEmpty) {
   orderedListTextTrimmed = StringUtils.EMPTY;
  }
  return orderedListTextTrimmed;
 }
 
}
