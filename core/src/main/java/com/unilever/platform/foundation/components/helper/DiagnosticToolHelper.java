/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.components.helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;

import com.adobe.cq.commerce.common.CommerceHelper;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.constants.SearchConstants;
import com.unilever.platform.aem.foundation.core.dto.SearchDTO;
import com.unilever.platform.aem.foundation.core.dto.TaggingSearchCriteria;
import com.unilever.platform.aem.foundation.core.helper.PageComparator;
import com.unilever.platform.aem.foundation.core.service.SearchParam;
import com.unilever.platform.aem.foundation.core.service.SearchService;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.Article;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * The Class DiagnosticToolHelper.
 */
public class DiagnosticToolHelper {
 private static final String PRODUCT = "Product";

/**
  * Instantiates a new diagnostic tool helper.
  */
 private DiagnosticToolHelper() {
  
 }
 /**
  * Gets the articles.
  * 
  * @param searchService
  *         the search service
  * @param tagIds
  *         the tag ids
  * @param currentPage
  *         the current page
  * @param httpServletRequest
  *         the http servlet request
  * @param articleProp
  * @param searchLimit
  * @return the articles
  */
 public static List<Map<String, Object>> getArticles(Map<String, String> articleProp, SearchService searchService, List<String> tagIds,
   Page currentPage, SlingHttpServletRequest httpServletRequest, int searchLimit) {
  List<Map<String, Object>> articleDataList = new ArrayList<Map<String, Object>>();
  ResourceResolver resourceResolver = httpServletRequest.getResourceResolver();
  int limit = MapUtils.getIntValue(articleProp, "numberOfArticlesToDisplay", 0);
  List<String> pagePathList = getDataByTags(searchService, tagIds, "Article", articleProp, searchLimit, currentPage.getPath(),
    resourceResolver);
  Collections.sort(pagePathList, new PageComparator(tagIds, null, false, currentPage.getPageManager()));
  PageManager pageManager = currentPage.getPageManager();
  Iterator<String> itr = pagePathList.iterator();
  while (itr.hasNext()) {
   String articlePagePath = itr.next();
   Page articlePage = pageManager.getPage(articlePagePath);
   Map<String, Object> dataMap = new HashMap<String, Object>();
   Article article = new Article();
   article.setData(articlePage, httpServletRequest);
   dataMap.put("title", article.getTitle());
   dataMap.put("url", article.getUrl());
   dataMap.put("copyText", article.getCopyText());
   dataMap.put("longCopy", article.getLongCopy());
   dataMap.put("image", article.getImage());
   dataMap.put("author", article.getAuthorName());
   dataMap.put("publishDate", article.getFormattedDate());
   articleDataList.add(dataMap);
  }
    
  return articleDataList.size() > limit ? articleDataList.subList(0, limit) : articleDataList;
 }
 
 /**
  * Gets the products.
  * 
  * @param productProp
  * 
  * @param searchService
  *         the search service
  * @param tagIds
  *         the tag ids
  * @param currentPage
  *         the current page
  * @param httpServletRequest
  *         the http servlet request
  * @param limit
  *         the limit
  * @param searchLimit
  *         the searchLimit
  * @return the products
  */
 public static List<Map<String, Object>> getProducts(Map<String, String> productProp, SearchService searchService, List<String> tagIds,
   int searchLimit, Page currentPage, SlingHttpServletRequest httpServletRequest) {
  
  List<Map<String, Object>> productDataList = new ArrayList<Map<String, Object>>();
  int limit = MapUtils.getIntValue(productProp, "numberOfProductsToDisplay", 0);
  ResourceResolver resourceResolver = httpServletRequest.getResourceResolver();
  List<String> pagePathList = getDataByTags(searchService, tagIds, PRODUCT, productProp, searchLimit, currentPage.getPath(),
    resourceResolver);
  Collections.sort(pagePathList, new PageComparator(tagIds, null, false, currentPage.getPageManager()));
  pagePathList = ProductBundlingToBinUtil.getProductsListForMultipleBuy(pagePathList, currentPage, currentPage.adaptTo(ConfigurationService.class));
  Iterator<String> itr = pagePathList.iterator();
  PageManager pageManager = currentPage.getPageManager();
  while (itr.hasNext()) {
   String productPagePath = itr.next();
   Page productPage = pageManager.getPage(productPagePath);
   Map<String, Object> productData = ProductHelperExt.getProductTeaserData(productPage, currentPage, httpServletRequest);
   
   Map<String, Object> ratingsMap = ProductHelper.getProductRatingsMap((UNIProduct) CommerceHelper.findCurrentProduct(productPage), currentPage,
     currentPage.adaptTo(ConfigurationService.class), "diagnosticTool");
   productData.put(ProductConstants.PRODUCT_RATINGS_MAP, ratingsMap);
   
   productData.remove("image");
   productDataList.add(productData);
  }
    
  return productDataList.size() > limit ? productDataList.subList(0, limit) : productDataList;
  
 }
 
 /**
  * Gets the data by tags.
  * 
  * @param searchService
  *         the search service
  * @param tagIds
  *         the tag ids
  * @param contentType
  *         the content type
  * @param prop
  *         the prop
  * @param limit
  *         the limit
  * @param currentPath
  *         the current path
  * @param resourceResolver
  *         the resource resolver
  * @return the data by tags
  */
 public static List<String> getDataByTags(SearchService searchService, List<String> tagIds, String contentType, Map<String, String> prop,
    int limit, String currentPath, ResourceResolver resourceResolver) {
  String incTags = PRODUCT.equals(contentType)?"productInclusionTags":"articleInclusionTags";
  String excTags = PRODUCT.equals(contentType)?"productExclusionTags":"articleExclusionTags";
  String inclusionTags = MapUtils.getString(prop, incTags, "");
  String exclusionTags = MapUtils.getString(prop, excTags, "");
  List<String> pagePathList = new ArrayList<String>();
  List<TaggingSearchCriteria> criteriaList = new ArrayList<TaggingSearchCriteria>();
  TaggingSearchCriteria orCriteria = new TaggingSearchCriteria();
  orCriteria.setOperator(SearchConstants.SEARCH_OR);
  orCriteria.setTagList(tagIds);
  criteriaList.add(orCriteria);
  
  if (StringUtils.isNotBlank(inclusionTags)) {
   TaggingSearchCriteria andCriteria = new TaggingSearchCriteria();
   andCriteria.setOperator(SearchConstants.SEARCH_AND);
   andCriteria.setTagList(ComponentUtil.getListFromStringArray(inclusionTags.split(",")));
   criteriaList.add(andCriteria);
  }
  
  Map<String, List<String>> propertyMap = new HashMap<String, List<String>>();
  propertyMap.put(UnileverConstants.JCR_CONTENT_CONTENT_TYPE, ComponentUtil.getListFromStringArray(contentType.split(",")));
  
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  List<SearchDTO> results = searchService.getMultiTagResults(criteriaList, ComponentUtil.getListFromStringArray(exclusionTags.split(",")),
    currentPath, propertyMap, new SearchParam(0, limit, UnileverConstants.DESCENDING), resourceResolver);
  Iterator<SearchDTO> itr = results.iterator();
  while (itr.hasNext()) {
   SearchDTO result = itr.next();
   String path = result.getResultNodePath();
   Page page = pageManager.getPage(path);
   if (page != null) {
    pagePathList.add(path);
   }
  }
  
  return pagePathList;
 }
 
 /**
  * Gets the overridden config map.
  * 
  * @param configMap
  *         the config map
  * @param valueMap
  *         the value map
  * @param overrideConfig
  *         the overrideConfig
  * @return the overridden config map
  */
 public static Map<String, Object> getOverriddenConfigMap(Map<String, String> configMap, Map<String, Object> valueMap, boolean overrideConfig) {
  
  Iterator<String> configItr = configMap.keySet().iterator();
  Map<String, Object> finalConfigMap = valueMap;
  if(!overrideConfig){
   finalConfigMap = new LinkedHashMap<String, Object>();
   while (configItr.hasNext()) {
    String key = configItr.next();
    String overridenValue = MapUtils.getString(configMap, key, StringUtils.EMPTY);
    finalConfigMap.put(key, overridenValue);
  }
  }
  return finalConfigMap;
 }
}
