/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.components.helper;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;
import com.unilever.platform.foundation.viewhelper.constants.StoreLocatorConstants;

/**
 * The Class StoreLocatorUtility.
 */
public final class StoreLocatorUtility {
 
 private static final String UK_POSTAL_CODE = "ukPostalCode";
 private static final String POSTAL_CODE = "postalCode";
 private static final String REGEX = "regex";
 private static final String RESET_FILTER_CTA = "resetFilterCta";
 private static final String ERROR_MESSAGE = "errorMessage";
 private static final String GO_CTA = "goCta";
 private static final String GPS_SHARE_LOCATION = "gpsShareLocation";
 private static final String POSTAL_CODE_FIELD_HELP_TEXT = "postalCodeFieldHelpText";
 private static final String BUY_IN_STORE = "buyInStore";
 private static final String STORE_SEARCH_HEADING_TEXT = "storeSearch.headingText";
 private static final String STORE_SEARCH_POSTCODE_FIELD_HELP_TEXT = "storeSearch.postcodeFieldHelpText";
 private static final String STORE_SEARCH_GPS_SHARE_LOCATION_CTA_LABEL = "storeSearch.gpsShareLocationCtaLabel";
 private static final String STORE_SEARCH_CTA_LABEL = "storeSearch.ctaLabel";
 private static final String CTA_LABELS = "ctaLabels";
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(StoreLocatorUtility.class);
 
 /**
  * Instantiates a new store locator utility.
  */
 private StoreLocatorUtility() {
  LOGGER.info("Store Locator Constructor Called.");
 }
 
 /**
  * Checks if is store locator enabled.
  * 
  * @param productPage
  *         the product page
  * @param configurationService
  *         the configuration service
  * @param componentName
  *         the component name
  * @return the boolean
  */
 public static Boolean isStoreLocatorEnabled(Page productPage, ConfigurationService configurationService, String componentName) {
  Boolean flag = false;
  
  String storeLocatorEnabled = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, productPage,
    StoreLocatorConstants.GLOBAL_CONFIG_CAT, StoreLocatorConstants.STORE_LOCATOR_ENABLED);
  String componentList = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, productPage,
    StoreLocatorConstants.GLOBAL_CONFIG_CAT, StoreLocatorConstants.IS_ENABLED_COMPONENT_LIST);
  String[] componentArray = StringUtils.split(componentList, ",");
  if (storeLocatorEnabled.equalsIgnoreCase(StoreLocatorConstants.TRUE)) {
   for (String obj : componentArray) {
    if (obj.equalsIgnoreCase(componentName)) {
     flag = true;
    }
   }
  }
  return flag;
 }
 
 /**
  * Gets the store locator map v2.
  * 
  * @param i18n
  *         the i18n
  * @param resources
  *         the resources
  * @param configurationService
  *         the configuration service
  * @param componentName
  *         the componentName
  * @return store locator configuration map for v2
  */
 public static Map<String, Object> getStoreLocatorMapV2(I18n i18n, Map<String, Object> resources, ConfigurationService configurationService,
   String componentName) {
  Map<String, Object> storeLocatorConfigurationMap = StoreLocatorUtility.getStoreLocatorMap(BaseViewHelper.getCurrentPage(resources), null,
    configurationService, BaseViewHelper.getSlingRequest(resources));
  storeLocatorConfigurationMap.put(StoreLocatorConstants.STORE_LOCATOR_ENABLED,
    isStoreLocatorEnabled(BaseViewHelper.getCurrentPage(resources), configurationService, componentName));
  Map<String, String> staticMap = new LinkedHashMap<String, String>();
  staticMap.put(BUY_IN_STORE, i18n.get(STORE_SEARCH_HEADING_TEXT));
  staticMap.put(POSTAL_CODE_FIELD_HELP_TEXT, i18n.get(STORE_SEARCH_POSTCODE_FIELD_HELP_TEXT));
  staticMap.put(GPS_SHARE_LOCATION, i18n.get(STORE_SEARCH_GPS_SHARE_LOCATION_CTA_LABEL));
  staticMap.put(GO_CTA, i18n.get(STORE_SEARCH_CTA_LABEL));
  staticMap.put(ERROR_MESSAGE, i18n.get("storeLocator.noResultsMessage"));
  staticMap.put(RESET_FILTER_CTA, i18n.get("storeLocator.resetFilterCta"));
  storeLocatorConfigurationMap.put(CTA_LABELS, staticMap);
  return storeLocatorConfigurationMap;
 }
 
 /**
  * Gets the store locator map.
  * 
  * @param resources
  *         the resources
  * @param configurationService
  *         the configuration service
  * @param i18n
  *         the i18n
  * @param componentName
  *         the component name
  * @return the store locator map
  */
 public static Map<String, Object> getStoreLocatorMap(Map<String, Object> resources, ConfigurationService configurationService,
   String componentName) {
  Map<String, Object> storeLocatorMap = new HashMap<String, Object>();
  
  I18n i18n = BaseViewHelper.getI18n(resources);
  ResourceResolver resourceResolver = (ResourceResolver) resources.get(ProductConstants.RESOURCE_RESOLVER);
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) resources.get(ProductConstants.REQUEST);
  Resource compResource = slingRequest.getResource();
  Page currentPage = pageManager.getContainingPage(compResource);
  
  storeLocatorMap.put(StoreLocatorConstants.IS_LOCATION_BASED_SEARCH_ENABLED, GlobalConfigurationUtility.getValueFromConfiguration(
    configurationService, currentPage, StoreLocatorConstants.GLOBAL_CONFIG_CAT, StoreLocatorConstants.IS_LOCATION_BASED_SEARCH_ENABLED));
  storeLocatorMap.put(StoreLocatorConstants.BUY_IN_STORE_CTA, i18n.get(StoreLocatorConstants.STORE_LOCATOR_BUY_IN_STORE_CTA));
  storeLocatorMap.put(StoreLocatorConstants.DEFAULT_RADIOUS_FOR_SEARCH, GlobalConfigurationUtility.getValueFromConfiguration(configurationService,
    BaseViewHelper.getCurrentPage(resources), StoreLocatorConstants.GLOBAL_CONFIG_CAT, StoreLocatorConstants.DEFAULT_RADIOUS_FOR_SEARCH));
  
  storeLocatorMap.put(StoreLocatorConstants.FIELDS, getFieldsMap(currentPage, configurationService, i18n, componentName));
  
  ValueMap valueMap = compResource.getValueMap();
  
  String compName = (String) valueMap.getOrDefault("componentName", "");
  
  String storeLocatorTitleKey = compName + "." + "storeLocatorTitle";
  
  String storeLocatorTitle = i18n.get(storeLocatorTitleKey);
  storeLocatorMap.put("storeLocatorTitle", storeLocatorTitle);
  
  return storeLocatorMap;
 }
 
 /**
  * Gets the fields map.
  * 
  * @param currentPage
  *         the current page
  * @param configurationService
  *         the configuration service
  * @param i18n
  *         the i18n
  * @param componentName
  *         the component name
  * @return the fields map
  */
 public static Map<String, Object> getFieldsMap(Page currentPage, ConfigurationService configurationService, I18n i18n, String componentName) {
  Map<String, Object> fieldsMap = new HashMap<String, Object>();
  
  Map<String, Object> postCodeMap = new HashMap<String, Object>();
  Map<String, Object> distanceWithinMap = new HashMap<String, Object>();
  Map<String, Object> productVarientMap = new HashMap<String, Object>();
  postCodeMap.put(ProductConstants.LABEL, i18n.get(StoreLocatorConstants.STORE_LOCATOR_POST_CODE_LABEL));
  postCodeMap.put(StoreLocatorConstants.DEFAULT_VALUE, i18n.get(StoreLocatorConstants.STORE_LOCATOR_POST_CODE_DEFAULT_VALUE));
  postCodeMap.put(StoreLocatorConstants.ERROR_MSG, i18n.get(StoreLocatorConstants.STORE_LOCATOR_POST_CODE_ERROR_MSG));
  String postalCodeRegEx = getPostalCodeRegex(currentPage, configurationService);
  postCodeMap.put(StoreLocatorConstants.REGEX, postalCodeRegEx);
  distanceWithinMap.put(ProductConstants.LABEL, i18n.get(StoreLocatorConstants.STORE_LOCATOR_DISTANCE_WITHIN_LABEL));
  
  distanceWithinMap.put(StoreLocatorConstants.OPTIONS, getOptions(currentPage, configurationService));
  distanceWithinMap.put(StoreLocatorConstants.OPTIONS_LABEL, i18n.get(StoreLocatorConstants.STORE_LOCATOR_DISTANCE_WITHIN_OPTIONS_LABEL));
  fieldsMap.put(StoreLocatorConstants.POST_CODE, postCodeMap);
  fieldsMap.put(StoreLocatorConstants.POST_CODE_TOOLTIP, i18n.get(StoreLocatorConstants.STORE_LOCATOR_POST_CODE_TOOLTIP));
  fieldsMap.put(StoreLocatorConstants.DISTANCE_WITHIN, distanceWithinMap);
  
  if (!componentName.equalsIgnoreCase(StoreLocatorConstants.STORE_SEARCH_RESULTS)) {
   productVarientMap.put(ProductConstants.LABEL, i18n.get(StoreLocatorConstants.STORE_LOCATOR_PRODUCT_VARIENT_LABEL));
   fieldsMap.put(StoreLocatorConstants.PRODUCT_VARIENT, productVarientMap);
  }
  
  putDefaultRadiusForSearch(currentPage, configurationService, fieldsMap);
  
  return fieldsMap;
 }
 
 /**
  * This method puts defaultRadiusForSearch in field map.
  * 
  * @param currentPage
  *         the current page
  * @param configurationService
  *         the configuration service
  * @param fieldsMap
  *         the field map
  */
 private static void putDefaultRadiusForSearch(Page currentPage, ConfigurationService configurationService, Map<String, Object> fieldsMap) {
  String defaultRadiusForSearch = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
    StoreLocatorConstants.GLOBAL_CONFIG_CAT, StoreLocatorConstants.DEFAULT_RADIOUS_FOR_SEARCH);
  fieldsMap.put(StoreLocatorConstants.DEFAULT_RADIOUS_FOR_SEARCH, defaultRadiusForSearch);
 }
 
 /**
  * Gets the field map
  * 
  * @param i18n
  *         the i18n
  * @param currentPage
  *         the currentPage
  * @param configurationService
  *         the configuration service
  * @return the field map
  */
 public static Map<String, Object> getfieldsMap(Page currentPage, ConfigurationService configurationService, I18n i18n) {
  Map<String, Object> fieldsMap = new HashMap<String, Object>();
  
  Map<String, Object> postCodeMap = new HashMap<String, Object>();
  Map<String, Object> distanceWithinMap = new HashMap<String, Object>();
  postCodeMap.put(StoreLocatorConstants.DEFAULT_VALUE, i18n.get(StoreLocatorConstants.STORE_LOCATOR_POST_CODE_DEFAULT_VALUE));
  postCodeMap.put(StoreLocatorConstants.ERROR_MSG, i18n.get(StoreLocatorConstants.STORE_LOCATOR_POST_CODE_ERROR_MSG));
  String postalCodeRegEx = getPostalCodeRegex(currentPage, configurationService);
  postCodeMap.put(StoreLocatorConstants.REGEX, postalCodeRegEx);
  
  distanceWithinMap.put(StoreLocatorConstants.OPTIONS, getOptions(currentPage, configurationService));
  fieldsMap.put(StoreLocatorConstants.POST_CODE, postCodeMap);
  fieldsMap.put(StoreLocatorConstants.POST_CODE_TOOLTIP, i18n.get(StoreLocatorConstants.STORE_LOCATOR_POST_CODE_TOOLTIP));
  fieldsMap.put(StoreLocatorConstants.DISTANCE_WITHIN, distanceWithinMap);
  putDefaultRadiusForSearch(currentPage, configurationService, fieldsMap);
  
  return fieldsMap;
 }
 
 /**
  * This method returns postal code regex reading it from config.
  * 
  * @param currentPage
  *         the current page
  * @param configurationService
  *         the configuration services
  * @return postal code regex string
  */
 private static String getPostalCodeRegex(Page currentPage, ConfigurationService configurationService) {
  String postalCodeRegEx = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, REGEX, POSTAL_CODE);
  if (StringUtils.isBlank(postalCodeRegEx)) {
   postalCodeRegEx = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, REGEX, UK_POSTAL_CODE);
  }
  return postalCodeRegEx;
 }
 
 /**
  * Gets the options.
  * 
  * @param currentPage
  *         the current page
  * @param configurationService
  *         the configuration service
  * @return the options
  */
 public static Object getOptions(Page currentPage, ConfigurationService configurationService) {
  
  String options = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, StoreLocatorConstants.GLOBAL_CONFIG_CAT,
    StoreLocatorConstants.DISTANCE_WITHIN_OPTIONS);
  String[] optionsToArray = StringUtils.split(options, ",");
  return (Object) optionsToArray;
 }
 
 /**
  * Sets the store locator map.
  * 
  * @param product
  *         the product
  * @param resources
  *         the resources
  * @param configurationService
  *         the configuration service
  * @param currentPage
  *         the currentPage
  * @param slingRequest
  *         the slingRequest
  * @return store locator map
  */
 public static Map<String, Object> getStoreLocatorMap(Page currentPage, UNIProduct product, ConfigurationService configurationService,
   SlingHttpServletRequest slingRequest) {
  Map<String, Object> storeLocatorProperties = new LinkedHashMap<String, Object>();
  boolean isLocationBasedSearchEnabled = false;
  String storeLocatorUrl = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
    StoreLocatorConstants.GLOBAL_CONFIG_CAT, StoreLocatorConstants.CTA_URL);
  String productUpcCode = (product != null) ? product.getShortIdentifierValue() : StringUtils.EMPTY;
  String defaultRadiusForSearch = StringUtils.EMPTY;
  ResourceResolver resourceResolver = currentPage.getContentResource().getResourceResolver();
  String fullCtaUrl = ComponentUtil.getFullURL(resourceResolver, storeLocatorUrl, slingRequest);
  try {
   isLocationBasedSearchEnabled = Boolean.parseBoolean(configurationService.getConfigValue(currentPage, StoreLocatorConstants.GLOBAL_CONFIG_CAT,
     StoreLocatorConstants.IS_LOCATION_BASED_SEARCH_ENABLED));
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find isLocationBasedSearchEnabled key in storeLocator category", e);
  }
  defaultRadiusForSearch = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
    StoreLocatorConstants.GLOBAL_CONFIG_CAT, StoreLocatorConstants.DEFAULT_RADIOUS_FOR_SEARCH);
  String postalCodeRegEx = getPostalCodeRegex(currentPage, configurationService);
  storeLocatorProperties.put(StoreLocatorConstants.REGEX, postalCodeRegEx);
  storeLocatorProperties.put(StoreLocatorConstants.IS_LOCATION_BASED_SEARCH_ENABLED, isLocationBasedSearchEnabled);
  storeLocatorProperties.put(UnileverConstants.STORE_LOCATOR_URL, fullCtaUrl);
  storeLocatorProperties.put(UnileverConstants.PRODUCT_UPC_CODE, productUpcCode);
  storeLocatorProperties.put(StoreLocatorConstants.DEFAULT_RADIOUS_FOR_SEARCH, defaultRadiusForSearch);
  return storeLocatorProperties;
 }
}
