/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.jcr.resource.JcrPropertyMap;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * The SiteMapViewHelperImpl class is a view helper that populates the site map .
 * 
 * @author SapientNitro
 */
@Component(description = "SiteMapViewHelperImpl", immediate = true, metatype = true, label = "Site Map ViewHelper")
@Service(value = { SiteMapViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.SITEMAP, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class SiteMapViewHelperImpl extends BaseViewHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(SiteMapViewHelperImpl.class);
 
 /**
  * Instantiates a new primary navigation view helper.
  */
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The Constant SITEMAP_ROOT_PATH. */
 public static final String SITEMAP_ROOT_PATH = "siteMapRootPath";
 /** The Constant SECTIONS. */
 public static final String SECTIONS = "sections";
 /** The Constant HEADLINE. */
 public static final String HEADLINE = "headline";
 /** The Constant CONTENT_TYPE. */
 public static final String CONTENT_TYPE = "contentType";
 /** The Constant SITEMAP_TITLE. */
 public static final String SITEMAP_TITLE = "sitemap.title";
 /** The Constant JCR_CONTENT. */
 public static final String JCR_CONTENT = "/jcr:content";
 /** The Constant ORDER. */
 public static final String ORDER = "order";
 /** The Constant ORDER. */
 public static final String EXCLUDE_CONTENT_TYPES = "excludeContentTypes";
 
 /**
  * Instantiates a new site map view helper impl.
  */
 public SiteMapViewHelperImpl() {
  super();
 }
 
 /**
  * The method read the root path from the dialog and creates the site map .
  * 
  * @param content
  *         {@link Map} of all user input received as input
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return Map holding the result data.
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("inside SiteMapViewHelperImpl view helper");
  Map<String, Object> properties = new HashMap<String, Object>();
  Map<String, Object> siteMapProperties = getProperties(content);
  String rootPath = siteMapProperties.get(SITEMAP_ROOT_PATH).toString();
  Page rootPage = getPageManager(resources).getContainingPage(rootPath);
  
  LOGGER.debug("Creating the site map for path=" + rootPage);
  
  String contentTypeExcludes = "";
  
  try {
   contentTypeExcludes = configurationService.getConfigValue(getCurrentPage(resources), "sitemap", EXCLUDE_CONTENT_TYPES);
   contentTypeExcludes = contentTypeExcludes != null ? contentTypeExcludes : StringUtils.EMPTY;
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find excludeContentTypes key in sitemap category", e);
  }
  
  List<String> contentTypeExcludeList = Arrays.asList(contentTypeExcludes.split(","));
  
  List<PageDataBean> pageDataBeanList = new ArrayList<PageDataBean>();
  if (null != rootPage) {
   
   pageDataBeanList = getChildPages(rootPage, getResourceResolver(resources), contentTypeExcludeList, getSlingRequest(resources));
   LOGGER.debug("Level One Pages  = " + pageDataBeanList);
   prepareSiteMap(pageDataBeanList, resources, contentTypeExcludeList);
   
   properties.put(SECTIONS, pageDataBeanList);
   LOGGER.info("sitemap prepared successfully");
  }
  
  properties.put(HEADLINE, getI18n(resources).get(SITEMAP_TITLE));
  
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE), properties);
  
  return data;
 }
 
 /**
  * Prepare the site Map from the first level list.
  * 
  * @param pageDataBeanList
  *         the page data bean list
  * @param resources
  *         the resources
  * @param contentTypeExcludeList
  *         the content type exclude list
  */
 
 public void prepareSiteMap(List<PageDataBean> pageDataBeanList, Map<String, Object> resources, List<String> contentTypeExcludeList) {
  ResourceResolver resourceResolver = getResourceResolver(resources);
  long startTime = System.currentTimeMillis();
  for (PageDataBean p : pageDataBeanList) {
   
   List<PageDataBean> childList = getChildPages(p.getPage(), resourceResolver, contentTypeExcludeList, getSlingRequest(resources));
   if (childList != null && !childList.isEmpty()) {
    p.setSubsections(childList);
    prepareSiteMap(childList, resources, contentTypeExcludeList);
   }
  }
  
  long stopTime = System.currentTimeMillis();
  
  LOGGER.debug("Time elapsed in preparing the site map  = " + (stopTime - startTime));
  
 }
 
 /**
  * Gets the child pages.
  * 
  * @param page
  *         the page
  * @param resourceResolver
  *         the resource resolver
  * @param contentTypeExcludeList
  *         the content type exclude list
  * @param request
  * @return the child pages
  */
 public List<PageDataBean> getChildPages(Page page, ResourceResolver resourceResolver, List<String> contentTypeExcludeList,
   SlingHttpServletRequest request) {
  List<PageDataBean> pageDataBeanList = new ArrayList<PageDataBean>();
  LOGGER.debug("Fetching the child pages of ", page.getTitle());
  final Iterator<Page> children = page.listChildren();
  Page childPage = null;
  String pageUrl = null;
  
  while (null != children && children.hasNext()) {
   childPage = children.next();
   boolean noIndex = ComponentUtil.isNoIndex(childPage);
   boolean isHideInSiteMap = ComponentUtil.isPageHideInSiteMap(childPage);
   String contentType = getContentType(childPage, resourceResolver);
   contentType = contentType != null ? contentType : StringUtils.EMPTY;
   if (checkContentTypeExcludeList(contentTypeExcludeList, noIndex, isHideInSiteMap, contentType)) {
    PageDataBean pageDataBean = new PageDataBean();
    pageDataBean.setPage(childPage);
    String title = getNavTitle(childPage, resourceResolver);
    pageDataBean.setTitle(title);
    pageUrl = childPage.getPath();
    pageDataBean.setUrl(ComponentUtil.getFullURL(resourceResolver, pageUrl, request));
    pageDataBeanList.add(pageDataBean);
   } else if (childPage.listChildren().hasNext()) {
    List<PageDataBean> childPages = getChildPages(childPage, resourceResolver, contentTypeExcludeList, request);
    if (childPages != null && !childPages.isEmpty()) {
     pageDataBeanList.addAll(childPages);
    }
   }
   LOGGER.debug("child page added");
  }
  LOGGER.info("child pages list created successfully");
  return pageDataBeanList;
 }
 
 /**
  * Check content type exclude list.
  *
  * @param contentTypeExcludeList the content type exclude list
  * @param noIndex the no index
  * @param isHideInSiteMap the is hide in site map
  * @param contentType the content type
  * @return true, if successful
  */
 private boolean checkContentTypeExcludeList(List<String> contentTypeExcludeList, boolean noIndex, boolean isHideInSiteMap, String contentType){
  return contentTypeExcludeList != null && (!contentTypeExcludeList.isEmpty() && !contentTypeExcludeList.contains(contentType)) && (!noIndex && !isHideInSiteMap);
 }
 
 /**
  * Gets the navigation title.
  * 
  * @param page
  *         Page object.
  * @param resourceResolver
  *         the resource resolver
  * @return navTitle String navigation title.
  */
 public String getNavTitle(Page page, ResourceResolver resourceResolver) {
  String navTitle = "";
  
  Resource resource = resourceResolver.getResource(page.getPath() + JCR_CONTENT);
  
  if (null != resource) {
   Node currentNode = resource.adaptTo(Node.class);
   if (null != currentNode) {
    try {
     if (currentNode.hasProperty(NameConstants.PN_NAV_TITLE) && null != currentNode.getProperty(NameConstants.PN_NAV_TITLE)) {
      
      navTitle = new JcrPropertyMap(currentNode).get(NameConstants.PN_NAV_TITLE, StringUtils.EMPTY);
      
     }
    } catch (RepositoryException e) {
     LOGGER.error("could not find nav title", e);
    }
   }
  }
  
  if (StringUtils.isBlank(navTitle)) {
   navTitle = page.getTitle();
  }
  LOGGER.debug("Navigation title " + navTitle);
  return navTitle;
 }
 
 /**
  * Gets the Content Type.
  * 
  * @param page
  *         Page object.
  * @param resourceResolver
  *         the resource resolver
  * @return Content type.
  */
 public String getContentType(Page page, ResourceResolver resourceResolver) {
  String contentType = "";
  
  Resource resource = resourceResolver.getResource(page.getPath() + JCR_CONTENT);
  
  if (null != resource) {
   Node currentNode = resource.adaptTo(Node.class);
   try {
    if (null != currentNode && currentNode.hasProperty(CONTENT_TYPE) && null != currentNode.getProperty(CONTENT_TYPE)) {
     contentType = new JcrPropertyMap(currentNode).get(CONTENT_TYPE, StringUtils.EMPTY);
     
    }
   } catch (RepositoryException e) {
    LOGGER.error("could not find content type for page :" + resource.getPath(), e);
   }
  }
  return contentType;
 }
 
 /**
  * PageDataBean.
  * 
  */
 class PageDataBean {
  /** The page title. */
  private String title;
  
  /** The redirect URL. */
  private String url;
  
  /** The Page object. */
  private Page page;
  
  /** The TreeMap object. */
  private List<PageDataBean> subsections = new ArrayList<PageDataBean>();
  
  /**
   * Getter method.
   * 
   * @return pageTitle
   */
  public String getTitle() {
   return title;
  }
  
  /**
   * Setter method.
   * 
   * @param title
   *         the new title
   */
  public void setTitle(String title) {
   this.title = title;
  }
  
  /**
   * Getter method.
   * 
   * @return pageUrl
   */
  public String getUrl() {
   return url;
  }
  
  /**
   * Setter method.
   * 
   * @param pageUrl
   *         String pageUrl.
   */
  public void setUrl(String pageUrl) {
   this.url = pageUrl;
  }
  
  /**
   * Gets the page.
   * 
   * @return the page
   */
  Page getPage() {
   return page;
  }
  
  /**
   * Sets the page.
   * 
   * @param page
   *         the new page
   */
  void setPage(Page page) {
   this.page = page;
  }
  
  /**
   * Gets the subsections.
   * 
   * @return the subsections
   */
  public List<PageDataBean> getSubsections() {
   return subsections;
  }
  
  /**
   * Sets the subsections.
   * 
   * @param subsections
   *         the new subsections
   */
  public void setSubsections(List<PageDataBean> subsections) {
   this.subsections = subsections;
  }
 }
 
}
