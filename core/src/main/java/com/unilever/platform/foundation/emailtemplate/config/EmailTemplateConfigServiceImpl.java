/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.emailtemplate.config;

import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.PropertyUnbounded;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.commons.osgi.PropertiesUtil;

/**
 * The Class EmailTemplateConfigServiceImpl
 * 
 */
@Component(immediate = true, label = "Email Template Configuration Service", description = "Gets the email templates from configuration", metatype = true)
@Service(value = EmailTemplateConfigService.class)
public class EmailTemplateConfigServiceImpl implements EmailTemplateConfigService {
 
 @Property(unbounded = PropertyUnbounded.ARRAY, description = "add the template type")
 private static final String TEMPLATE_TYPE = "property.email.types";
 
 private String[] templates;
 
 @Activate
 protected void activate(final Map<String, Object> config) {
  this.templates = PropertiesUtil.toStringArray(config.get(TEMPLATE_TYPE), ArrayUtils.EMPTY_STRING_ARRAY);
 }
 
 @Override
 public String[] getEmailTemplates() {
  return this.templates;
 }
 
}
