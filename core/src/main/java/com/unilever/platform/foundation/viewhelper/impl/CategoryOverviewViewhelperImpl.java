/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sapient.platform.iea.aem.core.cache.ComponentViewAndViewHelperCache;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.utils.CollectionUtils;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.CategoryOverviewHelper;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * <p>
 * Responsible for reading author input for category overview component and generating a list of collapsible sections setting in pageContext for the
 * view. Input can be manual or reading property attributes from product data.
 * </p>
 */
@Component(description = "CategoryOverviewViewhelperImpl", immediate = true, metatype = true, label = "CategoryOverviewViewhelperImpl")
@Service(value = { CategoryOverviewViewhelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.CATEGORY_OVERVIEW, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class CategoryOverviewViewhelperImpl extends BaseViewHelper {
 
 /**
  * Identifier for sub heading for each categoryOverview column.
  */
 private static final String SUB_HEADING = "subHeading";
 /**
  * Identifier for heading for category overview component.
  */
 private static final String HEADING = "heading";
 
 /**
  * Identifier for all menu items.
  */
 private static final String LINK_LIST = "linkedList";
 
 /**
  * Identifier for category overview component headline.
  */
 private static final String HEADLINE = "headline";
 /**
  * Identifier for categories list.
  */
 private static final String CATEGORIES_LIST = "categories";
 
 private static final String NAVIGATION_LINKS = "navigationLinks";
 
 /**
  * logger object.
  */
 private static final Logger LOGGER = LoggerFactory.getLogger(CategoryOverviewViewhelperImpl.class);
 
 /** The component view and view helper cache. */
 /*
  * reference for cache object
  */
 @Reference
 ComponentViewAndViewHelperCache componentViewAndViewHelperCache;
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /**
  * The method processData overrides method is responsible validating author input and returning a list of sections containing text, links This method
  * converts the multiwidget input (a String Array) into sections of maps so that it can be accessed in JSP in consistent way.This method also
  * validates the link url provided by author.
  * 
  * @param content
  *         {@link Map} of all user input received as input
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return Map holding the result data.
  **/
 @Override
 @SuppressWarnings("unchecked")
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  LOGGER.debug("Category Overview View Helper #processData called for processing fixed list.");
  SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) resources.get(ProductConstants.REQUEST);
  Resource compResource = slingRequest.getResource();
  
  List<Map<String, Object>> columnList = new ArrayList<Map<String, Object>>();
  Map<String, Object> categoryOverview = new HashMap<String, Object>();
  Map<String, Object> propertiesActual = (Map<String, Object>) content.get(PROPERTIES);
  Map<String, Object> properties = CollectionUtils.getJcrPropertyAsMap(propertiesActual);
  String jsonNameSpace = MapUtils.getString(content, "jsonNameSpace");
  String heading = MapUtils.getString(properties, HEADING);
  Map<String, Object> categoryOverviewLinksMap = new HashMap<String, Object>();
  if (properties.get(CategoryOverviewHelper.MENU_LABEL) instanceof List) {
   properties.put(CategoryOverviewHelper.MENU_LABEL, ((ArrayList<String>) properties.get(CategoryOverviewHelper.MENU_LABEL)).toArray());
  }
  if (properties.get(CategoryOverviewHelper.MENU_LINK) instanceof List) {
   properties.put(CategoryOverviewHelper.MENU_LINK, ((ArrayList<String>) properties.get(CategoryOverviewHelper.MENU_LINK)).toArray());
  }
  if (properties.get(CategoryOverviewHelper.OPEN_IN_NEW_WINDOW) instanceof List) {
   properties.put(CategoryOverviewHelper.OPEN_IN_NEW_WINDOW,
     ((ArrayList<String>) properties.get(CategoryOverviewHelper.OPEN_IN_NEW_WINDOW)).toArray());
  }
  LOGGER.debug("The properties map for Category Overview component is " + properties);
  
  List<String> propertyNames = new ArrayList<String>();
  propertyNames.add(SUB_HEADING);
  
  List<Map<String, String>> primaryList = ComponentUtil.getNestedMultiFieldProperties(compResource, NAVIGATION_LINKS);
  LOGGER.info("The number of category overview list defined is " + primaryList.size());
  for (Map<String, String> map : primaryList) {
   Map<String, Object> columnMap = new LinkedHashMap<String, Object>();
   String subHeading = StringUtils.defaultString(map.get(SUB_HEADING), "");
   columnMap.put(HEADLINE, subHeading);
   List<Map<String, Object>> categoryLinkList = CategoryOverviewHelper.convertToListMap(map.get(CategoryOverviewHelper.CATEGORY_OVERVIEW_LINKS),
     resources);
   columnMap.put(LINK_LIST, categoryLinkList);
   
   LOGGER.debug("The column map corresponding to the categoryOverview is " + columnMap);
   columnList.add(columnMap);
  }
  categoryOverview.put(HEADLINE, heading);
  categoryOverview.put(CATEGORIES_LIST, columnList.toArray());
  categoryOverviewLinksMap.put(jsonNameSpace, categoryOverview);
  LOGGER.debug("The category overview final data map returned is " + categoryOverviewLinksMap.size());
  return categoryOverviewLinksMap;
 }
 
}
