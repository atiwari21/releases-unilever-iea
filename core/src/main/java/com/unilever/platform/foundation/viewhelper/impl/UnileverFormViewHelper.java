/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;

import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.sapient.platform.iea.aem.viewhelper.FormViewHelperImpl;
import com.unilever.platform.aem.foundation.configuration.GlobalConfiguration;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * The Class UnileverFormViewHelper.
 */
@Component(description = "Unilever-Form Viewhelper Impl", immediate = true, metatype = true, label = "Unilever-Form helper Impl")
@Service(value = { UnileverFormViewHelper.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.UNILEVERFORM, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class UnileverFormViewHelper extends BaseViewHelper {
 
 private static final String FORM_DATA_URL = "formDataUrl";
 
 /** The configuration service. */
 @Reference
 FormViewHelperImpl formViewHelperImpl;
 
 @Reference
 ViewHelper formHiddenViewHelperImpl;
 
 @Reference
 GlobalConfiguration globalConfiguration;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  
  Map<String, Object> formHelperMap = formViewHelperImpl.processData(content, resources);
  String jsonNamespace = getJsonNameSpace(content);
  ResourceResolver resourceResolver = getResourceResolver(resources);
  
  Map<String, Object> formMap = MapUtils.getMap(formHelperMap, jsonNamespace);
  
  boolean preFillFormWithProfileData = MapUtils.getBoolean(getProperties(content), "preFillFormWithProfileData", false);
  
  String formDataUrl = MapUtils.getString(getProperties(content), FORM_DATA_URL, StringUtils.EMPTY);
  
  formMap.put("isPreFillFormWithProfileData", preFillFormWithProfileData);
  if (preFillFormWithProfileData) {
   formMap.put(FORM_DATA_URL, resourceResolver.map(formDataUrl));
  } else {
   formMap.put(FORM_DATA_URL, StringUtils.EMPTY);
  }
  formMap.put("getSurveyUrl", resourceResolver.map(globalConfiguration.getConfigValue("campaign.getSurveyUrl")));
  
  return formHelperMap;
 }
 
}
