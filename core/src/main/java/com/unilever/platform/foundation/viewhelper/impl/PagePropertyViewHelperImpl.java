/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.Resource;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * The class PagePropertyViewHelperImpl
 */
@Component(description = "PagePropertyViewHelperImpl", immediate = true, metatype = true, label = "Page Property")
@Service(value = { PagePropertyViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.PAGE_PROPERTY, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class PagePropertyViewHelperImpl extends BaseViewHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(PagePropertyViewHelperImpl.class);
 
 private static final String PREFIX_TEXT = "prefixText";
 
 /** Constant PAGE_PROPERTY_NAME. */
 private static final String PAGE_PROPERTY_NAME = "pagePropertyList";
 
 /** Constant CRX_PROPERTY. */
 private static final String CRX_PROPERTY = "crxProperty";
 
 /** Constant CONFIG_CATNAME. */
 private static final String CONFIG_CATNAME = "pageProperty";
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @SuppressWarnings("unchecked")
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("Executing PagePropertyViewHelperImpl. Inside processData method ");
  Page currentPage = getCurrentPage(resources);
  Resource resource = getCurrentResource(resources);
  Map<String, Object> pageProperties = (Map<String, Object>) content.get(PAGE_PROPERTIES);
  List<Map<String, Object>> pagePropertyStepsListFinal = new ArrayList<Map<String, Object>>();
  Map<String, Object> pagePropertiesMap = new LinkedHashMap<String, Object>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  
  List<Map<String, String>> pagePropertySectionsList = ComponentUtil.getNestedMultiFieldProperties(resource, "sections");
  
  // preparing list of steps with headline and description
  List<Map<String, Object>> sectionsListFinal = prepareSectionList(currentPage, pagePropertySectionsList, pageProperties);
  
  LOGGER.debug("The sections list being passed to the JSP after processing is :- " + sectionsListFinal);
  pagePropertyStepsListFinal.addAll(sectionsListFinal);
  
  pagePropertiesMap.put("propertiesList", pagePropertyStepsListFinal.toArray());
  LOGGER.debug("Page Property Map: " + pagePropertiesMap.size());
  Map<String, Object> finalDataMap = new HashMap<String, Object>();
  finalDataMap.put(getJsonNameSpace(content), pagePropertiesMap);
  return finalDataMap;
  
 }
 
 private List<Map<String, Object>> prepareSectionList(Page currentPage, List<Map<String, String>> pagePropertySectionsList,
   Map<String, Object> pageProperties) {
  List<Map<String, Object>> finalSectionsList = new ArrayList<Map<String, Object>>();
  
  for (Map<String, String> sectionMap : pagePropertySectionsList) {
   String prefixText = MapUtils.getString(sectionMap, PREFIX_TEXT, StringUtils.EMPTY);
   String suffixText = MapUtils.getString(sectionMap, "suffixText", StringUtils.EMPTY);
   String pagePropertyName = sectionMap.get(PAGE_PROPERTY_NAME);
   Map<String, Object> finalSectionsMap = new LinkedHashMap<String, Object>();
   
   if ("custom".equals(pagePropertyName)) {
    String crxProperty = sectionMap.get(CRX_PROPERTY);
    pagePropertyName = crxProperty;
   }
   
   if (NameConstants.PN_PAGE_TITLE.equals(pagePropertyName)) {
    pagePropertyName = JcrConstants.JCR_TITLE;
   }
   
   getPropertyValue(currentPage, pagePropertyName, pageProperties, finalSectionsMap);
   
   finalSectionsMap.put(PREFIX_TEXT, prefixText);
   finalSectionsMap.put("suffixText", suffixText);
   
   finalSectionsList.add(finalSectionsMap);
   
  }
  
  return finalSectionsList;
 }
 
 private void getPropertyValue(Page currentPage, String pagePropertiesOptions, Map<String, Object> pageProperties,
   Map<String, Object> pagePropertiesMap) {
  String propertyValue = StringUtils.EMPTY;
  if ("publishDate".equals(pagePropertiesOptions) && pageProperties.get(pagePropertiesOptions) != null) {
   
   String dt = ComponentUtil.getFormattedDateString((GregorianCalendar) pageProperties.get(pagePropertiesOptions), null);
   propertyValue = ComponentUtil.getformattedDateByConfig(currentPage, dt, CONFIG_CATNAME);
   
  } else if ("lastReviewDate".equals(pagePropertiesOptions) && pageProperties.get(pagePropertiesOptions) != null) {
   
   String dtReview = ComponentUtil.getFormattedDateString((GregorianCalendar) pageProperties.get(pagePropertiesOptions), null);
   propertyValue = ComponentUtil.getformattedDateByConfig(currentPage, dtReview, CONFIG_CATNAME);
   
  } else if ("pageReadTime".equals(pagePropertiesOptions)) {
   int readTimeValue = pageProperties.get(pagePropertiesOptions) != null ? Integer.parseInt((String) pageProperties.get(pagePropertiesOptions)) : 0;
   pagePropertiesMap.put("propertyValue", readTimeValue);
  } else {
   propertyValue = pageProperties.get(pagePropertiesOptions) != null ? pageProperties.get(pagePropertiesOptions).toString() : StringUtils.EMPTY;
  }
  if (!"pageReadTime".equals(pagePropertiesOptions)) {
   pagePropertiesMap.put("propertyValue", propertyValue);
  }
 }
 
}
