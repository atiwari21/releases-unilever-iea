/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
/**
 * Â© Confidential property of Unilever
 * Do not reproduce or re-distribute without the express written consent of Unilever
 *
 */
package com.unilever.platform.foundation.viewhelper.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.jcr.Session;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Reference;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.jcr.resource.JcrResourceConstants;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.i18n.I18n;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.viewhelper.impl.DefaultBaseViewHelper;
import com.unilever.d2.platform.foundation.core.util.ResourceUtils;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.ConfigurationServiceImpl;
import com.unilever.platform.aem.foundation.configuration.GlobalConfiguration;
import com.unilever.platform.aem.foundation.constants.CommonConstants;
import com.unilever.platform.aem.foundation.core.dto.BrandMarketBean;
import com.unilever.platform.aem.foundation.utils.AdaptiveUtil;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.core.i18n.UnileverI18nImpl;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * Extending iea DefaultBaseViewHelper for component view helper classes to
 * extract the data from component Have methods processData
 * ,getComponentViewAndViewHelperCache,preProcess.
 * 
 * @return list of map of resources and content of component.
 */
public abstract class BaseViewHelper extends DefaultBaseViewHelper {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(BaseViewHelper.class);

	/** The Constant ANCHOR_LINK_TITLE. */
	private static final String ANCHOR_LINK_TITLE = "anchorLinkTitle";

	/** The map for json keys. */
	private static String MAP_FOR_JSON_KEYS = "mapForJsonKeys";

	/** The content. */
	private static String CONTENT = "content";

	/** The Constant HEADER_IPARSYS_NAME. */
	private static final String HEADER_IPARSYS_NAME = "headerpar";

	/** The Constant FOOTER_IPARSYS_NAME. */
	private static final String FOOTER_IPARSYS_NAME = "footerpar";

	/** The Constant FOOTER_IPARSYS_NAME. */
	private static final String SECONDARY_IPARSYS_NAME = "secondarypar";

	/** The global configuration. */
	private GlobalConfiguration globalConfiguration;

	@Reference
	private static QueryBuilder builder;


	private static final String LOCATIONNODE = "location";

	private static final String TEASERPAGE = "cq/personalization/components/teaserpage";

	private static final String PERS_RES_TYPE = "cq/personalization/components/target";

	/**
	 * The configuration service.
	 * 
	 * @param content
	 *            the content
	 * @param resources
	 *            the resources
	 * @return the map
	 */

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sapient.platform.iea.aem.core.viewhelper.impl.DefaultBaseViewHelper
	 * #processData(java.util.Map, java.util.Map)
	 */
	@Override
	public abstract Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sapient.platform.iea.aem.core.viewhelper.impl.DefaultBaseViewHelper
	 * #preProcess(java.util.Map, java.util.Map)
	 */
	@Override
	public Map<String, Object> preProcess(final Map<String, Object> content, final Map<String, Object> objects) {

		Map<String, Object> resources = objects;
		UnileverI18nImpl unileverI18n = (UnileverI18nImpl) getI18n(getSlingRequest(resources),
				getCurrentPage(resources));
		resources.put("unileverI18n", unileverI18n);

		ConfigurationService configurationService = getCurrentPage(resources).adaptTo(ConfigurationService.class);
		resources.put("configurationService", configurationService);

		return super.preProcess(content, resources);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sapient.platform.iea.aem.core.viewhelper.impl.DefaultBaseViewHelper
	 * #postProcess(java.util.Map, java.util.Map, java.util.Map)
	 */
	@Override
	public Map<String, Object> postProcess(final Map<String, Object> content, final Map<String, Object> objects,
			final Map<String, Object> mapForJsonKeys) {
		Map<String, Map<String, Object>> map = addAditionKeysToJSON(objects, content, mapForJsonKeys);
		Map<String, Object> modfiedMapForJsonKeys = map.get(MAP_FOR_JSON_KEYS) != null ? map.get(MAP_FOR_JSON_KEYS)
				: mapForJsonKeys;
		modfiedMapForJsonKeys.remove("path");
		Map<String, Object> modifiedContent = map.get(CONTENT) != null ? map.get(CONTENT) : content;
		setMaxAgeHeader(objects);
		return super.postProcess(modifiedContent, objects, modfiedMapForJsonKeys);

	}

	/**
	 * Adds the adition keys to json.
	 * 
	 * @param objects
	 *            the objects
	 * @param content
	 *            the content
	 * @param mapForJsonKeys
	 *            the map for json keys
	 * @return the map
	 */
	private Map<String, Map<String, Object>> addAditionKeysToJSON(final Map<String, Object> objects,
			Map<String, Object> content, Map<String, Object> mapForJsonKeys) {
		Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>();
		try {

			ResourceResolver resourceResolver = (ResourceResolver) objects.get(ProductConstants.RESOURCE_RESOLVER);
			SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) objects.get(ProductConstants.REQUEST);
			Resource compResource = slingRequest.getResource();
			PageManager pm = resourceResolver.adaptTo(PageManager.class);
			Page currentPage =  getCurrentPage(objects);
			BrandMarketBean bm = new BrandMarketBean(currentPage);
			String brandName = bm.getBrand();
			String market = bm.getMarket();
			String currentPageLocale = currentPage.getLanguage(false).toLanguageTag();

			ValueMap componentProperties = compResource.getValueMap();
			ValueMap componentParent = compResource.getParent().getValueMap();
			String parentResourceType = componentParent.get(JcrResourceConstants.SLING_RESOURCE_TYPE_PROPERTY) != null
					? componentParent.get(JcrResourceConstants.SLING_RESOURCE_TYPE_PROPERTY).toString() : "";
					String experienceName = componentParent.get(JcrConstants.JCR_TITLE) != null
							? componentParent.get(JcrConstants.JCR_TITLE).toString() : "";

							Iterator<String> itr = content.keySet().iterator();
							if (itr.hasNext()) {
								String key = itr.next();
								Object obj = content.get(key);
								if (obj instanceof Map) {
									@SuppressWarnings("unchecked")
									Map<String, Object> contentMap = (Map<String, Object>) content.get(key);
									contentMap.put(UnileverConstants.BRAND_NAME_KEY, brandName);
									setBaseImagePath(slingRequest, contentMap);
									contentMap.put(UnileverConstants.MARKET, market);
									contentMap.put(UnileverConstants.LOCALE, currentPageLocale);
									contentMap.put("componentVariation", componentProperties.getOrDefault("viewType", "defaultView"));
									contentMap.put("componentPosition", getComponentPosition(objects));
									contentMap.put("componentName", getComponentName(compResource, slingRequest));
									/**
									 * Since the component path is always unique, this path is
									 * used to find MD5 hash code, this hash code is always
									 * unique and can be used as random number, this approach
									 * ensures that the random number is always unique even if
									 * the component is copied one this approach is used for all
									 * components except TabbedControl component
									 */
									if (componentProperties.get(UnileverConstants.RANDOM_NUMBER) != null
											&& componentProperties.get(UnileverConstants.RANDOM_NUMBER) instanceof String) {
										contentMap.put("randomNumber", DigestUtils.md5Hex(slingRequest.getResource().getPath()));
									} else {
										contentMap.put("randomNumber", componentProperties.get(UnileverConstants.RANDOM_NUMBER));
									}

									String componentName = getComponentName(compResource, slingRequest);
									String[] dontIncludeStyleTab = null;
									final BundleContext bundleContext = FrameworkUtil.getBundle(GlobalConfiguration.class)
											.getBundleContext();
									final ServiceReference osgiRef = bundleContext
											.getServiceReference(GlobalConfiguration.class.getName());
									this.globalConfiguration = (GlobalConfiguration) bundleContext.getService(osgiRef);
									if (this.globalConfiguration != null) {
										dontIncludeStyleTab = globalConfiguration.getCommonGlobalConfigValueArray("dontIncludeStyleTab");
									}
									if (!(ArrayUtils.contains(dontIncludeStyleTab, componentName))) {
										contentMap.put("style", getStyleProperties(componentProperties));
									}
									contentMap.put("analytics", getAnalyticsContextProperties(componentProperties));
									contentMap.put("anchorTitle", getI18n(slingRequest, currentPage).get("anchorLinkTitle"));
									content.put(key, contentMap);
									if (parentResourceType.contains(UnileverConstants.EXPERIENCE_PAGE)) {
										contentMap.put("componentExperienceVariant", experienceName);
									} else {
										contentMap.put("componentExperienceVariant", "default");
									}
								}
							}
							//Code to Add config error.
							ConfigurationServiceImpl configService = (ConfigurationServiceImpl) objects.get("configurationService");
							Map<String, List<Map<String, String>>> errorMapList = configService.getErrorMapList();
							String mapKey = bm.getBrand() + "_" + bm.getMarket();
							if(errorMapList != null && errorMapList.containsKey(mapKey) && errorMapList.get(mapKey) != null){
							 content.put("configError",  errorMapList.get(mapKey).toArray());
							 //Empty the Configuration error list
							 errorMapList.put(mapKey, null);
							 configService.setErrorMapList(errorMapList);
							 
							}
							map.put("content", content);
							map.put("mapForJsonKeys", mapForJsonKeys);
		} catch (Exception e) {
			LOGGER.error("error in addAditionKeysToJSON method in BaseViewHelper ", e);
		}
		return map;
	}

	/**
	 * Gets the component name.
	 * 
	 * @param compResource
	 *            the component properties
	 * @param slingRequest
	 *            the sling request
	 * @return the component name
	 */
	public static String getComponentName(Resource compResource, SlingHttpServletRequest slingRequest) {
		String componentName = "";
		if (compResource != null) {
			componentName = compResource.getValueMap().get("componentName", String.class);
		}

		Locale enLocale = new Locale("en");
		ResourceBundle resourceBundle = slingRequest.getResourceBundle(enLocale);
		return new I18n(resourceBundle).getVar(componentName);
	}

	/**
	 * Gets the analytics propertties.
	 * 
	 * @param componentProperties
	 *            the component properties
	 * @return the analytics properties
	 */
	private Map<String, Object> getAnalyticsContextProperties(ValueMap componentProperties) {
		String analyticContext = MapUtils.getString(componentProperties, "configurejson", StringUtils.EMPTY);
		String analyticsEventType = MapUtils.getString(componentProperties, "analyticsEventType", StringUtils.EMPTY);
		String customAnalyticsContext = MapUtils.getString(componentProperties, "customAnalyticsContext",
				StringUtils.EMPTY);
		Map<String, Object> analyticsContextProperties = new LinkedHashMap<String, Object>();
		analyticContext = StringUtils.isNotBlank(customAnalyticsContext) && ("Custom").equals(analyticContext)
				? customAnalyticsContext : analyticContext;
		analyticContext = (UnileverConstants.PLEASE_SELECT).equals(analyticContext) ? StringUtils.EMPTY
				: analyticContext;
		analyticsEventType = (UnileverConstants.PLEASE_SELECT).equals(analyticsEventType) ? StringUtils.EMPTY
				: analyticsEventType;
		analyticsContextProperties.put("usageContext", analyticContext);
		analyticsContextProperties.put("eventType", analyticsEventType);
		return analyticsContextProperties;
	}

	/**
	 * Gets the style properties.
	 * 
	 * @param componentProperties
	 *            the component properties
	 * @return the style properties
	 */
	public static Map<String, Object> getStyleProperties(ValueMap componentProperties) {

		Map<String, Object> cssStyleProperties = new LinkedHashMap<String, Object>();

		String cssStyleType = "";
		String cssStyleClass = "";
		if (componentProperties != null) {
			cssStyleType = componentProperties.get("type", StringUtils.EMPTY);
			cssStyleClass = componentProperties.get("class", StringUtils.EMPTY);

		}
		cssStyleProperties.put("type", cssStyleType);
		cssStyleProperties.put("class", cssStyleClass);

		LOGGER.debug("Style properties map : " + cssStyleProperties);
		return cssStyleProperties;

	}

	/**
	 * Gets the component position.
	 * 
	 * @param resources
	 *            the resources
	 * @return the component position
	 */
	public static int getComponentPosition(final Map<String, Object> resources) {
		int componentPosition = 0;
		Page currentPage = getCurrentPage(resources);
		Page actualPage = getActualPage(resources);
		Resource jcrContentResource = currentPage.getContentResource();
		List<String> resourcesPathFullList = new ArrayList<String>();
		List<String> headerComponentFullList = new ArrayList<String>();
		List<String> footerComponentFullList = new ArrayList<String>();
		List<String> secondaryComponentFullList = new ArrayList<String>();
		if (actualPage != null) {
			Iterator<Resource> childResourceItr = jcrContentResource.listChildren();
			if (StringUtils.equalsIgnoreCase(currentPage.getPath(), actualPage.getPath())) {
				while (childResourceItr.hasNext()) {
					resourcesPathFullList.addAll(ComponentUtil.getNonParChildResourceNameList(childResourceItr.next()));
				}
			} else {
				while (childResourceItr.hasNext()) {
					Resource childResource = childResourceItr.next();
					if (StringUtils.contains(childResource.getName(), HEADER_IPARSYS_NAME)) {
						headerComponentFullList.addAll(ComponentUtil.getNonParChildResourceNameList(childResource));
					} else if (StringUtils.contains(childResource.getName(), FOOTER_IPARSYS_NAME)) {
						footerComponentFullList.addAll(ComponentUtil.getNonParChildResourceNameList(childResource));
					} else if (StringUtils.contains(childResource.getName(), SECONDARY_IPARSYS_NAME)) {
						secondaryComponentFullList.addAll(ComponentUtil.getNonParChildResourceNameList(childResource));
					}
				}
			}
			String currentResourceParentName = ResourceUtils.getCurrentResourceParentName(resources);
			Page headerPage = ResourceUtils.getIparsysParent(actualPage, HEADER_IPARSYS_NAME);

			if ((headerPage != null) && StringUtils.equalsIgnoreCase(headerPage.getPath(), actualPage.getPath())) {
				componentPosition = resourcesPathFullList.indexOf(getCurrentResource(resources).getPath()) + 1;
			} else if (StringUtils.equalsIgnoreCase(currentResourceParentName, HEADER_IPARSYS_NAME)) {
				componentPosition = headerComponentFullList.indexOf(getCurrentResource(resources).getPath()) + 1;
			} else if (StringUtils.equalsIgnoreCase(currentResourceParentName, FOOTER_IPARSYS_NAME)) {
				componentPosition = ResourceUtils.getComponentCount(resources, HEADER_IPARSYS_NAME)
						+ ResourceUtils.getComponentCount(resources, SECONDARY_IPARSYS_NAME)
						+ ResourceUtils.getActualPageComponentCount(resources)
						+ footerComponentFullList.indexOf(getCurrentResource(resources).getPath()) + 1;
			} else if (StringUtils.equalsIgnoreCase(currentResourceParentName, SECONDARY_IPARSYS_NAME)) {
				componentPosition = ResourceUtils.getComponentCount(resources, HEADER_IPARSYS_NAME)
						+ secondaryComponentFullList.indexOf(getCurrentResource(resources).getPath()) + 1;
			} else {
				componentPosition = ResourceUtils.getComponentCount(resources, HEADER_IPARSYS_NAME)
						+ ResourceUtils.getComponentCount(resources, SECONDARY_IPARSYS_NAME)
						+ resourcesPathFullList.indexOf(getCurrentResource(resources).getPath()) + 1;
			}
		}

		return componentPosition;
	}

	/**
	 * Gets the non par child resource name list.
	 * 
	 * @param resource
	 *            the resource
	 * @return the non par child resource name list
	 */
	public static List<String> getNonParChildResourceNameList(Resource resource) {
		List<String> resourcePathList = new ArrayList<String>();
		String resType = resource.getResourceType();
		if (resType != null && !resType.endsWith("/parsys")) {
			resourcePathList.add(resource.getPath());
		} else {
			Iterator<Resource> childResourceItr = resource.listChildren();
			while (childResourceItr.hasNext()) {
				resourcePathList.addAll(getNonParChildResourceNameList(childResourceItr.next()));
			}
		}
		return resourcePathList;
	}

	/**
	 * Gets the current page.
	 * 
	 * @param content
	 *            the content
	 * @return the current page
	 */
	@SuppressWarnings("unchecked")
	public static Map<String, Object> getProperties(final Map<String, Object> content) {
		return (Map<String, Object>) content.get(PROPERTIES);
	}

	/**
	 * Gets the resource resolver.
	 * 
	 * @param resources
	 *            the resources
	 * @return the resource resolver
	 */
	public static ResourceResolver getResourceResolver(final Map<String, Object> resources) {
		return (ResourceResolver) resources.get(ProductConstants.RESOURCE_RESOLVER);
	}

	/**
	 * Gets the sling request.
	 * 
	 * @param resources
	 *            the resources
	 * @return the sling request
	 */
	public static SlingHttpServletRequest getSlingRequest(final Map<String, Object> resources) {
		return (SlingHttpServletRequest) resources.get(ProductConstants.REQUEST);

	}

	/**
	 * Gets the current resource.
	 * 
	 * @param resources
	 *            the resources
	 * @return the current resource
	 */
	public static Resource getCurrentResource(final Map<String, Object> resources) {
		return ((SlingHttpServletRequest) resources.get(ProductConstants.REQUEST)).getResource();
	}

	/**
	 * Gets the page manager.
	 * 
	 * @param resources
	 *            the resources
	 * @return the page manager
	 */
	public static PageManager getPageManager(final Map<String, Object> resources) {
		ResourceResolver resourceResolver = (ResourceResolver) resources.get(ProductConstants.RESOURCE_RESOLVER);
		return resourceResolver.adaptTo(PageManager.class);
	}

	/**
	 * Gets the tag manager.
	 * 
	 * @param resources
	 *            the resources
	 * @return the tag manager
	 */
	public static TagManager getTagManager(final Map<String, Object> resources) {
		return getResourceResolver(resources).adaptTo(TagManager.class);
	}

	/**
	 * Gets the json name space.
	 * 
	 * @param content
	 *            the content
	 * @return the json name space
	 */
	public static String getJsonNameSpace(final Map<String, Object> content) {
		return MapUtils.getString(content, "jsonNameSpace");
	}

	/**
	 * Gets the current page.
	 * 
	 * @param resources
	 *            the resources
	 * @return the current page
	 */

	public static Page getCurrentPage(final Map<String, Object> resources) {

		PageManager pageManager = getPageManager(resources);
		Page currentPage = pageManager.getContainingPage(getSlingRequest(resources).getResource());
		ValueMap campProps=currentPage.getProperties();
		String resourceType=MapUtils.getString(campProps, JcrResourceConstants.SLING_RESOURCE_TYPE_PROPERTY, StringUtils.EMPTY);
		if (resourceType.equals(TEASERPAGE)) {
			String locationProperty=MapUtils.getString(campProps, LOCATIONNODE, StringUtils.EMPTY);
			String locationNode =locationProperty;
			String path = locationProperty.replaceAll(UnileverConstants.HYPHEN_PARAM, UnileverConstants.FORWARD_SLASH);
			int slash1 = path.indexOf(UnileverConstants.FORWARD_SLASH);
			int slash2 = path.indexOf(UnileverConstants.FORWARD_SLASH, slash1 + 1);
			int slash3 = path.indexOf(UnileverConstants.FORWARD_SLASH, slash2 + 1);
			String subPath=StringUtils.EMPTY;
			try			
			{ subPath= path.substring(0, slash3);
			}
			catch(StringIndexOutOfBoundsException e)
			{
				LOGGER.error("String out of bound ", e);
			}
			if (!subPath.startsWith(UnileverConstants.FORWARD_SLASH)) {
				subPath = UnileverConstants.FORWARD_SLASH + subPath;
			}
			Iterator<Resource> queryResources = getCurrentTargetPage(resources, subPath, locationNode);
			if (queryResources != null)
			{
				return pageManager.getContainingPage(queryResources.next());
			}

		}
		return pageManager.getContainingPage(getSlingRequest(resources).getResource());
	}

	public static Iterator<Resource> getCurrentTargetPage(final Map<String, Object> resources, String path,
			String location) {
		ResourceResolver resolver = getResourceResolver(resources);
		Session session = resolver.adaptTo(Session.class);
	    builder = resolver.adaptTo(QueryBuilder.class);
		Map<String, String> map = new HashMap<String, String>();
		if (builder != null) {
			map.put("path", path);
			map.put("p.limit", "-1");
			map.put("type", JcrConstants.NT_UNSTRUCTURED);
			map.put("1_property", LOCATIONNODE);
			map.put("1_property.value", location);
			map.put("2_property", JcrResourceConstants.SLING_RESOURCE_TYPE_PROPERTY);
			map.put("2_property.value", PERS_RES_TYPE);
			map.put("property.and","true");
			return getResultfromQuery(session, map);
		}

		return null;

	}


	/**
	 * returns the i18n object.
	 * 
	 * @param resources
	 *            the resources
	 * @return the i18n
	 */
	public static I18n getI18n(final Map<String, Object> resources) {
		return (UnileverI18nImpl) resources.get("unileverI18n");
	}

	/**
	 * Gets the i18n.
	 * 
	 * @param slingRequest
	 *            the sling request
	 * @param currentPage
	 *            the current page
	 * @return the i18n
	 */
	public static I18n getI18n(SlingHttpServletRequest slingRequest, Page currentPage) {
		Locale lang = currentPage.getLanguage(false);
		ResourceBundle resourceBundleWithBaseName = slingRequest
				.getResourceBundle(new BrandMarketBean(currentPage).getBrand(), currentPage.getLanguage(false));
		UnileverI18nImpl unileverI18n = new UnileverI18nImpl(resourceBundleWithBaseName);
		ResourceBundle resourceBundleWithOutBaseName = slingRequest.getResourceBundle("unilever", lang);
		I18n i18n = new I18n(resourceBundleWithOutBaseName);
		unileverI18n.setI18n(i18n);
		return unileverI18n;
	}

	/**
	 * Sets the base image path.
	 * 
	 * @param slingRequest
	 *            the sling request
	 * @param contentMap
	 *            the content map
	 */
	private void setBaseImagePath(SlingHttpServletRequest slingRequest, Map<String, Object> contentMap) {
		if (AdaptiveUtil.isAdaptive(slingRequest)) {
			StringBuilder stringBuilder = new StringBuilder();
			String baseImagePath = stringBuilder.append("/etc/ui/")
					.append(AdaptiveUtil.getAdaptiveTenantName(slingRequest)).append("/clientlibs/core/core")
					.toString();
			contentMap.put("baseImagePath", baseImagePath);
		}
	}

	/**
	 * Gets the locale.
	 * 
	 * @param currentPage
	 *            the current page
	 * @return the locale
	 */
	public static Locale getLocale(Page currentPage) {
		if (currentPage != null) {
			return currentPage.getLanguage(false);
		}
		return null;

	}

	/**
	 * Gets the actual page.
	 * 
	 * @param resources
	 *            the resources
	 * @return the actual page
	 */
	public static Page getActualPage(final Map<String, Object> resources) {

		Page actualPage = null;
		String actualPagePath = StringUtils.EMPTY;

		PageManager pageManager = getPageManager(resources);
		SlingHttpServletRequest request = getSlingRequest(resources);

		if (request != null) {
			actualPagePath = request.getPathInfo();
		}

		try {
			if (pageManager != null && StringUtils.isNotBlank(actualPagePath)) {
				if (actualPagePath.contains(".html")) {
					actualPagePath = actualPagePath.substring(0, actualPagePath.indexOf(".html"));
					String pageNameIncludingSelectors = actualPagePath.substring(actualPagePath.lastIndexOf("/") + 1,
							actualPagePath.length());
					if (pageNameIncludingSelectors.contains(".")) {
						pageNameIncludingSelectors = pageNameIncludingSelectors.substring(0,
								pageNameIncludingSelectors.indexOf("."));
						actualPagePath = actualPagePath.substring(0,
								actualPagePath.lastIndexOf(pageNameIncludingSelectors)
								+ pageNameIncludingSelectors.length());
					}
				} else if (actualPagePath.contains("_jcr_content")) {
					actualPagePath = actualPagePath.substring(0, actualPagePath.indexOf("_jcr_content") - 1);
				} else if (actualPagePath.contains(JcrConstants.JCR_CONTENT)) {
					actualPagePath = actualPagePath.substring(0, actualPagePath.indexOf(JcrConstants.JCR_CONTENT) - 1);
				}
				actualPage = pageManager.getPage(actualPagePath);
				if (actualPage == null) {
					String path = request.getRequestPathInfo().getResourcePath();
					actualPage = pageManager.getContainingPage(path);

				}
			}
		} catch (Exception e) {
			LOGGER.error("Exception while getting actual page path - ", e);
		}
		return actualPage;
	}

	/**
	 * Gets the anchor link title.
	 * 
	 * @param i18n
	 *            the i18n
	 * @return the anchor link title
	 */
	public static String getAnchorLinkTitle(I18n i18n) {
		return i18n.get(ANCHOR_LINK_TITLE);

	}

	/**
	 * Checks if current resource is configured in brand configuration and if
	 * yes, then sets the max-age so that component response is cached at
	 * dispatcher for this period.
	 * 
	 * @param resources
	 */
	private static void setMaxAgeHeader(final Map<String, Object> resources) {
		ConfigurationService configurationService = getCurrentPage(resources).adaptTo(ConfigurationService.class);
		Map<String, String> serverSideIncludes = null;
		String[] sdiComponentList = null;
		if (configurationService != null) {
			try {
				serverSideIncludes = configurationService.getCategoryConfiguration(getCurrentPage(resources),
						"serverSideIncludes");
			} catch (ConfigurationNotFoundException e) {
				LOGGER.debug("configurations for serverSideIncludes are not found: ", e);
			}
			boolean enabled = false;
			if (serverSideIncludes.get("enabled") != null) {
				enabled = Boolean.parseBoolean(serverSideIncludes.get("enabled"));
			}
			if (enabled && serverSideIncludes != null && serverSideIncludes.get("resourceTypes") != null) {
				String resourceTypes = serverSideIncludes.get("resourceTypes");
				resourceTypes = StringUtils.replace(resourceTypes, " ", "");
				sdiComponentList = resourceTypes.split(CommonConstants.COMMA);
				if (ArrayUtils.indexOf(sdiComponentList, getResourceType(resources)) > -1) {
					SlingScriptHelper scriptHelper = (SlingScriptHelper) resources.get("sling");
					SlingHttpServletResponse slingResponse = scriptHelper.getResponse();
					final HttpServletResponse response = (HttpServletResponse) slingResponse;
					response.setHeader("Cache-Control", "max-age=" + serverSideIncludes.get("ttl"));
				}
			}
		}
	}

	/**
	 * Get resource type of current resource
	 * 
	 * @param resources
	 * @return resource type
	 */
	public static String getResourceType(final Map<String, Object> resources) {
		String resourceType = StringUtils.EMPTY;
		Resource resource = getCurrentResource(resources);
		if (resource != null) {
			resourceType = resource.getResourceType();
		}
		return resourceType;
	}
    
	@SuppressWarnings("unchecked")
	 private static Iterator<Resource> getResultfromQuery(Session session, Map<String, String> map) {
	  
	  LOGGER.debug("get result query");
	  
	  Query query = builder.createQuery(PredicateGroup.create(map), session);
	  SearchResult result = null;
	  Iterator<Resource> resources = IteratorUtils.emptyIterator();
	  
	  query.setHitsPerPage(0);
	  if (query != null) {
	   result = query.getResult();
	  }
	  
	  if (result != null) {
	   LOGGER.debug("result not null.");
	   LOGGER.debug("result count --->" + result.getTotalMatches());
	   resources = result.getResources();
	  }
	  
	  return resources;
	 }
}
