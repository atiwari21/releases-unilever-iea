/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * This class prepares the data map for IFrame Component It reads the values from the dialog and passes the info as a json.
 */

@Component(description = "iframe", immediate = true, metatype = true, label = "iframe")
@Service(value = { IFrameViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.IFRAME, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class IFrameViewHelperImpl extends BaseViewHelper {
 
 /** The Constant SRC. */
 private static final String SRC = "src";
 
 /** The Constant NAME. */
 private static final String NAME = "name";
 
 /** The Constant WIDTH. */
 private static final String WIDTH = "width";
 
 /** The Constant HEIGHT. */
 private static final String HEIGHT = "height";
 
 /** The Constant SCROLLING. */
 private static final String SCROLLING = "scrolling";
 
 /** The Constant FRAME_BORDER. */
 private static final String FRAME_BORDER = "frameborder";
 
 /** The Constant ALIGN. */
 private static final String ALIGN = "align";
 
 /** The Constant ALLOW_FORMS. */
 private static final String ALLOW_FORMS = "allowForms";
 
 /** The Constant ALLOW_POINTER_LOCK. */
 private static final String ALLOW_POINTER_LOCK = "allowPointerLock";
 
 /** The Constant ALLOW_POP_UPS. */
 private static final String ALLOW_POP_UPS = "allowPopups";
 
 /** The Constant SCROLLING. */
 private static final String ALLOW_SAME_ORIGIN = "allowSameOrigin";
 
 /** The Constant ALLOW_SAME_ORIGIN. */
 private static final String ALLOW_SCRIPTS = "allowScripts";
 
 /** The Constant ALLOW_TOP_NAVIGATION. */
 private static final String ALLOW_TOP_NAVIGATION = "allowTopNavigation";
 
 /** The Constant MARGIN_HEIGHT. */
 private static final String MARGIN_HEIGHT = "marginheight";
 
 /** The Constant MARGIN_WIDTH. */
 private static final String MARGIN_WIDTH = "marginwidth";
 
 /** The Constant LONG_DESCRIPTION. */
 private static final String LONG_DESCRIPTION = "longdesc";
 
 /** The Constant SRC_DOC. */
 private static final String SRC_DOC = "srcdoc";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(IFrameViewHelperImpl.class);
 
 /** The Constant SAND_ALLOW_FORMS. */
 private static final String SAND_ALLOW_FORMS = "allow-forms ";
 
 /** The Constant SAND_ALLOW_POINTER_LOCK. */
 private static final String SAND_ALLOW_POINTER_LOCK = "allow-pointer-lock ";
 
 /** The Constant SAND_ALLOW_POPUPS. */
 private static final String SAND_ALLOW_POPUPS = "allow-popups ";
 
 /** The Constant SAND_ALLOW_SAME_ORIGIN. */
 private static final String SAND_ALLOW_SAME_ORIGIN = "allow-same-origin ";
 
 /** The Constant SAND_ALLOW_SCRIPTS. */
 private static final String SAND_ALLOW_SCRIPTS = "allow-scripts ";
 
 /** The Constant SAND_ALLOW_TOP_NAVIGATION. */
 private static final String SAND_ALLOW_TOP_NAVIGATION = "allow-top-navigation ";
 
 /** The Constant ENABLE_RESTRICTIONS. */
 private static final String ENABLE_RESTRICTIONS = "enableRestrictions";
 
 /** The Constant APPLY_ALL_RESTRICTIONS. */
 private static final String APPLY_ALL_RESTRICTIONS = "applyAllRestrictions";
 
 /** The Constant IS_AUTO_FRAME_SIZE. */
 private static final String IS_AUTO_FRAME_SIZE = "isAutoFrameSize";
 
 /** The Constant AUTO_FRAME_SIZE. */
 private static final String AUTO_FRAME_SIZE = "autoFrameSize";
 
 /** The Constant SANDBOX. */
 private static final String SANDBOX = "sandbox";
 
 /**
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.info("IFrame Component View Helper #processData() method called..");
  Map<String, Object> iframeFinalProperties = new HashMap<String, Object>();
  
  String currentPagePath   = getCurrentPage(resources).getPath();
  ResourceResolver resolver = (ResourceResolver) resources.get(ProductConstants.RESOURCE_RESOLVER);
  
  String currentPageUrl = ComponentUtil.getFullURL(resolver,currentPagePath);

  int index = StringUtils.indexOf(currentPageUrl, resolver.map(currentPagePath));
  if(index != -1){
   currentPageUrl = StringUtils.substring(currentPageUrl, 0, index);
  }
  iframeFinalProperties = getIframeData(content, currentPageUrl);
  
  Map<String, Object> finalData = new HashMap<String, Object>();
  finalData.put(getJsonNameSpace(content), iframeFinalProperties);
  
  return finalData;
  
 }
 
 private Map<String, Object> getIframeData(Map<String, Object> content, String currePageUrl) {
  Map<String, Object> properties = getProperties(content);
  Map<String, Object> iframeProperties = new HashMap<String, Object>();
  
  boolean autoFrameSize = MapUtils.getBoolean(properties, IS_AUTO_FRAME_SIZE, false);
  
  if (!autoFrameSize) {
   iframeProperties.put(WIDTH, MapUtils.getString(properties, WIDTH, StringUtils.EMPTY));
   iframeProperties.put(HEIGHT, MapUtils.getString(properties, HEIGHT, StringUtils.EMPTY));
  }
 
  String iframeSrc = MapUtils.getString(properties, SRC, StringUtils.EMPTY);
  
  if(StringUtils.isNotEmpty(iframeSrc) && StringUtils.indexOf(iframeSrc, "?") != -1){
   iframeSrc += "&pageDm=" + currePageUrl;
  } else if (StringUtils.isNotEmpty(iframeSrc)) {
   iframeSrc += "?pageDm=" + currePageUrl;
  }

  iframeProperties.put(SRC, iframeSrc);
  iframeProperties.put(NAME, MapUtils.getString(properties, NAME, StringUtils.EMPTY));
  iframeProperties.put(SRC_DOC, MapUtils.getString(properties, SRC_DOC, StringUtils.EMPTY));
  iframeProperties.put(AUTO_FRAME_SIZE, autoFrameSize);
  
  // Code for backward compatibility to get the old value and pass it to json.
  iframeProperties.put(SCROLLING, MapUtils.getString(properties, SCROLLING, "auto"));
  iframeProperties.put(FRAME_BORDER, MapUtils.getString(properties, FRAME_BORDER, "no"));
  iframeProperties.put(ALIGN, MapUtils.getString(properties, ALIGN, "left"));
  iframeProperties.put(MARGIN_HEIGHT, MapUtils.getString(properties, MARGIN_HEIGHT, StringUtils.EMPTY));
  iframeProperties.put(MARGIN_WIDTH, MapUtils.getString(properties, MARGIN_WIDTH, StringUtils.EMPTY));
  iframeProperties.put(LONG_DESCRIPTION, MapUtils.getString(properties, LONG_DESCRIPTION, StringUtils.EMPTY));
  // End
  
  boolean enableRestrction = MapUtils.getBoolean(properties, ENABLE_RESTRICTIONS, false);
  iframeProperties.put(ENABLE_RESTRICTIONS, enableRestrction);
  if (enableRestrction) {
   boolean applyAllRestrction = MapUtils.getBoolean(properties, APPLY_ALL_RESTRICTIONS, false);
   StringBuilder restrictions = new StringBuilder();
   if (applyAllRestrction) {
    
    restrictions.append(
      SAND_ALLOW_FORMS + SAND_ALLOW_POINTER_LOCK + SAND_ALLOW_POPUPS + SAND_ALLOW_SAME_ORIGIN + SAND_ALLOW_SCRIPTS + SAND_ALLOW_TOP_NAVIGATION);
    iframeProperties.put(SANDBOX, restrictions.toString().trim());
   } else {
    restrictions.append(MapUtils.getBoolean(properties, ALLOW_FORMS, false) ? SAND_ALLOW_FORMS : StringUtils.EMPTY);
    restrictions.append(MapUtils.getBoolean(properties, ALLOW_POINTER_LOCK, false) ? SAND_ALLOW_POINTER_LOCK : StringUtils.EMPTY);
    restrictions.append(MapUtils.getBoolean(properties, ALLOW_POP_UPS, false) ? SAND_ALLOW_POPUPS : StringUtils.EMPTY);
    restrictions.append(MapUtils.getBoolean(properties, ALLOW_SAME_ORIGIN, false) ? SAND_ALLOW_SAME_ORIGIN : StringUtils.EMPTY);
    restrictions.append(MapUtils.getBoolean(properties, ALLOW_SCRIPTS, false) ? SAND_ALLOW_SCRIPTS : StringUtils.EMPTY);
    restrictions.append(MapUtils.getBoolean(properties, ALLOW_TOP_NAVIGATION, false) ? SAND_ALLOW_TOP_NAVIGATION : StringUtils.EMPTY);
    iframeProperties.put(SANDBOX, restrictions.toString().trim());
   }
  } else {
   iframeProperties.put(SANDBOX, StringUtils.EMPTY);
  }
  return iframeProperties;
 }
}
