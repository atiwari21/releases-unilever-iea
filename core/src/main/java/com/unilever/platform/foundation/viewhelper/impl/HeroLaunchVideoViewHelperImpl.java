/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.VideoHelper;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.Image;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * <p>
 * The goal of this component is to display the heroLaunchVideo and preview images with option to play video . The component will be configured by
 * editor to provide Hero component video id preview image url and image alt text.
 * 
 * </p>
 */
@Component(description = "heroLaunchVideo Viewhelper Impl", immediate = true, metatype = true, label = "HeroLaunchVideo")
@Service(value = { HeroLaunchVideoViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.HEROLAUNCHVIDEO, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class HeroLaunchVideoViewHelperImpl extends BaseViewHelper {
 
 /**
  * Constant for retrieving heroLaunchVideo component property.
  */
 /** The Constant VIDEO_TITLE. */
 private static final String VIDEO_TITLE = "title";
 /** The Constant ALT_IMAGE. */
 private static final String ALT_IMAGE = "altImage";
 
 /** The Constant ALT_IMAGE. */
 private static final String IMAGE = "image";
 
 /** The Constant VIDEO_ID. */
 private static final String VIDEO_ID = "videoId";
 /** The Constant ID. */
 private static final String ID = "id";
 
 /** The Constant PREVIEW_IMAGE_URL. */
 private static final String PREVIEW_IMAGE_URL = "previewImageUrl";
 /** The Constant video type. */
 private static final String VIDEO_TYPE = "videoType";
 
 /**
  * logger object.
  **/
 private static final Logger LOGGER = LoggerFactory.getLogger(HeroLaunchVideoViewHelperImpl.class);
 
 /**
  * The method processData overrides method is responsible validating author input and returning a list of sections containing rich text,text This
  * method converts the multiwidget input (a String Array) into sections of maps so that it can be accessed in JSP in consistent way.This method also
  * validates the link url provided by author.
  * 
  * @param content
  *         , - {@link Map} of all user input received as input <code>ViewHelper.onGetData</code> method
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return {@link Map} holding the data
  */
 @Override
 @SuppressWarnings("unchecked")
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  
  LOGGER.debug("hero Launch Video View Helper #processData called for processing fixed list.");
  Map<String, Object> heroLaunchProperties = (Map<String, Object>) content.get(PROPERTIES);
  
  Map<String, Object> heroLaunchVideoProperties = new LinkedHashMap<String, Object>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  
  ResourceResolver resourceResolver = (ResourceResolver) resources.get("RESOURCE_RESOLVER");
  
  SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) resources.get(ProductConstants.REQUEST);
  Resource currentResource = slingRequest.getResource();
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  Page page = pageManager.getContainingPage(currentResource);
  
  String jsonNameSpace = MapUtils.getString(content, "jsonNameSpace");
  String videoTitle = getProperty(heroLaunchProperties, VIDEO_TITLE);
  String altImage = getProperty(heroLaunchProperties, ALT_IMAGE);
  String videoId = getProperty(heroLaunchProperties, VIDEO_ID);
  String previewImageUrl = getProperty(heroLaunchProperties, PREVIEW_IMAGE_URL);
  String videoType = getProperty(heroLaunchProperties, VIDEO_TYPE);
  Image image = new Image(previewImageUrl, altImage, videoTitle, page, getSlingRequest(resources));
  if (!StringUtils.EMPTY.equals(videoTitle)) {
   heroLaunchVideoProperties.put(VIDEO_TITLE, videoTitle);
  }
  String videoSource = StringUtils.isNotBlank(videoType) ? videoType : "youtube";
  heroLaunchVideoProperties.put(VIDEO_TYPE, videoSource);
  heroLaunchVideoProperties.put(ID, videoId);
  heroLaunchVideoProperties.put(UnileverConstants.CONTAINER_TAG, ComponentUtil.getVideoContainerTagMap(page, videoId));
  heroLaunchVideoProperties.put(IMAGE, image.convertToMap());
  // Enable video controls as in v2
  heroLaunchVideoProperties.put("videoControls", VideoHelper.getVideoControls(heroLaunchProperties, videoSource));
  LOGGER.debug("Hero Launch Video Properties Map Size" + heroLaunchVideoProperties.size());
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(jsonNameSpace, heroLaunchVideoProperties);
  
  return data;
  
 }
 
 /**
  * Adds the image parameters.
  * 
  * @param url
  *         the url
  * @param imageMap
  *         the image map
  * @param resourceResolver
  */
 public void addImageParameters(String url, Map<String, Object> imageMap, ResourceResolver resourceResolver) {
  
  String filePath = url;
  String extension = StringUtils.EMPTY;
  Resource resource = resourceResolver.getResource(filePath);
  ValueMap valueMap = null;
  if (resource != null) {
   valueMap = resource.adaptTo(ValueMap.class);
  }
  String fileName = fetchImageFileName(filePath);
  if (StringUtils.isEmpty(fileName)) {
   imageMap.put(UnileverComponents.URL, StringUtils.EMPTY);
  }
  imageMap.put(UnileverComponents.FILENAME, fileName);
  if (StringUtils.isNotEmpty(filePath) & filePath.contains(".")) {
   
   extension = filePath.substring(filePath.lastIndexOf('.', filePath.length()) + 1);
  }
  
  imageMap.put(UnileverComponents.EXTENSION, extension);
  
  imageMap.put(UnileverComponents.IMAGE_TITLE_PROPERTY, MapUtils.getString(valueMap, UnileverComponents.IMAGE_TITLE_PROPERTY, StringUtils.EMPTY));
 }
 
 /**
  * Fetch image file name.
  * 
  * @param fileReference
  *         the file reference
  * @return the string
  */
 public static String fetchImageFileName(String fileReference) {
  String fileName = StringUtils.EMPTY;
  if (StringUtils.isNotEmpty(fileReference) & fileReference.contains(".")) {
   
   fileName = fileReference.substring(fileReference.lastIndexOf('/') + 1, fileReference.lastIndexOf('.'));
   
  }
  
  return fileName;
 }
 
 /**
  * Gets the property.
  * 
  * @param heroProperties
  *         the hero properties
  * @param propertyName
  *         the property name
  * @return the property
  */
 public static String getProperty(Map<String, Object> heroProperties, String propertyName) {
  String propertyValue = StringUtils.EMPTY;
  if (heroProperties != null && StringUtils.isNotBlank(propertyName)) {
   propertyValue = MapUtils.getString(heroProperties, propertyName);
  }
  
  if (StringUtils.isBlank(propertyValue)) {
   propertyValue = StringUtils.EMPTY;
  }
  
  return propertyValue;
 }
 
}
