/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
/**
 * © Confidential property of Unilever
 * Do not reproduce or re-distribute without the express written consent of Unilever
 *
 */
package com.unilever.platform.foundation.viewhelper.constants;

/**
 * Home for all the constants related to Product.
 * 
 * @author nbhart
 * 
 */
public class ProductConstants {
 /** title. */
 public static final String TITLE = "title";
 
 /** The Constant IMAGES. */
 public static final String IMAGES = "images";
 
 /** The Constant LINK. */
 public static final String LINK = "link";
 
 /** The Constant URL. */
 public static final String URL = "url";
 
 /** The Constant SUMMARY. */
 public static final String SUMMARY = "summary";
 
 /** The Constant PRICE. */
 public static final String PRICE = "price";
 
 /** The Constant RESOURCE_RESOLVER. */
 public static final String RESOURCE_RESOLVER = "RESOURCE_RESOLVER";
 
 /** The Constant REQUEST. */
 public static final String REQUEST = "request";
 
 /** The Constant Kritique. */
 public static final String KQ_KEY = "kritique";
 
 /** The Constant UNIT. */
 public static final String UNIT = "unit";
 
 /** The Constant UNITTEXT. */
 public static final String UNITTEXT = "unitText";
 
 /** The Constant UNITTEXT. */
 public static final String VALUE = "value";
 
 /** The Constant RETAIL. */
 public static final String RETAIL = "retail";
 
 /** The Constant RETAIL_LABEL_VALUE. */
 public static final String RETAIL_LABEL_VALUE = "productListing.retailLabel";
 
 /** The Constant TEXT. */
 public static final String TEXT = "text";
 
 /** The Constant ProductID. */
 public static final String PRODUCT_ID = "productID";
 
 /** The Constant ProductOverview_Title. */
 public static final String PRODUCT_OVERVIEW_TITLE = "productOverviewtitle";
 
 /** The Constant PRODUCT_OVERVIEW_APPLICATION_ID. */
 public static final String PRODUCT_OVERVIEW_APPLICATION_ID = "productOverviewApplicationID";
 
 /** The Constant PRODUCT_OVERVIEW_SMART_PRODUCT_ID. */
 public static final String PRODUCT_OVERVIEW_SMART_PRODUCT_ID = "productOverview_SmartProduct_ID3";
 
 /** The Constant LABEL. */
 public static final String LABEL = "label";
 
 /** The Constant DESCRIPTION. */
 public static final String DESCRIPTION = "description";
 
 /** The Constant ZOOM_ENABLED. */
 public static final String ZOOM_ENABLED = "zoomEnabled";
 
 /** The Constant INFORMATION. */
 public static final String INFORMATION = "information";
 
 /** The Constant CATEGORY. */
 public static final String CATEGORY = "primaryCategory";
 
 /** The Constant CUSTOM. */
 public static final String CUSTOM = "custom";
 
 /** The Constant ATTR. */
 public static final String ATTR = "attr";
 
 /** The Constant ACTION. */
 public static final String ACTION = "action";
 
 /** The Constant TRACKEVENT. */
 public static final String TRACKEVENT = "trackevent";
 
 /** The Constant CLICKTOACTION. */
 public static final String CLICKTOACTION = "clickToAction";
 
 /** The Constant SHOPNOW. */
 public static final String SHOPNOW = "shopNow";
 
 /** The Constant SERVICE_PROVIDER_NAME. */
 public static final String SERVICE_PROVIDER_NAME = "serviceProviderName";
 
 /** The Constant ISPRODUCT_OVERVIEW_RATINGANDREVIEW_ENABLED. */
 public static final String ISPRODUCT_OVERVIEW_RATINGANDREVIEW_ENABLED = "isProductOverviewRatingAndReviewsEnabled";
 
 /** The Constant APIKEY. */
 public static final String APIKEY = "apiKey";
 
 /** The Constant BRAND_ID. */
 public static final String BRAND_ID = "brandId";
 
 /** The Constant APPLICATION_ID. */
 public static final String APPLICATION_ID = "applicationid";
 
 /** The Constant ENABLED. */
 public static final String ENABLED = "enabled";
 
 /** The Constant ISRRPENABLED. */
 public static final String ISRRPENABLED = "isRRPEnabled";
 
 /** The Constant RATING_AND_REVIEWS. */
 public static final String RATING_AND_REVIEWS = "ratingAndReviews";
 /** The Constant SKUID. */
 public static final String SKUID = "skuid";
 
 /** The Constant SHORT_IDENTIFIER_VALUE. */
 public static final String SHORT_IDENTIFIER_VALUE = "shortIdentifierValue";
 
 /** The Constant LABEL_INSIGHT_ID. */
 public static final String LABEL_INSIGHT_ID = "labelInsightId";
 
 /** The Constant SKU. */
 public static final String SKU = "sku";
 
 /** The Constant ISBINPRICEENABLED. */
 public static final String ISBINPRICEENABLED = "isBINPriceEnabled";
 
 /** The Constant ISNOTADAPTIVEIMAGE. */
 public static final String ISNOTADAPTIVEIMAGE = "isNotAdaptiveImage";
 
 /** The Constant IS_PRODUCT_OVERVIEW_ZOOM_ENABLED. */
 public static final String IS_PRODUCT_OVERVIEW_ZOOM_ENABLED = "isProductOverviewZoomEnabled";
 /** The Constant PRODUCT_SIZE. */
 public static final String PRODUCT_SIZE = "productSize";
 
 /** The Constant SMART_PRODUCT_ID. */
 public static final String SMART_PRODUCT_ID = "smartProductId";
 
 /** The Constant PRODUCT_DETAIL_MAP. */
 public static final String PRODUCT_DETAIL_MAP = "productsDetail";
 
 /** The Constant PRODUCT_RATINGS_MAP. */
 public static final String PRODUCT_RATINGS_MAP = "ratings";
 
 /** The Constant ZOOM_MAP. */
 public static final String ZOOM_MAP = "zoom";
 
 /** The Constant PRODUCT_MAP. */
 public static final String PRODUCT_MAP = "product";
 
 /** The Constant STATIC_MAP. */
 public static final String STATIC_MAP = "static";
 
 /** The Constant SHOPNOW_MAP. */
 public static final String SHOPNOW_MAP = "shopNow";
 
 /** The Constant SHOPNOW_MAP. */
 public static final String ZOOM_IMAGE_SIZE = "zoomImageSize";
 
 /** The Constant REVIEW_MAP. */
 public static final String REVIEW_MAP = "review";
 
 /** The Constant IS_RATING_AND_REVIEW_ENABLED. */
 public static final String IS_RATING_AND_REVIEW_ENABLED = "isRatingAndReviewEnabled";
 
 /** The Constant CURRENCY_MAP. */
 public static final String CURRENCY_MAP = "currencySymbol";
 
 /** The Constant RETAIL_LABEL. */
 public static final String RETAIL_LABEL = "productOverview.retailLabel";
 
 /** The Constant ZOOM_LABEL. */
 public static final String ZOOM_LABEL = "productOverview.zoomLabel";
 
 /** The Constant ZOOM. */
 public static final String ZOOM = "zoom";
 
 /** The Constant ZOOM_CATEGORY. */
 public static final String ZOOM_CATEGORY = "Zoom";
 
 /** The Constant GLOBAL_CONFIGURATION_CATEGORY. */
 public static final String GLOBAL_CONFIGURATION_CATEGORY = "productOverview";
 
 /** The Constant THUMB_IMAGE_LABEL. */
 public static final String THUMB_IMAGE_LABEL = "thumbImage";
 
 /** The Constant THUMB_IMAGE_LABEL. */
 public static final String SHOW_PRICE = "showPrice";
 
 /** The Constant BACKGROUND_IMAGE. */
 public static final String BACKGROUND_IMAGE = "backgroundImage";
 
 /** The Constant SIZE. */
 public static final String SIZE = "size";
 
 /** The Constant PRODUCT_OVERVIEW. */
 public static final String PRODUCT_OVERVIEW = "product_overview";
 
 /** The Constant FALSE. */
 public static final String FALSE = "false";
 
 /** The Constant TRUE. */
 public static final String TRUE = "true";
 /** The Constant CURRENCY. */
 public static final String CURRENCY = "currency";
 
 /** The Constant CURRENCY. */
 public static final String CURRENCY_SYMBOL = "currencySymbol";
 
 /** The Constant RRP. */
 public static final String RRP = "RRP";
 
 /** The Constant TEXT_LINK. */
 public static final String TEXT_LINK = "textLink";
 
 /** The Constant PRODUCT_LIST_MAP. */
 public static final String PRODUCT_LIST_MAP = "productList";
 
 /** The Constant PRODUCT_COUNT_LABEL. */
 public static final String PRODUCT_COUNT_LABEL = "productCountlabel";
 
 /** The Constant MORELINK_MAP. */
 public static final String MORELINK_MAP = "moreLink";
 
 /** The Constant CONTAINERTAG. */
 public static final String CONTAINERTAG = "containertag";
 
 /** The Constant MASTER_FILTER. */
 public static final String MASTER_FILTER = "masterfilters";
 
 /** The Constant COMPONENT_TITLE. */
 public static final String COMPONENT_TITLE = "componentTitle";
 
 /** The Constant DISPLAY_COUNT. */
 public static final String DISPLAY_COUNT = "displayCount";
 
 /** The Constant AJAX_URL. */
 public static final String AJAX_URL = "ajaxUrl";
 
 /** The Constant ANCHOR_LINK. */
 public static final String ANCHOR_LINK = "anchorLinkNavigation";
 
 /** The Constant SHOWAS_NAVIGATION. */
 public static final String SHOWAS_NAVIGATION = "showAsNavigation";
 
 /** The Constant RELEASE_DATE. */
 public static final String RELEASE_DATE = "releaseDate";
 
 /** The Constant ISNEWPRODUCT. */
 public static final String ISNEWPRODUCT = "isNewProduct";
 
 /** The Constant PRODUCT_TAG. */
 public static final String PRODUCT_TAG = "productTag";
 
 /** The Constant ISCAROUSELENABLED. */
 public static final String ISCAROUSELENABLED = "isCarouselEnabled";
 
 /** The Constant PRODUCTOVERVIEW. */
 public static final String PRODUCTOVERVIEW = "productOverview";
 
 /** The Constant TAG_ID. */
 public static final String TAG_ID = "id";
 
 /** The Constant TAG_PATH. */
 public static final String TAG_PATH = "path";
 
 /** The Constant THUMBNAIL_IMAGE_SUFFIX. */
 public static final String THUMBNAIL_IMAGE_SUFFIX = "thumbnailImageSuffix";
 
 /** The Constant IS_NEW. */
 public static final int DAYS_DIFF = 90;
 
 /** The Constant MULtiplication_CONSTANT. */
 public static final long MULTIPLICATION_CONSTANT = 24 * 60 * 60 * 1000;
 
 /** The Constant NEW_PRODUCT_LABEL. */
 public static final String NEW_PRODUCT_LABEL = "productListing.newLabel";
 
 /** The Constant QUICK_VIEW_LABEL. */
 public static final String QUICK_VIEW_LABEL = "quickViewLabel";
 
 /** The Constant LINK_TEXT_LABEL. */
 public static final String LINK_TEXT_LABEL = "productListing.linkTextLabel";
 
 /** The Constant QUICK_VIEW. */
 public static final String QUICK_VIEW = "productListing.quickViewlabel";
 
 /** The Constant RATING_UNIQUE_ID. */
 public static final String RATING_UNIQUE_ID = "uniqueId";
 
 /** The Constant RATING_IDENTIFIER_TYPE. */
 public static final String RATING_IDENTIFIER_TYPE = "identifierType";
 
 /** The Constant RATING_IDENTIFIER_VALUE. */
 public static final String RATING_IDENTIFIER_VALUE = "identifierValue";
 
 /** The Constant RATING_ENTITY_TYPE. */
 public static final String RATING_ENTITY_TYPE = "entityType";
 
 /** The Constant RATING_VIEW_TYPE. */
 public static final String RATING_VIEW_TYPE = "viewType";
 
 /** The Constant RATING_REVIEW_VIEW_TYPE. */
 public static final String RATING_REVIEW_VIEW_TYPE = "ratingAndReviewViewType";
 
 /** The Constant KQ_VIEW_TYPE. */
 public static final String KQ_VIEW_TYPE = "KritiqueViewType";
 
 /** The Constant IMAGE_ALT_TEXT. */
 public static final String IMAGE_ALT_TEXT = "altText";
 
 /** The Constant KQSEO_ENABLED. */
 public static final String KQSEO_ENABLED = "KQSEOenabled";
 /** The Constant kritique key api key. */
 public static final String API_KEY = "apiKey";
 /** The Constant kritique key locale id. */
 public static final String LOCALE_ID = "localeId";
 /** The Constant kritique key records count. */
 public static final String RECORDS_COUNT = "recordsCount";
 /** The Constant kritique key sitesource. */
 public static final String SITE_SOURCE = "sitesource";
 /** The Constant kritique key syndication. */
 public static final String SYNDICATION = "syndication";
 /** The Constant kritique key pageNo. */
 public static final String PAGE_NUMBER = "pageNo";
 /** The Constant kritique key sortBy. */
 public static final String SORT_BY = "sortBy";
 /** The Constant product source id. */
 public static final String SOURCE_ID = "ProductSourceID";
 
 /** The Constant KQ_API_BASE_PATH. */
 public static final String KQ_API_BASE_PATH = "kritiqueApiBasePath";
 /** The Constant KQ_REFERER. */
 public static final String REFERER = "referer";
 
 /** The Constant CTA_LABEL. */
 public static final String TX1_URL = "customizationPage";
 
 /** The Constant LABEL_TEXT. */
 public static final String BANNER_TEXT = "buttonLabel";
 
 /** The Constant GLOBAL_CONFIG_CAT. */
 public static final String CTA_CONFIG_CAT = "customisedcta";
 
 /** The Constant PERSONALISED_PRICE. */
 public static final String PERSONALISED_PRICE = "personalisedPrice";
 
 /** The Constant TOOLTIP_TEXT. */
 public static final String TOOLTIP_TEXT = "tooltipText";
 
 /** The Constant DELIVERY_CONDITION. */
 public static final String DELIVERY_CONDITION = "deliveryConditionCopy";
 
 /** The Constant PERSONALISED_DESC. */
 public static final String PERSONALISED_DESC = "personalisationDesc";
 
 /** The Constant CUSTOMISED_CTA. */
 public static final String CUSTOMISED_CTA = "customiseCTA";
 
 /** The Constant BANNER_KEY. */
 public static final String BANNER_KEY = "customizedCTA.banner";
 
 /** The Constant CUSTOMIZABLE. */
 public static final String CUSTOMISABLE = "customisable";
 
 /** The Constant PERSONALIZABLE. */
 public static final String PERSONALIZABLE = "productListing.personalizable";
 
 /** The Constant E GIFTING CATEGORY. */
 public static final String EGIFTING_CAT = "egifting";
 
 /** The Constant ENCRYPTION_KEY. */
 public static final String ENCRYPTION_KEY = "encryptionKey";
 
 /** The Constant EAN. */
 public static final String EAN = "ean";
 
 /** The Constant CUSTOMISED_PRICE. */
 public static final String CUSTOMISED_PRICE = "customisedPrice";
 
 /** The Constant CUSTOMISED_DELIVERY_PRICE. */
 public static final String CUSTOMISED_DELIVERY_PRICE = "customisedDeliveryPrice";
 
 /** The Constant ENCRYPTED_DELIVERY_PRICE. */
 public static final String ENCRYPTED_DELIVERY_PRICE = "encryptedDeliveryPrice";
 
 /** The Constant CUSTOMISED_WRAPPING_PRICE. */
 public static final String CUSTOMISED_WRAPPING_PRICE = "customisedWrappingPrice";
 
 /** The Constant ENCRYPTED_WRAPPING_PRICE. */
 public static final String ENCRYPTED_WRAPPING_PRICE = "encryptedWrappingPrice";
 
 /** The Constant ENCRYPTED_PRICE. */
 public static final String ENCRYPTED_PRICE = "encryptedPrice";
 
 /** The Constant DELIVERY_TOOL_TIP. */
 public static final String DELIVERY_TOOL_TIP = "deliveryTooltip";
 
 /** The Constant DELIVERY_CONDITIONS_COPY. */
 public static final String DELIVERY_CONDITIONS_COPY = "deliveryConditionsCopy";
 
 /** The Constant GIFT_WRAPS. */
 public static final String GIFT_WRAPS = "giftWraps";
 
 /** The Constant GIFT_CONFIGURATOR. */
 public static final String GIFT_CONFIGURATOR = "giftConfigurator";
 
 /** The Constant EGIFTING_FREE_LABEL. */
 public static final String EGIFTING_FREE_LABEL = "freeLabel";
 
 /** The Constant EGIFTING_FREE_KEY. */
 public static final String EGIFTING_FREE_KEY = "egifting.freeLabel";
 
 /** The Constant DELIVERY_PRICE. */
 public static final String DELIVERY_PRICE = "deliveryPrice";
 
 /** The Constant WRAPPING_DESC_TEXT. */
 public static final String WRAPPING_DESC_TEXT = "customizeGiftWrapDescription";
 
 /** The Constant DELIVERY_DETAIL. */
 public static final String DELIVERY_DETAIL = "deliveryDetail";
 
 /** The Constant E_GIFTING_CAT. */
 public static final String E_GIFTING_CAT = "egifting";
 
 /** The Constant CUSTOM_IMAGE. */
 public static final String CUSTOM_IMAGE = "customImage";
 
 /** The Constant ALLERGY. */
 public static final String ALLERGY = "allergy";
 
 /** The Constant INGREDIENTS. */
 public static final String INGREDIENTS = "ingredients";
 
 public static final String INGREDIENTS_ARRAY_FLAG = "isIngredientsArray";
 
 /** The Constant NUTRITIONFACTS. */
 public static final String NUTRITIONFACTS = "nutritionFacts";
 
 /** The Constant PRODUCTNAMES. */
 public static final String PRODUCTNAMES = "productNames";
 
 /** The Constant NUTRITIONAL_FACTS_PREFIX. */
 public static final String NUTRITIONAL_FACTS_PREFIX = "NutritionFacts.";
 
 /** The Constant PRODUCT_INFO_PREFIX. */
 public static final String PRODUCT_INFO_PREFIX = "productInfo.";
 
 /** The Constant PRODUCT_NAME. */
 public static final String PRODUCT_NAME = "productName";
 
 /** The Constant NUTRITIONAL_DETAILS. */
 public static final String NUTRITIONAL_DETAILS = "nutritionalDetails";
 
 /** The Constant DETAILS. */
 public static final String DETAILS = "details";
 
 /** The Constant DISCLAIMER. */
 public static final String DISCLAIMER = "disclaimer";
 
 /** The Constant FACT_TABLE. */
 public static final String FACT_TABLE = "factTable";
 
 /** The Constant PRODUCT_INFOLABEL. */
 public static final String PRODUCT_INFOLABEL = "productInfoLabel";
 /** The Constant PRODUCT_INFO_LABEL. */
 public static final String PRODUCT_INFO_LABEL = "productListing.productInfoLabel";
 
 /**
  * name.
  */
 public static final String NAME = "name";
 
 /** The Constant SEO_ENABLED. */
 public static final String SEO_ENABLED = "SEOenabled";
 
 /** The Constant EAN_SELECTOR. */
 public static final String EAN_SELECTOR = "eanSelector";
 
 /** The Constant STORE_LOCATOR_RESULT_URL. */
 public static final String STORE_LOCATOR_RESULT_URL = "storeLocatorResultUrl";
 /** The Constant SHOW PRODUCT INFO LABEL. */
 public static final String SHOW_PRODUCTINFO_LABEL = "showProductInfoLabel";
 /** The Constant SMART_LABEL. */
 public static final String SMART_LABEL = "smartLabel";
 
 /** The Constant ILS_PRODUCT_NAME. */
 public static final String ILS_PRODUCT_NAME = "ilsProductName";
 
 /** The Constant AWARDS. */
 public static final String AWARDS = "awards";
 
 /** The Constant LONG_PAGE_DESCRIPTION. */
 public static final String LONG_PAGE_DESCRIPTION = "longPageDescription";
 
 /** The Constant ABOUT_THIS_PRODUCT_TITLE. */
 public static final String ABOUT_THIS_PRODUCT_TITLE = "aboutThisProductTitle";
 
 /** The Constant ABOUT_THIS_PRODUCT_BULLETS. */
 public static final String ABOUT_THIS_PRODUCT_BULLETS = "aboutThisProductBullets";
 
 /** The Constant ABOUT_THIS_PRODUCT_DESCRIPTION. */
 public static final String ABOUT_THIS_PRODUCT_DESCRIPTION = "aboutThisProductDescription";
 
 /** The Constant PERFECT_FOR_TITLE. */
 public static final String PERFECT_FOR_TITLE = "perfectForTitle";
 
 /** The Constant PERFECT_FOR_HEADINGS_AND_DESCRIPTION. */
 public static final String PERFECT_FOR_HEADINGS_AND_DESCRIPTION = "perfectForHeadingsAndDescription";
 
 /** The Constant HOW_TO_USE_TITLE. */
 public static final String HOW_TO_USE_TITLE = "howToUseTitle";
 
 /** The Constant HOW_TO_USE_DESCRIPTION. */
 public static final String HOW_TO_USE_DESCRIPTION = "howToUseDescription";
 
 /** The Constant TRY_THIS_TITLE. */
 public static final String TRY_THIS_TITLE = "tryThisTitle";
 
 /** The Constant TRY_THIS_DESCTIPTION. */
 public static final String TRY_THIS_DESCTIPTION = "tryThisDesctiption";
 
 /** The Constant INGREDIENTS_DISCLAIMER. */
 public static final String INGREDIENTS_DISCLAIMER = "ingredientsDisclaimer";
 
 /** The Constant SHORT_PRODUCT_DESCRIPTION. */
 public static final String SHORT_PRODUCT_DESCRIPTION = "shortProductDescription";
 
 /** The Constant EAN_PARENT. */
 public static final String EAN_PARENT = "EANparent";
 
 /** The Constant PRODUCT_IMAGE. */
 public static final String PRODUCT_IMAGE = "productImage";
 
 /** The Constant ALT_TEXT_IMAGES. */
 public static final String ALT_TEXT_IMAGES = "altTextImages";
 
 /** The Constant IMAGE_TYPE. */
 public static final String IMAGE_TYPE = "imageType";
 
 /** The Constant IDENTIFIER_TYPE. */
 public static final String IDENTIFIER_TYPE = "identifierType";
 
 /** The Constant IDENTIFIER_VALUE. */
 public static final String IDENTIFIER_VALUE = "identifierValue";
 
 /** The Constant PRODUCTION_DATE. */
 public static final String PRODUCTION_DATE = "productionDate";
 
 /** The Constant SHORT_PAGE_DESCRIPTION. */
 public static final String SHORT_PAGE_DESCRIPTION = "shortPageDescription";
 
 /** The Constant COMMUNITY_PRODUCT_QA_CAT. */
 public static final String COMMUNITY_PRODUCT_QA_CAT = "communityProductQnA";
 
 /** The Constant EGIFTING_CONFIGURATOR_CAT. */
 public static final String EGIFTING_CONFIGURATOR_CAT = "eGiftingConfigurator";
 
 /** The Constant GIFT_WRAP_CONDITIONS_COPY. */
 public static final String GIFT_WRAP_CONDITIONS_COPY = "giftWrapConditionsCopy";
 
 /** The Constant PRODUCT_GIFT_DETAILS_MAP. */
 public static final String PRODUCT_GIFT_DETAILS_MAP = "productGiftDetails";
 
 /** The Constant PRODUCT_VARIANTS_MAP. */
 public static final String PRODUCT_VARIANTS_MAP = "productVariantsDetail";
 
 /** The Constant PRODUCT_VARIANTS_MAP. */
 public static final String PRICE_ENABLED = "priceEnabled";
 public static final String PRICE_CATEGORY = "RRP";
 /** The Constant DISABLE_BUY_IT_NOW. */
 public static final String DISABLE_BUY_IT_NOW = "disableBuyItNow";
 
 /** The Constant PRODUCT_IMAGES. */
 public static final String PRODUCT_IMAGES = "productImages";
 
 public static final String PRODUCT_IMAGES_ZOOM_ENABLED = "zoomEnabled";
 
 /** The Constant FILTER_PRODUCTS_LABEL. */
 public static final String FILTER_PRODUCTS_LABEL = "filterProductsLabel";
 
 /** The Constant HIDE_FILTERS_LABEL. */
 public static final String HIDE_FILTERS_LABEL = "hideFiltersLabel";
 
 public static final String VIDEO_ID = "videoId";
 
 /**
  * Instantiates a new product constants.
  */
 private ProductConstants() {
  
 }
 
}
