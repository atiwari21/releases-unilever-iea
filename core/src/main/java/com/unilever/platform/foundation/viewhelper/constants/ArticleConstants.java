/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.constants;

/**
 * The Class ArticleConstants.
 */
public class ArticleConstants {
 
 /** The Constant ARTICLE_UNIQUE_ID. */
 public static final String ARTICLE_UNIQUE_ID = "articleUniqueId";
 
 /** The Constant ARTICLE_ENTITY_NAME. */
 public static final String ARTICLE_ENTITY_NAME = "articleEntityName";
 
 /** The Constant ARTICLE_ENTITY_DESC. */
 public static final String ARTICLE_ENTITY_DESC = "articleEntityDescription";
 
 /** The Constant ARTICLE_ENTITY_URL. */
 public static final String ARTICLE_ENTITY_URL = "articleEntityURL";
 
 /** The Constant ARTICLE_IMAGE_URL. */
 public static final String ARTICLE_IMAGE_URL = "articleImageURL";
 
 /** The Constant ARTICLE_CATEGORY_SOURCE_ID. */
 public static final String ARTICLE_CATEGORY_SOURCE_ID = "articleCategorySourceId";
 
 /** The Constant ARTICLE_CATEGORY_NAME. */
 public static final String ARTICLE_CATEGORY_NAME = "articleCategoryName";
 
 /** The Constant ARTICLE_CATEGORY_PAGE_URL. */
 public static final String ARTICLE_CATEGORY_PAGE_URL = "articleCategoryPageURL";
 
 /** The Constant ARTICLE. */
 public static final String ARTICLE = "article";
 
 /** The Constant ARTICLE_PAGE. */
 public static final String ARTICLE_PAGE = "articlePage";
 
 /** The Constant ARTICLE_RATINGS_MAP. */
 public static final String ARTICLE_RATINGS_MAP = "ratings";
 
 /** The Constant ARTICLE_RATING_AND_REVIEWS. */
 public static final String ARTICLE_RATING_AND_REVIEWS = "articleRatingAndReviews";
 
 /** The Constant ENABLED. */
 public static final String ENABLED = "enabled";
 
 /** The Constant ARTICLE_MAP. */
 public static final String ARTICLE_MAP = "article";
 
 /** The Constant SEO_ENABLED. */
 public static final String SEO_ENABLED = "SEOenabled";
 
 /** The Constant SERVICE_PROVIDER_NAME. */
 public static final String SERVICE_PROVIDER_NAME = "serviceProviderName";
 
 /** The Constant VIEW_TYPE. */
 public static final String VIEW_TYPE = "viewType";
 
 /** The Constant ENTITY_TYPE. */
 public static final String ENTITY_TYPE = "entityType";
 
 /** The Constant ARTICLE_CONFIG_CAT. */
 public static final String ARTICLE_CONFIG_CAT = "articleConfiguration";
 
 /** The Constant ARTICLE_CONTENT_TYPE. */
 public static final String ARTICLE_CONTENT_TYPE = "articleContentTypes";
 
 /** The Constant ARTICLE_PAGE_SELECTOR. */
 public static final String ARTICLE_PAGE_SELECTOR = "articlePageSelector";
 
 /** The Constant REVIEW_ENABLED. */
 public static final String REVIEW_ENABLED = "reviewEnable";
 
 /**
  * Instantiates a new article constants.
  */
 private ArticleConstants() {
  
 }
 
}
