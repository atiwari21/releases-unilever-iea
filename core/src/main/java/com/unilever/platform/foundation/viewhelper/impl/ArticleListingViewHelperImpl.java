/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.tagging.Tag;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.utils.CollectionUtils;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.core.dto.SearchDTO;
import com.unilever.platform.aem.foundation.core.dto.TaggingSearchCriteria;
import com.unilever.platform.aem.foundation.core.service.SearchParam;
import com.unilever.platform.aem.foundation.core.service.SearchService;
import com.unilever.platform.foundation.components.SearchResultComparator;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.constants.CommonConstants;
import com.unilever.platform.foundation.constants.ContainerTagConstants;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.constants.MultipleRelatedArticlesConstants;
import com.unilever.platform.foundation.viewhelper.constants.MultipleRelatedProductsConstants;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;
import com.unilever.platform.foundation.viewhelper.productoverview.config.ConfigureProductImageRenditionService;

/**
 * <p>
 * Responsible for reading author input for prduct copy component and generating a list of collapsible sections setting in pageContext for the view.
 * Input can be manual or reading property attributes from product data.
 * </p>
 */
@Component(description = "ArticleListingViewHelperImpl", immediate = true, metatype = true, label = "ArticleListingViewHelperImpl")
@Service(value = { ArticleListingViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.ARTICLE_LISTING, propertyPrivate = true),
  
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class ArticleListingViewHelperImpl extends MultipleRelatedArticlesBaseViewHelperImpl {
 
 /**
  * logger object.
  */
 @Reference
 ConfigurationService configurationService;
 
 /** The search service. */
 @Reference
 SearchService searchService;
 
 /** The configure product image rendition service. */
 @Reference
 ConfigureProductImageRenditionService configureProductImageRenditionService;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ArticleListingViewHelperImpl.class);
 
 /** The Constant globalConfigCat. */
 private static final String GLOBAL_CONFIG_CAT = "articleListingVariation";
 
 /** The Constant PURPOSE_TAGS. */
 private static final String PURPOSE_TAGS = "purposeTags";
 
 /** The Constant IS_CATEGORY_TITLE. */
 private static final String IS_CATEGORY_TITLE = "isCategoryTitle";
 
 /** The Constant CTA_LABEL. */
 private static final String CTA_LABEL = "cta";
 
 /** The Constant CTA_LABEL_VAL. */
 private static final String CTA_LABEL_VAL = "articleListing.ctaLabel";
 
 private static final String SEEMORE_LABEL_VAL = "articleListing.seeMoreLabel";
 
 /** The Constant SEE_MORE_URL. */
 private static final String SEE_MORE_URL = "seeMoreUrl";
 
 /** The Constant SEE_MORE. */
 private static final String SEE_MORE = "seeMore";
 
 /** The Constant MAX_MOBILE_COUNT. */
 private static final String MAX_MOBILE_COUNT = "maxMobileCount";
 
 /** Constant for retrieving article type. */
 private static final String ARTICLE_TYPE = "mode";
 
 /** Constant for retrieving Context. */
 private static final String CONTEXT = "context";
 
 /** Constant for retrieving Description. */
 private static final String DESCRIPTION = "description";
 
 /**
  * Constant for setting featured article type auto.
  */
 private static final String AUTO = "Auto";
 
 /**
  * Constant for setting see all label for Mobile View.
  */
 private static final String SEE_ALL_LABEL = "seeAllLabel";
 
 /*
  * (non-Javadoc)
  * 
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData (java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("Article Listing View Helper #processData called .");
  
  Page currentPage = getCurrentPage(resources);
  Map<String, Object> properties = getProperties(content);
  I18n i18n = getI18n(resources);
  String ctaLabel = i18n.get(CTA_LABEL_VAL);
  String articleType = MapUtils.getString(properties, "articleType") != null ? MapUtils.getString(properties, "articleType") : StringUtils.EMPTY;
  String context = MapUtils.getString(properties, CONTEXT) != null ? MapUtils.getString(properties, CONTEXT) : StringUtils.EMPTY;
  String description = MapUtils.getString(properties, DESCRIPTION) != null ? MapUtils.getString(properties, DESCRIPTION) : StringUtils.EMPTY;
  Map<String, Object> articleListMap = new HashMap<String, Object>();
  
  Map<String, String> configMap = ComponentUtil.getConfigMap(currentPage, GLOBAL_CONFIG_CAT);
  
  List<Map<String, Object>> articleListData = new ArrayList<Map<String, Object>>();
  List<String> articlesPagePathList = new ArrayList<String>();
  
  List<SearchDTO> articlesResultList = new ArrayList<SearchDTO>();
  List<String> manualArticleListData = new ArrayList<String>();
  
  if (AUTO.equals(articleType) || StringUtils.isEmpty(articleType)) {
   List<String> contentType = new ArrayList<String>();
   List<String> purposeTagsList = new ArrayList<String>();
   contentType = getContentTypeList(properties);
   populatePurposeTags(currentPage, purposeTagsList, properties);
   
   LOGGER.debug("ArticleListingViewHelperImpl configMap = " + configMap);
   articlesResultList = getArticlesPagePathList(configMap, purposeTagsList, properties, contentType, resources);
   articlesPagePathList = getSortedFinalListBasedOnComparator(articlesResultList, configMap, properties, purposeTagsList);
   
   articleListData = getArticleListData(articlesPagePathList, currentPage, resources);
  } else {
   manualArticleListData = getArticleManual(properties);
   articleListData = getArticleListData(manualArticleListData, currentPage, resources);
   
  }
  
  String isCategoryTitle = MapUtils.getString(properties, IS_CATEGORY_TITLE) != null ? MapUtils.getString(properties, IS_CATEGORY_TITLE) : "false";
  articleListMap.put(IS_CATEGORY_TITLE, isCategoryTitle);
  articleListMap.put(ARTICLE_TYPE, articleType);
  articleListMap.put(CONTEXT, context);
  articleListMap.put(DESCRIPTION, description);
  articleListMap.put(ProductConstants.TITLE, MapUtils.getString(properties, ProductConstants.TITLE));
  articleListMap.put(MultipleRelatedArticlesConstants.ARTICLES, articleListData.toArray());
  if (MapUtils.getString(properties, ProductConstants.DISPLAY_COUNT) != null) {
   articleListMap.put(ProductConstants.DISPLAY_COUNT, Integer.parseInt(MapUtils.getString(properties, ProductConstants.DISPLAY_COUNT)));
  } else {
   articleListMap.put(ProductConstants.DISPLAY_COUNT, StringUtils.EMPTY);
   
  }
  articleListMap.put(UnileverConstants.ANCHOR_NAVIGATION, ComponentUtil.getAnchorLinkMap(properties));
  articleListMap.put(CTA_LABEL, ctaLabel);
  articleListMap.put(SEE_MORE, getSeeMoreMapProperties(i18n, properties, getResourceResolver(resources), getSlingRequest(resources)));
  articleListMap.put(SEE_ALL_LABEL, i18n.get("articleListing.seeAllLabel"));
  articleListMap.put(MAX_MOBILE_COUNT, StringUtils.isNotBlank(getMaxMobileCount(configMap)) ? Integer.parseInt(getMaxMobileCount(configMap))
    : StringUtils.EMPTY);
  
  Map<String, Object> containerTagMap = new LinkedHashMap<String, Object>();
  containerTagMap.put(ContainerTagConstants.LOAD_MORE, ComponentUtil.getContainerTagMap(currentPage, ContainerTagConstants.LOAD_MORE, "Article"));
  articleListMap.put(UnileverConstants.CONTAINER_TAG, containerTagMap);
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE), articleListMap);
  return data;
  
 }
 
 private List<String> getContentTypeList(Map<String, Object> articleListingProperties) {
  List<String> propertyNames = new ArrayList<String>();
  propertyNames.add("contentType");
  List<Map<String, String>> contentSectionsList = CollectionUtils.convertMultiWidgetToList(articleListingProperties, propertyNames);
  return prepareContentSectionList(contentSectionsList);
  
 }
 
 private List<String> prepareContentSectionList(List<Map<String, String>> contentSectionsList) {
  List<String> finalSectionsList = new ArrayList<String>();
  for (Map<String, String> sectionMap : contentSectionsList) {
   String contenttype = sectionMap.get(UnileverConstants.CONTENT_TYPE);
   finalSectionsList.add(contenttype);
  }
  
  return finalSectionsList;
  
 }
 
 private List<String> getArticleManual(Map<String, Object> articleListingProperties) {
  List<String> propertyNames = new ArrayList<String>();
  propertyNames.add("articlePath");
  List<Map<String, String>> articleSectionsList = CollectionUtils.convertMultiWidgetToList(articleListingProperties, propertyNames);
  return prepareSectionList(articleSectionsList);
 }
 
 private List<String> prepareSectionList(List<Map<String, String>> articleSectionsList) {
  List<String> finalSectionsList = new ArrayList<String>();
  for (Map<String, String> sectionMap : articleSectionsList) {
   String articlePath = sectionMap.get("articlePath");
   finalSectionsList.add(articlePath);
  }
  
  return finalSectionsList;
 }
 
 /**
  * Gets the max mobile count.
  * 
  * 
  * @param configMap
  *         the config map
  * @return the max mobile count
  */
 private String getMaxMobileCount(Map<String, String> configMap) {
  return !StringUtils.isBlank(configMap.get(MAX_MOBILE_COUNT)) ? configMap.get(MAX_MOBILE_COUNT) : StringUtils.EMPTY;
  
 }
 
 /**
  * Populate purpose tags.
  * 
  * @param currentPage
  * 
  * 
  * @param purposeTagsList
  *         the purpose tags list
  * @param dialogProperties
  *         the dialog properties
  */
 private void populatePurposeTags(Page currentPage, List<String> purposeTagsList, Map<String, Object> dialogProperties) {
  
  String[] prop = ComponentUtil.getPropertyValueArray(dialogProperties, PURPOSE_TAGS);
  if (prop != null && prop.length > 0) {
   for (String string : prop) {
    purposeTagsList.add(string);
    
   }
  } else {
   Tag[] currentPageTagsArr = currentPage.getTags();
   for (Tag currentPageTag : currentPageTagsArr) {
    String tagId = currentPageTag.getTagID();
    LOGGER.debug("Inside current page tags with id  {}", tagId);
    purposeTagsList.add(tagId);
   }
   
  }
 }
 
 /**
  * Gets the articles page path list.
  * 
  * 
  * @param currentPage
  *         the current page
  * @param configMap
  *         the config map
  * @param purposeTagsList
  *         the purpose tags list
  * @param properties
  *         the properties
  * @param contentType
  * @return the articles page path list
  */
 private List<SearchDTO> getArticlesPagePathList(Map<String, String> configMap, List<String> purposeTagsList, Map<String, Object> properties,
   List<String> contentType, Map<String, Object> resources) {
  LOGGER.debug("Inside getArticlesPagePathList of ArticleListingViewHelperImpl");
  int offset = 0, limit = CommonConstants.TWENTY;
  Page currentPage = getCurrentPage(resources);
  ResourceResolver resourceResolver = getResourceResolver(resources);
  try {
   limit = StringUtils.isBlank(configMap.get(UnileverConstants.LIMIT)) ? CommonConstants.TWENTY : Integer.parseInt(configMap
     .get(UnileverConstants.LIMIT));
  } catch (NumberFormatException nfe) {
   LOGGER.error("limit for page list must be number");
   LOGGER.error(ExceptionUtils.getStackTrace(nfe));
   
  }
  Map<String, List<String>> propMap = new HashMap<String, List<String>>();
  List<String> propValueList = new ArrayList<String>();
  
  propValueList = contentType;
  propMap.put(UnileverConstants.JCR_CONTENT_CONTENT_TYPE, propValueList);
  List<String> excludeTagIdList = new ArrayList<String>();
  LOGGER.debug("ArticlesListingBaseViewHelperImpl includeTagIdList : " + purposeTagsList.size());
  LOGGER.debug("ArticlesListingBaseViewHelperImpl excludeTagIdList : " + excludeTagIdList.size());
  String currentPagePath = currentPage.getPath();
  
  String[] filterTagsNamespace = ComponentUtil.getFilterTagsNamespace(properties, configMap);
  List<TaggingSearchCriteria> taggingSearchCriteriaList = ComponentUtil.getFilterTagList(currentPage, filterTagsNamespace, purposeTagsList);
  if (taggingSearchCriteriaList != null && !taggingSearchCriteriaList.isEmpty()) {
   List<SearchDTO> resultList = searchService.getMultiTagResults(taggingSearchCriteriaList, excludeTagIdList, currentPagePath, propMap,
     new SearchParam(offset, limit, UnileverConstants.DESCENDING), resourceResolver);
   LOGGER.debug("Valid Pages Path List  = " + resultList.size());
   return resultList;
  } else {
   return new ArrayList<SearchDTO>();
   
  }
  
 }
 
 /**
  * Gets the sorted final list based on comparator.
  * 
  * 
  * @param articlesResultList
  *         the articles result list
  * @param configMap
  *         the config map
  * @param properties
  *         the properties
  * @param purposeTagsList
  *         the purpose tags list
  * @return the sorted final list based on comparator
  */
 protected List<String> getSortedFinalListBasedOnComparator(List<SearchDTO> articlesResultList, Map<String, String> configMap,
   Map<String, Object> properties, List<String> purposeTagsList) {
  List<SearchDTO> articlesResultListFinal = new ArrayList<SearchDTO>();
  
  String orderbyTagId = configMap.get(MultipleRelatedProductsConstants.ORDER_BY) != null ? configMap.get(MultipleRelatedProductsConstants.ORDER_BY)
    : StringUtils.EMPTY;
  String[] orderbyTagIdArr = StringUtils.isNotBlank(orderbyTagId) ? orderbyTagId.split(CommonConstants.COMMA_CHAR) : new String[] {};
  long startTime = System.currentTimeMillis();
  Collections.sort(articlesResultList, new SearchResultComparator(purposeTagsList, orderbyTagIdArr, true));
  long stopTime = System.currentTimeMillis();
  
  LOGGER.info("Time elapsed in soring the result of Article Listing View Helper = " + (stopTime - startTime));
  
  if (MapUtils.getString(properties, ProductConstants.DISPLAY_COUNT) != null) {
   int max = MapUtils.getIntValue(properties, ProductConstants.DISPLAY_COUNT);
   LOGGER.debug("Maximum articles for page list= " + max);
   articlesResultListFinal = articlesResultList.size() > max ? articlesResultList.subList(0, max) : articlesResultList;
  } else {
   articlesResultListFinal = articlesResultList;
   
  }
  return getPathList(articlesResultListFinal);
  
 }
 
 /**
  * Gets the static map properties.
  * 
  * 
  * @param i18n
  * 
  *         the i18n
  * @param properties
  *         the properties
  * @param resourceResolver
  * @return the static map properties
  */
 private Map<String, Object> getSeeMoreMapProperties(I18n i18n, Map<String, Object> properties, ResourceResolver resourceResolver,
   SlingHttpServletRequest slingRequest) {
  Map<String, Object> seeMore = new HashMap<String, Object>();
  seeMore.put(UnileverConstants.LABEL, i18n.get(SEEMORE_LABEL_VAL));
  String url = MapUtils.getString(properties, SEE_MORE_URL) != null ? MapUtils.getString(properties, SEE_MORE_URL) : StringUtils.EMPTY;
  String fullUrl = !url.isEmpty() && url != null ? ComponentUtil.getFullURL(resourceResolver, url, slingRequest) : StringUtils.EMPTY;
  seeMore.put(UnileverConstants.URL, fullUrl);
  LOGGER.debug("Static Map Properties : " + seeMore);
  
  return seeMore;
  
 }
 
 /*
  * (non-Javadoc)
  * 
  * 
  * @see com.unilever.platform.foundation.viewhelper.impl. MultipleRelatedArticlesBaseViewHelperImpl#getSearchService()
  */
 @Override
 protected SearchService getSearchService() {
  
  return this.searchService;
  
 }
 
 /*
  * (non-Javadoc)
  * 
  * 
  * @see com.unilever.platform.foundation.viewhelper.impl. MultipleRelatedArticlesBaseViewHelperImpl#getConfigService()
  */
 @Override
 protected ConfigurationService getConfigService() {
  
  return this.configurationService;
  
 }
}
