/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.constants;

/**
 * The Class MultipleRelatedProductsConstants.
 */
public class MultipleRelatedProductsConstants {
 /** The Constant HEADLINE. */
 public static final String TITLE = "title";
 
 /** The Constant REVIEW. */
 public static final String REVIEW = "review";
 
 /** The Constant STATIC. */
 public static final String STATIC = "static";
 
 /** The Constant OFFSET. */
 public static final String OFFSET = "offset";
 
 /** The Constant LIMIT. */
 public static final String LIMIT = "limit";
 
 /** The Constant ORDER_BY. */
 public static final String ORDER_BY = "orderby";
 
 /** The Constant INCLUDE_SORT_LIST. */
 public static final String INCLUDE_SORT_LIST = "includeSortList";
 
 /**
  * Instantiates a new multiple related products constants.
  */
 private MultipleRelatedProductsConstants() {
  
 }
 
}
