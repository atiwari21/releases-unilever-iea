/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * This class prepares the data map for Virtual Agent ComponentComponent It reads the values from the dialog and passes the info as a json.
 */

@Component(description = "VirtualAgent", immediate = true, metatype = true, label = "VirtualAgent")
@Service(value = { VirtualAgentViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.VIRTUAL_AGENT, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class VirtualAgentViewHelperImpl extends BaseViewHelper {
 
 /** The Constant AUTO_SUGGEST_RESULT_COUNT. */
 private static final String AUTO_SUGGEST_RESULT_COUNT = "autoSuggestResultCount";
 
 /** image key for json */
 private static final String IMAGE = "image";
 
 /** image url key for json */
 private static final String IMAGE_URL2 = "imageUrl";
 
 /** image alt key for json */
 private static final String IMAGE_ALT2 = "imageAlt";
 
 /** image title key for json */
 private static final String IMAGE_TITLE2 = "imageTitle";
 
 /** image title from dialog */
 private static final String IMAGE_TITLE = "waImageTitle";
 
 /** image alt from dialog */
 private static final String IMAGE_ALT = "waImageAlt";
 
 /** image url from dialog */
 private static final String IMAGE_URL = "waImage";
 
 /** intro text read from dialog */
 private static final String INTRO_TEXT = "waIntroText";
 
 /** no results json key */
 private static final String NO_RESULTS = "noResults";
 
 /** no results text */
 private static final String VIRTUAL_AGENT_NO_RESULTS = "virtualAgent.noResults";
 
 /** view introducttion text key for json */
 private static final String INTRODUCTION_TEXT = "introductionText";
 
 /** information available label */
 private static final String INFO_AVAILABLE = "infoAvailable";
 
 /** description title for the result displayed */
 private static final String VIRTUAL_AGENT_RESULTS_DESCRIPTION_TITLE = "virtualAgent.resultsDescriptionTitle";
 
 /** the cta label for the search submit button */
 private static final String VIRTUAL_AGENT_CTA_LABEL = "virtualAgent.ctaLabel";
 
 /** the url for retrieving results */
 private static final String RETRIVAL_QUERY_PARAMS = "retrivalQueryParams";
 
 /** the constant static */
 private static final String STATIC = "static";
 
 /** label for the contact us button */
 private static final String CONTACT_US_CTALABEL = "contactUsCtalabel";
 
 /** link for the contact us page */
 private static final String CONTACT_US_LINK = "contactUsLink";
 
 /** label on contact us cta */
 private static final String CONTACTUS_LABEL = "contactusQuote";
 
 /** title of the results */
 private static final String RESULTS_TITLE = "resultsTitle";
 
 /** the constant search */
 private static final String SEARCH = "search";
 
 /** the constant search input */
 private static final String SEARCH_INPUT = "searchInput";
 
 /** text to appear in the search area */
 private static final String PLACE_HOLDER = "placeHolder";
 
 /** the constant label */
 private static final String LABEL = "label";
 
 /** the constant request servlet */
 private static final String REQUEST_SERVLET = "requestServlet";
 
 /** link to the contact us page */
 private static final String VIRTUAL_AGENT_CONTACT_US_LINK = "waContactUsLink";
 
 /** text to be displayed on contact us cta */
 private static final String VIRTUAL_AGENT_CONTACT_US_CTA_LABEL = "virtualAgent.contactUsCtaLabel";
 
 /** contact us quote */
 private static final String VIRTUAL_AGENT_CONTACT_US_QUOTE = "waContactUsLabel";
 
 /** virtual agent results title */
 private static final String VIRTUAL_AGENT_RESULTS_TITLE = "virtualAgent.resultsTitle";
 
 /** place holder text to appear in the search area */
 private static final String VIRTUAL_AGENT_SEARCH_PLACE_HOLDER = "virtualAgent.searchPlaceHolder";
 
 /** The Constant WEB_ASSIST_BRAND_NAME. */
 private static final String WEB_ASSIST_BRAND_NAME = "webAssistBrandName";
 
 /** The Constant VIRTUAL_AGENT. */
 private static final String VIRTUAL_AGENT = "virtualAgent";
 
 /** The Constant VIRTUAL_AGENT. */
 private static final String BRAND_NAME = "brandName";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(VirtualAgentViewHelperImpl.class);
 
 /** The Constant VIRTUAL_AGENT_RELATED_SEARCH_RESULTS. */
 private static final String VIRTUAL_AGENT_RELATED_SEARCH_RESULTS = "virtualAgent.relatedSearchResults";
 
 /** The Constant RELATED_SEARCH_RESULTS. */
 private static final String RELATED_SEARCH_RESULT = "relatedSearchResult";
 
 /** the i18n */
 private static I18n i18n = null;
 
 private static final int FIVE = 5;
 
 /** reference for results link extractor service */
 @Reference
 VirtualAgentLinkExtractor resultsExtractor;
 
 @Reference
 ConfigurationService configurationService;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("Executing VirtualAgentViewHelperImpl. Inside processData method ");
  Map<String, Object> properties = new HashMap<String, Object>();
  Map<String, Object> webAssistSearchProperties = getProperties(content);
  
  i18n = getI18n(resources);
  
  LOGGER.info("Component found for current page {}", getCurrentResource(resources).getName());
  
  /* reading properties of the component from i18n */
  String ctaLabel = getCtaLabel();
  String searchPlaceHolder = getSearchPlaceHolder();
  String waResultsTitle = getWaResultsTitle();
  String contactUsCtalabel = getContactUsCtaLabel();
  String infoAvailable = getInfoAvailable();
  String relatedSearchResult = getRelatedSearchResult();
  String noResults = getNoResults();
  
  /* reading properties of the component from the dialog */
  String contactUsQuote = getContactUsQuote(webAssistSearchProperties);
  String contactUsLink = getContactUsLink(webAssistSearchProperties);
  String introText = getIntroText(webAssistSearchProperties);
  
  /* reading the suggestion and results urls from OSGI configurations */
  String suggestionHandler = resultsExtractor.getSuggestionUrl()[0];
  String resultsHandler = resultsExtractor.getResultsUrl()[0];
  
  Map<String, Object> imageMap = getImageMap(webAssistSearchProperties);
  Map<String, String> configMap = new HashMap<String, String>();
  try {
   configMap = configurationService.getCategoryConfiguration(getCurrentPage(resources), VIRTUAL_AGENT);
  } catch (ConfigurationNotFoundException e1) {
   LOGGER.debug("Virtual Agent configuration not found",e1);
  }
  
  Map<String, Object> searchMap = new HashMap<String, Object>();
  searchMap.put(REQUEST_SERVLET, suggestionHandler);
  searchMap.put(LABEL, ctaLabel);
  searchMap.put(PLACE_HOLDER, searchPlaceHolder);
  Map<String, Object> searchContainerMap = new HashMap<String, Object>();
  searchContainerMap.put(SEARCH_INPUT, searchMap);
  
  Map<String, Object> staticDataMap = new HashMap<String, Object>();
  staticDataMap.put(INFO_AVAILABLE, infoAvailable);
  staticDataMap.put(RELATED_SEARCH_RESULT, relatedSearchResult);
  staticDataMap.put(RESULTS_TITLE, waResultsTitle);
  staticDataMap.put(CONTACTUS_LABEL, contactUsQuote);
  staticDataMap.put(CONTACT_US_LINK, ComponentUtil.getFullURL(getResourceResolver(resources), contactUsLink, getSlingRequest(resources)));
  staticDataMap.put(CONTACT_US_CTALABEL, contactUsCtalabel);
  staticDataMap.put(INTRODUCTION_TEXT, introText);
  String brandName = MapUtils.getString(configMap, BRAND_NAME, StringUtils.EMPTY);
  int autoSuggestResultCount = MapUtils.getIntValue(configMap, AUTO_SUGGEST_RESULT_COUNT, FIVE);
  
  Map<String, Object> resultsHandlerMap = new HashMap<String, Object>();
  resultsHandlerMap.put(REQUEST_SERVLET, resultsHandler);
  
  /* storing the properties in one map */
  properties.put(SEARCH, searchContainerMap);
  properties.put(STATIC, staticDataMap);
  properties.put(RETRIVAL_QUERY_PARAMS, resultsHandlerMap);
  properties.put(NO_RESULTS, noResults);
  properties.put(IMAGE, imageMap);
  properties.put(WEB_ASSIST_BRAND_NAME, brandName);
  properties.put(AUTO_SUGGEST_RESULT_COUNT, autoSuggestResultCount);
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(getJsonNameSpace(content), properties);
  return data;
  
 }
 
 /**
  * This method returns introText by reading properties of the component from the dialog.
  * 
  * @param webAssistSearchProperties
  * @return introText
  */
 private String getIntroText(Map<String, Object> webAssistSearchProperties) {
  return null == webAssistSearchProperties.get(INTRO_TEXT) ? StringUtils.EMPTY : webAssistSearchProperties.get(INTRO_TEXT).toString();
 }
 
 /**
  * This method returns contactUsLink by reading properties of the component from the dialog.
  * 
  * @param webAssistSearchProperties
  * @return contactUsLink
  */
 private String getContactUsLink(Map<String, Object> webAssistSearchProperties) {
  return null == webAssistSearchProperties.get(VIRTUAL_AGENT_CONTACT_US_LINK) ? StringUtils.EMPTY : webAssistSearchProperties.get(
    VIRTUAL_AGENT_CONTACT_US_LINK).toString();
 }
 
 /**
  * This method returns contactUsQuote by reading properties of the component from the dialog.
  * 
  * @param webAssistSearchProperties
  * @return contactUsQuote
  */
 private String getContactUsQuote(Map<String, Object> webAssistSearchProperties) {
  return null == webAssistSearchProperties.get(VIRTUAL_AGENT_CONTACT_US_QUOTE) ? StringUtils.EMPTY : webAssistSearchProperties.get(
    VIRTUAL_AGENT_CONTACT_US_QUOTE).toString();
 }
 
 /**
  * This method returns noResults by reading value of key provided in i18n.
  * 
  * @return noResults
  */
 private String getNoResults() {
  return null == i18n.get(VIRTUAL_AGENT_NO_RESULTS) ? StringUtils.EMPTY : i18n.get(VIRTUAL_AGENT_NO_RESULTS);
 }
 
 /**
  * This method returns relatedSearchResult by reading value of key provided in i18n.
  * 
  * @return relatedSearchResult
  */
 private String getRelatedSearchResult() {
  return null == i18n.get(VIRTUAL_AGENT_RELATED_SEARCH_RESULTS) ? StringUtils.EMPTY : i18n.get(VIRTUAL_AGENT_RELATED_SEARCH_RESULTS);
 }
 
 /**
  * This method returns infoAvailable by reading value of key provided in i18n.
  * 
  * @return infoAvailable
  */
 private String getInfoAvailable() {
  return null == i18n.get(VIRTUAL_AGENT_RESULTS_DESCRIPTION_TITLE) ? StringUtils.EMPTY : i18n.get(VIRTUAL_AGENT_RESULTS_DESCRIPTION_TITLE);
 }
 
 /**
  * This method returns contactUsCtaLabel by reading value of key provided in i18n.
  * 
  * @return contactUsCtalabel
  */
 private String getContactUsCtaLabel() {
  return null == i18n.get(VIRTUAL_AGENT_CONTACT_US_CTA_LABEL) ? StringUtils.EMPTY : i18n.get(VIRTUAL_AGENT_CONTACT_US_CTA_LABEL);
 }
 
 /**
  * This method returns waResultsTitle by reading value of key provided in i18n.
  * 
  * @return waResultsTitle
  */
 private String getWaResultsTitle() {
  return null == i18n.get(VIRTUAL_AGENT_RESULTS_TITLE) ? StringUtils.EMPTY : i18n.get(VIRTUAL_AGENT_RESULTS_TITLE);
 }
 
 /**
  * This method returns searchPlaceHolder by reading value of key provided in i18n.
  * 
  * @return searchPlaceHolder
  */
 private String getSearchPlaceHolder() {
  return null == i18n.get(VIRTUAL_AGENT_SEARCH_PLACE_HOLDER) ? StringUtils.EMPTY : i18n.get(VIRTUAL_AGENT_SEARCH_PLACE_HOLDER);
 }
 
 /**
  * This method returns ctaLabel by reading value of key provided in i18n.
  * 
  * @return ctaLabel
  */
 private String getCtaLabel() {
  return null == i18n.get(VIRTUAL_AGENT_CTA_LABEL) ? StringUtils.EMPTY : i18n.get(VIRTUAL_AGENT_CTA_LABEL);
 }
 
 /**
  * return map of virtual agent image
  * 
  * @param webAssistSearchProperties
  * @return imageMap
  */
 private Map<String, Object> getImageMap(Map<String, Object> webAssistSearchProperties) {
  Map<String, Object> imageMap = new HashMap<String, Object>();
  String imageUrl = null == webAssistSearchProperties.get(IMAGE_URL) ? StringUtils.EMPTY : webAssistSearchProperties.get(IMAGE_URL).toString();
  String imageAlt = null == webAssistSearchProperties.get(IMAGE_ALT) ? StringUtils.EMPTY : webAssistSearchProperties.get(IMAGE_ALT).toString();
  String imageTitle = null == webAssistSearchProperties.get(IMAGE_TITLE) ? StringUtils.EMPTY : webAssistSearchProperties.get(IMAGE_TITLE).toString();
  imageMap.put(IMAGE_TITLE2, imageTitle);
  imageMap.put(IMAGE_ALT2, imageAlt);
  imageMap.put(IMAGE_URL2, imageUrl);
  
  return imageMap;
  
 }
 
}
