/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.servlet;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameterMap;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.io.JSONWriter;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.aem.foundation.constants.CommonConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * The Class SocialChannelServlet.
 */
@SlingServlet(label = "Unilever Social Channel Servlet to Fetch channels list", resourceTypes = { CommonConstants.DEFAULT_SERVLET_NAME }, methods = {
        HttpConstants.METHOD_GET }, selectors = { "optionPopulator", "searchContTab","soundCloud" }, extensions = { "json" })
@Properties({
        @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.foundation.components.OptionsPopulatorServlet", propertyPrivate = false),
        @Property(name = Constants.SERVICE_DESCRIPTION, value = "Displays JSONS based on the global configuration ", propertyPrivate = false),

})
public class OptionsPopulatorServlet extends SlingAllMethodsServlet {
    
    /** The configuration service. */
    @Reference
    ConfigurationService configurationService;
    
    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(OptionsPopulatorServlet.class);
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
    
      
    /** The Constant COMPONENT_NAME. */
    private static final String COMPONENT_NAME = "cmp";
        
    private static final String I18_FLAG = "i18";

    private static final String CATEGORY = "ct";
    
    private String pattern=StringUtils.EMPTY;;
    
    boolean i18nKey=false;
    
    
    /*
     * (non-Javadoc)
     * 
     * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
     * org.apache.sling.api.SlingHttpServletResponse)
     */
    @Override
    protected final void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws IOException {
        
        String categoryName=StringUtils.EMPTY;
        
        String path = request.getRequestPathInfo().getResourcePath();
        ResourceResolver resourceResolver = request.getResourceResolver();
        
        Writer out = response.getWriter();
        JSONWriter writer = new JSONWriter(out);
        
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        
        Page page = getCurrentPage(request);
        ValueMap componentMap=resourceResolver.resolve(path).adaptTo(ValueMap.class);
        
        RequestParameterMap requestParametrs = request.getRequestParameterMap();
        i18nKey = requestParametrs.containsKey(I18_FLAG) ? Boolean.parseBoolean(requestParametrs.getValue(I18_FLAG).toString()): Boolean.FALSE;
        categoryName=requestParametrs.containsKey(CATEGORY) ? requestParametrs.getValue(CATEGORY).toString()
                : StringUtils.EMPTY;
        
        if (page != null) {
        	String[] selectors = request.getRequestPathInfo().getSelectors();
            Map<String, String> channelsMap = GlobalConfigurationUtility.getMapFromConfiguration(configurationService, page, categoryName);
            String componentName = requestParametrs.containsKey(COMPONENT_NAME) ? requestParametrs.getValue(COMPONENT_NAME).toString()
                    : StringUtils.EMPTY;
            if(StringUtils.equalsIgnoreCase(componentName, "undefined") || StringUtils.isEmpty(componentName)){
                componentName = (String) componentMap.getOrDefault("componentName", StringUtils.EMPTY);
            }
            if(ArrayUtils.contains(selectors, "optionPopulator")){
            	pattern=".recipe%sDataDisplayLabel";
            String[] selctiveChannels = getSelectiveChannels(componentName, page);
            
            try {
                writer.array();
                if (!ArrayUtils.isEmpty(selctiveChannels)) {
                    addSelectiveChannels(componentName,channelsMap, selctiveChannels, writer, request);
                } else {
                    addAllChannels(componentName,channelsMap, writer,request);
                }
                writer.endArray();
            } catch (JSONException e) {
                LOGGER.error("RepositoryException occurred ", e);
            }
            }
            if(ArrayUtils.contains(selectors, "searchContTab")){
            	pattern=".%s.contentTab";
            	try {
					writer.array();
					for(String key:channelsMap.keySet()){
						addWriterObj(componentName, writer, key, key, request);
					}
					writer.endArray();
				} catch (JSONException e) {
	                LOGGER.error("RepositoryException occurred ", e);
	            }
            }
            if(ArrayUtils.contains(selectors, "soundCloud")){
                pattern=".%s.control";
                try {
                    writer.array();
                    for(String key:channelsMap.keySet()){
                        String value=channelsMap.getOrDefault(key, "");
                        if(StringUtils.contains(value, ",")){
                            String[] attr = value.split(",");
                            if (StringUtils.equalsIgnoreCase(attr[1], "true") ||StringUtils.equalsIgnoreCase(attr[1], "false")) {
                            addWriterObj(categoryName, writer, key, value, request);
                        }
                        }
                    }
                    writer.endArray();
                } catch (JSONException e) {
                    LOGGER.error("RepositoryException occurred ", e);
                }
            }
            
        }
    }
    
    /**
     * Adds the writer obj. 
     * 
     * @param writer
     *            the writer
     * @param key
     *            the key
     * @param value
     *            the value
     * @throws JSONException
     *             the JSON exception
     */
    private void addWriterObj(String componentName, JSONWriter writer, String key, String value, SlingHttpServletRequest request) throws JSONException {
        writer.object();
        String keyText=this.i18nKey?getI18nKey(componentName,key.trim(),request):key.trim();
        writer.key("text").value(keyText);
        writer.key("value").value(value.trim());
        writer.endObject();
    }
    
    /**
     * Adds the all channels.
     * 
     * @param channelsMap
     *            the channels map
     * @param writer
     *            the writer
     * @throws JSONException
     *             the JSON exception
     */
    private void addAllChannels(String componentName, Map<String, String> channelsMap, JSONWriter writer, SlingHttpServletRequest request) throws JSONException {
        for (String key : channelsMap.keySet()) {
            addWriterObj(componentName, writer, key, channelsMap.get(key),request);
        }
    }
    
    /**
     * Adds the selective channels.
     * 
     * @param channelsMap
     *            the channels map
     * @param includeChannelsList
     *            the include channels list
     * @param writer
     *            the writer
     * @throws JSONException
     *             the JSON exception
     */
    private void addSelectiveChannels(String componentName, Map<String, String> channelsMap, String[] includeChannelsList, JSONWriter writer, SlingHttpServletRequest request) throws JSONException {
        for (String channelName : includeChannelsList) {
            if (channelsMap.containsKey(channelName.trim())) {
                addWriterObj(componentName, writer, channelName, channelsMap.get(channelName.trim()),request);
            }
        }
    }
    
    /**
     * Gets the selective channels.
     * 
     * @param componentName
     *            the component name
     * @param page
     *            the page
     * @return the selective channels
     */
    private String[] getSelectiveChannels(String componentName, Page page) {
        String[] selctiveChannels = ArrayUtils.EMPTY_STRING_ARRAY;
        if (!StringUtils.isBlank(componentName)) {
            String channelsName = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, page, componentName,
                    "recipeData");
            if (!StringUtils.isBlank(channelsName)) {
                selctiveChannels = channelsName.split(",");
            }
        }
        return selctiveChannels;
    }
    
    /**
     * 
     * @param componentName
     * @param key
     * @param request
     * @return
     */
    private String getI18nKey(String componentName, String key, SlingHttpServletRequest request) {
        String i18nKeyValue = componentName + this.pattern;
        I18n i18n = BaseViewHelper.getI18n(request, getCurrentPage(request));
        String keyValue = key.substring(0, 1).toUpperCase() + key.substring(1);
        return i18n.get(String.format(i18nKeyValue, keyValue));
    }
    
    /**
     * 
     * @param request
     * @return
     */
    private Page getCurrentPage(SlingHttpServletRequest request){
        Resource currentResource = request.getResource();
        PageManager pageManager = request.getResourceResolver().adaptTo(PageManager.class);
        Page page = pageManager.getContainingPage(currentResource);
        return page;
    }
}
