/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.template.dto;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.constants.CommonConstants;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.UnileverDateHelper;
import com.unilever.platform.foundation.constants.ContainerTagConstants;
import com.unilever.platform.foundation.constants.TaggingComponentsConstants;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * The Class Article.
 */
public class Article {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(Article.class);
 /** The Constant config cat name. */
 private static final String CONFIG_CATNAME = "articleListingVariation";
 
 /** The Constant TITLE. */
 private static final String TITLE = "title";
 
 /** The Constant URL. */
 private static final String URL = "url";
 
 /** The Constant COPY_TEXT. */
 private static final String COPY_TEXT = "copyText";
 /** The Constant LONG COPY. */
 private static final String LONG_COPY = "longCopy";
 /** The Constant CONTENT_TYPE. */
 private static final String CONTENT_TYPE = "contentType";
 
 /** The Constant AUTHOR_NAME. */
 private static final String AUTHOR_NAME = "authorName";
 
 /** The Constant AUTHOR. */
 private static final String AUTHOR = "author";
 
 /** The Constant CUSTOM_BODY_CLASS. */
 private static final String CUSTOM_BODY_CLASS = "customBodyClass";
 
 /** The content type. */
 private String contentType;
 
 /** The author name. */
 private String authorName;
 
 /** The Constant IMAGE. */
 private static final String IMAGE = "image";
 
 /** The Constant PARENT. */
 private static final String PARENT = "parent";
 
 /** The Constant SHORT_ARTICLE. */
 private static final String SHORT_ARTICLE = "text";
 
 /** The Constant ARTICLE_COPY. */
 private static final String ARTICLE_COPY = "articleCopy";
 /** The Constant PAGE_READ_TIME. */
 private static final String PAGE_READ_TIME = "pageReadTime";
 private static final String SUB_CONTENT_TYPE = "subContentType";
 /** The title. */
 private String title;
 
 /** The url. */
 private String url;
 
 /** The launch date. */
 private Date launchDate;
 
 /** The copy text. */
 private String copyText;
 
 /** The long copy. */
 private String longCopy;
 
 /** The image. */
 private Image image;
 
 /** The parent. */
 private Map<String, Object> parent;
 
 /** The article copy. */
 private String articleCopy;
 
 /** The container tag map. */
 private Map<String, Object> containerTagMap;
 
 /** The list of flag map. */
 private List<Map<String, Object>> articleTagFlagMap;
 
 /** The Formatted Date. */
 private String formattedDate;
 
 /** The Formatted Review Date. */
 private String formattedReviewDate;
 
 /** The Page Read Time. */
 private int pageReadtime;
 private String customBodyClass;
 private String subContentType;
 
 public String getCustomBodyClass() {
  return customBodyClass;
 }
 
 public void setCustomBodyClass(String customBodyClass) {
  this.customBodyClass = customBodyClass;
 }
 
 /**
  * Sets the data.
  * 
  * @param articlePage
  *         the article page
  * @param slingRequest
  *         the sling request
  */
 public void setData(Page articlePage, SlingHttpServletRequest slingRequest) {
  LOGGER.info("Inside Article constructor");
  if (articlePage != null) {
   ValueMap properties = articlePage.getProperties();
   ResourceResolver resourceResolver = articlePage.getContentResource().getResourceResolver();
   this.title = properties.get(UnileverConstants.TEASER_TITLE, StringUtils.EMPTY);
   this.url = ComponentUtil.getFullURL(resourceResolver, articlePage.getPath(), slingRequest);
   this.copyText = properties.get(UnileverConstants.TEASER_COPY, StringUtils.EMPTY);
   this.longCopy = properties.get(UnileverConstants.TEASER_LONG_COPY, StringUtils.EMPTY);
   String articleTeaserImage = properties.get(UnileverConstants.TEASER_IMAGE, String.class);
   
   this.contentType = properties.get(UnileverConstants.CONTENT_TYPE, StringUtils.EMPTY);
   this.subContentType = properties.get(UnileverConstants.SUB_CONTENT_TYPE, StringUtils.EMPTY);
   this.authorName = properties.get(AUTHOR_NAME, StringUtils.EMPTY);
   
   this.customBodyClass = properties.get(CUSTOM_BODY_CLASS, StringUtils.EMPTY);
   
   String articleTeaserImageAltText = properties.get(UnileverConstants.TEASER_IMAGE_ALT_TEXT, String.class);
   String date = UnileverDateHelper.getDateString(properties, UnileverConstants.PUBLISH_DATE);
   String reviewdate = UnileverDateHelper.getDateString(properties, UnileverConstants.LAST_REVIEW_DATE);
   
   this.formattedDate = ComponentUtil.getformattedDateByConfig(articlePage, date, CONFIG_CATNAME);
   this.formattedReviewDate = ComponentUtil.getformattedDateByConfig(articlePage, reviewdate, CONFIG_CATNAME);
   
   this.image = new Image(articleTeaserImage, articleTeaserImageAltText, this.title, articlePage, slingRequest);
   String pageReadTime = StringUtils.isNotBlank(MapUtils.getString(properties, PAGE_READ_TIME)) ? MapUtils.getString(properties, PAGE_READ_TIME)
     : String.valueOf(CommonConstants.ZERO);
   this.pageReadtime = Integer.parseInt(pageReadTime);
   Page parentPage = articlePage.getParent();
   setParent(resourceResolver, parentPage, slingRequest);
   setArticleCopy(articlePage);
   Map<String, Object> articleClickMap = ComponentUtil.getContainerTagMap(articlePage, "article", this.title);
   List<Map<String, Object>> tagFlagMap = ComponentUtil.getArticleTagFlagMap(articlePage, CONFIG_CATNAME);
   this.articleTagFlagMap = tagFlagMap;
   Map<String, Object> containerTagMapLocal = new LinkedHashMap<String, Object>(CommonConstants.TWO * CommonConstants.ONE);
   containerTagMapLocal.put(ContainerTagConstants.ARTICLE_CLICK, articleClickMap);
   
   this.containerTagMap = containerTagMapLocal;
  }
  
 }
 
 /**
  * Sets the parent.
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param parentPage
  *         the parent page
  * @param slingHttpServletRequest
  *         the sling http servlet request
  */
 private void setParent(ResourceResolver resourceResolver, Page parentPage, SlingHttpServletRequest slingHttpServletRequest) {
  String parentPageTitle = parentPage.getTitle();
  String parentPageUrl = ComponentUtil.getFullURL(resourceResolver, parentPage.getPath(), slingHttpServletRequest);
  
  ValueMap properties = parentPage.getProperties();
  
  String articleTeaserImage = properties.get(UnileverConstants.TEASER_IMAGE) != null ? properties.get(UnileverConstants.TEASER_IMAGE).toString()
    : null;
  String articleTeaserImageAltText = properties.get(UnileverConstants.TEASER_IMAGE_ALT_TEXT) != null ? properties.get(
    UnileverConstants.TEASER_IMAGE_ALT_TEXT).toString() : null;
  
  String teaserTitle = properties.get(UnileverConstants.TEASER_TITLE) != null ? properties.get(UnileverConstants.TEASER_TITLE).toString()
    : StringUtils.EMPTY;
  Image teaserImage = new Image(articleTeaserImage, articleTeaserImageAltText, teaserTitle, parentPage, slingHttpServletRequest);
  
  String teaserCopyText = properties.get(UnileverConstants.TEASER_COPY) != null ? properties.get(UnileverConstants.TEASER_COPY).toString()
    : StringUtils.EMPTY;
  String teaserLongCopy = properties.get(UnileverConstants.TEASER_LONG_COPY) != null ? properties.get(UnileverConstants.TEASER_LONG_COPY).toString()
    : StringUtils.EMPTY;
  
  String dt = "";
  String dtReview = "";
  try {
   dt = ComponentUtil.getFormattedDateString((GregorianCalendar) MapUtils.getObject(properties, UnileverConstants.PUBLISH_DATE), null);
   dtReview = ComponentUtil.getFormattedDateString((GregorianCalendar) MapUtils.getObject(properties, UnileverConstants.LAST_REVIEW_DATE), null);
  } catch (Exception e) {
   LOGGER.warn("Article parent Publish or Review date parsing exception", e);
  }
  String teaserFormattedDate = ComponentUtil.getformattedDateByConfig(parentPage, dt, CONFIG_CATNAME);
  String teaserFormattedReviewDate = ComponentUtil.getformattedDateByConfig(parentPage, dtReview, CONFIG_CATNAME);
  int readTime = properties.get(PAGE_READ_TIME) != null ? Integer.parseInt((String) properties.get(PAGE_READ_TIME)) : 0;
  
  Map<String, Object> parentMap = new HashMap<String, Object>();
  parentMap.put(TITLE, parentPageTitle);
  parentMap.put(URL, parentPageUrl);
  parentMap.put(UnileverConstants.TEASER_IMAGE, teaserImage);
  parentMap.put(UnileverConstants.TEASER_TITLE, teaserTitle);
  parentMap.put(UnileverConstants.TEASER_COPY, teaserCopyText);
  parentMap.put(UnileverConstants.TEASER_LONG_COPY, teaserLongCopy);
  parentMap.put(UnileverConstants.PUBLISH_DATE, teaserFormattedDate);
  parentMap.put(UnileverConstants.LAST_REVIEW_DATE, teaserFormattedReviewDate);
  parentMap.put(PAGE_READ_TIME, readTime);
  setParent(parentMap);
 }
 
 /**
  * Gets the title.
  * 
  * @return the title
  */
 public String getTitle() {
  return title;
 }
 
 /**
  * Sets the title.
  * 
  * @param title
  *         the new title
  */
 public void setTitle(String title) {
  this.title = title;
 }
 
 /**
  * Gets the url.
  * 
  * @return the url
  */
 public String getUrl() {
  return url;
 }
 
 /**
  * Sets the url.
  * 
  * @param url
  *         the new url
  */
 public void setUrl(String url) {
  this.url = url;
 }
 
 /**
  * Gets the copy text.
  * 
  * @return the copy text
  */
 public String getCopyText() {
  return copyText;
 }
 
 /**
  * Sets the copy text.
  * 
  * @param copyText
  *         the new copy text
  */
 public void setCopyText(String copyText) {
  this.copyText = copyText;
 }
 
 /**
  * Gets the image.
  * 
  * @return the image
  */
 public Image getImage() {
  return image;
 }
 
 /**
  * Sets the image.
  * 
  * @param image
  *         the new image
  */
 public void setImage(Image image) {
  this.image = image;
 }
 
 /**
  * Convert to map.
  * 
  * @return the map
  */
 public Map<String, Object> convertToMap() {
  Map<String, Object> map = new LinkedHashMap<String, Object>();
  map.put(TITLE, this.title);
  map.put(URL, this.url);
  map.put(COPY_TEXT, this.copyText);
  map.put(LONG_COPY, this.longCopy);
  map.put(IMAGE, this.image.convertToMap());
  map.put(PARENT, this.parent);
  map.put(ARTICLE_COPY, this.articleCopy);
  map.put(CONTENT_TYPE, this.contentType);
  map.put(SUB_CONTENT_TYPE, this.subContentType);
  map.put(AUTHOR, this.authorName);
  map.put(CUSTOM_BODY_CLASS, this.customBodyClass);
  
  map.put(UnileverConstants.PUBLISH_DATE, this.formattedDate);
  map.put(UnileverConstants.LAST_REVIEW_DATE, this.formattedReviewDate);
  
  map.put(UnileverConstants.ARTICLE_FLAGS, this.articleTagFlagMap);
  map.put(UnileverConstants.CONTAINER_TAG, this.containerTagMap);
  map.put(PAGE_READ_TIME, this.pageReadtime);
  return map;
 }
 
 /**
  * Convert to map.
  * 
  * @param flagMap
  *         the flag map
  * @return the map
  */
 @SuppressWarnings("unchecked")
 public Map<String, Object> convertToMap(Map<String, Boolean> flagMap) {
  Map<String, Object> map = convertToMap();
  if (!MapUtils.getBooleanValue(flagMap, TaggingComponentsConstants.IMAGE_FLAG, false)) {
   map.put(IMAGE, null);
  }
  if (!MapUtils.getBooleanValue(flagMap, TaggingComponentsConstants.COPY_FLAG, false)) {
   map.put(COPY_TEXT, "");
  }
  if (!MapUtils.getBooleanValue(flagMap, TaggingComponentsConstants.LONG_COPY_FLAG, false)) {
   map.put(LONG_COPY, "");
  }
  if (!MapUtils.getBooleanValue(flagMap, TaggingComponentsConstants.PUBLISH_DATE_FLAG, false)) {
   map.put(UnileverConstants.PUBLISH_DATE, "");
  }
  if (!MapUtils.getBooleanValue(flagMap, TaggingComponentsConstants.LAST_REVIEW_DATE_FLAG, false)) {
   map.put(UnileverConstants.LAST_REVIEW_DATE, "");
  }
  if (!MapUtils.getBooleanValue(flagMap, TaggingComponentsConstants.READ_TIME_FLAG, false)) {
   map.put(PAGE_READ_TIME, 0);
  }
  if (!MapUtils.getBooleanValue(flagMap, TaggingComponentsConstants.AUTHOR_NAME_FLAG, false)) {
   map.put(AUTHOR, "");
  }
  Map<String, Object> parentMap = MapUtils.getMap(map, PARENT);
  if (MapUtils.getBooleanValue(flagMap, TaggingComponentsConstants.PARETNT_PAGE_DETAILS_FLAG, false)) {
   
   if (!MapUtils.getBooleanValue(flagMap, TaggingComponentsConstants.IMAGE_FLAG, false)) {
    parentMap.put(UnileverConstants.TEASER_IMAGE, null);
   }
   if (!MapUtils.getBooleanValue(flagMap, TaggingComponentsConstants.COPY_FLAG, false)) {
    parentMap.put(UnileverConstants.TEASER_COPY, "");
   }
   if (!MapUtils.getBooleanValue(flagMap, TaggingComponentsConstants.LONG_COPY_FLAG, false)) {
    parentMap.put(UnileverConstants.TEASER_LONG_COPY, "");
   }
   if (!MapUtils.getBooleanValue(flagMap, TaggingComponentsConstants.PUBLISH_DATE_FLAG, false)) {
    parentMap.put(UnileverConstants.PUBLISH_DATE, "");
   }
   if (!MapUtils.getBooleanValue(flagMap, TaggingComponentsConstants.LAST_REVIEW_DATE_FLAG, false)) {
    parentMap.put(UnileverConstants.LAST_REVIEW_DATE, "");
   }
   if (!MapUtils.getBooleanValue(flagMap, TaggingComponentsConstants.READ_TIME_FLAG, false)) {
    map.put(PAGE_READ_TIME, 0);
   }
  } else {
   parentMap.put(UnileverConstants.TEASER_IMAGE, null);
   parentMap.put(UnileverConstants.TEASER_COPY, "");
   parentMap.put(UnileverConstants.TEASER_LONG_COPY, "");
   parentMap.put(UnileverConstants.PUBLISH_DATE, "");
   parentMap.put(UnileverConstants.LAST_REVIEW_DATE, "");
   parentMap.put(PAGE_READ_TIME, 0);
   map.put(PARENT, parentMap);
  }
  map.remove(UnileverConstants.CONTAINER_TAG);
  return map;
 }
 
 
 /**
  * Gets the parent.
  * 
  * @return the parent
  */
 public Map<String, Object> getParent() {
  return parent;
 }
 
 /**
  * Sets the parent.
  * 
  * @param parent
  *         the parent
  */
 public void setParent(Map<String, Object> parent) {
  this.parent = parent;
 }
 
 /**
  * Gets the article copy.
  * 
  * @return the article copy
  */
 public String getArticleCopy() {
  return articleCopy;
 }
 
 /**
  * Sets the article copy.
  * 
  * @param articlePage
  *         the new article copy
  */
 public void setArticleCopy(Page articlePage) {
  Resource bodycopyHeadlineResource = articlePage.getContentResource(UnileverConstants.BODY_COPY_HEADLINE);
  ValueMap valueMap = bodycopyHeadlineResource != null ? bodycopyHeadlineResource.getValueMap() : null;
  this.articleCopy = valueMap != null && valueMap.get(SHORT_ARTICLE) != null ? bodycopyHeadlineResource.getValueMap().get(SHORT_ARTICLE).toString()
    : StringUtils.EMPTY;
 }
 
 /**
  * Gets the launch date.
  * 
  * @return the launch date
  */
 public Date getLaunchDate() {
  return launchDate;
 }
 
 /**
  * Sets the launch date.
  * 
  * @param launchDate
  *         the new launch date
  */
 public void setLaunchDate(Date launchDate) {
  this.launchDate = launchDate;
 }
 
 /**
  * Gets the container tag map.
  * 
  * @return the container tag map
  */
 public Map<String, Object> getContainerTagMap() {
  return containerTagMap;
 }
 
 /**
  * Sets the container tag map.
  * 
  * @param containerTagMap
  *         the container tag map
  */
 public void setContainerTagMap(Map<String, Object> containerTagMap) {
  this.containerTagMap = containerTagMap;
  
 }
 
 /**
  * Gets the content type.
  * 
  * @return the content type
  */
 public String getContentType() {
  return contentType;
 }
 
 /**
  * Sets the content type.
  * 
  * @param contentType
  *         the new content type
  */
 public void setContentType(String contentType) {
  this.contentType = contentType;
 }
 
 /**
  * Gets the author name.
  * 
  * @return the author name
  */
 public String getAuthorName() {
  return authorName;
 }
 
 /**
  * Sets the author name.
  * 
  * @param authorName
  *         the new author name
  */
 public void setAuthorName(String authorName) {
  this.authorName = authorName;
 }
 
 /**
  * Gets the article tag flag map.
  * 
  * @return the articleTagFlagMap
  */
 public List<Map<String, Object>> getArticleTagFlagMap() {
  return articleTagFlagMap;
 }
 
 /**
  * Sets the article tag flag map.
  * 
  * @param articleTagFlagMap
  *         the articleTagFlagMap to set
  */
 public void setArticleTagFlagMap(List<Map<String, Object>> articleTagFlagMap) {
  this.articleTagFlagMap = articleTagFlagMap;
 }
 
 /**
  * Gets the formatted date.
  * 
  * @return the formatted date
  */
 public String getFormattedDate() {
  return formattedDate;
 }
 
 /**
  * Sets the formatted date.
  * 
  * @param formattedDate
  *         the new formatted date
  */
 public void setFormattedDate(String formattedDate) {
  this.formattedDate = formattedDate;
 }
 
 /**
  * Gets the sling request.
  * 
  * @param resources
  *         the resources
  * @return the sling request
  */
 public static SlingHttpServletRequest getSlingRequest(final Map<String, Object> resources) {
  return (SlingHttpServletRequest) resources.get(ProductConstants.REQUEST);
  
 }
 
 /**
  * Gets the long copy.
  * 
  * @return the long copy
  */
 public String getLongCopy() {
  return longCopy;
 }
 
 /**
  * Sets the long copy.
  * 
  * @param longCopy
  *         the new long copy
  */
 public void setLongCopy(String longCopy) {
  this.longCopy = longCopy;
 }
 
 /**
  * Gets the page readtime.
  * 
  * @return the page readtime
  */
 public int getPageReadtime() {
  return pageReadtime;
 }
 
 /**
  * Sets the page readtime.
  * 
  * @param pageReadtime
  *         the new page readtime
  */
 public void setPageReadtime(int pageReadtime) {
  this.pageReadtime = pageReadtime;
 }
}
