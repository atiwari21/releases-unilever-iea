/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.servlet;

import java.io.IOException;
import java.io.Writer;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.io.JSONWriter;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.aem.foundation.constants.CommonConstants;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * The Class MultipleProductViewSupportedServlet.
 */
@SlingServlet(label = "Unilever Multiple Product View Supported Servlet to Fetch suported view", resourceTypes = { CommonConstants.DEFAULT_SERVLET_NAME }, methods = { HttpConstants.METHOD_GET }, selectors = { "multipleProductViewSupported" }, extensions = { "json" })
@Properties({
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.foundation.servlet.MultipleProductViewSupportedServlet", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "Unilever Multiple Product View Supported Servlet to Fetch suported view", propertyPrivate = false),

})
public class MultipleProductViewSupportedServlet extends SlingAllMethodsServlet {
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(MultipleProductViewSupportedServlet.class);
 
 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = 1L;
 
 /** The Constant MULTIPLE_PRODUCT_VIEW_SUPPORTED. */
 private static final String MULTIPLE_PRODUCT_VIEW_SUPPORTED = "multipleProductViewSupported";
 private static final String MULTIPLE_PRODUCT_VIEW = "Multiple product view";
 private static final String MULTIPLE_PRODUCT_VIEW_VALUE = "true";
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected final void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws IOException {
  
  String path = request.getRequestPathInfo().getResourcePath();
  ResourceResolver resourceResolver = request.getResourceResolver();
  
  Writer out = response.getWriter();
  JSONWriter writer = new JSONWriter(out);
  
  response.setContentType("application/json");
  response.setCharacterEncoding("UTF-8");
  
  Resource currentResource = resourceResolver.getResource(path);
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  Page page = pageManager.getContainingPage(currentResource);
  String serviceProviderName = StringUtils.EMPTY;
  String multipleProductViewSupported = StringUtils.EMPTY;
  Map<String, String> multipleProductViewSupportedMap = new LinkedHashMap<String, String>();
  if (page != null) {
   serviceProviderName = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, page, ProductConstants.RATING_AND_REVIEWS,
     ProductConstants.SERVICE_PROVIDER_NAME);
   if (!StringUtils.isBlank(serviceProviderName)) {
    multipleProductViewSupported = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, page, serviceProviderName,
      MULTIPLE_PRODUCT_VIEW_SUPPORTED);
   }
   if ("True".equalsIgnoreCase(multipleProductViewSupported)) {
    multipleProductViewSupportedMap.put(MULTIPLE_PRODUCT_VIEW, MULTIPLE_PRODUCT_VIEW_VALUE);
   }
   try {
    writer.array();
    if (multipleProductViewSupportedMap.size() > 0) {
     addProductView(multipleProductViewSupportedMap, writer);
    }
    writer.endArray();
   } catch (JSONException e) {
    LOGGER.error("RepositoryException occurred ", e);
   }
  }
 }
 
 /**
  * Adds the writer obj.
  * 
  * @param writer
  *         the writer
  * @param key
  *         the key
  * @param value
  *         the value
  * @throws JSONException
  *          the JSON exception
  */
 private void addWriterObj(JSONWriter writer, String key, String value) throws JSONException {
  writer.object();
  writer.key("text").value(key.trim());
  writer.key("value").value(value.trim());
  writer.endObject();
 }
 
 /**
  * Adds the product View.
  * 
  * @param multipleProductViewSupportedMap
  *         the multipleProductViewSupportedMap
  * @param writer
  *         the writer
  * @throws JSONException
  *          the JSON exception
  */
 private void addProductView(Map<String, String> multipleProductViewSupportedMap, JSONWriter writer) throws JSONException {
  for (String key : multipleProductViewSupportedMap.keySet()) {
   addWriterObj(writer, key, multipleProductViewSupportedMap.get(key));
  }
 }
}
