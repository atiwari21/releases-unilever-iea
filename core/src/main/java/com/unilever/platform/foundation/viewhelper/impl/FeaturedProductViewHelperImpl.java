/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.api.Product;
import com.adobe.cq.commerce.common.CommerceHelper;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.core.dto.SearchDTO;
import com.unilever.platform.aem.foundation.core.service.SearchService;
import com.unilever.platform.aem.foundation.utils.AdaptiveUtil;
import com.unilever.platform.foundation.components.SearchResultFeaturedComparator;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.constants.CommonConstants;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.kritique.config.KritiqueReviewsCommentsService;
import com.unilever.platform.foundation.template.dto.Image;
import com.unilever.platform.foundation.template.dto.KritiqueDTO;
import com.unilever.platform.foundation.viewhelper.constants.MultipleRelatedProductsConstants;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * <p>
 * Responsible for reading author input for featured product component and
 * returns a featured product.
 *
 * </p>
 */
/**
 * @author ksaty1
 * 
 */
@Component(description = "Featured Product ViewHelper Impl", immediate = true, metatype = true, label = "Featured Product View Helper")
@Service(value = { FeaturedProductViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.FEATURED_PRODUCT, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class FeaturedProductViewHelperImpl extends MultipleRelatedProductsBaseViewHelperImpl {
 /**
  * Constant for retrieving anchor link navigation label.
  */
 private static final String ANCHOR_MAP = "anchorLinkNavigation";
 
 /**
  * Constant for retrieving featured product type.
  */
 private static final String FEATURED_TYPE = "featuredProductType";
 
 /**
  * Constant for retrieving background image label.
  */
 
 private static final String BACKGROUND = "backgroundImage";
 
 /**
  * Constant for retrieving product list label.
  */
 
 private static final String PRODUCT_LIST = "productList";
 
 /**
  * Constant for setting image title label.
  */
 
 private static final String TITLE = "title";
 
 /**
  * Constant for setting featured product type auto.
  */
 
 private static final String AUTO = "Auto";
 
 /**
  * Constant for retrieving mannual product page.
  */
 
 private static final String PRODUCT_PAGE = "productPage";
 
 /**
  * Constant for retrieving context label.
  */
 
 private static final String CONTEXT_LABEL = "contextLabel";
 
 /**
  * Constant for retrieving background.
  */
 
 private static final String FEATURED_BACKGROUND = "featuredbackgroundImage";
 
 /**
  * Constant for retrieving cta label.
  */
 
 private static final String CTA_LABEL = "title";
 
 /**
  * Constant for retrieving type label.
  */
 
 private static final String TYPE = "type";
 
 /**
  * Constant for setting alt text for background image.
  */
 
 private static final String ALT = "alt";
 
 /**
  * Constant for setting alt text for background image.
  */
 
 private static final String CTA = "textLink";
 /**
  * logger object.
  **/
 
 @Reference
 ConfigurationService configurationService;
 
 /** The search service. */
 @Reference
 SearchService searchService;
 
 /** The kritique service. */
 @Reference
 KritiqueReviewsCommentsService kritiqueReviewsCommentsService;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(FeaturedProductViewHelperImpl.class);
 
 /**
  * The method processData overrides method is responsible validating author input and returning a map Map can be accessed in JSP in consistent
  * way.This method also validates the link url provided by author.
  * 
  * @param content
  *         , - {@link Map} of all user input received as input <code>ViewHelper.onGetData</code> method
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return {@link Map} holding the data
  */
 @Override
 @SuppressWarnings("unchecked")
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.info("Featured product View Helper #processData called for processing fixed list.");
  Map<String, Object> featuredProductProperties = (Map<String, Object>) content.get(PROPERTIES);
  Map<String, Object> featuredProductFinalMap = new LinkedHashMap<String, Object>();
  ResourceResolver resourceResolver = (ResourceResolver) resources.get(ProductConstants.RESOURCE_RESOLVER);
  SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) resources.get(ProductConstants.REQUEST);
  String jsonNameSpace = MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE);
  Resource currentResource = slingRequest.getResource();
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  
  Page currentPage = pageManager.getContainingPage(currentResource);
  String productType = MapUtils.getString(featuredProductProperties, FEATURED_TYPE);
  
  // checking type of data
  if (StringUtils.isNotEmpty(productType)) {
   if (AUTO.equals(productType)) {
    String gloablConfigCatName = MapUtils.getString(featuredProductProperties, UnileverConstants.GLOBAL_CONFIGURATION_CATEGORY_NAME);
    LOGGER.debug("Global Configuration category for FeaturedProductViewHelperImpl = " + gloablConfigCatName);
    Map<String, String> configMap = ComponentUtil.getConfigMap(currentPage, gloablConfigCatName);
    LOGGER.debug("created Map for featured product configuration ,size of this Map is " + configMap.size());
    List<SearchDTO> productsResultList = getProductPagePathList(resources, configMap, featuredProductProperties);
    
    List<String> productsPagePathList = getSortedListBasedOnComparator(resources, productsResultList, configMap, featuredProductProperties);
    LOGGER.debug("created list of pages after sorting for featured product ,size of the list is " + productsPagePathList.size());
    List<Map<String, Object>> list = ProductHelper.getProductList(productsPagePathList, resources, jsonNameSpace);
    LOGGER.debug("product list of PDP pages for featured product has been created ,size of the list is " + list.size());
    Map<String, Object> productMap = getproductMap(slingRequest, productsPagePathList, featuredProductProperties, configMap, list, currentPage,
      resources);
    featuredProductFinalMap.put(jsonNameSpace, productMap);
    return featuredProductFinalMap;
   } else {
    featuredProductFinalMap = getProductsManual(resourceResolver, slingRequest, content, featuredProductProperties, currentPage, resources);
   }
  } else {
   List<Map<String, Object>> blankFeaturedProductListFinal = new ArrayList<Map<String, Object>>();
   featuredProductFinalMap.put(jsonNameSpace, blankFeaturedProductListFinal.toArray());
  }
  return featuredProductFinalMap;
 }
 
 /**
  * Gets the product map.
  * 
  * @param slingRequest
  *         the sling request
  * @param properties
  *         the properties
  * @param configMap
  *         the config map
  * @param list
  *         the list
  * 
  * @return the product map
  */
 protected Map<String, Object> getproductMap(SlingHttpServletRequest slingRequest, List<String> productsPagePathList, Map<String, Object> properties,
   Map<String, String> configMap, List<Map<String, Object>> list, Page currentPage, Map<String, Object> resources) {
  
  PageManager pageManager = getResourceResolver(resources).adaptTo(PageManager.class);
  Map<String, Object> productMap = new LinkedHashMap<String, Object>();
  boolean ratingFlag = false;
  
  if (CollectionUtils.isNotEmpty(productsPagePathList)) {
   Page productpage = pageManager.getContainingPage(productsPagePathList.get(0));
   
   String type = MapUtils.getString(properties, FEATURED_TYPE);
   String themeBackground = MapUtils.getString(properties, CommonConstants.THEME);
   productMap.put(TYPE, type.toLowerCase());
   productMap.put(CommonConstants.THEME, themeBackground.toLowerCase());
   productMap.put(ProductConstants.REVIEW_MAP, ProductHelper.getReviewMapProperties(configurationService, productpage));
   KritiqueDTO kritiqueparams = new KritiqueDTO(currentPage, configurationService);
   List<Map<String, Object>> listwithrating = new ArrayList<Map<String, Object>>();
   
   ratingFlag = getKQForFeaturePhone(slingRequest, list, productMap, productpage, kritiqueparams, listwithrating);
   
  }
  if (!ratingFlag) {
   productMap.put(ProductConstants.PRODUCT_LIST_MAP, list.toArray());
  }
  
  String contextLabel = checkNullValues(MapUtils.getString(properties, CONTEXT_LABEL));
  Map<String, Object> backgroundImageAutoMap = getImageProperties(properties, currentPage, resources);
  LOGGER.debug("created a Map for background image auto for featured product ,size of th Map is " + backgroundImageAutoMap.size());
  if (StringUtils.isNotEmpty(contextLabel)) {
   productMap.put(CONTEXT_LABEL, contextLabel);
  }
  productMap.put(BACKGROUND, backgroundImageAutoMap);
  LOGGER.debug("created review map for featured product,size of the Map is " + ComponentUtil.getReviewMap(configMap).size());
  String title = properties.get(TITLE) != null ? (String) properties.get(TITLE) : StringUtils.EMPTY;
  productMap.put(MultipleRelatedProductsConstants.TITLE, title);
  Map<String, String> anchorLinkMap = ComponentUtil.getAnchorLinkMap(properties);
  productMap.put(UnileverConstants.ANCHOR_NAVIGATION, anchorLinkMap);
  return productMap;
 }
 
 /**
  * This method used to get all product information from the product page in which user selects.
  * 
  * @param content
  *         the content
  * @param featuredProductProperties
  *         the featured product properties
  * @param pageManager
  *         the page manager
  * @param currentPage
  *         the current page
  * @param category
  *         the category
  * @param action
  *         the action
  * @param resources
  *         the resources
  * @return the products manual
  */
 private Map<String, Object> getProductsManual(ResourceResolver resourceResolver, SlingHttpServletRequest slingRequest, Map<String, Object> content,
   Map<String, Object> featuredProductProperties, Page currentPage, Map<String, Object> resources) {
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  Map<String, Object> productListFinalMap = new LinkedHashMap<String, Object>();
  String jsonNameSpace = MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE);
  List<Map<String, Object>> productList = new ArrayList<Map<String, Object>>();
  Map<String, Object> productListMap = new LinkedHashMap<String, Object>();
  String tagPage = MapUtils.getString(featuredProductProperties, PRODUCT_PAGE);
  String themeBackground = MapUtils.getString(featuredProductProperties, CommonConstants.THEME);
  String featuredType = MapUtils.getString(featuredProductProperties, FEATURED_TYPE);
  String contextLabel = MapUtils.getString(featuredProductProperties, CONTEXT_LABEL);
  String backgroundImage = MapUtils.getString(featuredProductProperties, FEATURED_BACKGROUND);
  backgroundImage = checkNullValues(backgroundImage);
  
  Map<String, Object> backgroundImageMap = getImageProperties(featuredProductProperties, currentPage, resources);
  String gloablConfigCategoryName = MapUtils.getString(featuredProductProperties, UnileverConstants.GLOBAL_CONFIGURATION_CATEGORY_NAME);
  Map<String, String> globalConfigMap = ComponentUtil.getConfigMap(currentPage, gloablConfigCategoryName);
  Map<String, String> anchorLinkMap = ComponentUtil.getAnchorLinkMap(featuredProductProperties);
  Map<String, Object> ctaMap = new LinkedHashMap<String, Object>();
  String ctaLabel = MapUtils.getString(featuredProductProperties, CTA_LABEL);
  Page currentProductPage = pageManager.getContainingPage(tagPage);
  String currentProductPagePath = ComponentUtil.getFullURL(resourceResolver, tagPage, slingRequest);
  // container map for analytics
  ctaMap.put(UnileverConstants.URL, currentProductPagePath);
  KritiqueDTO kritiqueparams = new KritiqueDTO(currentProductPage, configurationService);
  if (currentProductPage != null) {
   productListMap = ProductHelper.getProductMap(currentProductPage, jsonNameSpace, resources);
   LOGGER.debug("created a Map of product for featured product,size of the Map is " + productListMap.size());
   getAdaptiveRating(slingRequest, productListMap, currentProductPage, kritiqueparams);
   if (MapUtils.isNotEmpty(productListMap)) {
    productListMap.put(CTA, ctaMap);
   }
  }
  if (MapUtils.isNotEmpty(productListMap)) {
   productList.add(productListMap);
  }
  productListFinalMap.put(ANCHOR_MAP, anchorLinkMap);
  productListFinalMap.put(PRODUCT_LIST, productList.toArray());
  productListFinalMap.put(BACKGROUND, backgroundImageMap);
  LOGGER.debug("created review map for featured product,size of the Map is " + ComponentUtil.getReviewMap(globalConfigMap).size());
  if (currentProductPage != null) {
   productListFinalMap.put(ProductConstants.REVIEW_MAP, ProductHelper.getReviewMapProperties(configurationService, currentProductPage));
  }
  productListFinalMap.put(CommonConstants.THEME, themeBackground.toLowerCase());
  productListFinalMap.put(CONTEXT_LABEL, contextLabel);
  productListFinalMap.put(TYPE, featuredType.toLowerCase());
  productListFinalMap.put(TITLE, ctaLabel);
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  data.put(jsonNameSpace, productListFinalMap);
  return data;
 }
 
 /**
  * @param slingRequest
  * @param productListMap
  * @param currentProductPage
  * @param kritiqueparams
  */
 private void getAdaptiveRating(SlingHttpServletRequest slingRequest, Map<String, Object> productListMap, Page currentProductPage,
   KritiqueDTO kritiqueparams) {
  if (AdaptiveUtil.isAdaptive(slingRequest)) {
   UNIProduct product = null;
   Product pageProduct = CommerceHelper.findCurrentProduct(currentProductPage);
   if (pageProduct instanceof UNIProduct) {
    product = (UNIProduct) pageProduct;
   }
   LOGGER.info("Feature phone view called and product path is " + currentProductPage.getPath());
   if (pageProduct != null && product != null) {
    kritiqueparams.setSiteProductId(product.getSKU());
   }
   try {
    List<Map<String, Object>> kqMap = kritiqueReviewsCommentsService.getRatingsService(kritiqueparams, configurationService);
    if (!(kqMap.isEmpty())) {
     Map<String, Object> reviewMap = kqMap.get(0);
     productListMap.put(ProductConstants.PRODUCT_RATINGS_MAP, reviewMap);
    }
    LOGGER.debug("Product list map created successfully for featured product");
   } catch (Exception e) {
    LOGGER.error("Exception while getting kritique response ", e);
   }
  }
 }
 
 /**
  * This method returns an empty string if the key is null or undefined otherwise it returns key.
  * 
  * @param key
  *         the key
  * @return key
  */
 private String checkNullValues(Object key) {
  String value = StringUtils.EMPTY;
  if (null == key) {
   value = StringUtils.EMPTY;
  } else if (StringUtils.EMPTY.equals(key)) {
   value = StringUtils.EMPTY;
  } else {
   value = key.toString();
  }
  
  return value;
  
 }
 
 /**
  * This method is used to get image properties like path,alt text for image extension etc.
  * 
  * @param altText
  *         the alt text
  * @param imageOne
  *         the image one
  * @param imageOneArray
  *         the image one array
  * @return the image properties
  */
 
 private Map<String, Object> getImageProperties(Map<String, Object> properties, Page currentPage, Map<String, Object> resources) {
  Map<String, Object> backgroundImageMap = new LinkedHashMap<String, Object>();
  String altImage = checkNullValues(MapUtils.getString(properties, ALT));
  String backgroundImage = checkNullValues(MapUtils.getString(properties, FEATURED_BACKGROUND));
  Image productBackgroundImage = new Image(backgroundImage, altImage, altImage, currentPage, getSlingRequest(resources));
  backgroundImageMap = productBackgroundImage.convertToMap();
  return backgroundImageMap;
  
 }
 
 /**
  * Gets the sorted final list based on comparator.
  * 
  * @param productsResultList
  *         the products result list
  * @param configMap
  *         the config map
  * @param currentPage
  *         the current page
  * @param tagManager
  * @return the sorted final list based on comparator
  */
 protected List<String> getSortedListBasedOnComparator(Map<String, Object> resources, List<SearchDTO> productsResultList,
   Map<String, String> configMap, Map<String, Object> properties) {
  int max = CommonConstants.FOUR;
  try {
   max = StringUtils.isBlank(configMap.get(UnileverConstants.MAX)) ? CommonConstants.FOUR : Integer.parseInt(configMap.get(UnileverConstants.MAX));
  } catch (NumberFormatException nfe) {
   LOGGER.error("Maximum products for page list of featured product must be number");
   LOGGER.error(ExceptionUtils.getStackTrace(nfe));
  }
  String[] include = configMap.get(UnileverConstants.INCLUDE_LIST) != null ? configMap.get(UnileverConstants.INCLUDE_LIST).split(
    CommonConstants.COMMA_CHAR) : new String[] {};
  String[] exclude = configMap.get(UnileverConstants.EXCLUDE_LIST) != null ? configMap.get(UnileverConstants.EXCLUDE_LIST).split(
    CommonConstants.COMMA_CHAR) : new String[] {};
  List<String> includeTagIdList = getIncludeTagIdList(resources, include, exclude);
  LOGGER.debug("Maximum products for page list of featured product= " + max);
  String orderbyTagId = configMap.get(MultipleRelatedProductsConstants.ORDER_BY) != null ? configMap.get(MultipleRelatedProductsConstants.ORDER_BY)
    : StringUtils.EMPTY;
  String[] featuredTagFromConfig = StringUtils.isNotBlank(orderbyTagId) ? orderbyTagId.split(CommonConstants.COMMA_CHAR) : new String[] {};
  String includeSegmentRulesProp = MapUtils.getString(properties, "includeSegmentRules", StringUtils.EMPTY);
  boolean includeSegmentRules = ("true").equalsIgnoreCase(includeSegmentRulesProp) ? true : false;
  String[] featuredTags = ComponentUtil.getUpdatedIncludedArrayFromResources(resources, featuredTagFromConfig, includeSegmentRules);
  long startTime = System.currentTimeMillis();
  
  Collections.sort(productsResultList, new SearchResultFeaturedComparator(includeTagIdList, featuredTags, false));
  
  long stopTime = System.currentTimeMillis();
  
  LOGGER.info("Time elapsed in soring the result of Featured Product = " + (stopTime - startTime));
  List<SearchDTO> productsResultListFinal = productsResultList.size() > max ? productsResultList.subList(0, max) : productsResultList;
  LOGGER.debug("List of product pages created for featured product after sorting and size of the list is " + productsResultListFinal.size());
  return getPathList(productsResultListFinal);
 }
 
 /**
  * Gets the config service.
  * 
  * @return configurationService
  */
 @Override
 protected ConfigurationService getConfigService() {
  
  return this.configurationService;
 }
 
 /**
  * Gets the search service.
  * 
  * @return searchService
  */
 @Override
 protected SearchService getSearchService() {
  
  return this.searchService;
 }
 
 /**
  * @param slingRequest
  * @param list
  * @param productMap
  * @param ratingFlag
  * @param productpage
  * @param ratingMap
  * @param kritiqueparams
  * @param listwithrating
  * @return
  */
 private boolean getKQForFeaturePhone(SlingHttpServletRequest slingRequest, List<Map<String, Object>> list, Map<String, Object> productMap,
   Page productpage, KritiqueDTO kritiqueparams, List<Map<String, Object>> listwithrating) {
  boolean ratingFlag = false;
  if (AdaptiveUtil.isAdaptive(slingRequest)) {
   
   UNIProduct product = null;
   Product pageProduct = CommerceHelper.findCurrentProduct(productpage);
   if (pageProduct instanceof UNIProduct) {
    product = (UNIProduct) pageProduct;
   }
   LOGGER.info("Feature phone view called and product page path is " + productpage.getPath());
   if (pageProduct != null && product != null) {
    kritiqueparams.setSiteProductId(product.getSKU());
   }
   if (!list.isEmpty()) {
    Map<String, Object> ratingMap = null;
    for (int i = 0; i < list.size(); i++) {
     ratingMap = list.get(i);
    }
    try {
     List<Map<String, Object>> kqMap = kritiqueReviewsCommentsService.getRatingsService(kritiqueparams, configurationService);
     if (!(kqMap.isEmpty())) {
      ratingMap.put(ProductConstants.PRODUCT_RATINGS_MAP, kqMap.get(0));
      listwithrating.add(ratingMap);
      ratingFlag = true;
      LOGGER.debug("List created successfully for featured product and size of the list is " + listwithrating.size());
      productMap.put(ProductConstants.PRODUCT_LIST_MAP, listwithrating.toArray());
     }
    } catch (Exception e) {
     LOGGER.error("Exception while getting kritique response ", e);
    }
    
   }
  }
  return ratingFlag;
 }
 
}
