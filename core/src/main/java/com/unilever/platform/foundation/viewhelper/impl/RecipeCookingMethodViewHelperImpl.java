/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.http.osgi.services.HttpClientBuilderFactory;
import org.apache.sling.api.SlingConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.RMSJson;
import com.unilever.platform.foundation.components.helper.RecipeHelper;
import com.unilever.platform.foundation.template.dto.RecipeConfigDTO;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * The Class RecipeCopyViewHelperImpl.
 */
@Component(description = "RecipeCookingMethodViewHelperImpl", immediate = true, metatype = true, label = "Recipe Cooking Method Component")
@Service(value = { RecipeCookingMethodViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.RECIPE_COOKING_METHOD, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class RecipeCookingMethodViewHelperImpl extends BaseViewHelper {
 
 /** The Constant RECIPE_COPY_SUB_TITLE. */
 private static final String RECIPE_COOKING_METHOD_SUB_TITLE = "recipeCookingMethodSubTitle";
 
 /** The Constant RECIPE_COPY_TITLE. */
 private static final String RECIPE_COOKING_METHOD_TITLE = "recipeCookingMethodTitle";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(FeaturedInstagramPostViewHelperImpl.class);
 
 @Reference
 HttpClientBuilderFactory factory;
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  
  LOGGER.info("Recipe Copy View Helper #processData called for processing fixed list.");
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  Map<String, Object> recipesMap = new LinkedHashMap<String, Object>();
  
  Map<String, Object> dialogProperties = getProperties(content);
  
  Page currentPage = getCurrentPage(resources);
  String jsonNameSpace = getJsonNameSpace(content);
  
  String recipeCopyTitle = MapUtils.getString(dialogProperties, RECIPE_COOKING_METHOD_TITLE);
  String recipeCopySubTitle = MapUtils.getString(dialogProperties, RECIPE_COOKING_METHOD_SUB_TITLE);
  
  RecipeConfigDTO recipeConfigDTO = RecipeHelper.getRecipeConfigurations(configurationService, currentPage, jsonNameSpace);
  
  RMSJson.getRecipeJsonFromPage(resources, dialogProperties, recipesMap, recipeConfigDTO, factory);
  RMSJson.setRMSServiceDownKey(resources, recipesMap, configurationService, getI18n(resources));
  recipesMap.put(RECIPE_COOKING_METHOD_TITLE, recipeCopyTitle);
  recipesMap.put(RECIPE_COOKING_METHOD_SUB_TITLE, recipeCopySubTitle);
  data.put(jsonNameSpace, recipesMap);
  return data;
 }
 
}
