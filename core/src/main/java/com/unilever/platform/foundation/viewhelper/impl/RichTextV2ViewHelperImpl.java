/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * The Class BodyCopyViewHelperImpl.
 */
@Component(description = "RichTextV2ViewHelperImpl", immediate = true, metatype = true, label = "RichTextV2ViewHelperImpl")
@Service(value = { RichTextV2ViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.RICH_TEXT_V2, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class RichTextV2ViewHelperImpl extends BaseViewHelper {
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(RichTextV2ViewHelperImpl.class);
 
 /** The Constant TEXT. */
 private static final String TEXT = "text";
 
 /** The Constant DESCRIPTION. */
 private static final String DESCRIPTION = "description";
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper# processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.info("inside richText view helper");
  ResourceResolver resourceResolver = getResourceResolver(resources);
  Map<String, Object> data = new HashMap<String, Object>();
  try {
   SlingHttpServletRequest slingRequest = getSlingRequest(resources);
   Resource compResource = slingRequest.getResource();
   Map<String, Object> properties = compResource.getValueMap();
   String jsonNameSpace = MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE);
   Map<String, Object> richTextMap = new HashMap<String, Object>();
   String text = properties.get(TEXT) != null ? (String) properties.get(TEXT) : "";
   richTextMap.put(DESCRIPTION, text);
   PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
   Page currentPage = pageManager.getContainingPage(compResource);
   richTextMap.put(UnileverConstants.PUBLISH_DATE, ComponentUtil.getPublishedDate(currentPage));
   data.put(jsonNameSpace, richTextMap);
   LOGGER.debug("Map of rich text created, Size of rich text map is " + richTextMap.size());
  } catch (Exception e) {
   LOGGER.error("Error At RichTextViewHelperImpl " + ExceptionUtils.getStackTrace(e));
  }
  return data;
 }
}
