/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.components.helper;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.Article;
import com.unilever.platform.foundation.viewhelper.constants.ArticleConstants;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

import com.day.cq.commons.jcr.JcrConstants;

/**
 * The Class ArticleHelper.
 * 
 * @author vku105
 */
public class ArticleHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ArticleHelper.class);
 private static final String RATING_REVIEWS = "ratingReviews";
 private static final String PARENT_TITLE = "title";
 private static final String PARENT_URL = "url";
 private static final String BAZAARVOICE = "bazaarvoice";
 private static final String KRITIQUE = "kritique";
 
 /**
  * Instantiates a new Article Helper.
  */
 private ArticleHelper() {
  
 }
 
 /**
  * Gets the Article ratings map.
  * 
  * @param article
  *         the article
  * @param page
  *         the page
  * @param configurationService
  *         the configuration service
  * @param componentName
  *         the component name
  * @param finalArticlePage
  *         the finalArticlePage
  * @return the article ratings map
  */
 public static Map<String, Object> getArticleRatingsMap(Article article, Page page, ConfigurationService configurationService, String componentName,
   Page finalArticlePage) {
  Map<String, Object> details = new HashMap<String, Object>();
  String uniqueId = StringUtils.EMPTY;
  String articleCategorySourceId = StringUtils.EMPTY;
  String articleEntityDescription = StringUtils.EMPTY;
  if (finalArticlePage != null) {
   uniqueId = (String) finalArticlePage.getProperties().getOrDefault(UnileverConstants.UNIQUE_PAGE_ID, StringUtils.EMPTY);
   articleCategorySourceId = finalArticlePage.getParent() != null ? (String) finalArticlePage.getParent().getProperties()
     .getOrDefault(UnileverConstants.UNIQUE_PAGE_ID, StringUtils.EMPTY) : StringUtils.EMPTY;
   articleEntityDescription = finalArticlePage.getProperties() != null ? (String) finalArticlePage.getProperties().getOrDefault(JcrConstants.JCR_DESCRIPTION,
     StringUtils.EMPTY) : StringUtils.EMPTY;
  }
  String entityType = ArticleConstants.ARTICLE;
  String viewType = StringUtils.EMPTY;
  try {
   if (componentName != null) {
    viewType = configurationService.getConfigValue(page, ProductConstants.KQ_VIEW_TYPE, componentName);
   }
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Currency not found in global configurations", e);
  }
  if (null != article) {
   String articleImageURL = article.getImage() != null ? article.getImage().getUrl() : StringUtils.EMPTY;
   String articleCategoryName = article.getParent() != null ? (String) article.getParent().getOrDefault(PARENT_TITLE, StringUtils.EMPTY)
     : StringUtils.EMPTY;
   String articleCategoryPageURL = article.getParent() != null ? (String) article.getParent().getOrDefault(PARENT_URL, StringUtils.EMPTY)
     : StringUtils.EMPTY;
   details.put(ArticleConstants.ARTICLE_UNIQUE_ID, uniqueId);
   details.put(ArticleConstants.ARTICLE_ENTITY_NAME, article.getTitle());
   details.put(ArticleConstants.ARTICLE_ENTITY_DESC, articleEntityDescription);
   details.put(ArticleConstants.ARTICLE_ENTITY_URL, article.getUrl());
   details.put(ArticleConstants.ARTICLE_IMAGE_URL, articleImageURL);
   details.put(ArticleConstants.ARTICLE_CATEGORY_SOURCE_ID, articleCategorySourceId);
   details.put(ArticleConstants.ARTICLE_CATEGORY_NAME, articleCategoryName);
   details.put(ArticleConstants.ARTICLE_CATEGORY_PAGE_URL, articleCategoryPageURL);
   details.put(ArticleConstants.VIEW_TYPE, viewType);
   details.put(ArticleConstants.ENTITY_TYPE, entityType);
  }
  
  LOGGER.debug("Article Ratings Map Properties : " + details);
  
  return details;
 }
 
 /**
  * Gets the Article ratings map.
  * 
  * @param contentType
  *         the contentType
  * @param currentPage
  *         the currentPage
  * @param slingRequest
  *         the slingRequest
  * @param configurationService
  *         the configurationService
  * @param resourceResolver
  *         the resourceResolver
  * @return the alternatePage
  */
 public static Page getAlternateArticlePage(String contentType, Page currentPage, SlingHttpServletRequest slingRequest,
   ConfigurationService configurationService, ResourceResolver resourceResolver) {
  Page alternatePage = null;
  String availableContentType = StringUtils.EMPTY;
  String articlePageSelector = StringUtils.EMPTY;
  try {
   availableContentType = configurationService
     .getConfigValue(currentPage, ArticleConstants.ARTICLE_CONFIG_CAT, ArticleConstants.ARTICLE_CONTENT_TYPE);
   articlePageSelector = configurationService
     .getConfigValue(currentPage, ArticleConstants.ARTICLE_CONFIG_CAT, ArticleConstants.ARTICLE_PAGE_SELECTOR);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("articleContentTypes not found in global configurations", e);
  }
  String[] availableContentTypes = availableContentType.split(",");
  List<String> availableContentTypesList = Arrays.asList(availableContentTypes);
  if (availableContentTypesList.contains(contentType)) {
   alternatePage = currentPage;
  } else if (null != slingRequest.getParameter(articlePageSelector) && StringUtils.isNotEmpty(slingRequest.getParameter(articlePageSelector))) {
   PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
   alternatePage = pageManager.getPage(slingRequest.getParameter(articlePageSelector));
  }
  return alternatePage;
  
 }
 
 /**
  * Gets the review map properties.
  * 
  * @param configurationService
  *         the configuration service
  * @param page
  *         the page
  * @return the review map properties
  */
 public static Map<String, String> getReviewMapProperties(
 
 ConfigurationService configurationService, Page page) {
  String serviceProviderNameReview = StringUtils.EMPTY;
  String isEnabled = StringUtils.EMPTY;
  String serviceProviderName = StringUtils.EMPTY;
  Map<String, String> reviewMapProperties = new HashMap<String, String>();
  try {
   Resource resource = page.getContentResource();
   InheritanceValueMap valueMap = new HierarchyNodeInheritanceValueMap(resource);
   if (valueMap != null) {
    serviceProviderName = (String) valueMap.getInherited(RATING_REVIEWS, StringUtils.EMPTY);
   }
   if (StringUtils.isNotBlank(serviceProviderName)) {
    serviceProviderNameReview = serviceProviderName;
   } else {
    serviceProviderNameReview = configurationService.getConfigValue(page, ArticleConstants.ARTICLE_RATING_AND_REVIEWS,
      ArticleConstants.SERVICE_PROVIDER_NAME);
   }
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find serviceProviderName key in serviceProviderNameShopNow category", e);
  }
  
  try {
   reviewMapProperties = configurationService.getCategoryConfiguration(page, serviceProviderNameReview);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find serviceProviderNameReview category in configuration hence disabling the reviews.", e);
   reviewMapProperties.put(ArticleConstants.ENABLED, ProductConstants.FALSE);
  }
  try {
   isEnabled = configurationService.getConfigValue(page, ArticleConstants.ARTICLE_RATING_AND_REVIEWS, ArticleConstants.ENABLED);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find isRatingAndReviewEnabled key in productOverview category", e);
  }
  reviewMapProperties.put(ProductConstants.ENABLED, isEnabled);
  if (BAZAARVOICE.equalsIgnoreCase(serviceProviderNameReview) || serviceProviderNameReview.startsWith(BAZAARVOICE)) {
   reviewMapProperties.put("serviceProviderName", BAZAARVOICE);
  } else if (KRITIQUE.equalsIgnoreCase(serviceProviderNameReview) || serviceProviderNameReview.startsWith(KRITIQUE)) {
   reviewMapProperties.put("serviceProviderName", KRITIQUE);
  }
  LOGGER.debug("Review Map Properties : " + reviewMapProperties);
  return reviewMapProperties;
 }
}
