/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.components;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.api.Product;
import com.adobe.cq.commerce.common.CommerceHelper;
import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfiguration;
import com.unilever.platform.foundation.constants.UnileverConstants;

/**
 * The Class KritiqueSEO.
 */
/*
 * This class is used to implement SEO functionality.
 */
@Component(immediate = true, metatype = false)
@Service(value = KritiqueSEO.class)
public class KritiqueSEO {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(KritiqueSEO.class);
 
 /** The Constant KQ_KEY. */
 private static final String KQ_KEY = "kritique";
 
 /** The Constant PRODUCT_NAME. */
 private static final String PRODUCT_NAME = "ProductName";
 
 /** The Constant NO_OF_RECORDS. */
 private static final String NO_OF_RECORDS = "NoOfRecords";
 
 /** The Constant SITE_PRODUCT_ID. */
 private static final String SITE_PRODUCT_ID = "SiteProductId";
 
 /** The Constant IS_SYNDICATION_ON. */
 private static final String IS_SYNDICATION_ON = "IsSyndicationOn";
 
 /** The Constant SITE_SOURCE. */
 private static final String SITE_SOURCE = "siteSource";
 
 /** The Constant LOCALE_ID. */
 private static final String LOCALE_ID = "LocaleId";
 
 /** The Constant BRAND_ID. */
 private static final String BRAND_ID = "BrandId";
 
 /** The Constant AMPERSAND. */
 private static final String AMPERSAND = "&";
 
 /** The Constant EQUALS. */
 private static final String EQUALS = "=";
 
 /** The Constant PAGE_URL. */
 private static final String PAGE_URL = "PageURL";
 
 /** The Constant IDENTIFIER_VALUE. */
 private static final String IDENTIFIER_VALUE = "IdentifierValue";
 
 /** The Constant PAGE_NUMBER. */
 private static final String PAGE_NUMBER = "PageNumber";
 
 /** The Constant ID_TYPE. */
 private static final String ID_TYPE = "IdType";
 
 /** The Constant DEAFULT_CONNECTION_TIMEOUT. */
 private static final int DEAFULT_CONNECTION_TIMEOUT = 2000;
 
 /** The factory. */
 @Reference
 org.apache.http.osgi.services.HttpClientBuilderFactory factory;
 
 /**
  * Gets the SEO with sdk.
  * 
  * @param page
  *         the page
  * @param configurationService
  *         the configuration service
  * @param slingRequest
  *         the slingRequest service
  * @return the SEO with sdk
  */
 public String getSEOWithSDK(SlingHttpServletRequest slingRequest, Page page, ConfigurationService configurationService) {
  String output = "";
  String apiKey = "";
  long brandId = 0;
  boolean isSyndicationOn = false;
  long localeId = 0;
  int noOfRecords = 0;
  String productName = "";
  String siteProductId = "";
  String siteSource = "";
  String apiPath = "";
  String idType = "";
  String identifierValue = "";
  String pageNumber = "1";
  String pageUrl = "";
  HttpGet requestService = null;
  UNIProduct product = null;
  
  try {
   
   Product pageProduct = CommerceHelper.findCurrentProduct(page);
   if (pageProduct instanceof UNIProduct) {
    product = (UNIProduct) pageProduct;
   }
   
   if (slingRequest != null) {
    pageUrl = StringEscapeUtils.escapeHtml4(URLEncoder.encode(slingRequest.getRequestURL().toString(), "UTF-8"));
   }
   if (page != null) {
    ValueMap vMap = page.getProperties();
    String recipeId = vMap.containsKey(UnileverConstants.RECIPE_ID) ? vMap.get(UnileverConstants.RECIPE_ID).toString() : StringUtils.EMPTY;
    GlobalConfiguration globalConfiguration = page.adaptTo(GlobalConfiguration.class);
    apiPath = globalConfiguration.getConfigValue("kritiqueSEOApiPath");
    
    apiKey = getProperteryFromConfig(page, configurationService, KQ_KEY, "apiKey");
    brandId = Long.parseLong(getProperteryFromConfig(page, configurationService, KQ_KEY, "brandId"));
    localeId = Long.parseLong(getProperteryFromConfig(page, configurationService, KQ_KEY, "localeId"));
    siteSource = getProperteryFromConfig(page, configurationService, KQ_KEY, "sitesource");
    isSyndicationOn = Boolean.parseBoolean(getProperteryFromConfig(page, configurationService, KQ_KEY, "syndication"));
    noOfRecords = Integer.parseInt(getProperteryFromConfig(page, configurationService, KQ_KEY, "recordsCount"));
    if(product != null){
     siteProductId = product.getSKU();
     productName = StringEscapeUtils.escapeHtml4(URLEncoder.encode(product.getName(), "UTF-8"));
     idType = product.getIdentifierType();
     identifierValue = product.getIdentifierValue();
    } else if(StringUtils.isNotEmpty(recipeId)){
     siteProductId = recipeId;
     //as there is no such name defined for recipe, passing the recipe page title
     productName = StringEscapeUtils.escapeHtml4(URLEncoder.encode(page.getTitle(), "UTF-8"));
     identifierValue = recipeId;
    }
    
    String serviceURL = "";
    
    // Get API url as per region
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(apiPath);
    stringBuilder.append(AMPERSAND).append(BRAND_ID).append(EQUALS).append(brandId);
    stringBuilder.append(AMPERSAND).append(LOCALE_ID).append(EQUALS).append(localeId);
    stringBuilder.append(AMPERSAND).append(SITE_SOURCE).append(EQUALS).append(siteSource);
    stringBuilder.append(AMPERSAND).append(IS_SYNDICATION_ON).append(EQUALS).append(isSyndicationOn);
    stringBuilder.append(AMPERSAND).append(ID_TYPE).append(EQUALS).append(idType);
    stringBuilder.append(AMPERSAND).append(IDENTIFIER_VALUE).append(EQUALS).append(identifierValue);
    stringBuilder.append(AMPERSAND).append(SITE_PRODUCT_ID).append(EQUALS).append(siteProductId);
    stringBuilder.append(AMPERSAND).append(NO_OF_RECORDS).append(EQUALS).append(noOfRecords);
    stringBuilder.append(AMPERSAND).append(PAGE_NUMBER).append(EQUALS).append(pageNumber);
    stringBuilder.append(AMPERSAND).append(PRODUCT_NAME).append(EQUALS).append(productName);
    stringBuilder.append(AMPERSAND).append(PAGE_URL).append(EQUALS).append(pageUrl);
    
    serviceURL = stringBuilder.toString();
    LOGGER.info("Service Url : " + serviceURL);
    
    RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(DEAFULT_CONNECTION_TIMEOUT)
      .setConnectionRequestTimeout(DEAFULT_CONNECTION_TIMEOUT).setSocketTimeout(DEAFULT_CONNECTION_TIMEOUT).build();
    
    HttpClient httpclient = factory.newBuilder().disableAutomaticRetries().disableConnectionState().disableRedirectHandling()
      .setDefaultRequestConfig(requestConfig).build();
    
    requestService = new HttpGet(serviceURL);
    
    // add request header
    requestService.setHeader("Content-Type", "application/json");
    requestService.setHeader("X-ApiKey", apiKey);
    requestService.setHeader("Referer", slingRequest.getRequestURL().toString());
    
    HttpResponse response = httpclient.execute(requestService);
    StatusLine statusLine = response.getStatusLine();
    int statuscode = statusLine.getStatusCode();
    LOGGER.info("statuscode : " + statuscode + " statusLine : " + statusLine);
    if (statuscode == HttpStatus.SC_OK) {
     HttpEntity entity = response.getEntity();
     InputStream content = entity.getContent();
     output = convertStreamToString(content);
     output = output.replace("\"", "").replace("\n", "").replace("\r", "");
    }
   }
  } catch (Exception e) {
   LOGGER.warn("Error in getting KQ SEO : ", e.getMessage());
   LOGGER.error("Error in getting KQ SEO : ", e);
   return StringUtils.EMPTY;
  } finally {
   if (requestService != null) {
    requestService.releaseConnection();
   }
  }
  
  return output;
 }
 
 /**
  * Gets the propertery from config.
  * 
  * @param page
  *         the page
  * @param configurationService
  *         the configuration service
  * @param configCategory
  *         the config category
  * @param configKey
  *         the config key
  * @return the propertery from config
  */
 private String getProperteryFromConfig(Page page, ConfigurationService configurationService, String configCategory, String configKey) {
  
  String value = StringUtils.EMPTY;
  try {
   value = configurationService.getConfigValue(page, configCategory, configKey);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error(configKey + "not found in global configurations", e);
  }
  return value;
 }
 
 /**
  * Convert stream to string.
  * 
  * @param input
  *         the input
  * @return the string
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  */
 private String convertStreamToString(InputStream input) throws IOException {
  BufferedReader reader = new BufferedReader(new InputStreamReader(input));
  StringBuilder out = new StringBuilder();
  String newLine = System.getProperty("line.separator");
  String line;
  while ((line = reader.readLine()) != null) {
   out.append(line);
   out.append(newLine);
  }
  return out.toString();
 }
}
