/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;
import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.utils.AdaptiveUtil;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * This class prepares the data map for Anchor Link.
 */
@Component(description = "AnchorLink", immediate = true, metatype = true, label = "AnchorLinkViewHelperImpl")
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.ANCHOR_LINK, propertyPrivate = true), })
public class AnchorLinkViewHelperImpl extends BaseViewHelper {
 /** The Constant ALT_TEXT. */
 private static final String ALT_TEXT = "altText";
 
 /** The Constant FILE_NAME. */
 private static final String FILE_NAME = "fileName";
 
 /** The Constant FILE_REFERENCE. */
 private static final String FILE_REFERENCE = "fileReference";
 
 /** The Constant EXTENSION. */
 private static final String EXTENSION = "extension";
 
 /** The Constant ANCHOR_LINK_CATEGORY. */
 private static final String ANCHOR_LINK_CATEGORY = "anchorLinkComponent";
 
 /** The Constant ANCHOR_LINK_LOGO. */
 private static final String ANCHOR_LINK_LOGO = "logo";
 
 /** The Constant ANCHOR_LINK_ALT_TEXT. */
 private static final String ANCHOR_LINK_ALT_TEXT = "altText";
 
 /** The Constant ANCHOR_LINK_MOBILE_TEXT. */
 private static final String ANCHOR_LINK_MOBILE_TEXT = "mobileText";
 /** i18n tag for title */
 private static final String ANCHOR_LINK_MOBILE_TEXT_I18N = "anchorlink.mobileText";
 /** Constant for getting anchor link label */
 private static final String ANCHOR_LABEL = "label";
 /** Constant for getting anchor link flag show as navigatiion */
 private static final String SHOW_AS_NAV = "showAsNavigation";
 /** Constant for getting random number property */
 private static final String RANDOM_NUMBER = "randomNumber";
 /** Constant for setting anchor link id */
 private static final String ANCHOR_ID = "id";
 
 /** The Constant ANCHOR_LINKS. */
 private static final String ANCHOR_LINKS = "anchorLinks";
 
 /** The Constant REG_EX_SPECIAL_CHAR. */
 private static final String REG_EX_SPECIAL_CHAR = "[^0-9^A-Z^a-z]";
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(AnchorLinkViewHelperImpl.class);
 
 /*
  * (Javadoc)
  * 
  * This helper class reads all the data from the anchor link component.It takes the logo, alt text and It fetcher "mobile view text" from the global
  * configsUsing the logo field, it extracts the extension, path, name of the file and passesit in the json
  * 
  * For details on json and component structure,please visit https://tools.sapient .com/confluence/display/UU/CP-03+Anchor+Link+Navigation
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("Executing AnchorLinkViewHelperImpl.java. Inside processData method");
  Map<String, Object> properties = new HashMap<String, Object>();
  Resource currentResource = getCurrentResource(resources);
  
  Page currentPage = getCurrentPage(resources);
  I18n i18n = getI18n(resources);
  Resource parentResource = currentResource.getParent();
  Node parentNode = parentResource.adaptTo(Node.class);
  
  if (AdaptiveUtil.isAdaptive(getSlingRequest(resources))) {
   properties.put(ANCHOR_LINKS, getanchorLinksMap(parentNode).toArray());
  }
  
  Map<String, Object> logoProperties = new HashMap<String, Object>();
  
  LOGGER.info("Component found for current page " + currentResource.getName());
  // stores the extension, file name and file path if the "logo" field
  // is given
  
  String logoPath = StringUtils.EMPTY;
  try {
   logoPath = configurationService.getConfigValue(currentPage, ANCHOR_LINK_CATEGORY, ANCHOR_LINK_LOGO);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Configuration for logo Not found:", e);
  }
  if (!StringUtils.isBlank(logoPath)) {
   Resource assetResource = getResourceResolver(resources).getResource(logoPath);
   Asset asset = assetResource.adaptTo(Asset.class);
   String name = asset.getName().substring(0, asset.getName().lastIndexOf("."));
   logoProperties.put(FILE_NAME, name);
   String ext = FilenameUtils.getExtension(logoPath);
   logoProperties.put(EXTENSION, ext);
   String fileRef = logoPath;
   fileRef = fileRef.substring(0, logoPath.lastIndexOf("."));
   logoProperties.put(FILE_REFERENCE, fileRef);
  } else {
   logoProperties.put(FILE_NAME, logoPath);
   logoProperties.put(EXTENSION, logoPath);
   logoProperties.put(FILE_REFERENCE, logoPath);
  }
  
  String altText = StringUtils.EMPTY;
  try {
   altText = configurationService.getConfigValue(currentPage, ANCHOR_LINK_CATEGORY, ANCHOR_LINK_ALT_TEXT);
   
  } catch (ConfigurationNotFoundException e) {
   LOGGER.info("Configuration for alt text Not found:", e);
  }
  logoProperties.put(ALT_TEXT, altText);
  
  properties.put(ANCHOR_LINK_LOGO, logoProperties);
  
  String anchorLinkTitle = StringUtils.EMPTY;
  anchorLinkTitle = i18n.get(ANCHOR_LINK_MOBILE_TEXT_I18N);
  properties.put(ANCHOR_LINK_MOBILE_TEXT, anchorLinkTitle);
  LOGGER.info("Exiting anchor link view helper ");
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE), properties);
  return data;
  
 }
 
 /**
  * 
  * @param parentNode
  * @param currentPagePath
  * @param resourceResolver
  * @return
  */
 private List<Map<String, Object>> getanchorLinksMap(Node parentNode) {
  List<Map<String, Object>> anchorLinkDetailList = new ArrayList<Map<String, Object>>();
  try {
   NodeIterator childNodes = parentNode.getNodes();
   while (childNodes.hasNext()) {
    Node parsysLevelNode = childNodes.nextNode();
    getAnchorNavMap(anchorLinkDetailList, parsysLevelNode);
   }
  } catch (RepositoryException e1) {
   LOGGER.equals("Path Not Found Exception for the node path " + e1);
  }
  return anchorLinkDetailList;
 }
 
 /**
  * @param anchorLinkDetailList
  * @param parsysLevelNode
  * @throws RepositoryException
  */
 private void getAnchorNavMap(List<Map<String, Object>> anchorLinkDetailList, Node parsysLevelNode) throws RepositoryException {
  Map<String, Object> anchorNavMap;
  if (parsysLevelNode.hasProperty(ANCHOR_LABEL) && parsysLevelNode.hasProperty(SHOW_AS_NAV)) {
   anchorNavMap = getAnchorMap(parsysLevelNode);
   anchorLinkDetailList.add(anchorNavMap);
  }
  NodeIterator parysNodes = parsysLevelNode.getNodes();
  while (parysNodes.hasNext()) {
   anchorNavMap = new HashMap<String, Object>();
   Node actualNode = parysNodes.nextNode();
   if (actualNode.hasProperty(ANCHOR_LABEL) && actualNode.hasProperty(SHOW_AS_NAV)) {
    anchorNavMap = getAnchorMap(actualNode);
    anchorLinkDetailList.add(anchorNavMap);
   }
  }
 }
 
 /**
  * 
  * @param node
  * @param path
  * @param resourceResolver
  * @return
  */
 private Map<String, Object> getAnchorMap(Node node) {
  Map<String, Object> anchorMap = new HashMap<String, Object>();
  String anchorLabel = null;
  String showAsNavigation = null;
  String randomNumber;
  String id = null;
  try {
   anchorLabel = node.getProperty(ANCHOR_LABEL).getString();
   showAsNavigation = node.getProperty(SHOW_AS_NAV).getString();
   randomNumber = node.getProperty(RANDOM_NUMBER).getString();
   id = anchorLabel.replaceAll(REG_EX_SPECIAL_CHAR, "").toLowerCase() + randomNumber;
   
   anchorMap.put(ANCHOR_LABEL, anchorLabel);
   anchorMap.put(SHOW_AS_NAV, showAsNavigation);
   anchorMap.put(ANCHOR_ID, "#" + id);
   
  } catch (RepositoryException e) {
   LOGGER.error("Path Not Found for Node " + e);
  }
  return anchorMap;
 }
 
}
