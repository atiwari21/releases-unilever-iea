/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.core.service.SearchService;
import com.unilever.platform.aem.foundation.dam.DAMAssetUtility;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.constants.CommonConstants;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.Image;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * <p>
 * Responsible for reading author input for featured category component and returns a featured product.
 * 
 * </p>
 */
@Component(description = "SpotlightViewHelperImpl", immediate = true, metatype = true, label = "SpotlightViewHelperImpl")
@Service(value = { SpotlightViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.SPOTLIGHT, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class SpotlightViewHelperImpl extends BaseViewHelper {
 
 /**
  * Constant for setting landing page.
  */
 private static final String LANDING_PAGE_PATH = "landingPage";
 
 /**
  * Constant for setting page type.
  */
 private static final String PAGE_TYPE = "pageType";
 
 /**
  * Constant for retrieving anchor link navigation label.
  */
 private static final String ANCHOR_MAP = "anchorLinkNavigation";
 
 /**
  * Constant for setting featured category page type product.
  */
 private static final String PRODUCT = "Product";
 
 /**
  * Constant for retrieving background image.
  */
 private static final String FEATURED_BACKGROUND = "backgroundImage";
 
 /**
  * Constant for retrieving alt text for background.
  */
 
 private static final String ALT_BACKGROUND = "altBackgroundImage";
 
 /**
  * Constant for setting cta label.
  */
 
 private static final String LABEL_TEXT = "labelText";
 
 /**
  * Constant for setting fileName.
  */
 
 private static final String FILE_NAME = "fileName";
 
 /**
  * Constant for setting navigation label.
  */
 
 private static final String NAVIGATION_URL = "navigationURL";
 
 /**
  * Constant for setting product info position.
  */
 
 private static final String IMAGE_POSITION = "imagePosition";
 
 /**
  * Constant for setting cta map.
  */
 
 private static final String CTA = "cta";
 
 /**
  * Constant for setting cta value.
  */
 
 private static final String CTA_TEXT = "ctaText";
 
 /** The Constant NEW_WINDOW. */
 private static final String NEW_WINDOW = "newWindow";
 
 /** The Constant OPEN_IN_NEW_WINDOW. */
 private static final String OPEN_IN_NEW_WINDOW = "openInNewWindow";
 
 /** The Constant GLOBAL_CONFIG_CAT. */
 private static final String GLOBAL_CONFIG_CAT = "featuredSocialTopic";
 /**
  * Constant for retrieving spotlight product type.
  */
 private static final String SPOTLIGHT_TYPE = "spotlightType";
 
 /**
  * Constant for setting featured product type auto.
  */
 
 private static final String AUTO = "Auto";
 /** The Constant START_DATE. */
 private static final String START_DATE = "startDate";
 /** The Constant END_DATE. */
 private static final String END_DATE = "endDate";
 
 /**
  * logger object.
  **/
 
 @Reference
 ConfigurationService configurationService;
 
 /** The search service. */
 @Reference
 SearchService searchService;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(SpotlightViewHelperImpl.class);
 
 private static final String FOREGROUND_IMAGE = "foregroundImage";
 
 private static final String ALT_FOREGROUND = "altForegroundImage";
 
 private static final String MODE = "mode";
 
 /**
  * The method processData overrides method is responsible validating author input and returning a map Map can be accessed in JSP in consistent
  * way.This method also validates the link url provided by author.
  * 
  * @param content
  *         , - {@link Map} of all user input received as input <code>ViewHelper.onGetData</code> method
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return {@link Map} holding the data
  */
 @Override
 @SuppressWarnings("unchecked")
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.info("Spotlight View Helper #processData called for processing fixed list.");
  Map<String, Object> properties = (Map<String, Object>) content.get(PROPERTIES);
  Map<String, Object> spotlightFinalMap = new LinkedHashMap<String, Object>();
  ResourceResolver resourceResolver = (ResourceResolver) resources.get(ProductConstants.RESOURCE_RESOLVER);
  
  String jsonNameSpace = MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE);
  String spotlightType = MapUtils.getString(properties, SPOTLIGHT_TYPE);
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  
  SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) resources.get(ProductConstants.REQUEST);
  Resource compResource = slingRequest.getResource();
  Page currentPage = pageManager.getContainingPage(compResource);
  
  Map<String, String> configMap = ComponentUtil.getConfigMap(currentPage, GLOBAL_CONFIG_CAT);
  if (AUTO.equals(spotlightType)) {
   spotlightFinalMap = getValuesForPageType(resourceResolver, properties, currentPage, pageManager, configMap, resources);
  } else {
   spotlightFinalMap = getValuesForManualType(resourceResolver, properties, currentPage, resources);
  }
  Map<String, Object> dataManual = new LinkedHashMap<String, Object>();
  dataManual.put(jsonNameSpace, spotlightFinalMap);
  LOGGER.debug("created final data Map used by spotlight, size of this map is " + dataManual.size());
  return dataManual;
  
 }
 
 private Map<String, Object> getValuesForManualType(ResourceResolver resourceResolver, Map<String, Object> properties, Page currentPage,
   Map<String, Object> resources) {
  
  Map<String, Object> finalProperties = new LinkedHashMap<String, Object>();
  Map<String, Object> productImageMap = getProductImageProperties(properties, currentPage, resources);
  Map<String, Object> backgroundImageMap = getImageProperties(properties, currentPage, resources);
  String pageType = StringUtils.EMPTY;
  LOGGER.debug("created featured product range image manual map ,size of the Map is " + productImageMap.size());
  String themeBackground = checkNullValues(MapUtils.getString(properties, "theme"));
  String rtetext = checkNullValues(MapUtils.getString(properties, "rte"));
  // cta map
  Map<String, Object> ctaMap = new LinkedHashMap<String, Object>();
  String ctaText = checkNullValues(MapUtils.getString(properties, CTA_TEXT));
  String pagePath = MapUtils.getString(properties, NAVIGATION_URL);
  String navigationURL = ComponentUtil.getFullURL(resourceResolver, pagePath, getSlingRequest(resources));
  ctaMap.put(LABEL_TEXT, ctaText);
  ctaMap.put(NAVIGATION_URL, navigationURL);
  String newWindow = checkNullValues(MapUtils.getString(properties, NEW_WINDOW));
  if (newWindow.equals(CommonConstants.BOOLEAN_TRUE)) {
   ctaMap.put(OPEN_IN_NEW_WINDOW, Boolean.valueOf(CommonConstants.BOOLEAN_TRUE));
  } else {
   ctaMap.put(OPEN_IN_NEW_WINDOW, Boolean.valueOf(ProductConstants.FALSE));
  }
  String mantitle = MapUtils.getString(properties, UnileverConstants.TITLE);
  finalProperties = commonProperties(finalProperties, properties);
  LOGGER.debug("created Map for common fields used by featured product range auto and manual,size of this map is " + finalProperties.size());
  finalProperties.put(UnileverConstants.TEASER_IMAGE, productImageMap);
  finalProperties.put(FEATURED_BACKGROUND, backgroundImageMap);
  finalProperties.put(CommonConstants.THEME, themeBackground.toLowerCase());
  finalProperties.put(PAGE_TYPE, StringUtils.isNotBlank(pageType) ? pageType.toLowerCase() : StringUtils.EMPTY);
  finalProperties.put(UnileverConstants.TEASER_COPY, rtetext);
  finalProperties.put(UnileverConstants.TEASER_TITLE, StringUtils.isNotBlank(mantitle) ? mantitle : StringUtils.EMPTY);
  
  finalProperties.put(CTA, ctaMap);
  
  return finalProperties;
  
 }
 
 /**
  * Gets the values for page type.
  * 
  * @param jsonNameSpace
  *         the json name space
  * @param resourceResolver
  *         the resource resolver
  * @param properties
  *         the properties
  * @param currentPage
  *         the current page
  * @param pageManager
  *         the page manager
  * @param configMap
  *         the config map
  * @return the values for page type
  */
 private Map<String, Object> getValuesForPageType(ResourceResolver resourceResolver, Map<String, Object> properties, Page currentPage,
   PageManager pageManager, Map<String, String> configMap, Map<String, Object> resources) {
  
  Map<String, Object> finalProperties = new LinkedHashMap<String, Object>();
  
  String landingPage = MapUtils.getString(properties, LANDING_PAGE_PATH);
  String pageType = MapUtils.getString(properties, PAGE_TYPE);
  Page currentLandingPage = pageManager.getContainingPage(landingPage);
  String cqTemplateConfig = getCqTemplete(configMap);
  boolean flag = true;
  if (currentLandingPage != null) {
   
   String pageTemplate = StringUtils.EMPTY;
   ValueMap valueMap = currentLandingPage.getContentResource().getValueMap();
   pageTemplate = MapUtils.getString(valueMap, NameConstants.PN_TEMPLATE);
   if (cqTemplateConfig.equals(pageTemplate)) {
    flag = false;
    ComponentUtil.setPageProperties(finalProperties, currentLandingPage, currentPage, getSlingRequest(resources));
    Map<String, Object> currentLandingPageProperties = currentLandingPage.getProperties();
    String socialChannels = MapUtils.getString(currentLandingPageProperties, UnileverConstants.SOCIAL_CHANNELS);
    finalProperties.put(UnileverConstants.SOCIAL_CHANNELS, StringUtils.isNotEmpty(socialChannels) ? socialChannels : StringUtils.EMPTY);
    
   }
  }
  if (flag) {
   ComponentUtil.setPageProperties(finalProperties, currentLandingPage, currentPage, getSlingRequest(resources));
  }
  
  // background image
  if (PRODUCT.equals(pageType)) {
   String backgroundImage = MapUtils.getString(properties, FEATURED_BACKGROUND);
   backgroundImage = checkNullValues(backgroundImage);
   
   Map<String, Object> backgroundImageMap = getImageProperties(properties, currentPage, resources);
   LOGGER.debug("created map for spotlight background image, size of the image map is " + backgroundImageMap.size());
   if (MapUtils.isNotEmpty(backgroundImageMap)) {
    finalProperties.put(FEATURED_BACKGROUND, backgroundImageMap);
   }
  } else {
   Image blankImage = new Image("", "", "", currentPage, getSlingRequest(resources));
   finalProperties.put(FEATURED_BACKGROUND, blankImage.convertToMap());
  }
  finalProperties.put(PAGE_TYPE, StringUtils.isNotBlank(pageType) ? pageType.toLowerCase() : StringUtils.EMPTY);
  // adding common properties map
  commonProperties(finalProperties, properties);
  
  // adding cta map
  getCtaMap(properties, finalProperties, resourceResolver, landingPage, getSlingRequest(resources));
  
  LOGGER.debug("created Map for getting page type properties used by spotlight, size of this map is " + finalProperties.size());
  return finalProperties;
  
 }
 
 /**
  * Gets the cq templete.
  * 
  * @param currentPage
  *         the current page
  * @param configMap
  *         the config map
  * @return the cq templete
  */
 private String getCqTemplete(Map<String, String> configMap) {
  String cQTemplate = StringUtils.EMPTY;
  cQTemplate = !StringUtils.isBlank(configMap.get(NameConstants.PN_TEMPLATE)) ? configMap.get(NameConstants.PN_TEMPLATE) : StringUtils.EMPTY;
  return cQTemplate;
 }
 
 /**
  * Common properties.
  * 
  * @param finalProperties
  *         the final properties
  * @param properties
  *         the featured category properties
  * @return the map
  */
 private Map<String, Object> commonProperties(Map<String, Object> finalProperties, Map<String, Object> properties) {
  String title = MapUtils.getString(properties, UnileverConstants.TITLE);
  String imagePosition = MapUtils.getString(properties, IMAGE_POSITION);
  if (StringUtils.isEmpty(imagePosition)) {
   imagePosition = "Left";
  }
  Map<String, String> anchorLinkMap = ComponentUtil.getAnchorLinkMap(properties);
  finalProperties.put(UnileverConstants.TITLE, StringUtils.isNotBlank(title) ? title : StringUtils.EMPTY);
  String spotlightType = MapUtils.getString(properties, SPOTLIGHT_TYPE);
  finalProperties.put(MODE, StringUtils.isNotBlank(spotlightType) ? spotlightType : AUTO);
  finalProperties.put(ANCHOR_MAP, anchorLinkMap);
  GregorianCalendar startDate = (GregorianCalendar) MapUtils.getObject(properties, START_DATE);
  finalProperties.put(START_DATE, DAMAssetUtility.getEffectiveDateFromCalendar(startDate));
  GregorianCalendar endDate = (GregorianCalendar) MapUtils.getObject(properties, END_DATE);
  finalProperties.put(END_DATE, DAMAssetUtility.getEffectiveDateFromCalendar(endDate));
  finalProperties.put(IMAGE_POSITION, StringUtils.isNotBlank(imagePosition) ? imagePosition : StringUtils.EMPTY);
  LOGGER.debug("created Map for common fields used by spotlight, size of this map is " + finalProperties.size());
  return finalProperties;
  
 }
 
 /**
  * This method returns an empty string if the key is null or undefined otherwise it returns key.
  * 
  * @param key
  *         the key
  * @return key
  */
 private String checkNullValues(Object key) {
  String value = "";
  if (null == key) {
   value = "";
  } else if ("".equals(key)) {
   value = "";
  } else {
   value = key.toString();
  }
  
  return value;
  
 }
 
 /**
  * This method is used to get background image properties like path, alt text for image, extension etc.
  * 
  * @param properties
  *         the properties
  * @param page
  *         the page
  * @return the image properties
  */
 private Map<String, Object> getImageProperties(Map<String, Object> properties, Page page, Map<String, Object> resources) {
  Map<String, Object> backgroundImageMap = new LinkedHashMap<String, Object>();
  String altImage = checkNullValues(MapUtils.getString(properties, ALT_BACKGROUND));
  String backgroundImage = checkNullValues(MapUtils.getString(properties, FEATURED_BACKGROUND));
  Image productBackgroundImage = new Image(backgroundImage, altImage, altImage, page, getSlingRequest(resources));
  backgroundImageMap = productBackgroundImage.convertToMap();
  backgroundImageMap.put(FILE_NAME, StringUtils.EMPTY);
  return backgroundImageMap;
  
 }
 
 /**
  * Gets the cta map.
  * 
  * @param properties
  *         the properties
  * @param finalProperties
  *         the final properties
  * @param resourceResolver
  *         the resource resolver
  * @param landingPage
  *         the landing page
  * @return the cta map
  */
 private Map<String, Object> getCtaMap(Map<String, Object> properties, Map<String, Object> finalProperties, ResourceResolver resourceResolver,
   String landingPage, SlingHttpServletRequest slingRequest) {
  Map<String, Object> ctaMap = new LinkedHashMap<String, Object>();
  String ctaText = checkNullValues(MapUtils.getString(properties, CTA_TEXT));
  String landingPagePath = ComponentUtil.getFullURL(resourceResolver, landingPage, slingRequest);
  
  if (StringUtils.isNotEmpty(landingPagePath)) {
   ctaMap.put(LABEL_TEXT, ctaText);
   ctaMap.put(NAVIGATION_URL, landingPagePath);
  }
  
  String newWindow = checkNullValues(MapUtils.getString(properties, NEW_WINDOW));
  if (newWindow.equals(CommonConstants.BOOLEAN_TRUE)) {
   ctaMap.put(OPEN_IN_NEW_WINDOW, Boolean.valueOf(CommonConstants.BOOLEAN_TRUE));
  } else {
   ctaMap.put(OPEN_IN_NEW_WINDOW, Boolean.valueOf(ProductConstants.FALSE));
  }
  finalProperties.put(CTA, ctaMap);
  LOGGER.debug("created Map for cta fields used by spotlight, size of this map is " + finalProperties.size());
  return finalProperties;
 }
 
 /**
  * This method is used to get product image properties(in case of manual) like path,alt text for image extension etc.
  * 
  * @param properties
  *         the properties
  * @param page
  *         the page
  * @return the product image properties
  */
 private Map<String, Object> getProductImageProperties(Map<String, Object> properties, Page page, Map<String, Object> resources) {
  
  Map<String, Object> productImageMap = new LinkedHashMap<String, Object>();
  
  String imagePath = checkNullValues(MapUtils.getString(properties, FOREGROUND_IMAGE));
  String imageAltText = checkNullValues(MapUtils.getString(properties, ALT_FOREGROUND));
  
  if (StringUtils.isNotEmpty(imagePath)) {
   Image image = new Image(imagePath, imageAltText, imageAltText, page, getSlingRequest(resources));
   productImageMap = image.convertToMap();
   
  } else {
   Image fallbackImage = new Image("", "", "", page, getSlingRequest(resources));
   productImageMap = fallbackImage.convertToMap();
   
  }
  
  return productImageMap;
  
 }
}
