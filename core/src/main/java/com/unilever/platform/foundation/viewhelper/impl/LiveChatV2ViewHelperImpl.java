/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.sling.api.SlingConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.lang.StringEscapeUtils;
import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.core.dto.BrandMarketBean;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * This class prepared the data map for LiveChatV2 component.
 * 
 */
@Component(description = "LiveChatV2", immediate = true, metatype = true, label = "LiveChatV2ViewHelperImpl")
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.LIVE_CHAT_V2, propertyPrivate = true), })
public class LiveChatV2ViewHelperImpl extends BaseViewHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(LiveChatV2ViewHelperImpl.class);
 
 /** The Constant HEADING_TEXT. */
 private static final String HEADING_TEXT = "headingText";
 
 /** The Constant ONLINE_STATUS_MESSAGE. */
 private static final String ONLINE_STATUS_MESSAGE = "onlineStatusMessage";
 
 /** The Constant OFFLINE_STATUS_MESSAGE. */
 private static final String OFFLINE_STATUS_MESSAGE = "offlineStatusMessage";
 
 /** The Constant LIVE_CHAT_V2. */
 private static final String LIVE_CHAT_V2 = "liveChatV2";
 
 /** The Constant HTML_SNIPPET. */
 private static final String HTML_SNIPPET = "htmlSnippet";
 
 private static final String BUTTON_SIZE = "buttonSize";
 private static final String BUTTON_COLOUR = "buttonColour";
 private static final String FB_APP_ID = "fbAppId";
 private static final String FB_PAGE_ID = "fbPageId";
 private static final String DATA_REF = "dataRef";
 private static final String CHAT_SERVICE = "chatService";
 private static final String J_CHAT_SERVICE = "chatService";
 private static final String J_HEADING_TEXT = "headingText";
 private static final String J_HTML_SNIPPET = "htmlSnippet";
 private static final String J_HTML_SAFE_SNIPPET = "htmlSnippetHtmlSafe";
 private static final String J_ONLINE_STATUS = "onlineStatusMessage";
 private static final String J_OFFLINE_STATUS = "offlineStatusMessage";
 private static final String J_FB_MESSENGER_CONFIG = "messengerConfig";
 private static final String J_BUTTON_SIZE = "buttonSize";
 private static final String J_BUTTON_COLOUR = "buttonColour";
 private static final String J_FB_APP_ID = "fbAppId";
 private static final String J_FB_PAGE_ID = "fbPageId";
 private static final String J_DATA_REF = "dataRef";
 private static final String CONTENT_TYPE = "contentType";
 private static final String PAGE_NAME_PLACEHOLDER = "aemPageNamePlaceholder";
 private static final String CONTENT_TYPE_PLACEHOLDER = "contentTypePlaceholder";
 private static final String BRAND_NAME_PLACEHOLDER = "brandNamePlaceholder";
 private static final String SITE_LOCALE_PLACEHOLDER = "siteLocalePlaceholder";
 private static final String FB_WIDGET_TYPE = "fbWidgetType";
 private static final String MESSENGER_CTA_LABEL = "messengerCtaLabel";
 private static final String J_FB_WIDGET_TYPE = "widgetType";
 private static final String J_MESSENGER_CTA_LABEL = "messengerCtaLabel";
 private static final String OTHER_CHAT_SERVICE = "otherChatService";
 private static final String J_OTHER_CHAT_SERVICE = "otherChatService";
 private static final String LIVE_CHAT_WITH_OTHER_SERVICE = "liveChatWithOtherService";
 private static final String MESSAGE_US = "messageUs";
 private static final String SEND_TO_MESSENGER = "sendToMessenger";
 private static final String M_ME_LINK = "liveChatV2.m.me.link";
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /**
  * This is overriden method from base view helper which prepares data map by calling all required methods.
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("Executing LiveChatV2ViewHelperImpl.java. Inside processData method");
  
  Page currentPage = getCurrentPage(resources);
  
  Map<String, Object> liveChatV2Map = new LinkedHashMap<String, Object>();
  Map<String, Object> properties = getProperties(content);
  Map<String, String> configMap = new LinkedHashMap<String, String>();
  Map<String, Object> fbMessengerConfigMap = new LinkedHashMap<String, Object>();
  try {
   configMap = configurationService.getCategoryConfiguration(currentPage, LIVE_CHAT_V2);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Exception in configurationService inside process data method of LiveCahtV2viewhelperImpl.java" + e);
  }
  
  String htmlSnippet = MapUtils.getString(configMap, HTML_SNIPPET, StringUtils.EMPTY);
  String headingText = MapUtils.getString(properties, HEADING_TEXT, StringUtils.EMPTY);
  String onlineStatusMessage = MapUtils.getString(properties, ONLINE_STATUS_MESSAGE, StringUtils.EMPTY);
  String offlineStatusMessage = MapUtils.getString(properties, OFFLINE_STATUS_MESSAGE, StringUtils.EMPTY);
  String chatService = MapUtils.getString(properties, CHAT_SERVICE, StringUtils.EMPTY);
  htmlSnippet = htmlSnippet.replace("online_status_message_placeholder", onlineStatusMessage);
  htmlSnippet = htmlSnippet.replace("offline_status_message_placeholder", offlineStatusMessage);
  htmlSnippet = htmlSnippet.replace("heading_text_placeholder", headingText);
  
  setFacebookMesengerConfig(fbMessengerConfigMap, properties, configMap, currentPage, getI18n(resources));
  
  liveChatV2Map.put(J_ONLINE_STATUS, onlineStatusMessage);
  liveChatV2Map.put(J_OFFLINE_STATUS, offlineStatusMessage);
  liveChatV2Map.put(J_FB_MESSENGER_CONFIG, fbMessengerConfigMap);
  liveChatV2Map.put(J_HEADING_TEXT, headingText);
  liveChatV2Map.put(J_CHAT_SERVICE, chatService);
  liveChatV2Map.put(J_HTML_SNIPPET, htmlSnippet);
  liveChatV2Map.put(J_HTML_SAFE_SNIPPET, StringEscapeUtils.escapeHtml(htmlSnippet));
  if(chatService.equals(LIVE_CHAT_WITH_OTHER_SERVICE)){
      liveChatV2Map.put(J_OTHER_CHAT_SERVICE, MapUtils.getString(properties, OTHER_CHAT_SERVICE, StringUtils.EMPTY));
  }
  
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  data.put(getJsonNameSpace(content), liveChatV2Map);
  
  return data;
 }
 
 private void setFacebookMesengerConfig(Map<String, Object> fbMessengerConfigMap, Map<String, Object> properties,
   Map<String, String> fbSendToMessengerMap, Page currentPage, I18n i18n) {
  
  boolean overrideGlobalConfig = MapUtils.getBoolean(properties, UnileverConstants.OVERRIDE_GLOBAL_CONFIG,false);
  
  Map<String, Object> tempMap = new LinkedHashMap<String, Object>();
  if (overrideGlobalConfig) {
   tempMap = properties;
  } else if (null != fbSendToMessengerMap) {
   tempMap.putAll(fbSendToMessengerMap);
  }
  String fbWidgetType = MapUtils.getString(tempMap, FB_WIDGET_TYPE, StringUtils.EMPTY);
  String messengerCtaLabel = overrideGlobalConfig ? MapUtils.getString(tempMap, MESSENGER_CTA_LABEL, StringUtils.EMPTY) : i18n.get(M_ME_LINK);
  String buttonSize = MapUtils.getString(tempMap, BUTTON_SIZE, StringUtils.EMPTY);
  String buttonColour = MapUtils.getString(tempMap, BUTTON_COLOUR, StringUtils.EMPTY);
  String fbAppId = MapUtils.getString(tempMap, FB_APP_ID, StringUtils.EMPTY);
  String fbPageId = MapUtils.getString(tempMap, FB_PAGE_ID, StringUtils.EMPTY);
  String dataRef = MapUtils.getString(tempMap, DATA_REF, StringUtils.EMPTY);
  if (null != currentPage) {
   String pageName = currentPage.getName();
   String contentType = (String) currentPage.getProperties().get(CONTENT_TYPE);
   BrandMarketBean bm = new BrandMarketBean(currentPage);
   String brandName = bm != null ? bm.getBrand() : StringUtils.EMPTY;
   String currentPageLocale = currentPage.getLanguage(false).toLanguageTag();
   dataRef = StringUtils.isNotEmpty(pageName) ? dataRef.replaceAll(PAGE_NAME_PLACEHOLDER, pageName) : dataRef;
   dataRef = StringUtils.isNotEmpty(contentType) ? dataRef.replaceAll(CONTENT_TYPE_PLACEHOLDER, contentType) : dataRef;
   dataRef = StringUtils.isNotEmpty(brandName) ? dataRef.replaceAll(BRAND_NAME_PLACEHOLDER, brandName) : dataRef;
   dataRef = StringUtils.isNotEmpty(currentPageLocale) ? dataRef.replaceAll(SITE_LOCALE_PLACEHOLDER, currentPageLocale) : dataRef;
  }
  if(fbWidgetType.equals(MESSAGE_US)){
      fbMessengerConfigMap.put(J_MESSENGER_CTA_LABEL, messengerCtaLabel);
  } else if(fbWidgetType.equals(SEND_TO_MESSENGER)){
      fbMessengerConfigMap.put(J_BUTTON_SIZE, buttonSize);
      fbMessengerConfigMap.put(J_BUTTON_COLOUR, buttonColour);
      fbMessengerConfigMap.put(J_FB_APP_ID, fbAppId);
  }
  fbMessengerConfigMap.put(J_FB_WIDGET_TYPE, fbWidgetType);
  fbMessengerConfigMap.put(J_FB_PAGE_ID, fbPageId);
  fbMessengerConfigMap.put(J_DATA_REF, dataRef);
  
 }
 
}
