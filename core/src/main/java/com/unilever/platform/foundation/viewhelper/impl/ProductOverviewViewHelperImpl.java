/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.aem.foundation.utils.AdaptiveUtil;
import com.unilever.platform.foundation.components.BazaarVoiceSEO;
import com.unilever.platform.foundation.components.KritiqueSEO;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.components.helper.ProductHelperExt;
import com.unilever.platform.foundation.components.helper.TurnToHelper;
import com.unilever.platform.foundation.kritique.config.KritiqueReviewsCommentsService;
import com.unilever.platform.foundation.template.dto.KritiqueDTO;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * This class prepares the data map for Product Container.
 */
@Component(description = "ProductOverviewViewHelperImpl", immediate = true, metatype = true, label = "Product Overview ViewHelper")
@Service(value = { ProductOverviewViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.PRODUCT_OVERVIEW, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class ProductOverviewViewHelperImpl extends BaseViewHelper {
 private static final String TURN_TO = "turnTo";
 
 /**
  * logger object.
  */
 @Reference
 ConfigurationService configurationService;
 
 @Reference
 KritiqueSEO kqseoService;
 
 @Reference
 BazaarVoiceSEO bvseoService;
 
 @Reference
 KritiqueReviewsCommentsService kritiqueReviewsCommentsService;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ProductOverviewViewHelperImpl.class);
 private static final String SEO = "SEO";
 private static final String ADAPTIVE_REVIEW_LABEL = "readReviews";
 private static final String ADAPTIVE_REVIEW_I18N = "productOverview.adaptiveReadReview";
 private static final String RATING_REVIEWS = "ratingReviews";
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper# processData (java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("Product Overview View Helper #processData called .");
  
  SlingHttpServletRequest slingRequest = getSlingRequest(resources);
  I18n i18n = getI18n(resources);
  Page productPage = getCurrentPage(resources);
  
  Map<String, Object> staticProperties = new HashMap<String, Object>();
  Map<String, Object> zoomProperties = new HashMap<String, Object>();
  Map<String, Object> properties = new LinkedHashMap<String, Object>();
  Map<String, Object> productProperties = new HashMap<String, Object>();
  
  String jsonNameSapce = getJsonNameSpace(content);
  
  UNIProduct product = ProductHelper.getUniProduct(productPage);
  
  // fetching product map from PDP page
  productProperties = ProductHelper.getProductMap(productPage, jsonNameSapce, resources);
  
  boolean flag = ProductHelperExt.isCustomizableProduct(product, configurationService, productPage);
  if (flag && product != null) {
   productProperties.put(ProductConstants.CUSTOMISED_CTA, getCustomiseCATMap(i18n, productPage, product, slingRequest));
   
  }
  productProperties.put(ProductConstants.PRICE_ENABLED, ProductHelper.isPriceEnabled(configurationService, productPage));
  productProperties.put(ProductConstants.BACKGROUND_IMAGE, GlobalConfigurationUtility.getValueFromConfiguration(configurationService, productPage,
    ProductConstants.PRODUCTOVERVIEW, ProductConstants.BACKGROUND_IMAGE));
  
  productProperties.put(ProductConstants.ISCAROUSELENABLED, GlobalConfigurationUtility.getValueFromConfiguration(configurationService, productPage,
    ProductConstants.PRODUCTOVERVIEW, ProductConstants.ISCAROUSELENABLED));
  String serviceProvidername = StringUtils.EMPTY;
  String serviceProviderNameTemp = StringUtils.EMPTY;
  
  try {
   Resource resource = productPage.getContentResource();
   InheritanceValueMap valueMap = new HierarchyNodeInheritanceValueMap(resource);
   if (valueMap != null) {
    serviceProviderNameTemp = (String) valueMap.getInherited(RATING_REVIEWS, StringUtils.EMPTY);
   }
   if (StringUtils.isNotBlank(serviceProviderNameTemp)) {
    serviceProvidername = serviceProviderNameTemp;
   } else {
    serviceProvidername = configurationService.getConfigValue(productPage, ProductConstants.RATING_AND_REVIEWS,
      ProductConstants.SERVICE_PROVIDER_NAME);
   }
   Boolean isEnabled = Boolean.parseBoolean(configurationService.getConfigValue(productPage, ProductConstants.RATING_AND_REVIEWS,
     ProductConstants.ENABLED));
   if (isEnabled) {
    Boolean enabled = Boolean.parseBoolean(configurationService.getConfigValue(productPage, ProductConstants.PRODUCTOVERVIEW,
      ProductConstants.SEO_ENABLED));
    if (enabled) {
     if ("bazaarvoice".equalsIgnoreCase(serviceProvidername) || serviceProvidername.startsWith("bazaarvoice")) {
      productProperties.put(SEO, bvseoService.getSEOWithSDK(productPage, configurationService, resources));
     } else if ("kritique".equalsIgnoreCase(serviceProvidername) || serviceProvidername.startsWith("kritique")) {
      productProperties.put(SEO, kqseoService.getSEOWithSDK(slingRequest, productPage, configurationService));
     }
     
    }
    
    if (AdaptiveUtil.isAdaptive(slingRequest)) {
     properties.put(ADAPTIVE_REVIEW_LABEL, i18n.get(ADAPTIVE_REVIEW_I18N));
     
     LOGGER.info("Feature phone view called and product page path is " + productPage.getPath());
     if (product != null) {
      KritiqueDTO kritiqueparams = new KritiqueDTO(productPage, configurationService);
      kritiqueparams.setSiteProductId(product.getSKU());
      
      Map<String, Object> reviewMap = getKQMap(kritiqueparams);
      
      properties.put(ProductConstants.PRODUCT_RATINGS_MAP, reviewMap);
      productProperties.put(ProductConstants.PRODUCT_RATINGS_MAP, reviewMap);
      
      LOGGER.debug("Rating map for productOverview of feature phone created succesfully");
     }
    }
   }
   
  } catch (Exception e) {
   LOGGER.error("could not find KQSEOenabled key in productOverview category", e);
  }
  properties.putAll(ProductHelperExt.getDeliveryDetailMap(productPage));
  properties.put(ProductConstants.STATIC_MAP,
    ProductHelper.getStaticMapProperties(staticProperties, zoomProperties, configurationService, i18n, productPage));
  properties.put(ProductConstants.SHOPNOW_MAP, ProductHelper.getshopNowProperties(configurationService, productPage, getI18n(resources),
    getResourceResolver(resources), getSlingRequest(resources)));
  properties.put(ProductConstants.REVIEW_MAP, ProductHelper.getReviewMapProperties(configurationService, productPage));
  
  properties.put(ProductConstants.PRODUCT_MAP, productProperties);
  properties.put(TURN_TO, TurnToHelper.getTurnToConfig(getCurrentPage(resources), configurationService, product.getSKU()));
  LOGGER.info("The Product list corresponding to Product Overview is successfully generated");
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(jsonNameSapce, properties);
  return data;
 }
 
 private Map<String, Object> getKQMap(KritiqueDTO kritiqueparams) {
  Map<String, Object> reviewMap = null;
  try {
   List<Map<String, Object>> kqMap = kritiqueReviewsCommentsService.getRatingsService(kritiqueparams, configurationService);
   if (!kqMap.isEmpty()) {
    reviewMap = kqMap.get(0);
   }
  } catch (Exception e) {
   LOGGER.error("Exception while getting kritique response ", e);
  }
  return reviewMap;
 }
 
 /**
  * Gets the customise cat map.
  * 
  * @param i18n
  *         the i18n
  * @param productPage
  *         the product page
  * @param currentProduct
  *         the current product
  * @return the customise cat map
  */
 private Map<String, Object> getCustomiseCATMap(I18n i18n, Page productPage, UNIProduct currentProduct, SlingHttpServletRequest slingRequest) {
  Map<String, Object> customiseCTAMap = new HashMap<String, Object>();
  String bannerKey = i18n.get(ProductConstants.BANNER_KEY) != null ? i18n.get(ProductConstants.BANNER_KEY) : "";
  customiseCTAMap.put(ProductConstants.BANNER_TEXT, bannerKey);
  
  String productId = currentProduct.getUniqueID();
  if (StringUtils.isBlank(productId)) {
   productId = currentProduct.getSKU();
  }
  
  String url = StringUtils.EMPTY;
  try {
   url = configurationService.getConfigValue(productPage, ProductConstants.EGIFTING_CAT, ProductConstants.TX1_URL);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find tx1URL key in customisedcta category", e);
  }
  url = StringUtils.isEmpty(url) ? "" : ComponentUtil.getFullURL(productPage.getContentResource().getResourceResolver(), url, slingRequest) + "?"
    + ProductConstants.EAN + "=" + productId;
  customiseCTAMap.put(ProductConstants.URL, url);
  return customiseCTAMap;
 }
 
}
