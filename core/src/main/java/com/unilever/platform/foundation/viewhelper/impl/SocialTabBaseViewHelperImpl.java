/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.Resource;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.api.Product;
import com.adobe.cq.commerce.common.CommerceHelper;
import com.day.cq.commons.ImageResource;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.Image;
import com.unilever.platform.foundation.template.dto.SocialTabQueryDTO;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * The Class SocialTabBaseViewHelperImpl.
 */
@Component(description = "Social Tab Base View Helper", immediate = true, metatype = true, label = "Social Tab Base View Helper")
@Service(value = { SocialTabBaseViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.SOCIAL_TAB_BASE, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class SocialTabBaseViewHelperImpl extends BaseViewHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(SocialTabBaseViewHelperImpl.class);
 
 /**
  * Constant for Hash Field.
  */
 private static final String HASH_FIELD_NAME = "hashField";
 
 /**
  * Constant for product home story cta text link.
  */
 private static final String CTA_URL = "ctaPath";
 
 /**
  * Constant for Channel List name in crx.
  */
 private static final String SOCIAL_CHANNELS = "socialChannels";
 
 /** The Constant SOCIAL_CHANNEL_IMG. */
 private static final String SOCIAL_CHANNEL = "socialChannelForAcntId";
 
 /** The Constant ACCOUNT_IDS_JSON. */
 private static final String ACCOUNT_IDS_JSON = "accountIdsJson";
 
 /**
  * Constant for ACNT_ID.
  */
 private static final String ACNT_ID = "accountId";
 /**
  * Constant for SOCIAL_CHANNEL_ACCOUNT_ID.
  */
 private static final String SOCIAL_CHANNEL_ACCOUNT_ID = "socialChannelAccountId";
 
 /**
  * Constant for title of social gallery carousel.
  */
 private static final String TITLE = "title";
 
 /**
  * Constant for introduction copy of social gallery carousel.
  */
 private static final String TEXT = "text";
 
 private static final String ACCOUNT_ID = "accountId";
 
 /** Constant for product home story cta text title. */
 private static final String CTA_TEXT = "ctaText";
 
 /** The Constant SOCIAL_GALLERY_IMAGES. */
 private static final String SOCIAL_GALLERY_IMAGES = "socialGalleryPostsJson";
 
 /** The Constant SOCIAL_GALLERY_EXPANDED_IMAGES. */
 private static final String SOCIAL_GALLERY_EXPANDED_IMAGES = "socialGalleryCarouselPostsJson";
 
 /** The Constant IMAGE_PATH. */
 private static final String IMAGE_PATH = "imagePath";
 
 /** The Constant PAGE_PATH. */
 private static final String PAGE_PATH = "pagePath";
 
 /** The Constant RELATED_CTA_LABEL. */
 private static final String RELATED_CTA_LABEL = "relatedCtaLabel";
 
 /** The Constant NUMBER_OF_POSTS. */
 public static final String NUMBER_OF_POSTS = "numberOfPosts";
 
 /** The Constant MAXIMUM_IMAGES. */
 public static final String MAXIMUM_IMAGES = "maximumImages";
 
 /** The Constant DELAY_TIME. */
 public static final String DELAY_TIME = "delayTime";
 
 /** The Constant BRAND_NAME_PARAM. */
 private static final String BRAND_NAME_PARAM = "brandNameParameter";
 
 /** Date Format for social tab base */
 protected DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
 
 /** The Constant BOOLEAN_TRUE. */
 public static final String BOOLEAN_TRUE = "true";
 
 /** The Constant BOOLEAN_FALSE. */
 public static final String BOOLEAN_FALSE = "false";
 
 /** The Constant POST_ID. */
 private static final String POST_ID = "postId";
 
 /** The Constant OPEN_IN_NEW_WINDOW. */
 public static final String OPEN_IN_NEW_WINDOW = "openInNewWindow";
 
 /** The Constant NAME. */
 public static final String NAME = "name";
 
 /** The Constant NAME. */
 public static final String URL = "url";
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  
  LOGGER.debug("Executing SocialTabBaseViewHelperImpl. Inside processData method ");
  
  String jsonNameSpace = getJsonNameSpace(content);
  Map<String, Object> socialTabBaseMap = new LinkedHashMap<String, Object>();
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(jsonNameSpace, socialTabBaseMap);
  return data;
 }
 
 /**
  * Gets the hash tags.
  * 
  * @param currentResource
  *         the current resource
  * @return the hash tags
  */
 public List<Map<String, String>> getHashTags(Resource currentResource) {
  return ComponentUtil.getNestedMultiFieldProperties(currentResource, HASH_FIELD_NAME);
 }
 
 /**
  * Gets the social channels.
  * 
  * @param properties
  *         the current resource
  * @return the social channels
  */
 public List<String> getSocialChannels(Map<String, Object> properties) {
  List<String> list = new ArrayList<String>();
  Object obj = MapUtils.getObject(properties, SOCIAL_CHANNELS);
  if (obj instanceof String[]) {
   for (String channel : (String[]) obj) {
    list.add(channel);
   }
  } else if (obj instanceof String) {
   list.add((String) obj);
   
  }
  return list;
 }
 
 /**
  * Gets the title.
  * 
  * @param properties
  *         the properties
  * @return the title
  */
 public String getTitle(Map<String, Object> properties) {
  return MapUtils.getString(properties, TITLE);
 }
 
 /**
  * Gets the post url.
  * 
  * @param properties
  *         the properties
  * @return the post url
  */
 public String getPostId(Map<String, Object> properties) {
  return MapUtils.getString(properties, POST_ID);
 }
 
 /**
  * Gets the text.
  * 
  * @param properties
  *         the properties
  * @return the text
  */
 public String getText(Map<String, Object> properties) {
  return MapUtils.getString(properties, TEXT);
 }
 
 /**
  * Gets the account id.
  * 
  * @param properties
  *         the properties
  * @return the account id
  */
 public static String getAccountId(Map<String, Object> properties) {
  return MapUtils.getString(properties, ACCOUNT_ID);
 }
 
 /**
  * Gets the cta text.
  * 
  * @param properties
  *         the properties
  * @return the cta text
  */
 public String getCtaText(Map<String, Object> properties) {
  return MapUtils.getString(properties, CTA_TEXT);
 }
 
 /**
  * Gets the cta url.
  * 
  * @param properties
  *         the properties
  * @param resourceResolver
  *         the resource resolver
  * @param resources
  * @return the cta url
  */
 public String getCtaUrl(Map<String, Object> properties, Map<String, Object> resources) {
  return ComponentUtil.getFullURL(getResourceResolver(resources), MapUtils.getString(properties, CTA_URL), getSlingRequest(resources));
 }
 
 /**
  * Gets the social gallery images map.
  * 
  * @param currentResource
  *         the current resource
  * @return the social gallery images map
  */
 public List<Map<String, String>> getSocialGalleryImagesMap(Resource currentResource) {
  return ComponentUtil.getNestedMultiFieldProperties(currentResource, SOCIAL_GALLERY_IMAGES);
 }
 
 /**
  * Gets the social gallery expanded images map.
  * 
  * @param currentResource
  *         the current resource
  * @return the social gallery expanded images map
  */
 public List<Map<String, String>> getSocialGalleryExpandedImagesMap(Resource currentResource) {
  return ComponentUtil.getNestedMultiFieldProperties(currentResource, SOCIAL_GALLERY_EXPANDED_IMAGES);
 }
 
 /**
  * Gets the image path.
  * 
  * @param map
  *         the map
  * @return the image path
  */
 public String getImagePath(Map<String, String> map) {
  return StringUtils.defaultString(map.get(IMAGE_PATH), StringUtils.EMPTY);
 }
 
 /**
  * Gets the page path.
  * 
  * @param map
  *         the map
  * @return the page path
  */
 public String getPagePath(Map<String, String> map) {
  return StringUtils.defaultString(map.get(PAGE_PATH), StringUtils.EMPTY);
 }
 
 /**
  * Gets the new window from multifield.
  * 
  * @param map
  *         the map
  * @return the new window from multifield
  */
 public boolean getNewWindowFromMultifield(Map<String, String> map) {
  
  String newWindow = StringUtils.defaultString(map.get(OPEN_IN_NEW_WINDOW), StringUtils.EMPTY);
  String openInNewWindow = newWindow != null && ("[true]".equals(newWindow) || "[yes]".equals(newWindow)) ? BOOLEAN_TRUE : BOOLEAN_FALSE;
  return Boolean.valueOf(openInNewWindow);
 }
 
 /**
  * Gets the related cta label.
  * 
  * @param map
  *         the map
  * @return the related cta label
  */
 public String getRelatedCtaLabel(Map<String, String> map) {
  return StringUtils.defaultString(map.get(RELATED_CTA_LABEL), StringUtils.EMPTY);
 }
 
 /**
  * Gets the brand name parameter.
  * 
  * @param configurationService
  *         the configuration service
  * @param page
  *         the page
  * @return the brand name parameter
  */
 public String getBrandNameParameter(ConfigurationService configurationService, Page page) {
  return GlobalConfigurationUtility.getValueFromConfiguration(configurationService, page, UnileverConstants.SOCIAL_TAB_BASE_CONFIG_CAT,
    BRAND_NAME_PARAM);
 }
 
 /**
  * Gets the product map.
  * 
  * @param productPage
  *         the product page
  * @param resources
  * @return the product map
  */
 public Map<String, Object> getProductMap(Page productPage, Map<String, Object> resources) {
  
  Map<String, Object> productMap = new HashMap<String, Object>();
  
  Product product = CommerceHelper.findCurrentProduct(productPage);
  UNIProduct currentProduct = null;
  if ((product != null) && (product instanceof UNIProduct)) {
   currentProduct = (UNIProduct) product;
  }
  
  if (currentProduct != null) {
   Image image = null;
   
   List<ImageResource> images = currentProduct.getImages();
   if (!images.isEmpty()) {
    image = new Image(images.get(0), productPage, getSlingRequest(resources));
   }
   
   productMap.put("name", currentProduct.getName());
   if (image != null) {
    productMap.put("image", image.convertToMap());
   }
  }
  return productMap;
 }
 
 /**
  * 
  * 
  * @param properties
  *         the properties
  * @param resources
  * @param currentPage
  * @param configurationService
  * @param numberOfPosts
  * @param maxImages
  * @return the product map
  */
 public SocialTabQueryDTO getSocialAggregatorQueryDataObj(Map<String, Object> resources, Map<String, Object> properties, Page currentPage,
   ConfigurationService configurationService, int numberOfPosts, int maxImages) {
  
  SocialTabQueryDTO aggregatorQueryDTO = new SocialTabQueryDTO();
  
  String aggregatorServiceUrl = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
    UnileverConstants.SOCIAL_AGGREGATOR_SERVICE, "url");
  
  aggregatorQueryDTO.setBrandNameParameter(getBrandNameParameter(configurationService, currentPage));
  aggregatorQueryDTO.setConfigurationService(configurationService);
  aggregatorQueryDTO.setHashTags(getHashTags(getCurrentResource(resources)));
  aggregatorQueryDTO.setMaxImages(maxImages);
  aggregatorQueryDTO.setNoOfPost(numberOfPosts);
  aggregatorQueryDTO.setPage(currentPage);
  aggregatorQueryDTO.setPagePath(getResourceResolver(resources).map(aggregatorServiceUrl));
  aggregatorQueryDTO.setSocialChannels(getSocialChannels(properties));
  
  return aggregatorQueryDTO;
 }
 
 /**
  * Adds the account id to social channels map.
  * 
  * @param map
  *         the map
  * @param resources
  *         the resources
  */
 public static void addParametersToSocialChannels(Map<String, Object> map, Map<String, Object> resources) {
  
  Object[] socialChannelNamesMapArr = (Object[]) map.get("socialChannelNames");
  List<Map<String, String>> accountIdslist = getAccountIdMap(getCurrentResource(resources));
  if (socialChannelNamesMapArr.length > 0) {
   for (int i = 0; i < socialChannelNamesMapArr.length; i++) {
    Map<String, Object> socialChannelNamesMap = (Map<String, Object>) socialChannelNamesMapArr[i];
    for (Map<String, String> tempMap : accountIdslist) {
     String socialChannel = StringUtils.defaultString(tempMap.get(SOCIAL_CHANNEL), StringUtils.EMPTY);
     if (socialChannelNamesMap.get("name").toString().equalsIgnoreCase(socialChannel)) {
      socialChannelNamesMap.put(ACNT_ID, MapUtils.getString(tempMap, SOCIAL_CHANNEL_ACCOUNT_ID, StringUtils.EMPTY));
     }
    }
   }
   
  }
 }
 
 /**
  * Gets the social carousel map.
  * 
  * @param currentResource
  *         the current resource
  * @return the social carousel map
  */
 public static List<Map<String, String>> getAccountIdMap(Resource currentResource) {
  return ComponentUtil.getNestedMultiFieldProperties(currentResource, ACCOUNT_IDS_JSON);
 }
}
