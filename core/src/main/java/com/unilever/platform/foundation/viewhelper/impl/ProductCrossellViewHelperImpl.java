/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.cache.ComponentViewAndViewHelperCache;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.core.dto.SearchDTO;
import com.unilever.platform.aem.foundation.core.service.SearchParam;
import com.unilever.platform.aem.foundation.core.service.SearchService;
import com.unilever.platform.foundation.components.ProductResultComparator;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.constants.CommonConstants;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * The Class ProductCrossellViewHelperImpl.
 */
@Component(description = "ProductCrossellViewHelperImpl", immediate = true, metatype = true, label = "ProductCrossellViewHelperImpl")
@Service(value = { ProductCrossellViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.PRODUCT_CROSSELL, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class ProductCrossellViewHelperImpl extends MultipleRelatedProductsBaseViewHelperImpl {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ProductCrossellViewHelperImpl.class);
 
 /** The Constant GLOBAL_CONFIG_CAT. */
 private static final String GLOBAL_CONFIG_CAT = "productCrossell";
 
 /** The Constant CTA_LABEL_VAL. */
 private static final String CTA_LABEL_VAL = "productCrossell.ctaLabel";
 
 /** The component view and view helper cache. */
 /*
  * reference for cache object
  */
 @Reference
 ComponentViewAndViewHelperCache componentViewAndViewHelperCache;
 /*
  * reference for site configuration service
  */
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The search service. */
 @Reference
 SearchService searchService;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.impl. MultipleRelatedProductsBaseViewHelperImpl#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  LOGGER.debug("Executing ProductCrossellViewHelperImpl.java. Inside processData method");
  Page currentPage = getCurrentPage(resources);
  Map<String, String> configMap = ComponentUtil.getConfigMap(currentPage, GLOBAL_CONFIG_CAT);
  
  Map<String, Object> productCrossellMap = new HashMap<String, Object>();
  productCrossellMap = getValuesForPageType(content, currentPage, configMap, resources);
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(getJsonNameSpace(content), productCrossellMap);
  return data;
 }
 
 /**
  * Gets the values for page type.
  * 
  * @param content
  *         the content
  * @param resourceResolver
  *         the resource resolver
  * @param currentPage
  *         the current page
  * @param configMap
  *         the config map
  * @param slingRequest
  *         the sling request
  * @param resources
  *         the resources
  * @return the values for page type
  */
 private Map<String, Object> getValuesForPageType(Map<String, Object> content, Page currentPage, Map<String, String> configMap,
   Map<String, Object> resources) {
  Map<String, Object> properties = getProperties(content);
  String jsonNameSpace = MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE);
  List<Map<String, Object>> productPropertiesList = new ArrayList<Map<String, Object>>();
  
  String customizableTag = configMap.get("customizableTag");
  List<String> excludeList = getExcludeList(currentPage);
  List<String> includeList = getIncludeList(currentPage);
  String featureTag = getFeatureTag(currentPage);
  String[] featuredTags = StringUtils.isNotBlank(featureTag) ? featureTag.split(CommonConstants.COMMA_CHAR) : new String[] {};
  String includeSegmentRulesProp = MapUtils.getString(properties, "includeSegmentRules", StringUtils.EMPTY);
  boolean includeSegmentRules = ("true").equalsIgnoreCase(includeSegmentRulesProp) ? true : false;
  featuredTags = ComponentUtil.getUpdatedIncludedArrayFromResources(resources, featuredTags, includeSegmentRules);
  int max = getMax(configMap);
  
  if (!includeList.isEmpty() && includeList != null) {
   List<String> crossellPagesPathList = getFilteredProductSet(includeList, excludeList, resources);
   
   Collections.sort(crossellPagesPathList, new ProductResultComparator(featuredTags, customizableTag, currentPage.getContentResource()
     .getResourceResolver()));
   
   List<String> crossellPagesPathListFinal = crossellPagesPathList.size() > max ? crossellPagesPathList.subList(0, max) : crossellPagesPathList;
   
   productPropertiesList = ProductHelper.getProductListWithTag(crossellPagesPathListFinal, resources, jsonNameSpace,
     getI18n(resources).get(CTA_LABEL_VAL));
  }
  
  Map<String, Object> dataManual = new LinkedHashMap<String, Object>();
  dataManual.put(UnileverConstants.TITLE, MapUtils.getString(properties, UnileverConstants.TITLE));
  dataManual.put(ProductConstants.PRODUCT_LIST_MAP, productPropertiesList.toArray());
  return dataManual;
 }
 
 /**
  * Gets the include list.
  * 
  * @param page
  *         the page
  * @return the include list
  */
 private List<String> getIncludeList(Page page) {
  
  List<String> includeList = new ArrayList<String>();
  try {
   String val = configurationService.getConfigValue(page, GLOBAL_CONFIG_CAT, UnileverConstants.INCLUDE_LIST);
   if (StringUtils.isNotBlank(val)) {
    String[] valArr = val.split(UnileverConstants.COMMA);
    for (String string : valArr) {
     includeList.add(string != null && string.isEmpty() ? "" : string);
    }
   }
  } catch (ConfigurationNotFoundException e) {
   LOGGER.debug("could not find includeList key in productCrossell category", e);
  }
  
  LOGGER.debug("Namespaces for includeList= " + includeList);
  return includeList;
 }
 
 /**
  * Gets the exclude list.
  * 
  * @param page
  *         the page
  * @return the exclude list
  */
 private List<String> getExcludeList(Page page) {
  
  List<String> namespaceForAndCondList = new ArrayList<String>();
  try {
   String val = configurationService.getConfigValue(page, GLOBAL_CONFIG_CAT, UnileverConstants.EXCLUDE_LIST);
   String[] valArr = val.split(UnileverConstants.COMMA);
   if (valArr != null) {
    for (String string : valArr) {
     namespaceForAndCondList.add(string != null && string.isEmpty() ? "" : string);
    }
   }
   
  } catch (ConfigurationNotFoundException e) {
   LOGGER.debug("could not find ExcludeList key in productListing category", e);
  }
  
  LOGGER.debug("Namespaces for And ExcludeList Condition List   = " + namespaceForAndCondList);
  return namespaceForAndCondList;
 }
 
 /**
  * Gets the filtered product set.
  * 
  * @param includeList
  *         the include list
  * @param excludeList
  *         the exclude list
  * @param currentPage
  *         the current page
  * @return the filtered product set
  */
 private List<String> getFilteredProductSet(List<String> includeList, List<String> excludeList, Map<String, Object> resources) {
  List<String> taggedPagesList = new ArrayList<String>();
  List<String> negateTag = new ArrayList<String>();
  Page currentPage = getCurrentPage(resources);
  String currentPagePath = currentPage.getPath();
  Map<String, List<String>> propMap = new HashMap<String, List<String>>();
  List<String> propValueList = new ArrayList<String>();
  propValueList.add(UnileverConstants.PRODUCT);
  propMap.put(UnileverConstants.JCR_CONTENT_CONTENT_TYPE, propValueList);
  int limit = getLimit(currentPage);
  List<SearchDTO> searchResult = searchService.getCreteriaPagesList(includeList, excludeList, negateTag, currentPagePath, propMap, new SearchParam(0,
    limit, UnileverConstants.DESCENDING), getResourceResolver(resources));
  for (SearchDTO searchDTO : searchResult) {
   taggedPagesList.add(searchDTO.getResultNodePath());
  }
  LOGGER.debug("Valid Pages Path List  = " + taggedPagesList.size());
  
  return taggedPagesList;
 }
 
 /**
  * Gets the max.
  * 
  * @param configMap
  *         the config map
  * @return the max
  */
 private int getMax(Map<String, String> configMap) {
  int max = CommonConstants.FOUR;
  try {
   max = StringUtils.isBlank(configMap.get(UnileverConstants.MAX)) ? CommonConstants.FOUR : Integer.parseInt(configMap.get(UnileverConstants.MAX));
  } catch (NumberFormatException nfe) {
   LOGGER.error("Maximum article for page list must be number", nfe);
  }
  return max;
 }
 
 /**
  * Gets the limit.
  * 
  * @param page
  *         the page
  * @return the limit
  */
 private int getLimit(Page page) {
  int limit = CommonConstants.FIFTY;
  try {
   String val = configurationService.getConfigValue(page, GLOBAL_CONFIG_CAT, "limit");
   limit = StringUtils.isNotBlank(val) ? Integer.parseInt(val) : limit;
  } catch (ConfigurationNotFoundException e) {
   LOGGER.debug("could not find limit key in productListing category", e);
  }
  return limit;
 }
 
 /**
  * Gets the feature tag.
  * 
  * @param page
  *         the page
  * @return the feature tag
  */
 private String getFeatureTag(Page page) {
  try {
   String featureTag = StringUtils.EMPTY;
   featureTag = configurationService.getConfigValue(page, GLOBAL_CONFIG_CAT, "featureTag");
   return featureTag;
  } catch (ConfigurationNotFoundException e) {
   LOGGER.debug("could not find featureTag key in " + GLOBAL_CONFIG_CAT + "category", e);
   
   return null;
  }
 }
 
 /**
  * Gets the config service.
  * 
  * @return configurationService
  */
 protected ConfigurationService getConfigService() {
  
  return this.configurationService;
 }
 
 /**
  * Gets the search service.
  * 
  * @return searchService
  */
 protected SearchService getSearchService() {
  
  return this.searchService;
 }
 
}
