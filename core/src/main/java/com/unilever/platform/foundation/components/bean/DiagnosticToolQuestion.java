/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.components.bean;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;

import com.unilever.platform.foundation.components.helper.ComponentUtil;

/**
 * The Class DiagnosticToolQuestion.
 */
public class DiagnosticToolQuestion {
 
 /** The id. */
 private String id;
 
 /** The type. */
 private String type;
 
 /** The heading text. */
 private String headingText;
 
 /** The is tag not required. */
 private boolean isTagNotRequired;
 
 /** The answer map. */
 private Map<String, Answer> allAnswerMap;
 
 /** The selected answer list. */
 private List<Answer> selectedAnswerList;
 
 /**
  * Instantiates a new question.
  * 
  * @param diagnosticToolQuestion
  *         the diagnostic tool question
  */
 public DiagnosticToolQuestion(Resource diagnosticToolQuestion) {
  ValueMap properties = diagnosticToolQuestion.getValueMap();
  this.id = MapUtils.getString(properties, "questionId", "");
  this.headingText = MapUtils.getString(properties, "headingText", "");
  this.type = MapUtils.getString(properties, "questionType", "");
  this.isTagNotRequired = MapUtils.getBooleanValue(properties, "tagNotRequired", false);
  List<Map<String, String>> list = ComponentUtil.getNestedMultiFieldProperties(diagnosticToolQuestion, "responseOptionJson");
  Iterator<Map<String, String>> itr = list.iterator();
  Map<String, Answer> answerMap = new HashMap<String, Answer>();
  while (itr.hasNext()) {
   Map<String, String> responseMap = itr.next();
   Answer answer = new Answer(responseMap);
   answerMap.put(answer.getId(), answer);
  }
  this.allAnswerMap = answerMap;
 }
 
 /**
  * Gets the id.
  * 
  * @return the id
  */
 public String getId() {
  return id;
 }
 
 /**
  * Sets the id.
  * 
  * @param id
  *         the new id
  */
 public void setId(String id) {
  this.id = id;
 }
 
 /**
  * Gets the type.
  * 
  * @return the type
  */
 public String getType() {
  return type;
 }
 
 /**
  * Sets the type.
  * 
  * @param type
  *         the new type
  */
 public void setType(String type) {
  this.type = type;
 }
 
 /**
  * Gets the heading text.
  * 
  * @return the heading text
  */
 public String getHeadingText() {
  return headingText;
 }
 
 /**
  * Sets the heading text.
  * 
  * @param headingText
  *         the new heading text
  */
 public void setHeadingText(String headingText) {
  this.headingText = headingText;
 }
 
 /**
  * Gets the all answer map.
  * 
  * @return the all answer map
  */
 public Map<String, Answer> getAllAnswerMap() {
  return allAnswerMap;
 }
 
 /**
  * Sets the all answer map.
  * 
  * @param allAnswerMap
  *         the all answer map
  */
 public void setAllAnswerMap(Map<String, Answer> allAnswerMap) {
  this.allAnswerMap = allAnswerMap;
 }
 
 /**
  * Gets the selected answer list.
  * 
  * @return the selected answer list
  */
 public List<Answer> getSelectedAnswerList() {
  return selectedAnswerList;
 }
 
 /**
  * Sets the selected answer list.
  * 
  * @param selectedAnswerList
  *         the new selected answer list
  */
 public void setSelectedAnswerList(List<Answer> selectedAnswerList) {
  this.selectedAnswerList = selectedAnswerList;
 }
 
 /**
  * Checks if is tag not required.
  * 
  * @return true, if is tag not required
  */
 public boolean isTagNotRequired() {
  return isTagNotRequired;
 }
 
 /**
  * Sets the tag not required.
  * 
  * @param isTagNotRequired
  *         the new tag not required
  */
 public void setTagNotRequired(boolean isTagNotRequired) {
  this.isTagNotRequired = isTagNotRequired;
 }
 
 /**
  * The Class Answer.
  */
 public class Answer {
  
  /** The id. */
  private String id;
  
  /** The response text. */
  private String responseText;
  
  /** The tag id list. */
  private String[] tagIds;
  
  /**
   * Instantiates a new answer.
   * 
   * @param answerMap
   *         the answer map
   */
  public Answer(Map<String, String> answerMap) {
   this.id = MapUtils.getString(answerMap, "responseId", "");
   this.responseText = MapUtils.getString(answerMap, "responseText", "");
   this.tagIds = StringUtils.split(MapUtils.getString(answerMap, "matchingTag", ""), ",");
  }
  
  /**
   * Gets the id.
   * 
   * @return the id
   */
  public String getId() {
   return id;
  }
  
  /**
   * Sets the id.
   * 
   * @param id
   *         the new id
   */
  public void setId(String id) {
   this.id = id;
  }
  
  /**
   * Gets the response text.
   * 
   * @return the response text
   */
  public String getResponseText() {
   return responseText;
  }
  
  /**
   * Sets the response text.
   * 
   * @param responseText
   *         the new response text
   */
  public void setResponseText(String responseText) {
   this.responseText = responseText;
  }
  
  /**
   * Gets the tag ids.
   * 
   * @return the tag ids
   */
  public String[] getTagIds() {
   return tagIds;
  }
  
  /**
   * Sets the tag ids.
   * 
   * @param tagIds
   *         the new tag ids
   */
  public void setTagIds(String[] tagIds) {
   this.tagIds = tagIds;
  }
  
 }
 
}
