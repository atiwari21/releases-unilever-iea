/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.template.dto;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The Class Award.
 */
public class Award implements Comparator<Award>{
 
 /** The tag id. */
 private String tagId;
 
 /** The image. */
 private Image image;
 
 /** The type. */
 private String type;
 
 /** The name. */
 private String name;
 
 /** The url. */
 private String url;
 
 /** The start date. */
 private String startDate;
 
 /** The end date. */
 private String endDate;
 
 /** The description. */
 private String description;
 
 /** The alt text. */
 private String altText;
 
 /** The cta title. */
 private String ctaTitle;
 
 /** The open in new window. */
 private boolean openInNewWindow;
 
 /** The award year. */
 private String awardYear;
 
 /** The associated tags to the award. */
 private List<Map<String, String>> associatedTags;
 
 /** The associated products to the award. */
 private List<Map<String, String>> associatedProducts;
 
 /**
  * Gets the image.
  * 
  * @return the image
  */
 public Image getImage() {
  return image;
 }
 
 /**
  * Sets the image.
  * 
  * @param image2
  *         the new image
  */
 public void setImage(Image image2) {
  this.image = image2;
 }
 
 /**
  * Gets the type.
  * 
  * @return the type
  */
 public String getType() {
  return type;
 }
 
 /**
  * Sets the type.
  * 
  * @param type
  *         the new type
  */
 public void setType(String type) {
  this.type = type;
 }
 
 /**
  * Gets the name.
  * 
  * @return the name
  */
 public String getName() {
  return name;
 }
 
 /**
  * Sets the name.
  * 
  * @param name
  *         the new name
  */
 public void setName(String name) {
  this.name = name;
 }
 
 /**
  * Gets the url.
  * 
  * @return the url
  */
 public String getUrl() {
  return url;
 }
 
 /**
  * Sets the url.
  * 
  * @param url
  *         the new url
  */
 public void setUrl(String url) {
  this.url = url;
 }
 
 /**
  * Gets the start date.
  * 
  * @return the start date
  */
 public String getStartDate() {
  return startDate;
 }
 
 /**
  * Sets the start date.
  * 
  * @param parseStartDate
  *         the new start date
  */
 public void setStartDate(String parseStartDate) {
  this.startDate = parseStartDate;
 }
 
 /**
  * Gets the end date.
  * 
  * @return the end date
  */
 public String getEndDate() {
  return endDate;
 }
 
 /**
  * Sets the end date.
  * 
  * @param parseEndDate
  *         the new end date
  */
 public void setEndDate(String parseEndDate) {
  this.endDate = parseEndDate;
 }
 
 /**
  * Gets the tag id.
  * 
  * @return the tag id
  */
 public String getTagId() {
  return tagId;
 }
 
 /**
  * Sets the tag id.
  * 
  * @param tagId
  *         the new tag id
  */
 public void setTagId(String tagId) {
  this.tagId = tagId;
 }
 
 /**
  * Gets the description.
  * 
  * @return the description
  */
 public String getDescription() {
  return description;
 }
 
 /**
  * Sets the description.
  * 
  * @param description
  *         the new description
  */
 public void setDescription(String description) {
  this.description = description;
 }
 
 /**
  * Gets the alt text.
  * 
  * @return the alt text
  */
 public String getAltText() {
  return altText;
 }
 
 /**
  * Sets the alt text.
  * 
  * @param altText
  *         the new alt text
  */
 public void setAltText(String altText) {
  this.altText = altText;
 }
 
 /**
  * Gets the cta title.
  * 
  * @return the cta title
  */
 public String getCtaTitle() {
  return ctaTitle;
 }
 
 /**
  * Sets the cta title.
  * 
  * @param ctaTitle
  *         the new cta title
  */
 public void setCtaTitle(String ctaTitle) {
  this.ctaTitle = ctaTitle;
 }
 
 /**
  * Checks if is open in new window.
  * 
  * @return true, if is open in new window
  */
 public boolean isOpenInNewWindow() {
  return openInNewWindow;
 }
 
 /**
  * Sets the open in new window.
  * 
  * @param openInNewWindow
  *         the new open in new window
  */
 public void setOpenInNewWindow(boolean openInNewWindow) {
  this.openInNewWindow = openInNewWindow;
 }
 
 /**
  * Conver to map.
  * 
  * @return the map
  */
 public Map<String, Object> converToMap() {
  Map<String, Object> map = new HashMap<String, Object>();
  map.put("tagId", tagId);
  map.put("image", image.convertToMap());
  map.put("type", type);
  map.put("name", name);
  map.put("url", url);
  map.put("startDate", startDate);
  map.put("endDate", endDate);
  map.put("description", description);
  map.put("altText", altText);
  map.put("ctaTitle", ctaTitle);
  map.put("openInNewWindow", openInNewWindow);
  map.put("awardYear", awardYear);
  map.put("associatedTags", associatedTags);
  map.put("associatedProducts", associatedProducts);
  return map;
  
 }

 public String getAwardYear() {
  return awardYear;
 }

 public void setAwardYear(String awardYear) {
  this.awardYear = awardYear;
 }

 public List<Map<String, String>> getAssociatedTags() {
  return associatedTags;
 }

 public void setAssociatedTags(List<Map<String, String>> associatedTags) {
  this.associatedTags = associatedTags;
 }

 public List<Map<String, String>> getAssociatedProducts() {
  return associatedProducts;
 }

 public void setAssociatedProducts(List<Map<String, String>> associatedProducts) {
  this.associatedProducts = associatedProducts;
 }

 @Override
 public int compare(Award o1, Award o2) {
  return o1.getName().compareTo(o2.getName());
 }
 
}
