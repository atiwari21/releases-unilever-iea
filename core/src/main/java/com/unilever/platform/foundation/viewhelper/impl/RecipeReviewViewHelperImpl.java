/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.aem.foundation.integration.UrlResolverUtility;
import com.unilever.platform.foundation.components.BazaarVoiceSEO;
import com.unilever.platform.foundation.components.KritiqueSEO;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * The Class RecipeReviewViewHelperImpl.
 */
@Component(description = "RecipeReviewViewHelperImpl", immediate = true, metatype = true, label = "Recipe Review")
@Service(value = { RecipeReviewViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.RECIPE_REVIEW, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class RecipeReviewViewHelperImpl extends BaseViewHelper {
 
 @Reference
 ConfigurationService configurationService;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(RecipeReviewViewHelperImpl.class);
 @Reference
 KritiqueSEO kqseoService;

 @Reference
 BazaarVoiceSEO bvseoService;
 private static final String RECIPE_RATING_REVIEWS = "recipeRatingAndReviews";
 private static final String RATING_REVIEWS = "ratingReviews";
 private static final String SEO = "SEO";
 private static final String BAZAARVOICE = "bazaarvoice";
 private static final String KRITIQUE = "kritique";
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  
  LOGGER.info("Recipe Review View Helper #processData called.");
  
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  Map<String, Object> recipesMap = new LinkedHashMap<String, Object>();
  String jsonNameSpace = getJsonNameSpace(content);
  
  Page currentPage = getCurrentPage(resources);
  ResourceResolver resolver = getResourceResolver(resources);
  
  SlingHttpServletRequest request = getSlingRequest(resources);
  ValueMap properties = currentPage.getProperties();
  
  String pagePath = UrlResolverUtility.getResolvedPagePath(configurationService, currentPage);
  String parentPagePath = UrlResolverUtility.getResolvedPagePath(configurationService, currentPage.getParent());
  
  String recipeId = properties.containsKey(UnileverConstants.RECIPE_ID) ? properties.get(UnileverConstants.RECIPE_ID).toString() : StringUtils.EMPTY;
  
  String pageUrl = ComponentUtil.getFullURL(resolver, pagePath, request);
  String categoryPageUrl = ComponentUtil.getFullURL(resolver, parentPagePath, request);
  
  String templateName = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, ProductConstants.KQ_VIEW_TYPE,
    "recipeReview");
  
  recipesMap.put("entityType", "recipe");
  recipesMap.put("entityUrl", pageUrl);
  recipesMap.put("categoryUrl", categoryPageUrl);
  recipesMap.put("templateName", "reviewCount");
  recipesMap.put("uniqueId", recipeId);
  recipesMap.put("viewType", templateName);
  
  Map<String, Object> review = new LinkedHashMap<String, Object>();
  
  review.putAll(ProductHelper.getReviewMapProperties(configurationService, currentPage, RECIPE_RATING_REVIEWS, jsonNameSpace));
  review.put("entityType", "recipe");
  recipesMap.put("review", review);
  recipesMap.put(ProductConstants.PRODUCT_RATINGS_MAP, getProductRatingsMap(recipeId, templateName));
  boolean isEnabled = MapUtils.getBoolean(review, ProductConstants.ENABLED,false);
  boolean seoEnabled = Boolean.parseBoolean(GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
    RECIPE_RATING_REVIEWS, ProductConstants.SEO_ENABLED));
  String serviceProvidername = StringUtils.EMPTY;
  Resource resource = currentPage.getContentResource();
  InheritanceValueMap valueMap = new HierarchyNodeInheritanceValueMap(resource);
  if (valueMap != null) {
   serviceProvidername = (String) valueMap.getInherited(RATING_REVIEWS, StringUtils.EMPTY);
  }
  if(StringUtils.isBlank(serviceProvidername)) {
   serviceProvidername = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
     RECIPE_RATING_REVIEWS, ProductConstants.SERVICE_PROVIDER_NAME);
  }
  if(isEnabled && seoEnabled){
   if (BAZAARVOICE.equalsIgnoreCase(serviceProvidername) || serviceProvidername.startsWith(BAZAARVOICE)) {
    recipesMap.put(SEO, bvseoService.getSEOWithSDK(currentPage, configurationService, resources));
   } else if (KRITIQUE.equalsIgnoreCase(serviceProvidername) || serviceProvidername.startsWith(KRITIQUE)) {
    recipesMap.put(SEO, kqseoService.getSEOWithSDK(getSlingRequest(resources), currentPage, configurationService));
   }
  }
  data.put(jsonNameSpace, recipesMap);
  return data;
 }
 
 /**
  * Gets the product ratings map.
  *
  * @param recipeId
  *         the recipe id
  * @param viewType
  *         the view type
  * @return the product ratings map
  */
 private static Map<String, Object> getProductRatingsMap(String recipeId, String viewType) {
  Map<String, Object> details = new HashMap<String, Object>();
  
  details.put(ProductConstants.RATING_UNIQUE_ID, recipeId);
  details.put(ProductConstants.RATING_IDENTIFIER_TYPE, StringUtils.EMPTY);
  details.put(ProductConstants.RATING_IDENTIFIER_VALUE, recipeId);
  details.put(ProductConstants.RATING_ENTITY_TYPE, ProductConstants.PRODUCT_MAP);
  details.put(ProductConstants.RATING_VIEW_TYPE, viewType);
  
  LOGGER.debug("Recipe Ratings Map Properties : " + details);
  return details;
 }
}
