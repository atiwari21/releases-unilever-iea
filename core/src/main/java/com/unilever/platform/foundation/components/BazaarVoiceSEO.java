/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.components;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.api.Product;
import com.adobe.cq.commerce.common.CommerceHelper;
import com.bazaarvoice.seo.sdk.BVManagedUIContent;
import com.bazaarvoice.seo.sdk.BVUIContent;
import com.bazaarvoice.seo.sdk.config.BVClientConfig;
import com.bazaarvoice.seo.sdk.config.BVConfiguration;
import com.bazaarvoice.seo.sdk.config.BVSdkConfiguration;
import com.bazaarvoice.seo.sdk.model.BVParameters;
import com.bazaarvoice.seo.sdk.model.ContentType;
import com.bazaarvoice.seo.sdk.model.SubjectType;
import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfiguration;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * The Class BazaarVoiceSEO.
 */
/*
 * This class is used to implement SEO functionality.
 */
@Component(immediate = true, metatype = true)
@Service(value = BazaarVoiceSEO.class)
public class BazaarVoiceSEO {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(BazaarVoiceSEO.class);
 
 private static final String BV_KEY = "bazaarvoice";
 
 /**
  * Gets the SEO with sdk.
  * 
  * @param product
  *         the product
  * @param page
  *         the page
  * @param configurationService
  *         the configuration service
  * @param resources
  *         the resources
  * @return the SEO with sdk
  */
 public String getSEOWithSDK(Page page, ConfigurationService configurationService, Map<String, Object> resources) {
  UNIProduct product = null;
  String sBvOutputReviews = "";
  BVConfiguration bvConfig = new BVSdkConfiguration();
  String pageURL = null;
  String subjectID = "";
  GlobalConfiguration globalConfiguration = page.adaptTo(GlobalConfiguration.class);
  String proxyHost = globalConfiguration.getConfigValue("proxy_host");
  String proxyPort = globalConfiguration.getConfigValue("proxy_port");
  Product pageProduct = CommerceHelper.findCurrentProduct(page);
  if (pageProduct instanceof UNIProduct) {
   product = (UNIProduct) pageProduct;
  }

  if (page != null) {
   ValueMap vMap = page.getProperties();
   String recipeId = vMap.containsKey(UnileverConstants.RECIPE_ID) ? vMap.get(UnileverConstants.RECIPE_ID).toString() : StringUtils.EMPTY;
   String executionTimeOut = getProperteryFromConfig(page, configurationService, BV_KEY, "executionTimeOut");
   String executionTimeOutBot = getProperteryFromConfig(page, configurationService, BV_KEY, "executionTimeOutBot");
   if (StringUtils.isBlank(executionTimeOut)) {
    executionTimeOut = "1000";
   }
   if (StringUtils.isBlank(executionTimeOutBot)) {
    executionTimeOutBot = "1000";
   }
   bvConfig.addProperty(BVClientConfig.SEO_SDK_ENABLED, getProperteryFromConfig(page, configurationService, BV_KEY, "seo_enabled"));
   bvConfig.addProperty(BVClientConfig.EXECUTION_TIMEOUT, executionTimeOut);
   bvConfig.addProperty(BVClientConfig.EXECUTION_TIMEOUT_BOT, executionTimeOutBot);
   bvConfig.addProperty(BVClientConfig.CLOUD_KEY, getProperteryFromConfig(page, configurationService, BV_KEY, "cloudkey"));
   bvConfig.addProperty(BVClientConfig.BV_ROOT_FOLDER, getProperteryFromConfig(page, configurationService, BV_KEY, "deploymentzone")); // adjust
   bvConfig.addProperty(BVClientConfig.STAGING, getProperteryFromConfig(page, configurationService, BV_KEY, "staging"));
   if (StringUtils.isNotEmpty(proxyHost) && StringUtils.isNotEmpty(proxyPort)) {
    bvConfig.addProperty(BVClientConfig.PROXY_HOST, proxyHost);
    bvConfig.addProperty(BVClientConfig.PROXY_PORT, proxyPort);
    
   }
   
   // Prepare SubjectID/ProductID/RecipeID values.
   if(product != null){
    subjectID = product.getSKU();
   } else if(StringUtils.isNotEmpty(recipeId)){
    subjectID = recipeId;
   }
  }
  // Set BV Parameters that are specific to the page and content type.
  BVParameters bvParam = new BVParameters();
  SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) resources.get(ProductConstants.REQUEST);
  pageURL = slingRequest.getQueryString() == null ? slingRequest.getRequestURI() : slingRequest.getRequestURI() + "?" + slingRequest.getQueryString();
  bvParam.setUserAgent(slingRequest.getHeader("User-Agent"));
  pageURL = ComponentUtil.getFullURL((ResourceResolver) resources.get(ProductConstants.RESOURCE_RESOLVER), pageURL);
  // this value is used to build pagination links
  bvParam.setBaseURI(pageURL);
  // this value is used to extract the page number from bv URL parameters
  bvParam.setPageURI(pageURL);
  bvParam.setContentType(ContentType.REVIEWS);
  bvParam.setSubjectType(SubjectType.PRODUCT);
  bvParam.setPageNumber("1");
  bvParam.setSubjectId(subjectID);
  
  BVUIContent bvOutput = new BVManagedUIContent(bvConfig);
  // Get content and place into strings, which output into the injection divs.
  // getAggregateRating delivers the AggregateRating section only
  String sBvOutputSummary = bvOutput.getAggregateRating(bvParam);
  // getReviews delivers the review content with pagination only%>
  sBvOutputReviews = bvOutput.getReviews(bvParam);
  sBvOutputReviews = sBvOutputReviews.replaceAll("(?s)<script.*?>.*?</script>", "");
  sBvOutputReviews = sBvOutputReviews.replaceAll("<script.*?>", "<noscript>");
  sBvOutputReviews = sBvOutputReviews.replaceAll("</script>", "</noscript>");
  String source = "<div id=\"bvseo-reviewsSection\">";
  String target = "<div id=\"bvseo-reviewsSection\" style=\"display: none\">";
  sBvOutputReviews = sBvOutputReviews.replaceAll(source, target);
  StringBuilder stringBuilder = new StringBuilder();
  stringBuilder.append("<div style=\"display: none\">");
  stringBuilder.append(sBvOutputReviews);
  stringBuilder.append(sBvOutputSummary);
  stringBuilder.append("</div>");
  
  return stringBuilder.toString();
 }
 
 /**
  * Gets the propertery from config.
  * 
  * @param page
  *         the page
  * @param configurationService
  *         the configuration service
  * @param configCategory
  *         the config category
  * @param configKey
  *         the config key
  * @return the propertery from config
  */
 private static String getProperteryFromConfig(Page page, ConfigurationService configurationService, String configCategory, String configKey) {
  
  String value = StringUtils.EMPTY;
  try {
   value = configurationService.getConfigValue(page, configCategory, configKey);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error(configKey + "not found in global configurations", e);
  }
  return value;
 }
 
}
