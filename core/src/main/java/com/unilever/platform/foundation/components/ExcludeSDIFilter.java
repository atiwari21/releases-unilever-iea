/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.components;

import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferenceCardinality;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.apache.sling.engine.EngineConstants;
import org.osgi.framework.Constants;
import org.osgi.service.component.ComponentContext;

import com.cognifide.cq.includefilter.Configuration;

/**
 * The Class ExcludeSDIFilter.
 */
@Component(immediate = true, description = "Filter to add SDI header.", label = "Unilever SDI Exclude Filter", enabled = true, metatype = true)
@Service(Filter.class)
@Properties({
  @Property(name = EngineConstants.SLING_FILTER_SCOPE, value = { EngineConstants.FILTER_SCOPE_REQUEST, EngineConstants.FILTER_SCOPE_INCLUDE }, propertyPrivate = true),
  @Property(name = Constants.SERVICE_VENDOR, value = "unilever"), @Property(name = "filter.order", intValue = -601, propertyPrivate = true),
  @Property(name = "isEnabled", boolValue = true) })
public class ExcludeSDIFilter implements Filter {
 
 private static final String INCLUDE_SEGMENT_RULES = "includeSegmentRules";
 private static final String ENABLED = "isEnabled";
 private boolean isEnabled;
 @Reference(referenceInterface = Configuration.class, cardinality = ReferenceCardinality.OPTIONAL_MULTIPLE, policy = ReferencePolicy.DYNAMIC)
 private Set<Configuration> configs = new CopyOnWriteArraySet<Configuration>();
 
 @Activate
 protected void activate(ComponentContext context, Map<String, ?> properties) {
  this.isEnabled = PropertiesUtil.toBoolean(properties.get(ENABLED), false);
 }
 
 @Override
 public void init(FilterConfig filterConfig) throws ServletException {
  //empty method
 }
 
 @Override
 public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
  if (((!(request instanceof SlingHttpServletRequest)) || (!(response instanceof SlingHttpServletResponse))) || !isEnabled) {
   chain.doFilter(request, response);
   return;
  }
  SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) request;
  for (Configuration c : this.configs) {
   if (isEnabled(c, slingRequest)) {
    Resource resource = slingRequest.getResource();
    if (resource != null) {
     ValueMap map = resource.adaptTo(ValueMap.class);
     if (!MapUtils.getBooleanValue(map, INCLUDE_SEGMENT_RULES) && isEnabled) {
      String header = c.getRequiredHeader();
      if (StringUtils.isNotBlank(header)) {
       String name = header;
       if (StringUtils.contains(header, "=")) {
        name = header.split("=")[0];
       }
       AddHeaderSlingRequest addHeaderSlingRequest = new AddHeaderSlingRequest(slingRequest);
       addHeaderSlingRequest.addHeader(name, INCLUDE_SEGMENT_RULES);
       chain.doFilter(addHeaderSlingRequest, response);
      }
     }
    }
   }
  }
  chain.doFilter(request, response);
 }
 
 private boolean isEnabled(Configuration config, SlingHttpServletRequest request) {
  String requestPath = request.getRequestPathInfo().getResourcePath();
  boolean enabled = false;
  Resource resource = request.getResource();
  if (resource != null) {
   enabled = config.isSupportedResourceType(resource.getResourceType(), request);
   enabled = enabled && (!ResourceUtil.isSyntheticResource(resource));
   enabled = enabled && (StringUtils.equals(config.getIncludeTypeName(), "Javascript"));
   
  }
  return (config.isEnabled()) && (StringUtils.startsWith(requestPath, config.getBasePath()) && enabled);
 }
 
 @Override
 public void destroy() {
  //Empty method.
 }
 
 protected void bindConfigs(Configuration config) {
  this.configs.add(config);
 }
 
 protected void unbindConfigs(Configuration config) {
  this.configs.remove(config);
 }
 
}
