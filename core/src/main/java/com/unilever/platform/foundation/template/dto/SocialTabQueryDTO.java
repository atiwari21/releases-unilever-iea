/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.template.dto;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;

/**
 * The Class SocialTabQueryDTO.
 */
public class SocialTabQueryDTO {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(SocialTabQueryDTO.class);
 
 /** The hash tags. */
 private List<Map<String, String>> hashTags;
 
 /** The social channels. */
 private List<String> socialChannels;
 
 /** The no of post. */
 private int noOfPost;
 
 /** The page. */
 private Page page;
 
 /** The page path. */
 private String pagePath;
 
 /** The brand name parameter. */
 private String brandNameParameter;
 
 /** The max images. */
 private int maxImages;
 
 /** The configuration service. */
 private ConfigurationService configurationService;
 
 /**
  * Instantiates a new social tab query DTO.
  */
 public SocialTabQueryDTO() {
  LOGGER.debug("Social Tab Query Dto Constructor Called.");
 }
 
 /**
  * Gets the hash tags.
  * 
  * @return the hash tags
  */
 public List<Map<String, String>> getHashTags() {
  return hashTags;
 }
 
 /**
  * Sets the hash tags.
  * 
  * @param hashTags
  *         the hash tags
  */
 public void setHashTags(List<Map<String, String>> hashTags) {
  this.hashTags = hashTags;
 }
 
 /**
  * Gets the social channels.
  * 
  * @return the social channels
  */
 public List<String> getSocialChannels() {
  return socialChannels;
 }
 
 /**
  * Sets the social channels.
  * 
  * @param socialChannels
  *         the new social channels
  */
 public void setSocialChannels(List<String> socialChannels) {
  this.socialChannels = socialChannels;
 }
 
 /**
  * Gets the no of post.
  * 
  * @return the no of post
  */
 public int getNoOfPost() {
  return noOfPost;
 }
 
 /**
  * Sets the no of post.
  * 
  * @param noOfPost
  *         the new no of post
  */
 public void setNoOfPost(int noOfPost) {
  this.noOfPost = noOfPost;
 }
 
 /**
  * Gets the page.
  * 
  * @return the page
  */
 public Page getPage() {
  return page;
 }
 
 /**
  * Sets the page.
  * 
  * @param page
  *         the new page
  */
 public void setPage(Page page) {
  this.page = page;
 }
 
 /**
  * Gets the brand name parameter.
  * 
  * @return the brand name parameter
  */
 public String getBrandNameParameter() {
  return brandNameParameter;
 }
 
 /**
  * Sets the brand name parameter.
  * 
  * @param brandNameParameter
  *         the new brand name parameter
  */
 public void setBrandNameParameter(String brandNameParameter) {
  this.brandNameParameter = brandNameParameter;
 }
 
 /**
  * Gets the max images.
  * 
  * @return the max images
  */
 public int getMaxImages() {
  return maxImages;
 }
 
 /**
  * Sets the max images.
  * 
  * @param maxImages
  *         the new max images
  */
 public void setMaxImages(int maxImages) {
  this.maxImages = maxImages;
 }
 
 /**
  * Gets the page path.
  * 
  * @return the page path
  */
 public String getPagePath() {
  return pagePath;
 }
 
 /**
  * Sets the page path.
  * 
  * @param pagePath
  *         the new page path
  */
 public void setPagePath(String pagePath) {
  this.pagePath = pagePath;
 }
 
 /**
  * Gets the configuration service.
  * 
  * @return the configuration service
  */
 public ConfigurationService getConfigurationService() {
  return configurationService;
 }
 
 /**
  * Sets the configuration service.
  * 
  * @param configurationService
  *         the new configuration service
  */
 public void setConfigurationService(ConfigurationService configurationService) {
  this.configurationService = configurationService;
 }
}
