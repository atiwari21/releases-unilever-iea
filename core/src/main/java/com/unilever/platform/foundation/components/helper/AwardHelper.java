/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.components.helper;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagConstants;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.aem.foundation.dam.DAMAssetUtility;
import com.unilever.platform.foundation.template.dto.Award;
import com.unilever.platform.foundation.template.dto.Image;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * The Class AwardHelper.
 */
public final class AwardHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(AwardHelper.class);
 
 /** The Constant AWARD. */
 public static final String AWARD = "awardConfig";
 
 /** The Constant URL. */
 public static final String URL = "url";
 
 /** The associated awards with product. */
 private static Map<String, Set<Award>> associatedAwardsMap = new HashMap<String, Set<Award>>();
 
 /** The Constant RESOURCE_TYPE. */
 public static final String RESOURCE_TYPE = "sling:resourceType";
 
 /** The Constant RESOURCE_TYPE_VALUE. */
 public static final String RESOURCE_TYPE_VALUE = "/apps/unilever-iea/components/product";
 
 /** The Constant SKU. */
 public static final String SKU = "sku";
 
 /** The Constant IDENTIFIER_TYPE. */
 public static final String IDENTIFIER_TYPE = "identifierType";
 
 /** The Constant NAME. */
 public static final String NAME = "name";
 
 /** The Constant PRODUCT_PATH. */
 public static final String PRODUCT_PATH = "productPath";
 
 /**
  * Instantiates a new award helper.
  */
 private AwardHelper() {
  
 }
 
 /**
  * Gets the product awards.
  * 
  * @param product
  *         the product
  * @param page
  *         the page
  * @param configurationService
  *         the configuration service
  * @param resources
  *         the resources
  * @return the product awards
  */
 public static List<Map<String, Object>> getProductAwards(UNIProduct product, Page page, ConfigurationService configurationService,
   Map<String, Object> resources) {
  List<Map<String, Object>> awardsList = new ArrayList<Map<String, Object>>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  if (product != null && page != null) {
   try {
    LOGGER.debug("Inside getProductMethod ");
    getAllAwards(page, configurationService, resources, product);
    if(associatedAwardsMap.size() != 0 && associatedAwardsMap.containsKey(product.getSKU())){
     Set<Award> tempAwardList = associatedAwardsMap.get(product.getSKU());
     Iterator<Award> listIterator = tempAwardList.iterator();
     while(listIterator.hasNext()){
      awardsList.add(listIterator.next().converToMap());
     }
    }
   } catch (Exception e) {
    LOGGER.error("Error while getting awards ", e);
   }
  }
  
  return awardsList;
 }
 
 /**
  * Gets the all awards.
  *
  * @param page         the page
  * @param configurationService         the configuration service
  * @param resources         the resources
  * @param product the product
  * @return the all awards
  */
 public static void getAllAwards(Page page, ConfigurationService configurationService, Map<String, Object> resources, UNIProduct product) {
  String awardPage = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, page, AWARD, URL);
  if (StringUtils.isNotBlank(awardPage)) {
   Page awardConfigpage = page.getPageManager().getPage(awardPage);
   if (awardConfigpage != null) {
    List<Award> awardList = new ArrayList<Award>();
    Iterable<Resource> child = awardConfigpage.getContentResource().getChild("par").getChildren();
    
    for (Resource res : child) {
     if ("unilever-aem/components/content/award".equals(res.getResourceType())) {
      ValueMap properties = res.getValueMap();
      Award award = getAward(properties, page, resources);
      if (award != null) {
       awardList.add(award);
      }
     }
    }
    addAssociatedAwards(product, awardList, associatedAwardsMap);
   }
  }
 }
 
 
 /**
  * Gets the associated awards with the product.
  * 
  * @param product
  *         the product
  * @param awardList
  *         the award List
  * @param finalAwardListMap
  *         the final Award List Map
  */
 private static void addAssociatedAwards(UNIProduct product, List<Award> awardList, Map<String, Set<Award>> finalAwardListMap) {
  if (product != null && !awardList.isEmpty()) {
   Set<Award> associatedAwardList = new HashSet<Award>();
   Iterator<Award> awardtItr = awardList.iterator();
   while(awardtItr.hasNext()){
    Award award = awardtItr.next();
    ListIterator<Map<String, String>> listIterator = award.getAssociatedProducts().listIterator();
    while(listIterator.hasNext()){
     Map<String, String> associatedProductsMap = listIterator.next();
     if(associatedProductsMap.containsKey(SKU) && associatedProductsMap.get(SKU).toString().equals(product.getSKU())){
      associatedAwardList.add(award);
     }
    }
   }
   if(!associatedAwardList.isEmpty()){
    finalAwardListMap.put(product.getSKU(), associatedAwardList);
   }
  }
 }
 
 /**
  * Gets the associated awards with the product.
  *
  * @param awardList         the award List
  * @param resolver the resolver
  * @param pathSpecificAssociatedAwardsMap the path specific associated awards map
  * @return the map
  */
 public static Map<String, LinkedHashSet<Award>> addPathSpecificAssociatedAwards(Set<Award> awardList, ResourceResolver resolver, Map<String, LinkedHashSet<Award>> pathSpecificAssociatedAwardsMap) {
  if (CollectionUtils.isNotEmpty(awardList)) {
   PageManager manager = resolver.adaptTo(PageManager.class);
   Iterator<Award> awardtItr = awardList.iterator();
   while(awardtItr.hasNext()){
    Award award = awardtItr.next();
    ListIterator<Map<String, String>> listIterator = award.getAssociatedProducts().listIterator();
    while(listIterator.hasNext()){
     Map<String, String> associatedProductsMap = listIterator.next();
     if(associatedProductsMap.containsKey(PRODUCT_PATH)){
      String productPath = associatedProductsMap.get(PRODUCT_PATH);
      Resource productPathResource = resolver.getResource(productPath);
      if(productPathResource!=null){
      Page productpagePath = manager.getContainingPage(productPathResource);
      createAwardsSetPerProduct(productpagePath.getPath(), award, pathSpecificAssociatedAwardsMap);
      }
     }
    }
   }
  }
  return pathSpecificAssociatedAwardsMap;
 }
 
 /**
  * Gets the award.
  *
  * @param properties         the properties
  * @param page         the page
  * @param resources the resources
  * @return the award
  */
 private static Award getAward(ValueMap properties, Page page, Map<String, Object> resources) {
  Award award = null;
  List<Map<String, String>> associatedTags = new LinkedList<Map<String, String>>();
  ResourceResolver resourceResolver = (ResourceResolver) resources.get(ProductConstants.RESOURCE_RESOLVER);
  TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
  if (properties != null) {
   award = new Award();
   
   String[] tag = ArrayUtils.EMPTY_STRING_ARRAY;
   if (properties.containsKey(TagConstants.PN_TAGS)) {
    tag = (String[]) properties.get(TagConstants.PN_TAGS);
   }
   
   String name = getPropertyFromValueMap(properties, "name");
   String imagePath = getPropertyFromValueMap(properties, "image");
   String url = getPropertyFromValueMap(properties, "url");
   String description = getPropertyFromValueMap(properties, "description");
   String altText = getPropertyFromValueMap(properties, "altText");
   String ctaTitle = getPropertyFromValueMap(properties, "ctaText");
   String awardYear = getPropertyFromValueMap(properties, "awardYear");
   String type = getPropertyFromValueMap(properties, "type");
   Boolean openInNewWindow = Boolean.parseBoolean(getPropertyFromValueMap(properties, "openInNewWindow"));
   String[] productPages = getMultiPropertyFromValueMap(properties, "product");
   List<Map<String, String>> associatedProducts = getAssociatedProductsMap(productPages, resources);

   Image image = new Image(imagePath, altText, StringUtils.EMPTY, page, getSlingRequest(resources));
   
   GregorianCalendar startDate = getDatePropertyFromValueMap(properties, "startDate");
   GregorianCalendar endDate = getDatePropertyFromValueMap(properties, "endDate");
   
   String parseStartDate = DAMAssetUtility.getEffectiveDateFromCalendar(startDate);
   String parseEndDate = DAMAssetUtility.getEffectiveDateFromCalendar(endDate);
   
   award.setImage(image);
   award.setType(type);
   award.setName(name);
   award.setUrl(url);
   if (tag != null && !ArrayUtils.isEmpty(tag)) {
    award.setTagId(tag[0]);
    for (String dummyTag : tag) {
     Tag tagNode = tagManager.resolve(dummyTag);
     if (tagNode != null) {
      Map<String, String> tagObject = new HashMap<String, String>();
      tagObject.put("tagID", tagNode.getLocalTagID());
      tagObject.put("title", tagNode.getTitle(BaseViewHelper.getLocale(BaseViewHelper.getCurrentPage(resources))));
      associatedTags.add(tagObject);
     }
    }
   } else {
    award.setTagId(StringUtils.EMPTY);
   }
   award.setStartDate(parseStartDate);
   award.setEndDate(parseEndDate);
   award.setDescription(description);
   award.setAltText(altText);
   award.setCtaTitle(ctaTitle);
   award.setOpenInNewWindow(openInNewWindow);
   award.setAwardYear(awardYear);
   award.setAssociatedTags(associatedTags);
   award.setAssociatedProducts(associatedProducts);
  }
  return award;
 }
 
 /**
  * Gets the award.
  *
  * @param properties         the properties
  * @param page         the page
  * @param resourceResolver the resource resolver
  * @param request the request
  * @return the award
  */
 public static Award getPathSpecificAward(ValueMap properties, Page page, ResourceResolver resourceResolver, SlingHttpServletRequest request) {
  Award award = null;
  List<Map<String, String>> associatedTags = new LinkedList<Map<String, String>>();
  TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
  if (properties != null) {
   award = new Award();
   
   String[] tag = ArrayUtils.EMPTY_STRING_ARRAY;
   if (properties.containsKey(TagConstants.PN_TAGS)) {
    tag = (String[]) properties.get(TagConstants.PN_TAGS);
   }
   
   String name = getPropertyFromValueMap(properties, "name");
   String imagePath = getPropertyFromValueMap(properties, "image");
   String url = getPropertyFromValueMap(properties, "url");
   String description = getPropertyFromValueMap(properties, "description");
   String altText = getPropertyFromValueMap(properties, "altText");
   String ctaTitle = getPropertyFromValueMap(properties, "ctaText");
   String awardYear = getPropertyFromValueMap(properties, "awardYear");
   String type = getPropertyFromValueMap(properties, "type");
   Boolean openInNewWindow = Boolean.parseBoolean(getPropertyFromValueMap(properties, "openInNewWindow"));
   String[] productPages = getMultiPropertyFromValueMap(properties, "product");
   List<Map<String, String>> associatedProducts = getPathSpecificAssociatedProductsMap(productPages, resourceResolver);

   Image image = new Image(imagePath, altText, StringUtils.EMPTY, page, request);
   
   GregorianCalendar startDate = getDatePropertyFromValueMap(properties, "startDate");
   GregorianCalendar endDate = getDatePropertyFromValueMap(properties, "endDate");
   
   String parseStartDate = DAMAssetUtility.getEffectiveDateFromCalendar(startDate);
   String parseEndDate = DAMAssetUtility.getEffectiveDateFromCalendar(endDate);
   
   award.setImage(image);
   award.setType(type);
   award.setName(name);
   award.setUrl(url);
   if (tag != null && !ArrayUtils.isEmpty(tag)) {
    award.setTagId(tag[0]);
    for (String dummyTag : tag) {
     Tag tagNode = tagManager.resolve(dummyTag);
     if (tagNode != null) {
      Map<String, String> tagObject = new HashMap<String, String>();
      tagObject.put("tagID", tagNode.getLocalTagID());
      tagObject.put("title", tagNode.getTitle(BaseViewHelper.getLocale(page)));
      associatedTags.add(tagObject);
     }
    }
   } else {
    award.setTagId(StringUtils.EMPTY);
   }
   award.setStartDate(parseStartDate);
   award.setEndDate(parseEndDate);
   award.setDescription(description);
   award.setAltText(altText);
   award.setCtaTitle(ctaTitle);
   award.setOpenInNewWindow(openInNewWindow);
   award.setAwardYear(awardYear);
   award.setAssociatedTags(associatedTags);
   award.setAssociatedProducts(associatedProducts);
  }
  return award;
 }
 
 /**
  * Gets the property from value map.
  *
  * @param properties the properties
  * @param key the key
  * @return the property from value map
  */
 private static String getPropertyFromValueMap(ValueMap properties, String key) {
  String value = StringUtils.EMPTY;
  if (properties != null && properties.containsKey(key)) {
   value = (String) properties.get(key);
  }
  return value;
 }
 
 /**
  * Gets the date property from value map.
  *
  * @param properties the properties
  * @param key the key
  * @return the date property from value map
  */
 private static GregorianCalendar getDatePropertyFromValueMap(ValueMap properties, String key) {
  GregorianCalendar value = null;
  if (properties != null && properties.containsKey(key)) {
   if (properties.get(key) instanceof GregorianCalendar) {
    value = (GregorianCalendar) properties.get(key);
   } else {
    DateFormat df = new SimpleDateFormat("MM-dd-yyyy");
    Date date = null;
    try {
     date = df.parse(MapUtils.getString(properties, key));
    } catch (ParseException e) {
     LOGGER.warn("Cannot Parse TO Gregorian Calander{}", e + " " + key);
    }
    Calendar cal = new GregorianCalendar();
    cal.setTime(date);
    value = (GregorianCalendar) cal;
   }
  }
  return value;
 }
 
 /**
  * Gets Sling Request.
  *
  * @param resources         the resources
  * @return sling http servlet request
  */
 public static SlingHttpServletRequest getSlingRequest(final Map<String, Object> resources) {
  return (SlingHttpServletRequest) resources.get(ProductConstants.REQUEST);
  
 }
 
 /**
  * Gets the multi property from value map.
  *
  * @param properties the properties
  * @param propertyKey the property key
  * @return the multi property from value map
  */
 private static String[] getMultiPropertyFromValueMap(ValueMap properties, String propertyKey) {
  String[] propertyValArr = null;
  if (properties != null) {
   String[] propertyValObj = properties.get(propertyKey, String[].class);
   if (propertyValObj != null) {
    if (propertyValObj instanceof String[]) {
     propertyValArr = (String[]) propertyValObj;
    } else {
     propertyValArr = new String[] { propertyValObj.toString() };
    }
   }
  }
  return propertyValArr;
 }
 
 /**
  * Gets the associated products Map.
  * 
  * @param pdpArray
  *         the products Array
  * @param resources
  *         the resources
  * @return productList
  *         the productList
  */
 private static List<Map<String, String>> getAssociatedProductsMap(String[] pdpArray, Map<String, Object> resources) {
  ResourceResolver resourceResolver = (ResourceResolver) resources.get(ProductConstants.RESOURCE_RESOLVER);
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  List<Map<String, String>> productList = new ArrayList<Map<String, String>>();
   if (pdpArray != null) {
    for (int i = 0; i < pdpArray.length; i++) {
     Map<String, String> productMap = new HashMap<String, String>();
     try {
      JSONObject configurationJSON = new JSONObject(pdpArray[i]);
      String productPath = configurationJSON.getString(PRODUCT_PATH);
      UNIProduct product = ProductHelper.getUniProduct(pageManager.getPage(productPath));
      if (null != product) {
       productMap.put(SKU, product.getSKU());
       productMap.put(IDENTIFIER_TYPE, product.getIdentifierType());
       productMap.put(NAME, product.getName());
       productList.add(productMap);
      }
     } catch (JSONException e) {
      LOGGER.error("Could not find productPath",e);
     }
    }
   }
  return productList;
 }
 
 /**
  * Gets the associated products Map.
  *
  * @param pdpArray         the products Array
  * @param resourceResolver the resource resolver
  * @return productList
  *         the productList
  */
 private static List<Map<String, String>> getPathSpecificAssociatedProductsMap(String[] pdpArray, ResourceResolver resourceResolver) {
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  List<Map<String, String>> productList = new ArrayList<Map<String, String>>();
   if (pdpArray != null) {
    for (int i = 0; i < pdpArray.length; i++) {
     Map<String, String> productMap = new HashMap<String, String>();
     try {
      JSONObject configurationJSON = new JSONObject(pdpArray[i]);
      String productPath = configurationJSON.getString(PRODUCT_PATH);
      UNIProduct product = ProductHelper.getUniProduct(pageManager.getPage(productPath));
      if (null != product) {
       productMap.put(SKU, product.getSKU());
       productMap.put(IDENTIFIER_TYPE, product.getIdentifierType());
       productMap.put(NAME, product.getName());
       productMap.put(PRODUCT_PATH, pageManager.getContainingPage(product.getPath()).getPath());
       productList.add(productMap);
      }
     } catch (JSONException e) {
      LOGGER.error("Could not find productPath",e);
     }
    }
   }
  return productList;
 }
 
 /**
  * Creates the awards set per product.
  *
  * @param productPath the product path
  * @param award the award
  * @param pathSpecificAssociatedAwardsMap the path specific associated awards map
  */
 private static void createAwardsSetPerProduct(String productPath, Award award, Map<String, LinkedHashSet<Award>> pathSpecificAssociatedAwardsMap){
  if(pathSpecificAssociatedAwardsMap.containsKey(productPath)){
   LinkedHashSet<Award> awardsSet = pathSpecificAssociatedAwardsMap.get(productPath);
   awardsSet.add(award);
   pathSpecificAssociatedAwardsMap.put(productPath, awardsSet);
  }else{
   LinkedHashSet<Award> awardSet = new LinkedHashSet<Award>();
   awardSet.add(award);
   pathSpecificAssociatedAwardsMap.put(productPath, awardSet);
  }
 }
}


