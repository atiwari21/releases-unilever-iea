/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.constants;

/**
 * The Class MultipleRelatedArticlesConstants.
 */
public class MultipleRelatedArticlesConstants {
 
 /** The background image. */
 public static final String BACKGROUND_IMAGE = "backgroundImage";
 
 /** The Constant BACKGROUND_IMAGE_ALT_TEXT. */
 public static final String BACKGROUND_IMAGE_ALT_TEXT = "backgroundImageAltText";
 
 /** The Constant LOAD_MORE_BUTTON_TEXT. */
 public static final String LOAD_MORE_BUTTON_TEXT = "loadMoreButtonText";
 
 /** The title. */
 public static final String TITLE = "title";
 
 /** The button text. */
 public static final String BUTTON_TEXT = "buttonText";
 
 /** The articles. */
 public static final String ARTICLES = "articles";
 
 /** The icon. */
 public static final String ICON = "icon";
 
 /** The icon path. */
 public static final String ICON_PATH = "iconPath";
 
 public static final String TYPE = "type";
 
 /**
  * Instantiates a new multiple related products constants.
  */
 private MultipleRelatedArticlesConstants() {
  
 }
 
}
