/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.Resource;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.Image;
import com.unilever.platform.foundation.template.dto.SocialTabQueryDTO;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;
import com.unilever.platform.foundation.viewhelper.constants.SocialMediaGalleryConstants;

/**
 * The Class SocialMediaGalleryViewHelperImpl.
 */
@Component(description = "SocialImageGallery to show mix of image and video", immediate = true, metatype = true, label = "Social And Image Gallery")
@Service(value = { SocialMediaGalleryViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.SOCIAL_MEDIA_GALLERY, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class SocialMediaGalleryViewHelperImpl extends SocialTabBaseViewHelperImpl {
 /**
  * logger object.
  **/
 private static final Logger LOGGER = LoggerFactory.getLogger(SocialMediaGalleryViewHelperImpl.class);
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.impl. SocialTabBaseViewHelperImpl#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.info("In processData method of SocialImageGalleryViewHelperImpl Class");
  I18n i18n = getI18n(resources);
  Resource currentResource = getCurrentResource(resources);
  Map<String, Object> properties = getProperties(content);
  Page currentPage = getCurrentPage(resources);
  Map<String, Object> socialMediaGallery = new LinkedHashMap<String, Object>();
  String aggregatorServiceUrl = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
    SocialMediaGalleryConstants.AGGREGATOR_SERVICE, SocialMediaGalleryConstants.URL);
  String galleryType = MapUtils.getString(properties, SocialMediaGalleryConstants.GENERAL_CONFIG_GALLERY_TYPE, StringUtils.EMPTY);
  
  socialMediaGallery.put(SocialMediaGalleryConstants.PAGINATION_KEY, getPaginationProperties(properties, i18n));
  
  // Included Social Channels Check
  boolean isFacebook = MapUtils.getBoolean(properties, SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_FACEBOOK, false);
  boolean isTwitter = MapUtils.getBoolean(properties, SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_TWITTER, false);
  boolean isInstagram = MapUtils.getBoolean(properties, SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_INSTAGRAM, false);
  boolean isVideo = MapUtils.getBoolean(properties, SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_VIDEO, false);
  boolean isLocalContent = MapUtils.getBoolean(properties, SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_LOCAL_ASSET, false);
  
  // Service end point
  SocialTabQueryDTO tabQuery = new SocialTabQueryDTO();
  tabQuery.setBrandNameParameter(getBrandNameParameter(configurationService, currentPage));
  tabQuery.setConfigurationService(configurationService);
  
  if (StringUtils.equals(galleryType, SocialMediaGalleryConstants.FIXED_LIST)) {
   tabQuery.setSocialChannels(addSocialChannelNamesFixed(currentResource));
   isFacebook = false;
   isTwitter = false;
   isInstagram = false;
   isVideo = false;
   isLocalContent = false;
  } else {
   Map<String, Boolean> socialChannels = new LinkedHashMap<>();
   socialChannels.put(WordUtils.capitalize(SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_FACEBOOK), isFacebook);
   socialChannels.put(WordUtils.capitalize(SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_TWITTER), isTwitter);
   socialChannels.put(WordUtils.capitalize(SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_INSTAGRAM), isInstagram);
   socialChannels.put(WordUtils.capitalize(SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_VIDEO), isVideo);
   socialChannels.put(WordUtils.capitalize(SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_LOCAL_ASSET), isLocalContent);
   tabQuery.setSocialChannels(setServiceEndPointObject(socialChannels));
  }
  tabQuery.setPage(currentPage);
  tabQuery.setPagePath(getResourceResolver(resources).map(aggregatorServiceUrl));
  
  // TS service layer map.
  addRetrievalQueryParametrsMap(socialMediaGallery, tabQuery, galleryType, properties, currentPage, currentResource, resources);
  
  // Get and set generalConfigTab Data.
  Map<String, Object> generalConfigDataMap = getGeneralConfigTabData(properties, i18n, galleryType, currentResource);
  generalConfigDataMap.put(SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_TWITTER, isTwitter);
  generalConfigDataMap.put(SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_INSTAGRAM, isInstagram);
  generalConfigDataMap.put(SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_FACEBOOK, isFacebook);
  generalConfigDataMap.put(SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_VIDEO, isVideo);
  generalConfigDataMap.put(SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_LOCAL_ASSET, isLocalContent);
  
  // i8n labels
  generalConfigDataMap.put(SocialMediaGalleryConstants.SOCIAL_POST_LIKES_SUFFIX,
    i18n.get(SocialMediaGalleryConstants.SOCIAL_MEDIA_GALLERY_SOCIAL_POST_LIKE_SUFFIX));
  generalConfigDataMap.put(SocialMediaGalleryConstants.VIEW_MULTIPLE_HEADING_TEXT,
    i18n.get(SocialMediaGalleryConstants.SOCIAL_MEDIA_GALLERY_VIEW_MULTIPLE_HEADING_TEXT));
  
  socialMediaGallery.put(SocialMediaGalleryConstants.GENERAL_CONFIG, generalConfigDataMap);
  
  List<Map<String, Object>> socialMediaConfigurationList = new ArrayList<Map<String, Object>>();
  if (StringUtils.equals(SocialMediaGalleryConstants.FIXED_LIST, galleryType)) {
   socialMediaConfigurationList.addAll(getIncludedSocialChannelMap(currentResource, resources, currentPage, SocialMediaGalleryConstants.FIXED_LIST));
  } else if (StringUtils.equals(SocialMediaGalleryConstants.DYNAMIC_GALLERY, galleryType)) {
   
   if (isFacebook) {
    socialMediaConfigurationList
      .addAll(getIncludedSocialChannelMap(currentResource, resources, currentPage, SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_FACEBOOK));
   }
   if (isTwitter) {
    socialMediaConfigurationList
      .addAll(getIncludedSocialChannelMap(currentResource, resources, currentPage, SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_TWITTER));
   }
   if (isInstagram) {
    socialMediaConfigurationList
      .addAll(getIncludedSocialChannelMap(currentResource, resources, currentPage, SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_INSTAGRAM));
   }
   if (isVideo) {
    socialMediaConfigurationList
      .addAll(getIncludedSocialChannelMap(currentResource, resources, currentPage, SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_VIDEO));
   }
   if (isLocalContent) {
    socialMediaConfigurationList
      .addAll(getIncludedSocialChannelMap(currentResource, resources, currentPage, SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_LOCAL_ASSET));
   }
  }
  socialMediaGallery.put(SocialMediaGalleryConstants.SOCIAL_CONFIGURATION, socialMediaConfigurationList.toArray());
  Map<String, Object> finalDataMap = new HashMap<String, Object>();
  finalDataMap.put(getJsonNameSpace(content), socialMediaGallery);
  return finalDataMap;
 }
 
 /**
  * Sets the service end point object.
  *
  * @param socialChannels
  *         the social channels
  * @return the list
  */
 private List<String> setServiceEndPointObject(Map<String, Boolean> socialChannels) {
  List<String> channelNameList = new ArrayList<String>();
  for (Map.Entry<String, Boolean> entry : socialChannels.entrySet()) {
   if (entry.getValue()) {
    channelNameList.add(entry.getKey());
   }
  }
  return channelNameList;
 }
 
 /**
  * Gets the general config tab data.
  *
  * @param properties
  *         the properties
  * @param i18n
  *         the i18n
  * @return the general config tab data
  */
 private Map<String, Object> getGeneralConfigTabData(Map<String, Object> properties, I18n i18n, String galleryType, Resource currentRes) {
  Map<String, Object> generalConfigDataMap = new LinkedHashMap<String, Object>();
  
  generalConfigDataMap.put(SocialMediaGalleryConstants.GENERAL_CONFIG_HEADING_TEXT,
    MapUtils.getString(properties, SocialMediaGalleryConstants.GENERAL_CONFIG_HEADING_TEXT, StringUtils.EMPTY));
  generalConfigDataMap.put(SocialMediaGalleryConstants.GENERAL_CONFIG_SUB_HEADING_TEXT,
    MapUtils.getString(properties, SocialMediaGalleryConstants.GENERAL_CONFIG_SUB_HEADING_TEXT, StringUtils.EMPTY));
  generalConfigDataMap.put(SocialMediaGalleryConstants.GENERAL_CONFIG_LONG_HEADING_TEXT,
    MapUtils.getString(properties, SocialMediaGalleryConstants.GENERAL_CONFIG_LONG_HEADING_TEXT, StringUtils.EMPTY));
  generalConfigDataMap.put(SocialMediaGalleryConstants.GENERAL_CONFIG_CTA_LABEL,
    MapUtils.getString(properties, SocialMediaGalleryConstants.GENERAL_CONFIG_CTA_LABEL, StringUtils.EMPTY));
  generalConfigDataMap.put(SocialMediaGalleryConstants.GENERAL_CONFIG_CTA_URL,
    MapUtils.getString(properties, SocialMediaGalleryConstants.GENERAL_CONFIG_CTA_URL, StringUtils.EMPTY));
  generalConfigDataMap.put(SocialMediaGalleryConstants.GENERAL_CONFIG_NEW_WINDOW,
    MapUtils.getBoolean(properties, SocialMediaGalleryConstants.OPEN_IN_NEW_WINDOW, false));
  generalConfigDataMap.put(SocialMediaGalleryConstants.GENERAL_CONFIG_GALLERY_TYPE, galleryType);
  
  boolean enableFilter = MapUtils.getBoolean(properties, SocialMediaGalleryConstants.GENERAL_CONFIG_ENABLE_FILTER, false);
  generalConfigDataMap.put(SocialMediaGalleryConstants.GENERAL_CONFIG_ENABLE_FILTER, enableFilter);
  
  if (enableFilter) {
   generalConfigDataMap.put(SocialMediaGalleryConstants.FILTER_CONFIG, getFilterConfigTabData(properties, i18n, currentRes));
  }
  
  return generalConfigDataMap;
 }
 
 /**
  * Gets the social hash tag map.
  *
  * @param isSocialChannelEnabled
  *         the is social channel enabled
  * @param currentResource
  *         the current resource
  * @param socialHashTagArrayName
  *         the social hash tag array name
  * @param socialHashTagFieldName
  *         the social hash tag field name
  * @param newList
  *         the new list
  * @return the social hash tag map
  */
 private List<Map<String, String>> getMultiFieldListMap(boolean isSocialChannelEnabled, Resource currentResource, String socialHashTagArrayName,
   String socialHashTagFieldName, String keyName) {
  List<Map<String, String>> localNewList = new LinkedList<Map<String, String>>();
  if (isSocialChannelEnabled) {
   List<Map<String, String>> socialHashTagList = ComponentUtil.getNestedMultiFieldProperties(currentResource, socialHashTagArrayName);
   for (Map<String, String> map : socialHashTagList) {
    String hashTag = (String) map.get(socialHashTagFieldName);
    map.remove(socialHashTagFieldName);
    map.put(keyName, hashTag);
    localNewList.add(map);
   }
  }
  return localNewList;
 }
 
 /**
  * Adds the social channel names fixed.
  *
  * @param currentResource
  *         the current resource
  * @return the list
  */
 private static List<String> addSocialChannelNamesFixed(Resource currentResource) {
  List<String> channelNameList = new ArrayList<String>();
  List<Map<String, String>> list = ComponentUtil.getNestedMultiFieldProperties(currentResource, SocialMediaGalleryConstants.FIXED_LIST_POST);
  
  for (Map<String, String> map : list) {
   channelNameList.add(MapUtils.getString(map, SocialMediaGalleryConstants.FIXED_CHANNEL_TYPE, StringUtils.EMPTY));
  }
  List<String> newList = new ArrayList<String>(new HashSet<String>(channelNameList));
  return newList;
 }
 
 /**
  * Adds the retrieval query parametrs map.
  *
  * @param map
  *         the map
  * @param socialTabQuery
  *         the social tab query
  * @param galleryType
  *         the gallery type
  * @param properties
  *         the properties
  * @param currentPage
  *         the current page
  */
 public void addRetrievalQueryParametrsMap(Map<String, Object> map, SocialTabQueryDTO socialTabQuery, String galleryType,
   Map<String, Object> properties, Page currentPage, Resource currentResource, Map<String, Object> resources) {
  
  Map<String, Object> requestServletUrlMap = new LinkedHashMap<String, Object>();
  
  requestServletUrlMap.put(SocialMediaGalleryConstants.BRAND_NAME_PARAMETER, socialTabQuery.getBrandNameParameter());
  setGlobalConfigAttributes(currentPage, requestServletUrlMap, properties, galleryType);
  
  //Shop Now Map if but it now enabled.
  if((Boolean)requestServletUrlMap.get(SocialMediaGalleryConstants.ENABLE_BUY_IT_NOW)){
	  Map<String, String> shopNowMap = ProductHelper.getshopNowProperties(configurationService, currentPage,
				getI18n(resources), getResourceResolver(resources), getSlingRequest(resources));
	  requestServletUrlMap.put(ProductConstants.SHOPNOW_MAP, shopNowMap);
  }else{
	  requestServletUrlMap.put(ProductConstants.SHOPNOW_MAP, null);
  }
  
  requestServletUrlMap.put(SocialMediaGalleryConstants.REQUEST_SERVLET_URL, socialTabQuery.getPagePath());
  
  addSocialChannelsMap(requestServletUrlMap, socialTabQuery.getSocialChannels(), socialTabQuery.getPage(), socialTabQuery.getConfigurationService(),
    properties, currentResource);
  map.put(SocialMediaGalleryConstants.RETRIEVAL_QUERY_PARAMS, requestServletUrlMap);
  
 }
 
 /**
  * Sets the global config attributes.
  *
  * @param currentPage
  *         the current page
  * @param requestServletUrlMap
  *         the request servlet url map
  * @param properties
  *         the properties
  */
 private void setGlobalConfigAttributes(Page currentPage, Map<String, Object> requestServletUrlMap, Map<String, Object> properties, String galleryType) {
  boolean overrideGlobalConfig = MapUtils.getBoolean(properties, UnileverConstants.OVERRIDE_GLOBAL_CONFIG, false);
  
  int limitResultsTo = GlobalConfigurationUtility.getIntegerValueFromConfiguration(SocialMediaGalleryConstants.SOCIAL_MEDIA_GALLERY,
    configurationService, currentPage, SocialMediaGalleryConstants.LIMIT_RESULTS_TO);
  boolean expandablePanelFlag = true;
  boolean enableCarouselPanel = false;
  boolean showAuthorDetails = false;
  boolean enableExpandablePanelOnDescription = false;
  boolean visualOpenInNewWindow = false;
  boolean enableBuyItNow = false;
  String linkToSocialChannel = StringUtils.EMPTY;
  
  boolean displayOverlay = false;
  boolean autoPlay = false;
  boolean hideControls = false;
  boolean muteAudio = false;
  boolean loopVideoPlayback = false;
  boolean showRelatedVideo = false;
  
  if(StringUtils.equals(galleryType, SocialMediaGalleryConstants.DYNAMIC_GALLERY)){
    String videoSource = MapUtils.getString(properties, SocialMediaGalleryConstants.VIDEO_SOURCE, StringUtils.EMPTY);
    if(StringUtils.equals(videoSource, "youku")){
     displayOverlay = getControlFlag(overrideGlobalConfig, SocialMediaGalleryConstants.DISPLAY_OVERLAY, properties, currentPage);
     autoPlay = getControlFlag(overrideGlobalConfig, SocialMediaGalleryConstants.AUTOPLAY, properties, currentPage);
    }else if(StringUtils.equals(videoSource, "iqiyi") || StringUtils.equals(videoSource, "tencent")){
     autoPlay = getControlFlag(overrideGlobalConfig, SocialMediaGalleryConstants.AUTOPLAY, properties, currentPage);
    }else if(StringUtils.equals(videoSource, "youtube")){
     displayOverlay = getControlFlag(overrideGlobalConfig, SocialMediaGalleryConstants.DISPLAY_OVERLAY, properties, currentPage);
     autoPlay = getControlFlag(overrideGlobalConfig, SocialMediaGalleryConstants.AUTOPLAY, properties, currentPage);
     hideControls = getControlFlag(overrideGlobalConfig, SocialMediaGalleryConstants.HIDE_CONTROLS, properties, currentPage);
     muteAudio = getControlFlag(overrideGlobalConfig, SocialMediaGalleryConstants.MUTE_AUDIO, properties, currentPage);
     loopVideoPlayback = getControlFlag(overrideGlobalConfig, SocialMediaGalleryConstants.LOOP_VIDEO_PLAYBACK, properties, currentPage);
     showRelatedVideo = getControlFlag(overrideGlobalConfig, SocialMediaGalleryConstants.SHOW_RELATED_VIDEO_AT_END, properties, currentPage);
   }
  }else if(StringUtils.equals(galleryType, SocialMediaGalleryConstants.FIXED_LIST)){
   
    displayOverlay = getControlFlag(overrideGlobalConfig, SocialMediaGalleryConstants.DISPLAY_OVERLAY, properties, currentPage);
    autoPlay = getControlFlag(overrideGlobalConfig, SocialMediaGalleryConstants.AUTOPLAY, properties, currentPage);
    hideControls = getControlFlag(overrideGlobalConfig, SocialMediaGalleryConstants.HIDE_CONTROLS, properties, currentPage);
    muteAudio = getControlFlag(overrideGlobalConfig, SocialMediaGalleryConstants.MUTE_AUDIO, properties, currentPage);
    loopVideoPlayback = getControlFlag(overrideGlobalConfig, SocialMediaGalleryConstants.LOOP_VIDEO_PLAYBACK, properties, currentPage);
    showRelatedVideo = getControlFlag(overrideGlobalConfig, SocialMediaGalleryConstants.SHOW_RELATED_VIDEO_AT_END, properties, currentPage);
  }
  if (overrideGlobalConfig) {
   limitResultsTo = MapUtils.getIntValue(properties, SocialMediaGalleryConstants.LIMIT, limitResultsTo);
   expandablePanelFlag = MapUtils.getBoolean(properties, SocialMediaGalleryConstants.EXPANDABLE_PANEL, false);
   
   enableCarouselPanel = MapUtils.getBoolean(properties, SocialMediaGalleryConstants.ENABLE_CAROUEL_PNAEL, false);
   showAuthorDetails = MapUtils.getBoolean(properties, SocialMediaGalleryConstants.SHOW_AUTHOR_DETAILS, false);
   enableExpandablePanelOnDescription = MapUtils.getBoolean(properties, SocialMediaGalleryConstants.EXPAND_PANEL_ON_DESCRIPTION, false);
   enableBuyItNow = MapUtils.getBoolean(properties, SocialMediaGalleryConstants.ENABLE_BUY_IT_NOW, false);
   visualOpenInNewWindow = MapUtils.getBoolean(properties, SocialMediaGalleryConstants.VISUAL_OPEN_NEW_WINDOW, false);
   linkToSocialChannel = MapUtils.getString(properties, SocialMediaGalleryConstants.LINK_TO_SOCIAL_CHANNEL, linkToSocialChannel);
  } else {
   expandablePanelFlag = Boolean.parseBoolean(GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
     SocialMediaGalleryConstants.SOCIAL_MEDIA_GALLERY, SocialMediaGalleryConstants.EXPANDABLE_PANEL_FLAG));
   enableCarouselPanel = Boolean.parseBoolean(GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
     SocialMediaGalleryConstants.SOCIAL_MEDIA_GALLERY, SocialMediaGalleryConstants.ENABLE_CAROUEL_PNAEL));
   enableBuyItNow = Boolean.parseBoolean(GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
     SocialMediaGalleryConstants.SOCIAL_MEDIA_GALLERY, SocialMediaGalleryConstants.ENABLE_BUY_IT_NOW));
   visualOpenInNewWindow = Boolean.parseBoolean(GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
     SocialMediaGalleryConstants.SOCIAL_MEDIA_GALLERY, SocialMediaGalleryConstants.VISUAL_OPEN_NEW_WINDOW));
   linkToSocialChannel = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
     SocialMediaGalleryConstants.SOCIAL_MEDIA_GALLERY, SocialMediaGalleryConstants.LINK_TO_SOCIAL_CHANNEL);
   showAuthorDetails = Boolean.parseBoolean(GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
     SocialMediaGalleryConstants.SOCIAL_MEDIA_GALLERY, SocialMediaGalleryConstants.SHOW_AUTHOR_DETAILS));
   enableExpandablePanelOnDescription = Boolean.parseBoolean(GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
     SocialMediaGalleryConstants.SOCIAL_MEDIA_GALLERY, SocialMediaGalleryConstants.EXPAND_PANEL_ON_DESCRIPTION));
  }
  requestServletUrlMap.put(SocialMediaGalleryConstants.LIMIT_RESULTS_TO, limitResultsTo);
  requestServletUrlMap.put(SocialMediaGalleryConstants.EXPANDABLE_PANEL_FLAG, expandablePanelFlag);
  requestServletUrlMap.put(SocialMediaGalleryConstants.ENABLE_CAROUEL_PNAEL, enableCarouselPanel);
  requestServletUrlMap.put(SocialMediaGalleryConstants.ENABLE_BUY_IT_NOW, enableBuyItNow);
  requestServletUrlMap.put(SocialMediaGalleryConstants.VISUAL_OPEN_NEW_WINDOW, visualOpenInNewWindow);
  requestServletUrlMap.put(SocialMediaGalleryConstants.LINK_TO_SOCIAL_CHANNEL, linkToSocialChannel);
  requestServletUrlMap.put(SocialMediaGalleryConstants.SHOW_AUTHOR_DETAILS, showAuthorDetails);
  requestServletUrlMap.put(SocialMediaGalleryConstants.EXPAND_PANEL_ON_DESCRIPTION, enableExpandablePanelOnDescription);
  
  // video settings
  Map<String, Boolean> videoMap = new LinkedHashMap<>();
  videoMap.put(SocialMediaGalleryConstants.DISPLAY_OVERLAY, displayOverlay);
  videoMap.put(SocialMediaGalleryConstants.AUTOPLAY, autoPlay);
  videoMap.put(SocialMediaGalleryConstants.HIDE_CONTROLS, hideControls);
  videoMap.put(SocialMediaGalleryConstants.MUTE_AUDIO, muteAudio);
  videoMap.put(SocialMediaGalleryConstants.LOOP_VIDEO_PLAYBACK, loopVideoPlayback);
  videoMap.put(SocialMediaGalleryConstants.SHOW_RELATED_VIDEO_AT_END, showRelatedVideo);
  
  requestServletUrlMap.put(SocialMediaGalleryConstants.MEDIA, videoMap);
 }
 
  private boolean getControlFlag(boolean overrideGlobalConfig, String propName, Map<String, Object> properties, Page currentPage){
   if(overrideGlobalConfig){
    return MapUtils.getBoolean(properties, propName, false);
   }else{
    return Boolean.parseBoolean(GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
      SocialMediaGalleryConstants.SOCIAL_MEDIA_GALLERY_VIDEO_VIDEO_SETTINGS, propName));
   }
  }
 /**
  * Adds the hash tags map.
  *
  * @param requestServletUrlMap
  *         the request servlet url map
  * @param hashTags
  *         the hash tags
  */
 private List<Map<String, Object>> createMutilFieldList(List<Map<String, String>> hashTags, String getKeyName, String putKeyID, String replaceChar) {
  List<Map<String, Object>> hashTagList = new ArrayList<Map<String, Object>>();
  
  for (Map<String, String> map : hashTags) {
   Map<String, Object> hashTagMap = new LinkedHashMap<String, Object>();
   
   String hashTag = StringUtils.defaultString(map.get(getKeyName), StringUtils.EMPTY);
   hashTag = hashTag.trim().replaceFirst(replaceChar, StringUtils.EMPTY);
   hashTagMap.put(putKeyID, hashTag);
   hashTagList.add(hashTagMap);
  }
  return hashTagList;
 }
 
 /**
  * Adds the social channels map.
  *
  * @param requestServletUrlMap
  *         the request servlet url map
  * @param socialChannels
  *         the social channels
  * @param currentPage
  *         the current page
  * @param configurationService
  *         the configuration service
  * @param properties
  *         the properties
  */
 private void addSocialChannelsMap(Map<String, Object> requestServletUrlMap, List<String> socialChannels, Page currentPage,
   ConfigurationService configurationService, Map<String, Object> properties, Resource cuResource) {
  List<Map<String, Object>> socialChannelsList = new ArrayList<Map<String, Object>>();
  for (String channel : socialChannels) {
   socialChannelsList.add(getSocialChannelMap(channel.toLowerCase(), currentPage, configurationService, properties, cuResource));
  }
  requestServletUrlMap.put(SocialMediaGalleryConstants.SOCIAL_CHANNEL_NAMES, socialChannelsList.toArray());
 }
 
 /**
  * Gets the social channel map.
  *
  * @param channel
  *         the channel
  * @param currentPage
  *         the current page
  * @param configurationService
  *         the configuration service
  * @param properties
  *         the properties
  * @return the social channel map
  */
 private Map<String, Object> getSocialChannelMap(String channel, Page currentPage, ConfigurationService configurationService,
   Map<String, Object> properties, Resource currentResource) {
  Map<String, Object> socialChannelsMap = new LinkedHashMap<String, Object>();
  Map<String, String> channelsConfigs = GlobalConfigurationUtility.getMapFromConfiguration(configurationService, currentPage, channel.toLowerCase());
  socialChannelsMap.put(SocialMediaGalleryConstants.NAME, channel);
  socialChannelsMap.put(SocialMediaGalleryConstants.URL_JSON_KEY, channelsConfigs.get(SocialMediaGalleryConstants.URL_CONFIG_KEY));
  socialChannelsMap.put(SocialMediaGalleryConstants.IMAGE_PATH, channelsConfigs.get(SocialMediaGalleryConstants.LOGO));
  
  if (StringUtils.equals(SocialMediaGalleryConstants.FIXED_LIST,
    MapUtils.getString(properties, SocialMediaGalleryConstants.GENERAL_CONFIG_GALLERY_TYPE, SocialMediaGalleryConstants.DYNAMIC_GALLERY))) {
   setFixedListData(channel, socialChannelsMap, properties, currentResource);
  } else {
   if (StringUtils.equals(SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_FACEBOOK, channel)) {
    // set account fields
    List<Map<String, String>> accountIdFieldMap = getMultiFieldListMap(true, currentResource,
      SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_FACEBOOK + SocialMediaGalleryConstants.ACCOUNT_ID_JSON,
      SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_FACEBOOK + WordUtils.capitalize(SocialMediaGalleryConstants.ACCOUNT_ID),
      SocialMediaGalleryConstants.ACCOUNT_ID);
    socialChannelsMap.put(SocialMediaGalleryConstants.ACCOUNT_ID,
      createMutilFieldList(accountIdFieldMap, SocialMediaGalleryConstants.ACCOUNT_ID, SocialMediaGalleryConstants.ID, StringUtils.EMPTY)
        .toArray());
    
    // set hash tag fields
    List<Map<String, String>> hashTagFieldMap = getMultiFieldListMap(true, currentResource,
      SocialMediaGalleryConstants.HASH_TAGS + WordUtils.capitalize(SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_FACEBOOK)
        + WordUtils.capitalize(SocialMediaGalleryConstants.JSON),
      SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_FACEBOOK + WordUtils.capitalize(SocialMediaGalleryConstants.HASH_TAG_NAME),
      SocialMediaGalleryConstants.HASH_TAG_NAME);
    socialChannelsMap.put(SocialMediaGalleryConstants.HASH_TAGS, createMutilFieldList(hashTagFieldMap, SocialMediaGalleryConstants.HASH_TAG_NAME,
      SocialMediaGalleryConstants.ID, SocialMediaGalleryConstants.HASH).toArray());
    
    // include filters
    socialChannelsMap.put(SocialMediaGalleryConstants.IMAGE_ONLY, MapUtils.getBoolean(properties,
      SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_FACEBOOK + WordUtils.capitalize(SocialMediaGalleryConstants.IMAGE_ONLY), false));
    socialChannelsMap.put(SocialMediaGalleryConstants.VIDEO_ONLY, MapUtils.getBoolean(properties,
      SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_FACEBOOK + WordUtils.capitalize(SocialMediaGalleryConstants.VIDEO_ONLY), false));
    socialChannelsMap.put(SocialMediaGalleryConstants.TEXT_ONLY, MapUtils.getBoolean(properties,
      SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_FACEBOOK + WordUtils.capitalize(SocialMediaGalleryConstants.TEXT_ONLY), false));
   } else if (StringUtils.equals(SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_TWITTER, channel)) {
    // set account fields
    List<Map<String, String>> accountIdFieldMap = getMultiFieldListMap(true, currentResource,
      SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_TWITTER + WordUtils.capitalize(SocialMediaGalleryConstants.ACCOUNT_ID)
        + SocialMediaGalleryConstants.JSON,
      SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_TWITTER + WordUtils.capitalize(SocialMediaGalleryConstants.ACCOUNT_ID),
      SocialMediaGalleryConstants.ACCOUNT_ID);
    socialChannelsMap.put(SocialMediaGalleryConstants.ACCOUNT_ID,
      createMutilFieldList(accountIdFieldMap, SocialMediaGalleryConstants.ACCOUNT_ID, SocialMediaGalleryConstants.ID, StringUtils.EMPTY)
        .toArray());
    
    // search post by
    String searchPostBy = MapUtils.getString(properties, SocialMediaGalleryConstants.SEARCH_POST_BY, SocialMediaGalleryConstants.HASH_TAGS);
    socialChannelsMap.put(SocialMediaGalleryConstants.SEARCH_POST_BY, searchPostBy);
    if (StringUtils.equals(SocialMediaGalleryConstants.HASH_TAGS, searchPostBy)) {
     // set hash tag fields
     List<Map<String, String>> hashTagFieldMap = getMultiFieldListMap(true, currentResource,
       SocialMediaGalleryConstants.HASH_TAGS + WordUtils.capitalize(SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_TWITTER)
         + SocialMediaGalleryConstants.JSON,
       SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_TWITTER + WordUtils.capitalize(SocialMediaGalleryConstants.HASH_TAG_NAME),
       SocialMediaGalleryConstants.HASH_TAG_NAME);
     socialChannelsMap.put(SocialMediaGalleryConstants.HASH_TAGS, createMutilFieldList(hashTagFieldMap, SocialMediaGalleryConstants.HASH_TAG_NAME,
       SocialMediaGalleryConstants.ID, SocialMediaGalleryConstants.HASH).toArray());
    } else if (StringUtils.equals(SocialMediaGalleryConstants.LIST, searchPostBy)) {
     // set hash tag fields
     List<Map<String, String>> hashTagFieldMap = getMultiFieldListMap(true, currentResource,
       SocialMediaGalleryConstants.LIST + WordUtils.capitalize(SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_TWITTER)
         + SocialMediaGalleryConstants.JSON, SocialMediaGalleryConstants.LIST_NAME, SocialMediaGalleryConstants.LIST_NAME);
     socialChannelsMap.put(SocialMediaGalleryConstants.LIST,
       createMutilFieldList(hashTagFieldMap, SocialMediaGalleryConstants.LIST_NAME, SocialMediaGalleryConstants.ID, SocialMediaGalleryConstants.HASH)
         .toArray());
    }
    // include filters
    socialChannelsMap.put(SocialMediaGalleryConstants.IMAGE_ONLY, MapUtils.getBoolean(properties,
      SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_TWITTER + WordUtils.capitalize(SocialMediaGalleryConstants.IMAGE_ONLY), false));
    socialChannelsMap.put(SocialMediaGalleryConstants.VIDEO_ONLY, MapUtils.getBoolean(properties,
      SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_TWITTER + WordUtils.capitalize(SocialMediaGalleryConstants.VIDEO_ONLY), false));
    socialChannelsMap.put(SocialMediaGalleryConstants.TEXT_ONLY, MapUtils.getBoolean(properties,
      SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_TWITTER + WordUtils.capitalize(SocialMediaGalleryConstants.TEXT_ONLY), false));
   } else if (StringUtils.equals(SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_INSTAGRAM, channel)) {
    // set account fields
    List<Map<String, String>> accountIdFieldMap = getMultiFieldListMap(true, currentResource,
      SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_INSTAGRAM + WordUtils.capitalize(SocialMediaGalleryConstants.ACCOUNT_ID)
        + SocialMediaGalleryConstants.JSON,
      SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_INSTAGRAM + WordUtils.capitalize(SocialMediaGalleryConstants.ACCOUNT_ID),
      SocialMediaGalleryConstants.ACCOUNT_IDS);
    socialChannelsMap.put(SocialMediaGalleryConstants.ACCOUNT_ID,
      createMutilFieldList(accountIdFieldMap, SocialMediaGalleryConstants.ACCOUNT_IDS, SocialMediaGalleryConstants.ID, StringUtils.EMPTY)
        .toArray());
    
    // set hash tag fields
    List<Map<String, String>> hashTagFieldMap = getMultiFieldListMap(true, currentResource,
      SocialMediaGalleryConstants.HASH_TAGS + WordUtils.capitalize(SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_INSTAGRAM)
        + SocialMediaGalleryConstants.JSON,
      SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_INSTAGRAM + WordUtils.capitalize(SocialMediaGalleryConstants.HASH_TAG_NAME),
      SocialMediaGalleryConstants.HASH_TAG_NAME);
    socialChannelsMap.put(SocialMediaGalleryConstants.HASH_TAGS, createMutilFieldList(hashTagFieldMap, SocialMediaGalleryConstants.HASH_TAG_NAME,
      SocialMediaGalleryConstants.ID, SocialMediaGalleryConstants.HASH).toArray());
    
    // include filters
    socialChannelsMap.put(SocialMediaGalleryConstants.IMAGE_ONLY, MapUtils.getBoolean(properties,
      SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_INSTAGRAM + WordUtils.capitalize(SocialMediaGalleryConstants.IMAGE_ONLY), false));
    socialChannelsMap.put(SocialMediaGalleryConstants.VIDEO_ONLY, MapUtils.getBoolean(properties,
      SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_INSTAGRAM + WordUtils.capitalize(SocialMediaGalleryConstants.VIDEO_ONLY), false));
   } else {
    setVideoAndTabData(channel, socialChannelsMap, properties, currentResource);
   }
  }
  return socialChannelsMap;
 }
 
 /**
  * Sets the video and tab data.
  *
  * @param channel
  *         the channel
  * @param socialChannelsMap
  *         the social channels map
  * @param properties
  *         the properties
  * @param currentResource
  *         the current resource
  */
 private void setVideoAndTabData(String channel, Map<String, Object> socialChannelsMap, Map<String, Object> properties, Resource currentResource) {
  if (StringUtils.equals(SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_VIDEO, channel)) {
   socialChannelsMap.put(SocialMediaGalleryConstants.VIDEO_SOURCE,
     MapUtils.getString(properties, SocialMediaGalleryConstants.VIDEO_SOURCE, SocialMediaGalleryConstants.YOU_TUBE));
   socialChannelsMap.put(SocialMediaGalleryConstants.VIDEO_TYPE,
     MapUtils.getString(properties, SocialMediaGalleryConstants.VIDEO_TYPE, SocialMediaGalleryConstants.PLAY_LIST));
   socialChannelsMap.put(SocialMediaGalleryConstants.VIDEO_ID,
     MapUtils.getString(properties, SocialMediaGalleryConstants.VIDEO_ID, StringUtils.EMPTY));
  } else if (StringUtils.equals(StringUtils.lowerCase(SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_LOCAL_ASSET), channel)) {
   // set hash tag fields
   List<Map<String, String>> hashTagFieldMap = getMultiFieldListMap(true, currentResource,
     SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_LOCAL_ASSET + WordUtils.capitalize(SocialMediaGalleryConstants.HASH_TAGS),
     SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_LOCAL_ASSET + WordUtils.capitalize(SocialMediaGalleryConstants.HASH_TAG_NAME),
     SocialMediaGalleryConstants.HASH_TAG_NAME);
   socialChannelsMap.put(SocialMediaGalleryConstants.HASH_TAGS, createMutilFieldList(hashTagFieldMap, SocialMediaGalleryConstants.HASH_TAG_NAME,
     SocialMediaGalleryConstants.ID, SocialMediaGalleryConstants.HASH).toArray());
  }
 }
 
 /**
  * Sets the fixed list data.
  *
  * @param channel
  *         the channel
  * @param socialChannelsMap
  *         the social channels map
  * @param properties
  *         the properties
  */
 private void setFixedListData(String channel, Map<String, Object> socialChannelsMap, Map<String, Object> properties, Resource currentResource) {
  
  List<Map<String, String>> linkPostsList = ComponentUtil.getNestedMultiFieldProperties(currentResource, SocialMediaGalleryConstants.FIXED_LIST_POST);
  List<Map<String, Object>> arrayMapIdList = new ArrayList<>();
  for (Map<String, String> map : linkPostsList) {
   Map<String, Object> arrayMapID = new LinkedHashMap<>();
   String chanelType = map.getOrDefault(SocialMediaGalleryConstants.FIXED_CHANNEL_TYPE, StringUtils.EMPTY);
   boolean containsChannel = StringUtils.equals(chanelType, channel);
   boolean containsVideo = StringUtils.equals(chanelType, SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_VIDEO);
   if (containsChannel && !StringUtils.equals(chanelType, SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_LOCAL_ASSET)) {
    arrayMapID.put(SocialMediaGalleryConstants.ACCOUNT_ID, map.getOrDefault(SocialMediaGalleryConstants.FIXED_ACCOUNT_ID, StringUtils.EMPTY));
    arrayMapID.put(SocialMediaGalleryConstants.POST_ID, map.getOrDefault(SocialMediaGalleryConstants.FIXED_POST_ID, StringUtils.EMPTY));
    if (containsVideo) {
     arrayMapID.put(SocialMediaGalleryConstants.VIDEO_ID, map.getOrDefault(SocialMediaGalleryConstants.FIXED_VIDEO_ID, StringUtils.EMPTY));
     arrayMapID.put(SocialMediaGalleryConstants.VIDEO_SOURCE, map.getOrDefault(SocialMediaGalleryConstants.FIXED_VIDEO_SOURCE, StringUtils.EMPTY));
    }
   }
   if (!arrayMapID.isEmpty()) {
    arrayMapIdList.add(arrayMapID);
   }
  }
  socialChannelsMap.put(SocialMediaGalleryConstants.FIXED_LIST_SOCIAL, arrayMapIdList.toArray());
 }
 
 /**
  * Gets the pagination properties.
  *
  * @param properties
  *         the properties
  * @param i18n
  *         the i18n
  * @return the pagination properties
  */
 private Map<String, Object> getPaginationProperties(Map<String, Object> properties, I18n i18n) {
  Map<String, Object> paginationMap = new LinkedHashMap<String, Object>();
  String paginationType = MapUtils.getString(properties, SocialMediaGalleryConstants.PAGINATION_TYPE, StringUtils.EMPTY);
  paginationMap.put(SocialMediaGalleryConstants.PAGINATION_TYPE, paginationType);
  paginationMap.put(SocialMediaGalleryConstants.ITEMS_PER_PAGE,
    Integer.parseInt(MapUtils.getString(properties, SocialMediaGalleryConstants.ITEMS_PER_PAGE, "9")));
  String paginationCTALabel = MapUtils.getString(properties, SocialMediaGalleryConstants.PAGINATION_CTA_LABEL, StringUtils.EMPTY);
  if (StringUtils.isEmpty(paginationCTALabel)) {
   paginationCTALabel = i18n.get(SocialMediaGalleryConstants.SOCIAL_MEDIA_GALLERY_PAGINATION_CTA_LABEL);
  }
  if (StringUtils.equals(SocialMediaGalleryConstants.PAGINATION_KEY, paginationType)) {
   paginationMap.put(SocialMediaGalleryConstants.STATIC, getStaticData(i18n, paginationCTALabel));
  } else {
   Map<String, Object> staticMap = new LinkedHashMap<String, Object>();
   staticMap.put(SocialMediaGalleryConstants.PAGINATION_CTA_LABEL, paginationCTALabel);
   paginationMap.put(SocialMediaGalleryConstants.STATIC, staticMap);
  }
  return paginationMap;
 }
 
 /**
  * Gets the static data.
  *
  * @param i18n
  *         the i18n
  * @param paginationCTALabel
  *         the pagination cta label
  * @return the static data
  */
 private Map<String, Object> getStaticData(I18n i18n, String paginationCTALabel) {
  Map<String, Object> staticMap = new LinkedHashMap<String, Object>();
  staticMap.put(SocialMediaGalleryConstants.FIRST_LABEL, i18n.get(SocialMediaGalleryConstants.PAGE_LISTING_PAGINATION_FIRST));
  staticMap.put(SocialMediaGalleryConstants.LAST_LABEL, i18n.get(SocialMediaGalleryConstants.PAGE_LISTING_PAGINATION_LAST));
  staticMap.put(SocialMediaGalleryConstants.PREVIOUS_LABEL, i18n.get(SocialMediaGalleryConstants.PAGE_LISTING_PAGINATION_PREVIOUS));
  staticMap.put(SocialMediaGalleryConstants.NEXT_LABEL, i18n.get(SocialMediaGalleryConstants.PAGE_LISTING_PAGINATION_NEXT));
  staticMap.put(SocialMediaGalleryConstants.PAGINATION_CTA_LABEL, paginationCTALabel);
  return staticMap;
 }
 
 /**
  * Adds the cta map.
  *
  * @param socialCarouselMap
  *         the social carousel map
  * @param ctaType
  *         the cta type
  * @param pagePath
  *         the page path
  * @param associatedPage
  *         the associated page
  * @param i18n
  *         the i18n
  * @param resources
  *         the resources
  */
 private void addCTAMap(Map<String, Object> socialCarouselMap, String ctaType, String pagePath, Page associatedPage, I18n i18n,
   Map<String, Object> resources) {
  socialCarouselMap.put(SocialMediaGalleryConstants.POST_LINKED_TO_URL,
    ComponentUtil.getFullURL(getResourceResolver(resources), pagePath, getSlingRequest(resources)));
  Map<String, Object> productMap = new HashMap<String, Object>();
  setTeaserDetails(socialCarouselMap, associatedPage);
  if (StringUtils.equals(SocialMediaGalleryConstants.ARTICLE, ctaType)) {
   socialCarouselMap.put(SocialMediaGalleryConstants.GENERAL_CONFIG_CTA_LABEL,
     i18n.get(SocialMediaGalleryConstants.SOCIAL_MEDIA_GALLERY_VIEW_ARTICLE_CTA_LABEL));
   socialCarouselMap.put(SocialMediaGalleryConstants.GENERAL_CONFIG_HEADING_TEXT,
     i18n.get(SocialMediaGalleryConstants.SOCIAL_MEDIA_GALLERY_VIEW_ARTICLE_HEADING_TEXT));
   socialCarouselMap.put(SocialMediaGalleryConstants.ASSOCIATED_PAGE, getArticleMap(associatedPage, resources));
  } else if (StringUtils.equals(SocialMediaGalleryConstants.PRODUCT, ctaType)) {
   socialCarouselMap.put(SocialMediaGalleryConstants.GENERAL_CONFIG_CTA_LABEL,
     i18n.get(SocialMediaGalleryConstants.SOCIAL_MEDIA_GALLERY_VIEW_PRODUCT_CTA_LABEL));
   socialCarouselMap.put(SocialMediaGalleryConstants.GENERAL_CONFIG_HEADING_TEXT,
     i18n.get(SocialMediaGalleryConstants.SOCIAL_MEDIA_GALLERY_VIEW_PRODUCT_HEADING_TEXT));
   socialCarouselMap.put(SocialMediaGalleryConstants.BUY_IT_NOW_LABEL,
     i18n.get(SocialMediaGalleryConstants.SOCIAL_MEDIA_GALLERY_VIEW_PRODUCT_BUY_IT_NOW));
   if (null != associatedPage) {
    socialCarouselMap.put(SocialMediaGalleryConstants.ASSOCIATED_PAGE, getProductMap(associatedPage, resources));
   } else {
    socialCarouselMap.put(SocialMediaGalleryConstants.ASSOCIATED_PAGE, productMap);
   }
  } else {
   socialCarouselMap.put(SocialMediaGalleryConstants.ASSOCIATED_PAGE, getArticleMap(associatedPage, resources));
  }
 }
 
 /**
  * Sets the teaser details.
  *
  * @param socialCarouselMap
  *         the social carousel map
  * @param pageProperties
  *         the page properties
  */
 private void setTeaserDetails(Map<String, Object> socialCarouselMap, Page articlePage) {
  if (articlePage != null) {
   Map<String, Object> pageProperties = articlePage.getProperties();
   socialCarouselMap.put(SocialMediaGalleryConstants.CUSTOM_LINK_TITLE,
     MapUtils.getString(pageProperties, SocialMediaGalleryConstants.CUSTOM_LINK_TITLE, StringUtils.EMPTY));
   socialCarouselMap.put(SocialMediaGalleryConstants.CUSTOM_LINK_DESCRIPTION,
     MapUtils.getString(pageProperties, SocialMediaGalleryConstants.CUSTOM_LINK_DESCRIPTION, StringUtils.EMPTY));
  }
 }
 
 /**
  * Gets the article map.
  *
  * @param articlePage
  *         the article page
  * @param resources
  *         the resources
  * @return the article map
  */
 private Map<String, Object> getArticleMap(Page articlePage, Map<String, Object> resources) {
  Map<String, Object> articleMap = new HashMap<String, Object>();
  if (null != articlePage) {
   Map<String, Object> pageProperties = articlePage.getProperties();
   String imagePath = MapUtils.getString(pageProperties, SocialMediaGalleryConstants.TEASER_IMAGE, StringUtils.EMPTY);
   String altText = MapUtils.getString(pageProperties, SocialMediaGalleryConstants.TEASER_ALT_TEXT, StringUtils.EMPTY);
   String title = MapUtils.getString(pageProperties, SocialMediaGalleryConstants.TEASER_TITLE, StringUtils.EMPTY);
   Image image = new Image(imagePath, altText, title, articlePage, getSlingRequest(resources));
   articleMap.put(SocialMediaGalleryConstants.IMAGE, image.convertToMap());
   articleMap.put(SocialMediaGalleryConstants.NAME, articlePage.getName());
  }
  return articleMap;
 }
 
 /**
  * Gets the included social channel map.
  *
  * @param currentResource
  *         the current resource
  * @param resources
  *         the resources
  * @param page
  *         the page
  * @param channnelName
  *         the channnel name
  * @return the included social channel map
  */
 private List<Map<String, Object>> getIncludedSocialChannelMap(Resource currentResource, Map<String, Object> resources, Page page,
   String channnelName) {
  
  List<Map<String, Object>> linkPostsListFinalMap = new ArrayList<Map<String, Object>>();
  List<Map<String, String>> linkPostsList = ComponentUtil.getNestedMultiFieldProperties(currentResource,
    channnelName + SocialMediaGalleryConstants.LINK_POST_CONFIG_JSON);
  for (Map<String, String> map : linkPostsList) {
   Map<String, Object> linkPostToContentProperties = new LinkedHashMap<String, Object>();
   linkPostToContentProperties.put(SocialMediaGalleryConstants.SOCIAL_CHANNELS, WordUtils.capitalize(channnelName));
   if (StringUtils.equals(channnelName, SocialMediaGalleryConstants.FIXED_LIST)) {
    String fixedChannelType = MapUtils.getString(map, channnelName + SocialMediaGalleryConstants.CHANNEL_TYPE, StringUtils.EMPTY);
    linkPostToContentProperties.put(SocialMediaGalleryConstants.SOCIAL_CHANNELS,
      WordUtils.capitalize(fixedChannelType));
	   //Add tab Image
       if(fixedChannelType.equals(SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_LOCAL_ASSET)){
        String tabImagePath = MapUtils.getString(map, SocialMediaGalleryConstants.FIXED_TAB_IMAGE, StringUtils.EMPTY);
        String tabImgaeAlt = MapUtils.getString(map, SocialMediaGalleryConstants.FIXED_TAB_IMAGE_ALT, StringUtils.EMPTY);
        Image tabImage = new Image(tabImagePath, tabImgaeAlt, StringUtils.EMPTY, page, getSlingRequest(resources));
        linkPostToContentProperties.put(SocialMediaGalleryConstants.TAB_IMAGE, tabImage.convertToMap());
       }
   }
   linkPostToContentProperties.put(SocialMediaGalleryConstants.POST_ID,
     MapUtils.getString(map, channnelName + WordUtils.capitalize(SocialMediaGalleryConstants.POST_ID), StringUtils.EMPTY));
   linkPostToContentProperties.put(SocialMediaGalleryConstants.POST_POSITION,
     MapUtils.getString(map, channnelName + WordUtils.capitalize(SocialMediaGalleryConstants.POST_POSITION), StringUtils.EMPTY));
   linkPostToContentProperties.put(SocialMediaGalleryConstants.POST_CLASS,
     MapUtils.getString(map, channnelName + WordUtils.capitalize(SocialMediaGalleryConstants.POST_CLASS), StringUtils.EMPTY));
   boolean isLinkToPage = false;
   if (StringUtils.equals(SocialMediaGalleryConstants.TRUE,
     MapUtils.getString(map, channnelName + SocialMediaGalleryConstants.LINK_POST_TO_PRODUCT_OR_PAGE, SocialMediaGalleryConstants.FALSE))) {
    isLinkToPage = true;
   }
   linkPostToContentProperties.put(SocialMediaGalleryConstants.IS_LINK_TO_PAGE, isLinkToPage);
   if (isLinkToPage) {
    boolean openInNewWindow = false;
    if (StringUtils.equals(SocialMediaGalleryConstants.TRUE, MapUtils.getString(map,
      channnelName + WordUtils.capitalize(SocialMediaGalleryConstants.OPEN_IN_NEW_WINDOW), SocialMediaGalleryConstants.FALSE))) {
     openInNewWindow = true;
    }
    linkPostToContentProperties.put(SocialMediaGalleryConstants.OPEN_IN_NEW_WINDOW, openInNewWindow);
    String pagePath = MapUtils.getString(map, channnelName + SocialMediaGalleryConstants.LINK_POST_TO, StringUtils.EMPTY);
    Page associatedPage = page.getPageManager().getPage(pagePath);
    
    String linkType = MapUtils.getString(map, channnelName + WordUtils.capitalize(SocialMediaGalleryConstants.LINK_TYPE), StringUtils.EMPTY);
    linkPostToContentProperties.put(SocialMediaGalleryConstants.LINK_TYPE, linkType);
    if (StringUtils.equals(SocialMediaGalleryConstants.CUSTOM_INTERNAL, linkType)
      || StringUtils.equals(SocialMediaGalleryConstants.CUSTOM_EXTERNAL, linkType)) {
     linkPostToContentProperties.put(SocialMediaGalleryConstants.GENERAL_CONFIG_HEADING_TEXT,
       MapUtils.getString(map, channnelName + SocialMediaGalleryConstants.CUSTOM_HEADING_TEXT, StringUtils.EMPTY));
     linkPostToContentProperties.put(SocialMediaGalleryConstants.GENERAL_CONFIG_CTA_LABEL,
       MapUtils.getString(map, channnelName + SocialMediaGalleryConstants.CUSTOM_CTA_LABEL, StringUtils.EMPTY));
     if (StringUtils.equals(SocialMediaGalleryConstants.CUSTOM_EXTERNAL, linkType)) {
      String teaserTitle = MapUtils.getString(map, channnelName + SocialMediaGalleryConstants.CUSTOM_TEASER_TITLE, StringUtils.EMPTY);
      linkPostToContentProperties.put(SocialMediaGalleryConstants.CUSTOM_LINK_TITLE, teaserTitle);
      linkPostToContentProperties.put(SocialMediaGalleryConstants.CUSTOM_LINK_DESCRIPTION,
        MapUtils.getString(map, channnelName + SocialMediaGalleryConstants.CUSTOM_TEASER_DESCRIPTION, StringUtils.EMPTY));
      String thumbImagePath = MapUtils.getString(map, channnelName + SocialMediaGalleryConstants.CUSTOM_TEASER_IMAGE, StringUtils.EMPTY);
      String imageAltText = MapUtils.getString(map, channnelName + SocialMediaGalleryConstants.CUSTOM_TEASER_IMAGE_ALT, StringUtils.EMPTY);
      Image image = new Image(thumbImagePath, imageAltText, teaserTitle, associatedPage, getSlingRequest(resources));
      Map<String, Object> associateMap = new HashMap<String, Object>();
      associateMap.put(SocialMediaGalleryConstants.IMAGE, image.convertToMap());
      associateMap.put(SocialMediaGalleryConstants.NAME, StringUtils.EMPTY);
      linkPostToContentProperties.put(SocialMediaGalleryConstants.ASSOCIATED_PAGE, associateMap);
     }
    }
    if (!StringUtils.equals(SocialMediaGalleryConstants.CUSTOM_EXTERNAL, linkType)) {
     addCTAMap(linkPostToContentProperties, linkType, pagePath, associatedPage, getI18n(resources), resources);
    }
   }
   if (StringUtils.equals(SocialMediaGalleryConstants.GENERAL_CONFIG_CHANNEL_LOCAL_ASSET, channnelName)
     || StringUtils.equals(SocialMediaGalleryConstants.FIXED_LIST, channnelName)) {
    setLocalContentConfiguration(linkPostToContentProperties, page, resources, map, channnelName);
   }
   if (StringUtils.equals(SocialMediaGalleryConstants.FIXED_LIST, channnelName)) {
    setFixedListConfiguration(linkPostToContentProperties, map);
   }
   linkPostsListFinalMap.add(linkPostToContentProperties);
  }
  return linkPostsListFinalMap;
 }
 
 private void setFixedListConfiguration(Map<String, Object> linkPostToContentProperties, Map<String, String> map) {
  boolean addCustomTitleAndDesc = false;
  if (StringUtils.equals(SocialMediaGalleryConstants.TRUE,
    MapUtils.getString(map, SocialMediaGalleryConstants.FIXED_POST_ADD_CUSTOM_TITLE_AND_DESC, SocialMediaGalleryConstants.FALSE))) {
   addCustomTitleAndDesc = true;
  }
  linkPostToContentProperties.put(SocialMediaGalleryConstants.ADD_CUSTOM_TITLE_DESC, addCustomTitleAndDesc);
  if (addCustomTitleAndDesc) {
   linkPostToContentProperties.put(SocialMediaGalleryConstants.FIXED_POST_TITLE,
     MapUtils.getString(map, SocialMediaGalleryConstants.FIXED_POST_TITLE, StringUtils.EMPTY));
   linkPostToContentProperties.put(SocialMediaGalleryConstants.FIXED_POST_SHORT_DESCRIPTION,
     MapUtils.getString(map, SocialMediaGalleryConstants.FIXED_POST_SHORT_DESCRIPTION, StringUtils.EMPTY));
   linkPostToContentProperties.put(SocialMediaGalleryConstants.FIXED_POST_LONG_DESCRIPTION,
     MapUtils.getString(map, SocialMediaGalleryConstants.FIXED_POST_LONG_DESCRIPTION, StringUtils.EMPTY));
  }
 }
 
 /**
  * Sets the local content configuration.
  *
  * @param linkPostToContentProperties
  *         the link post to content properties
  * @param page
  *         the page
  * @param resources
  *         the resources
  * @param map
  *         the map
  */
 private void setLocalContentConfiguration(Map<String, Object> linkPostToContentProperties, Page page, Map<String, Object> resources,
   Map<String, String> map, String channelType) {
  boolean isChannelImage = false;
  if (StringUtils.equals(SocialMediaGalleryConstants.TRUE, MapUtils.getString(map,
    channelType + WordUtils.capitalize(SocialMediaGalleryConstants.SET_CUSTOM_CHANNEL_IMAGE), SocialMediaGalleryConstants.FALSE))) {
   isChannelImage = true;
  }
  linkPostToContentProperties.put(SocialMediaGalleryConstants.IS_CHANNEL_IMAGE, isChannelImage);
  if (isChannelImage) {
   String channelImagePath = MapUtils.getString(map, channelType + WordUtils.capitalize(SocialMediaGalleryConstants.CHANNEL_IMAGE),
     StringUtils.EMPTY);
   String channelImgAltText = MapUtils.getString(map, channelType + WordUtils.capitalize(SocialMediaGalleryConstants.CHANNEL_IMAGE_ALT),
     StringUtils.EMPTY);
   Image image = new Image(channelImagePath, channelImgAltText, StringUtils.EMPTY, page, getSlingRequest(resources));
   linkPostToContentProperties.put(SocialMediaGalleryConstants.CHANNEL_IMAGE, image.convertToMap());
  }
  // set tab image.
  if (!StringUtils.equals(SocialMediaGalleryConstants.FIXED_LIST, channelType)) {
   String tabImagePath = MapUtils.getString(map, SocialMediaGalleryConstants.TAB_IMAGE, StringUtils.EMPTY);
   Image tabImage = new Image(tabImagePath, StringUtils.EMPTY, StringUtils.EMPTY, page, getSlingRequest(resources));
   linkPostToContentProperties.put(SocialMediaGalleryConstants.TAB_IMAGE, tabImage.convertToMap());
  }
 }
 
 /**
  * Gets the filter config tab data.
  *
  * @param properties
  *         the properties
  * @param i18n
  *         the i18n
  * @param currentResource
  *         the current resource
  * @return the filter config tab data
  */
 private Map<String, Object> getFilterConfigTabData(Map<String, Object> properties, I18n i18n, Resource currentResource) {
  Map<String, Object> filterConfigDataMap = new LinkedHashMap<String, Object>();
  
  filterConfigDataMap.put(SocialMediaGalleryConstants.SEARCH_HEADING,
    MapUtils.getString(properties, SocialMediaGalleryConstants.SEARCH_HEADING, StringUtils.EMPTY));
  
  filterConfigDataMap.put(SocialMediaGalleryConstants.SEARCH_DESCRIPTION,
    MapUtils.getString(properties, SocialMediaGalleryConstants.SEARCH_DESCRIPTION, StringUtils.EMPTY));
  
  filterConfigDataMap.put(SocialMediaGalleryConstants.SEARCH_PLACEHOLDER,
    MapUtils.getString(properties, SocialMediaGalleryConstants.SEARCH_PLACEHOLDER, StringUtils.EMPTY));
  
  filterConfigDataMap.put(SocialMediaGalleryConstants.FILTER_HEADING,
    MapUtils.getString(properties, SocialMediaGalleryConstants.FILTER_HEADING, StringUtils.EMPTY));
  
  filterConfigDataMap.put(SocialMediaGalleryConstants.FILTER_DESCRIPTION,
    MapUtils.getString(properties, SocialMediaGalleryConstants.FILTER_DESCRIPTION, StringUtils.EMPTY));
  
  filterConfigDataMap.put(SocialMediaGalleryConstants.ENABLE_COLLAPSE_FUNCTION,
    MapUtils.getBoolean(properties, SocialMediaGalleryConstants.ENABLE_COLLAPSE_FUNCTION, false));
  
  filterConfigDataMap.put(SocialMediaGalleryConstants.ENABLE_SEARCH_INPUT,
    MapUtils.getBoolean(properties, SocialMediaGalleryConstants.ENABLE_SEARCH_INPUT, false));
  
  filterConfigDataMap.put(SocialMediaGalleryConstants.FILTER_SEARCH_PLACEHOLDER,
    MapUtils.getString(properties, SocialMediaGalleryConstants.FILTER_SEARCH_PLACEHOLDER, StringUtils.EMPTY));
  
  filterConfigDataMap.put(SocialMediaGalleryConstants.DESCRIPTION_READ_MORE_LABEL,
    i18n.get(SocialMediaGalleryConstants.SOCIAL_MEDIA_GALLERY_DESCRIPTION_READ_MORE_LABEL));
  filterConfigDataMap.put(SocialMediaGalleryConstants.DESCRIPTION_READ_LESS_LABEL,
    i18n.get(SocialMediaGalleryConstants.SOCIAL_MEDIA_GALLERY_DESCRIPTION_READ_LESS_LABEL));
  filterConfigDataMap.put(SocialMediaGalleryConstants.FILTER_RESET_FILTER_LABEL,
    i18n.get(SocialMediaGalleryConstants.SOCIAL_MEDIA_GALLERY_FILTER_RESET_FILTER_LABEL));
  filterConfigDataMap.put(SocialMediaGalleryConstants.FILTER_SUBMIT_LABEL,
    i18n.get(SocialMediaGalleryConstants.SOCIAL_MEDIA_GALLERY_FILTER_SUBMIT_LABEL));
  filterConfigDataMap.put(SocialMediaGalleryConstants.SEARCH_RESULT_HEADLINE,
    i18n.get(SocialMediaGalleryConstants.SOCIAL_MEDIA_GALLERY_SEARCH_RESULTS_HEADLINE));
  filterConfigDataMap.put(SocialMediaGalleryConstants.NO_SEARCH_RESULT_HEADLINE,
    i18n.get(SocialMediaGalleryConstants.SOCIAL_MEDIA_GALLERY_NO_SEARCH_RESULT_HEADLINE));
  filterConfigDataMap.put(SocialMediaGalleryConstants.NO_SEARCH_RESULT_MESSAGE,
    i18n.get(SocialMediaGalleryConstants.SOCIAL_MEDIA_GALLERY_NO_SEARCH_RESULT_MESSAGE));
  filterConfigDataMap.put(SocialMediaGalleryConstants.FILTER_TITLE_TEXT,
    i18n.get(SocialMediaGalleryConstants.SOCIAL_MEDIA_GALLERY_FILTER_TITLE_TEXT));
  filterConfigDataMap.put(SocialMediaGalleryConstants.FILTER_SHOW_MORE_LABEL,
    i18n.get(SocialMediaGalleryConstants.SOCIAL_MEDIA_GALLERY_FILTER_SHOW_MORE_LABEL));
  filterConfigDataMap.put(SocialMediaGalleryConstants.FILTER_SHOW_LESS_LABEL,
    i18n.get(SocialMediaGalleryConstants.SOCIAL_MEDIA_GALLERY_FILTER_SHOW_LESS_LABEL));
  
  List<Map<String, String>> list = ComponentUtil.getNestedMultiFieldProperties(currentResource, SocialMediaGalleryConstants.KEYWORD_FILTERS);
  
  filterConfigDataMap.put(SocialMediaGalleryConstants.KEYWORD_FILTERS, list.toArray());
  return filterConfigDataMap;
 }
}
