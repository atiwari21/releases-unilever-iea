/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.search.constants.SearchConstants;
import com.sapient.platform.iea.aem.core.search.input.vo.PropertyInput;
import com.sapient.platform.iea.aem.core.search.input.vo.SearchInput;
import com.sapient.platform.iea.aem.core.search.input.vo.TagInput;
import com.sapient.platform.iea.aem.core.search.result.vo.Hits;
import com.sapient.platform.iea.aem.core.search.result.vo.Result;
import com.sapient.platform.iea.aem.core.search.utils.QueryMapGenerator;
import com.sapient.platform.iea.aem.core.search.utils.enums.SearchInputUtilsEnums.JoinOperator;
import com.sapient.platform.iea.aem.core.search.utils.enums.SearchInputUtilsEnums.SortOrder;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.sapient.platform.iea.aem.search.service.api.SearchService;
import com.sapient.platform.iea.aem.utils.ContentDetailFetchUtil;
import com.sapient.platform.iea.aem.utils.constants.CommonConstants;
import com.sapient.platform.iea.aem.viewhelper.constants.ContentResultsGridConstants;
import com.sapient.platform.iea.core.exception.SystemException;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * <p>
 * Responsible for creating the Content Results Grid and reading the hits page paths to get the relevant data to show it on the front end..
 * </p>
 */
@Component(description = "ContentResultsGridViewHelperImpl", immediate = true, metatype = true, label = "ContentResultsGridViewHelperImpl")
@Service(value = { ContentResultsGridViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.CONTENT_RESULTS_GRID, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class ContentResultsGridViewHelperImpl extends BaseViewHelper {
 /**
  * Identifier to hold constant integer 2.
  */
 private static final int TWO = 2;
 /**
  * Identifier to hold the contentType key.
  */
 public static final String CONTENT_TYPE = "contentType";
 /**
  * Identifier to hold the limit key.
  */
 public static final String LIMIT = "limit";
 /**
  * Constant to hold the SORT_OPTIONS identifier.
  */
 public static final String SORT_OPTIONS = "sortOptions";
 /**
  * Constant to for DO_LAZY_LOAD identifier.
  */
 public static final String DO_LAZY_LOAD = "doLazyLoad";
 /**
  * Constant to for FIELD identifier.
  */
 public static final String FIELD = "field";
 /**
  * Constant to for TOTAL_RESULTS identifier.
  */
 public static final String TOTAL_RESULTS = "totalResults";
 /**
  * Constant to for SORTING_REQUIRED identifier.
  */
 public static final String SORTING_REQUIRED = "sortingRequired";
 /**
  * Constant to for SORT_DEFAULT_TEXT identifier.
  */
 public static final String SORT_DEFAULT_TEXT = "sortDefaultText";
 /**
  * Constant to for RESULT_COUNT identifier.
  */
 public static final String RESULT_COUNT = "resultCount";
 /**
  * Constant to for NUMBER_OF_RESULTS identifier.
  */
 public static final String NUMBER_OF_RESULTS = "noOfResults";
 /**
  * Reference object for searchService.
  */
 @Reference
 private SearchService searchService;
 /**
  * Logger for the ListViewHelperImpl.
  */
 private static final Logger LOGGER = LoggerFactory.getLogger(ContentResultsGridViewHelperImpl.class);
 /**
  * Constant for Image details label.
  */
 private static final String IMAGE_DETAIL = "imageDetail";
 
 /**
  * Constant for ONE.
  */
 private static final int ONE = 1;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.sapient.platform.iea.aem.viewhelper.ContentTypeListViewHelper# processData(java.util.Map, java.util.Map)
  */
 @SuppressWarnings("unchecked")
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  long startTime = System.currentTimeMillis();
  LOGGER.info("ContentResultsGrid View Helper #processData called for processing.");
  Map<String, Object> returnMap = new LinkedHashMap<String, Object>(TWO * CommonConstants.ARRAY_INIT);
  Map<String, Object> finalMap = new LinkedHashMap<String, Object>(TWO * CommonConstants.ARRAY_INIT);
  Map<String, Object> properties = (Map<String, Object>) content.get(PROPERTIES);
  
  String jsonNameSpace = MapUtils.getString(content, "jsonNameSpace");
  
  List<Map<String, Object>> contentsHitList = new ArrayList<Map<String, Object>>();
  Result results = null;
  String contentTypeProperty = null;
  String[] selectorArray = (String[]) content.get(SELECTOR_ARRAY);
  String limit = "20";
  String resultCount = "20";
  int fixedListSize = 0;
  String limitChildren = (String) properties.get(LIMIT + "_" + ContentResultsGridConstants.LIST_CHILDREN);
  String limitTags = (String) properties.get(LIMIT + "_" + ContentResultsGridConstants.LIST_TAGS);
  
  limit = getLimit(properties, limit, limitChildren, limitTags);
  if (!(selectorArray.length >= TWO && ("all").equals(selectorArray[1])) && MapUtils.getBooleanValue(properties, DO_LAZY_LOAD, false)) {
   resultCount = limit;
   limit = MapUtils.getString(properties, NUMBER_OF_RESULTS);
   
  }
  returnMap.put("configurations", setConfigurations(properties));
  if (ContentResultsGridConstants.LIST_CHILDREN.equals(properties.get(ContentResultsGridConstants.LIST_FROM))
    || ContentResultsGridConstants.LIST_TAGS.equals(properties.get(ContentResultsGridConstants.LIST_FROM))) {
   
   LOGGER.debug("component configured for children and tag based list");
   
   String[] tags = (String[]) properties.get(ContentResultsGridConstants.LIST_TAGS);
   
   results = getResults(content, resources, limit, returnMap);
   contentsHitList = generateData(results, content, resources);
   contentTypeProperty = getContentTypeProperty(returnMap, properties, results, tags);
   
  } else {
   LOGGER.debug("component configured for fixed list");
   List<String> linksList = new ArrayList<String>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
   putTotalResults(returnMap, properties, linksList);
   if (selectorArray.length >= TWO && ("all").equals(selectorArray[1])) {
    contentsHitList = getChildPagesContentMapList(content, resources, linksList);
   }
   
   if (MapUtils.getBooleanValue(properties, DO_LAZY_LOAD, false)) {
    List<String> tempList = new ArrayList<String>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
    limit = MapUtils.getString(properties, NUMBER_OF_RESULTS);
    int listSize = linksList.size();
    fixedListSize = listSize;
    if (Integer.valueOf(limit) > listSize) {
     limit = String.valueOf(listSize);
    }
    prepareTempList(limit, linksList, tempList);
    contentsHitList = getChildPagesContentMapList(content, resources, tempList);
   } else {
    contentsHitList = getChildPagesContentMapList(content, resources, linksList);
   }
   
  }
  
  returnMap.put("contentResults", contentsHitList);
  putResultCount(returnMap, properties, contentsHitList, results, selectorArray, resultCount, fixedListSize);
  returnMap.put("showPrefixText", properties.get("showPrefixText"));
  returnMap.put("showPostfixText", properties.get("showPostfixText"));
  returnMap.put(CONTENT_TYPE, contentTypeProperty);
  
  finalMap.put(jsonNameSpace, returnMap);
  LOGGER.debug("Content results:-" + finalMap);
  LOGGER.error("execution time (milliseconds) taken by method processData with args [Map content, Map resources] "
    + "in class ContentResultsGridViewHelperImpl, is:" + (System.currentTimeMillis() - startTime));
  return finalMap;
 }
 
 /**
  * This method puts linksList into tempList.
  * 
  * @param limit
  * @param linksList
  * @param tempList
  */
 private void prepareTempList(String limit, List<String> linksList, List<String> tempList) {
  for (int i = 0; i < Integer.valueOf(limit); i++) {
   tempList.add(linksList.get(i));
  }
 }
 
 /**
  * This method puts value of total results into returnMap.
  * 
  * @param returnMap
  * @param properties
  * @param linksList
  */
 private void putTotalResults(Map<String, Object> returnMap, Map<String, Object> properties, List<String> linksList) {
  // tempLinksList has been introduced to avoid SONAR issue.
  List<String> tempLinksList = linksList;
  if (properties.get(ContentResultsGridConstants.PAGES) != null) {
   if (properties.get(ContentResultsGridConstants.PAGES) instanceof String) {
    tempLinksList.add((String) properties.get(ContentResultsGridConstants.PAGES));
   } else {
    tempLinksList = Arrays.asList((String[]) properties.get(ContentResultsGridConstants.PAGES));
   }
   returnMap.put(TOTAL_RESULTS, tempLinksList.size());
  }
 }
 
 /**
  * This method returns the property content type.
  * 
  * @param returnMap
  * @param properties
  * @param results
  * @param tags
  * @return contentTypeProperty
  */
 private String getContentTypeProperty(Map<String, Object> returnMap, Map<String, Object> properties, Result results, String[] tags) {
  String contentTypeProperty;
  if (ContentResultsGridConstants.LIST_CHILDREN.equals(properties.get(ContentResultsGridConstants.LIST_FROM))) {
   returnMap.put(TOTAL_RESULTS, results.getTotalMatches());
   if (properties.get(CONTENT_TYPE + "_" + ContentResultsGridConstants.LIST_CHILDREN) instanceof String) {
    contentTypeProperty = (String) properties.get(CONTENT_TYPE + "_" + ContentResultsGridConstants.LIST_CHILDREN);
   } else {
    contentTypeProperty = Arrays.toString((String[]) properties.get(CONTENT_TYPE + "_" + ContentResultsGridConstants.LIST_CHILDREN));
   }
  } else {
   if (tags != null && tags.length > 0) {
    putTotalResults(returnMap, results);
    
   } else {
    returnMap.put(TOTAL_RESULTS, 0);
   }
   if (properties.get(CONTENT_TYPE + "_" + ContentResultsGridConstants.LIST_TAGS) instanceof String) {
    contentTypeProperty = (String) properties.get(CONTENT_TYPE + "_" + ContentResultsGridConstants.LIST_TAGS);
   } else {
    contentTypeProperty = Arrays.toString((String[]) properties.get(CONTENT_TYPE + "_" + ContentResultsGridConstants.LIST_TAGS));
   }
  }
  return contentTypeProperty;
 }
 
 /**
  * This method computes limit based on list type.
  * 
  * @param properties
  * @param limit
  * @param limitChildren
  * @param limitTags
  * @return tempLimit
  */
 private String getLimit(Map<String, Object> properties, String limit, String limitChildren, String limitTags) {
  // tempLimit has been introduced to avoid SONAR issue.
  String tempLimit = limit;
  if (ContentResultsGridConstants.LIST_CHILDREN.equals(properties.get(ContentResultsGridConstants.LIST_FROM))) {
   if (limitChildren != null && !limitChildren.trim().isEmpty()) {
    tempLimit = (String) limitChildren;
   }
  } else if (ContentResultsGridConstants.LIST_TAGS.equals(properties.get(ContentResultsGridConstants.LIST_FROM)) && limitTags != null
    && !limitTags.trim().isEmpty()) {
   tempLimit = (String) limitTags;
  }
  return tempLimit;
 }
 
 /**
  * This method puts total result in returnMap.
  * 
  * @param returnMap
  * @param results
  */
 private void putTotalResults(Map<String, Object> returnMap, Result results) {
  if (null != results) {
   returnMap.put(TOTAL_RESULTS, results.getTotalMatches());
  } else {
   LOGGER.error(" Results obtained based on tags are null. ");
  }
 }
 
 /**
  * This method puts value of result count into returnMap.
  * 
  * @param returnMap
  * @param properties
  * @param contentsHitList
  * @param results
  * @param selectorArray
  * @param resultCount
  * @param fixedListSize
  */
 private void putResultCount(Map<String, Object> returnMap, Map<String, Object> properties, List<Map<String, Object>> contentsHitList,
   Result results, String[] selectorArray, String resultCount, int fixedListSize) {
  if (CollectionUtils.isEmpty(contentsHitList)) {
   returnMap.put(RESULT_COUNT, "0");
  } else {
   if (!(selectorArray.length >= TWO && ("all").equals(selectorArray[1]))) {
    if (MapUtils.getBooleanValue(properties, DO_LAZY_LOAD, false)) {
     putResultCountIfLazyLoad(returnMap, properties, results, resultCount, fixedListSize);
    } else {
     returnMap.put(RESULT_COUNT, contentsHitList.size());
    }
   } else {
    returnMap.put(RESULT_COUNT, contentsHitList.size());
   }
  }
 }
 
 /**
  * @param returnMap
  * @param properties
  * @param results
  * @param resultCount
  * @param fixedListSize
  */
 private void putResultCountIfLazyLoad(Map<String, Object> returnMap, Map<String, Object> properties, Result results, String resultCount,
   int fixedListSize) {
  if (!ContentResultsGridConstants.LIST_FIXED.equals(properties.get(ContentResultsGridConstants.LIST_FROM))) {
   if (Long.parseLong(resultCount) > results.getTotalMatches()) {
    returnMap.put(RESULT_COUNT, results.getTotalMatches());
   } else {
    returnMap.put(RESULT_COUNT, resultCount);
   }
  } else {
   if (Integer.parseInt(resultCount) > fixedListSize) {
    returnMap.put(RESULT_COUNT, fixedListSize);
   } else {
    returnMap.put(RESULT_COUNT, resultCount);
   }
  }
 }
 
 /**
  * This method is used to set the static data configurations in a map which is then sent in response.
  * 
  * @param properties
  *         The properties object.
  * @return Map of String,Object
  */
 protected Map<String, Object> setConfigurations(final Map<String, Object> properties) {
  LOGGER.info("Setting up the static configurations..");
  Map<String, Object> configurationsMap = new LinkedHashMap<String, Object>(TWO * CommonConstants.ARRAY_INIT);
  
  configurationsMap.put(ContentResultsGridConstants.RSLT_LABEL, (String) properties.get(ContentResultsGridConstants.RSLT_LABEL));
  configurationsMap.put(ContentResultsGridConstants.VIEW_FORMAT, (String) properties.get(ContentResultsGridConstants.VIEW_FORMAT));
  configurationsMap.put("pageSize", (String) properties.get(NUMBER_OF_RESULTS));
  configurationsMap.put("lazyload", MapUtils.getBoolean(properties, DO_LAZY_LOAD, false));
  configurationsMap.put("noResultCopy", (String) properties.get("noResultsText"));
  configurationsMap.put(ContentResultsGridConstants.SORT_REQUIRED, MapUtils.getBoolean(properties, SORTING_REQUIRED, false));
  if (MapUtils.getBoolean(properties, SORTING_REQUIRED, false)) {
   
   configurationsMap.put(ContentResultsGridConstants.SORT_LABEL, (String) properties.get(ContentResultsGridConstants.SORT_LABEL));
   configurationsMap.put("defaultSortOption", (String) properties.get(SORT_DEFAULT_TEXT));
   List<Map<String, String>> sortOptions = getSortOptionsConfig(properties);
   configurationsMap.put(ContentResultsGridConstants.SORT_OPTIONS, sortOptions);
   
  }
  
  LOGGER.debug("The configuration map generated is :-" + configurationsMap);
  
  return configurationsMap;
  
 }
 
 /**
  * This method prepares the sort options configurations and send it across to the calling method. These options would be used on the front end to
  * build up the sort by drop down.
  * 
  * @param properties
  *         The properties array.
  * @return the sortOptions map.
  */
 private List<Map<String, String>> getSortOptionsConfig(final Map<String, Object> properties) {
  List<Map<String, String>> sortOptions = new ArrayList<Map<String, String>>(TWO * CommonConstants.ARRAY_INIT);
  
  List<String> propertyNames = new ArrayList<String>();
  propertyNames.add(SORT_OPTIONS);
  propertyNames.add("sortDisplayValue");
  propertyNames.add("fieldValue");
  List<Map<String, String>> sortOptionsList = com.sapient.platform.iea.aem.core.utils.CollectionUtils.convertMultiWidgetToList(properties,
    propertyNames);
  
  LOGGER.info("Populating the sorting information as sorting is required..");
  // logic to populate the values. Text value entered for options
  // takes precedence.
  Iterator<Map<String, String>> iterator = sortOptionsList.iterator();
  
  while (iterator.hasNext()) {
   Map<String, String> sortOptionsMap = new LinkedHashMap<String, String>(TWO * CommonConstants.ARRAY_INIT);
   Map<String, String> tempMap = iterator.next();
   if (("numasc").equalsIgnoreCase(tempMap.get(SORT_OPTIONS))) {
    sortOptionsMap.put(CommonConstants.KEY, "asc");
   } else if (("numdesc").equalsIgnoreCase(tempMap.get(SORT_OPTIONS))) {
    sortOptionsMap.put(CommonConstants.KEY, "desc");
   } else {
    sortOptionsMap.put(CommonConstants.KEY, tempMap.get(SORT_OPTIONS));
   }
   sortOptionsMap.put("value", tempMap.get("sortDisplayValue"));
   // Added field property for sorting options
   sortOptionsMap.put(FIELD, tempMap.get("fieldValue"));
   if (!(StringUtils.isEmpty(sortOptionsMap.get("value")) || StringUtils.isEmpty(sortOptionsMap.get(FIELD)))) {
    sortOptions.add(sortOptionsMap);
   }
  }
  
  LOGGER.debug("The sort options are:- " + sortOptions);
  return sortOptions;
 }
 
 /**
  * This method creates the search input from selectors/component properties. The searched results are populated into a list of maps and returned to
  * the calling function.
  * 
  * @param content
  *         {@link Map} of all user input received as input.
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @param limit
  * @param tagsArray
  *         An array of tags as given in input. For e.g. Sakonnet:redWine.
  * @return A list of maps containing the contents associated with the hits.
  */
 protected Result getResults(final Map<String, Object> content, final Map<String, Object> resources, final String limit, Map<String, Object> returnMap) {
  
  @SuppressWarnings("unchecked")
  Map<String, Object> properties = (Map<String, Object>) content.get("properties");
  SearchInput searchInput = new SearchInput();
  SlingHttpServletRequest request = (SlingHttpServletRequest) resources.get("request");
  ResourceResolver resourceResolver = request.getResourceResolver();
  String defaultSearchBasePath = setDefaultSearchBasePath(properties, request);
  LOGGER.debug("The default search path comes out to be :- " + defaultSearchBasePath);
  // setting of tags
  if (ContentResultsGridConstants.LIST_TAGS.equals(properties.get("listFrom"))) {
   searchInput = setSearchInputForTags(properties, searchInput);
  }
  
  List<String> contentType = setContentType(properties);
  LOGGER.debug("Content Type is set to : - " + contentType);
  searchInput.setPath(defaultSearchBasePath);
  
  searchInput.setType(getResourceType());
  searchInput.setLimit(limit);
  if (properties.get(SORTING_REQUIRED) != null) {
   searchInput = setSearchInputForSorting(returnMap, properties, searchInput);
  } else {
   searchInput.setOrderBy("@" + CommonConstants.JCR_CONTENT_IDENTIFIER + SearchConstants.RELEVANCE_ORDER_PROPERTY);
   searchInput.setSortOrder(SortOrder.desc);
  }
  
  // setting all the available content types in or condition.
  setPropertyList(contentType, searchInput);
  LOGGER.info("Inside generateData() method");
  Map<String, String> predicateMap = QueryMapGenerator.getQueryMap(searchInput);
  LOGGER.debug("Predicate Map generated is : " + predicateMap);
  LOGGER.debug("Resource Resolver Passed is : " + resourceResolver);
  Result result = searchService.getSearchResult(predicateMap, resourceResolver);
  LOGGER.info("The searchInput is set correctly. Exiting method getResults.");
  
  return result;
  
 }
 
 /**
  * This method sets content type.
  * 
  * @param properties
  * @return
  */
 private List<String> setContentType(Map<String, Object> properties) {
  List<String> contentType = new ArrayList<String>(TWO * CommonConstants.ARRAY_INIT);
  
  if (ContentResultsGridConstants.LIST_CHILDREN.equals(properties.get(ContentResultsGridConstants.LIST_FROM))) {
   if (properties.get(CONTENT_TYPE + "_" + ContentResultsGridConstants.LIST_CHILDREN) != null) {
    if (properties.get(CONTENT_TYPE + "_" + ContentResultsGridConstants.LIST_CHILDREN) instanceof String) {
     contentType.add((String) properties.get(CONTENT_TYPE + "_" + ContentResultsGridConstants.LIST_CHILDREN));
     
    } else {
     contentType = Arrays.asList((String[]) properties.get(CONTENT_TYPE + "_" + ContentResultsGridConstants.LIST_CHILDREN));
    }
   }
  } else if (ContentResultsGridConstants.LIST_TAGS.equals(properties.get(ContentResultsGridConstants.LIST_FROM))
    && properties.get(CONTENT_TYPE + "_" + ContentResultsGridConstants.LIST_TAGS) != null) {
   if (properties.get(CONTENT_TYPE + "_" + ContentResultsGridConstants.LIST_TAGS) instanceof String) {
    contentType.add((String) properties.get(CONTENT_TYPE + "_" + ContentResultsGridConstants.LIST_TAGS));
    
   } else {
    contentType = Arrays.asList((String[]) properties.get(CONTENT_TYPE + "_" + ContentResultsGridConstants.LIST_TAGS));
   }
  }
  
  return contentType;
 }
 
 /**
  * This method sets the default search base path.
  * 
  * @param properties
  * @param request
  * @return
  */
 public String setDefaultSearchBasePath(Map<String, Object> properties, SlingHttpServletRequest request) {
  
  /**
   * Changed the method to get the current page path(SearchUtils.getCurrentPagePath) , this is done as part of Defect IEA-4527
   **/
  String defaultSearchBasePath = StringUtils.defaultIfEmpty((String) properties.get("parentPage"), getCurrentPagePath(request)).replace("|", "/");
  if (ContentResultsGridConstants.LIST_TAGS.equals(properties.get("listFrom"))) {
   LOGGER.debug("The content results grid is configured for tag based results retrieval");
   /**
    * Changed the method to get the current page path(SearchUtils.getCurrentPagePath) , this is done as part of Defect IEA-4527
    **/
   defaultSearchBasePath = StringUtils.defaultIfEmpty((String) properties.get("tagsSearchRoot"), getCurrentPagePath(request)).replace("|", "/");
   LOGGER.debug("The default path is overriden to :- " + defaultSearchBasePath);
  }
  
  return defaultSearchBasePath;
 }
 
 /**
  * This method sets search input for sorting.
  */
 private SearchInput setSearchInputForSorting(Map<String, Object> returnMap, Map<String, Object> properties, SearchInput searchInput) {
  String order = null;
  String columnToSort = null;
  @SuppressWarnings("unchecked")
  List<Map<String, Object>> sortOptionMap = (List<Map<String, Object>>) ((Map<String, Object>) returnMap.get("configurations")).get("sortOptions");
  if (!sortOptionMap.isEmpty()) {
   if (properties.get(SORT_DEFAULT_TEXT) != null) {
    order = getOrder(properties, order, sortOptionMap);
    columnToSort = getColumnToSort(properties, columnToSort, sortOptionMap);
   } else {
    for (Map<String, Object> item : sortOptionMap) {
     order = (String) item.get("key");
     columnToSort = getColumnToSort(item);
     break;
    }
   }
  } else {
   order = SortOrder.desc.toString();
   columnToSort = SearchConstants.RELEVANCE_ORDER_PROPERTY;
  }
  
  searchInput.setOrderBy("@" + CommonConstants.JCR_CONTENT_IDENTIFIER + columnToSort);
  getSortOrder(searchInput, order);
  return searchInput;
 }
 
 /**
  * This method returns column to sort.
  * 
  * @param properties
  * @param columnToSort
  * @param sortOptionMap
  * @return tempColumnToSort
  */
 private String getColumnToSort(Map<String, Object> properties, String columnToSort, List<Map<String, Object>> sortOptionMap) {
  // tempColumnToSort has been introduced to avoid SONAR issue.
  String tempColumnToSort = columnToSort;
  for (Map<String, Object> item : sortOptionMap) {
   if (item.containsValue(properties.get(SORT_DEFAULT_TEXT).toString().toLowerCase())
     || item.containsValue(properties.get(SORT_DEFAULT_TEXT).toString().toUpperCase())) {
    tempColumnToSort = getColumnToSort(item);
    break;
   }
  }
  return tempColumnToSort;
 }
 
 /**
  * This method returns order.
  * 
  * @param properties
  * @param order
  * @param sortOptionMap
  * @return tempOrder
  */
 private String getOrder(Map<String, Object> properties, String order, List<Map<String, Object>> sortOptionMap) {
  // tempOrder has been introduced to avoid SONAR issue.
  String tempOrder = order;
  for (Map<String, Object> item : sortOptionMap) {
   if (item.containsValue(properties.get(SORT_DEFAULT_TEXT).toString().toLowerCase())
     || item.containsValue(properties.get(SORT_DEFAULT_TEXT).toString().toUpperCase())) {
    tempOrder = (String) item.get("key");
    break;
   }
  }
  return tempOrder;
 }
 
 /**
  * This method sets searchInput for tag based list.
  */
 private SearchInput setSearchInputForTags(Map<String, Object> properties, SearchInput searchInput) {
  // set tags input
  String[] tags = (String[]) properties.get("tags");
  String tagOperator = (String) properties.get("tagsMatch");
  if ((JoinOperator.and.toString()).equals(tagOperator)) {
   LOGGER.debug("The tags would be configured with AND operator");
   searchInput.setTag(new TagInput(getTagProperty(), tags, JoinOperator.and));
  } else {
   LOGGER.debug("The tags would be configured with OR operator");
   searchInput.setTag(new TagInput(getTagProperty(), tags, JoinOperator.or));
  }
  
  return searchInput;
 }
 
 /**
  * @param searchInput
  * @param order
  */
 private void getSortOrder(SearchInput searchInput, String order) {
  if (null != order && order.equalsIgnoreCase(SortOrder.asc.name())) {
   searchInput.setSortOrder(SortOrder.asc);
  } else if (null != order && order.equalsIgnoreCase(SortOrder.desc.name())) {
   searchInput.setSortOrder(SortOrder.desc);
  }
 }
 
 /**
  * @param columnToSort
  * @param item
  * @return
  */
 private String getColumnToSort(Map<String, Object> item) {
  String columnToBeSorted = "";
  if ("titleDetail".equals(item.get(FIELD))) {
   columnToBeSorted = ContentResultsGridConstants.PREFIX_JCR + "title";
  } else if ("shortDescription".equals(item.get(FIELD))) {
   columnToBeSorted = ContentResultsGridConstants.PREFIX_JCR + "description";
  } else if ("price".equals(item.get(FIELD))) {
   columnToBeSorted = "price";
  }
  return columnToBeSorted;
 }
 
 /**
  * This method returns the tag property name which has to be searched.
  * 
  * @return tag property.
  */
 protected String getTagProperty() {
  return ContentResultsGridConstants.TAG_PROPERTY;
 }
 
 /**
  * Method to get the resource type name.
  * 
  * @return resource type.
  */
 protected String getResourceType() {
  return SearchConstants.CQ_PAGE;
 }
 
 /**
  * This method generates the data by first searching the matched paths in the repository against an xpath query and then gets the data in those
  * paths. To-Do :- Add a processing for facet generation once facets filter component approach is finalized.
  * 
  * @param result
  *         The search result for setting the search parameters.
  * @param searchService
  *         The searchAccessor object.
  * @param content
  *         The content containing the dialog and other data.
  * @param resources
  *         The map having the resources.
  * @return the list of map of the generated data.
  */
 protected List<Map<String, Object>> generateData(final Result result, final Map<String, Object> content, final Map<String, Object> resources) {
  
  List<Map<String, Object>> contentsHitList = null;
  if (null != result) {
   List<Hits> hitsList = result.getHits();
   if (org.apache.commons.collections.CollectionUtils.isNotEmpty(hitsList)) {
    // Captured as To-Do
    facetDataProcessor();
    contentsHitList = getChildPagesContentMapList(content, resources, getChildPagesPathList(hitsList));
   }
  }
  
  LOGGER.debug("ContentHitList is : " + contentsHitList);
  
  return contentsHitList;
 }
 
 /**
  * This method iterate on the hit list list and creates a List of string (Paths) out of it.
  * 
  * @param hitsList
  *         The hits list obtained after search/lookup.
  * @return the list containing the paths.
  */
 protected List<String> getChildPagesPathList(final List<Hits> hitsList) {
  List<String> childPagesPathList = new ArrayList<String>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  
  Iterator<Hits> iterator = hitsList.iterator();
  while (iterator.hasNext()) {
   try {
    childPagesPathList.add(iterator.next().getPath());
   } catch (NoSuchElementException e) {
    LOGGER.error("Error encountered while adding hit to list. Please check the hitsList values:- " + hitsList);
    throw new SystemException("The error trace is :- " + e.getMessage() + " Exception caught " + e);
   } catch (RepositoryException e) {
    LOGGER.error("Error encountered while adding hit to list. Please check the hitsList values:- " + hitsList + " Exception caught " + e);
    throw new SystemException("The error trace is :- " + e.getMessage());
   }
   
  }
  LOGGER.debug("The childPages Path List is " + childPagesPathList);
  return childPagesPathList;
 }
 
 /**
  * This method is used to get the individual pages data using the logic written in the content type detail view helper methods. Responsibility :-
  * Fetch the data such as title, CTA link, CTA text, Summary, Image etc from the matched pages list.
  * 
  * @param content
  *         The content map
  * @param resources
  *         The resources map.
  * @param childPagesPathList
  *         The list of paths
  * @return @link Map final data map.
  */
 @SuppressWarnings("unchecked")
 protected List<Map<String, Object>> getChildPagesContentMapList(final Map<String, Object> content, final Map<String, Object> resources,
   final List<String> childPagesPathList) {
  List<Map<String, Object>> childPagesContentMapList = new ArrayList<Map<String, Object>>();
  Map<String, Object> currentPageProperties = new HashMap<String, Object>();
  PageManager pageManager = getPageManager(resources);
  Page currentPage = getCurrentPage(resources);
  
  Map<String, Object> properties = (Map<String, Object>) content.get("properties");
  for (int i = 0; i < childPagesPathList.size(); i++) {
   
   String currentPagePath = childPagesPathList.get(i) + "/jcr:content";
   LOGGER.debug("value of KEY_CURRENT_CONTENT_PAGE_PATH  : ", currentPagePath);
   
   currentPage = pageManager.getContainingPage(currentPagePath);
   currentPageProperties = currentPage.getProperties();
   
   content.put("KEY_CURRENT_CONTENT_PAGE_PATH", currentPagePath);
   content.put("KEY_CURRENT_CONTENT_PARENT_PAGE_PATH", childPagesPathList.get(i));
   
   try {
    ContentDetailFetchUtil.processData(content, resources);
   } catch (UnsupportedEncodingException e) {
    throw new SystemException("Error while fetching details." + e.getMessage() + "exception: " + e);
   }
   List<Map<String, Object>> dataList = (List<Map<String, Object>>) content.get(ContentResultsGridConstants.CONTENT_TYPE_LIST);
   
   if (CollectionUtils.isNotEmpty(dataList) && dataList.get(0).size() > 1) {
    dataList.get(0).put("ctaText", properties.get("ctaText"));
    String thumbnailImagePath = getThumbnailImagePath(resources, dataList);
    if (thumbnailImagePath.isEmpty()) {
     dataList.get(0).put("imageDetail", currentPageProperties.get("teaserImage"));
    }
    LOGGER.debug(new StringBuilder("The data fetched for Page " + i + 1 + "with URL " + childPagesPathList.get(i) + "is : - " + dataList).toString());
    
    childPagesContentMapList.add(dataList.get(0));
   } else {
    LOGGER.info(new StringBuilder("No relevant data is obtained from the hit page. Check configuration for page with URL" + childPagesPathList.get(i)
      + "for any data configuration issues.").toString());
   }
  }
  LOGGER.info("Completed data fetching. Exiting method");
  return childPagesContentMapList;
 }
 
 /**
  * This method returns thumbnail image path if it has been set by page properties. Thumbanil image of any page is stored in CRX in image node under
  * that page node. This image node has property called filename which contains the image name if thumbnail image has been set. This method creates
  * instance of that image node and retrieves the "filename" property. If thumbnail image is not set, then there is no property called "filename" in
  * CRX for image node. In such cases, this method returns null.
  * 
  * @param resources
  *         The resources map.
  * @param dataList
  *         The list of properties of pages.
  * @return thumbnail image path
  */
 private String getThumbnailImagePath(final Map<String, Object> resources, List<Map<String, Object>> dataList) {
  String thumbnailImagePath = StringUtils.EMPTY;
  if (dataList.get(0).get(IMAGE_DETAIL) != null && !"".equals(dataList.get(0).get(IMAGE_DETAIL))) {
   Resource target = getCurrentResource(resources);
   Session session = target.getResourceResolver().adaptTo(Session.class);
   
   try {
    Node node = session.getRootNode();
    Node currentNode = node.getNode(getImageNodePath((String) dataList.get(0).get(IMAGE_DETAIL)));
    if (currentNode.hasProperty("fileName")) {
     thumbnailImagePath = currentNode.getProperty("fileName").getString();
    } else if (currentNode.hasProperty("fileReference")) {
     thumbnailImagePath = currentNode.getProperty("fileReference").getString();
    }
   } catch (PathNotFoundException e) {
    LOGGER.error("Node path not found:" + e);
   } catch (RepositoryException e) {
    LOGGER.error("Repository not found:" + e);
   }
  }
  return thumbnailImagePath;
 }
 
 /**
  * This method returns image node path in proper format. e.g.: Format initially:
  * "/content/whitelabel/en_gb/group-test-pages/test/test_content/_jcr_content/image", Format returned :
  * "content/whitelabel/en_gb/group-test-pages/shivamtest/test_content/jcr:content/image". "_jcr_content" is replaced with "jcr:content" and initial
  * "/" is removed.
  * 
  * @param fullPath
  *         the full path of image node
  * @return path of image node in required format.
  */
 private String getImageNodePath(String fullPath) {
  String finalPath = StringUtils.EMPTY;
  String[] split = fullPath.split("_jcr_content");
  finalPath = split[0].substring(ONE) + JcrConstants.JCR_CONTENT + split[1];
  return finalPath;
 }
 
 /**
  * This method is used to set several property inputs on search input required to create a query.
  * <ul>
  * It sets the following into search map:-
  * <li>"Do not search the contentOnlyPage" condition.</li>
  * <li>content types like event, article, product in OR conndition.</li>
  * </ul>
  * 
  * @param contentType
  *         THe contentType list.
  * @param searchInput
  *         THe search input object.
  * @return The modified searchInput.
  */
 protected SearchInput setPropertyList(final List<String> contentType, final SearchInput searchInput) {
  LOGGER.info("Setting the property inputs into search input object.");
  searchInput.setPropertiesGrouped(true);
  
  if (!org.apache.commons.collections.CollectionUtils.isEmpty(contentType)) {
   searchInput.setPropertyInput(new PropertyInput(SearchConstants.CONTENT_TYPE, contentType.toArray(new String[contentType.size()])));
  } else {
   LOGGER.debug("Content Type is empty. Any content type inputs would not be set and "
     + "search would happen on all the corresponding pages other than contentOnlyPage.");
  }
  return searchInput;
 }
 
 /**
  * This method shall take in the hits list and get the facets out of these results.
  * 
  * @param hitsList
  *         The list of hits obtained after searching the repository.
  */
 protected void facetDataProcessor() {
  
  /*
   * The logic to generate the facets on the hits could be written here.
   */
  
 }
 
 /**
  * This method returns current page path.
  * 
  * @param request
  * @return page path
  */
 private static String getCurrentPagePath(SlingHttpServletRequest request) {
  PageManager pageManager = (PageManager) request.getResourceResolver().adaptTo(PageManager.class);
  
  Page page = pageManager.getContainingPage(request.getResource());
  return page.getPath();
 }
 
}
