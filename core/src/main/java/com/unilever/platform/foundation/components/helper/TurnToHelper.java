/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.components.helper;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;

/**
 * The Class TurnToHelper.
 */
public class TurnToHelper {
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(TurnToHelper.class);
 
 /** The Constant TURN_TO. */
 private static final String TURN_TO = "turnTo";
 
 /** The Constant SITE_KEY. */
 private static final String SITE_KEY = "siteKey";
 
 /** The Constant SKU. */
 private static final String SKU = "sku";
 
 /** The Constant ITEM_JS. */
 private static final String ITEM_JS = "itemJS";
 
 /** The Constant SOURCE. */
 private static final String SOURCE = "source";
 
 /**
  * Instantiates a new turn to helper.
  */
 private TurnToHelper() {
  
 }
 
 /**
  * Gets the turn to config.
  * 
  * @param currentPage
  *         the current page
  * @param configurationService
  *         the configuration service
  * @param productId
  *         the product id
  * @return the turn to config
  */
 public static Map<String, String> getTurnToConfig(Page currentPage, ConfigurationService configurationService, String productId) {
  Map<String, String> turnToConfig = new HashMap<String, String>();
  try {
   turnToConfig = configurationService.getCategoryConfiguration(currentPage, TURN_TO);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Configuration is not found for {}.", TURN_TO, e);
  }
  
  String siteKey = turnToConfig.get(SITE_KEY) != null ? turnToConfig.get(SITE_KEY) : "";
  
  String itemJS = turnToConfig.get(ITEM_JS) != null ? turnToConfig.get(ITEM_JS) : "";
  itemJS = StringUtils.isNotBlank(productId) ? itemJS.replace(SKU, productId) : itemJS;
  itemJS = StringUtils.isNotBlank(siteKey) ? itemJS.replace(SITE_KEY, siteKey) : itemJS;
  
  String source = turnToConfig.get(SOURCE) != null ? turnToConfig.get(SOURCE) : "";
  source = StringUtils.isNotBlank(siteKey) ? source.replace(SITE_KEY, siteKey) : source;
  
  turnToConfig.put(ITEM_JS, itemJS);
  turnToConfig.put(SOURCE, source);
  
  return turnToConfig;
 }
 
 /**
  * Gets the empty for if null.
  * 
  * @param obj
  *         the obj
  * @return the empty for if null
  */
 public static String getEmptyForIfNull(Object obj) {
  return null == obj ? StringUtils.EMPTY : obj.toString();
  
 }
 

}
