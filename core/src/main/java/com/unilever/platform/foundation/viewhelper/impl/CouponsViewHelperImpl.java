/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.d2.platform.foundation.core.crypto.CryptoFactory;
import com.unilever.d2.platform.foundation.core.crypto.Cryptor;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfiguration;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.ImageHelper;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * <p>
 * The goal of this component is to display the heroImage, heroVideo, smallImage, heroLaunchVideo and preview images with option to play video . The
 * component will be configured by editor to provide Hero component title Short copy,Read more CTA and other properties.
 * </p>
 */
@Component(description = "coupons", immediate = true, metatype = true, label = "coupons")
@Service(value = { CouponsViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.COUPONS, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class CouponsViewHelperImpl extends BaseViewHelper {
 
 private static final String CAMPAIGN_ID = "campaignId";
 
 private static final String COUPONS_ERRORMESSAGE = "coupons.errorMessage";

 private static final String COUPONS_CONFIG = "couponsConfig";
 
 private static final String EXPIRY = "expiry";
 
 private static final String GENERAL_CONFIG = "generalConfig";
 private static final String J_AEM_CAMPAIGN_CONFIG = "aemCampaignConfig";
 private static final String AEM_CAMPAIGN_ID = "aemCampaignId";
 private static final String J_AEM_CAMPAIGN_ID = "aemCampaignId";
 private static final String RESTRICT_CODE_VALIDATION = "restrictByCodeValidation";
 private static final String J_RESTRICT_CODE_VALIDATION = "restrictByCodeValidation";
 private static final String J_HIDE_EXPIRED_COUPONS = "hideExpiredCoupons";
 private static final String HIDE_EXPIRED_COUPONS = "hideExpiredCoupons";
 private static final String NO_COUPON_AVAILABLE = "noCouponAvailableMsg";
 private static final String J_NO_COUPON_AVAILABLE = "noCouponAvailableMsg";
 
 /**
  * logger object.
  **/
 private static final Logger LOGGER = LoggerFactory.getLogger(CouponsViewHelperImpl.class);
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 @Reference
 GlobalConfiguration globalConfiguration;
 
 /**
  * Process data.
  * 
  * @param content
  *         , - {@link Map} of all user input received as input <code>ViewHelper.onGetData</code> method
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return {@link Map} holding the data
  */
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  LOGGER.info("calling CouponsViewHelperImpl helper class inside pocess data");
  ResourceResolver resourceResolver = getResourceResolver(resources);
  Page currentPage = getCurrentPage(resources);
  I18n i18n = getI18n(resources);
  Map<String, Object> properties = getProperties(content);
  String jsonNameSpace = getJsonNameSpace(content);
  Map<String, Object> couponsMap = new LinkedHashMap<String, Object>();
  couponsMap.put(GENERAL_CONFIG, getGeneralConfigDataMap(properties, resourceResolver, resources));
  String getOfferUrl = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, COUPONS_CONFIG, "couponsGetOfferUrl");
  MapUtils.getMap(couponsMap, GENERAL_CONFIG).put("getOfferUrl", resourceResolver.map(getOfferUrl));
  String couponType = MapUtils.getString(properties, "couponType", "");
  String entity = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, COUPONS_CONFIG, "entity");
  if (("Manual").equals(couponType)) {
   couponsMap.put("manualConfig", getManualConfigDataMap(currentPage, properties, resources));
   
  }
  if (("WebBricks").equals(couponType)) {
   couponsMap.put("webBricksConfig", getWebConfigDataMap(currentPage, resources).toArray());
   MapUtils.getMap(couponsMap, GENERAL_CONFIG).put(CAMPAIGN_ID, MapUtils.getString(properties, CAMPAIGN_ID, StringUtils.EMPTY));
   
  }
  String errorMessage = null != i18n.get(COUPONS_ERRORMESSAGE) ? i18n.get(COUPONS_ERRORMESSAGE) : StringUtils.EMPTY;
  couponsMap.put("entity", entity);
  couponsMap.put("errorMessage", errorMessage);
  couponsMap.put(J_AEM_CAMPAIGN_CONFIG, getAemCampaignDataMap(properties));
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  data.put(jsonNameSpace, couponsMap);
  return data;
 }
 
 /**
  * Gets the image data map.
  * 
  * @param resources
  *         the resources
  * @param page
  *         the current page
  * @param altText
  * @param imagePath
  * @return the image data map
  */
 
 public static Map<String, Object> getImageMapFromPath(Map<String, Object> resources, Page page, String altText, String imagePath) {
  String imageName = ImageHelper.fetchImageFileName(imagePath);
  return ImageHelper.getImageMap(imagePath, page, imagePath, imageName, altText, BaseViewHelper.getSlingRequest(resources));
  
 }
 
 /**
  * Gets the general config data map.
  * 
  * @param properties
  *         the properties
  * @param resourceResolver
  *         the resource resolver
  * @return the general config data map
  */
 protected Map<String, Object> getGeneralConfigDataMap(Map<String, Object> properties, ResourceResolver resourceResolver,
   final Map<String, Object> resources) {
  Map<String, Object> generalConfigMap = new HashMap<String, Object>();
  
  Page currentPage = getCurrentPage(resources);
  String couponsApiPath = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, COUPONS_CONFIG, "couponsApiPath");
  String thankyouPath = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, COUPONS_CONFIG, "thankyouPath");
  String couponsSubmitPath = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, COUPONS_CONFIG,
    "couponsSubmitPath");
  String couponsRedirectUrl = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, COUPONS_CONFIG,
    "couponsRedirectUrl");
  boolean hideExpiredCoupon = MapUtils.getBoolean(properties, HIDE_EXPIRED_COUPONS) != null ? MapUtils.getBoolean(properties,
    HIDE_EXPIRED_COUPONS) : false;
  generalConfigMap.put("headingText", MapUtils.getString(properties, "headingText", ""));
  generalConfigMap.put("shortSubheadingText", MapUtils.getString(properties, "subheadingText", ""));
  generalConfigMap.put("longSubheadingText", MapUtils.getString(properties, "longSubheadingText", ""));
  if(!hideExpiredCoupon){
   generalConfigMap.put("couponExpiryMessage", MapUtils.getString(properties, "couponExpiryMessage", ""));
  }
  generalConfigMap.put(J_NO_COUPON_AVAILABLE, MapUtils.getString(properties, NO_COUPON_AVAILABLE, StringUtils.EMPTY));
  generalConfigMap.put("coupontype", MapUtils.getString(properties, "couponType", ""));
  generalConfigMap.put("multipleSelection", MapUtils.getBoolean(properties, "multipleSelection", false));
  generalConfigMap.put("multiCtaLabel", MapUtils.getString(properties, "multiCtaLabel", ""));
  generalConfigMap.put("multiSelectionLabel", "Select coupon");
  generalConfigMap.put("couponsApiPath", resourceResolver.map(couponsApiPath));
  generalConfigMap.put("thankyouPath", thankyouPath);
  generalConfigMap.put("couponsSubmitPath", resourceResolver.map(couponsSubmitPath));
  generalConfigMap.put("couponsRedirectUrl", couponsRedirectUrl);
  generalConfigMap.put(J_HIDE_EXPIRED_COUPONS, hideExpiredCoupon);
  return generalConfigMap;
 }
 
 protected List<Map<String, Object>> getManualConfigDataMap(Page currentPage, Map<String, Object> properties,
   Map<String, Object> resources) {
  
  Resource currentResource = getCurrentResource(resources);
  I18n i18n = getI18n(resources);
  List<Map<String, String>> manualConfig = ComponentUtil.getNestedMultiFieldProperties(currentResource, "manualSection");
  List<Map<String, Object>> manualConfigFinalList = new ArrayList<Map<String, Object>>();
  
  Iterator<Map<String, String>> iterator = manualConfig.iterator();
  while (iterator.hasNext()) {
   Map<String, Object> tempMap = new HashMap<String, Object>();
   
   Map<String, String> textAndImageMap = iterator.next();
   String couponHeading =  MapUtils.getString(textAndImageMap, "couponHeadingManual", StringUtils.EMPTY);
   String couponSubheading = MapUtils.getString(textAndImageMap, "couponSubheadingManual", StringUtils.EMPTY);
   String couponLongSubheading = MapUtils.getString(textAndImageMap, "couponLongSubheadingManual", StringUtils.EMPTY);
   String couponCtaLabel = MapUtils.getString(textAndImageMap, "couponCtaLabelManual", StringUtils.EMPTY);
   String couponCtaUrl = MapUtils.getString(textAndImageMap, "couponCtaUrlManual", StringUtils.EMPTY);
   
   String couponForegroundImage = MapUtils.getString(textAndImageMap, "couponForegroundImageManual", StringUtils.EMPTY);
   String couponForegroundImageAltText = MapUtils.getString(textAndImageMap, "couponForegroundImageAltTextManual", StringUtils.EMPTY);
   
   String couponBackgroundImage = MapUtils.getString(textAndImageMap, "couponBackgroundImageManual", StringUtils.EMPTY);
   String couponBackgroundImageManualAltText = MapUtils.getString(textAndImageMap, "couponBackgroundImageManualAltText", StringUtils.EMPTY);
   String displayStartsManual = MapUtils.getString(textAndImageMap, "displayStartsManual", StringUtils.EMPTY);
   String displayEndsManual =  MapUtils.getString(textAndImageMap, "displayEndsManual", StringUtils.EMPTY);
   
   Map<String, Object> foregroundImageMap = getImageMapFromPath(resources, currentPage, couponForegroundImageAltText, couponForegroundImage);
   Map<String, Object> backgroundImageMap = getImageMapFromPath(resources, currentPage, couponBackgroundImageManualAltText, couponBackgroundImage);
   
   if (!(("").equals(displayEndsManual))) {
    tempMap.put(EXPIRY, i18n.get("coupons.expiry"));
   } else {
    tempMap.put(EXPIRY, StringUtils.EMPTY);
   }
   tempMap.put("couponHeading", couponHeading);
   tempMap.put("couponSubheading", couponSubheading);
   tempMap.put("couponLongSubheading", couponLongSubheading);
   tempMap.put("couponCtaLabel", couponCtaLabel);
   tempMap.put("couponCtaUrl", couponCtaUrl);
   
   tempMap.put("openInNewWindow", MapUtils.getBoolean(properties, "openInNewWindow", false));
   tempMap.put("foregroundImage", foregroundImageMap);
   tempMap.put("backgroundImage", backgroundImageMap);
   tempMap.put("displayStarts", displayStartsManual);
   tempMap.put("displayEnds", displayEndsManual);
   
   manualConfigFinalList.add(tempMap);
   
  }
  
  return manualConfigFinalList;
  
 }
 
 protected List<Map<String, Object>> getWebConfigDataMap(Page currentPage, Map<String, Object> resources) {
  
  Resource currentResource = getCurrentResource(resources);
  I18n i18n = getI18n(resources);
  List<Map<String, String>> webBricksConfig = ComponentUtil.getNestedMultiFieldProperties(currentResource, "webBricksSection");
  List<Map<String, Object>> webBricksFinalList = new ArrayList<Map<String, Object>>();
  
  Iterator<Map<String, String>> iterator = webBricksConfig.iterator();
  while (iterator.hasNext()) {
   Map<String, Object> tempMap = new HashMap<String, Object>();
   
   Map<String, String> textAndImageMap = iterator.next();
   String offerCode = MapUtils.getString(textAndImageMap, "offerCodeWeb", StringUtils.EMPTY);
   String checkCode = MapUtils.getString(textAndImageMap, "checkCodeWeb", StringUtils.EMPTY);
   String longKey = MapUtils.getString(textAndImageMap, "longKeyWeb", StringUtils.EMPTY);
   String shortKey = MapUtils.getString(textAndImageMap, "shortKeyWeb", StringUtils.EMPTY);
   String offerId = MapUtils.getString(textAndImageMap, "offerIdWeb", StringUtils.EMPTY);
   String couponHeading = MapUtils.getString(textAndImageMap, "couponHeadingWeb", StringUtils.EMPTY);
   String couponSubheading = MapUtils.getString(textAndImageMap, "couponSubheadingWeb", StringUtils.EMPTY);
   String couponLongSubheading = MapUtils.getString(textAndImageMap, "couponLongSubheadingWeb", StringUtils.EMPTY);
   String couponCtaLabel = MapUtils.getString(textAndImageMap, "couponCtaLabelWeb", StringUtils.EMPTY);
   String couponCtaUrl = MapUtils.getString(textAndImageMap, "couponCtaUrlWeb", StringUtils.EMPTY);
   
   String couponForegroundImage = MapUtils.getString(textAndImageMap, "couponForegroundImageWeb", StringUtils.EMPTY);
   String couponForegroundImageAltText = MapUtils.getString(textAndImageMap, "couponForegroundImageAltTextWeb", StringUtils.EMPTY);
   
   String couponBackgroundImage = MapUtils.getString(textAndImageMap, "couponBackgroundImageWeb", StringUtils.EMPTY);
   String couponBackgroundImageAltText = MapUtils.getString(textAndImageMap, "couponBackgroundImageWebAltText", StringUtils.EMPTY);
   String displayStartsManual = MapUtils.getString(textAndImageMap, "displayStartsWeb", StringUtils.EMPTY);
   String displayEndsManual = MapUtils.getString(textAndImageMap, "displayEndsWeb", StringUtils.EMPTY);
   Cryptor cryptor = CryptoFactory.getInstance("UnileverSapientCouponIntegrationKey", "UnileverSapientCouponIntegrationSalt");
   String encryptedlongKey = cryptor.crypt(UnileverConstants.ENCRYPT_MODE, longKey);
   String encryptedshortKey = cryptor.crypt(UnileverConstants.ENCRYPT_MODE, shortKey);
   
   Map<String, Object> foregroundImageMap = getImageMapFromPath(resources, currentPage, couponForegroundImageAltText, couponForegroundImage);
   Map<String, Object> backgroundImageMap = getImageMapFromPath(resources, currentPage, couponBackgroundImageAltText, couponBackgroundImage);
   
   if (!(("").equals(displayEndsManual))) {
    tempMap.put(EXPIRY, i18n.get("coupons.expiry"));
   } else {
    tempMap.put(EXPIRY, StringUtils.EMPTY);
   }
   tempMap.put("offerCode", offerCode);
   tempMap.put("checkCode", checkCode);
   tempMap.put("longKey", encryptedlongKey);
   tempMap.put("shortKey", encryptedshortKey);
   tempMap.put("offerId", offerId);
   tempMap.put("couponHeading", couponHeading);
   tempMap.put("couponSubheading", couponSubheading);
   tempMap.put("couponLongSubheading", couponLongSubheading);
   tempMap.put("couponCtaLabel", couponCtaLabel);
   tempMap.put("couponCtaUrl", couponCtaUrl);
   tempMap.put("foregroundImage", foregroundImageMap);
   tempMap.put("backgroundImage", backgroundImageMap);
   tempMap.put("displayStarts", displayStartsManual);
   tempMap.put("displayEnds", displayEndsManual);
   
   webBricksFinalList.add(tempMap);
   
  }
  
  return webBricksFinalList;
  
 }
 
 /**
  * Gets the AEM Campaign data map.
  * 
  * @param properties
  *         the properties
  * @param resourceResolver
  *         the resource resolver
  * @return the general config data map
  */
 protected Map<String, Object> getAemCampaignDataMap(Map<String, Object> properties) {
  Map<String, Object> aemCampaignMap = new HashMap<String, Object>();
  boolean restrictByCodeValidationFlag = MapUtils.getBoolean(properties, RESTRICT_CODE_VALIDATION) != null ? MapUtils.getBoolean(properties,
    RESTRICT_CODE_VALIDATION) : false;
  aemCampaignMap.put(J_AEM_CAMPAIGN_ID, MapUtils.getString(properties, AEM_CAMPAIGN_ID, StringUtils.EMPTY));
  aemCampaignMap.put(J_RESTRICT_CODE_VALIDATION, restrictByCodeValidationFlag);

  return aemCampaignMap;
 }
 
}
