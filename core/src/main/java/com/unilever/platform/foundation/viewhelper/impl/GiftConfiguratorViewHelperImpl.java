/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.cache.ComponentViewAndViewHelperCache;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.commerce.api.CustomProduct;
import com.unilever.platform.aem.foundation.commerce.api.RecipientsInfo;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.core.service.SearchService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.components.helper.ProductHelperExt;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.GiftConfiguratorConstants;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * The Class ProductCrossellViewHelperImpl.
 */
@Component(description = "GiftConfiguratorViewHelperImpl", immediate = true, metatype = true, label = "GiftConfiguratorViewHelperImpl")
@Service(value = { GiftConfiguratorViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.GIFT_CONFIGURATOR, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class GiftConfiguratorViewHelperImpl extends BaseViewHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(GiftConfiguratorViewHelperImpl.class);
 
 /** The component view and view helper cache. */
 /*
  * reference for cache object
  */
 @Reference
 ComponentViewAndViewHelperCache componentViewAndViewHelperCache;
 /*
  * reference for site configuration service
  */
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The search service. */
 @Reference
 SearchService searchService;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.impl. MultipleRelatedProductsBaseViewHelperImpl#processData(java.util.Map, java.util.Map)
  */
 @SuppressWarnings("unchecked")
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  LOGGER.debug("Executing ProductCrossellViewHelperImpl.java. Inside processData method");
  Map<String, Object> data = new HashMap<String, Object>();
  ResourceResolver resourceResolver = getResourceResolver(resources);
  
  Map<String, Object> properties = getProperties(content);
  
  Page currentPage = getCurrentPage(resources);
  String jsonNameSpace = MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE);
  Map<String, String> configMap = ComponentUtil.getConfigMap(currentPage, GiftConfiguratorConstants.GLOBAL_CONFIG_CAT);
  Map<String, Object> giftConfiguratorMap = new HashMap<String, Object>();
  Map<String, Object> productProperties = new LinkedHashMap<String, Object>();
  I18n i18n = getI18n(resources);
  SlingHttpServletRequest slingRequest = getSlingRequest(resources);
  
  String productId = StringUtils.isNotBlank(slingRequest.getParameter(ProductConstants.EAN)) ? slingRequest.getParameter(ProductConstants.EAN)
    : StringUtils.EMPTY;
  UNIProduct uniProduct = null;
  if (StringUtils.isNotBlank(productId)) {
   uniProduct = ProductHelperExt.findProduct(currentPage, productId);
   if (uniProduct != null) {
    productProperties = ProductHelper.getProductMap(uniProduct, jsonNameSpace, resources, currentPage);
    
   } else {
    LOGGER.debug("UniProduct obj is null in getting from ProductHelperExt.findProduct");
   }
  }
  giftConfiguratorMap.put(GiftConfiguratorConstants.GIFT_FEILDS, getGiftFeilds(configMap, i18n, uniProduct));
  giftConfiguratorMap.putAll(ProductHelperExt.getDeliveryDetailMap(currentPage));
  giftConfiguratorMap.put(UnileverConstants.PRODUCT, productProperties);
  giftConfiguratorMap.put(UnileverConstants.TITLE,
    StringUtils.isNotBlank(MapUtils.getString(properties, UnileverConstants.TITLE)) ? MapUtils.getString(properties, UnileverConstants.TITLE)
      : StringUtils.EMPTY);
  
  String giftWrapLabel = StringUtils.EMPTY;
  Map<String, Object> nestedConfiguratorMap = (Map<String, Object>) productProperties.get(ProductConstants.GIFT_CONFIGURATOR);
  if (nestedConfiguratorMap != null) {
   Object[] nestedGiftWrapMap = (Object[]) nestedConfiguratorMap.get(ProductConstants.GIFT_WRAPS);
   int giftWrapSize = nestedGiftWrapMap != null ? nestedGiftWrapMap.length : 0;
   
   giftWrapLabel = giftWrapSize != 0 ? i18n.get(GiftConfiguratorConstants.GIFT_CONFIGURATOR_GIFTWRAP_LABEL) : "";
  }
  giftConfiguratorMap.put(GiftConfiguratorConstants.GIFT_WRAP_LABEL, giftWrapLabel);
  giftConfiguratorMap.put(GiftConfiguratorConstants.SUB_TOTAL_LABEL, i18n.get(GiftConfiguratorConstants.SUB_TOTAL));
  giftConfiguratorMap.put(GiftConfiguratorConstants.EXCLUSIVE_GIFT_WRAP_LABEL, i18n.get(GiftConfiguratorConstants.EXCLUSIVE_GIFT_WRAP_KEY));
  giftConfiguratorMap.put(ProductConstants.EGIFTING_FREE_LABEL, i18n.get(ProductConstants.EGIFTING_FREE_KEY));
  giftConfiguratorMap.put(GiftConfiguratorConstants.CTA, getCta(i18n, properties, resourceResolver, slingRequest));
  giftConfiguratorMap.put(CONFIG, getConfigData(configMap));
  
  data.put(jsonNameSpace, giftConfiguratorMap);
  return data;
 }
 
 /**
  * Gets the gift feilds.
  * 
  * @param configMap
  *         the config map
  * @param i18n
  *         the i18n
  * @param uniProduct
  *         the uni product
  * @return the gift feilds
  */
 private Object getGiftFeilds(Map<String, String> configMap, I18n i18n, UNIProduct uniProduct) {
  List<Map<String, Object>> giftFeilds = new ArrayList<Map<String, Object>>();
  if (uniProduct != null) {
   CustomProduct customeProduct = uniProduct.getCustomProduct();
   List<RecipientsInfo> recipentsInfo = customeProduct.getRecipientsInfo();
   
   for (RecipientsInfo recipientsInfo : recipentsInfo) {
    Map<String, Object> formElements = new HashMap<String, Object>();
    Map<String, Object> formElementsMap = new HashMap<String, Object>();
    
    String type = recipientsInfo.getType();
    
    formElementsMap
      .put(UnileverConstants.LABEL, i18n.get(GiftConfiguratorConstants.GLOBAL_CONFIG_CAT + "." + type + GiftConfiguratorConstants.LABEL));
    formElementsMap.put(UnileverConstants.ERROR_MSG,
      i18n.get(GiftConfiguratorConstants.GLOBAL_CONFIG_CAT + "." + type + GiftConfiguratorConstants.ERRORMSG));
    formElementsMap.put(UnileverConstants.DEFAULT_VALUE,
      i18n.get(GiftConfiguratorConstants.GLOBAL_CONFIG_CAT + "." + type + GiftConfiguratorConstants.DEFAULT_VALUE));
    formElementsMap.put(GiftConfiguratorConstants.NAME,
      i18n.get(GiftConfiguratorConstants.GLOBAL_CONFIG_CAT + "." + type + GiftConfiguratorConstants.FIELD_NAME));
    formElementsMap.put(
      GiftConfiguratorConstants.INPUT_LIMIT,
      StringUtils.isNotBlank(configMap.get(GiftConfiguratorConstants.RECIEPENT_FIELD_INPUT_LIMIT)) ? Integer.parseInt(configMap
        .get(GiftConfiguratorConstants.RECIEPENT_FIELD_INPUT_LIMIT)) : StringUtils.EMPTY);
    formElementsMap.put(GiftConfiguratorConstants.CHARACTER_COUNT, recipientsInfo.getCharacterCount());
    
    formElements.put(UnileverConstants.FORM_ELEMENT, formElementsMap);
    giftFeilds.add(formElements);
   }
  }
  
  return giftFeilds.toArray();
 }
 
 /**
  * Gets the config data.
  * 
  * @param configMap
  *         the config map
  * @return the config data
  */
 private Object getConfigData(Map<String, String> configMap) {
  Map<String, Object> configData = new HashMap<String, Object>();
  configData.put(
    GiftConfiguratorConstants.GIFTING_WRAP_ENABLE,
    StringUtils.isNotBlank(configMap.get(GiftConfiguratorConstants.GIFTING_WRAP_ENABLE)) ? Boolean.valueOf(configMap
      .get(GiftConfiguratorConstants.GIFTING_WRAP_ENABLE)) : Boolean.valueOf(ProductConstants.FALSE));
  configData.put(
    GiftConfiguratorConstants.MAX_GIFT_WRAPS,
    StringUtils.isNotBlank(configMap.get(GiftConfiguratorConstants.MAX_GIFT_WRAPS)) ? Integer.parseInt(configMap
      .get(GiftConfiguratorConstants.MAX_GIFT_WRAPS)) : StringUtils.EMPTY);
  return configData;
 }
 
 /**
  * Gets the cta.
  * 
  * @param i18n
  *         the i18n
  * @param properties
  *         the properties
  * @param resourceResolver
  *         the resource resolver
  * @return the cta
  */
 private Object getCta(I18n i18n, Map<String, Object> properties, ResourceResolver resourceResolver, SlingHttpServletRequest slingRequest) {
  Map<String, Object> ctaMap = new HashMap<String, Object>();
  ctaMap.put(UnileverConstants.LABEL, i18n.get(GiftConfiguratorConstants.GIFT_CONFIGURATOR_CTALABEL));
  String navigationUrl = StringUtils.isNotBlank(MapUtils.getString(properties, GiftConfiguratorConstants.NAVIGATION_URL)) ? MapUtils.getString(
    properties, GiftConfiguratorConstants.NAVIGATION_URL) : StringUtils.EMPTY;
  String navigationFullUrl = StringUtils.isNotBlank(navigationUrl) ? ComponentUtil.getFullURL(resourceResolver, navigationUrl, slingRequest)
    : StringUtils.EMPTY;
  ctaMap.put(GiftConfiguratorConstants.NAVIGATION_URL_KEY, navigationFullUrl);
  return ctaMap;
 }
 
 /**
  * Gets the config service.
  * 
  * @return configurationService
  */
 protected ConfigurationService getConfigService() {
  
  return this.configurationService;
 }
 
 /**
  * Gets the search service.
  * 
  * @return searchService
  */
 protected SearchService getSearchService() {
  
  return this.searchService;
 }
 
}
