/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.components.helper;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Value;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.jcr.resource.JcrPropertyMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.Externalizer;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.aem.foundation.core.dto.BrandMarketBean;
import com.unilever.platform.aem.foundation.core.dto.TaggingSearchCriteria;
import com.unilever.platform.aem.foundation.core.service.AnalyticsService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.dto.FeatureTagsDTO;
import com.unilever.platform.foundation.constants.CommonConstants;
import com.unilever.platform.foundation.constants.ContainerTagConstants;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.Image;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * The Class ComponentUtil.
 */
public class ComponentUtil {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ComponentUtil.class);
 
 /** The Constant EXTERNALIZER_DOMAIN_KEY. */
 private static final String EXTERNALIZER_DOMAIN_KEY = "externalizer_domain_key";
 
 /** The Constant BRAND. */
 private static final String BRAND = "brand";
 
 /** The Constant NAME. */
 private static final String NAME = "name";
 
 /** The Constant REG_EX_SPECIAL_CHAR. */
 private static final String REG_EX_SPECIAL_CHAR = "[^0-9^A-Z^a-z]";
 
 /** The Constant DEFAULT_DATE_FORMAT. */
 private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
 
 /** The Constant NO_INDEX. */
 private static final String NO_INDEX = "noindex";
 
 private static final String TARGET_DATEFORMAT = "dd/MM/yyyy";
 
 private static final String SYS_DATEFORMAT = "yyyy-MM-dd";
 
 /** The Constant DATE_FORMAT. */
 private static final String SYS_DATE_FORMAT = "sysDateFormat";
 
 /** The Constant DATE_FORMAT. */
 private static final String DATE_FORMAT = "dateFormat";
 /** The Constant WHITE_SPACE. */
 private static final String WHITE_SPACE = " ";
 /** The Constant REPLACE_WHITE_SPACE_WITH. */
 private static final String REPLACE_WHITE_SPACE_WITH = "%20";
 
 /** The Constant FALSE. */
 private static final Boolean FALSE = false;
 
 /** The Constant TRUE. */
 private static final Boolean TRUE = true;
 
 /** The Constant TAG_PRIMARY_PATH. */
 private static final String TAG_PRIMARY_PATH = "/etc/tags/unilever/";
 
 /** The Constant TAG_SECONDARY_PATH. */
 private static final String TAG_SECONDARY_PATH = "/etc/tags/unilever/segment/";
 
 /** The Constant SEGEMNT. */
 private static final String SEGEMNT = "/segment/";
 
 private static final String COMPONENT_LOGIN_REGISTER = "components/loginRegister";
 
 /** The Constant FOUR. */
 private static final int FOUR = 4;
 
 /** The Constant THREE. */
 private static final int THREE = 3;
 
 /**
  * Instantiates a new component util.
  */
 private ComponentUtil() {
  
 }
 
 /**
  * Gets the property value array. The method is responsible for returning a string array of property value
  * 
  * @param properties
  *         the properties
  * @param propertyKey
  *         the property key
  * @return the property value array
  */
 public static String[] getPropertyValueArray(Map<String, Object> properties, String propertyKey) {
  LOGGER.debug("Inside getPropertyValueArray");
  Object propertyValObj = properties.get(propertyKey);
  String[] propertyValArr = new String[] {};
  if (propertyValObj != null) {
   if (propertyValObj instanceof String[]) {
    propertyValArr = (String[]) propertyValObj;
   } else if (StringUtils.isNotBlank((String) propertyValObj)) {
    propertyValArr = new String[] { propertyValObj.toString() };
   }
  }
  return propertyValArr;
 }
 
 /**
  * Returns the full url of page. Domain path will be fetched from externalizer service and relative path will be added to it.
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param pagePathParam
  *         the page path param
  * @param slingRequest
  *         the slingRequest
  * @return the full url
  */
 public static String getFullURL(ResourceResolver resourceResolver, String pagePathParam, SlingHttpServletRequest slingRequest) {
  String fullUrl = getFullURL(resourceResolver, pagePathParam);
  String fullUrlWithUpdatedProtocol = StringUtils.EMPTY;
  if (null != slingRequest && StringUtils.startsWith(pagePathParam, "/")) {
   fullUrlWithUpdatedProtocol = getURLWithUpdatedProtocol(fullUrl, slingRequest);
  }
  return StringUtils.isBlank(fullUrlWithUpdatedProtocol) ? fullUrl : fullUrlWithUpdatedProtocol;
 }
 
 /**
  * Returns the full url of page. Domain path will be fetched from externalizer service and relative path will be added to it.
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param pagePathParam
  *         the page path param
  * @return the full url
  */
 public static String getFullURL(ResourceResolver resourceResolver, String pagePathParam) {
  LOGGER.debug("Inside getFullURL of ComponentUtil");
  String pagePath = pagePathParam;
  ConfigurationService configurationService = resourceResolver.adaptTo(ConfigurationService.class);
  String fullPath = StringUtils.EMPTY;
  Externalizer externalizer = resourceResolver.adaptTo(Externalizer.class);
  Resource resource = getResourceFromPath(resourceResolver, pagePath);
  if (resource != null) {
   if (externalizer != null) {
    pagePath = resourceResolver.map(pagePath);
    PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
    Page page = pageManager.getContainingPage(resource);
    if (page == null) {
     return pagePath;
    }
    
    if (!pagePath.contains(CommonConstants.DOT_CHAR)) {
     pagePath = pagePath + CommonConstants.PAGE_EXTENSION;
    }
    LOGGER.debug("Relative Path = " + pagePath);
    
    fullPath = getFullUrlUsingExternalizer(externalizer, page, configurationService, resourceResolver, pagePath);
   }
   
   if (StringUtils.isNotBlank(pagePath) && !pagePath.contains(CommonConstants.DOT_CHAR) && !StringUtils.equals(pagePath, "#")) {
    pagePath = pagePath + CommonConstants.PAGE_EXTENSION;
   }
   
   if (StringUtils.containsNone(pagePath, CommonConstants.DOT_CHAR)) {
    pagePath = pagePath + CommonConstants.PAGE_EXTENSION;
   }
   /* Need to decode the path as the path obtained form resource resolver is in encoded form for languages other than English */
   if (pagePath != null) {
    pagePath = getEncodedOrDecodeUrl(pagePath);
   }
   if (pagePath != null && StringUtils.contains(pagePath, WHITE_SPACE)) {
    pagePath = pagePath.replaceAll(WHITE_SPACE, REPLACE_WHITE_SPACE_WITH);
   }
  }
  return StringUtils.isBlank(fullPath) ? pagePath : replaceDomain(fullPath);
 }
 
 /**
  * 
  * @param fullPath
  * @return
  */
 public static String replaceDomain(String fullPath) {
  String tempFullUrl = fullPath;
  if (StringUtils.contains(tempFullUrl, "/secure/")) {
   String[] urlParts = tempFullUrl.split("://");
   if (StringUtils.equalsIgnoreCase(urlParts[0], "http")) {
    urlParts[0] = "https";
    tempFullUrl = String.join("://", urlParts);
   }
  }
  return tempFullUrl;
 }
 
 /**
  * This method checks if a given URL belongs to external domain or not. This method extracts host name from URL that has to be analyzed. Current page
  * is used as base page to compare. if host name and port number of both urls (to be analyzed URL and current page URL) matches, then it not external
  * url otherwise external.
  * 
  * @param slingRequest
  *         the sling request
  * @param resourceResolver
  *         the resource resolver
  * @param currentPage
  *         the current page
  * @param fullUrl
  *         the full url that has to be analyzed
  * @return true if full url belongs to external domain else false.
  */
 public static boolean isUrlExternal(ResourceResolver resourceResolver, SlingHttpServletRequest slingRequest, Page currentPage, String fullUrl) {
  URI uri = null;
  URI currentUri = null;
  String currentPagePath = currentPage.getPath();
  try {
   uri = new URI(fullUrl);
   currentUri = new URI(getFullURL(resourceResolver, currentPagePath, slingRequest));
  } catch (URISyntaxException e) {
   LOGGER.error("Error while creating uri object of fullURL" + e);
   return true;
  }
  String domain = uri.getHost();
  String currentDomain = currentUri.getHost();
  int domainPort = uri.getPort();
  int currentDomainPort = currentUri.getPort();
  
  
  if (currentDomain != null) {
   return !(currentDomain.equals(domain) && (domainPort == currentDomainPort));
  }
  return true;
 }
 
 /**
  * Gets the resource from path.
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param pagePath
  *         the page path
  * @return the resource from path
  */
 public static Resource getResourceFromPath(ResourceResolver resourceResolver, String pagePath) {
  Resource resource = null;
  
  String path = pagePath;
  if (StringUtils.contains(pagePath, CommonConstants.DOT_CHAR)) {
   path = path.substring(0, path.lastIndexOf(CommonConstants.DOT_CHAR));
  }
  resource = resourceResolver.getResource(path);
  return resource;
 }
 
 /**
  * Gets the full url using externalizer.
  * 
  * @param externalizer
  *         the externalizer
  * @param page
  *         the page
  * @param configurationService
  *         the configuration service
  * @param resourceResolver
  *         the resource resolver
  * @param pagePath
  *         the page path
  * @return the full url using externalizer
  */
 private static String getFullUrlUsingExternalizer(Externalizer externalizer, Page page, ConfigurationService configurationService,
   ResourceResolver resourceResolver, String pagePath) {
  String fullPath = StringUtils.EMPTY;
  String localPagePath = pagePath;
  String externalizerKey = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, page, BRAND, EXTERNALIZER_DOMAIN_KEY);
  if (!StringUtils.isBlank(externalizerKey)) {
   /* Need to decode the path as the path obtained form resource resolver is in encoded form for languages other than English */
   if (localPagePath != null) {
    localPagePath = getEncodedOrDecodeUrl(localPagePath);
   }
   try {
    fullPath = externalizer.externalLink(resourceResolver, externalizerKey, localPagePath);
   } catch (Exception e) {
    LOGGER.error("Exception in getting the externalizer url ", e);
   }
  }
  /* Need to decode the path again as the fullPath obtained form externalizer is in encoded form for languages other than English */
  if (fullPath != null) {
   fullPath = getEncodedOrDecodeUrl(fullPath);
  }
  if (fullPath != null && StringUtils.contains(fullPath, WHITE_SPACE)) {
   fullPath = fullPath.replaceAll(WHITE_SPACE, REPLACE_WHITE_SPACE_WITH);
  }
  return fullPath;
 }
 
 /**
  * Gets the anchor link map.
  * 
  * @param properties
  *         the properties
  * @return the anchor link map
  */
 public static Map<String, String> getAnchorLinkMap(Map<String, Object> properties) {
  Map<String, String> anchorNavigationMap = new HashMap<String, String>();
  String showAsNavigation = MapUtils.getString(properties, UnileverConstants.SHOW_AS_NAVIGATION) != null
    ? MapUtils.getString(properties, UnileverConstants.SHOW_AS_NAVIGATION) : "false";
  String label = MapUtils.getString(properties, UnileverConstants.LABEL) != null ? MapUtils.getString(properties, UnileverConstants.LABEL)
    : MapUtils.getString(properties, UnileverConstants.TITLE) != null ? MapUtils.getString(properties, UnileverConstants.TITLE) : StringUtils.EMPTY;
  String title = label.replaceAll(REG_EX_SPECIAL_CHAR, "").toLowerCase();
  String randomNumber = MapUtils.getString(properties, UnileverConstants.RANDOM_NUMBER) != null
    ? MapUtils.getString(properties, UnileverConstants.RANDOM_NUMBER) : StringUtils.EMPTY;
  String id = title + randomNumber;
  anchorNavigationMap.put(UnileverConstants.SHOW_AS_NAVIGATION, showAsNavigation);
  anchorNavigationMap.put(UnileverConstants.LABEL, label);
  anchorNavigationMap.put(UnileverConstants.ID, id);
  return anchorNavigationMap;
  
 }
 
 /**
  * Gets the container tag map.
  * 
  * @param page
  *         the page
  * @param analyticConfigCatName
  *         the analytic config cat name
  * @param label
  *         the label
  * @return the container tag map
  */
 public static Map<String, Object> getContainerTagMap(Page page, String analyticConfigCatName, Object label) {
  AnalyticsService analyticsService = page.adaptTo(AnalyticsService.class);
  Map<String, String> analyticConfigMap = analyticsService.getAnalyticsValues(page, analyticConfigCatName);
  Map<String, Object> containerTagMap = new HashMap<String, Object>();
  containerTagMap.put(ContainerTagConstants.CATEGORY,
    analyticConfigMap.get(ContainerTagConstants.CATEGORY) != null ? analyticConfigMap.get(ContainerTagConstants.CATEGORY) : "");
  containerTagMap.put(ContainerTagConstants.ACTION,
    analyticConfigMap.get(ContainerTagConstants.ACTION) != null ? analyticConfigMap.get(ContainerTagConstants.ACTION) : "");
  containerTagMap.put(ContainerTagConstants.TYPE,
    analyticConfigMap.get(ContainerTagConstants.TYPE) != null ? analyticConfigMap.get(ContainerTagConstants.TYPE) : "");
  containerTagMap.put(ContainerTagConstants.LABEL, label);
  String attributes = analyticConfigMap.get(ContainerTagConstants.ATTRIBUTES);
  if (attributes != null) {
   attributes = attributes.replaceAll("'", "\"");
   try {
    JSONObject attributesJSON = new JSONObject(attributes);
    Map<String, String> attributesMap = toMap(attributesJSON);
    containerTagMap.put(ContainerTagConstants.ATTRIBUTES, attributesMap);
   } catch (JSONException e) {
    LOGGER.error("attributes values is not in json format ", e);
   }
  }
  
  return containerTagMap;
 }
 
 /**
  * Gets the review map.
  * 
  * @param configMap
  *         the config map
  * @return the review map
  */
 public static Map<String, String> getReviewMap(Map<String, String> configMap) {
  String isRatingAndReviewEnabled = configMap.get(UnileverConstants.IS_RATING_AND_REVIEW_ENABLED) != null
    ? (String) configMap.get(UnileverConstants.IS_RATING_AND_REVIEW_ENABLED) : "false";
  String ratingAndReviewWidgetType = configMap.get(UnileverConstants.RATING_AND_REVIEW_WIDGET_TYPE) != null
    ? (String) configMap.get(UnileverConstants.RATING_AND_REVIEW_WIDGET_TYPE) : "";
  
  Map<String, String> map = new HashMap<String, String>();
  map.put(UnileverConstants.IS_RATING_AND_REVIEW_ENABLED, isRatingAndReviewEnabled);
  map.put(UnileverConstants.RATING_AND_REVIEW_WIDGET_TYPE, ratingAndReviewWidgetType);
  return map;
  
 }
 
 /**
  * Gets the nested multi field properties.
  * 
  * @param resource
  *         the resource
  * @param propertyName
  *         the property name
  * @return the nested multi field properties
  */
 public static List<Map<String, String>> getNestedMultiFieldProperties(Resource resource, String propertyName) {
  
  List<Map<String, String>> list = new ArrayList<Map<String, String>>();
  
  if (resource != null && propertyName != null) {
   Node currentNode = resource.adaptTo(Node.class);
   try {
    Property property = null;
    
    if (currentNode.hasProperty(propertyName)) {
     property = currentNode.getProperty(propertyName);
    }
    
    if (property != null) {
     Value[] values = getValueArray(property);
     
     for (Value val : values) {
      JSONObject rootObject = new JSONObject(val.getString());
      Map<String, String> topMap = toMap(rootObject);
      if (topMap != null) {
       list.add(topMap);
      }
     }
    }
   } catch (PathNotFoundException e) {
    LOGGER.error("path not found"+e.getMessage());
    LOGGER.debug("path not found",e);
   } catch (RepositoryException e) {
    LOGGER.error("could not connect to repository"+e.getMessage());
    LOGGER.debug("could not connect to repository",e);
   } catch (JSONException e) {
    LOGGER.error("Error while fetching JSON Object"+e.getMessage());
    LOGGER.debug("Error while fetching JSON Object", e);
   }
  }
  return list;
 }
 
 /**
  * Convert json array to list of string map.
  * 
  * @param jsonArrayString
  *         the json array string
  * @return the list
  */
 public static List<Map<String, String>> convertJsonArrayToListOfStringMap(String jsonArrayString) {
  
  List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
  try {
   JSONArray jsonObject = new JSONArray(jsonArrayString);
   if (jsonObject != null) {
    for (int i = 0; i < jsonObject.length(); i++) {
     Object value = jsonObject.get(i);
     if (value instanceof JSONObject) {
      Map<String, String> nestedMap = toMap((JSONObject) value);
      if (nestedMap != null) {
       mapList.add(nestedMap);
      }
     }
    }
   }
  } catch (JSONException e) {
   LOGGER.error("Error while fetching JSON Object", e);
  }
  return mapList;
 }
 
 /**
  * To map.
  * 
  * @param object
  *         the object
  * @return the map
  * @throws JSONException
  *          the JSON exception
  */
 public static Map<String, String> toMap(JSONObject object) throws JSONException {
  Map<String, String> map = new HashMap<String, String>();
  
  Iterator<String> keysItr = object.keys();
  while (keysItr.hasNext()) {
   String key = keysItr.next();
   Object value = object.get(key);
   map.put(key, value.toString());
  }
  return map;
 }
 
 /**
  * Gets the value array.
  * 
  * @param property
  *         the property
  * @return the value array
  * @throws RepositoryException
  *          the repository exception
  */
 private static Value[] getValueArray(Property property) throws RepositoryException {
  Value[] values = null;
  if (property.isMultiple()) {
   values = property.getValues();
  } else {
   values = new Value[1];
   values[0] = property.getValue();
  }
  
  return values;
 }
 
 /**
  * Gets the config map.
  * 
  * @param currentPage
  *         the current page
  * @param gloablConfigCatName
  *         the gloabl config cat name
  * @return the config map
  */
 public static Map<String, String> getConfigMap(Page currentPage, String gloablConfigCatName) {
  ConfigurationService configurationService = currentPage.adaptTo(ConfigurationService.class);
  Map<String, String> configMap = new HashMap<String, String>();
  try {
   configMap = configurationService.getCategoryConfiguration(currentPage, gloablConfigCatName);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Configuration not found ", e);
  }
  return configMap;
 }
 
 /**
  * Gets the brand name.
  * 
  * @param currentPage
  *         the current page
  * @return the brand name
  */
 public static String getBrandName(Page currentPage) {
  ConfigurationService configurationService = currentPage.adaptTo(ConfigurationService.class);
  try {
   return configurationService.getConfigValue(currentPage, BRAND, NAME);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Error finding brand name " + e.getMessage());
   LOGGER.debug("Error finding brand name " ,e);
   return StringUtils.EMPTY;
  }
 }
 
 /**
  * Gets the video container tag map.
  * 
  * @param page
  *         the page
  * @param videoId
  *         the video id
  * @return the video container tag map
  */
 public static Map<String, Object> getVideoContainerTagMap(Page page, String videoId) {
  Map<String, Object> map = new LinkedHashMap<String, Object>();
  Map<String, Object> videoPlayMap = getContainerTagMap(page, "videoPlay", videoId);
  Map<String, Object> videoCompleteMap = getContainerTagMap(page, "videoComplete", videoId);
  Map<String, Object> videoProgressMap = getContainerTagMap(page, "videoProgress", videoId);
  
  map.put("videoPlay", videoPlayMap);
  map.put("videoComplete", videoCompleteMap);
  map.put("videoProgress", videoProgressMap);
  
  return map;
 }
 
 /**
  * Gets the article flag tag map.
  * 
  * @param page
  *         the page
  * @param configCatName
  *         the config cat name
  * @return the container tag map
  */
 public static List<Map<String, Object>> getArticleTagFlagMap(Page page, String configCatName) {
  
  Map<String, String> configMap = getConfigMap(page, configCatName);
  
  String[] filters = !StringUtils.isBlank(configMap.get(UnileverConstants.ARTICLE_FLAG_NAMESPACE))
    ? configMap.get(UnileverConstants.ARTICLE_FLAG_NAMESPACE).split(CommonConstants.COMMA_CHAR) : new String[] {};
  
  Tag[] flagsArr = page.getTags();
  
  List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
  for (String filter : filters) {
   Map<String, Object> flagmap = new HashMap<String, Object>();
   LOGGER.debug("Inside filters loop and filter is {}", filter);
   for (Tag currentPageTag : flagsArr) {
    String tagId = currentPageTag.getTagID();
    String tagname = currentPageTag.getName();
    String tagtitle = currentPageTag.getTitle(BaseViewHelper.getLocale(page));
    LOGGER.debug("Inside Tags loop and tagid is {}", tagId);
    if (tagId.startsWith(filter)) {
     
     flagmap.put("title", tagtitle);
     flagmap.put("name", tagname);
     
     list.add(flagmap);
    }
   }
   
  }
  
  return list;
 }
 
 /**
  * Current year.
  * 
  * @return current year.
  */
 public static int getCurrentYear() {
  LocalDateTime now = LocalDateTime.now();
  return now.getYear();
 }
 
 /**
  * Returns the copyright text followed by current year.
  * 
  * @return copyright text followed by current year.
  */
 public static String getCopyrightSignWithYear() {
  return "&#169; " + getCurrentYear();
 }
 
 /**
  * Gets the filter tag list.
  * 
  * @param currentPage
  *         the current page
  * @param filters
  *         the filters
  * @param includeTagIdList
  *         the include tag id list
  * @param conditionOperators
  *         the condition operators
  * @return the filter tag list
  */
 public static List<TaggingSearchCriteria> getFilterTagList(Page currentPage, String[] filters, List<String> includeTagIdList,
   String[] conditionOperators) {
  LOGGER.debug("Inside getFilterTagList of ComponentUtil");
  List<TaggingSearchCriteria> filterTagList = new ArrayList<TaggingSearchCriteria>();
  List<TaggingSearchCriteria> finalfilterTagList = new ArrayList<TaggingSearchCriteria>();
  if (currentPage != null) {
   Tag[] currentPageTagsArr = currentPage.getTags();
   if (includeTagIdList != null && !includeTagIdList.isEmpty()) {
    TaggingSearchCriteria includeTags = new TaggingSearchCriteria();
    LOGGER.debug("Inside purpose tag list");
    includeTags.setTagList(includeTagIdList);
    includeTags.setOperator("or");
    filterTagList.add(includeTags);
    
   }
   finalfilterTagList = getFilterTaggingSearchCriteria(currentPageTagsArr, filters, conditionOperators, filterTagList);
   
  }
  return finalfilterTagList;
 }
 
 /**
  * Gets the filter tagging search criteria.
  * 
  * @param currentPageTagsArr
  *         the current page tags arr
  * @param filters
  *         the filters
  * @param conditionOperators
  *         the condition operators
  * @param filterTagList
  *         the filter tag list
  * @param includeTagIdList
  * @return the filter tagging search criteria
  */
 private static List<TaggingSearchCriteria> getFilterTaggingSearchCriteria(Tag[] currentPageTagsArr, String[] filters, String[] conditionOperators,
   List<TaggingSearchCriteria> filterTagList) {
  if (filters != null && filters.length > 0 && currentPageTagsArr != null && currentPageTagsArr.length > 0) {
   int counter = 0;
   for (String filter : filters) {
    LOGGER.debug("Inside filters loop and filter is {}", filter);
    List<String> list = new ArrayList<String>();
    TaggingSearchCriteria taggingSearchCriteria = new TaggingSearchCriteria();
    for (Tag currentPageTag : currentPageTagsArr) {
     String tagId = currentPageTag.getTagID();
     LOGGER.debug("Inside filter namespace with id {}", tagId);
     if (tagId.startsWith(filter)) {
      list.add(tagId);
     }
    }
    taggingSearchCriteria.setTagList(list);
    taggingSearchCriteria.setOperator(conditionOperators[counter]);
    filterTagList.add(taggingSearchCriteria);
    
    counter++;
   }
  }
  
  return filterTagList;
  
 }
 
 /**
  * Gets the filter tag list.
  * 
  * @param currentPage
  *         the current page
  * @param filters
  *         the filters
  * @param includeTagIdList
  *         the include tag id list
  * @return the filter tag list
  */
 public static List<TaggingSearchCriteria> getFilterTagList(Page currentPage, String[] filters, List<String> includeTagIdList) {
  String[] conditionOperators = new String[filters.length];
  for (int i = 0; i < filters.length; ++i) {
   conditionOperators[i] = "or";
  }
  return getFilterTagList(currentPage, filters, includeTagIdList, conditionOperators);
 }
 
 /**
  * Gets the filter tag list.
  * 
  * @param currentPage
  *         the current page
  * @param filterTagsNamespaces
  *         the filter tags namespaces
  * @param conditionOperators
  *         the condition operators
  * @return the filter tag list
  */
 public static List<TaggingSearchCriteria> getFilterTagList(Page currentPage, String[] filterTagsNamespaces, String[] conditionOperators) {
  return getFilterTagList(currentPage, filterTagsNamespaces, null, conditionOperators);
 }
 
 /**
  * Gets the filter tags namespace.
  * 
  * @param properties
  *         the properties
  * @param configMap
  *         the config map
  * @return the filter tags namespace
  */
 public static String[] getFilterTagsNamespace(Map<String, Object> properties, Map<String, String> configMap) {
  String[] filterTagsNamespace = properties != null && properties.get(UnileverConstants.FILTER_TAG_NAME_SPACE) != null
    ? (String[]) properties.get(UnileverConstants.FILTER_TAG_NAME_SPACE) : new String[] {};
  if (filterTagsNamespace.length == 0) {
   filterTagsNamespace = !StringUtils.isBlank(configMap.get(UnileverConstants.FILTER_TAG_NAME_SPACE))
     ? configMap.get(UnileverConstants.FILTER_TAG_NAME_SPACE).split(CommonConstants.COMMA_CHAR) : new String[] {};
  }
  return filterTagsNamespace;
 }
 
 /**
  * Gets the filter tags namespace.
  * 
  * @param properties
  *         the properties
  * @param configFilterTagNamespace
  *         the config filter tag namespace
  * @return the filter tags namespace
  */
 public static String[] getFilterTagsNamespace(Map<String, Object> properties, String configFilterTagNamespace) {
  String[] filterTagsNamespace = properties != null && properties.get(UnileverConstants.FILTER_TAG_NAME_SPACE) != null
    ? (String[]) properties.get(UnileverConstants.FILTER_TAG_NAME_SPACE) : new String[] {};
  if (filterTagsNamespace.length == 0) {
   filterTagsNamespace = !StringUtils.isBlank(configFilterTagNamespace) ? configFilterTagNamespace.split(CommonConstants.COMMA_CHAR)
     : new String[] {};
  }
  return filterTagsNamespace;
 }
 
 /**
  * Get the published date for the article.
  * 
  * @param page
  *         the page
  * @return the published date
  */
 public static Date getPublishedDate(Page page) {
  Date publishedDate = null;
  if (page != null) {
   ValueMap properties = page.getProperties();
   if (properties != null) {
    publishedDate = StringUtils.isNotBlank(MapUtils.getString(properties, UnileverConstants.PUBLISH_DATE))
      ? ((Calendar) properties.get(UnileverConstants.PUBLISH_DATE)).getTime() : null;
   }
  }
  
  return publishedDate;
 }
 
 /**
  * Gets the URL with updated protocol.
  * 
  * @param fullURL
  *         the full url
  * @param request
  *         the request
  * @return the URL with updated protocol
  */
 public static String getURLWithUpdatedProtocol(String fullURL, HttpServletRequest request) {
  String protocol = request.getHeader(UnileverConstants.X_FORWARD_PROTO_VAR) != null ? request.getHeader(UnileverConstants.X_FORWARD_PROTO_VAR)
    : "http";
  return fullURL.replaceAll("http://", protocol + "://");
 }
 
 /**
  * Gets the static file full path.
  * 
  * @param resourcePath
  *         the resource path
  * @param page
  *         the page
  * @return the static file full path
  */
 public static String getStaticFileFullPath(String resourcePath, Page page) {
  return getStaticFileFullPath(resourcePath, page, null);
 }
 
 /**
  * Gets the static file full path.
  * 
  * @param resourcePath
  *         the resource path
  * @param page
  *         the page
  * @param slingRequest
  *         the sling request
  * @return the static file full path
  */
 public static String getStaticFileFullPath(String resourcePath, Page page, SlingHttpServletRequest slingRequest) {
  LOGGER.debug("Inside getStaticFileFullPath of ComponentUtil");
  
  String fullPath = StringUtils.EMPTY;
  String localesourcePath = resourcePath;
  /* Need to decode the resourcePath as the resourcePath obtained form resource resolver is in encoded form for languages other than English */
  if (localesourcePath != null) {
   localesourcePath = getEncodedOrDecodeUrl(localesourcePath);
  }
  if (StringUtils.startsWith(localesourcePath, CommonConstants.SLASH_CHAR) && page != null) {
   ConfigurationService configurationService = page.adaptTo(ConfigurationService.class);
   ResourceResolver resourceResolver = page.getContentResource().getResourceResolver();
   Externalizer externalizer = page.getContentResource().getResourceResolver().adaptTo(Externalizer.class);
   if (externalizer == null) {
    LOGGER.error("Missing Externalizer service");
    return localesourcePath;
   } else {
    String externalizerKey = StringUtils.EMPTY;
    try {
     externalizerKey = configurationService.getConfigValue(page, BRAND, EXTERNALIZER_DOMAIN_KEY);
    } catch (ConfigurationNotFoundException ex) {
     LOGGER.warn("Exception while getting externalizer key for page : " + page.getPath() + ex.getMessage(),ex);
    }
    LOGGER.debug("ResourcePath = " + localesourcePath);
    if (!StringUtils.isBlank(externalizerKey)) {
     try {
      fullPath = externalizer.externalLink(resourceResolver, externalizerKey, localesourcePath);
     } catch (Exception e) {
      LOGGER.warn("Exception in getting the externalizer url " + e.getMessage(),e);
     }
    }
    
   }
  }
  if (slingRequest != null) {
   fullPath = getURLWithUpdatedProtocol(fullPath, slingRequest);
  }
  /* Need to decode the resourcePath again as the resourcePath obtained form externalizer is in encoded form for languages other than English */
  if (fullPath != null) {
   fullPath = getEncodedOrDecodeUrl(fullPath);
  }
  return StringUtils.isBlank(fullPath) ? localesourcePath : fullPath;
 }
 
 /**
  * Sets the page properties.
  * 
  * @param finalProperties
  *         the final properties
  * @param currentLandingPage
  *         the current landing page
  * @param currentPage
  *         the current page
  * @param slingRequest
  *         the slingRequest
  */
 public static void setPageProperties(Map<String, Object> finalProperties, Page currentLandingPage, Page currentPage,
   SlingHttpServletRequest slingRequest) {
  
  Map<String, Object> currentLandingPageProperties = new LinkedHashMap<String, Object>();
  if (currentLandingPage != null) {
   currentLandingPageProperties = currentLandingPage.getProperties();
  }
  
  String title = MapUtils.getString(currentLandingPageProperties, UnileverConstants.TEASER_TITLE);
  String description = MapUtils.getString(currentLandingPageProperties, UnileverConstants.TEASER_COPY);
  String imagePath = MapUtils.getString(currentLandingPageProperties, UnileverConstants.TEASER_IMAGE);
  String imageAltText = MapUtils.getString(currentLandingPageProperties, UnileverConstants.TEASER_IMAGE_ALT_TEXT);
  String longCopy = MapUtils.getString(currentLandingPageProperties, UnileverConstants.TEASER_LONG_COPY);
  if (StringUtils.isNotEmpty(imagePath)) {
   Image image = new Image(imagePath, imageAltText, title, currentPage, slingRequest);
   finalProperties.put(UnileverConstants.TEASER_IMAGE, image.convertToMap());
  } else {
   Image fallbackImage = new Image("", "", "", currentPage, slingRequest);
   finalProperties.put(UnileverConstants.TEASER_IMAGE, fallbackImage.convertToMap());
  }
  finalProperties.put(UnileverConstants.TEASER_TITLE, StringUtils.isNotBlank(title) ? title : StringUtils.EMPTY);
  finalProperties.put(UnileverConstants.TEASER_COPY, StringUtils.isNotBlank(description) ? description : StringUtils.EMPTY);
  finalProperties.put(UnileverConstants.TEASER_LONG_COPY, StringUtils.isNotBlank(longCopy) ? longCopy : StringUtils.EMPTY);
 }
 
 /**
  * Gets the propertery from config.
  * 
  * @param page
  *         the page
  * @param configurationService
  *         the configuration service
  * @param configCategory
  *         the config category
  * @param configKey
  *         the config key
  * @return the propertery from config
  */
 public static String getProperteryFromConfig(Page page, ConfigurationService configurationService, String configCategory, String configKey) {
  
  String value = StringUtils.EMPTY;
  try {
   value = configurationService.getConfigValue(page, configCategory, configKey);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error(configKey + "not found in global configurations", e);
  }
  return value;
 }
 
 /**
  * Map kritique product rating.
  * 
  * @param kqresponse
  *         the kqresponse
  * @param productPropertiesList
  *         the product properties list
  * @return the list
  */
 public static List<Map<String, Object>> mapKritiqueProductRating(List<Map<String, Object>> kqresponse,
   List<Map<String, Object>> productPropertiesList) {
  
  List<Map<String, Object>> productRatingPropertiesList = new ArrayList<Map<String, Object>>();
  String productSku = StringUtils.EMPTY;
  
  for (Map<String, Object> productList : productPropertiesList) {
   for (Map.Entry<String, Object> productListobject : productList.entrySet()) {
    String key = productListobject.getKey();
    Object value = productListobject.getValue();
    if (key.equals(ProductConstants.PRODUCT_ID)) {
     productSku = value.toString();
    }
    
   }
   for (Map<String, Object> Kqresponseobject : kqresponse) {
    
    for (Map.Entry<String, Object> entry : Kqresponseobject.entrySet()) {
     String key = entry.getKey();
     Object value = entry.getValue();
     if (key.equals(ProductConstants.SOURCE_ID) && value.equals(productSku)) {
      productList.put(ProductConstants.PRODUCT_RATINGS_MAP, Kqresponseobject);
      break;
     }
     
    }
    
   }
   
   productRatingPropertiesList.add(productList);
   
  }
  
  return productRatingPropertiesList;
  
 }
 
 /**
  * Gets the formatted date string.
  * 
  * @param date
  *         the date
  * @param format
  *         the format
  * @return the formatted date string
  */
 public static String getFormattedDateString(GregorianCalendar date, String format) {
  String tempFormat = format;
  if (tempFormat == null) {
   tempFormat = DEFAULT_DATE_FORMAT;
  }
  String value = StringUtils.EMPTY;
  try {
   DateFormat dateFormat = new SimpleDateFormat(tempFormat);
   if (date != null) {
    value = dateFormat.format(date.getTime());
   }
  } catch (IllegalArgumentException e) {
   LOGGER.error("Format string provided is not valid ", e);
  }
  return value;
 }
 
 /**
  * Gets the component position.
  * 
  * @param currentPage
  *         the currentPage
  * @param request
  *         the request
  * @return the component position
  */
 public static int getComponentPosition(Page currentPage, SlingHttpServletRequest request) {
  Resource jcrContentResource = currentPage.getContentResource();
  List<String> resourcesPathFullList = new ArrayList<String>();
  Iterator<Resource> childResourceItr = jcrContentResource.listChildren();
  while (childResourceItr.hasNext()) {
   resourcesPathFullList.addAll(getNonParChildResourceNameList(childResourceItr.next()));
  }
  
  return resourcesPathFullList.indexOf(request.getResource().getPath()) + 1;
 }
 
 /**
  * Gets the non par child resource name list.
  * 
  * @param resource
  *         the resource
  * @return the non par child resource name list
  */
 public static List<String> getNonParChildResourceNameList(Resource resource) {
  List<String> resourcePathList = new ArrayList<String>();
  String resType = resource.getResourceType();
  if (resType != null && !resType.endsWith("/parsys") && !resType.endsWith("/iparsys")) {
   resourcePathList.add(resource.getPath());
  }
  Iterator<Resource> childResourceItr = resource.listChildren();
  while (childResourceItr.hasNext()) {
   resourcePathList.addAll(getNonParChildResourceNameList(childResourceItr.next()));
  }
  return resourcePathList;
 }
 
 /**
  * Gets the list from string array.
  * 
  * @param array
  *         the array
  * 
  * @return the list from string array
  **/
 public static List<String> getListFromStringArray(String[] array) {
  List<String> list = new ArrayList<String>();
  if (array != null) {
   for (String str : array) {
    list.add(str);
   }
  }
  return list;
 }
 
 /**
  * Checks if is no index.
  * 
  * @param page
  *         the page
  * @return true, if is no index
  */
 public static boolean isNoIndex(Page page) {
  boolean noIndex = false;
  
  Resource resource = page.getContentResource();
  
  if (null != resource) {
   
   Node currentNode = resource.adaptTo(Node.class);
   try {
    if (null != currentNode && currentNode.hasProperty(NO_INDEX)) {
     noIndex = Boolean.valueOf(new JcrPropertyMap(currentNode).get(NO_INDEX, StringUtils.EMPTY));
     
    }
   } catch (RepositoryException e) {
    LOGGER.error("could not find property isIndexingEnabled for page :" + page.getPath(), e);
   }
  }
  return noIndex;
 }
 
 /**
  * Checks if is hide in sitemap.
  * 
  * @param page
  *         the page
  * @return true, if is hide in site map
  */
 public static boolean isPageHideInSiteMap(Page page) {
  boolean hideInSiteMap = false;
  
  Resource resource = page.getContentResource();
  
  if (null != resource) {
   
   Node currentNode = resource.adaptTo(Node.class);
   try {
    if (null != currentNode && currentNode.hasProperty("hideInSiteMap")) {
     hideInSiteMap = Boolean.valueOf(new JcrPropertyMap(currentNode).get("hideInSiteMap", StringUtils.EMPTY));
     
    }
   } catch (RepositoryException e) {
    LOGGER.error("could not find property isIndexingEnabled for page :" + page.getPath(), e);
   }
  }
  return hideInSiteMap;
 }
 
 /**
  * Gets Formatted Date by Config
  * 
  * @param configMap
  *         the configMap
  * @param dateString
  *         the dateString
  * @return formatted date
  */
 public static String getformattedDateByConfig(String dateString, Map<String, String> configMap) {
  String dateToStr = dateString;
  if (StringUtils.isNotBlank(dateString)) {
   String systemDateFormat = StringUtils.isBlank(configMap.get(SYS_DATE_FORMAT)) ? SYS_DATEFORMAT : configMap.get(SYS_DATE_FORMAT);
   String targetDateFormat = StringUtils.isBlank(configMap.get(DATE_FORMAT)) ? TARGET_DATEFORMAT : configMap.get(DATE_FORMAT);
   
   DateFormat format = new SimpleDateFormat(systemDateFormat);
   try {
    Date date = format.parse(dateString);
    DateFormat targetDateFmtter = new SimpleDateFormat(targetDateFormat);
    dateToStr = targetDateFmtter.format(date);
   } catch (ParseException pe) {
    LOGGER.warn("Error in date format : ", pe);
   }
  }
  return dateToStr;
 }
 
 /**
  * Gets the formatted date.
  * 
  * @param page
  *         the page
  * @param dateString
  *         the date string
  * @param configCatName
  *         the config cat name
  * @param date
  *         the date
  * @return the formatted date
  */
 public static String getformattedDateByConfig(Page page, String dateString, String configCatName) {
  Map<String, String> configMap = ComponentUtil.getConfigMap(page, configCatName);
  return getformattedDateByConfig(dateString, configMap);
 }
 
 /**
  * 
  * @param pagePath
  * @return
  */
 public static String getEncodedOrDecodeUrl(String pagePath) {
  String decodedPath = StringUtils.EMPTY;
  try {
   decodedPath = java.net.URLDecoder.decode(pagePath, "UTF-8");
  } catch (UnsupportedEncodingException e) {
   LOGGER.error("Error occured while decoding", e);
  }
  if (pagePath.length() == decodedPath.length()) {
   return pagePath;
  } else {
   return decodedPath;
  }
 }
 
 /**
  * 
  * @param currentPage
  * @param componentName
  * @return
  */
 public static Boolean isComponentExist(Page currentPage, String componentName) {
  Boolean exist = false;
  
  if (currentPage != null) {
   String[] split = StringUtils.split(currentPage.getPath(), "/");
   String homePagePath = "";
   if (split.length > THREE) {
    for (int x = 0; x < FOUR; x++) {
     homePagePath += "/" + split[x];
    }
   }
   PageManager pageManager = currentPage.getPageManager();
   Page homePage = pageManager.getPage(homePagePath);
   if (homePage != null) {
    Resource topLevelJcrContentResource = homePage.getContentResource();
    exist = checkChildResource(topLevelJcrContentResource);
   }
   if (FALSE.equals(exist)) {
    Resource jcrContentResource = currentPage.getContentResource();
    Iterator<Resource> resourceItr = jcrContentResource.listChildren();
    while (resourceItr.hasNext()) {
     Resource resource = resourceItr.next();
     String resType = resource.getResourceType();
     if (componentName.endsWith(resType)) {
      exist = true;
     }
     if(!exist){
      exist = checkChildResource(resource);
     }
     if (TRUE.equals(exist)) {
      break;
     }
    }
   }
  }
  return exist;
 }
 
 /**
  * Checks if is component exist.
  * 
  * @param currentPage
  *         the current page
  * @return the boolean
  */
 public static Boolean isComponentExist(Page currentPage) {
  return isComponentExist(currentPage, COMPONENT_LOGIN_REGISTER);
 }
 
 /**
  * Check child resource.
  * 
  * @param resource
  *         the resource
  * @return the boolean
  */
 private static Boolean checkChildResource(Resource resource) {
  Boolean exist = false;
  String resType = resource.getResourceType();
  if (resType != null) {
   Iterator<Resource> parchildResourceItr = resource.listChildren();
   while (parchildResourceItr.hasNext()) {
    Resource childresource = parchildResourceItr.next();
    String childResType = childresource.getResourceType();
    if (UnileverComponents.LOGIN_REGISTER.endsWith(childResType)) {
     exist = true;
    }
    if(!exist){
     exist = checkChildResource(childresource);
    }
    if (TRUE.equals(exist)) {
     break;
    }
   }
  }
  return exist;
  
 }
 
 /**
  * gets the updated include array from resources
  * 
  * @param resources
  *         the resources
  * @param featuredTags
  *         the featured tags
  * @param isAllowed
  *         the is allowed
  * @return the updated included array from resources
  */
 public static String[] getUpdatedIncludedArrayFromResources(final Map<String, Object> resources, String[] featuredTags, boolean isAllowed) {
  String[] featureTags = null;
  if (isAllowed) {
   String[] resolvedSegments = getResolvedSegments(resources);
   TagManager tagManager = BaseViewHelper.getTagManager(resources);
   if (resolvedSegments.length > 0) {
    if (featuredTags.length > 0) {
     List<String> includeAsList = Arrays.asList(featuredTags);
     List<String> segmentsAsList = Arrays.asList(resolvedSegments);
     featureTags = new String[featuredTags.length + resolvedSegments.length];
     featureTags = getIncludeArrayWithSegmetsTags(tagManager, includeAsList, segmentsAsList);
    } else {
     featureTags = new String[resolvedSegments.length];
     int count = 0;
     int arrayCount = 0;
     for (int k = 0; k < resolvedSegments.length; k++) {
      Tag tag = (tagManager != null) ? tagManager.resolve(resolvedSegments[k]) : null;
      if (tag != null) {
       featureTags[arrayCount++] = tag.getTagID();
      } else {
       count++;
       featureTags = (String[]) ArrayUtils.remove(featureTags, resolvedSegments.length - count);
      }
     }
    }
   }
  }
  return featureTags;
 }
 
 /**
  * removes the unresolved tags from resolved segments
  * 
  * @param include
  * @param resolvedSegments
  * @param tagManager
  * @param includeAsList
  * @param segmentsAsList
  * @param featuredTags
  * @param featuredAsList
  * @return
  */
 public static String[] getIncludeArrayWithSegmetsTags(TagManager tagManager, List<String> featuredAsList,
   List<String> segmentsAsList) {
  StringBuilder stringBuilder = new StringBuilder();
  StringBuilder stringBuilderTemp = new StringBuilder();
  /** Find the available tag corresponding to resolved segment and save it as string using string builder. */
  for (int j = 0; j < segmentsAsList.size(); j++) {
   Tag tag = (tagManager != null) ? tagManager.resolve(segmentsAsList.get(j)) : null;
   if (tag != null) {
    if (segmentsAsList.size() - 1 == j) {
     stringBuilder.append(tag.getTagID());
    } else {
     stringBuilder.append(tag.getTagID() + CommonConstants.COMMA_CHAR);
    }
   }
  }
  /** Convert featured tag list to comma separated string using string builder. */
  for (int j = 0; j < featuredAsList.size(); j++) {
   if (featuredAsList.size() - 1 == j) {
    stringBuilderTemp.append(featuredAsList.get(j));
   } else {
    stringBuilderTemp.append(featuredAsList.get(j) + CommonConstants.COMMA_CHAR);
   }
  }
  /** Get the final tags string. */
  if (StringUtils.isNotEmpty(stringBuilderTemp.toString())) {
   stringBuilder.append(CommonConstants.COMMA_CHAR + stringBuilderTemp.toString());
  }
  String tagString = stringBuilder.toString();
  /** Get the final tags string to tags Array. */
  String[] resolvedTags = (StringUtils.isNotEmpty(tagString)) ? tagString.split(CommonConstants.COMMA_CHAR) : new String[] {};
  return resolvedTags;
 }
 
 /**
  * gets the updated include array from resources
  * 
  * @param resources
  *         the resources
  * @param resolvedSegmentsFromCookies
  *         the resolvedSegmentsFromCookies
  * @return the resolved segments
  */
 public static String[] getResolvedSegments(final Map<String, Object> resources) {
  String[] resolvedSegments = new String[] {};
  String[] resolvedSeg = BaseViewHelper.getSlingRequest(resources).getRequestPathInfo().getSelectors();
  if (resolvedSeg.length > 0) {
   Page currentPage = BaseViewHelper.getCurrentPage(resources);
   BrandMarketBean bm = new BrandMarketBean(currentPage);
   String brandName = bm.getBrand();
   String[] resolvedSegmentsTemp = resolvedSeg;
   String[] finalResolvedSegments = new String[resolvedSegmentsTemp.length];
   for (int i = 0; i < resolvedSegmentsTemp.length; i++) {
    String lastSegment = resolvedSegmentsTemp[i].substring(resolvedSegmentsTemp[i].lastIndexOf(CommonConstants.SLASH_CHAR) + 1);
    if (StringUtils.isNotEmpty(brandName)) {
     finalResolvedSegments[i] = TAG_PRIMARY_PATH + brandName + SEGEMNT + lastSegment;
    } else {
     finalResolvedSegments[i] = TAG_SECONDARY_PATH + lastSegment;
    }
   }
   return finalResolvedSegments;
  }
  return resolvedSegments;
 }
 
 /**
  * This method returns the index of given tag from iterator.
  * 
  * @param iterator
  *         the iterator
  * @param tag
  *         the tag
  * @return index of tag in iterator.
  */
 public static int getIndexFromIterator(Iterator<?> iterator, Object tag) {
  int index = 0;
  while (iterator.hasNext()) {
   if (tag.equals(iterator.next())) {
    return index;
   }
   index++;
   
  }
  return -1;
 }
 
 /**
  * Put properties in map.
  *
  * @param componentMap the component map
  * @param sourceMap the source map
  * @param props the props
  */
 public static void putPropertiesInMap(Map<String, Object> componentMap, Map<String, Object> sourceMap, String... props){
     for(String prop:props){
     componentMap.put(prop, MapUtils.getObject(sourceMap, prop));
     }
 }
 
 /**
  * returns enableFeaturedTagInteraction value from searchConfiguration config
  * @param currentPage
  * @return {@link Boolean}
  */
 public static boolean isFeatureTagInteractionEnabled(Page currentPage){
  if(currentPage!=null){
  return Boolean.parseBoolean(GlobalConfigurationUtility.getValueFromConfiguration(currentPage.adaptTo(ConfigurationService.class), currentPage, "searchConfiguration", "enableFeaturedTagInteraction"));
  }else{
   return false;
  }
 }
 
 /**
  * 
  * @param resources
  * @param featureTagMap
  * @param tagIds
  */
 @SuppressWarnings("deprecation")
 public static List<FeatureTagsDTO> getFeatureTagList(Map<String, Object> resources, String[] tagIds){
  Page currentPage= BaseViewHelper.getCurrentPage(resources);
  TagManager tagManager = BaseViewHelper.getTagManager(resources);
  List<FeatureTagsDTO> featureTagList = new ArrayList<FeatureTagsDTO>();
  ConfigurationService configurationService = currentPage.adaptTo(ConfigurationService.class);
  for(String tagId:tagIds){
   Tag tag = tagManager.resolve(tagId);
   if(tag!=null){
    String searchPath=GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, "searchConfiguration", "searchListingV2Path");
    List<String> queryParams = new ArrayList<String>();
    BrandMarketBean brandMarketBean = new BrandMarketBean(currentPage);
    queryParams.add("Locale="+brandMarketBean.getMarket());
    queryParams.add("BrandName="+brandMarketBean.getBrand());
    queryParams.add("stags="+URLEncoder.encode(tag.getTagID()));
    queryParams.add("noesc");
    String url = getFullURL(BaseViewHelper.getResourceResolver(resources), searchPath, BaseViewHelper.getSlingRequest(resources));
    if(CollectionUtils.isNotEmpty(queryParams)){
     try {
      url = appendUri(url, queryParams.toArray(ArrayUtils.EMPTY_STRING_ARRAY));
     } catch (URISyntaxException e) {
      LOGGER.error("error while creating url"+e.getMessage()+" and the cause is"+e.getCause());
     }
    }
    FeatureTagsDTO dto = new FeatureTagsDTO(tag.getTitle(BaseViewHelper.getLocale(currentPage)), URLEncoder.encode(tag.getTagID()), url);
    featureTagList.add(dto);
   }
  }
  return featureTagList;
  
 }
 
 /**
  * 
  * @param uri
  * @param appendQuery
  * @return
  * @throws URISyntaxException
  */
 public static String appendUri(String uri, String... appendQueries) throws URISyntaxException {
  URI oldUri = new URI(uri);

  String newQuery = oldUri.getQuery();
  for (int i=0;i<appendQueries.length;i++) {
   if (i==0 && newQuery == null) {
    newQuery = appendQueries[i];
   } else {
    newQuery += "&" + appendQueries[i];
   }
  }
  URI newUri = new URI(oldUri.getScheme(), oldUri.getAuthority(),
    oldUri.getPath(), newQuery, oldUri.getFragment());
  return newUri.toString();
}

 /**
  * 
  * @param listToSort {@link List}
  * @param mapKey {@link String}
  * @param sortingOrder {@link List}
  * @return {@link ArrayList}
  */
 @SuppressWarnings("unchecked")
 public static List<Map<String, Object>> sortListByConfigOrder(List<Map<String, Object>> listToSort, String mapKey, List<String> sortingOrder){
  Map<String, Object> sortedmap = new LinkedHashMap<String, Object>();
  List<Map<String, Object>> sortedList = new LinkedList<Map<String,Object>>();
  
  for(Map<String, Object> data:listToSort){
   sortedmap.put(MapUtils.getString(data, mapKey), data);
  }
  for (String key : sortingOrder) {
   if(sortedmap.containsKey(key)){
    Map<String, Object> object = (Map<String, Object>) sortedmap.get(key);
   sortedList.add(object);
   listToSort.remove(object);
   }
  }
  sortedList.addAll(listToSort);
  return sortedList;
 }
 
 }
