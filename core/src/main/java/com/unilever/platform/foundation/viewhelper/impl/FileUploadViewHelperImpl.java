/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.sapient.platform.iea.aem.utils.constants.CommonConstants;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.servlets.FileUploadHelperServlet;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.FormElementConstants;

/**
 * This class prepares the data map for file upload component used for campaigning forms.
 */
@Component(description = "FileUploadViewHelperImpl", immediate = true, metatype = true, label = "FileUploadViewHelperImpl")
@Service(value = { FileUploadViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.CAMPAIGN_COMPONENT, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class FileUploadViewHelperImpl extends BaseViewHelper {
 
 /**
  * logger object.
  */
 private static final Logger LOGGER = LoggerFactory.getLogger(FileUploadViewHelperImpl.class);
 
 protected static final String SERVLET_PATH = "/bin/file/upload";
 
 public static final String NAME = "name";
 public static final String DESCRIPTION = "description";
 public static final String SIZECONSTRNT = "sizeConstraint";
 public static final String RULES = "fileSizeRules";
 public static final String SERVLETPATH = "servletPath";
 public static final String SIZEMSG = "sizeLimiterrorMsg";
 public static final String SIZELIMIT = "sizeLimit";
 public static final String MESSAGE = "msg";
 public static final String MSGFORMAT = "msgFormat";
 public static final String REQUIRED = "required";
 public static final String I18_SIZEMSG = "formFileUpload.sizeLimitErrorMsg";
 public static final String I18_REQMESSAGE = "formFileUpload.requiredErrorMsg";
 public static final String SIZE = "size";
 public static final String I18_FORMATMESSAGE = "formFileUpload.formatErrorMsg";
 public static final String LABEL = "label";
 public static final String LABELVALUE = "File upload";
 
 /** The Constant COUNTRY_SELECTOR. */
 private static final String FORM_FILEUPLOAD_CONFIGURATION = "formFileUpload";
 /** The configuration service. */
 @Reference
 private ConfigurationService configurationService;
 
 /**
  * <p>
  * This method returns image upload configuration map.
  * </p>
  * 
  * @param content
  *         {@link Map} of all user input received as input
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return Map holding the result data.
  **/
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  LOGGER.info("inside fileupload viewhelper processData");
  Map<String, Object> imageDataMap = new HashMap<String, Object>();
  Map<String, Object> data = new HashMap<String, Object>();
  Map<String, Object> properties = getProperties(content);
  String required = MapUtils.getString(properties, FormElementConstants.REQUIRED_FIELD);
  imageDataMap.put(NAME, MapUtils.getString(properties, FormElementConstants.ELEMENT_ID));
  imageDataMap.put(DESCRIPTION, MapUtils.getString(properties, DESCRIPTION));
  imageDataMap.put(SIZECONSTRNT, getimageSizeMap(resources));
  imageDataMap.put(RULES, getValidationMap(resources, required));
  imageDataMap.put(LABEL, LABELVALUE);
  imageDataMap.put(SERVLETPATH, FileUploadHelperServlet.SERVLET_PATH);
  String jsonNameSpace = MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE);
  data.put(jsonNameSpace, imageDataMap);
  LOGGER.debug("The Image data map returned is " + imageDataMap);
  return data;
 }
 
 /**
  * Gets the image size map.
  * 
  * @param i18n
  *         the i18n
  * @param sizeLimit
  *         the size limit
  * @return the image size map
  */
 private Map<String, Object> getimageSizeMap(final Map<String, Object> resources) {
  Map<String, Object> sizeConstraintMap = new HashMap<String, Object>();
  sizeConstraintMap.put(SIZEMSG, getI18n(resources).get(I18_SIZEMSG));
  String sizeLimit = "4";
  try {
   sizeLimit = configurationService.getConfigValue(getCurrentPage(resources), FORM_FILEUPLOAD_CONFIGURATION, SIZELIMIT);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find sizeLimit key in formFileUpload category", e);
  }
  
  sizeConstraintMap.put(SIZELIMIT, sizeLimit);
  return sizeConstraintMap;
  
 }
 
 /**
  * Gets the validation map.
  * 
  * @param i18n
  *         the i18n
  * @param required
  *         the required
  * @return the validation map
  */
 private Map<String, Object> getValidationMap(final Map<String, Object> resources, String required) {
  Map<String, Object> validationMap = new HashMap<String, Object>();
  validationMap.put(MESSAGE, getI18n(resources).get(I18_REQMESSAGE));
  validationMap.put(REQUIRED, required);
  validationMap.put(MSGFORMAT, getI18n(resources).get(I18_FORMATMESSAGE));
  return validationMap;
  
 }
 
}
