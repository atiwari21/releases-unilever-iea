/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.components.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.unilever.platform.aem.foundation.core.service.SearchService;
import com.unilever.platform.foundation.components.dto.FeatureTagsDTO;
import com.unilever.platform.foundation.components.dto.LandingAttributes;
import com.unilever.platform.foundation.constants.TaggingComponentsConstants;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.Article;
import com.unilever.platform.foundation.template.dto.Image;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * The Class LandingPageListHelper.
 */
public class LandingPageListHelper {
 
 /** The Constant FEATURE_CHILD_TAGS_FLAG. */
 private static final String FEATURE_CHILD_TAGS_FLAG = "featureChildTagsFlag";
 
 /** The Constant TRUE. */
 private static final String TRUE = "true";
 
 /** The Constant PAGES. */
 private static final String PAGES = "pages";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(LandingPageListHelper.class);
 
 /**
  * Instantiates a new tagging components helper.
  */
 private LandingPageListHelper() {
  
 }
 
 /**
  * Gets the articles map.
  * 
  * @param properties
  *         the properties
  * @param resources
  *         the resources
  * @param globalConfigMap
  *         the global config map
  * @param featureTagNameSpaces
  *         the feature tag name spaces
  * @param filters
  *         the filters
  * @param articleFlag
  *         the article flag
  * @return articlesMap
  */
 public static List<Map<String, Object>> getPagesList(Map<String, Object> properties, Map<String, Object> resources,
   Map<String, String> globalConfigMap, String[] featureTagNameSpaces, String[] filters, boolean articleFlag) {
  List<Map<String, Object>> pagesList = new LinkedList<Map<String, Object>>();
  Resource currentResource = BaseViewHelper.getCurrentResource(resources);
  ValueMap resProperties = currentResource.adaptTo(ValueMap.class);
  String componentName = resProperties.get("componentName", null);
  String[] tempFilters = filters;
  if (tempFilters == null) {
   tempFilters = new String[] {};
  }
  List<Map<String, String>> dataList = ComponentUtil.getNestedMultiFieldProperties(currentResource, PAGES);
  
  SlingHttpServletRequest request = BaseViewHelper.getSlingRequest(resources);
  
  Page currentPage = BaseViewHelper.getCurrentPage(resources);
  Map<String, Boolean> flagMap = getFlagMap(properties, globalConfigMap);
  if (!("relatedArticles".equals(componentName))) {
   flagMap.put(TaggingComponentsConstants.AUTHOR_NAME_FLAG, true);
  }
  for (Map<String, String> map : dataList) {
   if (TaggingComponentsConstants.TRUE.equals(MapUtils.getString(map, TaggingComponentsConstants.EXTERNAL_URL_REQUIRED, StringUtils.EMPTY))) {
    pagesList.add(getExternalPageData(map, currentPage, request));
   } else {
    Map<String, Boolean> pagePathWithWindowFlagMap = new HashMap<String, Boolean>();
    Boolean addFlag = true;
    pagePathWithWindowFlagMap.put(MapUtils.getString(map, "url", ""), isOpneInNewWindow(map));
    boolean featureChildTagsFlag = MapUtils.getBooleanValue(properties, FEATURE_CHILD_TAGS_FLAG, false);
    List<Map<String, Object>> list = LandingPageListHelper.getLandingPageListData(pagePathWithWindowFlagMap, resources, flagMap, globalConfigMap,
      featureTagNameSpaces, tempFilters, articleFlag, featureChildTagsFlag);
    if (TRUE.equals(request.getParameter(UnileverConstants.VIDEO_FILTER))) {
     String subContentType = MapUtils.getString(list.get(0), UnileverConstants.SUB_CONTENT_TYPE, StringUtils.EMPTY);
     addFlag = "video".equals(subContentType) ? true : false;
    }
    if (addFlag) {
     pagesList.addAll(list);
    }
   }
  }
  return pagesList;
 }
 
 /**
  * Gets the limit.
  * 
  * @param properties
  *         the properties
  * @param globalConfigMap
  *         the global config map
  * @return the limit
  */
 public static int getLimit(Map<String, Object> properties, Map<String, String> globalConfigMap) {
  int limit = 0;
  boolean overrideGlobalConfig = MapUtils.getBooleanValue(properties, UnileverConstants.OVERRIDE_GLOBAL_CONFIG, false);
  if (overrideGlobalConfig) {
   limit = MapUtils.getIntValue(properties, "limit", 0);
  } else {
   String limitStr = MapUtils.getString(globalConfigMap, "limit", "0");
   try {
    limit = Integer.parseInt(limitStr);
   } catch (NumberFormatException e) {
    LOGGER.warn("limit in config is not a number ", e);
   }
  }
  return limit;
 }
 
 /**
  * Gets the flag map.
  * 
  * @param properties
  *         the properties
  * @param globalConfigMap
  *         the global config map
  * @return the flag map
  */
 public static Map<String, Boolean> getFlagMap(Map<String, Object> properties, Map<String, String> globalConfigMap) {
  Map<String, Boolean> flagMap = new HashMap<String, Boolean>();
  Map<String, Object> tempMap = new HashMap<String, Object>();
  boolean overrideGlobalConfig = MapUtils.getBooleanValue(properties, UnileverConstants.OVERRIDE_GLOBAL_CONFIG, false);
  
  if (overrideGlobalConfig) {
   tempMap.putAll(properties);
  } else {
   tempMap.putAll(globalConfigMap);
  }
  flagMap.put(TaggingComponentsConstants.IMAGE_FLAG, MapUtils.getBoolean(tempMap, TaggingComponentsConstants.TEASER_IMAGE_FLAG, false));
  flagMap.put(TaggingComponentsConstants.COPY_FLAG, MapUtils.getBoolean(tempMap, TaggingComponentsConstants.TEASER_COPY_FLAG, false));
  flagMap.put(TaggingComponentsConstants.LONG_COPY_FLAG, MapUtils.getBoolean(tempMap, TaggingComponentsConstants.TEASER_LONG_COPY_FLAG, false));
  flagMap.put(TaggingComponentsConstants.PUBLISH_DATE_FLAG, MapUtils.getBoolean(tempMap, TaggingComponentsConstants.TEASER_PUBLISH_DATE_FLAG, false));
  flagMap.put(TaggingComponentsConstants.READ_TIME_FLAG, MapUtils.getBoolean(tempMap, TaggingComponentsConstants.READ_TIME_FLAG, false));
  flagMap.put(TaggingComponentsConstants.AUTHOR_NAME_FLAG, MapUtils.getBoolean(tempMap, TaggingComponentsConstants.AUTHOR_NAME_FLAG, false));
  flagMap.put(TaggingComponentsConstants.PARETNT_PAGE_DETAILS_FLAG,
    MapUtils.getBoolean(tempMap, TaggingComponentsConstants.PARENT_DETAILS_FLAG, false));
  return flagMap;
 }
 
 /**
  * Gets the external page data.
  * 
  * @param dataMap
  *         the data map
  * @param currentPage
  *         the current page
  * @param request
  *         the request
  * @return the external page data
  */
 public static Map<String, Object> getExternalPageData(Map<String, String> dataMap, Page currentPage, SlingHttpServletRequest request) {
  Map<String, Object> map = new LinkedHashMap<String, Object>();
  String url = MapUtils.getString(dataMap, "url", "");
  String title = MapUtils.getString(dataMap, UnileverConstants.TEASER_TITLE, StringUtils.EMPTY);
  String imagePath = MapUtils.getString(dataMap, UnileverConstants.TEASER_IMAGE, StringUtils.EMPTY);
  String altImage = MapUtils.getString(dataMap, UnileverConstants.TEASER_IMAGE_ALT_TEXT, StringUtils.EMPTY);
  Image image = new Image(imagePath, altImage, title, currentPage, request);
  
  String copyText = MapUtils.getString(dataMap, UnileverConstants.TEASER_COPY, StringUtils.EMPTY);
  String longCopy = MapUtils.getString(dataMap, UnileverConstants.TEASER_LONG_COPY, StringUtils.EMPTY);
  
  map.put("url", url);
  map.put("title", title);
  map.put("image", image);
  map.put("copyText", copyText);
  map.put("longCopy", longCopy);
  map.put(UnileverConstants.OPEN_IN_NEW_WINDOW, isOpneInNewWindow(dataMap));
  return map;
 }
 
 /**
  * Checks if is opne in new window.
  * 
  * @param dataMap
  *         the data map
  * @return true, if is opne in new window
  */
 private static boolean isOpneInNewWindow(Map<String, String> dataMap) {
  String openInNewWindow = MapUtils.getString(dataMap, UnileverConstants.OPEN_IN_NEW_WINDOW, StringUtils.EMPTY);
  boolean flag = false;
  if (TaggingComponentsConstants.TRUE.equals(openInNewWindow) || "[\"true\"]".equals(openInNewWindow)) {
   flag = true;
  }
  return flag;
 }
 
 /**
  * Gets the landing page list data.
  * 
  * @param pagePathWithWindowFlagMap
  *         the page path list
  * @param resources
  *         the resources
  * @param flagMap
  *         the flag map
  * @param globalConfigMap
  *         the global config map
  * @param featureTagNameSpaces
  *         the feature tag name spaces
  * @param filters
  *         the filters
  * @param articleFlag
  *         the article flag
  * @return the landing page list data
  */
 public static List<Map<String, Object>> getLandingPageListData(Map<String, Boolean> pagePathWithWindowFlagMap, Map<String, Object> resources,
   Map<String, Boolean> flagMap, Map<String, String> globalConfigMap, String[] featureTagNameSpaces, String[] filters, boolean articleFlag) {
  
  return getLandingPageListData(pagePathWithWindowFlagMap, resources, flagMap, globalConfigMap, featureTagNameSpaces, filters, articleFlag, false);
 }
 
 /**
  * Gets the landing page list data.
  * 
  * @param pagePathWithWindowFlagMap
  *         the page path list
  * @param resources
  *         the resources
  * @param flagMap
  *         the flag map
  * @param globalConfigMap
  *         the global config map
  * @param featureTagNameSpaces
  *         the feature tag name spaces
  * @param filters
  *         the filters
  * @param articleFlag
  *         the article flag
  * @param childTagsFlag
  *         the child tags flag
  * @return the landing page list data
  */
 public static List<Map<String, Object>> getLandingPageListData(Map<String, Boolean> pagePathWithWindowFlagMap, Map<String, Object> resources,
   Map<String, Boolean> flagMap, Map<String, String> globalConfigMap, String[] featureTagNameSpaces, String[] filters, boolean articleFlag,
   boolean childTagsFlag) {
  List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
  Iterator<String> pagePathItr = pagePathWithWindowFlagMap.keySet().iterator();
  while (pagePathItr.hasNext()) {
   String pagePath = pagePathItr.next();
   Boolean openInNewWindow = pagePathWithWindowFlagMap.get(pagePath);
   Page page = BaseViewHelper.getPageManager(resources).getPage(pagePath);
   SlingHttpServletRequest slingRequest = BaseViewHelper.getSlingRequest(resources);
   if (page != null && TaggingComponentsHelper.isEligbleFromFilter(page, filters)) {
    Map<String, Object> map = new HashMap<String, Object>();
    if (articleFlag) {
     Article dto = new Article();
     dto.setData(page, slingRequest);
     map = dto.convertToMap(flagMap);
    } else {
     LandingAttributes dto = new LandingAttributes(page, slingRequest, globalConfigMap);
     map = dto.convertToMap(flagMap);
    }
    map.put("pagePath", page.getPath());
    if (openInNewWindow != null) {
     map.put(UnileverConstants.OPEN_IN_NEW_WINDOW, openInNewWindow);
    }
    addFeatureTagsData(featureTagNameSpaces, map, page, resources, childTagsFlag);
    list.add(map);
   }
  }
  return list;
 }
 
 /**
  * Adds the feature tags data.
  * 
  * @param featureTagNameSpaces
  *         the feature tag name spaces
  * @param dataMap
  *         the article map
  * @param page
  *         the article page
  * @param resources
  *         the resources
  */
 public static void addFeatureTagsData(String[] featureTagNameSpaces, Map<String, Object> dataMap, Page page, Map<String, Object> resources) {
  addFeatureTagsData(featureTagNameSpaces, dataMap, page, resources, false);
  
 }
 
 /**
  * Adds the feature tags data.
  * 
  * @param featureTagNameSpaces
  *         the feature tag name spaces
  * @param dataMap
  *         the article map
  * @param page
  *         the article page
  * @param resources
  *         the resources
  * @param childTagsFlag
  *         the child tags flag
  */
 public static void addFeatureTagsData(String[] featureTagNameSpaces, Map<String, Object> dataMap, Page page, Map<String, Object> resources,
   boolean childTagsFlag) {
  List<String> featuretTagsApplied = new LinkedList<String>();
  List<FeatureTagsDTO> appliedFeaturesTagList = new ArrayList<FeatureTagsDTO>();
  boolean isFeatureTagInteractionTrue=ComponentUtil.isFeatureTagInteractionEnabled(BaseViewHelper.getCurrentPage(resources));
  boolean flag = false;
  TagManager tagManager = BaseViewHelper.getTagManager(resources);
  Page currentPage = BaseViewHelper.getCurrentPage(resources);
  if (featureTagNameSpaces != null && featureTagNameSpaces.length > 0) {
   List<String> tagIdList = TaggingComponentsHelper.getQualifiedTagListFromPage(featureTagNameSpaces, page, childTagsFlag, resources);
   Iterator<String> itr = tagIdList.iterator();
   while (itr.hasNext()) {
    String tagId = itr.next();
    Tag tag = tagManager.resolve(tagId);
    featuretTagsApplied.add(tag.getTitle(currentPage.getLanguage(false)));
   }
   flag = featuretTagsApplied.isEmpty() ? false : true;
   String[] tagIdArray=tagIdList!=null?new String[tagIdList.size()]:ArrayUtils.EMPTY_STRING_ARRAY;
   tagIdArray=tagIdList!=null?(String[]) tagIdList.toArray(tagIdArray):ArrayUtils.EMPTY_STRING_ARRAY;
   appliedFeaturesTagList=ComponentUtil.getFeatureTagList(resources, tagIdArray);
  }
  dataMap.put("isfeatureTagsPresent", flag);
  dataMap.put("appliedFeatureTags", featuretTagsApplied.toArray());
  dataMap.put(UnileverConstants.IS_ENABLED_FEATURE_TAG_INTERACTION, isFeatureTagInteractionTrue);
  if(isFeatureTagInteractionTrue){
  dataMap.put("interactionFeatureTags", appliedFeaturesTagList.toArray());
  }
  
 }
 
 /**
  * Gets the tagged page data list.
  * 
  * @param resources
  *         the resources
  * @param globalConfigMap
  *         the global config map
  * @param properties
  *         the properties
  * @param searchService
  *         the search service
  * @param articleFlag
  *         the article flag
  * @return the tagged page data list
  */
 public static List<Map<String, Object>> getTaggedPageDataList(Map<String, Object> resources, Map<String, String> globalConfigMap,
   Map<String, Object> properties, SearchService searchService, boolean articleFlag) {
  
  return getTaggedPageDataList(resources, globalConfigMap, properties, null, null, searchService, articleFlag);
 }
 
 /**
  * Gets the tags map.
  * 
  * @param resources
  *         the resources
  * @param globalConfigMap
  *         the global config map
  * @param properties
  *         the properties
  * @param featureTagNameSpaces
  *         the feature tag name spaces
  * @param filters
  *         the filters
  * @param searchService
  *         the search service
  * @param articleFlag
  *         the article flag
  * @return the tags map
  */
 public static List<Map<String, Object>> getTaggedPageDataList(Map<String, Object> resources, Map<String, String> globalConfigMap,
   Map<String, Object> properties, String[] featureTagNameSpaces, String[] filters, SearchService searchService, boolean articleFlag) {
  
  List<String> propertyNames = new ArrayList<String>();
  List<String> propValueList = new ArrayList<String>();
  
  propertyNames.add(UnileverConstants.CONTENT_TYPE);
  Resource currentResource = BaseViewHelper.getCurrentResource(resources);
  HttpServletRequest slingRequest = BaseViewHelper.getSlingRequest(resources);
  ValueMap resProperties = currentResource.adaptTo(ValueMap.class);
  String componentName = resProperties.get("componentName", null);
  boolean featureChildTagsFlag = MapUtils.getBooleanValue(properties, FEATURE_CHILD_TAGS_FLAG, false);
  List<Map<String, String>> contentTypeList = ComponentUtil.getNestedMultiFieldProperties(BaseViewHelper.getCurrentResource(resources),
    UnileverConstants.CONTENT_TYPE_JSON);
  for (Map<String, String> contentType : contentTypeList) {
   if (StringUtils.isNotBlank(MapUtils.getString(contentType, UnileverConstants.CONTENT_TYPE))) {
    propValueList.add(MapUtils.getString(contentType, UnileverConstants.CONTENT_TYPE));
   }
  }
  if (CollectionUtils.isEmpty(propValueList)) {
   propValueList.add(MapUtils.getString(globalConfigMap, UnileverConstants.CONTENT_TYPE, StringUtils.EMPTY));
  }
  
  Map<String, List<String>> propMap = new HashMap<String, List<String>>();
  propMap.put(UnileverConstants.JCR_CONTENT_CONTENT_TYPE, propValueList);
  if (TRUE.equals(slingRequest.getParameter(UnileverConstants.VIDEO_FILTER))) {
   List<String> subContentTypes = new ArrayList<String>();
   subContentTypes.add("video");
   propMap.put(UnileverConstants.JCR_SUB_CONTENT_CONTENT_TYPE, subContentTypes);
  }
  Page currentPage = BaseViewHelper.getCurrentPage(resources);
  
  PageManager pageManager = BaseViewHelper.getPageManager(resources);
  List<String> taggedPageList = TaggingComponentsHelper.getTaggedPageList(properties, globalConfigMap, currentPage, pageManager, propMap,
    searchService, resources);
  Map<String, Boolean> pagePathWithWindowFlagMap = getPagePathWithWindowFlagMap(taggedPageList);
  Map<String, Boolean> flagMap = getFlagMap(properties, globalConfigMap);
  if (!("relatedArticles".equals(componentName))) {
   flagMap.put(TaggingComponentsConstants.AUTHOR_NAME_FLAG, true);
  }
  return getLandingPageListData(pagePathWithWindowFlagMap, resources, flagMap, globalConfigMap, featureTagNameSpaces, filters, articleFlag,
    featureChildTagsFlag);
 }
 
 /**
  * Gets the page path with window flag map.
  * 
  * @param taggedPageList
  *         the tagged page list
  * @return the page path with window flag map
  */
 private static Map<String, Boolean> getPagePathWithWindowFlagMap(List<String> taggedPageList) {
  Map<String, Boolean> pagePathWithWindowFlagMap = new LinkedHashMap<String, Boolean>();
  Iterator<String> itr = taggedPageList.iterator();
  while (itr.hasNext()) {
   String pagePath = itr.next();
   pagePathWithWindowFlagMap.put(pagePath, null);
  }
  return pagePathWithWindowFlagMap;
 }
 
 /**
  * sub list the total result based on paginagtion and filters applied
  * 
  * @param articlePagePathList
  * @param pageNo
  * @param itemsPerPage
  * @return
  */
 public static List<Map<String, Object>> getSubListByPage(List<Map<String, Object>> articlePagePathList, int pageNo, int itemsPerPage) {
  if (pageNo <= 0) {
   return articlePagePathList;
  } else {
   int startIndex = (pageNo * itemsPerPage) - itemsPerPage;
   int endIndex = pageNo * itemsPerPage;
   int size = articlePagePathList.size();
   if (startIndex < size && endIndex >= size) {
    endIndex = size;
   }
   if (startIndex == endIndex) {
    List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
    if (CollectionUtils.isNotEmpty(articlePagePathList)) {
     list.add(articlePagePathList.get(startIndex));
    }
    
    return list;
   }
   return endIndex <= size ? articlePagePathList.subList(startIndex, endIndex) : null;
   
  }
 }
 
 /**
  * returns the map of filters
  * 
  * @param properties
  * @param resources
  * @param articlePagePathList
  * @param configMap
  * @param currentPage
  * @param tagManager
  * @param cuResource
  * @param gloablConfigCatName
  * @return
  */
 public static Map<String, Object> getFilterProperties(Map<String, Object> properties, List<Map<String, Object>> articlePagePathList,
   Page currentPage, TagManager tagManager, Resource cuResource, String gloablConfigCatName) {
  Map<String, String> configMap = ComponentUtil.getConfigMap(currentPage, gloablConfigCatName);
  List<Map<String, String>> filterList = ComponentUtil.getNestedMultiFieldProperties(cuResource, UnileverConstants.FILTER_OPTION_JSON);
  String resultSuffixText = MapUtils.getString(properties, UnileverConstants.RESULT_SUFFIX_TEXT, StringUtils.EMPTY);
  resultSuffixText = StringUtils.isNotBlank(resultSuffixText) ? resultSuffixText : MapUtils.getString(configMap,
    UnileverConstants.RESULT_SUFFIX_TEXT, StringUtils.EMPTY);
  String filterParentHeading = MapUtils.getString(properties, UnileverConstants.FILTER_PARENT_HEADING, StringUtils.EMPTY);
  filterParentHeading = StringUtils.isNotBlank(filterParentHeading) ? filterParentHeading : MapUtils.getString(configMap,
    UnileverConstants.FILTER_PARENT_HEADING, StringUtils.EMPTY);
  Map<String, Object> filterMaps = new LinkedHashMap<String, Object>();
  filterMaps.put(UnileverConstants.RESULT_SUFFIX, resultSuffixText);
  filterMaps.put(UnileverConstants.HEADING, filterParentHeading);
  List<Map<String, Object>> parentFilterList = new LinkedList<Map<String, Object>>();
  if (CollectionUtils.isNotEmpty(filterList) && CollectionUtils.isNotEmpty(articlePagePathList)) {
   setFiltersListMap(articlePagePathList, currentPage, filterList, parentFilterList, false, tagManager);
  } else if (CollectionUtils.isNotEmpty(articlePagePathList)) {
   List<Map<String, String>> globalConfigFiltersList = getFilterFromGlobalConfig(configMap);
   setFiltersListMap(articlePagePathList, currentPage, globalConfigFiltersList, parentFilterList, true, tagManager);
  } else {
   getDefaultBlankFilter(parentFilterList);
  }
  filterMaps.put(UnileverConstants.FILTER_OPTIONS, parentFilterList);
  return filterMaps;
 }
 
 /**
  * Sets the filters list map.
  * 
  * @param articlePagePathList
  *         the article page path list
  * @param currentPage
  *         the current page
  * @param filterList
  *         the filter list
  * @param parentFilterList
  *         the parent filter list
  * @param isFiltersFromConfig
  *         the is filters from config
  * @param tagManager
  *         the tag manager
  */
 private static void setFiltersListMap(List<Map<String, Object>> articlePagePathList, Page currentPage, List<Map<String, String>> filterList,
   List<Map<String, Object>> parentFilterList, boolean isFiltersFromConfig, TagManager tagManager) {
  for (Map<String, String> filterMap : filterList) {
   Map<String, Object> newMap = new LinkedHashMap<String, Object>();
   String filterHeading = MapUtils.getString(filterMap, UnileverConstants.FILTER_HEADING, StringUtils.EMPTY);
   String filterSubHeading = MapUtils.getString(filterMap, UnileverConstants.FILTER_SUB_HEADING, StringUtils.EMPTY);
   String displayFormat = MapUtils.getString(filterMap, UnileverConstants.FILTER_DISPLAY_FORMAT, StringUtils.EMPTY);
   String defaultValue = MapUtils.getString(filterMap, UnileverConstants.FILTER_DEFAULT_VALUE, StringUtils.EMPTY);
   String cssClass = MapUtils.getString(filterMap, UnileverConstants.CSS_STYLE_CLASS, StringUtils.EMPTY);
   newMap.put(UnileverConstants.HEADING, filterHeading);
   newMap.put(UnileverConstants.SUB_HEADING, filterSubHeading);
   newMap.put(UnileverConstants.DISPLAY_FORMAT, displayFormat);
   newMap.put(UnileverConstants.DEFAULT_VALUE, defaultValue);
   newMap.put(UnileverConstants.CLASS, cssClass);
   String parentTagKey = (isFiltersFromConfig) ? UnileverConstants.FILTER_PARENT_TAG : UnileverConstants.FILTER_PARENT_TAG_ONE;
   String parentFilterTag = MapUtils.getString(filterMap, parentTagKey, StringUtils.EMPTY);
   if (isFiltersFromConfig) {
    Tag presentTag = tagManager.resolve(parentFilterTag.replace(" ", "").toLowerCase());
    parentFilterTag = (presentTag != null) ? presentTag.getTagID() : parentFilterTag.replace(" ", "").toLowerCase();
   }
   Set<Map<String, Object>> childFilterSet = new HashSet<Map<String, Object>>();
   
   setFilterList(articlePagePathList, parentFilterTag, childFilterSet, currentPage.getPageManager());
   newMap.put(UnileverConstants.FILTERS, childFilterSet.toArray());
   parentFilterList.add(newMap);
  }
 }
 
 /**
  * gets the default blank filters if config is empty for filters
  * 
  * @param parentFilterList
  */
 private static void getDefaultBlankFilter(List<Map<String, Object>> parentFilterList) {
  Map<String, Object> newMap = new LinkedHashMap<String, Object>();
  Set<Map<String, Object>> childFilterSet = new HashSet<Map<String, Object>>();
  newMap.put(UnileverConstants.FILTERS, childFilterSet.toArray());
  parentFilterList.add(newMap);
 }
 
 /**
  * gets the list of filters map from global config
  * 
  * @param configMap
  * @return list of maps
  */
 private static List<Map<String, String>> getFilterFromGlobalConfig(Map<String, String> configMap) {
  List<Map<String, String>> globalConfigFiltersList = new LinkedList<Map<String, String>>();
  int filterCount = 1;
  Set<String> filterKeySet = configMap.keySet();
  for (int j = 0; j < filterCount; j++) {
   if (isExtraNumberOfFilters(filterCount, filterKeySet)) {
    filterCount++;
   }
  }
  Map<String, String> filteringMap = new LinkedHashMap<String, String>();
  
  for (int i = 1; i < filterCount; i++) {
   filteringMap = new LinkedHashMap<String, String>();
   String filterKeyHeading = getFilterKey(UnileverConstants.FILTER_HEADING, i);
   String filterKeySubHeading = getFilterKey(UnileverConstants.FILTER_SUB_HEADING, i);
   String filterKeyParentTag = getFilterKey(UnileverConstants.FILTER_PARENT_TAG, i);
   String filterDisplayFormat = getFilterKey(UnileverConstants.FILTER_DISPLAY_FORMAT, i);
   String filterDefaultValue = getFilterKey(UnileverConstants.FILTER_DEFAULT_VALUE, i);
   String filterCssStyleClass = getFilterKey(UnileverConstants.CSS_STYLE_CLASS, i);
   filteringMap.put(UnileverConstants.FILTER_HEADING, MapUtils.getString(configMap, filterKeyHeading, StringUtils.EMPTY));
   filteringMap.put(UnileverConstants.FILTER_SUB_HEADING, MapUtils.getString(configMap, filterKeySubHeading, StringUtils.EMPTY));
   filteringMap.put(UnileverConstants.FILTER_PARENT_TAG, MapUtils.getString(configMap, filterKeyParentTag, StringUtils.EMPTY));
   filteringMap.put(UnileverConstants.FILTER_DISPLAY_FORMAT, MapUtils.getString(configMap, filterDisplayFormat, StringUtils.EMPTY));
   filteringMap.put(UnileverConstants.FILTER_DEFAULT_VALUE, MapUtils.getString(configMap, filterDefaultValue, StringUtils.EMPTY));
   filteringMap.put(UnileverConstants.CSS_STYLE_CLASS, MapUtils.getString(configMap, filterCssStyleClass, StringUtils.EMPTY));
   globalConfigFiltersList.add(filteringMap);
  }
  return globalConfigFiltersList;
 }
 
 /**
  * @param filterCount
  * @param filterKeySet
  * @return
  */
 private static boolean isExtraNumberOfFilters(int filterCount, Set<String> filterKeySet) {
  boolean flag = false;
  if (filterKeySet.contains(UnileverConstants.FILTERS + UnileverConstants.DOT + String.valueOf(filterCount) + UnileverConstants.DOT
    + UnileverConstants.FILTER_PARENT_TAG)) {
   flag = true;
  }
  return flag;
 }
 
 /**
  * returns the list of map with tag id and label
  * 
  * @param resources
  * @param articlePagePathList
  * @param parentFilterTag
  * @param childFilterSet
  * @param pageManager
  * @return
  */
 public static void setFilterList(List<Map<String, Object>> articlePagePathList, String parentFilterTag, Set<Map<String, Object>> childFilterSet,
   PageManager pageManager) {
  
  for (Map<String, Object> articlePageMap : articlePagePathList) {
   String pagePath = MapUtils.getString(articlePageMap, UnileverConstants.PAGE_PATH, StringUtils.EMPTY);
   Page page = pageManager.getPage(pagePath);
   if (page != null && StringUtils.isNotBlank(parentFilterTag)) {
    Tag[] pageTags = page.getTags();
    for (Tag pageTag : pageTags) {
     String tagId = pageTag.getTagID();
     if (tagId.startsWith(parentFilterTag) && !tagId.equals(parentFilterTag)) {
      Map<String, Object> map = new LinkedHashMap<String, Object>();
      map.put(UnileverConstants.LABEL, pageTag.getTitle(page.getLanguage(false)));
      map.put(UnileverConstants.VALUE, tagId);
      childFilterSet.add(map);
     }
     
    }
   }
  }
 }
 
 /**
  * gets the appended long string for getting key value
  * 
  * @param key
  * @param filterCount
  * @return
  */
 public static String getFilterKey(String key, int filterCount) {
  StringBuilder stringBuilder = new StringBuilder();
  return stringBuilder.append(UnileverConstants.FILTERS).append(UnileverConstants.DOT).append(String.valueOf(filterCount))
    .append(UnileverConstants.DOT).append(key).toString();
 }
 
}
