/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.jcr.Session;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.jcr.resource.JcrResourceConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.i18n.I18n;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.constants.CommonConstants;
import com.unilever.platform.aem.foundation.core.service.SearchService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.dto.LandingAttributes;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.LandingPageListHelper;
import com.unilever.platform.foundation.components.helper.TaggingComponentsHelper;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.productoverview.config.ConfigureProductImageRenditionService;

/**
 * <p>
 * Responsible for reading author input for prduct copy component and generating a list of collapsible sections setting in pageContext for the view.
 * Input can be manual or reading property attributes from product data.
 * </p>
 */
@Component(description = "PageListingViewHelperImpl", immediate = true, metatype = true, label = "PageListingViewHelperImpl")
@Service(value = { PageListingV2ViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.PAGE_LISTING_V2, propertyPrivate = true),
  
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class PageListingV2ViewHelperImpl extends RelatedArticlesViewHelperImpl {
 
 /** The Constant SECTION_SUMMARY. */
 private static final String SECTION_SUMMARY = "sectionSummary";
 
 /** The Constant RESOURCE_COUNT. */
 private static final String RESOURCE_COUNT = "resourceCount";
 
 /** The Constant PAGE_COUNT. */
 private static final String PAGE_COUNT = "pageCount";
 
 /** The Constant CONFIGURE_RESOURCES_JSON. */
 private static final String CONFIGURE_RESOURCES_JSON = "configureResourcesJson";
 
 /** The Constant COMPONENTS_RESOURCE_LISTING. */
 private static final String COMPONENTS_RESOURCE_LISTING = "%components/resourceListing";
 
 /** The Constant LIKE. */
 private static final String LIKE = "like";
 
 /** The Constant PROPERTY_VALUE. */
 private static final String PROPERTY_VALUE = "property.value";
 
 /** The Constant PROPERTY_OPERATION. */
 private static final String PROPERTY_OPERATION = "property.operation";
 
 /** The Constant CONTENT_TYPE. */
 private static final String CONTENT_TYPE = "contentType";
 
 /** The Constant CQ_PAGE_CONTENT. */
 private static final String CQ_PAGE_CONTENT = "cq:PageContent";
 
 /** The Constant PROPERTY. */
 private static final String PROPERTY = "property";
 
 /** The Constant TYPE. */
 private static final String TYPE = "type";
 
 /** The Constant PAGE_LISTING_PAGE_SUMMARY_HEADING_TEXT. */
 private static final String PAGE_LISTING_PAGE_SUMMARY_HEADING_TEXT = "pageListing.pageSummaryHeadingText";
 
 /** The Constant RESOURCE_COUNT_LABEL_TEXT. */
 private static final String RESOURCE_COUNT_LABEL_TEXT = "resourceCountLabelText";
 
 /** The Constant PAGE_COUNT_LABEL_TEXT. */
 private static final String PAGE_COUNT_LABEL_TEXT = "pageCountLabelText";
 
 /** The Constant SUMMARY_HEADING_TEXT. */
 private static final String SUMMARY_HEADING_TEXT = "summaryHeadingText";
 
 /** The Constant PAGE_LISTING_RESOURCES_COUNT_LABEL_TEXT. */
 private static final String PAGE_LISTING_RESOURCES_COUNT_LABEL_TEXT = "pageListing.resourcesCountLabelText";
 
 /** The Constant PAGE_LISTING_PAGE_COUNT_LABEL_TEXT. */
 private static final String PAGE_LISTING_PAGE_COUNT_LABEL_TEXT = "pageListing.pageCountLabelText";
 
 /** The Constant RESOURCE_COUNT_LABEL. */
 private static final String RESOURCE_COUNT_LABEL = "resourceCountLabel";
 
 /** The Constant PAGE_COUNT_LABEL. */
 private static final String PAGE_COUNT_LABEL = "pageCountLabel";
 
 /** The Constant SUMMARISE_RESOURCES. */
 private static final String SUMMARISE_RESOURCES = "summariseResources";
 
 /** The Constant SUMMARISE_NUMBER_OF_PAGES. */
 private static final String SUMMARISE_NUMBER_OF_PAGES = "summariseNumberOfPages";
 
 /** The Constant CONTENT_TYPE_SECTION_SUMMARY_FIELDS. */
 private static final String CONTENT_TYPE_SECTION_SUMMARY_FIELDS = "contentTypeSectionSummaryFields";
 
 /** The Constant PAGE_LISTING_READ_TIME_LABEL. */
 private static final String PAGE_LISTING_READ_TIME_LABEL = "pageListing.readTimeLabel";
 
 /** The Constant PAGE_LISTING_READ_TIME_SUFFIX_TEXT. */
 private static final String PAGE_LISTING_READ_TIME_SUFFIX_TEXT = "pageListing.readTimeSuffixText";
 
 /** The Constant READ_TIME_LABEL. */
 private static final String READ_TIME_LABEL = "readTimeLabel";
 
 /** The Constant READ_TIME_SUFFIX_TEXT. */
 private static final String READ_TIME_SUFFIX_TEXT = "readTimeSuffixText";
 
 /** The Constant PAGINATION. */
 private static final String PAGINATION = "pagination";
 
 /** The Constant PAGINATION_CTA_LABEL. */
 private static final String PAGINATION_CTA_LABEL = "paginationCtaLabel";
 
 /** The Constant ZERO. */
 private static final int ZERO = 0;
 
 /** The Constant STRING_ONE. */
 private static final String STRING_ONE = "1";
 
 /** The Constant PARENT_PAGES_JSON. */
 private static final String PARENT_PAGES_JSON = "parentPagesJson";
 
 /** The Constant PAGINATION_TYPE. */
 private static final String PAGINATION_TYPE = "paginationType";
 
 /** The Constant PAGE_NO_PARAM_NAME. */
 private static final String PAGE_NO_PARAM_NAME = "pageNoParamName";
 
 /** The Constant FILTER_PARAM_NAME. */
 private static final String FILTER_PARAM_NAME = "filterParamName";
 
 /** The Constant PAGE_LISTING_PAGINATION_NEXT. */
 private static final String PAGE_LISTING_PAGINATION_NEXT = "pageListing.pagination.next";
 
 /** The Constant NEXT_LABEL. */
 private static final String NEXT_LABEL = "nextLabel";
 
 /** The Constant PAGE_LISTING_PAGINATION_PREVIOUS. */
 private static final String PAGE_LISTING_PAGINATION_PREVIOUS = "pageListing.pagination.previous";
 
 /** The Constant PREVIOUS_LABEL. */
 private static final String PREVIOUS_LABEL = "previousLabel";
 
 /** The Constant PAGE_LISTING_PAGINATION_LAST. */
 private static final String PAGE_LISTING_PAGINATION_LAST = "pageListing.pagination.last";
 
 /** The Constant LAST_LABEL. */
 private static final String LAST_LABEL = "lastLabel";
 
 /** The Constant PAGE_LISTING_PAGINATION_FIRST. */
 private static final String PAGE_LISTING_PAGINATION_FIRST = "pageListing.pagination.first";
 
 /** The Constant FIRST_LABEL. */
 private static final String FIRST_LABEL = "firstLabel";
 
 /** The Constant PAGE_PATH. */
 private static final String PAGE_PATH = "pagePath";
 
 /** The Constant CTA_LABEL_TEXT. */
 private static final String CTA_LABEL_TEXT = "ctaLabelText";
 
 /** The Constant SUB_HEADING_TEXT. */
 private static final String SUB_HEADING_TEXT = "subHeadingText";
 
 /** The Constant HEADING_TEXT. */
 private static final String HEADING_TEXT = "headingText";
 
 /** The Constant FILTER_CONFIG. */
 private static final String FILTER_CONFIG = "filterConfig";
 
 /** The Constant AJAX_URL. */
 private static final String AJAX_URL = "ajaxURL";
 
 /** The Constant DATA_JSON. */
 private static final String DATA_JSON = ".data.json";
 
 /** The Constant PAGES. */
 private static final String PAGES = "pages";
 
 /** The Constant TOTAL_COUNT. */
 private static final String TOTAL_COUNT = "totalCount";
 
 /** The Constant CHILD_PAGES. */
 private static final String CHILD_PAGES = "childPages";
 
 /** The Constant TAGS. */
 private static final String TAGS = "tags";
 
 /** The Constant FIXED_LIST. */
 private static final String FIXED_LIST = "fixedList";
 
 /** The Constant GLOBAL_CONFIG. */
 private static final String GLOBAL_CONFIG = "pageListingV2";
 
 /** The Constant ITEMS_PER_PAGE. */
 private static final String ITEMS_PER_PAGE = "itemsPerPage";
 
 /** The Constant FEATURE_TAGS. */
 private static final String FEATURE_TAGS = "featureTags";
 
 /** The Constant ZERO_STRING. */
 private static final String ZERO_STRING = "0";
 
 /** The Constant PN. */
 private static final String PN = "pn";
 
 /** The Constant FL. */
 private static final String FL = "fl";
 
 /** The Constant STATIC. */
 private static final String STATIC = "static";
 
 /**
  * logger object.
  */
 @Reference
 ConfigurationService configurationService;
 
 /** The search service. */
 @Reference
 SearchService searchService;
 
 /** The builder. */
 @Reference
 private QueryBuilder builder;
 
 /** The configure product image rendition service. */
 @Reference
 ConfigureProductImageRenditionService configureProductImageRenditionService;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(PageListingV2ViewHelperImpl.class);
 
 /** The Constant GENERAL_CONFIG. */
 private static final String GENERAL_CONFIG = "generalConfig";
 
 /** The Constant BUILD_NAVIGATION_USING. */
 private static final String BUILD_NAVIGATION_USING = "buildNavigationUsing";
 
 /** The Constant LIMIT_DEPTH. */
 private static final String LIMIT_DEPTH = "limitDepthTo";
 
 /** The Constant PARENT_PAGE_URL. */
 private static final String PARENT_PAGE_URL = "parentPageUrl";
 
 /** The Constant CONTENT_TYPE_JSON. */
 private static final String CONTENT_TYPE_JSON = "contentsections";
 
 /** The resource count. */
 private int resourceCount;
 
 /** The page list count. */
 private int pageListCount;
 
 /*
  * (non-Javadoc)
  * 
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper# processData (java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("Article Listing View Helper #processData called .");
  Map<String, Object> properties = getProperties(content);
  Map<String, Object> pageListingMap = new LinkedHashMap<String, Object>();
  
  SlingHttpServletRequest slingRequest = getSlingRequest(resources);
  
  String[] filters = slingRequest.getParameterValues(FL) != null ? slingRequest.getParameterValues(FL) : new String[] {};
  Page currentPage = getCurrentPage(resources);
  Map<String, String> globalConfigMap = ComponentUtil.getConfigMap(currentPage, GLOBAL_CONFIG);
  
  String[] featureTagNameSpaces = StringUtils.isNotBlank(MapUtils.getString(properties, FEATURE_TAGS)) ? ComponentUtil.getPropertyValueArray(
    properties, FEATURE_TAGS) : new String[] {};
  
  List<Map<String, Object>> pagePathList = new ArrayList<Map<String, Object>>();
  String buildNavigationUsing = MapUtils.getString(properties, BUILD_NAVIGATION_USING, StringUtils.EMPTY);
  
  if (FIXED_LIST.equals(buildNavigationUsing)) {
   pagePathList = LandingPageListHelper.getPagesList(properties, resources, globalConfigMap, featureTagNameSpaces, filters, false);
   
  } else if (TAGS.equals(buildNavigationUsing)) {
   pagePathList = LandingPageListHelper.getTaggedPageDataList(resources, globalConfigMap, properties, featureTagNameSpaces, filters, searchService,
     false);
  }
  setPageListWithFilters(content, resources, pageListingMap, filters, globalConfigMap, featureTagNameSpaces, pagePathList);
  getI18Values(resources, pageListingMap);
  
  setSectionSummaryValues(properties, resources, pageListingMap);
  
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(getJsonNameSpace(content), pageListingMap);
  return data;
 }
 
 /**
  * Gets the section summary values.
  * 
  * @param properties
  *         the properties
  * @param resources
  *         the resources
  * @param pageListingMap
  *         the page listing map
  * @return the section summary values
  */
 @SuppressWarnings("unchecked")
 private void setSectionSummaryValues(Map<String, Object> properties, Map<String, Object> resources, Map<String, Object> pageListingMap) {
  
  List<Map<String, String>> contentTypeSectionSummaryFieldsArray = ComponentUtil.getNestedMultiFieldProperties(getCurrentResource(resources),
    "contentTypeSectionSummary");
  I18n i18n = getI18n(resources);
  
  boolean summariseNumberOfPages = MapUtils.getBoolean(properties, SUMMARISE_NUMBER_OF_PAGES, false);
  String pageCountLabel = summariseNumberOfPages ? MapUtils.getString(properties, PAGE_COUNT_LABEL, i18n.get(PAGE_LISTING_PAGE_COUNT_LABEL_TEXT))
    : StringUtils.EMPTY;
  
  boolean summariseResources = MapUtils.getBoolean(properties, SUMMARISE_RESOURCES, false);
  String resourceCountLabel = summariseResources ? MapUtils.getString(properties, RESOURCE_COUNT_LABEL,
    i18n.get(PAGE_LISTING_RESOURCES_COUNT_LABEL_TEXT)) : StringUtils.EMPTY;
  
  setSectionSummaryMap(pageListingMap, i18n, pageCountLabel, resourceCountLabel);
  
  Object[] pageList = (Object[]) pageListingMap.get("pages");
  for (int i = 0; i < pageList.length; i++) {
   resourceCount = 0;
   pageListCount = 0;
   Map<String, Object> page = (Map<String, Object>) pageList[i];
   String pagePath = MapUtils.getString(page, "pagePath");
   setContentTypePages(resources, this.resourceCount, pageListCount, contentTypeSectionSummaryFieldsArray, pagePath);
   page.put(PAGE_COUNT, pageListCount);
   page.put(RESOURCE_COUNT, this.resourceCount);
  }
 }
 
 /**
  * Sets the section summary map.
  * 
  * @param pageListingMap
  *         the page listing map
  * @param i18n
  *         the i 18 n
  * @param pageCountLabel
  *         the page count label
  * @param resourceCountLabel
  *         the resource count label
  */
 @SuppressWarnings("unchecked")
 private void setSectionSummaryMap(Map<String, Object> pageListingMap, I18n i18n, String pageCountLabel, String resourceCountLabel) {
  Map<String, Object> sectionSummaryMap = new LinkedHashMap<String, Object>();
  Map<String, Object> generalConfigMap = MapUtils.getMap(pageListingMap, GENERAL_CONFIG, MapUtils.EMPTY_MAP);
  sectionSummaryMap.put(SUMMARY_HEADING_TEXT, i18n.get(PAGE_LISTING_PAGE_SUMMARY_HEADING_TEXT));
  sectionSummaryMap.put(PAGE_COUNT_LABEL_TEXT, pageCountLabel);
  sectionSummaryMap.put(RESOURCE_COUNT_LABEL_TEXT, resourceCountLabel);
  
  generalConfigMap.put(SECTION_SUMMARY, sectionSummaryMap);
 }
 
 /**
  * Gets the content type pages.
  * 
  * @param resources
  *         the resources
  * @param resourceCount
  *         the resource count
  * @param pageListCount
  *         the page list count
  * @param contentTypeSectionSummaryFieldsArray
  *         the content type section summary fields array
  * @param pagePath
  *         the page path
  * @return the content type pages
  */
 private void setContentTypePages(Map<String, Object> resources, int resourceCount, int pageListCount,
   List<Map<String, String>> contentTypeSectionSummaryFieldsArray, String pagePath) {
  
  Map<String, String> map = new HashMap<String, String>();
  
  map.put(PATH, pagePath);
  map.put(TYPE, CQ_PAGE_CONTENT);
  map.put(PROPERTY, CONTENT_TYPE);
  
  for (int i = 1; i <= contentTypeSectionSummaryFieldsArray.size(); i++) {
   String summary = MapUtils.getString(contentTypeSectionSummaryFieldsArray.get(i - 1), CONTENT_TYPE_SECTION_SUMMARY_FIELDS, StringUtils.EMPTY);
   map.put("property." + i + "_value", summary);
  }
  
  if (builder != null && !contentTypeSectionSummaryFieldsArray.isEmpty()) {
   returnPageCountfromQuery(resources, map, builder, resourceCount, pageListCount);
  }
 }
 
 /**
  * Gets the resultfrom query.
  * 
  * @param resources
  *         the resources
  * @param map
  *         the map
  * @param builder
  *         the builder
  * @param resourceCount
  *         the resource count
  * @param pageListCount
  *         the page list count
  * @return the resultfrom query
  */
 public void returnPageCountfromQuery(Map<String, Object> resources, Map<String, String> map, QueryBuilder builder, int resourceCount,
   int pageListCount) {
  
  SearchResult result = null;
  Query query = getQuery(resources, map, builder);
  int localPageListCount = pageListCount;
  if (query != null) {
   result = query.getResult();
   localPageListCount = result.getHits().size();
   Iterator<Resource> rsIterator = result.getResources();
   
   while (rsIterator.hasNext()) {
    String contentPath = rsIterator.next().getPath();
    getResourceCount(resources, resourceCount, contentPath);
   }
   this.pageListCount = localPageListCount;
  }
 }
 
 /**
  * Sets the query.
  * 
  * @param resources
  *         the resources
  * @param map
  *         the map
  * @param builder
  *         the builder
  * @return the query
  */
 private Query getQuery(Map<String, Object> resources, Map<String, String> map, QueryBuilder builder) {
  Session session = getResourceResolver(resources).adaptTo(Session.class);
  Query query = null;
  
  query = builder.createQuery(PredicateGroup.create(map), session);
  query.setHitsPerPage(0);
  return query;
 }
 
 /**
  * Gets the resource count.
  * 
  * @param resources
  *         the resources
  * @param resourceCount
  *         the resource count
  * @param contentPath
  *         the content path
  * @return the resource count
  */
 private void getResourceCount(Map<String, Object> resources, int resourceCount, String contentPath) {
  
  Map<String, String> map = new HashMap<String, String>();
  
  map.put(PATH, contentPath);
  map.put(TYPE, JcrConstants.NT_BASE);
  map.put(PROPERTY, JcrResourceConstants.SLING_RESOURCE_TYPE_PROPERTY);
  
  map.put(PROPERTY_OPERATION, LIKE);
  map.put(PROPERTY_VALUE, COMPONENTS_RESOURCE_LISTING);
  
  if (builder != null) {
   returnResourceCountfromQuery(resources, map, builder, resourceCount);
  }
 }
 
 /**
  * Gets the resultfrom query 2.
  * 
  * @param resources
  *         the resources
  * @param map
  *         the map
  * @param builder
  *         the builder
  * @param resourceCount
  *         the resource count
  * @return the resultfrom query 2
  */
 public void returnResourceCountfromQuery(Map<String, Object> resources, Map<String, String> map, QueryBuilder builder, int resourceCount) {
  
  SearchResult result = null;
  Query query = getQuery(resources, map, builder);
  int localResourceCount = resourceCount;
  if (query != null) {
   result = query.getResult();
   
   Iterator<Resource> rsIterator = result.getResources();
   while (rsIterator.hasNext()) {
    ValueMap propertyValue = rsIterator.next().getValueMap();
    int value = MapUtils.getInteger(propertyValue, CONFIGURE_RESOURCES_JSON, 0);
    localResourceCount = localResourceCount + value;
   }
   this.resourceCount = this.resourceCount + localResourceCount;
  }
 }
 
 /**
  * Gets the i 18 values.
  * 
  * @param resources
  *         the resources
  * @param pageListingMap
  *         the page listing map
  * @return the i 18 values
  */
 @SuppressWarnings("unchecked")
 private void getI18Values(Map<String, Object> resources, Map<String, Object> pageListingMap) {
  
  I18n i18n = getI18n(getSlingRequest(resources), getCurrentPage(resources));
  Map<String, Object> readTimeSummary = new HashMap<String, Object>();
  Map<String, Object> videoFilterTextMap = new HashMap<String, Object>();
  Map<String, Object> generalConfigMap = MapUtils.getMap(pageListingMap, GENERAL_CONFIG, MapUtils.EMPTY_MAP);
  
  readTimeSummary.put(READ_TIME_SUFFIX_TEXT, i18n.get(PAGE_LISTING_READ_TIME_SUFFIX_TEXT));
  readTimeSummary.put(READ_TIME_LABEL, i18n.get(PAGE_LISTING_READ_TIME_LABEL));
  
  videoFilterTextMap.put("videoFilterPrefixText", i18n.get("pageListingV2.videoFilterPrefixText"));
  videoFilterTextMap.put("videoFilterAllContentOptionText", i18n.get("pageListingV2.videoFilterAllContentOptionText"));
  videoFilterTextMap.put("videoFilterVideoContentOptionText", i18n.get("pageListingV2.videoFilterVideoContentOptionText"));
  
  generalConfigMap.put("readTimeSummary", readTimeSummary);
  generalConfigMap.put("videoFilterText", videoFilterTextMap);
 }
 
 /**
  * Sets the page list with filters.
  * 
  * @param content
  *         the content
  * @param resources
  *         the resources
  * @param pageListingMap
  *         the page listing map
  * @param filters
  *         the filters
  * @param globalConfigMap
  *         the global config map
  * @param featureTagNameSpaces
  *         the feature tag name spaces
  * @param pagePathList
  *         the page path list
  */
 private void setPageListWithFilters(Map<String, Object> content, Map<String, Object> resources, Map<String, Object> pageListingMap,
   String[] filters, Map<String, String> globalConfigMap, String[] featureTagNameSpaces, List<Map<String, Object>> pagePathList) {
  Map<String, Object> properties = getProperties(content);
  SlingHttpServletRequest slingRequest = getSlingRequest(resources);
  I18n i18n = getI18n(slingRequest, getCurrentPage(resources));
  if (StringUtils.isNotBlank(MapUtils.getString(properties, BUILD_NAVIGATION_USING, StringUtils.EMPTY))) {
   String pageNoStr = StringUtils.isNotBlank(slingRequest.getParameter(PN)) ? slingRequest.getParameter(PN) : STRING_ONE;
   int pageNo = ZERO;
   try {
    pageNo = Integer.parseInt(pageNoStr);
   } catch (NumberFormatException ne) {
    LOGGER.warn("Page Number is not a number");
   }
   String buildNavigationUsing = MapUtils.getString(properties, BUILD_NAVIGATION_USING, StringUtils.EMPTY);
   int itemsPerPage = MapUtils.getIntValue(properties, ITEMS_PER_PAGE);
   itemsPerPage = (itemsPerPage == ZERO) ? Integer.parseInt(MapUtils.getString(globalConfigMap, ITEMS_PER_PAGE, ZERO_STRING)) : itemsPerPage;
   List<Map<String, Object>> pagePathsList = (CHILD_PAGES.equals(buildNavigationUsing)) ? getNavigationItems(content, resources,
     featureTagNameSpaces, filters, globalConfigMap) : pagePathList;
   pageListingMap.put(GENERAL_CONFIG, getGeneralConfigDataMap(properties));
   pageListingMap.put(PAGINATION, getPaginationData(getI18n(resources), properties, itemsPerPage));
   pageListingMap.put(TOTAL_COUNT, pagePathsList.size());
   List<Map<String, Object>> list = LandingPageListHelper.getSubListByPage(pagePathsList, pageNo, itemsPerPage);
   if (list != null) {
    pageListingMap.put(PAGES, list.toArray());
   } else {
    pageListingMap.put(PAGES, new Object[] {});
   }
   
   Map<String, Object> filterMaps = LandingPageListHelper.getFilterProperties(properties, pagePathsList, getCurrentPage(resources),
     getTagManager(resources), getCurrentResource(resources), GLOBAL_CONFIG);
   filterMaps.put(FILTER_PARAM_NAME, FL);
   filterMaps.put(PAGE_NO_PARAM_NAME, PN);
   String resPath = getCurrentResource(resources).getPath();
   
   String ajaxURL = getResourceResolver(resources).map(resPath + DATA_JSON);
   filterMaps.put(AJAX_URL, ajaxURL);
   filterMaps.put(ITEMS_PER_PAGE, itemsPerPage);
   filterMaps.put("showFilterLabelText", i18n.get("pageListingV2.showFilterLabelText"));
   filterMaps.put("hideFilterLabelText", i18n.get("pageListingV2.hideFilterLabelText"));
   
   pageListingMap.put(FILTER_CONFIG, filterMaps);
  }
 }
 
 /**
  * gets the general config data like heading,sub heading of component section.
  * 
  * @param properties
  *         the properties
  * @return map
  */
 protected Map<String, Object> getGeneralConfigDataMap(Map<String, Object> properties) {
  Map<String, Object> generalConfigMap = new LinkedHashMap<String, Object>();
  generalConfigMap.put(HEADING_TEXT, MapUtils.getString(properties, HEADING_TEXT, StringUtils.EMPTY));
  generalConfigMap.put(SUB_HEADING_TEXT, MapUtils.getString(properties, SUB_HEADING_TEXT, StringUtils.EMPTY));
  generalConfigMap.put(CTA_LABEL_TEXT, MapUtils.getString(properties, CTA_LABEL_TEXT, StringUtils.EMPTY));
  generalConfigMap.put(BUILD_NAVIGATION_USING, MapUtils.getString(properties, BUILD_NAVIGATION_USING, StringUtils.EMPTY));
  return generalConfigMap;
 }
 
 /**
  * Gets the navigation items.
  * 
  * @param content
  *         the content
  * @param resources
  *         the resources
  * @param featureTagNameSpaces
  *         the properties
  * @param filters
  *         the filters
  * @param globalConfigMap
  *         the global config map
  * @return the navigation items
  */
 private List<Map<String, Object>> getNavigationItems(Map<String, Object> content, Map<String, Object> resources, String[] featureTagNameSpaces,
   String[] filters, Map<String, String> globalConfigMap) {
  LOGGER.debug("Inside section navigation method of Section Navigation to generate navigation items");
  
  PageManager pageManager = getResourceResolver(resources).adaptTo(PageManager.class);
  int limitDepth = Integer.parseInt(MapUtils.getString(getProperties(content), LIMIT_DEPTH, STRING_ONE));
  limitDepth = (limitDepth == CommonConstants.ZERO) ? CommonConstants.ONE : limitDepth;
  List<Map<String, String>> childPagesList = ComponentUtil.getNestedMultiFieldProperties(getCurrentResource(resources), PARENT_PAGES_JSON);
  List<Map<String, Object>> pageListMap = new LinkedList<Map<String, Object>>();
  if (CollectionUtils.isNotEmpty(childPagesList)) {
   for (Map<String, String> childMap : childPagesList) {
    String parentPage = MapUtils.getString(childMap, PARENT_PAGE_URL);
    Page page = pageManager.getPage(parentPage);
    List<String> pageList = new LinkedList<String>();
    getListChildrenUpToDepth(page, limitDepth, pageList);
    getChildPages(content, resources, featureTagNameSpaces, filters, globalConfigMap, pageListMap, pageList);
    LOGGER.debug("Auto Navigation list corresponding to Section Navigation is successfully generated");
    
   }
  } else {
   List<String> pageList = new LinkedList<String>();
   getListChildrenUpToDepth(getCurrentPage(resources), limitDepth, pageList);
   getChildPages(content, resources, featureTagNameSpaces, filters, globalConfigMap, pageListMap, pageList);
  }
  return pageListMap;
 }
 
 /**
  * gets the childpages list.
  * 
  * @param content
  *         the content
  * @param resources
  *         the resources
  * @param featureTagNameSpaces
  *         the feature tag name spaces
  * @param filters
  *         the filters
  * @param globalConfigMap
  *         the global config map
  * @param pageListMap
  *         the page list map
  * @param pageList
  *         the page list
  * @return the child pages
  */
 private void getChildPages(Map<String, Object> content, Map<String, Object> resources, String[] featureTagNameSpaces, String[] filters,
   Map<String, String> globalConfigMap, List<Map<String, Object>> pageListMap, List<String> pageList) {
  String[] includedContentTypes = getIncludedContentTypes(getCurrentResource(resources));
  for (String pagePath : pageList) {
   Page articlePage = getPageManager(resources).getContainingPage(pagePath);
   if (articlePage != null && isPageValidForContentType(articlePage, includedContentTypes)
     && TaggingComponentsHelper.isEligbleFromFilter(articlePage, filters)) {
    LandingAttributes landingAttributes = new LandingAttributes(articlePage, getSlingRequest(resources), globalConfigMap);
    LandingPageListHelper.getFlagMap(getProperties(content), globalConfigMap);
    Map<String, Object> map = landingAttributes.convertToMap();
    SlingHttpServletRequest slingRequest = getSlingRequest(resources);
    Boolean addFlag = true;
    if ("true".equals(slingRequest.getParameter(UnileverConstants.VIDEO_FILTER))) {
     String subContentType = MapUtils.getString(map, UnileverConstants.SUB_CONTENT_TYPE, StringUtils.EMPTY);
     addFlag = "video".equals(subContentType) ? true : false;
    }
    
    map.put(PAGE_PATH, pagePath);
    
    LandingPageListHelper.addFeatureTagsData(featureTagNameSpaces, map, articlePage, resources);
    if (addFlag) {
     pageListMap.add(map);
    }
    
   }
  }
 }
 
 /**
  * gets the string array of content types which has to be matched while getting page.
  * 
  * @param currentResource
  *         the current resource
  * @return the included content types
  */
 private String[] getIncludedContentTypes(Resource currentResource) {
  
  String[] includedContentTypes = null;
  List<Map<String, String>> manualList = ComponentUtil.getNestedMultiFieldProperties(currentResource, CONTENT_TYPE_JSON);
  if (!manualList.isEmpty()) {
   int size = manualList.size();
   includedContentTypes = new String[size];
   
   for (Map<String, String> map : manualList) {
    includedContentTypes[--size] = map.get("contentTypes");
   }
  }
  if (ArrayUtils.isEmpty(includedContentTypes)) {
   includedContentTypes = ArrayUtils.EMPTY_STRING_ARRAY;
  }
  return includedContentTypes;
 }
 
 /**
  * Checks if is page valid for section navigation.
  * 
  * @param page
  *         the page
  * @param includeContentTypes
  *         the include content types
  * @return true, if is page valid for section navigation
  */
 private boolean isPageValidForContentType(Page page, String[] includeContentTypes) {
  boolean isValidPage = false;
  if (includeContentTypes.length == 0) {
   return true;
  } else if (page != null && page.getContentResource() != null) {
   ValueMap pageProperties = page.getProperties();
   
   String pageContentType = pageProperties.get(UnileverConstants.CONTENT_TYPE, String.class);
   if (StringUtils.isNotBlank(pageContentType) && ArrayUtils.indexOf(includeContentTypes, pageContentType) != -1) {
    isValidPage = true;
   }
  }
  return isValidPage;
 }
 
 /**
  * does the child list iteartion according to limt depth.
  * 
  * @param page
  *         the page
  * @param level
  *         the level
  * @param articleList
  *         the article list
  * @return the list children up to depth
  */
 protected void getListChildrenUpToDepth(Page page, int level, List<String> articleList) {
  if (page != null && level > 0) {
   Iterator<Page> children = page.listChildren();
   while (children.hasNext()) {
    Page child = children.next();
    articleList.add(child.getPath());
    getListChildrenUpToDepth(child, level - 1, articleList);
   }
   
  }
 }
 
 /**
  * returns the map which has static values like pagination labels.
  * 
  * @param i18n
  *         the i 18 n
  * @param properties
  *         the properties
  * @param itemsPerPage
  *         the items per page
  * @return map
  */
 private Map<String, Object> getPaginationData(I18n i18n, Map<String, Object> properties, int itemsPerPage) {
  Map<String, Object> staticMap = new LinkedHashMap<String, Object>();
  staticMap.put(FIRST_LABEL, i18n.get(PAGE_LISTING_PAGINATION_FIRST));
  staticMap.put(LAST_LABEL, i18n.get(PAGE_LISTING_PAGINATION_LAST));
  staticMap.put(PREVIOUS_LABEL, i18n.get(PAGE_LISTING_PAGINATION_PREVIOUS));
  staticMap.put(NEXT_LABEL, i18n.get(PAGE_LISTING_PAGINATION_NEXT));
  Map<String, Object> paginationMap = new LinkedHashMap<String, Object>();
  paginationMap.put(PAGINATION_TYPE, MapUtils.getString(properties, PAGINATION_TYPE, StringUtils.EMPTY));
  paginationMap.put(PAGINATION_CTA_LABEL, MapUtils.getString(properties, PAGINATION_CTA_LABEL, StringUtils.EMPTY));
  paginationMap.put(ITEMS_PER_PAGE, itemsPerPage);
  paginationMap.put(STATIC, staticMap);
  return paginationMap;
 }
 
}
