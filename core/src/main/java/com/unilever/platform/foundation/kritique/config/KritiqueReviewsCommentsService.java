/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.kritique.config;

import java.util.List;
import java.util.Map;

import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.template.dto.KritiqueDTO;

/**
 * The Interface KritiqueReviewsCommentsService.
 */
public interface KritiqueReviewsCommentsService {
 
 /**
  * Gets the reviews.
  * 
  * @param reviewsdto
  *         the reviewsdto
  * @param configurationService
  *         the configuration service
  * @return the reviews
  */
 public Map<String, Object> getReviewsService(KritiqueDTO reviewsdto, ConfigurationService configurationService);
 
 /**
  * Gets the rating service.
  * 
  * @param reviewsdto
  *         the reviewsdto
  * @param configurationService
  *         the configuration service
  * @return the rating
  */
 public List<Map<String, Object>> getRatingsService(KritiqueDTO reviewsdto, ConfigurationService configurationService);
 
 /**
  * Gets the Seo service.
  * 
  * @param ratingdto
  *         the ratingdto
  * @return the seo rating and comments in noscript tag
  */
 public String getSEORatingsService(KritiqueDTO ratingdto);
 
 /**
  * Gets the article reviews.
  * 
  * @param kritiquedto
  *         the kritiquedto
  * @param configurationService
  *         the configuration service
  * @return the reviews
  */
 public Map<String, Object> getArticleReviewsService(KritiqueDTO kritiquedto, ConfigurationService configurationService);
 
 /**
  * Gets the article rating service.
  * 
  * @param kritiquedto
  *         the kritiquedto
  * @param configurationService
  *         the configuration service
  * @return the rating
  */
 public List<Map<String, Object>> getArticleRatingsService(KritiqueDTO kritiquedto, ConfigurationService configurationService);
 
}
