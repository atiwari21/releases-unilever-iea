/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.components.helper;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class VideoHelper.
 * 
 */
public final class VideoHelper {
 
 private static final String DISPLAY_IN_OVERLAY = "displayInOverlay";
 
 private static final String AUTO_PLAY = "autoplay";
 
 private static final String HIDE_VIDEO_CONTROLS = "hideVideoControls";
 
 private static final String MUTE_VIDEO_AUDIO = "muteVideoAudio";
 
 private static final String LOOP_VIDEO_PLAYBACK = "loopVideoPlayback";
 
 private static final String RELATED_VIDEO_AT_END = "relatedVideoAtEnd";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(VideoHelper.class);
 
 /**
  * Instantiates a new VideoHelper.
  */
 private VideoHelper() {
  
 }
 
 /**
  * @param properties
  *         the properties
  * @return video control map
  */
 public static Map<String, Boolean> getVideoControls(Map<String, Object> properties, String videoSource) {
  Map<String, Boolean> videoControlsMap = new HashMap<String, Boolean>();
  boolean displayInOverlay = false;
  boolean autoPlay = false;
  boolean hideVideoControls = false;
  boolean muteVideoControls = false;
  boolean loopVideoPlayback = false;
  boolean relatedVideos = false;
  if(StringUtils.equals(videoSource, "youku")){
   displayInOverlay  = MapUtils.getBooleanValue(properties, DISPLAY_IN_OVERLAY, false);
   autoPlay = MapUtils.getBooleanValue(properties, AUTO_PLAY, false);
  }else if(StringUtils.equals(videoSource, "iqiyi") || StringUtils.equals(videoSource, "tencent")){
   autoPlay = MapUtils.getBooleanValue(properties, AUTO_PLAY, false);
  }else if(StringUtils.equals(videoSource, "youtube")){
   displayInOverlay  = MapUtils.getBooleanValue(properties, DISPLAY_IN_OVERLAY, false);
   autoPlay = MapUtils.getBooleanValue(properties, AUTO_PLAY, false);
   hideVideoControls = MapUtils.getBooleanValue(properties, HIDE_VIDEO_CONTROLS, false);
   muteVideoControls = MapUtils.getBooleanValue(properties, MUTE_VIDEO_AUDIO, false);
   loopVideoPlayback  = MapUtils.getBooleanValue(properties, LOOP_VIDEO_PLAYBACK, false);
   relatedVideos = MapUtils.getBooleanValue(properties, RELATED_VIDEO_AT_END, false);
  }
  videoControlsMap.put("displayInOverlay", displayInOverlay);
  videoControlsMap.put("autoplay", autoPlay);
  videoControlsMap.put("hideVideoControls", hideVideoControls);
  videoControlsMap.put("muteVideoAudio", muteVideoControls);
  videoControlsMap.put("loopVideoPlayback", loopVideoPlayback);
  videoControlsMap.put("relatedVideoAtEnd", relatedVideos);
  LOGGER.debug("videoControlsMap = ", videoControlsMap);
  return videoControlsMap;
 }
 
}
