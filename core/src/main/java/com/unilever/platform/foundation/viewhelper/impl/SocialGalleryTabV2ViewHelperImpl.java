/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.SocialTABHelper;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.Image;
import com.unilever.platform.foundation.template.dto.SocialTabQueryDTO;

/**
 * This class prepares the data map for SocialGalleryTabV2 component.
 */
@Component(description = "SocialGalleryTabV2ViewHelperImpl", immediate = true, metatype = true, label = "SocialGalleryTabV2 ViewHelper")
@Service(value = { SocialGalleryTabV2ViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.SOCIAL_GALLERY_TAB_V2, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class SocialGalleryTabV2ViewHelperImpl extends SocialTabBaseViewHelperImpl {
 
 private static final String STATIC = "static";
 
 @Reference
 ConfigurationService configurationService;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(SocialGalleryTabV2ViewHelperImpl.class);
 private static final String GLOBAL_CONFIG = "socialGalleryTabV2";
 private static final String MANUAL = "manual";
 private static final String AUTOMATIC = "automatic";
 private static final String LINK_POST_TO_CONTENT = "linkPostsToContentJson";
 private static final String SOCIAL_POST_TO_DISPLAY = "socialPostsToDisplayJson";
 private static final String ARTICLE_HEADING_TEXT = "socialGallery.viewArticleHeadingText";
 private static final String ARTICLE_CTA_LABEL = "socialGallery.viewArticleCtaLabel";
 private static final String PRODUCT_HEADING_TEXT = "socialGallery.viewProductHeadingText";
 private static final String PRODUCT_CTA_LABEL = "socialGallery.viewProductCtaLabel";
 private static final String TEASER_IMAGE = "teaserImage";
 private static final String TEASER_ALT_TEXT = "teaserAltText";
 private static final String TEASER_TITLE = "teaserTitle";
 private static final String NAME_KEY = "name";
 private static final String IMAGE = "image";
 private static final String ASSOCIATED_PAGE = "associatedPage";
 private static final String HEADING_TEXT = "headingText";
 private static final String SUB_HEADING_TEXT = "subHeadingText";
 private static final String LONG_SUB_HEADING_TEXT = "longSubHeadingText";
 private static final String CTA_LABEL = "ctaLabel";
 private static final String CTA_URL = "ctaUrl";
 private static final String AGGREGATION_MODE = "aggregationMode";
 private static final String PAGINATION_TYPE = "paginationType";
 private static final String PAGINATION_CTA = "paginationCtaLabel";
 private static final String ITEMS_PER_PAGE = "itemsPerPage";
 private static final String LINK_POST_TO_URL = "linkPostToUrl";
 private static final String LINK_TYPE = "linkType";
 private static final String CUSTOM_HEADING = "customHeadingText";
 private static final String CUSTOM_CTA = "customCtaLabel";
 private static final String EXPANDABLE_PANEL = "expandablePanel";
 
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("SocialGalleryTabV2ViewHelperImpl #processData called .");
  Map<String, Object> properties = getProperties(content);
  Map<String, Object> socialGalleryTabV2Map = new LinkedHashMap<String, Object>();
  Page currentPage = getCurrentPage(resources);
  String aggregationModeType = MapUtils.getString(properties, AGGREGATION_MODE, StringUtils.EMPTY);
  socialGalleryTabV2Map.put("pagination", getPaginationProperties(properties, resources));
  int numberOfPosts = MapUtils.getIntValue(properties, ITEMS_PER_PAGE, 0);
  SocialTabQueryDTO tabQuery = new SocialTabQueryDTO();
  
  tabQuery.setBrandNameParameter(getBrandNameParameter(configurationService, currentPage));
  tabQuery.setConfigurationService(configurationService);
  tabQuery.setHashTags(getHashTags(getCurrentResource(resources)));
  tabQuery.setMaxImages(0);
  tabQuery.setNoOfPost(numberOfPosts);
  tabQuery.setPage(currentPage);
  tabQuery.setPagePath(getResourceResolver(resources).map(currentPage.getPath()));
  tabQuery.setSocialChannels(getSocialChannels(getProperties(content)));
  
  SocialTABHelper.addRetrievalQueryParametrsMap(socialGalleryTabV2Map, tabQuery);
  
  setCtaLabels(socialGalleryTabV2Map, getI18n(resources));
  setGlobalConfigAttributes(currentPage, socialGalleryTabV2Map, properties);
  
  setGeneralConfigDataMap(socialGalleryTabV2Map, properties, resources);
  
  if (aggregationModeType.equals(AUTOMATIC)) {
   setSocialGalleryPostsMap(socialGalleryTabV2Map, resources, currentPage, LINK_POST_TO_CONTENT, properties);
  } else if (aggregationModeType.equals(MANUAL)) {
   setSocialGalleryPostsMap(socialGalleryTabV2Map, resources, currentPage, SOCIAL_POST_TO_DISPLAY, properties);
  }
  
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(getJsonNameSpace(content), socialGalleryTabV2Map);
  return data;
 }
 
 private static Map<String, Object> setGeneralConfigDataMap(Map<String, Object> socialGalleryV2Map, Map<String, Object> properties,
   Map<String, Object> resources) {
  Map<String, Object> generalConfigDataMap = new LinkedHashMap<String, Object>();
  generalConfigDataMap.put(HEADING_TEXT, MapUtils.getString(properties, "headingText", StringUtils.EMPTY));
  generalConfigDataMap.put(SUB_HEADING_TEXT, MapUtils.getString(properties, "subheadingText", StringUtils.EMPTY));
  generalConfigDataMap.put(LONG_SUB_HEADING_TEXT, MapUtils.getString(properties, "text", StringUtils.EMPTY));
  generalConfigDataMap.put(CTA_LABEL, MapUtils.getString(properties, "ctaLabel", StringUtils.EMPTY));
  generalConfigDataMap
    .put(CTA_URL, ComponentUtil.getFullURL(getResourceResolver(resources), MapUtils.getString(properties, "ctaUrl", StringUtils.EMPTY),
      getSlingRequest(resources)));
  generalConfigDataMap.put(OPEN_IN_NEW_WINDOW, MapUtils.getBoolean(properties, "openInNewWindow", false));
  generalConfigDataMap.put(AGGREGATION_MODE, MapUtils.getString(properties, AGGREGATION_MODE));
  socialGalleryV2Map.put("generalConfig", generalConfigDataMap);
  return socialGalleryV2Map;
  
 }
 
 /**
  * Gets Pagination Properties
  * 
  * @param properties
  *         the properties
  * @param resources
  *         the resources
  * @return pagination property map
  */
 public Map<String, Object> getPaginationProperties(Map<String, Object> properties, Map<String, Object> resources) {
  Map<String, Object> paginationMap = new LinkedHashMap<String, Object>();
  String paginationType = MapUtils.getString(properties, "paginationType", StringUtils.EMPTY);
  paginationMap.put(PAGINATION_TYPE, paginationType);
  int itemsPerPage = MapUtils.getIntValue(properties, ITEMS_PER_PAGE, 0);
  if (itemsPerPage == 0) {
   itemsPerPage = GlobalConfigurationUtility.getIntegerValueFromConfiguration(GLOBAL_CONFIG, configurationService, getCurrentPage(resources),
     "itemsPerPage");
  }
  paginationMap.put(ITEMS_PER_PAGE, itemsPerPage);
  String paginationCTALabel = MapUtils.getString(properties, PAGINATION_CTA, StringUtils.EMPTY);
  if ("pagination".equals(paginationType)) {
   paginationMap.put(STATIC, getStaticData(getI18n(resources), paginationCTALabel));
  } else {
   Map<String, Object> staticMap = new LinkedHashMap<String, Object>();
   if ("".equals(paginationCTALabel)) {
    staticMap.put(PAGINATION_CTA, getI18n(resources).get("socialGalleryTabV2.paginationCtaLabel"));
   } else {
    staticMap.put(PAGINATION_CTA, paginationCTALabel);
   }
   paginationMap.put(STATIC, staticMap);
  }
  return paginationMap;
 }
 
 /**
  * Sets the social gallery posts.
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param page
  *         the page
  * @param currentResource
  *         the current resource
  * @param postsJson
  *         the postsJson
  * @return the social gallery posts
  */
 private void setSocialGalleryPostsMap(Map<String, Object> socialGalleryTabV2Map, Map<String, Object> resources, Page page, String postsJson,
   Map<String, Object> properties) {
  Resource currentResource = getCurrentResource(resources);
  ResourceResolver resourceResolver = getResourceResolver(resources);
  String aggregationMode = MapUtils.getString(properties, AGGREGATION_MODE);
  boolean isAggregationMode = StringUtils.equals(AUTOMATIC, aggregationMode);
  String imagePathTemp = "fixedTabImagePath";
  String linkPostToUrl = "fixedTabPagePath";
  String linkTypeTemp = "fixedTabLinkType";
  String customHeading = "fixedTabHeadingTextCustom";
  String customCtaLabel = "fixedTabCtaLabelCustom";
  String openInNewWindow = "fixedTabOpenInNewWindow";
  if(isAggregationMode){
   imagePathTemp = "imagePath";
   linkPostToUrl = "pagePath";
   linkTypeTemp = "linkType";
   customHeading = "headingTextCustom";
   customCtaLabel = "ctaLabelCustom";
   openInNewWindow = "searchOpenInNewWindow";
  }
  I18n i18n = getI18n(resources);
  List<Map<String, Object>> socialPostsList = new ArrayList<Map<String, Object>>();
  List<Map<String, String>> list = getSocialGalleryPostsMap(currentResource, postsJson);
  for (Map<String, String> map : list) {
   String imagePath = StringUtils.defaultString(map.get(imagePathTemp), StringUtils.EMPTY);
   String linkPostToPath = ComponentUtil.getFullURL(resourceResolver, StringUtils.defaultString(map.get(linkPostToUrl), StringUtils.EMPTY),
     getSlingRequest(resources));
   String linkType = StringUtils.defaultString(map.get(linkTypeTemp), StringUtils.EMPTY);
   String headingTextCustom = StringUtils.EMPTY;
   String ctaLabelCustom = StringUtils.EMPTY;
   Page associatedPage = page.getPageManager().getPage(StringUtils.defaultString(map.get(linkPostToUrl), StringUtils.EMPTY));
   Map<String, Object> socialPostsMap = SocialTABHelper.getImageMapWithMetadata(imagePath, page, resourceResolver, getSlingRequest(resources));
   if ("article".equals(linkType)) {
    headingTextCustom = i18n.get(ARTICLE_HEADING_TEXT);
    ctaLabelCustom = i18n.get(ARTICLE_CTA_LABEL);
    if (null != associatedPage) {
     socialPostsMap.put(ASSOCIATED_PAGE, getArticleMap(associatedPage, resources));
    } else {
     socialPostsMap.put(ASSOCIATED_PAGE, new HashMap<String, Object>());
    }
   } else if ("product".equals(linkType)) {
    headingTextCustom = i18n.get(PRODUCT_HEADING_TEXT);
    ctaLabelCustom = i18n.get(PRODUCT_CTA_LABEL);
    if (null != associatedPage) {
     socialPostsMap.put(ASSOCIATED_PAGE, getProductMap(associatedPage, resources));
    } else {
     socialPostsMap.put(ASSOCIATED_PAGE, new HashMap<String, Object>());
    }
   } else if ("custom".equals(linkType)) {
    headingTextCustom = StringUtils.defaultString(map.get(customHeading), StringUtils.EMPTY);
    ctaLabelCustom = StringUtils.defaultString(map.get(customCtaLabel), StringUtils.EMPTY);
    if (null != associatedPage) {
     socialPostsMap.put(ASSOCIATED_PAGE, getArticleMap(associatedPage, resources));
    } else {
     socialPostsMap.put(ASSOCIATED_PAGE, new HashMap<String, Object>());
    }
   }
   
   socialPostsMap.put(LINK_POST_TO_URL, linkPostToPath);
   socialPostsMap.put(LINK_TYPE, linkType);
   socialPostsMap.put(CUSTOM_HEADING, headingTextCustom);
   socialPostsMap.put(CUSTOM_CTA, ctaLabelCustom);
   socialPostsMap.put(OPEN_IN_NEW_WINDOW, getNewWindowFromMultifield(map, openInNewWindow));
   socialPostsList.add(socialPostsMap);
  }
  socialGalleryTabV2Map.put("socialGalleryPosts", socialPostsList.toArray());
 }
 
 /**
  * Gets the Posts json map
  * 
  * @param currentResource
  *         the current resource
  * @param postsJson
  *         the postsJson
  * @return the social gallery images map
  */
 public List<Map<String, String>> getSocialGalleryPostsMap(Resource currentResource, String postsJson) {
  return ComponentUtil.getNestedMultiFieldProperties(currentResource, postsJson);
 }
 
 /**
  * Sets the cta labels map properties.
  * 
  * @param socialGalleryV2Map
  *         the socialGalleryV2Map
  * @param i18n
  *         the i18n
  * @return the static map properties
  */
 private static void setCtaLabels(Map<String, Object> socialGalleryV2Map, I18n i18n) {
  
  Map<String, Object> staticMap = new HashMap<String, Object>();
  staticMap.put(PAGINATION_CTA, i18n.get("socialGalleryTab.paginationCtaLabel"));
  staticMap.put("socialPostLikesSuffix", i18n.get("socialGalleryTab.socialPostLikesSuffix"));
  staticMap.put("viewArticleHeadingText", i18n.get("socialGalleryTab.viewArticleHeadingText"));
  staticMap.put("viewArticleCtaLabel", i18n.get("socialGalleryTab.viewArticleCtaLabel"));
  staticMap.put("viewProductHeadingText", i18n.get("socialGalleryTab.viewProductHeadingText"));
  staticMap.put("viewProductCtaLabel", i18n.get("socialGalleryTab.viewProductCtaLabel"));
  socialGalleryV2Map.put("ctaLabels", staticMap);
 }
 
 /**
  * Sets the override global config attributes.
  * 
  * @param currentPage
  *         the uni currentPage
  * @param socialGalleryTabV2Map
  *         the socialGalleryTabV2Map
  * @param properties
  *         the properties
  * 
  */
 private void setGlobalConfigAttributes(Page currentPage, Map<String, Object> socialGalleryTabV2Map, Map<String, Object> properties) {
  boolean overrideGlobalConfig = MapUtils.getBoolean(properties, UnileverConstants.OVERRIDE_GLOBAL_CONFIG) != null ? MapUtils.getBoolean(properties,
    UnileverConstants.OVERRIDE_GLOBAL_CONFIG) : false;
  
  int limitResultsTo;
  boolean expandablePanelFlag = false;
  
  if (overrideGlobalConfig) {
   limitResultsTo = MapUtils.getIntValue(properties, "limit", 0);
   expandablePanelFlag = MapUtils.getBoolean(properties, EXPANDABLE_PANEL) != null ? MapUtils.getBoolean(properties, EXPANDABLE_PANEL) : false;
  } else {
   limitResultsTo = GlobalConfigurationUtility.getIntegerValueFromConfiguration(GLOBAL_CONFIG, configurationService, currentPage, "limit");
   expandablePanelFlag = Boolean.parseBoolean(GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, GLOBAL_CONFIG,
     EXPANDABLE_PANEL));
  }
  socialGalleryTabV2Map.put("limitResultsTo", limitResultsTo);
  socialGalleryTabV2Map.put("expandablePanelFlag", expandablePanelFlag);
 }
 
 /**
  * Gets the article map.
  * 
  * @param articlePage
  *         the article page
  * @param resources
  *         the resources
  * @return the article map
  */
 public Map<String, Object> getArticleMap(Page articlePage, Map<String, Object> resources) {
  Map<String, Object> articleMap = new HashMap<String, Object>();
  Image image = null;
  Map<String, Object> pageProperties = articlePage.getProperties();
  if (null != articlePage) {
   String imagePath = (String) pageProperties.get(TEASER_IMAGE) != null ? (String) pageProperties.get(TEASER_IMAGE) : StringUtils.EMPTY;
   String altText = (String) pageProperties.get(TEASER_ALT_TEXT) != null ? (String) pageProperties.get(TEASER_ALT_TEXT) : StringUtils.EMPTY;
   String title = (String) pageProperties.get(TEASER_TITLE) != null ? (String) pageProperties.get(TEASER_TITLE) : StringUtils.EMPTY;
   image = new Image(imagePath, altText, title, articlePage, getSlingRequest(resources));
   articleMap.put(NAME_KEY, articlePage.getName());
  }
  if (image != null) {
   articleMap.put(IMAGE, image.convertToMap());
  }
  
  return articleMap;
 }
 
 /**
  * Gets the new window from multifield.
  * 
  * @param map
  *         the map
  * @return the new window from multifield
  */
 private static boolean getNewWindowFromMultifield(Map<String, String> map, String openInNewWindowTemp) {
  
  String newWindow = StringUtils.defaultString(map.get(openInNewWindowTemp), StringUtils.EMPTY);
  String openInNewWindow = newWindow != null && "[true]".equals(newWindow) ? BOOLEAN_TRUE : BOOLEAN_FALSE;
  return Boolean.valueOf(openInNewWindow);
 }
 
 /**
  * Gets the static data.
  * 
  * @param i18n
  *         the i18n
  * @return the static data
  */
 private Map<String, Object> getStaticData(I18n i18n, String paginationCTALabel) {
  Map<String, Object> staticMap = new LinkedHashMap<String, Object>();
  staticMap.put("firstLabel", i18n.get("pageListing.pagination.first"));
  staticMap.put("lastLabel", i18n.get("pageListing.pagination.last"));
  staticMap.put("previousLabel", i18n.get("pageListing.pagination.previous"));
  staticMap.put("nextLabel", i18n.get("pageListing.pagination.next"));
  if ("".equals(paginationCTALabel)) {
   staticMap.put(PAGINATION_CTA, i18n.get("socialGalleryTabV2.paginationCtaLabel"));
  } else {
   staticMap.put(PAGINATION_CTA, paginationCTALabel);
  }
  return staticMap;
 }
 
}
