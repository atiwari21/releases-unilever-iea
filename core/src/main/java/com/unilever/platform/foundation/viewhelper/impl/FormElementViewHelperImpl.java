/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.commons.json.JSONTokener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.xss.XSSAPI;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.wcm.foundation.forms.FormsHelper;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.utils.CollectionUtils;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.FormElementConstants;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * This class prepares the data map for Form Element by invoking the corresponding service. For example, if the element type is of textField then
 * TextFieldService is called.
 */
@Component(description = "FormElementViewHelperImpl", immediate = true, metatype = true, label = "FormElementViewHelperImpl")
@Service(value = { FormElementViewHelperImpl.class, ViewHelper.class })
@Properties({
  // @Property(name = "componentName", value = "formElement",
  // propertyPrivate = true),
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = FormElementViewHelperImpl.COMP_NAME, propertyPrivate = true),
  @Property(name = org.osgi.framework.Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class FormElementViewHelperImpl extends BaseViewHelper {
 
 private static final String THE_DATA_MAP_RETURNED_FOR_THE_RADIO_ELEMENT_IS = "The data map returned for the radio element is ";
 private static final String THE_DROP_DOWN_OPTIONS_DYNAMICALLY_LOADED_ARE = "The drop down options dynamically loaded are ";
 private static final String THE_DYNAMIC_OPTION_LOAD_PATH_IS = "The dynamic option load path is ";
 private static final String CUSTOM_VALIDATION = "customValidation";
 private static final String ERROR_CONFIGS = "messageConfigs";
 private static final String ERROR_CONFIGURATION = "messageConfiguration";
 public static final String COMP_NAME = "/apps/unilever-iea/components/unilever-forms/formElement";
 private static final String SUB_TYPE = "subType";
 private static final String FALSE = "false";
 private static final String MAX_CHAR = "maxChar";
 private static final String MIN_CHAR = "minChar";
 /**
  * Logger Object.
  */
 private static final Logger LOGGER = LoggerFactory.getLogger(FormElementViewHelperImpl.class);
 /**
  * Reference for XSSAPI.
  */
 @Reference
 private XSSAPI xssapi;
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /**
  * This method invokes the corresponding element type service depending on the element/view type of the form element that is selected.
  * 
  * @param content
  *         {@link Map} of all user input received as input
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return Map holding the result data.
  */
 @SuppressWarnings("unchecked")
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  Map<String, Object> propertiesValueMap = (Map<String, Object>) content.get(PROPERTIES);
  Map<String, Object> finalMap = new HashMap<String, Object>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  Map<String, Object> properties = CollectionUtils.getJcrPropertyAsMap(propertiesValueMap);
  String viewType = (String) properties.get("viewType");
  String jsonNameSpace = MapUtils.getString(content, "jsonNameSpace");
  // isContainer property from form component.
  
  properties.put("isContainer", MapUtils.getBoolean(content, "isContainer", false));
  
  LOGGER.info("The form element is of type " + viewType);
  
  handleTextBox(finalMap, properties, viewType, jsonNameSpace, resources);
  
  handleCalendar(finalMap, properties, viewType, jsonNameSpace, resources);
  
  handleDropdown(resources, finalMap, properties, viewType, jsonNameSpace);
  
  handleRadio(resources, finalMap, properties, viewType, jsonNameSpace);
  
  handleButton(finalMap, properties, viewType, jsonNameSpace, resources);
  
  handleStartSection(finalMap, properties, viewType, jsonNameSpace, resources);
  
  handleLink(finalMap, properties, viewType, jsonNameSpace, resources);
  
  handleGoogleCapthcha(finalMap, properties, viewType, jsonNameSpace, resources);
  handleCheckBox(finalMap, properties, viewType, jsonNameSpace, resources);
  handleMultipleCheckbox(resources, finalMap, properties, viewType, jsonNameSpace);
  
  return finalMap;
 }
 
 /**
  * Handle radio.
  * 
  * @param resources
  *         the resources
  * @param finalMap
  *         the final map
  * @param properties
  *         the properties
  * @param viewType
  *         the view type
  * @param jsonNameSpace
  *         the json name space
  */
 private void handleRadio(final Map<String, Object> resources, Map<String, Object> finalMap, Map<String, Object> properties, String viewType,
   String jsonNameSpace) {
  if ("radio".equalsIgnoreCase(viewType)) {
   Map<String, Object> radioValues = fetchFormElementData(properties, resources);
   radioValues.put(FormElementConstants.VIEW_TYPE, "radio");
   radioValues.put(FormElementConstants.DEFAULT_VALUE, getEncodedValue(properties, FormElementConstants.DEFAULT_VALUE));
   
   if (properties.containsKey(FormElementConstants.OPTIONS_LOAD_PATH)) {
    LOGGER.info(THE_DYNAMIC_OPTION_LOAD_PATH_IS + (String) properties.get(FormElementConstants.OPTIONS_LOAD_PATH));
    SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) resources.get(ProductConstants.REQUEST);
    Resource resource = slingRequest.getResource();
    Map<String, String> displayValues = FormsHelper.getOptions(slingRequest, resource);
    LOGGER.debug(THE_DROP_DOWN_OPTIONS_DYNAMICALLY_LOADED_ARE + displayValues);
    radioValues.put(FormElementConstants.RADIO_OPTIONS, displayValues);
   } else {
    radioValues.put(FormElementConstants.RADIO_OPTIONS, populateRadioOptions(properties));
   }
   LOGGER.debug(THE_DATA_MAP_RETURNED_FOR_THE_RADIO_ELEMENT_IS + radioValues);
   finalMap.put(jsonNameSpace, radioValues);
   
  }
 }
 
 /**
  * Handle dropdown.
  * 
  * @param resources
  *         the resources
  * @param finalMap
  *         the final map
  * @param properties
  *         the properties
  * @param viewType
  *         the view type
  * @param jsonNameSpace
  *         the json name space
  */
 private void handleDropdown(final Map<String, Object> resources, Map<String, Object> finalMap, Map<String, Object> properties, String viewType,
   String jsonNameSpace) {
  if ("dropdown".equalsIgnoreCase(viewType)) {
   Map<String, Object> dropdownValues = fetchFormElementData(properties, resources);
   dropdownValues.put(FormElementConstants.VIEW_TYPE, "select");
   dropdownValues.put(FormElementConstants.DEFAULT_VALUE, getEncodedValue(properties, FormElementConstants.DEFAULT_VALUE));
   dropdownValues.put(FormElementConstants.MULTI_SELECT,
     Boolean.parseBoolean(MapUtils.getString(properties, FormElementConstants.MULTI_SELECT, FALSE)));
   
   if (properties.containsKey(FormElementConstants.OPTIONS_LOAD_PATH)) {
    LOGGER.info(THE_DYNAMIC_OPTION_LOAD_PATH_IS + (String) properties.get(FormElementConstants.OPTIONS_LOAD_PATH));
    SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) resources.get(ProductConstants.REQUEST);
    Resource resource = slingRequest.getResource();
    Map<String, String> displayValues = FormsHelper.getOptions(slingRequest, resource);
    dropdownValues.put(FormElementConstants.DROPDOWN_OPTIONS, displayValues);
    LOGGER.debug(THE_DROP_DOWN_OPTIONS_DYNAMICALLY_LOADED_ARE + displayValues);
   } else {
    dropdownValues.put(FormElementConstants.DROPDOWN_OPTIONS, populateDropDownOptions(properties));
   }
   LOGGER.debug("The data map returned for the dropdown element is " + dropdownValues);
   
   finalMap.put(jsonNameSpace, dropdownValues);
  }
 }
 
 /**
  * Handle calendar.
  * 
  * @param finalMap
  *         the final map
  * @param properties
  *         the properties
  * @param viewType
  *         the view type
  * @param jsonNameSpace
  *         the json name space
  * @param resources
  */
 private void handleCalendar(Map<String, Object> finalMap, Map<String, Object> properties, String viewType, String jsonNameSpace,
   Map<String, Object> resources) {
  if ("calendar".equalsIgnoreCase(viewType)) {
   Map<String, Object> dateFieldValues = fetchFormElementData(properties, resources);
   dateFieldValues.put(FormElementConstants.VIEW_TYPE, "text");
   String dateFormatProperty = MapUtils.getString(properties, FormElementConstants.DATE_FORMAT,
     "dd/mm/yyyy_/%5E%28%3F%3A0%5B1-9%5D%7C%5B12%5Dd%7C3%5B01%5D%29%5B/%5D%28%3F%3A0%5B1-9%5D%7C1%5B012%5D%29%5B/%5D%5B0-9%5D%7B4%7D%24");
   String dateFormatType = dateFormatProperty.substring(0, dateFormatProperty.indexOf('_'));
   String dateRegex = dateFormatProperty.substring(dateFormatProperty.indexOf('_') + 1, dateFormatProperty.length());
   dateFieldValues.put(FormElementConstants.DATE_FORMAT, dateFormatType);
   if (MapUtils.getBooleanValue(properties, ViewHelper.IS_CONTAINER)) {
    Map<String, Object> regexMap = new HashMap<String, Object>();
    regexMap.put(FormElementConstants.CONSTRAINT_REGEX, dateRegex);
    regexMap.put(FormElementConstants.CONSTRAINT_MSG, getEncodedValue(properties, FormElementConstants.CONSTRAINT_MESSAGE));
    @SuppressWarnings("unchecked")
    List<Map<String, Object>> rulesList = (List<Map<String, Object>>) dateFieldValues.get(FormElementConstants.RULES);
    dateFieldValues.put(FormElementConstants.RULES, rulesList);
   }
   LOGGER.debug("The data map returned for the date element is " + dateFieldValues);
   finalMap.put(jsonNameSpace, dateFieldValues);
  }
 }
 
 /**
  * Handle text box.
  * 
  * @param finalMap
  *         the final map
  * @param properties
  *         the properties
  * @param viewType
  *         the view type
  * @param jsonNameSpace
  *         the json name space
  * @param resources
  */
 private void handleTextBox(Map<String, Object> finalMap, Map<String, Object> properties, String viewType, String jsonNameSpace,
   Map<String, Object> resources) {
  if ("textarea".equalsIgnoreCase(viewType) || "text".equalsIgnoreCase(viewType)) {
   Map<String, Object> textValues = Collections.emptyMap();
   textValues = fetchFormElementData(properties, resources);
   
   String subtype = MapUtils.getString(properties, SUB_TYPE, null);
   String maxChar = MapUtils.getString(properties, MAX_CHAR, "0");
   String minChar = MapUtils.getString(properties, MIN_CHAR, "0");
   try {
    if (Integer.parseInt(maxChar) <= 0) {
     maxChar = "";
    }
    minChar = Integer.parseInt(minChar) <= 0 ? StringUtils.EMPTY : minChar;
   } catch (NumberFormatException nfe) {
    maxChar = "";
    minChar = StringUtils.EMPTY;
    LOGGER.error("Max Char provided in dialog is not a number ", nfe);
   }
   if (subtype != null) {
    textValues.put(SUB_TYPE, subtype);
   } else {
    textValues.put(SUB_TYPE, viewType);
   }
   textValues.put(MAX_CHAR, maxChar);
   textValues.put(MIN_CHAR, minChar);
   textValues.put(FormElementConstants.VIEW_TYPE, viewType);
   
   textValues.put(FormElementConstants.MULTIVALUE, Boolean.parseBoolean(MapUtils.getString(properties, FormElementConstants.MULTIVALUE, FALSE)));
   textValues.put(FormElementConstants.DEFAULT_VALUE, getEncodedValue(properties, FormElementConstants.DEFAULT_VALUE));
   
   if (MapUtils.getBooleanValue(properties, ViewHelper.IS_CONTAINER)) {
    Map<String, Object> regexMap = new HashMap<String, Object>();
    regexMap.put(FormElementConstants.CONSTRAINT_REGEX, MapUtils.getString(properties, FormElementConstants.CONSTRAINT_REGEX, ""));
    regexMap.put(FormElementConstants.CONSTRAINT_MSG, getEncodedValue(properties, FormElementConstants.CONSTRAINT_MESSAGE));
    @SuppressWarnings("unchecked")
    List<Map<String, Object>> rulesList = (List<Map<String, Object>>) textValues.get(FormElementConstants.RULES);
    rulesList.add(regexMap);
    textValues.put(FormElementConstants.RULES, rulesList);
   }
   finalMap.put(jsonNameSpace, textValues);
  }
 }
 
 /**
  * 
  * @param finalMap
  * @param properties
  * @param viewType
  * @param jsonNameSpace
  * @param resources
  */
 private void handleButton(Map<String, Object> finalMap, Map<String, Object> properties, String viewType, String jsonNameSpace,
   Map<String, Object> resources) {
  if ("button".equalsIgnoreCase(viewType)) {
   Map<String, Object> buttonValues = Collections.emptyMap();
   buttonValues = fetchFormElementData(properties, resources);
   buttonValues.put(FormElementConstants.VIEW_TYPE, viewType);
   
   finalMap.put(jsonNameSpace, buttonValues);
  }
 }
 
 /**
  * 
  * @param finalMap
  * @param properties
  * @param viewType
  * @param jsonNameSpace
  * @param resources
  */
 private void handleLink(Map<String, Object> finalMap, Map<String, Object> properties, String viewType, String jsonNameSpace,
   Map<String, Object> resources) {
  if ("link".equalsIgnoreCase(viewType)) {
   Map<String, Object> linkValues = Collections.emptyMap();
   linkValues = fetchFormElementData(properties, resources);
   linkValues.put(FormElementConstants.VIEW_TYPE, viewType);
   String link = getEncodedValue(properties, "link");
   link = StringUtils.isNotEmpty(link) ? link + ".html" : link;
   linkValues.put("link", link);
   String openInParentWindow = MapUtils.getString(properties, FormElementConstants.OPEN_IN_PARENT_WINDOW);
   linkValues.put(FormElementConstants.OPEN_IN_PARENT_WINDOW, Boolean.parseBoolean(openInParentWindow));
   
   finalMap.put(jsonNameSpace, linkValues);
  }
 }
 
 @SuppressWarnings("unchecked")
 private List<Map<String, String>> getErrorConfigs(final Map<String, Object> properties) {
  List<String> configs = new ArrayList<String>();
  List<Map<String, String>> configList = new ArrayList<Map<String, String>>();
  if (properties.containsKey(ERROR_CONFIGURATION)) {
   if (properties.get(ERROR_CONFIGURATION) instanceof List) {
    configs = (List<String>) properties.get(ERROR_CONFIGURATION);
   } else if (properties.get(ERROR_CONFIGURATION) instanceof String[]) {
    configs = (ArrayList<String>) properties.get(ERROR_CONFIGURATION);
   } else {
    configs.add((String) properties.get(ERROR_CONFIGURATION));
   }
  }
  for (String configString : configs) {
   try {
    configList.add(ComponentUtil.toMap(new JSONObject(configString)));
   } catch (JSONException e) {
    LOGGER.error("Unable to parse JSON", e);
   }
  }
  
  return configList;
  
 }
 
 /**
  * This method fetches the common data required by all the from elements, like fetching of element data, id, label, required filed and its
  * corresponding message.
  * 
  * @param properties
  *         Form element properties map
  * @param resources
  * @return The corresponding values set in the form element data
  */
 public Map<String, Object> fetchFormElementData(final Map<String, Object> properties, Map<String, Object> resources) {
  Map<String, Object> formData = new HashMap<String, Object>();
  formData.put(FormElementConstants.ELEMENT_LABEL, getEncodedValue(properties, FormElementConstants.ELEMENT_LABEL));
  formData.put(FormElementConstants.ELEMENT_ID, getEncodedValue(properties, FormElementConstants.ELEMENT_ID));
  formData.put(FormElementConstants.ELEMENT_DESCRIPTION, getEncodedValue(properties, FormElementConstants.ELEMENT_DESCRIPTION));
  formData.put(FormElementConstants.HIDE_TITLE, Boolean.parseBoolean(MapUtils.getString(properties, FormElementConstants.HIDE_TITLE, FALSE)));
  formData.put(FormElementConstants.SHOW_HELP_TEXT, Boolean.parseBoolean(MapUtils.getString(properties, FormElementConstants.SHOW_HELP_TEXT, FALSE)));
  boolean showHelpImage = Boolean.parseBoolean(MapUtils.getString(properties, FormElementConstants.SHOW_HELP_IMAGE, FALSE));
  formData.put(FormElementConstants.SHOW_HELP_IMAGE, showHelpImage);
  if (showHelpImage) {
   String helpImage = properties.get(FormElementConstants.HELP_IMAGE) != null ? (String) properties.get(FormElementConstants.HELP_IMAGE) : "";
   formData.put(FormElementConstants.HELP_IMAGE, helpImage);
  }
  
  formData.put(ERROR_CONFIGS, getErrorConfigs(properties).toArray());
  List<Map<String, Object>> rulesList = new LinkedList<Map<String, Object>>();
  Map<String, Object> requiredMap = new HashMap<String, Object>();
  requiredMap.put(FormElementConstants.REQUIRED_FIELD,
    Boolean.parseBoolean(MapUtils.getString(properties, FormElementConstants.REQUIRED_FIELD, FALSE)));
  requiredMap.put(FormElementConstants.REQUIRED_MESSAGE, getEncodedValue(properties, FormElementConstants.REQUIRED_MESSAGE));
  rulesList.add(requiredMap);
  formData.put(FormElementConstants.RULES, rulesList);
  String randomNumber = properties.get(UnileverConstants.RANDOM_NUMBER) != null ? (String) properties.get(UnileverConstants.RANDOM_NUMBER) : "";
  formData.put(UnileverConstants.RANDOM_NUMBER, randomNumber);
  formData.put("cssClass", MapUtils.getString(properties, "cssClass", StringUtils.EMPTY));
  try {
   Session repositorySession = getResourceResolver(resources).adaptTo(Session.class);
   String jsonPath = StringUtils.EMPTY;
   if (properties.containsKey("customValidationPath")) {
    jsonPath = properties.get("customValidationPath") + "/jcr:content";
    
    if (repositorySession.nodeExists(jsonPath)) {
     Node node = repositorySession.getNode(jsonPath);
     if (node.hasProperty(JcrConstants.JCR_DATA)) {
      javax.jcr.Property property = node.getProperty(JcrConstants.JCR_DATA);
      InputStream inputStream = property.getBinary().getStream();
      BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
      StringBuilder stringBuilder = new StringBuilder();
      String inputStreamLine = "";
      while ((inputStreamLine = bufferedReader.readLine()) != null) {
       stringBuilder.append(inputStreamLine);
      }
      String validationData = stringBuilder.toString();
      Object validationJSON = new JSONTokener(stringBuilder.toString()).nextValue();
      if (validationJSON instanceof JSONObject) {
       JSONObject jsonObject = new JSONObject(validationData);
       formData.put(CUSTOM_VALIDATION, toMap(jsonObject));
      } else if (validationJSON instanceof JSONArray) {
       JSONArray jsonArray = new JSONArray(validationData);
       formData.put(CUSTOM_VALIDATION, toList(jsonArray));
      } else {
       formData.put(CUSTOM_VALIDATION, "{}");
      }
      
     }
    }
   }
  } catch (javax.jcr.LoginException e) {
   LOGGER.error("Login Exception", e);
  } catch (RepositoryException e) {
   LOGGER.error("Repository Exception", e);
  } catch (JSONException e) {
   LOGGER.error("JSON Exception", e);
  } catch (IOException e) {
   LOGGER.error("IO Exception", e);
  }
  
  return formData;
 }
 
 /**
  * This method escapes the html characters if used any using the xssapi.encodeForHTML() method.
  * 
  * @param properties
  *         The properties map
  * @param propertyName
  *         The key name
  * @return The encoded string
  */
 @SuppressWarnings("deprecation")
 public String getEncodedValue(final Map<String, Object> properties, final String propertyName) {
  String fieldValue = MapUtils.getString(properties, propertyName, "");
  // xssapi.
  fieldValue = xssapi.encodeForHTML(fieldValue);
  return fieldValue;
 }
 
 /**
  * This method encodes the property to UTF-8 encoding.
  * 
  * @param properties
  *         The properties map
  * @param propertyName
  *         The key name
  * @return The UTF-8 encoded string.
  */
 public String getEncodedValueForPattern(final Map<String, Object> properties, final String propertyName) {
  String fieldValue = MapUtils.getString(properties, propertyName, "");
  try {
   fieldValue = URLEncoder.encode(fieldValue, "UTF-8");
  } catch (UnsupportedEncodingException e) {
   LOGGER.error(" Caught Exception in encoding pattern " + e);
  }
  return fieldValue;
 }
 
 /**
  * This method creates a map for drop down keys and its corresponding values which are used to populate the drop down form element.
  * 
  * @param properties
  *         properties map
  * @return the map with the drop down options
  */
 @SuppressWarnings("unchecked")
 private Map<String, String> populateDropDownOptions(final Map<String, Object> properties) {
  Map<String, String> optionsMap = new LinkedHashMap<String, String>();
  String[] dataKeys = null;
  String[] dataValues = null;
  if (checkInstanceOfListOrArray(properties)) {
   if (properties.get(FormElementConstants.DATA_SELECTION_KEY) instanceof List) {
    properties.put(FormElementConstants.DATA_SELECTION_KEY, ((ArrayList<String>) properties.get(FormElementConstants.DATA_SELECTION_KEY)).toArray());
    properties.put(FormElementConstants.DATA_SELECTION_VALUE,
      ((ArrayList<String>) properties.get(FormElementConstants.DATA_SELECTION_VALUE)).toArray());
    dataKeys = Arrays.copyOf((Object[]) properties.get(FormElementConstants.DATA_SELECTION_KEY),
      ((Object[]) properties.get(FormElementConstants.DATA_SELECTION_KEY)).length, String[].class);
    dataValues = Arrays.copyOf((Object[]) properties.get(FormElementConstants.DATA_SELECTION_VALUE),
      ((Object[]) properties.get(FormElementConstants.DATA_SELECTION_VALUE)).length, String[].class);
   } else {
    dataKeys = (String[]) properties.get(FormElementConstants.DATA_SELECTION_KEY);
    dataValues = (String[]) properties.get(FormElementConstants.DATA_SELECTION_VALUE);
   }
   
   for (int i = 0; i < dataKeys.length; i++) {
    optionsMap.put(dataKeys[i], dataValues[i]);
   }
  } else if (properties.containsKey(FormElementConstants.DATA_SELECTION_KEY)
    && properties.get(FormElementConstants.DATA_SELECTION_KEY) instanceof String) {
   optionsMap.put((String) properties.get(FormElementConstants.DATA_SELECTION_KEY),
     (String) properties.get(FormElementConstants.DATA_SELECTION_VALUE));
  }
  LOGGER.debug("The dropdown options map " + optionsMap);
  return optionsMap;
 }
 
 /**
  * This method creates a map for radio option keys and its corresponding values which are used to populate the radio form element.
  * 
  * @param properties
  *         properties map
  * @return the map with the radio options
  */
 @SuppressWarnings("unchecked")
 private Map<String, String> populateRadioOptions(final Map<String, Object> properties) {
  Map<String, String> optionsMap = new LinkedHashMap<String, String>();
  
  String[] dataKeys = null;
  String[] dataValues = null;
  if (checkInstanceOfListOrArray(properties)) {
   if (properties.get(FormElementConstants.DATA_RADIO_KEY) instanceof List) {
    properties.put(FormElementConstants.DATA_RADIO_KEY, ((ArrayList<String>) properties.get(FormElementConstants.DATA_RADIO_KEY)).toArray());
    properties.put(FormElementConstants.DATA_RADIO_VALUE, ((ArrayList<String>) properties.get(FormElementConstants.DATA_RADIO_VALUE)).toArray());
    dataKeys = Arrays.copyOf((Object[]) properties.get(FormElementConstants.DATA_RADIO_KEY),
      ((Object[]) properties.get(FormElementConstants.DATA_RADIO_KEY)).length, String[].class);
    dataValues = Arrays.copyOf((Object[]) properties.get(FormElementConstants.DATA_RADIO_VALUE),
      ((Object[]) properties.get(FormElementConstants.DATA_RADIO_VALUE)).length, String[].class);
   } else {
    dataKeys = (String[]) properties.get(FormElementConstants.DATA_RADIO_KEY);
    dataValues = (String[]) properties.get(FormElementConstants.DATA_RADIO_VALUE);
   }
   for (int i = 0; i < dataKeys.length; i++) {
    optionsMap.put(dataKeys[i], dataValues[i]);
   }
  } else if (properties.containsKey(FormElementConstants.DATA_RADIO_KEY) && properties.get(FormElementConstants.DATA_RADIO_KEY) instanceof String) {
   optionsMap.put((String) properties.get(FormElementConstants.DATA_RADIO_KEY), (String) properties.get(FormElementConstants.DATA_RADIO_VALUE));
  }
  LOGGER.debug("The radio options map " + optionsMap);
  return optionsMap;
 }
 
 /**
  * This method returns true if the dropdown keys and its corresponding values are instance of String[] or List.
  * 
  * @param properties
  *         the properties map
  * @return true or false
  */
 private boolean checkInstanceOfListOrArray(final Map<String, Object> properties) {
  boolean isListOfArray = properties.containsKey(FormElementConstants.DATA_SELECTION_KEY)
    && (properties.get(FormElementConstants.DATA_SELECTION_KEY) instanceof String[] || properties.get(FormElementConstants.DATA_SELECTION_KEY) instanceof List);
  isListOfArray = isListOfArray
    || (properties.containsKey(FormElementConstants.DATA_RADIO_VALUE) && (properties.get(FormElementConstants.DATA_RADIO_VALUE) instanceof String[] || properties
      .get(FormElementConstants.DATA_RADIO_VALUE) instanceof List));
  return isListOfArray;
  
 }
 
 private void handleStartSection(Map<String, Object> finalMap, Map<String, Object> properties, String viewType, String jsonNameSpace,
   Map<String, Object> resources) {
  if (("startsection".equalsIgnoreCase(viewType)) || ("endsection".equalsIgnoreCase(viewType))) {
   Map<String, Object> formsection = Collections.emptyMap();
   formsection = fetchFormElementData(properties, resources);
   
   String subtype = MapUtils.getString(properties, SUB_TYPE, null);
   if (subtype != null) {
    formsection.put(SUB_TYPE, subtype);
   } else {
    formsection.put(SUB_TYPE, viewType);
   }
   
   formsection.put(FormElementConstants.VIEW_TYPE, viewType);
   if ("startsection".equalsIgnoreCase(viewType)) {
    formsection.put(FormElementConstants.DEFAULT_VALUE, getEncodedValue(properties, FormElementConstants.DEFAULT_VALUE));
   }
   finalMap.put(jsonNameSpace, formsection);
   
  }
 }
 
 private void handleGoogleCapthcha(Map<String, Object> finalMap, Map<String, Object> properties, String viewType, String jsonNameSpace,
   final Map<String, Object> resources) {
  LOGGER.debug("viewType :" + viewType);
  if ("captcha".equalsIgnoreCase(viewType)) {
   Map<String, String> captchaMap = MapUtils.EMPTY_MAP;
   try {
    captchaMap = configurationService.getCategoryConfiguration(BaseViewHelper.getCurrentPage(resources), "googleCaptcha");
   } catch (ConfigurationNotFoundException e) {
    LOGGER.error("Caught Exception in getting captchaMap for category googleCaptcha", e);
   }
   if (!captchaMap.containsKey("googleCapthchaKey")) {
    captchaMap.put("googleCapthchaKey", "6LdSEw0TAAAAAGRKAPeRY-tX511HZAcLgGM4TPsX");
   }
   captchaMap.put(FormElementConstants.VIEW_TYPE, viewType);
   captchaMap.put("languageCode", MapUtils.getString(properties, "languageCode", "en"));
   finalMap.put(jsonNameSpace, captchaMap);
  }
 }
 
 /**
  * @param finalMap
  * @param properties
  * @param viewType
  * @param jsonNameSpace
  * @param resources
  */
 private void handleCheckBox(Map<String, Object> finalMap, Map<String, Object> properties, String viewType, String jsonNameSpace,
   Map<String, Object> resources) {
  if ("checkbox".equalsIgnoreCase(viewType)) {
   Map<String, Object> checkBoxValues = fetchFormElementData(properties, resources);
   checkBoxValues.put(FormElementConstants.VIEW_TYPE, "checkbox");
   String checked = MapUtils.getString(properties, FormElementConstants.CHECKED);
   checkBoxValues.put(FormElementConstants.CHECKED, Boolean.parseBoolean(checked));
   
   LOGGER.debug(THE_DATA_MAP_RETURNED_FOR_THE_RADIO_ELEMENT_IS + checkBoxValues);
   finalMap.put(jsonNameSpace, checkBoxValues);
   
  }
 }
 
 @SuppressWarnings("unchecked")
 private Map<String, String> populateCheckBoxOptions(final Map<String, Object> properties) {
  Map<String, String> optionsMap = new LinkedHashMap<String, String>();
  
  String[] dataKeys = null;
  String[] dataValues = null;
  if (checkInstanceOfListOrArray(properties)) {
   if (properties.get(FormElementConstants.DATA_CHECKBOX_KEY) instanceof List) {
    properties.put(FormElementConstants.DATA_CHECKBOX_KEY, ((ArrayList<String>) properties.get(FormElementConstants.DATA_CHECKBOX_KEY)).toArray());
    properties
      .put(FormElementConstants.DATA_CHECKBOX_VALUE, ((ArrayList<String>) properties.get(FormElementConstants.DATA_CHECKBOX_VALUE)).toArray());
    dataKeys = Arrays.copyOf((Object[]) properties.get(FormElementConstants.DATA_CHECKBOX_KEY),
      ((Object[]) properties.get(FormElementConstants.DATA_CHECKBOX_KEY)).length, String[].class);
    dataValues = Arrays.copyOf((Object[]) properties.get(FormElementConstants.DATA_CHECKBOX_VALUE),
      ((Object[]) properties.get(FormElementConstants.DATA_CHECKBOX_VALUE)).length, String[].class);
   } else {
    dataKeys = (String[]) properties.get(FormElementConstants.DATA_CHECKBOX_KEY);
    dataValues = (String[]) properties.get(FormElementConstants.DATA_CHECKBOX_VALUE);
   }
   for (int i = 0; i < dataKeys.length; i++) {
    optionsMap.put(dataKeys[i], dataValues[i]);
   }
  } else if (properties.containsKey(FormElementConstants.DATA_CHECKBOX_KEY)
    && properties.get(FormElementConstants.DATA_CHECKBOX_KEY) instanceof String) {
   optionsMap.put((String) properties.get(FormElementConstants.DATA_CHECKBOX_KEY), (String) properties.get(FormElementConstants.DATA_CHECKBOX_VALUE));
  }
  LOGGER.debug("The radio options map " + optionsMap);
  return optionsMap;
 }
 
 /**
  * Handle mutilple checkbox.
  * 
  * @param resources
  *         the resources
  * @param finalMap
  *         the final map
  * @param properties
  *         the properties
  * @param viewType
  *         the view type
  * @param jsonNameSpace
  *         the json name space
  */
 private void handleMultipleCheckbox(final Map<String, Object> resources, Map<String, Object> finalMap, Map<String, Object> properties,
   String viewType, String jsonNameSpace) {
  if ("multiplecheckbox".equalsIgnoreCase(viewType)) {
   Map<String, Object> checkboxValues = fetchFormElementData(properties, resources);
   String checkedIndex = MapUtils.getString(properties, "checkedIndex", "0");
   checkboxValues.put(FormElementConstants.VIEW_TYPE, "multiplecheckbox");
   checkboxValues.put(FormElementConstants.DEFAULT_VALUE, getEncodedValue(properties, FormElementConstants.DEFAULT_VALUE));
   checkboxValues.put(FormElementConstants.CHECKED_INDEX, Integer.parseInt(checkedIndex));
   if (properties.containsKey(FormElementConstants.OPTIONS_LOAD_PATH)) {
    LOGGER.info(THE_DYNAMIC_OPTION_LOAD_PATH_IS + (String) properties.get(FormElementConstants.OPTIONS_LOAD_PATH));
    SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) resources.get(ProductConstants.REQUEST);
    Resource resource = slingRequest.getResource();
    Map<String, String> displayValues = FormsHelper.getOptions(slingRequest, resource);
    LOGGER.debug(THE_DROP_DOWN_OPTIONS_DYNAMICALLY_LOADED_ARE + displayValues);
    checkboxValues.put(FormElementConstants.CHECKBOX_OPTIONS, displayValues);
   } else {
    checkboxValues.put(FormElementConstants.CHECKBOX_OPTIONS, populateCheckBoxOptions(properties));
   }
   LOGGER.debug(THE_DATA_MAP_RETURNED_FOR_THE_RADIO_ELEMENT_IS + checkboxValues);
   finalMap.put(jsonNameSpace, checkboxValues);
   
  }
 }
 
 private Map<String, Object> toMap(JSONObject object) throws JSONException {
  Map<String, Object> map = new HashMap<String, Object>();
  
  Iterator<String> keysItr = object.keys();
  while (keysItr.hasNext()) {
   String key = keysItr.next();
   Object value = object.get(key);
   
   if (value instanceof JSONArray) {
    value = toList((JSONArray) value);
   } else if (value instanceof JSONObject) {
    value = toMap((JSONObject) value);
   }
   map.put(key, value);
  }
  return map;
 }
 
 private Object toList(JSONArray array) throws JSONException {
  List<Object> list = new ArrayList<Object>();
  for (int i = 0; i < array.length(); i++) {
   Object value = array.get(i);
   if (value instanceof JSONArray) {
    value = toList((JSONArray) value);
   } else if (value instanceof JSONObject) {
    value = toMap((JSONObject) value);
   }
   list.add(value);
  }
  return list;
 }
}
