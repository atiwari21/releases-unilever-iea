/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.core.i18n;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.lang.StringUtils;

import com.day.cq.i18n.I18n;

/**
 * Wrapper class on AEM I18n to have a fall-back on the i18n object retrieved via base name.
 * 
 * @author nbhart
 * 
 */
public class UnileverI18nImpl extends I18n {
 
 /**
     * 
     */
 private I18n i18n;
 
 private ResourceBundle bundle;
 
 /**
  * @param bundle
  */
 public UnileverI18nImpl(ResourceBundle bundle) {
  super(bundle);
  this.bundle = bundle;
 }
 
 /**
  * @param key
  *         the key
  * @return value the value
  */
 public String get(String key) {
  
  String value = super.get(key);
  if (StringUtils.equals(value, key)) {
   value = getI18n().get(key);
  }
  return value;
 }
 
 /**
  * @param prefix
  *         the prefix
  * @return List the List
  */
 public List<Map<String, String>> getMap(String prefix) {
  Map<String, String> map = null;
  List<Map<String, String>> matchedKeys = new ArrayList<Map<String, String>>();
  List<String> allKeys = Collections.list(bundle.getKeys());
  for (String key : allKeys) {
   map = new HashMap<String, String>();
   if (key.startsWith(prefix)) {
    String value = get(key);
    String keyValue = key.substring(prefix.length() + 1);
    map.put("value", value);
    map.put("key", keyValue);
    matchedKeys.add(map);
   }
   
  }
  return matchedKeys;
 }
 
 /**
  * @return the i18n
  */
 public I18n getI18n() {
  return i18n;
 }
 
 /**
  * @param i18n
  *         the i18n to set
  */
 public void setI18n(I18n i18n) {
  this.i18n = i18n;
 }
 
}
