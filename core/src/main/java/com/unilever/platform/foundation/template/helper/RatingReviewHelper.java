/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.template.helper;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUse;
import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;

/**
 * The Class RatingReviewHelper.
 */
public class RatingReviewHelper extends WCMUse {
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(RatingReviewHelper.class);
 
 /** The Constant RATING_REVIEW_CATEGORY_KEY. */
 private static final String RATING_REVIEW_CATEGORY_KEY = "ratingAndReviews";
 
 private static final String TYPE_KIT_CATEGORY_KEY = "typeKit";
 
 /** The Constant SERVICE_PROVIDER. */
 private static final String SERVICE_PROVIDER = "serviceProviderName";
 
 private static final String ID = "id";
 
 /** The Constant ENABLED. */
 private static final String ENABLED = "enabled";
 
 /** The system name. */
 private String systemName;
 
 /** The enabled. */
 private boolean enabled;
 
 /** The variable typeKitId. */
 private String typeKitId;
 
 /** The variable isTypeKitConfigPresent. */
 private boolean isTypeKitConfigPresent;
 
 /** The variable isTypeKitIdBlank. */
 private boolean isTypeKitIdBlank;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.adobe.cq.sightly.WCMUse#activate()
  */
 @Override
 public void activate() {
  
  Page currentPage = getCurrentPage();
  SlingScriptHelper sling = getSlingScriptHelper();
  
  systemName = "nosystem.html";
  typeKitId = getTypeKitIdFromConfig(currentPage, sling);
  isTypeKitConfigPresent = getIfTypeKitConfigPresent(currentPage, sling);
  if (StringUtils.isBlank(typeKitId)) {
   isTypeKitIdBlank = true;
  } else {
   isTypeKitIdBlank = false;
  }
  
  enabled = isEnabled(currentPage, sling);
  if (enabled) {
   systemName = getRatingReviewSystemName(currentPage, sling) + ".jsp";
  }
  
 }
 
 private boolean getIfTypeKitConfigPresent(Page currentPage, SlingScriptHelper sling) {
  Map<String, String> categoryMap = null;
  boolean typeKitConfigPresent = true;
  ConfigurationService configurationService = sling.getService(ConfigurationService.class);
  try {
   categoryMap = configurationService.getCategoryConfiguration(currentPage, TYPE_KIT_CATEGORY_KEY);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Error in fetching typeKit category for page : " + currentPage.getPath(), e);
  }
  if (categoryMap == null || categoryMap.isEmpty()) {
   typeKitConfigPresent = false;
  } else {
   typeKitConfigPresent = true;
  }
  return typeKitConfigPresent;
 }
 
 /**
  * Gets the system name.
  * 
  * @return the system name
  */
 public String getSystemName() {
  return systemName;
 }
 
 public String getTypeKitId() {
  return typeKitId;
 }
 
 /**
  * Checks if is enabled.
  * 
  * @return true, if is enabled
  */
 public boolean isEnabled() {
  return enabled;
 }
 
 /**
  * Gets the rating review system name.
  * 
  * @param page
  *         the page
  * @param sling
  *         the sling
  * @return the rating review system name
  */
 private static String getRatingReviewSystemName(Page page, SlingScriptHelper sling) {
  
  LOGGER.debug("Getting Rating & Review System Name");
  
  Map<String, String> categoryMap = null;
  String serviceProviderName = StringUtils.EMPTY;
  try {
   ConfigurationService configurationService = sling.getService(ConfigurationService.class);
   categoryMap = configurationService.getCategoryConfiguration(page, RATING_REVIEW_CATEGORY_KEY);
   if ((categoryMap != null) && (categoryMap.containsKey(SERVICE_PROVIDER))) {
    serviceProviderName = categoryMap.get(SERVICE_PROVIDER);
    if (serviceProviderName != null) {
     serviceProviderName = serviceProviderName.toLowerCase();
    }
   }
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Error in fetching rating & review category for page : " + page.getPath(), e);
  }
  return serviceProviderName;
 }
 
 private static String getTypeKitIdFromConfig(Page page, SlingScriptHelper sling) {
  
  Map<String, String> categoryMap = null;
  String serviceProviderName = StringUtils.EMPTY;
  try {
   ConfigurationService configurationService = sling.getService(ConfigurationService.class);
   categoryMap = configurationService.getCategoryConfiguration(page, TYPE_KIT_CATEGORY_KEY);
   if ((categoryMap != null) && (categoryMap.containsKey(ID))) {
    serviceProviderName = categoryMap.get(ID);
    if (serviceProviderName != null) {
     serviceProviderName = serviceProviderName.toLowerCase();
    }
   }
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Error in fetching type kit id for page : " + page.getPath(), e);
  }
  return serviceProviderName;
 }
 
 /**
  * Checks if is enabled.
  * 
  * @param page
  *         the page
  * @param sling
  *         the sling
  * @return true, if is enabled
  */
 private static boolean isEnabled(Page page, SlingScriptHelper sling) {
  
  LOGGER.debug("Getting Rating & Review System enable status");
  
  Map<String, String> categoryMap = null;
  boolean isEnabled = false;
  String enabledValue = null;
  try {
   ConfigurationService configurationService = sling.getService(ConfigurationService.class);
   categoryMap = configurationService.getCategoryConfiguration(page, RATING_REVIEW_CATEGORY_KEY);
   if ((categoryMap != null) && (categoryMap.containsKey(ENABLED))) {
    enabledValue = categoryMap.get(ENABLED);
    if ((enabledValue != null) && ("true".equals(enabledValue))) {
     isEnabled = true;
    }
   }
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Error in fetching rating & review category for page : " + page.getPath(), e);
  }
  return isEnabled;
 }
 
 public boolean getIsTypeKitConfigPresent() {
  return isTypeKitConfigPresent;
 }
 
 public void setIsTypeKitConfigPresent(boolean isTypeKitConfigPresent) {
  this.isTypeKitConfigPresent = isTypeKitConfigPresent;
 }
 
 /**
  * @return the isTypeKitIdBlank
  */
 public boolean isTypeKitIdBlank() {
  return isTypeKitIdBlank;
 }
 
 /**
  * @param isTypeKitIdBlank
  *         the isTypeKitIdBlank to set
  */
 public void setTypeKitIdBlank(boolean isTypeKitIdBlank) {
  this.isTypeKitIdBlank = isTypeKitIdBlank;
 }
 
}
