/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;

import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * The Class FormHiddenViewHelperImpl.
 */
@Component(description = "FormHiddenViewHelperImpl", immediate = true, metatype = true, label = "FormHiddenViewHelperImpl")
@Service(value = { FormHiddenViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.FORM_HIDDEN, propertyPrivate = true),
  @Property(name = org.osgi.framework.Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class FormHiddenViewHelperImpl extends BaseViewHelper {
 
 /** The Constant DATA_EXCLUDE. */
 private static final String DATA_EXCLUDE = "dataExclude";
 
 /** The Constant HIDDEN_CLASS. */
 private static final String HIDDEN_CLASS = "hiddenClass";
 
 /** The Constant INPUT_TYPE. */
 private static final String INPUT_TYPE = "inputType";
 
 /** The Constant VALUE. */
 private static final String VALUE = "value";
 
 /** The Constant FORM_ELEMENT_ID. */
 private static final String FORM_ELEMENT_ID = "formElementId";
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  
  Map<String, Object> data = new HashMap<String, Object>();
  Map<String, Object> formHiddenMap = new HashMap<String, Object>();
  Map<String, Object> properties = getProperties(content);
  
  formHiddenMap.put(FORM_ELEMENT_ID, MapUtils.getString(properties, "name", StringUtils.EMPTY));
  formHiddenMap.put(VALUE, MapUtils.getString(properties, VALUE, StringUtils.EMPTY));
  formHiddenMap.put(INPUT_TYPE, MapUtils.getString(properties, INPUT_TYPE, StringUtils.EMPTY));
  formHiddenMap.put(HIDDEN_CLASS, MapUtils.getString(properties, HIDDEN_CLASS, StringUtils.EMPTY));
  formHiddenMap.put(DATA_EXCLUDE, MapUtils.getBoolean(properties, VALUE, false));
  
  data.put(getJsonNameSpace(content), formHiddenMap);
  
  return data;
 }
 
}
