/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.sling.api.SlingConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * 
 * 
 *
 */
@Component(description = "BackToTopCTA", immediate = true, metatype = true, label = "BackToTopCTAViewHelperImpl")
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.BACK_TO_TOP_CTA, propertyPrivate = true), })
public class BackToTopCTAViewHelperImpl extends BaseViewHelper {
 
 /** The Constant BACK_TO_TOP_CTA_LABEL. */
 private static final String BACK_TO_TOP_CTA_LABEL = "backToTopCtaLabel";
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(BackToTopCTAViewHelperImpl.class);
 
 /** The Constant BACK_TO_TOP_CTA_CATAGORY. */
 private static final String BACK_TO_TOP_CTA_CATAGORY = "backToTopCta";
 
 /**
     * 
     */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("Executing BackToTopCTAViewHelperImpl.java. Inside processData method");
  
  Map<String, Object> backToTopCtaMap = new LinkedHashMap<String, Object>();
  Map<String, Object> properties = getProperties(content);
  Map<String, String> configMap = new LinkedHashMap<String, String>();
  
  Page currentPage = getCurrentPage(resources);
  String backToTopCtaLabelKey = StringUtils.EMPTY;
  
  String backToTopCtaLabel = MapUtils.getString(properties, BACK_TO_TOP_CTA_LABEL, StringUtils.EMPTY);
  
  if (backToTopCtaLabel.isEmpty()) {
   try {
    configMap = configurationService.getCategoryConfiguration(currentPage, BACK_TO_TOP_CTA_CATAGORY);
   } catch (ConfigurationNotFoundException e) {
    LOGGER.error("Configuration for logo Not found:", e);
   }
   backToTopCtaLabelKey = MapUtils.getString(configMap, BACK_TO_TOP_CTA_LABEL, StringUtils.EMPTY);
   if (!backToTopCtaLabelKey.isEmpty()) {
    backToTopCtaLabel = getI18n(resources).get(backToTopCtaLabelKey);
   }
  }
  
  backToTopCtaMap.put("ctaLabel", backToTopCtaLabel);
  
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  data.put(getJsonNameSpace(content), backToTopCtaMap);
  
  return data;
 }
 
}
