/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.api.Product;
import com.adobe.cq.commerce.common.CommerceHelper;
import com.day.cq.tagging.Tag;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.aem.foundation.core.dto.SearchDTO;
import com.unilever.platform.aem.foundation.core.dto.TaggingSearchCriteria;
import com.unilever.platform.aem.foundation.core.service.SearchParam;
import com.unilever.platform.aem.foundation.core.service.SearchService;
import com.unilever.platform.foundation.components.ProductResultComparator;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.ImageHelper;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.constants.TaggingComponentsConstants;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * This class prepares the data map for Whats New Component It takes a product or a product range object, extracts the required information, and
 * passes the info as a json.
 */

/**
 * @author rhariv
 * 
 */
@Component(description = "Whats New component view helper", immediate = true, metatype = true, label = "Whats New ViewHelper")
@Service(value = { WhatsNewViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.WHATS_NEW, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class WhatsNewViewHelperImpl extends BaseViewHelper {
 
 /** numbers used for finding diff in production date and current date, to determine if product is new or not. */
 private static final int TWENTY_FOUR = 24;
 
 /** The Constant SIXTY. */
 private static final int SIXTY = 60;
 
 /** The Constant THOUSAND. */
 private static final int THOUSAND = 1000;
 
 /** default number of products if config is not found. */
 private static final int FIVE = 5;
 
 /** window for deciding new products. */
 private static final int THIRTY_ONE = 31;
 
 /** used to set limit in automatic population of products. */
 private static final String AUTO_PRODUCTS_LIMIT = "autoProductsLimit";
 
 /** Used to show if a product is new or not. */
 private static final String NO2 = "no";
 
 /** used to get the 'new' tag if a product is new. */
 private static final String NEW_I18N_TAG = "whatsnew.new";
 
 /** helper properties. */
 private static final String YES = "yes";
 
 /** helper property. */
 private static final String NAME = "name";
 
 /** used in cta maps. */
 private static final String CTA = "cta";
 
 /** used as a key in json. */
 private static final String OPEN_IN_NEW_WINDOW = "openInNewWindow";
 
 /** helper property. */
 private static final String LINK = "link";
 
 /** whether range should open in new window. */
 private static final String OPEN_PRODUCT_IN_NEW_WINDOW = "openProductInNewWindow";
 
 /** for storing information to be used in analytics. */
 private static final String PRODUCT_INFORMATION = "productInformation";
 
 /** cta label for product. */
 private static final String CTA_BUTTON_TEXT_PRODUCT = "CTAButtonTextProduct";
 
 /** constant for add product pathfield in manual selection. */
 private static final String ADD_PRODUCT = "addProduct";
 
 /** helper property. */
 private static final String TEXT_LINK = "textLink";
 
 /** helper property. */
 private static final String NO = "No";
 
 /** order in which the methor will sort the pages. */
 private static final String TAGGED_PAGES_SORTING_ORDER = "desc";
 
 /** tag used to get pages. */
 private static final String JCR_CONTENT_CONTENT_TYPE_TAG = "jcr:content/contentType";
 
 /** constant used as a key in json to denote tags on a product. */
 private static final String PRODUCTS_TAG = "productsTag";
 
 /** cta label for products in automatic population. */
 private static final String PROD_CTA_LABEL_AUTO = "prodCtaLabelAuto";
 
 /** product. */
 private static final String PRODUCT = "product";
 
 /** method of population of products. */
 private static final String PRODUCT_POPULATION_TYPE = "productPopulationType";
 
 /** if a range should open in a new window when cta is clicked. */
 private static final String OPEN_RANGE_IN_NEW_WINDOW = "openRangeInNewWindow";
 
 /** alt text for range images. */
 private static final String RANGE_IMAGE_ALT_TEXT = "rangeImageAltText";
 
 /** constant to store if the images should be prioritized. */
 private static final String PRIORITIZE_IMAGE_LEFT = "prioritizeImageLeft";
 
 /** to define if the type selected in the component is product or product ranges. */
 private static final String TYPE = "type";
 
 /** population type of range. */
 private static final String RANGE_POPULATION_TYPE = "rangePopulationType";
 
 /** helper property. */
 private static final String ON = "on";
 
 /** true. */
 private static final String TRUE = "true";
 
 /** false. */
 private static final String FALSE = "false";
 
 /** used as json key to store prioritize image. */
 private static final String PRIORITIZE_IMAGE = "prioritizeImage";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(WhatsNewViewHelperImpl.class);
 
 /** The Constant is not adaptive property. */
 @Reference
 ConfigurationService configurationService;
 /** The Constant is not adaptive property. */
 @Reference
 SearchService searchService;
 /** The Constant is range title property. */
 private static final String RANGE_TITLE = "rangeTitle";
 /** The Constant is product title property. */
 private static final String PRODUCT_TITLE = "productTitle";
 /** The Constant is product range property. */
 private static final String PRODUCT_RANGE = "productRange";
 /** The Constant is product ranges property. */
 private static final String PRODUCT_RANGES = "productRanges";
 /** The Constant is to select between product and ranges. */
 private static final String SELECT_PRODUCT_OR_RANGE = "selectProductOrRange";
 /** The Constant is the image for a range property. */
 private static final String RANGE_IMAGE = "rangeImage";
 /** The Constant is name of a range property. */
 private static final String RANGE_NAME = "rangeName";
 /** The Constant is a short copy of range property. */
 private static final String RANGE_SHORT_COPY = "rangeShortCopy";
 /** The Constant is cta button label property. */
 private static final String CTA_BUTTON_TEXT = "CTAButtonText";
 /** The Constant is cta button link property. */
 private static final String CTA_BUTTON_LINK = "CTAButtonLink";
 /** The Constant is range property. */
 private static final String RANGE = "range";
 /** The Constant is short copy property. */
 private static final String SHORT_COPY = "shortCopy";
 /** The Constant is new product property. */
 private static final String NEW_PRODUCT = "newProduct";
 /** The Constant is list of tags for products property. */
 private static final String TAG_LIST = "tagList";
 /** The Constant is products property. */
 private static final String PRODUCTS = "products";
 /** The Constant is max products allowed property. */
 private static final String MAXIMUM_PRODUCTS = "maximumProducts";
 /** The Constant is to enable product badges property. */
 private static final String ENABLE_PROD_BADGES = "enableProductBadge";
 /** The Constant is json name space property. */
 private static final String WHATS_NEW_CONFIGURATION_CATEGORY = "whatsNewComponent";
 /** The Constant is analytics category property. */
 /** The Constant is range badge for products property. */
 private static final String PRODUCT_RANGE_BADGE = "productRangeBadge";
 /** The Constant is range page in case of automatic population property. */
 private static final String PROPERTY_PROD_RANGE_PAGE_AUTO = "prodRangePageAuto";
 
 /** The Constant is manual population property. */
 public static final String MANUAL = "Manual ";
 /** The Constant is automatic population property. */
 public static final String AUTOMATIC = "Automatic";
 /** The Constant is minimum no of days for new product property. */
 private long showNewBadgeForDays;
 /** The Constant is no of days to be read from config property. */
 private static final String SHOW_NEW_BADGE_FOR_DAYS = "days";
 /** The Constant is ncta label property. */
 private static final String CTA_BUTTON_TEXT_AUTO = "CTAButtonTextAuto";
 /** The Constant is information for analyticse property. */
 private static final String PROPERTY_RANGE_AUTO_INFORMATION = "rangeAutoInformation";
 /** The Constant is open in new window property. */
 private static final String PROPERTY_OPENRANGE_NEW_WINDOW = "openRangeAutoInNewWindow";
 
 /** The Constant REG_EX_SPECIAL_CHAR. */
 private static final String REG_EX_SPECIAL_CHAR = "[^0-9^A-Z^a-z]";
 
 /*
  * responsible for reading the values from the dialog and creating a json, based on whther the user selects "products" or "product Ranges". It then
  * checks if "automatic" or "manual" population has been selected for the option selected above. This helper class then queries the product/ranges
  * based on tags or on inputs given by the author, and forwards it as a json
  * 
  * *
  */
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("executing WhatsNewViewHelperImpl. Inside processData method");
  Map<String, Object> properties = new HashMap<String, Object>();
  ResourceResolver resourceResolver = getResourceResolver(resources);
  SlingHttpServletRequest slingRequest = getSlingRequest(resources);
  Map<String, Object> whatsNewProperties = getProperties(content);
  Page currentPage = getCurrentPage(resources);
  try {
   String days = configurationService.getConfigValue(currentPage, UnileverConstants.PRODUCT_CATEGORY,
     UnileverConstants.IS_NEW_PRODUCT_LAUNCH_DATE_LIMIT);
   showNewBadgeForDays = Long.valueOf(days);
   LOGGER.debug("Key 'days' value from configuration {}", days);
  } catch (ConfigurationNotFoundException cnfe) {
   LOGGER.error("Configuration is not found for {}.", SHOW_NEW_BADGE_FOR_DAYS, cnfe);
   
  } catch (NumberFormatException nfe) {
   LOGGER.error("Count not format days to number {}. Defaulting to 31 days", showNewBadgeForDays, nfe);
   showNewBadgeForDays = THIRTY_ONE;
  }
  String propductsOrRange = MapUtils.getString(whatsNewProperties, SELECT_PRODUCT_OR_RANGE);
  LOGGER.debug("Requested properties for ", propductsOrRange);
  String defaultTitle = StringUtils.EMPTY;
  if (UnileverConstants.PRODUCT.equalsIgnoreCase(propductsOrRange)) {
   properties = getProducts(whatsNewProperties, resourceResolver, currentPage, resources);
   defaultTitle = MapUtils.getString(whatsNewProperties, PRODUCT_TITLE, StringUtils.EMPTY);
  } else if (PRODUCT_RANGE.equalsIgnoreCase(propductsOrRange)) {
   properties = getProductRange(resourceResolver, whatsNewProperties, properties, currentPage, slingRequest, resources);
   defaultTitle = MapUtils.getString(whatsNewProperties, RANGE_TITLE, StringUtils.EMPTY);
  }
  Map<String, String> anchorLink = ComponentUtil.getAnchorLinkMap(whatsNewProperties);
  if (StringUtils.isBlank(anchorLink.get(UnileverConstants.LABEL))) {
   anchorLink.put(UnileverConstants.LABEL, defaultTitle);
   anchorLink.put(UnileverConstants.ID,
     (defaultTitle.replaceAll(REG_EX_SPECIAL_CHAR, "").toLowerCase()) + MapUtils.getString(whatsNewProperties, UnileverConstants.RANDOM_NUMBER));
  }
  properties.put(UnileverConstants.ANCHOR_NAVIGATION, anchorLink);
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE), properties);
  return data;
  
 }
 
 /**
  * Gets the product range.
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param whatsNewProperties
  *         the whats new properties
  * @param properties
  *         the properties
  * @param currentPage
  *         the current page
  * @param slingRequest
  *         the sling request
  * @param resources
  * @return the product range
  */
 private Map<String, Object> getProductRange(ResourceResolver resourceResolver, Map<String, Object> whatsNewProperties,
   Map<String, Object> properties, Page currentPage, SlingHttpServletRequest slingRequest, Map<String, Object> resources) {
  
  Resource currentResource = getCurrentResource(resources);
  
  String prioritizeImage = MapUtils.getString(whatsNewProperties, PRIORITIZE_IMAGE, FALSE);
  if (prioritizeImage.trim().equals(ON)) {
   prioritizeImage = TRUE;
  }
  
  LOGGER.debug("Product population type: {}", MapUtils.getString(whatsNewProperties, RANGE_POPULATION_TYPE));
  if (AUTOMATIC.equalsIgnoreCase(MapUtils.getString(whatsNewProperties, RANGE_POPULATION_TYPE, StringUtils.EMPTY))) {
   List<Map<String, String>> productRangeList = ComponentUtil.getNestedMultiFieldProperties(currentResource, "panelsAuto");
   List<Map<String, Object>> rangeListFinal = prepareRangeListAutomatic(productRangeList, resourceResolver, currentPage, slingRequest);
   properties.put(PRODUCT_RANGES, rangeListFinal != null ? rangeListFinal.toArray() : StringUtils.EMPTY);
  } else {
   List<Map<String, String>> rangeMultifieldValues = ComponentUtil.getNestedMultiFieldProperties(currentResource, "panels");
   List<Map<String, Object>> sectionsListFinal = new ArrayList<Map<String, Object>>();
   
   sectionsListFinal = prepareRangeListManual(rangeMultifieldValues, resourceResolver, currentPage, slingRequest);
   LOGGER.debug("Number of ranges fetched is: {} And Time is: {}", sectionsListFinal.size(), System.currentTimeMillis());
   properties.put(PRODUCT_RANGES, sectionsListFinal != null ? sectionsListFinal.toArray() : StringUtils.EMPTY);
  }
  properties.put(TYPE, PRODUCT_RANGE);
  properties.put(ProductConstants.TITLE, MapUtils.getString(whatsNewProperties, RANGE_TITLE, StringUtils.EMPTY));
  properties.put(PRIORITIZE_IMAGE_LEFT, prioritizeImage);
  return properties;
 }
 
 /**
  * this method branches into two- manual product population and automatic product population.
  * 
  * @param productCopyProperties
  *         the product copy properties
  * @param resourceResolver
  *         the resource resolver
  * @param currentPage
  *         the current page
  * @param resources
  *         the resources
  * @return the products
  */
 private Map<String, Object> getProducts(Map<String, Object> productCopyProperties, ResourceResolver resourceResolver, Page currentPage,
   Map<String, Object> resources) {
  
  LOGGER.info("fetching products details into map");
  Map<String, Object> properties = new HashMap<String, Object>();
  String productPopulationType = MapUtils.getString(productCopyProperties, PRODUCT_POPULATION_TYPE, StringUtils.EMPTY).trim();
  String enableProductBadge = StringUtils.EMPTY;
  
  LOGGER.debug("Product Population Type is :{}", productPopulationType);
  if (MANUAL.trim().equalsIgnoreCase(productPopulationType)) {
   properties = getManualProducts(resourceResolver, currentPage, resources);
   
  } else if (AUTOMATIC.trim().equalsIgnoreCase(productPopulationType)) {
   properties = getProductsAutomatic(productCopyProperties, resourceResolver, currentPage, resources);
  }
  
  properties.put(TYPE, PRODUCT);
  properties.put(UnileverConstants.TITLE, MapUtils.getString(productCopyProperties, PRODUCT_TITLE, StringUtils.EMPTY));
  properties.put(PRIORITIZE_IMAGE_LEFT, TRUE);
  
  try {
   enableProductBadge = configurationService.getConfigValue(currentPage, WHATS_NEW_CONFIGURATION_CATEGORY, ENABLE_PROD_BADGES);
  } catch (ConfigurationNotFoundException e1) {
   
   LOGGER.error(" 'enableProductbadge' Configuration not found", e1);
  }
  
  properties.put(ENABLE_PROD_BADGES, enableProductBadge);
  
  return properties;
 }
 
 /**
  * gets product by searching product pages ,which have tags equal to the ones the author specifies in the global configuration files, and which have
  * the content type as 'product'.
  * 
  * @param whatsNewProperties
  *         the whats new properties
  * @param resourceResolver
  *         the resource resolver
  * @param currentPage
  *         the current page
  * @param resources
  *         the resources
  * @return the products automatic
  */
 private Map<String, Object> getProductsAutomatic(Map<String, Object> whatsNewProperties, ResourceResolver resourceResolver, Page currentPage,
   Map<String, Object> resources) {
  
  LOGGER.debug("Automatic population of products initializing. Time: {}", System.currentTimeMillis());
  
  List<String> propValue = new ArrayList<String>();
  List<String> rangeBadgeList = new ArrayList<String>();
  List<Object> productDetailsList = new ArrayList<Object>();
  List<String> productPagePathList = new ArrayList<String>();
  
  List<String> pageTagIdList = getPageTagIdList(currentPage);
  
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  
  String maximumProductsFromConfig = null;
  int maximumProducts = FIVE;
  
  int productsLimit = 0;
  String productRangeBadge = null;
  String ctalabel = MapUtils.getString(whatsNewProperties, PROD_CTA_LABEL_AUTO, StringUtils.EMPTY);
  
  maximumProductsFromConfig = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
    WHATS_NEW_CONFIGURATION_CATEGORY, MAXIMUM_PRODUCTS);
  
  if (maximumProductsFromConfig != null) {
   maximumProducts = Integer.parseInt(maximumProductsFromConfig);
  }
  
  productsLimit = Integer.parseInt(GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
    WHATS_NEW_CONFIGURATION_CATEGORY, AUTO_PRODUCTS_LIMIT));
  
  productRangeBadge = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, WHATS_NEW_CONFIGURATION_CATEGORY,
    PRODUCT_RANGE_BADGE);
  
  String configFilterTagNamespace = StringUtils.EMPTY;
  
  configFilterTagNamespace = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
    WHATS_NEW_CONFIGURATION_CATEGORY, UnileverConstants.FILTER_TAG_NAME_SPACE);
  
  String[] filters = ComponentUtil.getFilterTagsNamespace(whatsNewProperties, configFilterTagNamespace);
  LOGGER.debug("WhatsNewViewHelperImpl filters {}", filters);
  
  List<TaggingSearchCriteria> filteredTagList = ComponentUtil.getFilterTagList(currentPage, filters, pageTagIdList);
  LOGGER.debug("WhatsNewViewHelperImpl filteredTagList {}", filteredTagList);
  
  propValue.add(UnileverConstants.PRODUCT);
  
  String property = JCR_CONTENT_CONTENT_TYPE_TAG;
  Map<String, List<String>> propValueMap = new HashMap<String, List<String>>();
  propValueMap.put(property, propValue);
  
  List<String> negateTag = new ArrayList<String>();
  
  LOGGER.info("searching matching product from repository");
  /*
   * gets all the pages based on the tag provided in the global config
   */
  List<SearchDTO> taggedPagesList = searchService.getMultiTagResults(filteredTagList, negateTag, currentPage.getPath(), propValueMap,
    new SearchParam(0, productsLimit, TAGGED_PAGES_SORTING_ORDER), getResourceResolver(resources));
  
  LOGGER.debug("Pages found: {}", taggedPagesList);
  
  for (SearchDTO eachPage : taggedPagesList) {
   productPagePathList.add(eachPage.getResultNodePath());
  }
  String includeSegmentRulesProp = MapUtils.getString(whatsNewProperties, "includeSegmentRules", StringUtils.EMPTY);
  boolean includeSegmentRules = ("true").equalsIgnoreCase(includeSegmentRulesProp) ? true : false;
  String[] featuredTags = ComponentUtil.getUpdatedIncludedArrayFromResources(resources, new String[] {}, includeSegmentRules);
  if (featuredTags!=null && featuredTags.length > 0) {
   
   Collections.sort(productPagePathList, new ProductResultComparator(featuredTags, StringUtils.EMPTY, resourceResolver));
  } else {
   Collections.sort(productPagePathList, new ProductResultComparator(resourceResolver, true, false));
  }
  for (String taggedPage : productPagePathList) {
   Product product = CommerceHelper.findCurrentProduct(pageManager.getPage(taggedPage));
   UNIProduct uniProduct = null;
   if (product instanceof UNIProduct) {
    uniProduct = (UNIProduct) product;
   }
   List<String> productList = new ArrayList<String>();
   productList.add(taggedPage);
   Date launchDate = null;
   if (uniProduct != null) {
    launchDate = uniProduct.getProductionDate();
   }
   
   rangeBadgeList = getRangeBadge(productRangeBadge, pageManager, taggedPage);
   
   Map<String, Object> productDetails = getProductFromProductHelper(resources, productList);
   if (productDetails != null && !productDetails.isEmpty()) {
    Map<String, Object> ctaMap = getCTAMap(taggedPage, ctalabel, NO, resourceResolver, getSlingRequest(resources));
    
    productDetails.put(TEXT_LINK, ctaMap);
    productIsOldOrNew(resources, productDetails, launchDate, rangeBadgeList);
    
    productDetailsList.add(productDetails);
   }
   
  }
  if (productDetailsList.size() > maximumProducts) {
   productDetailsList = productDetailsList.subList(0, maximumProducts);
  }
  
  LOGGER.debug("No of products fetched is : {}. Time is: {}", productDetailsList.size(), System.currentTimeMillis());
  Map<String, Object> properties = new HashMap<String, Object>();
  properties.put(PRODUCTS, productDetailsList != null ? productDetailsList.toArray() : StringUtils.EMPTY);
  
  return properties;
  
 }
 
 /**
  * Gets the page tag id list.
  * 
  * @param currentPage
  *         the current page
  * @return the page tag id list
  */
 private List<String> getPageTagIdList(Page currentPage) {
  
  List<String> pageTagIdList = new ArrayList<String>();
  
  String tag = GlobalConfigurationUtility
    .getValueFromConfiguration(configurationService, currentPage, WHATS_NEW_CONFIGURATION_CATEGORY, PRODUCTS_TAG);
  LOGGER.debug("Tags configured for searching pages : {}", tag);
  
  if (tag != null) {
   if (tag.contains(",")) {
    String[] tags = tag.split(",");
    for (String s : tags) {
     pageTagIdList.add(s);
    }
   } else {
    pageTagIdList.add(tag);
   }
  }
  return pageTagIdList;
 }
 
 /**
  * Manual population of products . gets the data from the custom multifield in the dialog and passes it to getProductDetailsManual() as a list to get
  * the data
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param currentPage
  *         the current page
  * @param resources
  *         the resources
  * 
  * @return the manual products
  */
 private Map<String, Object> getManualProducts(ResourceResolver resourceResolver, Page currentPage, Map<String, Object> resources) {
  
  Resource currentResource = getCurrentResource(resources);
  
  List<Map<String, String>> productMultiFieldList = ComponentUtil.getNestedMultiFieldProperties(currentResource, "panelsProducts");
  List<Map<String, Object>> sectionsListFinal = getProductDetailsManual(resourceResolver, currentPage, productMultiFieldList, resources);
  LOGGER.debug("No of products fethced: {}  Time is: {}", sectionsListFinal.size(), System.currentTimeMillis());
  Map<String, Object> properties = new HashMap<String, Object>();
  properties.put(PRODUCTS, sectionsListFinal != null ? sectionsListFinal.toArray() : StringUtils.EMPTY);
  return properties;
 }
 
 /**
  * gets the range badge for each product.
  * 
  * @param productRangeBadge
  *         the product range badge
  * @param pageManager
  *         the page manager
  * @param taggedPage
  *         the tagged page
  * @return the range badge
  */
 private List<String> getRangeBadge(String productRangeBadge, PageManager pageManager, String taggedPage) {
  LOGGER.debug("Getting the range of the product");
  
  List<String> rangeBadgeList = new ArrayList<String>();
  
  Page page = pageManager.getPage(taggedPage);
  if (StringUtils.isNotBlank(productRangeBadge) && page != null) {
   
   String newProductRangeBadge = productRangeBadge.toLowerCase().trim();
   Tag[] tags = page.getTags();
   for (Tag tag : tags) {
    String tagId = tag != null ? tag.getTagID() : StringUtils.EMPTY;
    if (StringUtils.isNotBlank(tagId)) {
     tagId = tagId.toLowerCase().trim();
     if (tagId.startsWith(newProductRangeBadge)) {
      String subTagId = StringUtils.substringAfter(tagId, RANGE);
      if (!(RANGE.equals(subTagId))) {
       rangeBadgeList.add(StringUtils.substringAfterLast(subTagId, UnileverConstants.FORWARD_SLASH));
      }
     }
    }
   }
  }
  return rangeBadgeList;
 }
 
 /**
  * this method prepares a list of the product ranges when the user select automatic population for ranges.
  * 
  * @param productRangeList
  *         the product range list
  * @param resourceResolver
  *         the resource resolver
  * @param currentPage
  *         the current page
  * @param slingRequest
  *         the sling request
  * @return the list
  */
 private List<Map<String, Object>> prepareRangeListAutomatic(List<Map<String, String>> productRangeList, ResourceResolver resourceResolver,
   Page currentPage, SlingHttpServletRequest slingRequest) {
  
  List<Map<String, Object>> rangeList = new ArrayList<Map<String, Object>>();
  
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  LOGGER.debug("Preparing automatic range list.Time: {}", System.currentTimeMillis());
  for (Map<String, String> productRanageItem : productRangeList) {
   String rangePagePath = productRanageItem.get(PROPERTY_PROD_RANGE_PAGE_AUTO);
   if (StringUtils.isNotBlank(rangePagePath)) {
    Map<String, Object> rangeDetailsMap = new HashMap<String, Object>();
    Page rangePage = pageManager.getContainingPage(rangePagePath);
    if (rangePage != null) {
     ValueMap pageProperties = rangePage.getProperties();
     LOGGER.debug("Range Page Properties for {} : {}", rangePagePath, pageProperties);
     String name = StringUtils.defaultIfEmpty(pageProperties.get(UnileverConstants.TEASER_TITLE, String.class), StringUtils.EMPTY);
     String description = StringUtils.defaultIfEmpty(pageProperties.get(UnileverConstants.TEASER_COPY, String.class), StringUtils.EMPTY);
     String image = StringUtils.defaultIfEmpty(pageProperties.get(UnileverConstants.TEASER_IMAGE, String.class), StringUtils.EMPTY);
     String imageAltText = StringUtils.defaultIfEmpty(pageProperties.get(UnileverConstants.TEASER_IMAGE_ALT_TEXT, String.class), StringUtils.EMPTY);
     if (!name.isEmpty() && !description.isEmpty() && !image.isEmpty()) {
      LOGGER.debug("Creating CTA Map");
      String ctaButtonText = productRanageItem.get(CTA_BUTTON_TEXT_AUTO);
      Map<String, Object> ctaMap = new HashMap<String, Object>();
      ctaMap.put(LINK, ComponentUtil.getFullURL(resourceResolver, rangePage.getPath(), slingRequest));
      ctaMap.put(UnileverConstants.LABEL, ctaButtonText);
      String openInNewWindow = StringUtils.defaultIfEmpty(productRanageItem.get(PROPERTY_OPENRANGE_NEW_WINDOW), FALSE);
      openInNewWindow = openInNewWindowValue(openInNewWindow);
      ctaMap.put(OPEN_IN_NEW_WINDOW, openInNewWindow);
      
      String information = productRanageItem.get(PROPERTY_RANGE_AUTO_INFORMATION);
      if (StringUtils.isBlank(information)) {
       information = ctaButtonText;
      }
      
      if (StringUtils.isNotBlank(image)) {
       rangeDetailsMap.put(UnileverConstants.IMAGE, ImageHelper.getImageMap(image, currentPage, image, name, imageAltText, slingRequest));
      }
      rangeDetailsMap.put(CTA, ctaMap);
      rangeDetailsMap.put(NAME, name);
      rangeDetailsMap.put(SHORT_COPY, description);
      rangeList.add(rangeDetailsMap);
     }
    }
   }
  }
  LOGGER.debug("Range pages fetched successfully {}. And Time is: {}", rangeList.size(), System.currentTimeMillis());
  return rangeList;
 }
 
 /**
  * @param openInNewWindow
  * @return
  */
 private String openInNewWindowValue(String openInNewWindow) {
  String newWindow = StringUtils.EMPTY;
  if (TaggingComponentsConstants.TRUE.equals(openInNewWindow) || "[\"true\"]".equals(openInNewWindow)) {
   newWindow = TRUE;
  } else {
   newWindow = FALSE;
  }
  return newWindow;
 }
 
 /**
  * gets the CTA MAP for each link which will be used in a CTA button.
  * 
  * @param productPath
  *         the product path
  * @param ctalabel
  *         the ctalabel
  * @param openInNewWindow
  *         the open in new window
  * @param resourceResolver
  *         the resource resolver
  * @return the CTA map
  */
 private Map<String, Object> getCTAMap(String productPath, String ctalabel, String openInNewWindow, ResourceResolver resourceResolver,
   SlingHttpServletRequest slingRequest) {
  Map<String, Object> ctaMap = new HashMap<String, Object>();
  ctaMap.put(LINK, ComponentUtil.getFullURL(resourceResolver, productPath, slingRequest));
  ctaMap.put(UnileverConstants.LABEL, ctalabel);
  ctaMap.put(OPEN_IN_NEW_WINDOW, openInNewWindow);
  
  return ctaMap;
 }
 
 /**
  * checks if the product is old or new, based on the launch date.
  * 
  * @param productDetails
  *         the product details
  * @param launchDate
  *         the launch date
  * @param tagList
  *         the tag list
  */
 private void productIsOldOrNew(Map<String, Object> resources, Map<String, Object> productDetails, Date launchDate, List<String> tagList) {
  
  LOGGER.info("checking if the product is old or new");
  Date date = Calendar.getInstance().getTime();
  
  if (launchDate == null) {
   productDetails.put(NEW_PRODUCT, StringUtils.EMPTY);
   productDetails.put(TAG_LIST, tagList);
  } else {
   long diffInDays = getDateDifferenceInDays(date, launchDate);
   if (diffInDays <= showNewBadgeForDays) {
    LOGGER.info("Product is new");
    productDetails.put(NEW_PRODUCT, YES);
    String tag = getI18n(resources).get(NEW_I18N_TAG);
    tagList.add(tag);
    productDetails.put(TAG_LIST, tagList);
   } else {
    LOGGER.info("Product is not new");
    productDetails.put(NEW_PRODUCT, NO2);
   }
   
  }
 }
 
 /**
  * creates a list of the values for Products selected in multifield when the population type is "Manual".
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param currentPage
  *         the current page
  * @param sectionsList
  *         the sections list
  * @param resources
  *         the resources
  * @return the product details manual
  */
 private List<Map<String, Object>> getProductDetailsManual(ResourceResolver resourceResolver, Page currentPage,
   final List<Map<String, String>> sectionsList, Map<String, Object> resources) {
  
  LOGGER.debug("Fetching details for product configured manually. Time: {}", System.currentTimeMillis());
  List<Map<String, Object>> products = new ArrayList<Map<String, Object>>();
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  
  for (Map<String, String> sectionMap : sectionsList) {
   String productPagePath = sectionMap.get(ADD_PRODUCT);
   if (StringUtils.isNotEmpty(productPagePath) && resourceResolver != null) {
    
    Page productPage = pageManager.getPage(productPagePath);
    if (productPage != null) {
     LOGGER.debug("Product page Found : {}", productPage.getPath());
     UNIProduct product = (UNIProduct) CommerceHelper.findCurrentProduct(productPage);
     
     Map<String, Object> prodDetails = getProductDetailsMap(product, sectionMap, currentPage, productPagePath, resourceResolver, resources);
     if (prodDetails != null) {
      products.add(prodDetails);
     }
    }
   }
  }
  return products;
 }
 
 /**
  * Gets the product details map.
  * 
  * @param product
  *         the product
  * @param sectionMap
  *         the section map
  * @param currentPage
  *         the current page
  * @param productPagePath
  *         the product page path
  * @param resourceResolver
  *         the resource resolver
  * @param resources
  *         the resources
  * @return the product details map
  */
 private Map<String, Object> getProductDetailsMap(UNIProduct product, Map<String, String> sectionMap, Page currentPage, String productPagePath,
   ResourceResolver resourceResolver, Map<String, Object> resources) {
  
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  Map<String, Object> productDetails = null;
  
  if (product != null) {
   LOGGER.debug("Product Found : {}", product.getPageTitle());
   String ctaButtonLabel = sectionMap.get(CTA_BUTTON_TEXT_PRODUCT);
   String information = sectionMap.get(PRODUCT_INFORMATION);
   String openInNewWindow = StringUtils.defaultIfEmpty(sectionMap.get(OPEN_PRODUCT_IN_NEW_WINDOW), FALSE);
   
   List<String> productList = new ArrayList<String>();
   productList.add(productPagePath);
   
   openInNewWindow = openInNewWindowValue(openInNewWindow);
   
   if (StringUtils.isEmpty(information)) {
    information = ctaButtonLabel;
   }
   
   Date launchDate = product.getProductionDate();
   String productRangeBadge = StringUtils.EMPTY;
   
   productRangeBadge = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, WHATS_NEW_CONFIGURATION_CATEGORY,
     PRODUCT_RANGE_BADGE);
   
   Map<String, Object> ctaMap = getCTAMap(productPagePath, ctaButtonLabel, openInNewWindow, resourceResolver, getSlingRequest(resources));
   List<String> rangeBadges = getRangeBadge(productRangeBadge, pageManager, productPagePath);
   productDetails = getProductFromProductHelper(resources, productList);
   if (productDetails != null && !productDetails.isEmpty()) {
    productDetails.put(TEXT_LINK, ctaMap);
    productIsOldOrNew(resources, productDetails, launchDate, rangeBadges);
   }
  }
  
  return productDetails;
 }
 
 /**
  * gets the product list.
  * 
  * @param resources
  *         the resources
  * @param productList
  *         the product list
  * @return the product from product helper
  */
 private Map<String, Object> getProductFromProductHelper(Map<String, Object> resources, List<String> productList) {
  
  Map<String, Object> productDetails = null;
  
  List<Map<String, Object>> finalList = ProductHelper.getProductList(productList, resources, null);
  
  LOGGER.debug("Final List of  products from helper : {}", finalList);
  
  if (finalList != null && !finalList.isEmpty()) {
   productDetails = finalList.get(0);
  } else {
   productDetails = new HashMap<String, Object>();
  }
  LOGGER.info("Fetched the details of product from product helper");
  return productDetails;
 }
 
 /**
  * creates a list of values of custom multifield for Product Range.
  * 
  * @param rangeMultifieldValues
  *         the range multifield values
  * @param resourceResolver
  *         the resource resolver
  * @param currentPage
  *         the current page
  * @param slingRequest
  *         the sling request
  * @return the list
  */
 private List<Map<String, Object>> prepareRangeListManual(final List<Map<String, String>> rangeMultifieldValues, ResourceResolver resourceResolver,
   Page currentPage, SlingHttpServletRequest slingRequest) {
  
  List<Map<String, Object>> finalRangeList = new ArrayList<Map<String, Object>>();
  LOGGER.debug("Preparing range list manually. Time: {}", System.currentTimeMillis());
  for (Map<String, String> eachRangeMultifield : rangeMultifieldValues) {
   
   Map<String, Object> ctaMap = new HashMap<String, Object>();
   Map<String, Object> imageDetailsContainerMap = new HashMap<String, Object>();
   String productImagePath = eachRangeMultifield.get(RANGE_IMAGE);
   String altText = eachRangeMultifield.get(RANGE_IMAGE_ALT_TEXT);
   imageDetailsContainerMap.put(UnileverConstants.IMAGE,
     ImageHelper.getImageMap(productImagePath, currentPage, productImagePath, altText, altText, slingRequest));
   
   imageDetailsContainerMap.put(NAME, StringUtils.defaultIfEmpty(eachRangeMultifield.get(RANGE_NAME), StringUtils.EMPTY));
   imageDetailsContainerMap.put(SHORT_COPY, StringUtils.defaultIfEmpty(eachRangeMultifield.get(RANGE_SHORT_COPY), StringUtils.EMPTY));
   ctaMap.put(LINK, ComponentUtil.getFullURL(resourceResolver, eachRangeMultifield.get(CTA_BUTTON_LINK), slingRequest));
   ctaMap.put(UnileverConstants.LABEL, StringUtils.defaultIfEmpty(eachRangeMultifield.get(CTA_BUTTON_TEXT), StringUtils.EMPTY));
   
   String openInNewWindow = StringUtils.defaultIfEmpty(eachRangeMultifield.get(OPEN_RANGE_IN_NEW_WINDOW), FALSE);
   openInNewWindow = openInNewWindowValue(openInNewWindow);
   
   ctaMap.put(OPEN_IN_NEW_WINDOW, openInNewWindow);
   
   imageDetailsContainerMap.put(CTA, ctaMap);
   
   finalRangeList.add(imageDetailsContainerMap);
  }
  return finalRangeList;
 }
 
 /**
  * get the difference of the production date and current date to determine if a product is new or not.
  * 
  * @param firstDate
  *         the first date
  * @param secondDate
  *         the second date
  * @return the date difference in days
  */
 private long getDateDifferenceInDays(Date firstDate, Date secondDate) {
  long startTime = secondDate.getTime();
  long endTime = firstDate.getTime();
  long diffTime = endTime - startTime;
  return diffTime / (THOUSAND * SIXTY * SIXTY * TWENTY_FOUR);
 }
}
