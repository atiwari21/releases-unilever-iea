/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.utils.AdaptiveUtil;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.template.dto.Image;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * This class prepares the data map for Anchor Link.
 */
@Component(description = "AnchorLink", immediate = true, metatype = true, label = "AnchorLinkViewHelperImpl")
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.ANCHOR_LINK_NAVIGATION_V2, propertyPrivate = true), })
public class AnchorLinkNavigationV2ViewHelperImpl extends BaseViewHelper {
 /** The Constant ALT_TEXT. */
 private static final String ALT_TEXT = "altText";
 
 /** The Constant ANCHOR_LINK_CATEGORY. */
 private static final String ANCHOR_LINK_CATEGORY = "anchorLinkV2Component";
 
 /** The Constant ANCHOR_LINK_LOGO. */
 private static final String ANCHOR_LINK_LOGO = "logo";
 
 /** The Constant ANCHOR_LINK_MOBILE_TEXT. */
 private static final String ANCHOR_LINK_MOBILE_TEXT = "mobileText";
 
 /** i18n tag for title */
 private static final String ANCHOR_LINK_MOBILE_TEXT_I18N = "anchorlink.mobileText";
 
 /** Constant for getting anchor link label */
 private static final String ANCHOR_LABEL = "label";
 
 /** Constant for getting anchor link flag show as navigatiion */
 private static final String SHOW_AS_NAV = "showAsNavigation";
 
 /** Constant for getting random number property */
 private static final String RANDOM_NUMBER = "randomNumber";
 
 /** Constant for setting anchor link id */
 private static final String ANCHOR_ID = "id";
 
 /** The Constant ANCHOR_LINKS. */
 private static final String ANCHOR_LINKS = "anchorLinks";
 
 /** The Constant REG_EX_SPECIAL_CHAR. */
 private static final String REG_EX_SPECIAL_CHAR = "[^0-9^A-Z^a-z]";
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(AnchorLinkNavigationV2ViewHelperImpl.class);
 
 /** The Constant HEADING_TEXT. */
 private static final String HEADING_TEXT = "headingText";
 
 /*
  * (Javadoc)
  * 
  * This helper class reads all the data from the anchor link component.It takes the logo, alt text and It fetcher "mobile view text" from the global
  * configsUsing the logo field, it extracts the extension, path, name of the file and passesit in the json
  * 
  * For details on json and component structure,please visit https://tools.sapient .com/confluence/display/UU/CP-03+Anchor+Link+Navigation
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("Executing AnchorLinkViewHelperImpl.java. Inside processData method");
  Map<String, Object> properties = getProperties(content);
  Map<String, Object> anchorLinkMap = new HashMap<String, Object>();
  Map<String, String> configMap = new LinkedHashMap<String, String>();
  Map<String, Object> finalMap = new LinkedHashMap<String, Object>();
  Resource currentResource = getCurrentResource(resources);
  SlingHttpServletRequest slingRequest = getSlingRequest(resources);
  
  Page currentPage = getCurrentPage(resources);
  I18n i18n = getI18n(resources);
  Resource parentResource = currentResource.getParent();
  Node parentNode = parentResource.adaptTo(Node.class);
  
  if (AdaptiveUtil.isAdaptive(slingRequest)) {
   anchorLinkMap.put(ANCHOR_LINKS, getAnchorLinksMap(parentNode).toArray());
  }
  
  LOGGER.info("Component found for current page " + currentResource.getName());
  // stores the extension, file name and file path if the "logo" field
  // is given
  
  try {
   configMap = configurationService.getCategoryConfiguration(currentPage, ANCHOR_LINK_CATEGORY);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Configuration for logo Not found:", e);
  }
  
  finalMap = getOverriddenConfigMap(configMap, properties);
  String headingText = MapUtils.getString(finalMap, "headingText", StringUtils.EMPTY);
  boolean headingTextEmptyFlag = false;
  
  if (StringUtils.isBlank(headingText)) {
   headingTextEmptyFlag = true;
   headingText = i18n.get("anchorLink.mobileNavigation.ctaLabel");
  }
  
  Image image = new Image(MapUtils.getString(finalMap, ANCHOR_LINK_LOGO, StringUtils.EMPTY),
    MapUtils.getString(finalMap, ALT_TEXT, StringUtils.EMPTY), StringUtils.EMPTY, currentPage, slingRequest);
  anchorLinkMap.put(ANCHOR_LINK_LOGO, image.convertToMap());
  anchorLinkMap.put(HEADING_TEXT, headingText);
  anchorLinkMap.put("headingTextEmptyFlag", headingTextEmptyFlag);
  
  String anchorLinkTitle = StringUtils.EMPTY;
  anchorLinkTitle = i18n.get(ANCHOR_LINK_MOBILE_TEXT_I18N);
  anchorLinkMap.put(ANCHOR_LINK_MOBILE_TEXT, anchorLinkTitle);
  LOGGER.info("Exiting anchor link view helper ");
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(getJsonNameSpace(content), anchorLinkMap);
  return data;
  
 }
 
 private Map<String, Object> getOverriddenConfigMap(Map<String, String> configMap, Map<String, Object> properties) {
  Map<String, Object> finalMap = new LinkedHashMap<String, Object>();
  
  String headingText = MapUtils.getString(properties, HEADING_TEXT, StringUtils.EMPTY);
  String logo = MapUtils.getString(properties, ANCHOR_LINK_LOGO, StringUtils.EMPTY);
  String altText = MapUtils.getString(properties, ALT_TEXT, StringUtils.EMPTY);
  
  if (StringUtils.isBlank(headingText)) {
   headingText = MapUtils.getString(configMap, HEADING_TEXT, StringUtils.EMPTY);
  }
  if (StringUtils.isBlank(logo)) {
   headingText = MapUtils.getString(configMap, ANCHOR_LINK_LOGO, StringUtils.EMPTY);
  }
  if (StringUtils.isBlank(altText)) {
   headingText = MapUtils.getString(configMap, ALT_TEXT, StringUtils.EMPTY);
  }
  finalMap.put(HEADING_TEXT, headingText);
  finalMap.put(ANCHOR_LINK_LOGO, logo);
  finalMap.put(ALT_TEXT, altText);
  
  return finalMap;
 }
 
 /**
  * 
  * @param parentNode
  * @param currentPagePath
  * @param resourceResolver
  * @return anchorLinkDetailList
  */
 private List<Map<String, Object>> getAnchorLinksMap(Node parentNode) {
  List<Map<String, Object>> anchorLinkDetailList = new ArrayList<Map<String, Object>>();
  NodeIterator childNodes = getChildNodes(parentNode);
  while (childNodes.hasNext()) {
   anchorLinkDetailList = getAnchorList(anchorLinkDetailList, childNodes);
   NodeIterator parysNodes = getChildNodes(childNodes.nextNode());
   while (parysNodes.hasNext()) {
    anchorLinkDetailList = getAnchorList(anchorLinkDetailList, parysNodes);
   }
  }
  return anchorLinkDetailList;
 }
 
 /**
  * gets the child nodes of node
  * 
  * @param parentNode
  * @return
  */
 private NodeIterator getChildNodes(Node parentNode) {
  NodeIterator childNodes = null;
  try {
   childNodes = parentNode.getNodes();
  } catch (RepositoryException e) {
   LOGGER.error("Path Not Found Exception for the node path ", e);
  }
  return childNodes;
 }
 
 /**
  * gets the list of anchor link map
  * 
  * @param anchorLinkDetailList
  * @param childNodes
  * @return list
  */
 private List<Map<String, Object>> getAnchorList(List<Map<String, Object>> anchorLinkDetailList, NodeIterator childNodes) {
  Map<String, Object> anchorNavMap = new HashMap<String, Object>();
  Node parsysLevelNode = childNodes.nextNode();
  try {
   if (parsysLevelNode.hasProperty(ANCHOR_LABEL) && parsysLevelNode.hasProperty(SHOW_AS_NAV)) {
    anchorNavMap = getAnchorMap(parsysLevelNode);
    anchorLinkDetailList.add(anchorNavMap);
   }
  } catch (RepositoryException e) {
   LOGGER.equals("Path Not Found Exception for the node path " + e);
  }
  return anchorLinkDetailList;
 }
 
 /**
  * 
  * @param node
  * @param path
  * @param resourceResolver
  * @return anchorMap
  */
 private Map<String, Object> getAnchorMap(Node node) {
  Map<String, Object> anchorMap = new HashMap<String, Object>();
  String anchorLabel = getNodeProperty(node, ANCHOR_LABEL);
  String showAsNavigation = getNodeProperty(node, SHOW_AS_NAV);
  String randomNumber = getNodeProperty(node, RANDOM_NUMBER);
  String id = anchorLabel.replaceAll(REG_EX_SPECIAL_CHAR, "").toLowerCase() + randomNumber;
  
  anchorMap.put(ANCHOR_LABEL, anchorLabel);
  anchorMap.put(SHOW_AS_NAV, showAsNavigation);
  anchorMap.put(ANCHOR_ID, "#" + id);
  return anchorMap;
 }
 
 /**
  * returns property value of key
  * 
  * @param node
  * @param propertyKey
  * @return string
  */
 private String getNodeProperty(Node node, String propertyKey) {
  String propertyValue = null;
  try {
   propertyValue = node.getProperty(propertyKey).getString();
  } catch (RepositoryException e) {
   LOGGER.error("Error while getting node property", e);
  }
  return propertyValue;
  
 }
}
