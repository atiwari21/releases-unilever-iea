/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.foundation.components.helper;

import java.util.LinkedHashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * The Class EGiftingHelper.
 * 
 * @author asri54
 */
public final class EGiftingHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(EGiftingHelper.class);
 
 /** The Constant TOTAL_LABEL. */
 private static final String TOTAL_LABEL = "orderSummary.totalLabel";
 
 /** The Constant DELIVER_LABEL. */
 private static final String DELIVER_LABEL = "orderSummary.deliveryLabel";
 
 /** The Constant GIFT_WRAPPING_LABEL. */
 private static final String GIFT_WRAPPING_LABEL = "orderSummary.giftWrappingLabel";
 
 /** The Constant ORDER_SUMMARY_TITLE. */
 private static final String ORDER_SUMMARY_TITLE = "orderSummary.title";
 
 /** The Constant QUANTITY_LABEL. */
 private static final String QUANTITY_LABEL = "orderSummary.quantity";
 
 /**
  * Instantiates a new e gifting helper.
  */
 private EGiftingHelper() {
  
 }
 
 /**
  * Gets the order summary map.
  * 
  * @param i18n
  * @return the order summary map
  */
 
 public static Map<String, Object> getOrderSummaryMap(I18n i18n) {
  LOGGER.info("Order Summary View Helper #processData called for processing fixed list.");
  String label = i18n.get(ORDER_SUMMARY_TITLE) != null ? i18n.get(ORDER_SUMMARY_TITLE) : "";
  String totalLabel = i18n.get(TOTAL_LABEL) != null ? i18n.get(TOTAL_LABEL) : "";
  
  String deliveryLabel = i18n.get(DELIVER_LABEL) != null ? i18n.get(DELIVER_LABEL) : "";
  
  String giftWrappingLabel = i18n.get(GIFT_WRAPPING_LABEL) != null ? i18n.get(GIFT_WRAPPING_LABEL) : "";
  
  String quantityLabel = i18n.get(QUANTITY_LABEL) != null ? i18n.get(QUANTITY_LABEL) : "";
  
  Map<String, Object> orderSummary = new LinkedHashMap<String, Object>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  orderSummary.put("label", label);
  orderSummary.put("totalLabel", totalLabel);
  orderSummary.put("deliveryLabel", deliveryLabel);
  orderSummary.put("giftWrappingLabel", giftWrappingLabel);
  orderSummary.put("quantityLabel", quantityLabel);
  orderSummary.put(ProductConstants.EGIFTING_FREE_LABEL, i18n.get(ProductConstants.EGIFTING_FREE_KEY));
  return orderSummary;
 }
 
}
