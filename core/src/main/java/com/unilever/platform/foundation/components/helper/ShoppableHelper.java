/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.foundation.components.helper;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * The Class ShoppableHelper.
 */
public final class ShoppableHelper {
 
 private static final String YES_REMOVE_THIS_ITEM_LABEL = "yesRemoveThisItemLabel";
 
 private static final String REMOVE_THIS_PRODUCT_LABEL = "removeThisProductLabel";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ShoppableHelper.class);
 
 /** The Constant RETAILER_LABEL. */
 private static final String RETAILER_TEXT = "retailerText";
 
 /** The Constant PRODUCT_LABEL. */
 private static final String PRODUCT_TEXT = "productText";
 
 /** The Constant SIZE. */
 private static final String SIZE_TEXT = "sizeText";
 
 /** The Constant QUANTITY_LABEL. */
 private static final String QUANTITY_LABEL = "quantityText";
 
 /** The Constant PRICE_LABEL. */
 private static final String PRICE_LABEL = "priceText";
 
 /** The Constant TOTAL_LABEL. */
 private static final String TOTAL_LABEL = "totalText";
 
 /** The Constant SUB_TOTAL_LABEL. */
 private static final String SUB_TOTAL_TEXT = "subTotalText";
 
 /** The Constant REMOVE_LABEL. */
 private static final String REMOVE_TEXT = "removeText";
 
 /** The Constant EXIT_LABEL. */
 private static final String EXIT_LABEL = "exitLabel";
 
 /** The Constant SHIPPING_AND_TAX_LABEL. */
 private static final String SHIPPING_AND_TAX_TEXT = "shippingAndTaxText";
 
 /** The Constant CONTINUE_SHOPPING_LABEL. */
 private static final String CONTINUE_SHOPPING_TEXT = "continueShoppingText";
 
 /** The Constant CHECK_OUT_LABEL. */
 private static final String CHECK_OUT_TEXT = "checkOutText";
 
 /** The Constant PREFIX. */
 private static final String PREFIX = "shoppable.";
 
 /** The Constant PREFIX_SHOPPABLE_BAG. */
 private static final String PREFIX_SHOPPING_BAG = "shoppingBag.";
 
 /** The Constant PREFIX_COLUMN_HEADER. */
 private static final String PREFIX_COLUMN_HEADER = "columnHeader.";
 
 /** The Constant PREFIX_EDIT_ITEM. */
 private static final String PREFIX_EDIT_ITEM = "editItem.";
 
 /** The Constant PREFIX_REMOVE_ITEM. */
 private static final String PREFIX_REMOVE_ITEM = "removeItem.";
 
 /** The Constant PREFIX_MINI_SHOPPING_BAG. */
 private static final String PREFIX_MINI_SHOPPING_BAG = "miniShoppingBag.";
 
 /** The Constant SHOPPING_CART_POP_HEADING. */
 private static final String SHOPPING_CART_POP_HEADING = "shoppingCartPopHeading";
 
 /** The Constant VIEW_BAG_TEXT. */
 private static final String VIEW_BAG_TEXT = "viewBagText";
 
 /** The Constant ORDER_SUMMARY_TEXT. */
 private static final String ORDER_SUMMARY_TEXT = "orderSummaryText";
 
 /** The Constant EST_TOTAL_TEXT. */
 private static final String EST_TOTAL_TEXT = "estTotalText";
 
 /** The Constant BLANK_TEXT. */
 private static final String BLANK_TEXT = "blankText";
 
 /** The Constant QUANTITY_SHORT_TEXT. */
 private static final String QUANTITY_SHORT_TEXT = "quantityShortText";
 
 /** The Constant EDIT_TEXT. */
 private static final String EDIT_TEXT = "editText";
 
 /** The Constant ITEMS_TEXT. */
 private static final String ITEMS_TEXT = "itemsText";
 
 /** The Constant SAVE_TEXT. */
 private static final String SAVE_TEXT = "saveText";
 
 /** The Constant UPDATE_TEXT. */
 private static final String UPDATE_TEXT = "updateText";
 
 /** The Constant CANCEL_TEXT. */
 private static final String CANCEL_TEXT = "cancelText";
 
 /** The Constant NO_PRODUCT. */
 private static final String NO_PRODUCT = "noProduct";
 
 /** The Constant NO_PRODUCT_TEXT. */
 private static final String NO_PRODUCT_TEXT = "noProductText";
 
 /** The Constant SHOPPING_ICON_TITLE. */
 private static final String SHOPPING_ICON_TITLE = "shoppingIconTitle";
 
 /** The Constant PRODUCT_NAME_TEXT. */
 private static final String PRODUCT_NAME_TEXT = "productNameText";
 
 /** The Constant SHOPPABLE. */
 private static final String SHOPPABLE = "shoppable";
 
 /** The Constant VIEW_BAG_LINK. */
 public static final String VIEW_BAG_LINK = "viewBagLink";
 
 /** The Constant PUBLISHER_CHECKOUT_URL. */
 public static final String PUBLISHER_CHECKOUT_URL = "publisherCheckoutUrl";
 
 /** The Constant ORDER_COMPLETE_URL. */
 public static final String ORDER_COMPLETE_URL = "orderCompleteUrl";
 
 /** The Constant ORDER_COMPLETE_RETURN_TO_SITE_URL. */
 public static final String ORDER_COMPLETE_RETURN_TO_SITE_URL = "orderCompletereturnToSiteUrl";
 
 /** The Constant CONTINUE_SHOPPING_URL. */
 public static final String CONTINUE_SHOPPING_URL = "continueShoppingUrl";
 
 /** The Constant HEADING_TEXT. */
 private static final String HEADING_TEXT = "headingText";
 
 /** The Constant EDIT_CTA_LABEL. */
 private static final String EDIT_CTA_LABEL = "editCtaLabel";
 
 /** The Constant REMOVE_CTA_LABEL. */
 private static final String REMOVE_CTA_LABEL = "removeCtaLabel";
 
 /** The Constant CURRENCY_PREFIX_SYMBOL. */
 private static final String CURRENCY_PREFIX_SYMBOL = "currencyPrefixSymbol";
 
 /** The Constant SUBTOTAL_LABEL. */
 private static final String SUBTOTAL_LABEL = "subtotalLabel";
 
 /** The Constant SHIPPING_AND_TAX_LABEL. */
 private static final String SHIPPING_AND_TAX_LABEL = "shippingAndTaxLabel";
 
 /** The Constant CONTINUE_SHOPPING_CTA_LABEL. */
 private static final String CONTINUE_SHOPPING_CTA_LABEL = "continueShoppingCtaLabel";
 
 /** The Constant CHECKOUT_CTA_LABEL. */
 private static final String CHECKOUT_CTA_LABEL = "checkoutCtaLabel ";
 
 /** The Constant RETAILER. */
 private static final String RETAILER = "retailer";
 
 /** The Constant PRODUCT. */
 private static final String PRODUCT = "product";
 
 /** The Constant SIZE. */
 private static final String SIZE = "size";
 
 /** The Constant QUANTITY. */
 private static final String QUANTITY = "quantity";
 
 /** The Constant PRICE. */
 private static final String PRICE = "price";
 
 /** The Constant TOTAL_PRICE. */
 private static final String TOTAL_PRICE = "totalPrice";
 
 /** The Constant RETAILER_LABEL. */
 private static final String RETAILER_LABEL = "retailerLabel";
 
 /** The Constant PACK_SIZE_LABEL. */
 private static final String PACK_SIZE_LABEL = "packSizeLabel";
 
 /** The Constant EDIT_QUANTITY_LABEL. */
 private static final String EDIT_QUANTITY_LABEL = "quantityLabel";
 
 /** The Constant UNIT_PRICE_LABEL. */
 private static final String UNIT_PRICE_LABEL = "unitPriceLabel";
 
 /** The Constant TOTAL_PRICE_LABEL. */
 private static final String TOTAL_PRICE_LABEL = "totalPriceLabel";
 
 /** The Constant UPDATE_CTA_LABEL. */
 private static final String UPDATE_CTA_LABEL = "updateCtaLabel";
 
 /** The Constant CANCEL_CTA_LABEL. */
 private static final String CANCEL_CTA_LABEL = "cancelCtaLabel";
 
 /** The Constant COLUMN_HEADER. */
 private static final String COLUMN_HEADER = "columnHeader";
 
 /** The Constant EDIT_ITEM. */
 private static final String EDIT_ITEM = "editItem";
 
 /** The Constant REMOVE_ITEM. */
 private static final String REMOVE_ITEM = "removeItem";
 
 /** The Constant SHOPPING_BAG. */
 private static final String SHOPPING_BAG = "shoppingBag";
 
 /** The Constant ITEMS_ADDED_TO_BAG_RESPONSE_TEXT. */
 private static final String ITEMS_ADDED_TO_BAG_RESPONSE_TEXT = "itemAddedToBagResponseText";
 
 /** The Constant VIEW_SHOPPING_BAG_CTA_LABEL. */
 private static final String VIEW_SHOPPING_BAG_CTA_LABEL = "viewShoppingBagCtaLabel";
 
 /** The Constant EMPTY_BAG_MESSAGE_LINE_1. */
 private static final String EMPTY_BAG_MESSAGE_LINE_1 = "emptyBagMessageLine1";
 
 /** The Constant EMPTY_BAG_MESSAGE_LINE_2. */
 private static final String EMPTY_BAG_MESSAGE_LINE_2 = "emptyBagMessageLine2";
 
 /** The Constant MINI_SHOPPING_BAG. */
 private static final String MINI_SHOPPING_BAG = "miniShoppingBag";
 
 /**
  * Instantiates a new e Shoppable helper.
  */
 
 private ShoppableHelper() {
  
 }
 
 /**
  * Gets the order summary map.
  * 
  * @param resources
  *         the resources
  * @return the order summary map
  */
 
 public static Map<String, Object> getBagSummaryMap(Map<String, Object> resources) {
  LOGGER.info("Order Summary View Helper #processData called for processing fixed list.");
  Map<String, Object> orderSummary = new LinkedHashMap<String, Object>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  Map<String, Object> shoppingBagMap = new LinkedHashMap<String, Object>();
  Map<String, Object> columnHeaderMap = new LinkedHashMap<String, Object>();
  Map<String, Object> editItemMap = new LinkedHashMap<String, Object>();
  Map<String, Object> removeItemMap = new LinkedHashMap<String, Object>();
  Map<String, Object> miniShoppingBagMap = new LinkedHashMap<String, Object>();
  
  I18n i18n = BaseViewHelper.getI18n(resources);
  
  orderSummary.put(RETAILER_TEXT, i18n.get(PREFIX + RETAILER_TEXT));
  orderSummary.put(PRODUCT_TEXT, i18n.get(PREFIX + PRODUCT_TEXT));
  orderSummary.put(SIZE_TEXT, i18n.get(PREFIX + SIZE_TEXT));
  orderSummary.put(QUANTITY_LABEL, i18n.get(PREFIX + QUANTITY_LABEL));
  orderSummary.put(PRICE_LABEL, i18n.get(PREFIX + PRICE_LABEL));
  orderSummary.put(TOTAL_LABEL, i18n.get(PREFIX + TOTAL_LABEL));
  orderSummary.put(SUB_TOTAL_TEXT, i18n.get(PREFIX + SUB_TOTAL_TEXT));
  orderSummary.put(REMOVE_TEXT, i18n.get(PREFIX + REMOVE_TEXT));
  orderSummary.put(EXIT_LABEL, i18n.get(PREFIX + EXIT_LABEL));
  orderSummary.put(SHIPPING_AND_TAX_TEXT, i18n.get(PREFIX + SHIPPING_AND_TAX_TEXT));
  orderSummary.put(CONTINUE_SHOPPING_TEXT, i18n.get(PREFIX + CONTINUE_SHOPPING_TEXT));
  orderSummary.put(CHECK_OUT_TEXT, i18n.get(PREFIX + CHECK_OUT_TEXT));
  orderSummary.put(VIEW_BAG_TEXT, i18n.get(PREFIX + VIEW_BAG_TEXT));
  orderSummary.put(ORDER_SUMMARY_TEXT, i18n.get(PREFIX + ORDER_SUMMARY_TEXT));
  orderSummary.put(EST_TOTAL_TEXT, i18n.get(PREFIX + EST_TOTAL_TEXT));
  orderSummary.put(BLANK_TEXT, i18n.get(PREFIX + SHOPPING_CART_POP_HEADING));
  orderSummary.put(QUANTITY_SHORT_TEXT, i18n.get(PREFIX + QUANTITY_SHORT_TEXT));
  orderSummary.put(ITEMS_TEXT, i18n.get(PREFIX + ITEMS_TEXT));
  orderSummary.put(SAVE_TEXT, i18n.get(PREFIX + SAVE_TEXT));
  orderSummary.put(UPDATE_TEXT, i18n.get(PREFIX + UPDATE_TEXT));
  orderSummary.put(SHOPPING_CART_POP_HEADING, i18n.get(PREFIX + SHOPPING_CART_POP_HEADING));
  orderSummary.put(CANCEL_TEXT, i18n.get(PREFIX + CANCEL_TEXT));
  orderSummary.put(NO_PRODUCT_TEXT, i18n.get(PREFIX + NO_PRODUCT));
  orderSummary.put(SHOPPING_ICON_TITLE, i18n.get(PREFIX + SHOPPING_ICON_TITLE));
  orderSummary.put(EDIT_TEXT, i18n.get(PREFIX + EDIT_TEXT));
  orderSummary.put(PRODUCT_NAME_TEXT, i18n.get(PREFIX + PRODUCT_NAME_TEXT));
  
  shoppingBagMap.put(HEADING_TEXT, i18n.get(PREFIX_SHOPPING_BAG + HEADING_TEXT));
  shoppingBagMap.put(EDIT_CTA_LABEL, i18n.get(PREFIX_SHOPPING_BAG + EDIT_CTA_LABEL));
  shoppingBagMap.put(REMOVE_CTA_LABEL, i18n.get(PREFIX_SHOPPING_BAG + REMOVE_CTA_LABEL));
  shoppingBagMap.put(CURRENCY_PREFIX_SYMBOL, i18n.get(PREFIX_SHOPPING_BAG + CURRENCY_PREFIX_SYMBOL));
  shoppingBagMap.put(SUBTOTAL_LABEL, i18n.get(PREFIX_SHOPPING_BAG + SUBTOTAL_LABEL));
  shoppingBagMap.put(SHIPPING_AND_TAX_LABEL, i18n.get(PREFIX_SHOPPING_BAG + SHIPPING_AND_TAX_LABEL));
  shoppingBagMap.put(CONTINUE_SHOPPING_CTA_LABEL, i18n.get(PREFIX_SHOPPING_BAG + CONTINUE_SHOPPING_CTA_LABEL));
  shoppingBagMap.put(CHECKOUT_CTA_LABEL, i18n.get(PREFIX_SHOPPING_BAG + CHECKOUT_CTA_LABEL));
  
  columnHeaderMap.put(RETAILER, i18n.get(PREFIX_SHOPPING_BAG + PREFIX_COLUMN_HEADER + RETAILER));
  columnHeaderMap.put(PRODUCT, i18n.get(PREFIX_SHOPPING_BAG + PREFIX_COLUMN_HEADER + PRODUCT));
  columnHeaderMap.put(SIZE, i18n.get(PREFIX_SHOPPING_BAG + PREFIX_COLUMN_HEADER + SIZE));
  columnHeaderMap.put(QUANTITY, i18n.get(PREFIX_SHOPPING_BAG + PREFIX_COLUMN_HEADER + QUANTITY));
  columnHeaderMap.put(PRICE, i18n.get(PREFIX_SHOPPING_BAG + PREFIX_COLUMN_HEADER + PRICE));
  columnHeaderMap.put(TOTAL_PRICE, i18n.get(PREFIX_SHOPPING_BAG + PREFIX_COLUMN_HEADER + TOTAL_PRICE));
  
  editItemMap.put(RETAILER_LABEL, i18n.get(PREFIX_SHOPPING_BAG + PREFIX_EDIT_ITEM + RETAILER_LABEL));
  editItemMap.put(PACK_SIZE_LABEL, i18n.get(PREFIX_SHOPPING_BAG + PREFIX_EDIT_ITEM + PACK_SIZE_LABEL));
  editItemMap.put(EDIT_QUANTITY_LABEL, i18n.get(PREFIX_SHOPPING_BAG + PREFIX_EDIT_ITEM + EDIT_QUANTITY_LABEL));
  editItemMap.put(UNIT_PRICE_LABEL, i18n.get(PREFIX_SHOPPING_BAG + PREFIX_EDIT_ITEM + UNIT_PRICE_LABEL));
  editItemMap.put(TOTAL_PRICE_LABEL, i18n.get(PREFIX_SHOPPING_BAG + PREFIX_EDIT_ITEM + TOTAL_PRICE_LABEL));
  editItemMap.put(UPDATE_CTA_LABEL, i18n.get(PREFIX_SHOPPING_BAG + PREFIX_EDIT_ITEM + UPDATE_CTA_LABEL));
  editItemMap.put(CANCEL_CTA_LABEL, i18n.get(PREFIX_SHOPPING_BAG + PREFIX_EDIT_ITEM + CANCEL_CTA_LABEL));
  editItemMap.put(REMOVE_THIS_PRODUCT_LABEL, i18n.get(PREFIX_SHOPPING_BAG + PREFIX_EDIT_ITEM + REMOVE_THIS_PRODUCT_LABEL));
  editItemMap.put(YES_REMOVE_THIS_ITEM_LABEL, i18n.get(PREFIX_SHOPPING_BAG + PREFIX_EDIT_ITEM + YES_REMOVE_THIS_ITEM_LABEL));
  
  removeItemMap.put(HEADING_TEXT, i18n.get(PREFIX_SHOPPING_BAG + PREFIX_REMOVE_ITEM + HEADING_TEXT));
  removeItemMap.put(REMOVE_CTA_LABEL, i18n.get(PREFIX_SHOPPING_BAG + PREFIX_REMOVE_ITEM + REMOVE_CTA_LABEL));
  removeItemMap.put(CANCEL_CTA_LABEL, i18n.get(PREFIX_SHOPPING_BAG + PREFIX_REMOVE_ITEM + CANCEL_CTA_LABEL));
  
  miniShoppingBagMap.put(HEADING_TEXT, i18n.get(PREFIX_MINI_SHOPPING_BAG + HEADING_TEXT));
  miniShoppingBagMap.put(REMOVE_CTA_LABEL, i18n.get(PREFIX_MINI_SHOPPING_BAG + REMOVE_CTA_LABEL));
  miniShoppingBagMap.put(CURRENCY_PREFIX_SYMBOL, i18n.get(PREFIX_MINI_SHOPPING_BAG + CURRENCY_PREFIX_SYMBOL));
  miniShoppingBagMap.put(EDIT_QUANTITY_LABEL, i18n.get(PREFIX_MINI_SHOPPING_BAG + EDIT_QUANTITY_LABEL));
  miniShoppingBagMap.put(ITEMS_ADDED_TO_BAG_RESPONSE_TEXT, i18n.get(PREFIX_MINI_SHOPPING_BAG + ITEMS_ADDED_TO_BAG_RESPONSE_TEXT));
  miniShoppingBagMap.put(SUBTOTAL_LABEL, i18n.get(PREFIX_MINI_SHOPPING_BAG + SUBTOTAL_LABEL));
  miniShoppingBagMap.put(SHIPPING_AND_TAX_LABEL, i18n.get(PREFIX_MINI_SHOPPING_BAG + SHIPPING_AND_TAX_LABEL));
  miniShoppingBagMap.put(VIEW_SHOPPING_BAG_CTA_LABEL, i18n.get(PREFIX_MINI_SHOPPING_BAG + VIEW_SHOPPING_BAG_CTA_LABEL));
  miniShoppingBagMap.put(EMPTY_BAG_MESSAGE_LINE_1, i18n.get(PREFIX_MINI_SHOPPING_BAG + EMPTY_BAG_MESSAGE_LINE_1));
  miniShoppingBagMap.put(EMPTY_BAG_MESSAGE_LINE_2, i18n.get(PREFIX_MINI_SHOPPING_BAG + EMPTY_BAG_MESSAGE_LINE_2));
  
  shoppingBagMap.put(COLUMN_HEADER, columnHeaderMap);
  shoppingBagMap.put(EDIT_ITEM, editItemMap);
  shoppingBagMap.put(REMOVE_ITEM, removeItemMap);
  
  orderSummary.put(SHOPPING_BAG, shoppingBagMap);
  orderSummary.put(MINI_SHOPPING_BAG, miniShoppingBagMap);
  
  return orderSummary;
 }
 
 /**
  * Gets the shoppable config.
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param configurationService
  *         the configuration service
  * @param page
  *         the page
  * @param slingRequest
  *         the slingRequest
  * @return the shoppable config
  */
 public static Map<String, String> getShoppableConfig(ResourceResolver resourceResolver, ConfigurationService configurationService, Page page,
   SlingHttpServletRequest slingRequest) {
  LOGGER.info("Order Summary View Helper #processData called for processing fixed list.");
  Map<String, String> shoppableConfigMap = new HashMap<String, String>();
  String viewBagLink = StringUtils.EMPTY;
  String publisherCheckoutUrl = StringUtils.EMPTY;
  String orderCompletereturnToSiteUrl = StringUtils.EMPTY;
  String orderCompleteUrl = StringUtils.EMPTY;
  String continueShoppingUrl = StringUtils.EMPTY;
  try {
   shoppableConfigMap = configurationService.getCategoryConfiguration(page, SHOPPABLE);
   
  } catch (ConfigurationNotFoundException e) {
   LOGGER.debug("Could not find category for Shoppable or shopNow", e);
  }
  publisherCheckoutUrl = ComponentUtil.getFullURL(resourceResolver, shoppableConfigMap.get(PUBLISHER_CHECKOUT_URL), slingRequest);
  viewBagLink = ComponentUtil.getFullURL(resourceResolver, shoppableConfigMap.get(VIEW_BAG_LINK), slingRequest);
  orderCompletereturnToSiteUrl = ComponentUtil.getFullURL(resourceResolver, shoppableConfigMap.get(ORDER_COMPLETE_RETURN_TO_SITE_URL), slingRequest);
  orderCompleteUrl = ComponentUtil.getFullURL(resourceResolver, shoppableConfigMap.get(ORDER_COMPLETE_URL), slingRequest);
  continueShoppingUrl = ComponentUtil.getFullURL(resourceResolver, shoppableConfigMap.get(CONTINUE_SHOPPING_URL), slingRequest);
  shoppableConfigMap.put(PUBLISHER_CHECKOUT_URL, publisherCheckoutUrl);
  shoppableConfigMap.put(VIEW_BAG_LINK, viewBagLink);
  shoppableConfigMap.put(ORDER_COMPLETE_RETURN_TO_SITE_URL, orderCompletereturnToSiteUrl);
  shoppableConfigMap.put(ORDER_COMPLETE_URL, orderCompleteUrl);
  shoppableConfigMap.put(CONTINUE_SHOPPING_URL, continueShoppingUrl);
  return shoppableConfigMap;
 }
 
}
