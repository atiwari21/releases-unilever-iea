/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.sling.api.SlingConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.template.dto.Image;

/**
 * This class prepared the data map for Adaptive Image V2 component.
 * 
 */
@Component(description = "AdaptiveImage", immediate = true, metatype = true, label = "AdaptiveImageViewHelperImpl")
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.ADAPTIVE_IMAGE_V2, propertyPrivate = true), })
public class AdaptiveImageV2ViewHelperImpl extends BaseViewHelper {
 
 /** The Constant ALT_TEXT. */
 private static final String ALT_TEXT = "alt";
 
 /** The Constant LINK_URL. */
 private static final String LINK_URL = "linkURL";
 
 /** The Constant WINDOW_TYPE. */
 private static final String WINDOW_TYPE = "windowType";
 
 /** The Constant IS_NOT_ADAPTIVE. */
 private static final String IS_NOT_ADAPTIVE = "isNotAdaptive";
 
 /** The Constant FILE_REFERENCE. */
 private static final String FILE_REFERENCE = "fileReference";
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(AdaptiveImageV2ViewHelperImpl.class);
 
 /** The Constant IMAGE. */
 private static final String IMAGE = "image";
 
 /**
  * This is overridden method from base view helper which prepares data map by calling all required methods.
  * 
  * @param content
  *         the content
  * @param resources
  *         the resources
  * @return the map
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("Executing AdaptiveImageViewHelperImpl.java. Inside processData method");
  
  Map<String, Object> adaptiveImageMap = new LinkedHashMap<String, Object>();
  Map<String, Object> properties = getProperties(content);
  I18n i18n = getI18n(resources);
  
  Page currentPage = getCurrentPage(resources);
  String jsonNameSpace = getJsonNameSpace(content);
  String title = StringUtils.EMPTY;
  String fileReference = MapUtils.getString(properties, FILE_REFERENCE, StringUtils.EMPTY);
  String altText = MapUtils.getString(properties, ALT_TEXT, StringUtils.EMPTY);
  
  Image image = new Image(fileReference, altText, title, currentPage, getSlingRequest(resources));
  image.setNotAdaptiveImage(MapUtils.getBooleanValue(properties, IS_NOT_ADAPTIVE, false));
  adaptiveImageMap.put(IMAGE, image.convertToMap());
  
  String linkUrl = MapUtils.getString(properties, LINK_URL, StringUtils.EMPTY);
  adaptiveImageMap.put(LINK_URL, linkUrl);
  String completeUrl = linkUrl;
  if (StringUtils.isNotBlank(linkUrl)) {
   completeUrl = ComponentUtil.getFullURL(getResourceResolver(resources), linkUrl, getSlingRequest(resources));
  }
  adaptiveImageMap.put("completeUrl", completeUrl);
  adaptiveImageMap.put(WINDOW_TYPE, MapUtils.getString(properties, WINDOW_TYPE));
  
  Map<String, String> configMap = GlobalConfigurationUtility.getMapFromConfiguration(configurationService, currentPage, "adaptiveImageV2");
  boolean enableZoom = MapUtils.getBooleanValue(configMap, "enableZoom", false);
  boolean overrideGlobalConfig = MapUtils.getBooleanValue(properties, "overrideGlobalConfig", false);
  
  if (overrideGlobalConfig) {
   enableZoom = MapUtils.getBooleanValue(properties, "enableZoomImage", false);
  }
  
  String zoomLabel = i18n.get("adaptiveImage.zoomCtaLabel");
  
  adaptiveImageMap.put("enableZoom", enableZoom);
  adaptiveImageMap.put("zoomLabel", zoomLabel);
  
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  data.put(jsonNameSpace, adaptiveImageMap);
  
  return data;
 }
}
