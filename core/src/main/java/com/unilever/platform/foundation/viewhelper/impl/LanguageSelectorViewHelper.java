/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.LanguageSelectorUtility;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * <p>
 * The goal of this component is to display global footer on all pages. The component will be configured by editing sections in the dialog at root
 * level page once and then will get displayed on all children pages. Quote copy text, Brand logo image,Social sharing links,Footer links, Local
 * market identification,Copyright text, Unilever logo image are the properties which will be displayed in this component.
 * 
 * </p>
 */
@Component(description = "LanguageSelectorViewHelper Impl", immediate = true, metatype = true, label = "LanguageSelectorViewHelper")
@Service(value = { LanguageSelectorViewHelper.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.LANG_SELECTOR, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class LanguageSelectorViewHelper extends BaseViewHelper {
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /**
  * logger object.
  **/
 private static final Logger LOGGER = LoggerFactory.getLogger(LanguageSelectorViewHelper.class);
 
 /**
  * The resource resolver.
  * 
  * @param content
  *         the content
  * @param resources
  *         the resources
  * @return the map
  */
 
 /**
  * The method processData overrides method is responsible validating author input and returning a list of sections containing rich text,text This
  * method converts the multiwidget input (a String Array) into sections of maps so that it can be accessed in JSP in consistent way.This method also
  * validates the link url provided by author.
  * 
  * @param content
  *         , - {@link Map} of all user input received as input <code>ViewHelper.onGetData</code> method
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return {@link Map} holding the data
  */
 @Override
 @SuppressWarnings("unchecked")
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  
  LOGGER.debug("language selector View Helper #processData called for processing fixed list.");
  
  LOGGER.info("The Footer Map is successfully generated");
  Map<String, Object> data = new HashMap<String, Object>();
  data
    .put(getJsonNameSpace(content), LanguageSelectorUtility.getLanguages(getCurrentPage(resources), getSlingRequest(resources),
      getResourceResolver(resources), configurationService));
  
  return data;
 }
}
