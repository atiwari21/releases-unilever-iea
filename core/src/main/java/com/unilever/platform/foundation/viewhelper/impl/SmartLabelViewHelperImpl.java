/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.api.Product;
import com.adobe.cq.commerce.common.CommerceHelper;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * This class prepares the data map for SmartLabel.
 */

/**
 * @author mkathu
 * 
 */
@Component(description = "Smart Label view helper", immediate = true, metatype = true, label = "Smart Label ViewHelper")
@Service(value = { SmartLabelViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.SMART_LABEL, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class SmartLabelViewHelperImpl extends ProductCopyViewHelperImpl {
 
 @Reference
 ConfigurationService configurationService;
 
 /** used as a key in json. */
 private static final String ENABLED = "enabled";
 
 /** used as a key in json. */
 private static final String BUTTON_TEXT = "smartLabel.ButtonText";
 
 /** used as a key in json. */
 private static final String LOGO = "logo";
 /** used as a key in json. */
 private static final String BASE_URL = "baseUrl";
 /** used as a key in json. */
 private static final String MORE_INFOLABEL = "moreInfoLabel";
 /** used as a key in json. */
 private static final String PRODUCT_INFO = "productInfo";
 /** config category */
 private static final String CONFIG_CATEGORY = "smartlabel";
 /** used as a key in json */
 private static final String BUTTON_LABEL = "buttonLabel";
 /** used as a key in json */
 private static final String OPEN_IN_NEW_WINDOW = "openInNewWindow";
 /** used as a key in json */
 private static final String LOCALE = "locale";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(SmartLabelViewHelperImpl.class);
 /** used as a local */
 private static final String IS_PRODUCT = "isProduct";
 
 private static final Object DYNAMIC_URL = "dynamicURL";
 
 private static final Object DYNAMIC_URL_ENABLED = "dynamicURLEnabled";
 
 /*
  * responsible for reading the values from the dialog and creating a json.
  * 
  * *
  */
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData (java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  Boolean isProduct = false;
  LOGGER.debug("executing smart label. Inside processData method");
  Map<String, Object> properties = new HashMap<String, Object>();
  Map<String, Object> dynamicURLMap = new LinkedHashMap<String, Object>();
  Map<String, Object> smartLabelProperties = getProperties(content);
  ResourceResolver resourceResolver = (ResourceResolver) resources.get(ProductConstants.RESOURCE_RESOLVER);
  SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) resources.get(ProductConstants.REQUEST);
  Resource currentResource = slingRequest.getResource();
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  Page productPage = pageManager.getContainingPage(currentResource);
  Map<String, String> smartLabelconfigMap = ComponentUtil.getConfigMap(productPage, CONFIG_CATEGORY);
  Product currentProduct = CommerceHelper.findCurrentProduct(productPage);
  UNIProduct uniProduct = ProductHelper.getUniProduct(productPage);
  properties.put(BASE_URL, smartLabelconfigMap.get(BASE_URL));
  properties.put(LOGO, smartLabelconfigMap.get(LOGO));
  properties.put(LOCALE, smartLabelconfigMap.get(LOCALE));
  Boolean enabled = Boolean.parseBoolean(smartLabelconfigMap.get(ENABLED));
  properties.put(ENABLED, enabled);
  properties.put(BUTTON_LABEL, getI18n(resources).get(BUTTON_TEXT));
  properties.put(UnileverConstants.TITLE, MapUtils.getString(smartLabelProperties, UnileverConstants.TITLE, StringUtils.EMPTY));
  properties.put(MORE_INFOLABEL, MapUtils.getString(smartLabelProperties, MORE_INFOLABEL, StringUtils.EMPTY));
  Boolean openWindow = Boolean.parseBoolean(smartLabelconfigMap.get(OPEN_IN_NEW_WINDOW));
  properties.put(OPEN_IN_NEW_WINDOW, openWindow);
  properties.put(PRODUCT_INFO, getProductCopyData(content, resources));
  
  dynamicURLMap.put("enabled", MapUtils.getBoolean(smartLabelconfigMap, DYNAMIC_URL_ENABLED, false));
  dynamicURLMap.put("apiURL", MapUtils.getString(smartLabelconfigMap, DYNAMIC_URL, StringUtils.EMPTY));
  properties.put("dynamicURL", dynamicURLMap);
  
  if (currentProduct != null) {
   isProduct = true;
   Map<String, Object> productData = ProductHelper.getProductMap(uniProduct, "smartlabel", resources, productPage);
   properties.put(ProductConstants.PRODUCT_MAP, productData);
   
  }
  properties.put(IS_PRODUCT, isProduct);
  LOGGER.info("Fetched the result from the component.");
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE), properties);
  return data;
  
 }
 
}
