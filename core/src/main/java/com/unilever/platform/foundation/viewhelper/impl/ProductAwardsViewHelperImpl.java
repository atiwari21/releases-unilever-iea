/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.api.Product;
import com.adobe.cq.commerce.common.CommerceHelper;
import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.utils.AdaptiveUtil;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.AwardHelper;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.components.helper.ProductHelperExt;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * The Class ProductAwardsViewHelperImpl.
 */
@Component(description = "ProductAwardsViewHelperImpl", immediate = true, metatype = true, label = "ProductAwardsViewHelperImpl")
@Service(value = { ProductAwardsViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.PRODUCT_AWARDS, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class ProductAwardsViewHelperImpl extends BaseViewHelper {
 
 /** Constant for product copy title. */
 private static final String PRODUCT_AWARDS_TITLE = "productAwardsTitle";
 
 /** The Constant PRODUCT_AWARDS_SUB_TITLE. */
 private static final String PRODUCT_AWARDS_SUB_TITLE = "productAwardsSubTitle";
 
 /** The Constant PRODUCT_AWARDS_CTA_LABEL. */
 private static final String PRODUCT_AWARDS_CTA_LABEL = "productAwardsCtaLabel";
 
 /** The Constant PRODUCT_AWARDS_CTA_URL. */
 private static final String PRODUCT_AWARDS_CTA_URL = "productAwardsCtaUrl";
 /** Constant for product copy description. */
 private static final String PRODUCT_AWARDS_TEXT = "productAwardsText";
 /** Constant for heading label. */
 private static final String PRODUCT_AWARDS_HEADING = "headline";
 
 /** The Constant PRODUCT_AWARDS_SUB_HEADING. */
 private static final String PRODUCT_AWARDS_SUB_HEADING = "subHeadline";
 
 /** The Constant PRODUCT_AWARDS_CTALABEL. */
 private static final String PRODUCT_AWARDS_CTALABEL = "ctaLabel";
 
 /** The Constant PRODUCT_AWARDS_CTAURL. */
 private static final String PRODUCT_AWARDS_CTAURL = "ctaUrl";
 
 /** Constant forgetting adaptive attribute topLink. */
 private static final String TOP_LINK_I18N = "productAwards.topLink";
 
 /** Constant for description label. */
 private static final String PRODUCT_AWARDS_DESCRIPTION = "description";
 
 /** The Constant OPEN_IN_NEW_WINDOW. */
 public static final String OPEN_IN_NEW_WINDOW = "openInNewWindow";
 
 /** Constant for description label. */
 private static final String TOP_LINK = "topLink";
 
 /** The Constant AWARDS. */
 private static final String AWARDS = "awards";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ProductAwardsViewHelperImpl.class);
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  
  LOGGER.info("Product Awards View Helper #processData called");
  
  Map<String, Object> properties = getProperties(content);
  ResourceResolver resourceResolver = getResourceResolver(resources);
  Page currentPage = getCurrentPage(resources);
  
  SlingHttpServletRequest slingRequest = getSlingRequest(resources);
  String jsonNameSpace = getJsonNameSpace(content);
  I18n i18n = getI18n(resources);
  
  Resource currentResource = slingRequest.getResource();
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  
  boolean adaptiveNature = AdaptiveUtil.isAdaptive(slingRequest);
  String adaptiveTopLInk = i18n.get(TOP_LINK_I18N);
  
  String productAwardsTitle = MapUtils.getString(properties, PRODUCT_AWARDS_TITLE);
  String productAwardsSubTitle = MapUtils.getString(properties, PRODUCT_AWARDS_SUB_TITLE);
  String productAwardsCtaLabel = MapUtils.getString(properties, PRODUCT_AWARDS_CTA_LABEL);
  String productAwardsCtaUrl = MapUtils.getString(properties, PRODUCT_AWARDS_CTA_URL);
  String productAwardsDescription = MapUtils.getString(properties, PRODUCT_AWARDS_TEXT);
  
  String newWindow = MapUtils.getString(properties, OPEN_IN_NEW_WINDOW, StringUtils.EMPTY);
  
  String openInNewWindow = newWindow != null && "true".equals(newWindow) ? com.unilever.platform.foundation.constants.CommonConstants.BOOLEAN_TRUE
    : ProductConstants.FALSE;
  
  Map<String, Object> productAwardMap = new LinkedHashMap<String, Object>();
  
  productAwardMap.put(PRODUCT_AWARDS_HEADING, productAwardsTitle);
  productAwardMap.put(PRODUCT_AWARDS_SUB_HEADING, productAwardsSubTitle);
  productAwardMap.put(PRODUCT_AWARDS_CTALABEL, productAwardsCtaLabel);
  productAwardMap.put(PRODUCT_AWARDS_CTAURL, productAwardsCtaUrl);
  productAwardMap.put(OPEN_IN_NEW_WINDOW, Boolean.valueOf(openInNewWindow));
  
  productAwardMap.put(PRODUCT_AWARDS_DESCRIPTION, productAwardsDescription);
  
  if (adaptiveNature && StringUtils.isNotBlank(adaptiveTopLInk)) {
   productAwardMap.put(TOP_LINK, adaptiveTopLInk);
  }
  
  Product currentProduct = CommerceHelper.findCurrentProduct(pageManager.getContainingPage(currentResource));
  if (currentProduct == null) {
   currentProduct = getProductFromSelector(pageManager.getContainingPage(currentResource), slingRequest);
   
  }
  // Case handled if the product award component is used on some non-pdp page, and the pdp url is provided as CTA url.
  if (currentProduct == null && StringUtils.isNotBlank(productAwardsCtaUrl)) {
   currentProduct = ProductHelper.getUniProduct(pageManager.getPage(productAwardsCtaUrl));
  }
  if (currentProduct != null) {
   productAwardMap.put(AWARDS, AwardHelper.getProductAwards((UNIProduct) currentProduct, currentPage, configurationService, resources).toArray());
  }
  
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  data.put(jsonNameSpace, productAwardMap);
  
  return data;
 }
 
 /**
  * Gets the product from selector.
  * 
  * @param page
  *         the page
  * @param slingRequest
  *         the sling request
  * @return {@link Product}
  */
 private Product getProductFromSelector(Page page, SlingHttpServletRequest slingRequest) {
  String productId = slingRequest.getParameter(ProductConstants.EAN) != null ? slingRequest.getParameter(ProductConstants.EAN) : "";
  return ProductHelperExt.findProduct(page, productId);
  
 }
}
