/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.tagging.Tag;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.cache.ComponentViewAndViewHelperCache;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.constants.SearchConstants;
import com.unilever.platform.aem.foundation.utils.AdaptiveUtil;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.constants.CommonConstants;
import com.unilever.platform.foundation.constants.ContainerTagConstants;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;
import com.unilever.platform.foundation.viewhelper.constants.ProductTagsConstant;

/**
 * <p>
 * Responsible for reading author input for product tag component and get the get all the tags on the page and generate a list of links. Also read the
 * site configuration for minimum and maximum number configured for the links.
 * </p>
 */
@Component(description = "ProductTagsViewHelperImpl", immediate = true, metatype = true, label = "ProductTagsViewHelperImpl")
@Service(value = { ProductTagsViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.PRODUCT_TAG, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class ProductTagsViewHelperImpl extends BaseViewHelper {
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ProductTagsViewHelperImpl.class);
 
 private static final int NUMERIC_TWO = 2;
 private static final int NUMERIC_THREE = 3;
 private static final String SLASH_CHAR_LITERAL = "/";
 private static final String URL_BRAND = "&BrandName=";
 private static final String URL_LOCALE = "&Locale=";
 /** The component view and view helper cache. */
 /*
  * reference for cache object
  */
 @Reference
 ComponentViewAndViewHelperCache componentViewAndViewHelperCache;
 /*
  * reference for site configuration service
  */
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /**
  * The method processData overrides method is responsible validating author input and returning a list of links and minimum and maximum number for
  * the links from site configuration to be displayed on the page.
  * 
  * @param content
  *         {@link Map} of all user input received as input
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return Map holding the result data.
  */
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  LOGGER.debug("inside ProductTagsViewHelperImpl processData");
  Map<String, Object> data = new HashMap<String, Object>();
  try {
   ResourceResolver resourceResolver = (ResourceResolver) resources.get(ProductConstants.RESOURCE_RESOLVER);
   SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) resources.get(ProductConstants.REQUEST);
   Resource compResource = slingRequest.getResource();
   PageManager pm = resourceResolver.adaptTo(PageManager.class);
   Page currentPage = pm.getContainingPage(compResource);
   Map<String, String> productTagSiteConfigs = ComponentUtil.getConfigMap(currentPage, ProductTagsConstant.SITE_CONFIG_CATEGORY);
   LOGGER.debug("Config Map for producttag = " + productTagSiteConfigs);
   
   Map<String, Object> properties = compResource.getValueMap();
   LOGGER.debug("properties for producttag = " + properties);
   String jsonNameSpace = MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE);
   String searchPagePath = getSearchPagePath(resourceResolver, currentPage, slingRequest);
   LOGGER.debug("searchPagePath for producttag = " + searchPagePath);
   Map<String, Object> productTagsMap = generateProductTagsMap(currentPage, productTagSiteConfigs, properties, searchPagePath, slingRequest);
   data.put(jsonNameSpace, productTagsMap);
  } catch (Exception e) {
   LOGGER.error("Error At ProductTagsViewHelperImpl " + ExceptionUtils.getStackTrace(e));
  }
  return data;
 }
 
 /**
  * Gets the search page path.
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param currentPage
  *         the current page
  * @return the search page path
  */
 private String getSearchPagePath(ResourceResolver resourceResolver, Page currentPage, SlingHttpServletRequest slingRequest) {
  String searchPagePath = StringUtils.EMPTY;
  try {
   searchPagePath = configurationService
     .getConfigValue(currentPage, ProductTagsConstant.RELATIVE_URL_CONFIG_CAT, ProductTagsConstant.SEARCH_PAGE_KEY);
   searchPagePath = ComponentUtil.getFullURL(resourceResolver, searchPagePath, slingRequest);
   
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Configuration Not found for Category: " + ProductTagsConstant.RELATIVE_URL_CONFIG_CAT + " and key: "
     + ProductTagsConstant.SEARCH_PAGE_KEY, e);
  }
  return searchPagePath;
 }
 
 /**
  * Generate product tags map.
  * 
  * @param currentPage
  *         the current page
  * @param productTagSiteConfigs
  *         the product tag site configs
  * @param properties
  *         the properties
  * @param searchPagePath
  *         the search page path
  * @return the map
  */
 private Map<String, Object> generateProductTagsMap(Page currentPage, Map<String, String> productTagSiteConfigs, Map<String, Object> properties,
   String searchPagePath, SlingHttpServletRequest httpServletRequest) {
  
  String title = MapUtils.getString(properties, ProductTagsConstant.TITLE) != null ? MapUtils.getString(properties, ProductTagsConstant.TITLE)
    : StringUtils.EMPTY;
  String[] parentTagsArr = getParentTagsArray(properties, productTagSiteConfigs);
  
  Map<String, Object> productTagsMap = new HashMap<String, Object>();
  Map<String, Tag> tagMap = getTagMap(parentTagsArr, currentPage);
  
  productTagsMap.put(ProductTagsConstant.TITLE, title);
  productTagsMap.put(ProductTagsConstant.MINIMUM_LINKS, productTagSiteConfigs.get(ProductTagsConstant.MINIMUM_LINKS));
  productTagsMap.put(ProductTagsConstant.MAXIMUM_LINKS, productTagSiteConfigs.get(ProductTagsConstant.MAXIMUM_LINKS));
  int min = productTagSiteConfigs.get(ProductTagsConstant.MINIMUM_LINKS) != null ? Integer.parseInt(productTagSiteConfigs
    .get(ProductTagsConstant.MINIMUM_LINKS)) : 0;
  int max = productTagSiteConfigs.get(ProductTagsConstant.MAXIMUM_LINKS) != null ? Integer.parseInt(productTagSiteConfigs
    .get(ProductTagsConstant.MAXIMUM_LINKS)) : 0;
  
  List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
  if (tagMap.size() >= min) {
   list = addTagData(searchPagePath, tagMap, max, httpServletRequest);
  }
  // start code for container tag
  populateContainerTag(currentPage, productTagsMap, list);
  // end container tag code
  productTagsMap.put(ProductTagsConstant.LINKS, list.toArray());
  return productTagsMap;
 }
 
 private String[] getParentTagsArray(Map<String, Object> properties, Map<String, String> productTagSiteConfigs) {
  String[] parentTagsArr = ComponentUtil.getPropertyValueArray(properties, ProductTagsConstant.PARENT_TAG);
  String[] defaultParentTagsarr = productTagSiteConfigs.get(ProductTagsConstant.PARENT_TAG) != null ? productTagSiteConfigs.get(
    ProductTagsConstant.PARENT_TAG).split(CommonConstants.COMMA_CHAR) : new String[] {};
  
  parentTagsArr = parentTagsArr != null && parentTagsArr.length > 0 ? parentTagsArr : defaultParentTagsarr;
  LOGGER.debug("Value of ParentTagsArr = " + parentTagsArr);
  
  return parentTagsArr;
 }
 
 private Map<String, Tag> getTagMap(String[] parentTagsArr, Page currentPage) {
  
  Map<String, Tag> tagMap = new TreeMap<String, Tag>();
  Tag[] tags = currentPage.getTags();
  if (parentTagsArr != null) {
   for (String parentTagId : parentTagsArr) {
    if (tags != null) {
     tagMap = getTags(tagMap, tags, parentTagId, BaseViewHelper.getLocale(currentPage));
    }
   }
  }
  return tagMap;
 }
 
 /**
  * Populate container tag.
  * 
  * @param currentPage
  *         the current page
  * @param productTagsMap
  *         the product tags map
  * @param list
  *         the list
  */
 private void populateContainerTag(Page currentPage, Map<String, Object> productTagsMap, List<Map<String, Object>> list) {
  String tagNames = StringUtils.EMPTY;
  Iterator<Map<String, Object>> itr = list.iterator();
  while (itr.hasNext()) {
   Map<String, Object> map = itr.next();
   tagNames = tagNames + map.get("text") + ",";
  }
  if (tagNames.lastIndexOf(",") > -1) {
   tagNames = tagNames.substring(0, tagNames.lastIndexOf(","));
  }
  LOGGER.debug("tagNames for Container Tag = " + tagNames);
  productTagsMap.put(UnileverConstants.CONTAINER_TAG, ComponentUtil.getContainerTagMap(currentPage, ContainerTagConstants.PRODUCT_TAGS, tagNames));
 }
 
 /**
  * Gets the tags.
  * 
  * @param tagMap
  *         the tag map
  * @param tags
  *         the tags
  * @param parentTagId
  *         the parent tag id
  * @return the tags
  */
 private Map<String, Tag> getTags(Map<String, Tag> tagMap, Tag[] tags, String parentTagId, Locale locale) {
  for (Tag tag : tags) {
   if (tag != null) {
    String tagId = tag.getTagID();
    // all the tags will be added to the map which are
    // associated to the page and also falls in the hierarchy of the parent selected tag
    // in the dialog
    if (tagId.startsWith(parentTagId)) {
     tagMap.put(tag.getTitle(locale), tag);
    }
   }
  }
  return tagMap;
 }
 
 /**
  * Adds the tag data.
  * 
  * @param searchPagePath
  *         the search page path
  * @param tagMap
  *         the tag map
  * @param max
  *         the max
  * @return the list
  */
 private List<Map<String, Object>> addTagData(String searchPagePath, Map<String, Tag> tagMap, int max, SlingHttpServletRequest httpServletRequest) {
  List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
  Iterator<String> tagItr = tagMap.keySet().iterator();
  int counter = 0;
  while (tagItr.hasNext() && counter < max) {
   Map<String, Object> map = new HashMap<String, Object>();
   String tagTitle = tagItr.next();
   map.put(ProductTagsConstant.TEXT, tagTitle);
   
   String url = searchPagePath;
   String requestPath = httpServletRequest.getRequestPathInfo().getResourcePath();
   String[] pagePathArr = requestPath.split(SLASH_CHAR_LITERAL);
   String locale = pagePathArr.length > NUMERIC_THREE ? pagePathArr[NUMERIC_THREE] : "global";
   String brand = pagePathArr[NUMERIC_TWO];
   try {
    if (AdaptiveUtil.isAdaptive(httpServletRequest)) {
     url = searchPagePath + "?" + SearchConstants.SEARCH_KEYWORD + "=" + URLEncoder.encode(tagTitle, "UTF-8") + URL_BRAND + brand + URL_LOCALE
       + locale;
    } else {
     url = searchPagePath + "?" + SearchConstants.SEARCH_KEYWORD + "=" + URLEncoder.encode(tagTitle, "UTF-8");
    }
   } catch (UnsupportedEncodingException e) {
    LOGGER.error("UnsupportedEncodingException in ProductTag ", e);
   }
   map.put(ProductTagsConstant.URL, url);
   list.add(map);
   counter++;
  }
  return list;
 }
 
}
