/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.SocialTABHelper;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.SocialTabQueryDTO;

/**
 * The Class SocialGalleryCarouselExpandedViewHelperImpl.
 */
@Component(description = "SocialGalleryCarouselExpandedViewHelperImpl", immediate = true, metatype = true, label = "SocialGalleryCarouselExpandedViewHelperImpl")
@Service(value = { SocialGalleryCarouselExpandedViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.SOCIAL_GALLERY_CAROUSEL_EXPANDED, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class SocialGalleryCarouselExpandedViewHelperImpl extends SocialTabBaseViewHelperImpl {
 
 /**
  * Constant for title.
  */
 private static final String TITLE = "title";
 
 /**
  * Constant for Introduction Copy.
  */
 private static final String INTRO_COPY = "introCopy";
 
 /**
  * Constant for cta.
  */
 private static final String CTA = "cta";
 
 /**
  * Constant for label.
  */
 private static final String LABEL = "label";
 
 /** The Constant TYPE. */
 private static final String TYPE = "type";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(SocialGalleryCarouselExpandedViewHelperImpl.class);
 
 @Reference
 ConfigurationService configurationService;
 
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.info("Social Gallery Carousel Expanded View Helper #processData called");
  
  Map<String, Object> properties = getProperties(content);
  ResourceResolver resourceResolver = getResourceResolver(resources);
  Resource currentResource = getCurrentResource(resources);
  Page currentPage = getCurrentPage(resources);
  
  String newWindowCta = MapUtils.getString(properties, OPEN_IN_NEW_WINDOW, StringUtils.EMPTY);
  
  Map<String, Object> socialGalleryCarouselExpandedMap = new LinkedHashMap<String, Object>();
  
  int numberOfPosts = Integer.parseInt(GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
    UnileverConstants.SOCIAL_GALLERY_CAROUSEL_EXPANDED_TAB_CONFIG_CAT, NUMBER_OF_POSTS));
  
  int maxImages = Integer.parseInt(GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
    UnileverConstants.SOCIAL_GALLERY_CAROUSEL_EXPANDED_TAB_CONFIG_CAT, MAXIMUM_IMAGES));
  
  socialGalleryCarouselExpandedMap.put(TITLE, getTitle(properties));
  socialGalleryCarouselExpandedMap.put(INTRO_COPY, getText(properties));
  
  addCTAMap(socialGalleryCarouselExpandedMap, getCtaText(properties), getCtaUrl(properties, resources), CTA, newWindowCta);
  
  // adding social images map
  addSocialImagesMap(socialGalleryCarouselExpandedMap, resourceResolver, currentPage, currentResource, resources);
  
  // adding retrieval query parameters
  SocialTABHelper.addRetrievalQueryParametrsMap(socialGalleryCarouselExpandedMap, prepareTagQuery(content, resources, numberOfPosts, maxImages));
  
  socialGalleryCarouselExpandedMap.put("closeLabel", getI18n(resources).get("socialGalleryTAB.closeLabel"));
  socialGalleryCarouselExpandedMap.put("type", MapUtils.getString(properties, TYPE));
  
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  data.put(getJsonNameSpace(content), socialGalleryCarouselExpandedMap);
  
  return data;
 }
 
 private SocialTabQueryDTO prepareTagQuery(Map<String, Object> content, Map<String, Object> resources, int numberOfPosts, int maxImages) {
  Page currentPage = getCurrentPage(resources);
  SocialTabQueryDTO tabQuery = new SocialTabQueryDTO();
  tabQuery.setBrandNameParameter(getBrandNameParameter(configurationService, currentPage));
  tabQuery.setConfigurationService(configurationService);
  tabQuery.setHashTags(getHashTags(getCurrentResource(resources)));
  tabQuery.setMaxImages(maxImages);
  tabQuery.setNoOfPost(numberOfPosts);
  tabQuery.setPage(currentPage);
  tabQuery.setPagePath(getResourceResolver(resources).map(currentPage.getPath()));
  tabQuery.setSocialChannels(getSocialChannels(getProperties(content)));
  return tabQuery;
 }
 
 private void addSocialImagesMap(Map<String, Object> socialGalleryMap, ResourceResolver resourceResolver, Page page, Resource currentResource,
   Map<String, Object> resources) {
  
  List<Map<String, Object>> socialPostsList = new ArrayList<Map<String, Object>>();
  List<Map<String, String>> list = getSocialGalleryExpandedImagesMap(currentResource);
  for (Map<String, String> map : list) {
   String imagePath = getImagePath(map);
   String pagePath = getPagePath(map);
   String relatedCtaPagePath = ComponentUtil.getFullURL(resourceResolver, pagePath, getSlingRequest(resources));
   String relatedArticleCtaType = getRelatedCtaLabel(map);
   Page associatedPage = page.getPageManager().getPage(pagePath);
   Map<String, Object> socialPostsMap = SocialTABHelper.getImageMapWithMetadata(imagePath, page, resourceResolver, getSlingRequest(resources));
   addCTAMap(socialPostsMap, relatedArticleCtaType, relatedCtaPagePath, "realtedCta", associatedPage, resourceResolver, resources);
   socialPostsList.add(socialPostsMap);
  }
  socialGalleryMap.put("editorSocialImages", socialPostsList.toArray());
 }
 
 /**
  * Adds the cta map.
  * 
  * @param socialGalleryCarouselMap
  *         the social gallery carousel map
  * @param ctaType
  *         the label text
  * @param pagePath
  *         the link text
  */
 private void addCTAMap(Map<String, Object> socialGalleryCarouselMap, String ctaType, String pagePath, String key, Page associatedPage,
   ResourceResolver resourceResolver, Map<String, Object> resources) {
  Map<String, Object> ctaMap = new HashMap<String, Object>();
  String ctaLabelText = StringUtils.EMPTY;
  I18n i18n = getI18n(resources);
  
  if ("article".equals(ctaType)) {
   ctaLabelText = i18n.get("socialTabBase.viewArticle");
  } else if ("product".equals(ctaType)) {
   ctaLabelText = i18n.get("socialTabBase.viewProduct");
   ctaMap.put("associatedProduct", getProductMap(associatedPage, resources));
  }
  
  ctaMap.put(LABEL, ctaLabelText);
  ctaMap.put(URL, resourceResolver.map(pagePath));
  
  socialGalleryCarouselMap.put(key, ctaMap);
 }
 
 private static void addCTAMap(Map<String, Object> socialGalleryCarouselMap, String labelText, String pagePath, String key, String newWindowCta) {
  Map<String, Object> ctaMap = new HashMap<String, Object>();
  
  ctaMap.put(LABEL, labelText);
  ctaMap.put(URL, pagePath);
  if (newWindowCta.equals(BOOLEAN_TRUE)) {
   ctaMap.put(OPEN_IN_NEW_WINDOW, Boolean.valueOf(BOOLEAN_TRUE));
  } else {
   ctaMap.put(OPEN_IN_NEW_WINDOW, Boolean.valueOf(BOOLEAN_FALSE));
  }
  
  socialGalleryCarouselMap.put(key, ctaMap);
 }
 
}
