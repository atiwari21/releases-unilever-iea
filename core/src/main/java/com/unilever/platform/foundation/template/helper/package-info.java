/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

@Version("1.0")
@Export(optional = "provide:=true")
package com.unilever.platform.foundation.template.helper;

import aQute.bnd.annotation.Export;
import aQute.bnd.annotation.Version;
