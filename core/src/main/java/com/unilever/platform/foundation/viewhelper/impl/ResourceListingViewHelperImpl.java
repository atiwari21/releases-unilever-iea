/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.jcr.resource.JcrResourceConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.RecipeHelper;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ResourceCommonConstants;

/**
 * The Class ResourceListingViewHelperImpl.
 */
@Component(description = "ResourceListingViewHelperImpl", immediate = true, metatype = true, label = "ResourceListingViewHelperImpl")
@Service(value = { ResourceListingViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.RESOURCE_LISTING, propertyPrivate = true),
  
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class ResourceListingViewHelperImpl extends BaseViewHelper {
 
 private static final String COMMA = ",";
 
 private static final String PAGINATION = "pagination";
 
 private static final String RESOURCE_LISTING_ZIP_FILE_NAME = "resourceListing.zipFileName";
 
 private static final String ZIP = ".zip";
 
 private static final String SHORT_SUB_HEADING_TEXT = "shortSubHeadingText";
 
 private static final String LONG_SUB_HEADING_TEXT = "longSubHeadingText";
 
 private static final String GENERAL_CONFIG_MAP = "generalConfigMap";
 
 /** The Constant ZIP_FILE_NAME. */
 private static final String ZIP_FILE_NAME = "zipFileName";
 
 /** The Constant RESOURCES. */
 private static final String RESOURCES = "resources";
 
 /** The Constant DOCUMENTS. */
 private static final String DOCUMENTS = "documents";
 
 /** The Constant DOWNLOAD_PANEL_HEADING. */
 private static final String DOWNLOAD_PANEL_HEADING = "downloadPanelHeading";
 
 /** The Constant RESOURCE_ITEM_CSS_CLASS. */
 private static final String RESOURCE_ITEM_CSS_CLASS = "resourceItemCSSClass";
 
 /** The Constant DOWNLOAD_CTA. */
 private static final String DOWNLOAD_CTA = "downloadCTA";
 
 /** The Constant DISPLAY_ZIP_DOWNLOAD_OPTION. */
 private static final String DISPLAY_ZIP_DOWNLOAD_OPTION = "displayZipDownloadOption";
 
 /** The Constant TAGS_TO_DISPLAY. */
 private static final String TAGS_TO_DISPLAY = "tagsToDisplay";
 
 /** The Constant LONG_SUB_HEADINGTEXT. */
 private static final String LONG_SUB_HEADINGTEXT = "longSubHeadingtext";
 
 /** The Constant SUB_HEADINGTEXT. */
 private static final String SUB_HEADINGTEXT = "subHeadingtext";
 
 /** The Constant HEADING_TEXT. */
 private static final String HEADING_TEXT = "headingText";
 
 /** The Constant FIRST_LABEL. */
 private static final String FIRST_LABEL = "firstLabel";
 
 /** The Constant LAST_LABEL. */
 private static final String LAST_LABEL = "lastLabel";
 
 /** The Constant NEXT_LABEL. */
 private static final String NEXT_LABEL = "nextLabel";
 
 /** The Constant PREVIOUS_LABEL. */
 private static final String PREVIOUS_LABEL = "previousLabel";
 
 /** The Constant TEN. */
 private static final int TEN = 10;
 
 /** The Constant FILE_PATH. */
 private static final String FILE_PATH = "filePath";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ResourceListingViewHelperImpl.class);
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  
  LOGGER.debug("Article Listing View Helper #processData called .");
  Map<String, Object> properties = getProperties(content);
  Map<String, Object> resourceListingMap = new LinkedHashMap<String, Object>();
  
  I18n i18n = getI18n(resources);
  
  Map<String, Object> generalConfigMap = getGeneralConfigValues(properties);
  resourceListingMap.put(GENERAL_CONFIG_MAP, generalConfigMap);
  
  Map<String, Object> paginationMap = getPaginationOptionValues(properties, i18n);
  resourceListingMap.put(PAGINATION, paginationMap);
  
  TagManager tagMgr = getTagManager(resources);
  
  getResources(resources, resourceListingMap, tagMgr, i18n);
  
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(getJsonNameSpace(content), resourceListingMap);
  return data;
 }
 
 /**
  * Gets the resources.
  * 
  * @param resources
  *         the resources
  * @param properties
  *         the properties
  * @param resourceListingMap
  *         the resource listing map
  * @param tagMgr
  *         the tag mgr
  * @param i18n
  *         the i 18 n
  * @param resolver
  *         the resolver
  * @param currentPage
  *         the current page
  * @return the resources
  */
 @SuppressWarnings("unchecked")
 private void getResources(Map<String, Object> resources, Map<String, Object> resourceListingMap, TagManager tagMgr, I18n i18n) {
  
  ResourceResolver resolver = getResourceResolver(resources);
  Page currentPage = getCurrentPage(resources);
  List<Map<String, Object>> resourceList = new LinkedList<Map<String, Object>>();
  
  List<Map<String, Object>> filesList = getNestedMultiFieldProperties(getCurrentResource(resources), "configureResources");
  
  for (Map<String, Object> fileMap : filesList) {
   
   List<Map<String, Object>> tagsToDisplayList = new LinkedList<Map<String, Object>>();
   Map<String, Object> resourceMap = new LinkedHashMap<String, Object>();
   String headingText = MapUtils.getString(fileMap, "resourceHeading", StringUtils.EMPTY);
   resourceMap.put(HEADING_TEXT, headingText);
   resourceMap.put(SUB_HEADINGTEXT, MapUtils.getString(fileMap, "resourceSubHeading", StringUtils.EMPTY));
   resourceMap.put(LONG_SUB_HEADINGTEXT, MapUtils.getString(fileMap, "resourceLongSubHeading", StringUtils.EMPTY));
   
   String tagsToDisplay = StringUtils.isNotBlank(MapUtils.getString(fileMap, TAGS_TO_DISPLAY)) ? MapUtils.getString(fileMap, TAGS_TO_DISPLAY,
     StringUtils.EMPTY) : StringUtils.EMPTY;
   String[] tagToDisplayArray = StringUtils.isNotBlank(tagsToDisplay) ? tagsToDisplay.split(COMMA) : new String[] {};
   getTagList(tagsToDisplayList, tagMgr, tagToDisplayArray, getCurrentPage(resources));
   Map<String, Object>[] tagsToDisplayArray = (Map<String, Object>[]) new Map[tagsToDisplayList.size()];
   
   resourceMap.put(TAGS_TO_DISPLAY, tagsToDisplayList.toArray(tagsToDisplayArray));
   
   String displayZipDownloadOption = MapUtils.getString(fileMap, DISPLAY_ZIP_DOWNLOAD_OPTION);
   boolean displayZipDownloadOptionFlag = displayZipDownloadOption != null && "[true]".equals(displayZipDownloadOption) ? true : false;
   
   resourceMap.put(DISPLAY_ZIP_DOWNLOAD_OPTION, displayZipDownloadOptionFlag);
   
   resourceMap.put(DOWNLOAD_CTA, downloadCTAMap(i18n, resolver, currentPage));
   
   resourceMap.put(RESOURCE_ITEM_CSS_CLASS, MapUtils.getString(fileMap, "resourceitemCSSClass", StringUtils.EMPTY));
   
   resourceMap.put(DOWNLOAD_PANEL_HEADING, i18n.get("resourceListing.downloadIndividualDocumentsCtaLabel"));
   
   String downloadFileName = StringUtils.isNotBlank(headingText) ? getDownloadFileName(headingText) : i18n.get(RESOURCE_LISTING_ZIP_FILE_NAME);
   if (!StringUtils.endsWith(downloadFileName, ZIP)) {
    downloadFileName = downloadFileName + ZIP;
   }
   
   resourceMap.put(ZIP_FILE_NAME, downloadFileName);
   
   resourceMap.put(DOCUMENTS, getAllDocuments(fileMap));
   
   resourceList.add(resourceMap);
  }
  
  Object[] resourceArray = !resourceList.isEmpty() ? resourceList.toArray() : ArrayUtils.EMPTY_STRING_ARRAY;
  
  resourceListingMap.put(RESOURCES, resourceArray);
 }
 
 /**
  * gets the replaced non alpha numeric string
  * 
  * @param downloadFileName
  * @return
  */
 private String getDownloadFileName(String downloadFileName) {
  return downloadFileName.replaceAll(ResourceCommonConstants.NON_ALPHA_NUMERIC_REGEX, ResourceCommonConstants.UNDERSCORE);
 }
 
 /**
  * Gets the all documents.
  * 
  * @param properties
  *         the properties
  * @param documentsList
  *         the documents list
  * @return the all documents
  */
 @SuppressWarnings("unchecked")
 private List<Map<String, Object>> getAllDocuments(Map<String, Object> properties) {
  
  List<Map<String, Object>> filesList = (List<Map<String, Object>>) properties.get("files");
  List<Map<String, Object>> fileList = new LinkedList<Map<String, Object>>();
  
  for (Map<String, Object> eachFile : filesList) {
   fileList = new LinkedList<Map<String, Object>>();
   
   int fileNumber = MapUtils.getInteger(eachFile, "fileNumber", 0);
   
   for (int i = 1; i <= TEN; i++) {
    Object tes = eachFile.get(FILE_PATH + i);
    
    Map<String, Object> eachModifiedFile = new LinkedHashMap<String, Object>();
    
    if (!tes.equals(StringUtils.EMPTY) && i <= fileNumber) {
     
     eachModifiedFile.put("path", tes);
     eachModifiedFile.put("extension", ((String) tes).substring(((String) tes).lastIndexOf(".") + 1).trim());
     
     fileList.add(eachModifiedFile);
     eachFile.remove(FILE_PATH + i);
    } else {
     eachFile.remove(FILE_PATH + i);
    }
    eachFile.put("files", fileList);
   }
   
  }
  
  return filesList;
 }
 
 /**
  * Gets the nested multi field properties.
  * 
  * @param resource
  *         the resource
  * @param propertyName
  *         the property name
  * @return the nested multi field properties
  */
 public static List<Map<String, Object>> getNestedMultiFieldProperties(Resource resource, String propertyName) {
  
  List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
  
  if (resource != null && propertyName != null) {
   Node currentNode = resource.adaptTo(Node.class);
   try {
    javax.jcr.Property property = null;
    
    if (currentNode != null && currentNode.hasProperty(propertyName)) {
     property = currentNode.getProperty(propertyName);
    }
    
    if (property != null) {
     Value[] values = getValueArray(property);
     
     setList(list, values);
    }
   } catch (RepositoryException e) {
    LOGGER.error(ExceptionUtils.getStackTrace(e));
   }
  }
  return list;
 }
 
 /**
  * sets list
  * 
  * @param list
  * @param values
  * @throws JSONException
  * @throws ValueFormatException
  * @throws RepositoryException
  */
 private static void setList(List<Map<String, Object>> list, Value[] values) {
  for (Value val : values) {
   try {
    JSONObject rootObject = new JSONObject(val.getString());
    
    Map<String, Object> topMap = RecipeHelper.toMap(rootObject);
    if (topMap != null) {
     list.add(topMap);
    }
   } catch (RepositoryException e) {
    LOGGER.error(ExceptionUtils.getStackTrace(e));
   } catch (JSONException e) {
    LOGGER.error("Error while fetching JSON Object", e);
   }
  }
 }
 
 /**
  * Gets the value array.
  * 
  * @param property
  *         the property
  * @return the value array
  * @throws RepositoryException
  *          the repository exception
  */
 private static Value[] getValueArray(javax.jcr.Property property) {
  Value[] values = null;
  try {
   if (property.isMultiple()) {
    values = property.getValues();
   } else {
    values = new Value[1];
    values[0] = property.getValue();
   }
  } catch (ValueFormatException e) {
   LOGGER.error("error while formating value " + ExceptionUtils.getStackTrace(e));
  } catch (RepositoryException e) {
   LOGGER.error("repository exception found " + ExceptionUtils.getStackTrace(e));
  }
  
  return values;
 }
 
 /**
  * Download CTA map.
  * 
  * @param properties
  *         the properties
  * @param i18n
  *         the i 18 n
  * @param resolver
  *         the resolver
  * @param currentPage
  *         the current page
  * @return the map
  */
 private Map<String, Object> downloadCTAMap(I18n i18n, ResourceResolver resolver, Page currentPage) {
  
  Map<String, Object> downloadMap = new LinkedHashMap<String, Object>();
  downloadMap.put("label", i18n.get("resourceListing.downloadAllDocumentsCtaLabel"));
  downloadMap.put("url", resolver.map(currentPage.getPath()) + "." + "dwnld" + "." + "zip");
  
  return downloadMap;
  
 }
 
 /**
  * Gets the tag list.
  * 
  * @param tagsToDisplayList
  *         the tags to display list
  * @param tagMgr
  *         the tag mgr
  * @param tagsToDisplay
  *         the tags to display
  * @return the tag list
  */
 private void getTagList(List<Map<String, Object>> tagsToDisplayList, TagManager tagMgr, String[] tagsToDisplay, Page currentPage) {
  
  Map<String, Object> tagsToDisplayMap = new LinkedHashMap<String, Object>();
  for (String tagName : tagsToDisplay) {
   
   tagsToDisplayMap = new LinkedHashMap<String, Object>();
   
   if (tagMgr != null) {
    Tag tag = tagMgr.resolve(tagName);
    int tagDeepLength = tagName.split("/").length;
    String title = (tag != null) ? tag.getTitle(getLocale(currentPage)) : tagName.split("/")[tagDeepLength - 1];
    tagsToDisplayMap.put("tagName", title);
    
    tagsToDisplayList.add(tagsToDisplayMap);
   }
  }
 }
 
 /**
  * Gets the pagination option values.
  * 
  * @param properties
  *         the properties
  * @param resourceListingMap
  *         the resource listing map
  * @param i18n
  *         the i 18 n
  * @return
  * @return the pagination option values
  */
 private Map<String, Object> getPaginationOptionValues(Map<String, Object> properties, I18n i18n) {
  
  Map<String, Object> paginationMap = new LinkedHashMap<String, Object>();
  Map<String, Object> staticMap = new LinkedHashMap<String, Object>();
  staticMap.put(FIRST_LABEL, i18n.get("resourceListing.pagination.first"));
  staticMap.put(LAST_LABEL, i18n.get("resourceListing.pagination.last"));
  staticMap.put(PREVIOUS_LABEL, i18n.get("resourceListing.pagination.previous"));
  staticMap.put(NEXT_LABEL, i18n.get("resourceListing.pagination.next"));
  
  paginationMap.put("paginationType", MapUtils.getString(properties, "paginationType", StringUtils.EMPTY));
  paginationMap.put("paginationCtaLabel", MapUtils.getString(properties, "paginationCtaLabel", StringUtils.EMPTY));
  paginationMap.put("itemsPerPage", Integer.parseInt(MapUtils.getString(properties, "itemsPerPage", StringUtils.EMPTY)));
  
  paginationMap.put("static", staticMap);
  
  return paginationMap;
  
 }
 
 /**
  * Gets the general config values.
  * 
  * @param properties
  *         the properties
  * @param resourceListingMap
  *         the resource listing map
  * @return
  * @return the general config values
  */
 private Map<String, Object> getGeneralConfigValues(Map<String, Object> properties) {
  
  Map<String, Object> generalConfigMap = new HashMap<String, Object>();
  String headingText = MapUtils.getString(properties, HEADING_TEXT);
  String shortSubHeadingText = MapUtils.getString(properties, SHORT_SUB_HEADING_TEXT);
  String longSubHeadingText = MapUtils.getString(properties, LONG_SUB_HEADING_TEXT);
  
  generalConfigMap.put(HEADING_TEXT, headingText);
  generalConfigMap.put(SHORT_SUB_HEADING_TEXT, shortSubHeadingText);
  generalConfigMap.put(LONG_SUB_HEADING_TEXT, longSubHeadingText);
  
  return generalConfigMap;
  
 }
 
}