/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.engine.SlingRequestProcessor;
import org.apache.sling.xss.XSSAPI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.contentsync.handler.util.RequestResponseFactory;
import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.WCMMode;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.utils.CollectionUtils;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.aem.foundation.servlets.FileUploadHelperServlet;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.core.i18n.UnileverI18nImpl;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.FormElementConstants;
import com.unilever.platform.foundation.viewhelper.constants.FormElementConstantsV2;

/**
 * This class prepares the data map for Form Element V2 by invoking the corresponding service. For example, if the element type is of textField then
 * TextFieldMethod is called.
 */

@Component(description = "FormElementViewHelperImplV2", immediate = true, metatype = true, label = "FormElementViewHelperImpl")
@Service(value = { FormElementViewHelperImplV2.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.FORM_ELEMENT_V2, propertyPrivate = true),
  @Property(name = org.osgi.framework.Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class FormElementViewHelperImplV2 extends BaseViewHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(FormElementViewHelperImplV2.class);
 
 /** The Constant TWO. */
 private static final int TWO = 2;
 /**
  * Reference for XSSAPI.
  */
 @Reference
 private XSSAPI xssapi;
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The factory. */
 @Reference
 org.apache.http.osgi.services.HttpClientBuilderFactory factory;
 
 /** The request response factory. */
 @Reference
 private RequestResponseFactory requestResponseFactory;
 
 /** The request processor. */
 @Reference
 private SlingRequestProcessor requestProcessor;
 
 /**
  * This method invokes the corresponding element type service depending on the element/view type of the form element that is selected.
  * 
  * @param content
  *         {@link Map} of all user input received as input
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return Map holding the result data.
  */
 @SuppressWarnings("unchecked")
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  Map<String, Object> propertiesValueMap = (Map<String, Object>) content.get(PROPERTIES);
  String currentPath = MapUtils.getString(content, "currentPath", getCurrentResource(resources).getPath());
  ResourceResolver resourceResolver = getResourceResolver(resources);
  Resource currentResource = resourceResolver.getResource(currentPath);
  Resource parentResource = currentResource.getParent();
  Resource parentFormResource = parentResource.getParent();
  ValueMap parentProperties = parentFormResource.adaptTo(ValueMap.class);
  Page currentPage = getCurrentPage(resources);
  SlingHttpServletRequest slingHttpServletRequest = getSlingRequest(resources);
  I18n i18n = getI18n(resources);
  Map<String, Object> finalMap = new HashMap<String, Object>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  Map<String, Object> textValues = new LinkedHashMap<String, Object>();
  List<Map<String, Object>> rulesList = new LinkedList<Map<String, Object>>();
  Map<String, Object> properties = CollectionUtils.getJcrPropertyAsMap(propertiesValueMap);
  String elementType = (String) properties.get(FormElementConstantsV2.ELEMENT_TYPE);
  String buttonType = (String) properties.get(FormElementConstantsV2.BUTTONTYPE);
  String jsonNameSpace = MapUtils.getString(content, "jsonNameSpace");
  String fieldId = MapUtils.getString(properties, FormElementConstantsV2.FIELD_ID, StringUtils.EMPTY);
  getFieldValues(properties, textValues);
  handleRows(properties, textValues);
  addNameField(properties, textValues);
  addFieldValue(properties, textValues);
  handleRadioButton(properties, textValues, resources);
  
  handleDropdown(properties, textValues, currentResource);
  handleAllowMultipleFiles(properties, textValues, resources);
  handleHiddenValue(properties, textValues);
  handleCaptcha(resources, properties, textValues);
  handleMandatoryField(properties, rulesList);
  handleDate(properties, textValues, rulesList, currentResource);
  textValues.put("rules", rulesList);
  addCustomValidations(textValues, currentResource, i18n);
  
  List<Map<String, String>> messageConfigs = getErrorConfigs(slingHttpServletRequest, currentPage, parentProperties, fieldId);
  textValues.put("messageConfigs", messageConfigs.toArray());
  
  textValues.put(FormElementConstantsV2.ELEMENT_TYPE, elementType);
  if (FormElementConstantsV2.FORMBUTTON.equals(elementType)) {
   textValues.put(FormElementConstantsV2.BUTTONTYPE, buttonType);
  }
  
  properties.put("isContainer", MapUtils.getBoolean(properties, "isContainer", false));
  
  LOGGER.info("The form element is of type " + elementType);
  if (elementType.equals(FormElementConstantsV2.COUPON_CODE)) {
   handleCouponCode(properties, textValues, getI18n(resources));
  }
  finalMap.put(jsonNameSpace, textValues);
  return finalMap;
 }
 
 @SuppressWarnings("unchecked")
 private void addCustomValidations(Map<String, Object> textValues, Resource currentResource, I18n i18n) {
  
  if (currentResource != null) {
   String name = MapUtils.getString(currentResource.getValueMap(), FormElementConstantsV2.FIELD_ID, "");
   String jsonString = StringUtils.EMPTY;
   HttpServletRequest httpServletRequest = requestResponseFactory.createRequest("GET", currentResource.getPath() + ".jsons.customValidation.json");
   httpServletRequest.setAttribute(WCMMode.REQUEST_ATTRIBUTE_NAME, WCMMode.DISABLED);
   ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
   HttpServletResponse httpServletResponse = requestResponseFactory.createResponse(arrayOutputStream);
   
   try {
    requestProcessor.processRequest(httpServletRequest, httpServletResponse, currentResource.getResourceResolver());
    jsonString = arrayOutputStream.toString();
    Map<String, Object> validations = new Gson().fromJson(jsonString, new HashMap<String, Object>().getClass());
    @SuppressWarnings("rawtypes")
    Map customValidationMap = MapUtils.getMap(validations, name);
    if (customValidationMap != null) {
     String msg = i18n.get(MapUtils.getString(customValidationMap, "msg", ""));
     customValidationMap.put("msg", msg);
     textValues.put("customValidation", customValidationMap);
    }
    
   } catch (ServletException e) {
    LOGGER.error("Servlet Exception:", e.getMessage(), e);
   } catch (IOException e) {
    LOGGER.error("IO Exception", e.getMessage(), e);
   } catch (JsonSyntaxException e) {
    LOGGER.warn("JsonSyntaxException", e);
   }
  }
 }
 
 /**
  * Gets the field values.
  * 
  * @param properties
  *         the properties
  * @param textValues
  *         the text values
  * @return the field values
  */
 private void getFieldValues(Map<String, Object> properties, Map<String, Object> textValues) {
  String elementType = (String) properties.get(FormElementConstantsV2.ELEMENT_TYPE);
  String fieldLabel = MapUtils.getString(properties, FormElementConstantsV2.FIELD_LABEL, StringUtils.EMPTY);
  if (!(FormElementConstantsV2.CAPTCHA.equals(elementType) || FormElementConstantsV2.RADIO.equals(elementType))) {
   if (!(elementType.equals(FormElementConstantsV2.CHECKBOX))) {
    fieldLabel = fieldLabel.replaceAll(FormElementConstantsV2.ESCAPE_HTML_REGEX, "");
   }
   textValues.put(FormElementConstantsV2.FIELD_LABEL, fieldLabel);
   if (!(FormElementConstantsV2.FORMBUTTON.equals(elementType))) {
    textValues.put(FormElementConstantsV2.FIELD_ID, MapUtils.getString(properties, FormElementConstantsV2.FIELD_ID, StringUtils.EMPTY));
    textValues.put("placeHolderValue", MapUtils.getString(properties, FormElementConstantsV2.PLACE_HOLDER_VALUE, StringUtils.EMPTY));
    textValues.put("defaultValue", MapUtils.getString(properties, FormElementConstantsV2.DEFAULT_VALUE, StringUtils.EMPTY));
    textValues.put("toolTipText", MapUtils.getString(properties, FormElementConstantsV2.TOOL_TIP_TEXT, StringUtils.EMPTY));
   }
  }
 }
 
 /**
  * Handle rows.
  * 
  * @param properties
  *         the properties
  * @param textValues
  *         the text values
  */
 private void handleRows(Map<String, Object> properties, Map<String, Object> textValues) {
  String elementType = (String) properties.get(FormElementConstantsV2.ELEMENT_TYPE);
  if (elementType != null && elementType.equals(FormElementConstantsV2.TEXTAREA)) {
   textValues.put("rows", Integer.parseInt(MapUtils.getString(properties, FormElementConstantsV2.ROWS, "0")));
  }
 }
 
 /**
  * Adds the name field.
  * 
  * @param properties
  *         the properties
  * @param textValues
  *         the text values
  */
 private void addNameField(Map<String, Object> properties, Map<String, Object> textValues) {
  String fieldId = MapUtils.getString(properties, FormElementConstantsV2.FIELD_ID, StringUtils.EMPTY);
  textValues.put("name", MapUtils.getString(properties, FormElementConstantsV2.NAME, fieldId));
  
 }
 
 /**
  * Adds the field value.
  * 
  * @param properties
  *         the properties
  * @param textValues
  *         the text values
  */
 private void addFieldValue(Map<String, Object> properties, Map<String, Object> textValues) {
  String elementType = (String) properties.get(FormElementConstantsV2.ELEMENT_TYPE);
  if (elementType != null && elementType.equals(FormElementConstantsV2.CHECKBOX)) {
   textValues.put("fieldValue", MapUtils.getString(properties, FormElementConstantsV2.FIELD_VALUE, StringUtils.EMPTY));
   textValues.put(FormElementConstantsV2.CHECKED_BY_DEFAULT, MapUtils.getBoolean(properties, FormElementConstantsV2.CHECKED_BY_DEFAULT, false));
  }
 }
 
 /**
  * Handle radio button.
  * 
  * @param properties
  *         the properties
  * @param textValues
  *         the text values
  * @param resource
  *         the resource
  */
 private void handleRadioButton(Map<String, Object> properties, Map<String, Object> textValues, Map<String, Object> resource) {
  String elementType = (String) properties.get(FormElementConstantsV2.ELEMENT_TYPE);
  if (FormElementConstantsV2.RADIO.equals(elementType)) {
   textValues.put("groupName", MapUtils.getString(properties, FormElementConstantsV2.GROUP_NAME, StringUtils.EMPTY));
   textValues.put("radioOptions", getRadioValues(getCurrentResource(resource)).toArray());
  }
 }
 
 void handleDate(Map<String, Object> properties, Map<String, Object> textValues, List<Map<String, Object>> rulesList, Resource currentResource) {
  String elementType = (String) properties.get(FormElementConstantsV2.ELEMENT_TYPE);
  if (FormElementConstantsV2.DATE.equals(elementType)) {
   Boolean includeIntervalRestrictions = Boolean
     .parseBoolean(MapUtils.getString(properties, FormElementConstantsV2.DATE_INCLUDE_INTERVAL_RESTRICTION, FormElementConstantsV2.FALSE));
   Map<String, Object> includeIntervalMap = new HashMap<String, Object>();
   includeIntervalMap.put("includeIntervalRestriction", includeIntervalRestrictions);
   if (includeIntervalRestrictions) {
    includeIntervalMap.put("restrictions", getIncludeIntervalProperties(currentResource));
   }
   textValues.put("includeIntervalRestrictions", includeIntervalMap);
   String dateFormatProperty = MapUtils.getString(properties, "validationsvalidationExpression",
     "dd/mm/yyyy_/%5E%28%3F%3A0%5B1-9%5D%7C%5B12%5Dd%7C3%5B01%5D%29%5B/%5D%28%3F%3A0%5B1-9%5D%7C1%5B012%5D%29%5B/%5D%5B0-9%5D%7B4%7D%24");
   
   int splitterIndex = dateFormatProperty.indexOf('_');
   String dateFormatType = dateFormatProperty;
   if (splitterIndex > 0) {
    dateFormatType = dateFormatProperty.substring(0, dateFormatProperty.indexOf('_'));
    if (dateFormatProperty.length() >= splitterIndex + TWO) {
     String dateRegex = dateFormatProperty.substring(dateFormatProperty.indexOf('_') + TWO, dateFormatProperty.length());
     handleDateValidationFields(properties, rulesList, dateRegex);
    }
   }
   textValues.put(FormElementConstants.DATE_FORMAT, dateFormatType);
   
  }
 }
 
 /**
  * Handle dropdown.
  * 
  * @param properties
  *         the properties
  * @param textValues
  *         the text values
  * @param resource
  *         the resource
  */
 private void handleDropdown(Map<String, Object> properties, Map<String, Object> textValues, Resource resource) {
  String elementType = (String) properties.get(FormElementConstantsV2.ELEMENT_TYPE);
  if (FormElementConstantsV2.DROPDOWN.equals(elementType)) {
   textValues.put("dropdownOptions", getDropdownValues(resource).toArray());
  }
 }
 
 /**
  * Gets the radio values.
  * 
  * @param currentResource
  *         the current resource
  * @return the radio values
  */
 private List<Map<String, Object>> getRadioValues(Resource currentResource) {
  List<Map<String, Object>> radioMapList = new ArrayList<Map<String, Object>>();
  
  List<String> radioPropertyName = new ArrayList<String>();
  radioPropertyName.add("fieldLabelRadio");
  radioPropertyName.add("fieldValueRadio");
  radioPropertyName.add("fieldIdRadio");
  radioPropertyName.add("tooltipTextRadio");
  radioPropertyName.add("checkedByDefaultRadio");
  
  LOGGER.debug("footer field properties : " + radioPropertyName);
  
  List<Map<String, String>> list = ComponentUtil.getNestedMultiFieldProperties(currentResource, "datavalues");
  
  for (Map<String, String> map : list) {
   Map<String, Object> radioMap = new LinkedHashMap<String, Object>();
   
   radioMap.put("fieldLabel", xssapi.encodeForHTML(MapUtils.getString(map, FormElementConstantsV2.FIELD_LABEL_RADIO, StringUtils.EMPTY)));
   radioMap.put("fieldValue", xssapi.encodeForHTML(MapUtils.getString(map, FormElementConstantsV2.FIELD_VALUE_RADIO, StringUtils.EMPTY)));
   radioMap.put(FormElementConstantsV2.FIELD_ID,
     xssapi.encodeForHTML(MapUtils.getString(map, FormElementConstantsV2.FIELD_ID_RADIO, StringUtils.EMPTY)));
   radioMap.put("tooltipText", xssapi.encodeForHTML(MapUtils.getString(map, FormElementConstantsV2.TOOL_TIP_TEXT_RADIO)));
   boolean checkedByDefault = false;
   if ("[true]".equals(MapUtils.getString(map, FormElementConstantsV2.CHECKED_BY_DEFAULT_RADIO))) {
    checkedByDefault = true;
   }
   
   radioMap.put(FormElementConstantsV2.CHECKED_BY_DEFAULT, checkedByDefault);
   radioMapList.add(radioMap);
  }
  
  LOGGER.debug("Restrictions Properties  : " + radioMapList.size());
  return radioMapList;
 }
 
 /**
  * Handle allow multiple files.
  * 
  * @param properties
  *         the properties
  * @param textValues
  *         the text values
  * @param resources
  *         the resources
  */
 private void handleAllowMultipleFiles(Map<String, Object> properties, Map<String, Object> textValues, Map<String, Object> resources) {
  String elementType = (String) properties.get(FormElementConstantsV2.ELEMENT_TYPE);
  if (FormElementConstantsV2.FILEUPLOAD.equals(elementType)) {
   textValues.put(FormElementConstantsV2.ALLOW_MULTIPLE_FILES, MapUtils.getBoolean(properties, FormElementConstantsV2.ALLOW_MULTIPLE_FILES, false));
   textValues.put(FormElementConstantsV2.SERVLET_PATH,
     ComponentUtil.getFullURL(getResourceResolver(resources), FileUploadHelperServlet.SERVLET_PATH, getSlingRequest(resources)));
  }
 }
 
 /**
  * Handle hidden value.
  * 
  * @param properties
  *         the properties
  * @param textValues
  *         the text values
  */
 private void handleHiddenValue(Map<String, Object> properties, Map<String, Object> textValues) {
  String elementType = (String) properties.get(FormElementConstantsV2.ELEMENT_TYPE);
  if (FormElementConstantsV2.HIDDENFIELD.equals(elementType)) {
   textValues.put(FormElementConstantsV2.VALUE, MapUtils.getString(properties, FormElementConstantsV2.HIDDEN_VALUE, StringUtils.EMPTY));
   textValues.put(FormElementConstantsV2.DATA_TYPE, MapUtils.getString(properties, FormElementConstantsV2.DATA_TYPE, "text"));
  }
 }
 
 /**
  * Handle captcha.
  * 
  * @param resource
  *         the resource
  * @param properties
  *         the properties
  * @param textValues
  *         the text values
  */
 private void handleCaptcha(Map<String, Object> resource, Map<String, Object> properties, Map<String, Object> textValues) {
  String elementType = (String) properties.get(FormElementConstantsV2.ELEMENT_TYPE);
  if (elementType != null && elementType.equals(FormElementConstantsV2.CAPTCHA)) {
   Page page = getCurrentPage(resource);
   String configCat = "googleCaptchaV2";
   try {
    textValues.put(FormElementConstantsV2.GOOGLE_CAPTHCHA_KEY,
      configurationService.getConfigValue(page, configCat, FormElementConstantsV2.GOOGLE_CAPTHCHA_KEY));
    textValues.put("languageCode", configurationService.getConfigValue(getCurrentPage(resource), "googleCaptchaV2", "languageCode"));
   } catch (ConfigurationNotFoundException e) {
    LOGGER.error("Caught Exception in getting captchaMap for category googleCaptcha", e);
   }
   if (textValues.get(FormElementConstantsV2.GOOGLE_CAPTHCHA_KEY).equals(StringUtils.EMPTY)
     || null == textValues.get(FormElementConstantsV2.GOOGLE_CAPTHCHA_KEY)) {
    textValues.put(FormElementConstantsV2.GOOGLE_CAPTHCHA_KEY, "6LdSEw0TAAAAAGRKAPeRY-tX511HZAcLgGM4TPsX");
   }
   String name = StringUtils.defaultIfEmpty(GlobalConfigurationUtility.getValueFromConfiguration(configurationService, page, configCat, "name"),
     "captcha");
   textValues.put("name", name);
   
  }
 }
 
 /**
  * Gets the dropdown values.
  * 
  * @param currentResource
  *         the current resource
  * @return the dropdown values
  */
 private List<Map<String, String>> getDropdownValues(Resource currentResource) {
  List<Map<String, String>> dropdownList = new ArrayList<Map<String, String>>();
  
  List<String> dropdownPropertyName = new ArrayList<String>();
  dropdownPropertyName.add("displayText");
  dropdownPropertyName.add(FormElementConstantsV2.VALUE);
  
  LOGGER.debug("Dropdown properties : " + dropdownPropertyName);
  
  List<Map<String, String>> list = ComponentUtil.getNestedMultiFieldProperties(currentResource, "data");
  
  for (Map<String, String> map : list) {
   Map<String, String> dropdownMap = new LinkedHashMap<String, String>();
   dropdownMap.put("displayText", xssapi.encodeForHTML(MapUtils.getString(map, FormElementConstantsV2.DISPLAY_TEXT, StringUtils.EMPTY)));
   dropdownMap.put(FormElementConstantsV2.VALUE, xssapi.encodeForHTML(MapUtils.getString(map, FormElementConstantsV2.VALUE, StringUtils.EMPTY)));
   dropdownList.add(dropdownMap);
  }
  
  LOGGER.debug("Restrictions Properties. : " + dropdownList.size());
  return dropdownList;
 }
 
 /**
  * Handle mandatory field.
  * 
  * @param resource
  *         the resource
  * @param properties
  *         the properties
  * @param textValues
  *         the text values
  * @param rulesList
  *         the rules list
  */
 private void handleMandatoryField(Map<String, Object> properties, List<Map<String, Object>> rulesList) {
  String elementType = (String) properties.get(FormElementConstantsV2.ELEMENT_TYPE);
  Map<String, Object> requiredMap = new HashMap<String, Object>();
  String[] fieldsToInclude = { FormElementConstantsV2.HIDDENFIELD, FormElementConstantsV2.FORMBUTTON, FormElementConstantsV2.CAPTCHA };
  if (!(ArrayUtils.contains(fieldsToInclude, elementType))) {
   Boolean mandatoryField = Boolean
     .parseBoolean(MapUtils.getString(properties, FormElementConstantsV2.VALIDATIONS_MANDATORY_FIELD, FormElementConstantsV2.FALSE));
   requiredMap.put("required", mandatoryField);
   requiredMap.put(FormElementConstantsV2.MSG, getEncodedValue(properties, FormElementConstantsV2.VALIDATIONS_REQUIRED_ERROR_MESSAGE));
   
   rulesList.add(requiredMap);
   if (!elementType.equals(FormElementConstantsV2.COUPON_CODE)) {
    handleconstraintType(properties, rulesList);
   }
  }
 }
 
 /**
  * Handleconstraint type.
  * 
  * @param resource
  *         the resource
  * @param properties
  *         the properties
  * @param rulesList
  *         the rules list
  */
 private void handleconstraintType(Map<String, Object> properties, List<Map<String, Object>> rulesList) {
  String elementType = (String) properties.get(FormElementConstantsV2.ELEMENT_TYPE);
  Map<String, Object> regexMap = new HashMap<String, Object>();
  String constraintType = StringUtils.EMPTY;
  if (MapUtils.getBooleanValue(properties, ViewHelper.IS_CONTAINER)) {
   handleValidationsData(properties, rulesList, elementType, regexMap);
   if (!(FormElementConstantsV2.NUMBER.equals(elementType) || FormElementConstantsV2.DROPDOWN.equals(elementType)
     || FormElementConstantsV2.FILEUPLOAD.equals(elementType))) {
    
    constraintType = getConstraintTYpe(properties, elementType);
    if (constraintType.equals(StringUtils.EMPTY)) {
     regexMap.put(FormElementConstantsV2.PATTERN,
       MapUtils.getString(properties, FormElementConstantsV2.VALIDATIONS_VALIDATION_EXPRESSION, StringUtils.EMPTY));
     
    } else {
     regexMap.put(FormElementConstantsV2.PATTERN, constraintType);
     regexMap.put(FormElementConstantsV2.MSG, getEncodedValue(properties, FormElementConstantsV2.VALIDATIONS_VALIDATION_ERROR_MESSAGE));
     rulesList.add(regexMap);
    }
    int telephoneNumMaximumLength = Integer.parseInt(MapUtils.getString(properties, "telephoneNumMaximumLength", "0"));
    if ("telephonenumber".equals(elementType) && telephoneNumMaximumLength > 0) {
     regexMap.put("maxLength", telephoneNumMaximumLength);
    }
    regexMap.put(FormElementConstantsV2.MSG, getEncodedValue(properties, FormElementConstantsV2.VALIDATIONS_VALIDATION_ERROR_MESSAGE));
   }
   
  }
  if ((constraintType.equals(StringUtils.EMPTY)) && !(FormElementConstantsV2.DATE.equals(elementType))
    && !(FormElementConstantsV2.RADIO.equals(elementType)) && !(FormElementConstantsV2.CHECKBOX.equals(elementType))) {
   rulesList.add(regexMap);
  }
  
 }
 
 /**
  * Handle validations data.
  * 
  * @param resource
  *         the resource
  * @param properties
  *         the properties
  * @param rulesList
  *         the rules list
  * @param elementType
  *         the element type
  * @param regexMap
  *         the regex map
  */
 private void handleValidationsData(Map<String, Object> properties, List<Map<String, Object>> rulesList, String elementType,
   Map<String, Object> regexMap) {
  if ("textfield".equals(elementType) || "password".equals(elementType)) {
   handleMaxLength(properties, regexMap);
  } else if (FormElementConstantsV2.NUMBER.equals(elementType)) {
   handleNumValue(properties, regexMap);
  } else if (FormElementConstantsV2.FILEUPLOAD.equals(elementType)) {
   handleFileuploadValidationFields(properties, regexMap);
  }
  if (MapUtils.isNotEmpty(regexMap)) {
   rulesList.add(regexMap);
  }
 }
 
 private String getConstraintTYpe(Map<String, Object> properties, String elementType) {
  String constraintType = StringUtils.EMPTY;
  if ("emailaddress".equals(elementType)) {
   constraintType = MapUtils.getString(properties, "emailConstraintType", StringUtils.EMPTY);
  } else if ("date".equals(elementType)) {
   constraintType = MapUtils.getString(properties, FormElementConstantsV2.DATE_CONSTRAINT_TYPE, StringUtils.EMPTY);
  } else if ("cardnumber".equals(elementType)) {
   constraintType = MapUtils.getString(properties, FormElementConstantsV2.CARD_NUMBER_CONSTRAINT_TYPE, StringUtils.EMPTY);
  } else if ("telephonenumber".equals(elementType)) {
   constraintType = MapUtils.getString(properties, FormElementConstantsV2.TELEPHONE_NUMBER_CONSTRAINT_TYPE, StringUtils.EMPTY);
  } else {
   constraintType = MapUtils.getString(properties, FormElementConstantsV2.VALIDATIONS_CONSTRAINT_TYPE, StringUtils.EMPTY);
  }
  return constraintType;
 }
 
 /**
  * Handle max length.
  * 
  * @param properties
  *         the properties
  * @param regexMap
  *         the regex map
  */
 private void handleMaxLength(Map<String, Object> properties, Map<String, Object> regexMap) {
  String maxLength = MapUtils.getString(properties, FormElementConstantsV2.TEXT_FIELD_MAX_LENGTH);
  if (maxLength != null && !"0".equals(maxLength)) {
   regexMap.put("maxLength", Integer.parseInt(maxLength));
  }
 }
 
 /**
  * Handle num value.
  * 
  * @param properties
  *         the properties
  * @param regexMap
  *         the regex map
  */
 private void handleNumValue(Map<String, Object> properties, Map<String, Object> regexMap) {
  String numberMaximumValue = MapUtils.getString(properties, "numberMaximumValue");
  if (numberMaximumValue != null && !"0".equals(numberMaximumValue)) {
   regexMap.put("max", Integer.parseInt(numberMaximumValue));
  }
  regexMap.put("min", Integer.parseInt(MapUtils.getString(properties, "numberMinimumValue", "0")));
  regexMap.put(FormElementConstantsV2.MSG, getEncodedValue(properties, FormElementConstantsV2.VALIDATIONS_VALIDATION_ERROR_MESSAGE));
 }
 
 /**
  * Handle date validation fields.
  * 
  * @param properties
  *         the properties
  * @param regexMap
  *         the regex map
  * @param rulesList
  *         the rules list
  * @param resource
  *         the resource
  * @param dateRegex
  */
 private void handleDateValidationFields(Map<String, Object> properties, List<Map<String, Object>> rulesList, String dateRegex) {
  
  Map<String, Object> regexMap = new HashMap<String, Object>();
  if (MapUtils.getBooleanValue(properties, ViewHelper.IS_CONTAINER)) {
   
   regexMap.put(FormElementConstantsV2.PATTERN, dateRegex);
   regexMap.put(FormElementConstantsV2.MSG, getEncodedValue(properties, FormElementConstantsV2.VALIDATIONS_VALIDATION_ERROR_MESSAGE));
   
  }
  rulesList.add(regexMap);
 }
 
 /**
  * Handle fileupload validation fields.
  * 
  * @param properties
  *         the properties
  * @param regexMap
  *         the regex map
  */
 private void handleFileuploadValidationFields(Map<String, Object> properties, Map<String, Object> regexMap) {
  regexMap.put(FormElementConstantsV2.MAX_FILE_SIZE,
    Integer.parseInt(MapUtils.getString(properties, FormElementConstantsV2.FILE_UPLOAD_MAX_SIZE, "0")));
  regexMap.put(FormElementConstantsV2.SUPPORTED_FILE_EXTENSIONS,
    MapUtils.getString(properties, FormElementConstantsV2.FILE_UPLOAD_SUPPORTED_FILE_EXTENSION, StringUtils.EMPTY));
  regexMap.put(FormElementConstantsV2.MSG, getEncodedValue(properties, FormElementConstantsV2.VALIDATIONS_VALIDATION_ERROR_MESSAGE));
 }
 
 /**
  * Gets the encoded value.
  * 
  * @param properties
  *         the properties
  * @param propertyName
  *         the property name
  * @return the encoded value
  */
 public String getEncodedValue(final Map<String, Object> properties, final String propertyName) {
  return MapUtils.getString(properties, propertyName, "");
 }
 
 /**
  * Gets the include interval properties.
  * 
  * @param currentResource
  *         the current resource
  * @return the include interval properties
  */
 private Map<String, Object> getIncludeIntervalProperties(Resource currentResource) {
  List<Map<String, Object>> includeIntervalList = new ArrayList<Map<String, Object>>();
  
  List<String> restrictionPropertyName = new ArrayList<String>();
  restrictionPropertyName.add("dateCondition");
  restrictionPropertyName.add("dateYear");
  restrictionPropertyName.add("dateMonth");
  restrictionPropertyName.add("dateDay");
  
  LOGGER.debug("footer field properties : " + restrictionPropertyName);
  
  List<Map<String, String>> list = ComponentUtil.getNestedMultiFieldProperties(currentResource, "restriction");
  
  for (Map<String, String> map : list) {
   Map<String, Object> intervalRestrictionMap = new LinkedHashMap<String, Object>();
   intervalRestrictionMap.put("condition", xssapi.encodeForHTML(MapUtils.getString(map, FormElementConstantsV2.DATE_CONDITION, StringUtils.EMPTY)));
   String year = StringUtils.isBlank(MapUtils.getString(map, FormElementConstantsV2.DATE_YEAR)) ? "0"
     : xssapi.encodeForHTML(MapUtils.getString(map, FormElementConstantsV2.DATE_YEAR));
   String month = StringUtils.isBlank(MapUtils.getString(map, FormElementConstantsV2.DATE_MONTH, StringUtils.EMPTY)) ? "0"
     : xssapi.encodeForHTML(MapUtils.getString(map, FormElementConstantsV2.DATE_MONTH));
   String day = StringUtils.isBlank(MapUtils.getString(map, FormElementConstantsV2.DATE_DAY, StringUtils.EMPTY)) ? "0"
     : xssapi.encodeForHTML(MapUtils.getString(map, FormElementConstantsV2.DATE_DAY));
   intervalRestrictionMap.put("year", year);
   intervalRestrictionMap.put("month", month);
   intervalRestrictionMap.put("day", day);
   includeIntervalList.add(intervalRestrictionMap);
  }
  Map<String, Object> restrictionProperties = new LinkedHashMap<String, Object>();
  restrictionProperties.put("restrictionsList", includeIntervalList.toArray());
  LOGGER.debug("Restrictions Properties : " + restrictionProperties.size());
  return restrictionProperties;
 }
 
 private List<Map<String, String>> getErrorConfigs(SlingHttpServletRequest slingHttpServletRequest, Page currentPage, ValueMap parentProperties,
   String fieldId) {
  Locale pageLocale = currentPage.getLanguage(false);
  ResourceBundle resourceBundle = slingHttpServletRequest.getResourceBundle(pageLocale);
  UnileverI18nImpl i18nObject = new UnileverI18nImpl(resourceBundle);
  List<Map<String, String>> configMap = new ArrayList<Map<String, String>>();
  String formType = parentProperties.get("formType", StringUtils.EMPTY);
  if (StringUtils.isNotEmpty(fieldId)) {
   String prefix = "formElementV2." + formType + "." + fieldId;
   configMap = i18nObject.getMap(prefix);
  }
  return configMap;
 }
 
 /**
  * Adds the coupon information to the map.
  * 
  * @param properties
  *         the properties
  * @param textValues
  *         the text values
  * @param i18n
  *         the i18n
  */
 private void handleCouponCode(Map<String, Object> properties, Map<String, Object> textValues, I18n i18n) {
  Map<String, Object> couponValidationMsgMap = new LinkedHashMap<String, Object>();
  Map<String, Object> couponErrorCodesMap = new LinkedHashMap<String, Object>();
  String invalidCode = MapUtils.getString(properties, FormElementConstantsV2.INVALID_CODE, StringUtils.EMPTY);
  String codeExpiredMsg = MapUtils.getString(properties, FormElementConstantsV2.CODE_EXPIRED_MSG, StringUtils.EMPTY);
  String globalCountExceedMsg = MapUtils.getString(properties, FormElementConstantsV2.GLOBAL_COUNT_EXCEED_MSG, StringUtils.EMPTY);
  String userCountExceedMsg = MapUtils.getString(properties, FormElementConstantsV2.USER_COUNT_EXCEED_MSG, StringUtils.EMPTY);
  couponValidationMsgMap.put(FormElementConstantsV2.J_INVALID_CODE, invalidCode);
  couponValidationMsgMap.put(FormElementConstantsV2.J_CODE_EXPIRED_MSG, codeExpiredMsg);
  couponValidationMsgMap.put(FormElementConstantsV2.J_GLOBAL_COUNT_EXCEED_MSG, globalCountExceedMsg);
  couponValidationMsgMap.put(FormElementConstantsV2.J_USER_COUNT_EXCEED_MSG, userCountExceedMsg);
  
  textValues.put(FormElementConstantsV2.J_COUPON_VALIDATION_MSG, couponValidationMsgMap);
  
  couponErrorCodesMap.put(FormElementConstantsV2.J_ERROR_CODE_NOT_REDEEMED, i18n.get(FormElementConstantsV2.I18N_ERROR_CODE_NOT_REDEEMED));
  couponErrorCodesMap.put(FormElementConstantsV2.J_ERROR_NO_DOWNSTREAM_SYSTEM, i18n.get(FormElementConstantsV2.I18N_ERROR_NO_DOWNSTREAM_SYSTEM));
  couponErrorCodesMap.put(FormElementConstantsV2.J_ERROR_CAMPGN_ID_NOT_PASSED, i18n.get(FormElementConstantsV2.I18N_ERROR_CAMPGN_ID_NOT_PASSED));
  couponErrorCodesMap.put(FormElementConstantsV2.J_ERROR_NAME_NOT_RETRIEVED, i18n.get(FormElementConstantsV2.I18N_ERROR_NAME_NOT_RETRIEVED));
  couponErrorCodesMap.put(FormElementConstantsV2.J_ERROR_CODE_EXPIRY_MSG, i18n.get(FormElementConstantsV2.I18N_ERROR_CODE_EXPIRY_MSG));
  couponErrorCodesMap.put(FormElementConstantsV2.J_ERROR_INVALID_CODE_MSG, i18n.get(FormElementConstantsV2.I18N_ERROR_INVALID_CODE_MSG));
  couponErrorCodesMap.put(FormElementConstantsV2.J_ERROR_EXCEED_lIMIT_FOR_USER, i18n.get(FormElementConstantsV2.I18N_ERROR_EXCEED_lIMIT_FOR_USER));
  couponErrorCodesMap.put(FormElementConstantsV2.J_ERROR_EXCEED_USAGE_lIMIT_MSG, i18n.get(FormElementConstantsV2.I18N_ERROR_EXCEED_USAGE_lIMIT_MSG));
  
  textValues.put(FormElementConstantsV2.J_COUPON_ERROR_CODE, couponErrorCodesMap);
  
 }
}
