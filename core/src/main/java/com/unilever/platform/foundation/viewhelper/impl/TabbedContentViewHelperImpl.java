/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sapient.platform.iea.aem.core.cache.ComponentViewAndViewHelperCache;
import com.sapient.platform.iea.aem.core.utils.CollectionUtils;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.sapient.platform.iea.aem.utils.RandomNumberUtils;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.TabbedContentHelper;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * This class prepares the data map for Tabbed Control.
 */
@Component(description = "TabbedControlViewHelperImpl", immediate = true, metatype = true, label = "TabbedControlViewHelperImpl")
@Service({ TabbedContentViewHelperImpl.class, ViewHelper.class })
@Properties({
  @org.apache.felix.scr.annotations.Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.TABBED_CONTENT, propertyPrivate = true),
  @org.apache.felix.scr.annotations.Property(name = Constants.SERVICE_RANKING, intValue = { 5000 }, propertyPrivate = true) })
public class TabbedContentViewHelperImpl extends BaseViewHelper {
 private static final String STRING_ONE = "1";
 @Reference
 ComponentViewAndViewHelperCache componentViewAndViewHelperCache;
 private static final String TAB_TITLE = "tabTitle";
 private static final String TAB_LIST = "tabList";
 private static final String RANDOM_NUMBER = "randomNumber";
 private static final String DEFAULT_INDEX = "defaultIndex";
 private static final Logger LOGGER = LoggerFactory.getLogger(TabbedContentViewHelperImpl.class);
 
 public TabbedContentViewHelperImpl() {
 }
 
 @Override
 @SuppressWarnings("unchecked")
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  Map<String, Object> properties = (Map<String, Object>) content.get(PROPERTIES);
  ResourceResolver resourceResolver = getResourceResolver(resources);
  Iterator<Resource> childNodesIterator = resourceResolver.listChildren(resourceResolver.getResource((String) content
    .get("KEY_CURRENT_CONTENT_PAGE_PATH")));
  String[] slideRandomNumbersOrigArray = CollectionUtils.convertObjectArrayToStringArray(properties.get(RANDOM_NUMBER));
  TabbedContentHelper.deleteUnwantedNodes(childNodesIterator, resourceResolver, slideRandomNumbersOrigArray);
  Map<String, Object> tabbedContentMap = new LinkedHashMap<String, Object>();
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  List<String> propertyNames = new ArrayList<String>();
  propertyNames.add(TAB_TITLE);
  propertyNames.add(RANDOM_NUMBER);
  LOGGER.info("After property names");
  LOGGER.debug("After property names");
  
  List<Map<String, String>> primaryList = CollectionUtils.convertMultiWidgetToList(properties, propertyNames);
  
  String[] slideRandomNumbersNewArray = RandomNumberUtils.createRandomNumberArray(primaryList, RANDOM_NUMBER);
  LOGGER.info("After slideRandomNumbersNewArray");
  LOGGER.debug("After slideRandomNumbersNewArray");
  
  RandomNumberUtils.writeRandomNumberToNode((String) content.get("KEY_CURRENT_CONTENT_PAGE_PATH"), slideRandomNumbersOrigArray,
    slideRandomNumbersNewArray, resourceResolver, RANDOM_NUMBER);
  
  LOGGER.info("After writeRandomNumberToNode");
  LOGGER.debug("After writeRandomNumberToNode");
  tabbedContentMap.put(TAB_LIST, primaryList.toArray());
  String defaultIndex = MapUtils.getString(properties, DEFAULT_INDEX, STRING_ONE);
  tabbedContentMap.put(DEFAULT_INDEX, defaultIndex);
  data.put(getJsonNameSpace(content), tabbedContentMap);
  return data;
 }
}
