/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.servlet;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameterMap;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.io.JSONWriter;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.aem.foundation.constants.CommonConstants;

/**
 * The Class SocialChannelServlet.
 */
@SlingServlet(label = "Unilever Social Channel Servlet to Fetch channels list", resourceTypes = { CommonConstants.DEFAULT_SERVLET_NAME }, methods = { HttpConstants.METHOD_GET }, selectors = { "socialchannels" }, extensions = { "json" })
@Properties({
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.foundation.components.SocialChannelServlet", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "Unilever Social Channel Servlet to Fetch channels list", propertyPrivate = false),

})
public class SocialChannelServlet extends SlingAllMethodsServlet {
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(SocialChannelServlet.class);
 
 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = 1L;
 
 /** The Constant SOCIAL_CHANNELS. */
 private static final String SOCIAL_CHANNELS = "socialChannels";
 
 /** The Constant COMPONENT_NAME. */
 private static final String COMPONENT_NAME = "cmp";
 
 /** The Constant SOCIAL_CHANNELS_NAMES. */
 private static final String SOCIAL_CHANNELS_NAMES = "socialChannelNames";
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected final void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws IOException {
  
  String path = request.getRequestPathInfo().getResourcePath();
  ResourceResolver resourceResolver = request.getResourceResolver();
  
  Writer out = response.getWriter();
  JSONWriter writer = new JSONWriter(out);
  
  response.setContentType("application/json");
  response.setCharacterEncoding("UTF-8");
  
  Resource currentResource = resourceResolver.getResource(path);
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  Page page = pageManager.getContainingPage(currentResource);
  
  RequestParameterMap requestParametrs = request.getRequestParameterMap();
  String componentName = requestParametrs.containsKey(COMPONENT_NAME) ? requestParametrs.getValue(COMPONENT_NAME).toString() : StringUtils.EMPTY;
  
  if (page != null) {
   Map<String, String> channelsMap = GlobalConfigurationUtility.getMapFromConfiguration(configurationService, page, SOCIAL_CHANNELS);
   String[] selctiveChannels = getSelectiveChannels(componentName, page);
   
   try {
    writer.array();
    if (!ArrayUtils.isEmpty(selctiveChannels)) {
     addSelectiveChannels(channelsMap, selctiveChannels, writer);
    } else {
     addAllChannels(channelsMap, writer);
    }
    writer.endArray();
   } catch (JSONException e) {
    LOGGER.error("RepositoryException occurred ", e);
   }
  }
 }
 
 /**
  * Adds the writer obj.
  * 
  * @param writer
  *         the writer
  * @param key
  *         the key
  * @param value
  *         the value
  * @throws JSONException
  *          the JSON exception
  */
 private void addWriterObj(JSONWriter writer, String key, String value) throws JSONException {
  writer.object();
  writer.key("text").value(key.trim());
  writer.key("value").value(value.trim());
  writer.endObject();
 }
 
 /**
  * Adds the all channels.
  * 
  * @param channelsMap
  *         the channels map
  * @param writer
  *         the writer
  * @throws JSONException
  *          the JSON exception
  */
 private void addAllChannels(Map<String, String> channelsMap, JSONWriter writer) throws JSONException {
  for (String key : channelsMap.keySet()) {
   addWriterObj(writer, key, channelsMap.get(key));
  }
 }
 
 /**
  * Adds the selective channels.
  * 
  * @param channelsMap
  *         the channels map
  * @param includeChannelsList
  *         the include channels list
  * @param writer
  *         the writer
  * @throws JSONException
  *          the JSON exception
  */
 private void addSelectiveChannels(Map<String, String> channelsMap, String[] includeChannelsList, JSONWriter writer) throws JSONException {
  for (String channelName : includeChannelsList) {
   if (channelsMap.containsKey(channelName.trim())) {
    addWriterObj(writer, channelName, channelsMap.get(channelName.trim()));
   }
  }
 }
 
 /**
  * Gets the selective channels.
  * 
  * @param componentName
  *         the component name
  * @param page
  *         the page
  * @return the selective channels
  */
 private String[] getSelectiveChannels(String componentName, Page page) {
  String[] selctiveChannels = ArrayUtils.EMPTY_STRING_ARRAY;
  if (!StringUtils.isBlank(componentName)) {
   String channelsName = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, page, componentName, SOCIAL_CHANNELS_NAMES);
   if (!StringUtils.isBlank(channelsName)) {
    selctiveChannels = channelsName.split(",");
   }
  }
  return selctiveChannels;
 }
}
