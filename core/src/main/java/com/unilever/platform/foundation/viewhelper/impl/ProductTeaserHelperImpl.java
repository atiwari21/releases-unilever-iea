/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.RepositoryException;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * The Class ProductTeaserHelperImpl.
 */
@Component(description = "ProductTeaserHelperImpl", immediate = true, metatype = true, label = "ProductTeaserHelperImpl")
@Service(value = { ProductTeaserHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.PRODUCT_TEASER, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class ProductTeaserHelperImpl extends BaseViewHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ProductTeaserHelperImpl.class);
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  LOGGER.debug("Executing ProductTeaserHelperImpl.java. Inside processData method");
  Map<String, Object> data = new HashMap<String, Object>();
  SlingHttpServletRequest slingRequest = getSlingRequest(resources);
  Page currentPage = getCurrentPage(resources);
  String jsonNameSpace = getJsonNameSpace(content);
  Map<String, Object> productTeaserMap = new LinkedHashMap<String, Object>();
  Map<String, Object> productProperties = new LinkedHashMap<String, Object>();
  
  UNIProduct product = ProductHelper.getUniProduct(currentPage, slingRequest);
  if (product != null) {
   productProperties = ProductHelper.getProductMap(product, jsonNameSpace, resources, currentPage);
   productProperties.put(ProductConstants.EAN_SELECTOR, product.getSKU());
   ResourceResolver resolver = slingRequest.getResourceResolver();
   QueryBuilder builder = resolver.adaptTo(QueryBuilder.class);
   Map<String, String> predicate = new HashMap<String, String>();
   predicate.put("path", "/content");
   predicate.put("property", "productData");
   predicate.put("property.value", product.getPath());
   Query query = builder.createQuery(PredicateGroup.create(predicate), resolver.adaptTo(javax.jcr.Session.class));
   List<Hit> hits = query.getResult().getHits();
   if (!hits.isEmpty()) {
    try {
     String a = hits.get(0).getPath();
     Page page = resolver.adaptTo(PageManager.class).getContainingPage(a);
     productProperties.put(ProductConstants.URL, ComponentUtil.getFullURL(resolver, page.getPath(), slingRequest));
    } catch (RepositoryException e) {
     LOGGER.error("Error while fetching product page path", e);
    }
   }
   
  } else {
   LOGGER.debug("UniProduct obj is null in getting from ProductHelperExt.findProduct");
  }
  productTeaserMap.put(UnileverConstants.PRODUCT, MapUtils.isNotEmpty(productProperties) ? productProperties : StringUtils.EMPTY);
  data.put(jsonNameSpace, productTeaserMap);
  return data;
 }
}
