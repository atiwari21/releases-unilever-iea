/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfiguration;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.components.helper.StoreLocatorUtility;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;
import com.unilever.platform.foundation.viewhelper.constants.StoreLocatorConstants;

/**
 * The Class ProductCrossellViewHelperImpl.
 */
@Component(description = "BuyInStoreSearchResultsViewHelperImpl", immediate = true, metatype = true, label = "BuyInStoreSearchResultsViewHelperImpl")
@Service(value = { BuyInStoreSearchResultsViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.STORE_SEARCH_RESULTS, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class BuyInStoreSearchResultsViewHelperImpl extends BaseViewHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(BuyInStoreSearchResultsViewHelperImpl.class);
 
 /*
  * reference for site configuration service
  */
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 @Reference
 GlobalConfiguration globalConfiguration;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  LOGGER.debug("Executing StoreSearchResultsViewHelperImpl.java. Inside processData method");
  Map<String, Object> data = new HashMap<String, Object>();
  Map<String, Object> properties = getProperties(content);
  Page currentPage = getCurrentPage(resources);
  String jsonNameSpace = getJsonNameSpace(content);
  Map<String, Object> storeSearchResultsMap = new HashMap<String, Object>();
  I18n i18n = getI18n(resources);
  ResourceResolver resourceResolver = getResourceResolver(resources);
  
  String mapItLabel = i18n.get(StoreLocatorConstants.MAP_IT_KEY);
  storeSearchResultsMap.put("mapItLabel", mapItLabel);
  
  storeSearchResultsMap.putAll(getDialogProperties(properties));
  
  storeSearchResultsMap.put("buyInStoreData", getStoreSearchResultMap(i18n, properties, currentPage, jsonNameSpace, resourceResolver));
  storeSearchResultsMap.put(ProductConstants.REVIEW_MAP, ProductHelper.getReviewMapProperties(configurationService, getCurrentPage(resources)));
  
  UNIProduct uniProduct = ProductHelper.getUniProduct(currentPage, getSlingRequest(resources));
  if (uniProduct != null) {
   Map<String, Object> productProperties = ProductHelper.getProductMap(uniProduct, jsonNameSpace, resources, currentPage);
   storeSearchResultsMap.put(ProductConstants.PRODUCT_MAP, MapUtils.isNotEmpty(productProperties) ? productProperties : StringUtils.EMPTY);
   storeSearchResultsMap.put(ProductConstants.SHOPNOW_MAP,
     ProductHelper.getshopNowProperties(configurationService, currentPage, i18n, getResourceResolver(resources), getSlingRequest(resources)));
  } else {
   LOGGER.debug("UniProduct obj is null in getting from ProductHelperExt.findProduct");
  }
  
  data.put(jsonNameSpace, storeSearchResultsMap);
  return data;
 }
 
 /**
  * Gets the store search result map.
  * 
  * @param i18n
  *         the i18n
  * @param properties
  *         the properties
  * @param currentPage
  *         the current page
  * @param jsonNameSpace
  *         the json name space
  * @param resourceResolver
  * @return the store search result map
  */
 private Map<String, Object> getStoreSearchResultMap(I18n i18n, Map<String, Object> properties, Page currentPage, String jsonNameSpace,
   ResourceResolver resourceResolver) {
  Map<String, Object> storeSearchResultsMap = new HashMap<String, Object>();
  storeSearchResultsMap.put(UnileverConstants.TITLE, StringUtils.isNotBlank(MapUtils.getString(properties, UnileverConstants.TITLE))
    ? MapUtils.getString(properties, UnileverConstants.TITLE) : StringUtils.EMPTY);
  String noOfStores = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, StoreLocatorConstants.GLOBAL_CONFIG_CAT,
    StoreLocatorConstants.NO_OF_STORES);
  
  String loadMoreDisplayCount = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
    StoreLocatorConstants.GLOBAL_CONFIG_CAT, StoreLocatorConstants.LOAD_MORE_DISPLAY_COUNT);
  
  storeSearchResultsMap.put(StoreLocatorConstants.NO_OF_STORES,
    StringUtils.isNotBlank(noOfStores) ? Integer.parseInt(noOfStores) : StoreLocatorConstants.HUNDRED);
  
  storeSearchResultsMap.put(StoreLocatorConstants.LOAD_MORE_DISPLAY_COUNT,
    StringUtils.isNotBlank(loadMoreDisplayCount) ? Integer.parseInt(loadMoreDisplayCount) : StoreLocatorConstants.TEN);
  
  storeSearchResultsMap.put(StoreLocatorConstants.SEARCH_DESCRIPTION, i18n.get(StoreLocatorConstants.STORE_SEARCH_RESULTS_SEARCH_DESCRIPTION));
  storeSearchResultsMap.put(StoreLocatorConstants.ANOTHER_PRODUCT_LABEL, i18n.get(StoreLocatorConstants.STORE_SEARCH_RESULTS_ANOTHER_PRODUCT_LABEL));
  storeSearchResultsMap.put(StoreLocatorConstants.SELECTED_PRODUCT_LABEL,
    i18n.get(StoreLocatorConstants.STORE_SEARCH_RESULTS_SELECTED_PRODUCT_LABEL));
  storeSearchResultsMap.put(StoreLocatorConstants.BUY_ONLINE_CTA, i18n.get(StoreLocatorConstants.STORE_SEARCH_RESULTS_BUY_ONLINE_CTA));
  storeSearchResultsMap.put(StoreLocatorConstants.SHOW_ME_STORES_CTA, i18n.get(StoreLocatorConstants.STORE_SEARCH_RESULTS_SHOW_ME_STORES_CTA));
  storeSearchResultsMap.put(StoreLocatorConstants.LOAD_MORE_CTA, i18n.get(StoreLocatorConstants.STORE_SEARCH_RESULTS_LOAD_MORE_CTA));
  storeSearchResultsMap.put(StoreLocatorConstants.FIELDS, StoreLocatorUtility.getFieldsMap(currentPage, configurationService, i18n, jsonNameSpace));
  storeSearchResultsMap.put(StoreLocatorConstants.DEFAULT_RADIOUS_FOR_SEARCH, GlobalConfigurationUtility.getValueFromConfiguration(
    configurationService, currentPage, StoreLocatorConstants.GLOBAL_CONFIG_CAT, StoreLocatorConstants.DEFAULT_RADIOUS_FOR_SEARCH));
  
  String storeLocatorServiceURL = globalConfiguration.getConfigValue(StoreLocatorConstants.SERVICE_URL_KEY);
  
  storeSearchResultsMap.put(StoreLocatorConstants.SERVICE_URL, resourceResolver.map(storeLocatorServiceURL));
  storeSearchResultsMap.put(StoreLocatorConstants.TABLE_HEADING, getTableHeading(i18n));
  storeSearchResultsMap.put(StoreLocatorConstants.PRINT_LABEL, i18n.get(StoreLocatorConstants.STORE_SEARCH_RESULTS_PRINT_LABEL));
  storeSearchResultsMap.put(StoreLocatorConstants.SHARE_LOCATION, i18n.get(StoreLocatorConstants.STORE_SEARCH_RESULTS_SHARE_LOCATION));
  storeSearchResultsMap.put(StoreLocatorConstants.MAP_API, GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
    StoreLocatorConstants.GLOBAL_CONFIG_CAT, StoreLocatorConstants.MAP_API));
  storeSearchResultsMap.put(StoreLocatorConstants.GEO_CODE_API, GlobalConfigurationUtility.getValueFromConfiguration(configurationService,
    currentPage, StoreLocatorConstants.GLOBAL_CONFIG_CAT, StoreLocatorConstants.GEO_CODE_API));
  return storeSearchResultsMap;
 }
 
 /**
  * Gets the table heading.
  * 
  * @param i18n
  *         the i18n
  * @return the table heading
  */
 private Map<String, Object> getTableHeading(I18n i18n) {
  Map<String, Object> tableHeadingMap = new HashMap<String, Object>();
  tableHeadingMap.put(StoreLocatorConstants.COLUMN_ONE, i18n.get(StoreLocatorConstants.STORE_SEARCH_RESULTS_COLUMN_ONE));
  tableHeadingMap.put(StoreLocatorConstants.COLUMN_TWO, i18n.get(StoreLocatorConstants.STORE_SEARCH_RESULTS_COLUMN_TWO));
  tableHeadingMap.put(StoreLocatorConstants.COLUMN_THREE, i18n.get(StoreLocatorConstants.STORE_SEARCH_RESULTS_COLUMN_THREE));
  tableHeadingMap.put(StoreLocatorConstants.COLUMN_FOUR, i18n.get(StoreLocatorConstants.STORE_SEARCH_RESULTS_COLUMN_FOUR));
  return tableHeadingMap;
 }
 
 private Map<String, Object> getDialogProperties(Map<String, Object> properties) {
  Map<String, Object> map = new HashMap<String, Object>();
  String title = MapUtils.getString(properties, "title", "");
  
  String viewMapCtaLabel = MapUtils.getString(properties, "viewMapCtaLabel", "");
  
  String buyOnlineTitleText = MapUtils.getString(properties, "buyOnlineTitleText", "");
  
  String noResultTitle = MapUtils.getString(properties, "noResultTitle", "");
  String noResultCopy = MapUtils.getString(properties, "noResultCopy", "");
  
  map.put("title", title);
  map.put("viewMapCtaLabel", viewMapCtaLabel);
  map.put("buyOnlineTitleText", buyOnlineTitleText);
  map.put("noResultTitle", noResultTitle);
  map.put("noResultCopy", noResultCopy);
  return map;
  
 }
}
