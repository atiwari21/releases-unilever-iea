/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.constants.UnileverConstants;

/**
*
 */
@Component(description = "EndViewHelperImpl", immediate = true, metatype = true, label = "End Form Component")
@Service(value = { EndViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.END, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class EndViewHelperImpl extends BaseViewHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(FeaturedTweetViewHelperImpl.class);
 
 /** The Constant RESET_NAME. */
 private static final String RESET_NAME = "resetName";
 
 /** The Constant NAME. */
 private static final String NAME = "name";
 
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  
  LOGGER.info("End View Helper #processData called for processing fixed list.");
  
  Map<String, Object> endFormMap = new LinkedHashMap<String, Object>();
  Map<String, Object> properties = getProperties(content);
  
  String jsonNameSpace = getJsonNameSpace(content);
  String name = MapUtils.getString(properties, NAME);
  String resetName = MapUtils.getString(properties, RESET_NAME);
  String randomNumber = MapUtils.getString(properties, UnileverConstants.RANDOM_NUMBER);
  
  endFormMap.put(RESET_NAME, resetName);
  endFormMap.put(NAME, name);
  
  endFormMap.put(UnileverConstants.RANDOM_NUMBER, randomNumber);
  
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  data.put(jsonNameSpace, endFormMap);
  
  return data;
 }
 
}
