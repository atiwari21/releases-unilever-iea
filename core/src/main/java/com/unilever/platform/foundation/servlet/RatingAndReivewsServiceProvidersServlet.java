/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.servlet;

import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

import javax.jcr.RepositoryException;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.io.JSONWriter;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.aem.foundation.constants.CommonConstants;
import com.unilever.platform.aem.foundation.core.dto.BrandMarketBean;
import com.unilever.platform.foundation.viewhelper.constants.ArticleConstants;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * The Class RatingAndReivewsServiceProvidersServlet.
 */
@SlingServlet(label = "Rating and Reviews Service Providers Servlet to Fetch multiple service providers list form config", resourceTypes = { CommonConstants.DEFAULT_SERVLET_NAME }, methods = { HttpConstants.METHOD_GET }, selectors = { "ratingReviewsServiceProviders" }, extensions = { "json" })
@Properties({
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.foundation.servlet.RatingAndReivewsServiceProvidersServlet", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "Rating and Reviews Service Providers Servlet to Fetch multiple service providers list form config", propertyPrivate = false),

})
public class RatingAndReivewsServiceProvidersServlet extends SlingAllMethodsServlet {
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(RatingAndReivewsServiceProvidersServlet.class);
 
 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = 1L;
 private static final String RESOURCE_TYPE = "/components/content/siteConfig";
 private static final String CATEGORY = "category";
 private static final String CONTENT_TYPE = "contentType";
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache. sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected final void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws IOException {
  
  String path = request.getRequestPathInfo().getResourcePath();
  ResourceResolver resourceResolver = request.getResourceResolver();
  
  Writer out = response.getWriter();
  JSONWriter writer = new JSONWriter(out);
  
  response.setContentType("application/json");
  response.setCharacterEncoding("UTF-8");
  
  Resource currentResource = resourceResolver.getResource(path);
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  Page page = pageManager.getContainingPage(currentResource);
  BrandMarketBean bm = new BrandMarketBean(page);
  String configPagePath = StringUtils.EMPTY;
  String brandName = bm.getBrand();
  String market = bm.getMarket();
  if (brandName != null && market != null) {
   configPagePath = "/content/" + brandName + "/" + market + "/config";
  } else if (brandName != null && (market == null || market.isEmpty())) {
   configPagePath = "/content/" + brandName + "/global/config";
  }
  Page configPage = pageManager.getPage(configPagePath);
  String serviceProviderName = StringUtils.EMPTY;
  if (page != null && configPage != null) {
   String availableContentType = StringUtils.EMPTY;
   try {
    availableContentType = configurationService.getConfigValue(page, ArticleConstants.ARTICLE_CONFIG_CAT, ArticleConstants.ARTICLE_CONTENT_TYPE);
   } catch (ConfigurationNotFoundException e) {
    LOGGER.error("articleContentTypes not found in global configurations", e);
   }
   String[] availableContentTypes = availableContentType.split(",");
   List<String> availableContentTypesList = Arrays.asList(availableContentTypes);
   String contentType = (String) page.getProperties().get(CONTENT_TYPE);
   if (availableContentTypesList.contains(contentType)) {
    serviceProviderName = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, page,
      ArticleConstants.ARTICLE_RATING_AND_REVIEWS, ArticleConstants.SERVICE_PROVIDER_NAME);
   } else {
    serviceProviderName = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, page, ProductConstants.RATING_AND_REVIEWS,
      ProductConstants.SERVICE_PROVIDER_NAME);
   }
   Resource configResource = resourceResolver.getResource(configPage.getPath());
   List<String> serviceProvidersList = new ArrayList<String>();
   Map<String, String> configNodeMap = new LinkedHashMap<String, String>();
   setNestedChildResourceList(configResource, configNodeMap);
   try {
    setServiceProvidersList(configNodeMap, resourceResolver, serviceProvidersList, serviceProviderName);
    writer.array();
    addAllBazaarvoiceTypes(serviceProvidersList, writer);
    writer.endArray();
   } catch (JSONException e) {
    LOGGER.error("RepositoryException occurred ", e);
   } catch (RepositoryException e) {
    LOGGER.error("RepositoryException occurred ", e);
   }
  }
 }
 
 /**
  * Adds the writer obj.
  * 
  * @param writer
  *         the writer
  * @param value
  *         the value
  * @throws JSONException
  *          the JSON exception
  */
 private void addWriterObj(JSONWriter writer, String value) throws JSONException {
  writer.object();
  writer.key("text").value(value.trim());
  writer.key("value").value(value.trim());
  writer.endObject();
 }
 
 /**
  * Adds the all Service Providers.
  * 
  * @param channelsMap
  *         the channels map
  * @param writer
  *         the writer
  * @throws JSONException
  *          the JSON exception
  */
 private void addAllBazaarvoiceTypes(List<String> serviceProvidersList, JSONWriter writer) throws JSONException {
  if (!serviceProvidersList.isEmpty()) {
   Iterator<String> iter = serviceProvidersList.iterator();
   while (iter.hasNext()) {
    String result = iter.next();
    addWriterObj(writer, result);
   }
  }
 }
 
 /**
  * set setNestedChildResourceList.
  * 
  * @param Resource
  *         the res
  * @param configNodeMap
  *         the configNodeMap
  */
 private void setNestedChildResourceList(Resource res, Map<String, String> configNodeMap) {
  LOGGER.debug("inside setNestedChildResourceList of ratingAndReivewsServiceProvidersServlet");
  Iterator<Resource> itr = res.listChildren();
  while (itr.hasNext()) {
   Resource childRes = itr.next();
   String childResourceType = childRes.getResourceType();
   LOGGER.debug("Resource Type of current iterating resource : " + RESOURCE_TYPE);
   if (childResourceType.endsWith(RESOURCE_TYPE) && childRes.getValueMap().containsKey(CATEGORY)) {
    configNodeMap.put(childRes.getPath(), childRes.getResourceType());
   }
   setNestedChildResourceList(childRes, configNodeMap);
  }
 }
 
 /**
  * Set setServiceProvidersList.
  * 
  * @param map
  * @param resourceResolver
  * @param serviceProvidersList
  * @param serviceProviderName
  * 
  * @throws RepositoryException
  *          the repository exception
  */
 private void setServiceProvidersList(Map<String, String> map, ResourceResolver resourceResolver, List<String> serviceProvidersList,
   String serviceProviderName) throws RepositoryException {
  LOGGER.debug("inside setServiceProvidersList of ratingAndReivewsServiceProvidersServlet");
  Iterator<String> itr = map.keySet().iterator();
  while (itr.hasNext()) {
   String nodePath = itr.next();
   String resType = map.get(nodePath);
   Resource resource = resourceResolver.getResource(nodePath);
   if (resource != null && resType.endsWith(RESOURCE_TYPE) && resource.getValueMap().containsKey(CATEGORY)) {
     String categoryValue = resource.getValueMap().get(CATEGORY).toString();
     if (categoryValue.startsWith(serviceProviderName)) {
      serviceProvidersList.add(resource.getValueMap().get(CATEGORY).toString());
    }
   }
  }
 }
 
}
