/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.emailtemplate.config;

/**
 * The Interface EmailTemplateConfigService.
 */
public interface EmailTemplateConfigService {
 
 /**
  * Gets the email templates.
  * 
  * @return the email templates
  */
 public String[] getEmailTemplates();
}
