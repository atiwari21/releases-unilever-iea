/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * This class prepares the data map for Call out button Component It reads the values from the dialog and passes the info as a json.
 */

@Component(description = "CallOutButton", immediate = true, metatype = true, label = "Call Out Button")
@Service(value = { CallOutButtonViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.CALL_OUT_BUTTON, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class CallOutButtonViewHelperImpl extends BaseViewHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(CallOutButtonViewHelperImpl.class);
 
 @Reference
 ConfigurationService configurationService;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @SuppressWarnings("unchecked")
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("Executing CallOutButtonViewHelperImpl. Inside processData method ");
  Map<String, Object> properties = new HashMap<String, Object>();
  SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) resources.get(ProductConstants.REQUEST);
  Map<String, Object> callOutButtonProperties = (Map<String, Object>) content.get(PROPERTIES);
  Resource currentResource = slingRequest.getResource();
  String url = StringUtils.EMPTY;
  String ctaText = StringUtils.EMPTY;
  String shortCopy = StringUtils.EMPTY;
  boolean urlOpenInNewWindow = false;
  if (currentResource != null) {
   LOGGER.info("Component found for current page {}", currentResource.getName());
   url = callOutButtonProperties.get("cobUrl") == null ? StringUtils.EMPTY : callOutButtonProperties.get("cobUrl").toString();
   if (url != null && !url.isEmpty()) {
    urlOpenInNewWindow = MapUtils.getBooleanValue(callOutButtonProperties, "urlOpenInNewWindow", false);
    ctaText = callOutButtonProperties.get("cobCtaText") == null ? StringUtils.EMPTY : callOutButtonProperties.get("cobCtaText").toString();
    if (ctaText != null && !ctaText.isEmpty()) {
     properties.put("url", ComponentUtil.getFullURL(getResourceResolver(resources), url, getSlingRequest(resources)));
     properties.put(UnileverConstants.OPEN_IN_NEW_WINDOW, urlOpenInNewWindow);
     properties.put("ctaText", ctaText);
     shortCopy = callOutButtonProperties.get("cobShortCopy") == null ? StringUtils.EMPTY : callOutButtonProperties.get("cobShortCopy").toString();
     properties.put("shortCopy", shortCopy);
    } else {
     assignEmptyMap(properties, url, ctaText, shortCopy, urlOpenInNewWindow);
    }
    
   } else {
    assignEmptyMap(properties, url, ctaText, shortCopy, urlOpenInNewWindow);
   }
   
  } else {
   LOGGER.info("Component cannot be found for current page");
  }
  
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE), properties);
  return data;
  
 }
 
 private void assignEmptyMap(Map<String, Object> properties, String url, String ctaText, String shortCopy, boolean openInNewWindow) {
  properties.put("url", url);
  properties.put(UnileverConstants.OPEN_IN_NEW_WINDOW, openInNewWindow);
  properties.put("ctaText", ctaText);
  properties.put("shortCopy", shortCopy);
 }
 
}
