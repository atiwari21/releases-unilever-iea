/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.http.osgi.services.HttpClientBuilderFactory;
import org.apache.sling.api.SlingConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.RMSJson;
import com.unilever.platform.foundation.components.helper.RecipeHelper;
import com.unilever.platform.foundation.template.dto.RecipeConfigDTO;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * The Class RecipeAttributesViewHelperImpl
 * 
 */
@Component(description = "RecipeAttributesViewHelperImpl", immediate = true, metatype = true, label = "Recipe Attributes Component")
@Service(value = { RecipeAttributesViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.RECIPE_ATTRIBUTES, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class RecipeAttributesViewHelperImpl extends BaseViewHelper {
 
 /** The Constant DATA_DISPLAY_LABEL_POSTFIX. */
 private static final String DATA_DISPLAY_LABEL_POSTFIX = "DataDisplayLabel";
 
 /** The Constant RECIPE_DIETARY_ATTRIBUTES_RECIPE_PREFIX. */
 private static final String RECIPE_ATTRIBUTES_RECIPE_PREFIX = "recipeAttributes.recipe";
 
 /** The Constant RECIPE_ATTRIBUTES. */
 private static final String RECIPE_ATTRIBUTES = "recipeAttributes";
 
 /** The Constant RECIPE_ATTRIBUTES_LIST. */
 private static final String RECIPE_ATTRIBUTES_LIST = "recipeAttributesList";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(FeaturedInstagramPostViewHelperImpl.class);
 
 @Reference
 HttpClientBuilderFactory factory;
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  
  LOGGER.info("Recipe Attributes View Helper #processData called for processing fixed list.");
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  Map<String, Object> recipesMap = new LinkedHashMap<String, Object>();
  
  I18n i18n = getI18n(resources);
  
  Page currentPage = getCurrentPage(resources);
  String jsonNameSpace = getJsonNameSpace(content);
  
  Map<String, Object> dialogProperties = getProperties(content);
  Map<String, String> globalConfigMap = ComponentUtil.getConfigMap(currentPage, RECIPE_ATTRIBUTES_LIST);
  
  RecipeConfigDTO recipeConfigDTO = RecipeHelper.getRecipeConfigurations(configurationService, currentPage, jsonNameSpace);
  
  List<Map<String, Object>> attributeList = RecipeHelper.getAttributesDetails(i18n, dialogProperties, globalConfigMap,
    RECIPE_ATTRIBUTES_RECIPE_PREFIX, DATA_DISPLAY_LABEL_POSTFIX);
  
  RMSJson.getRecipeJsonFromPage(resources, dialogProperties, recipesMap, recipeConfigDTO, factory);
  RMSJson.setRMSServiceDownKey(resources, recipesMap, configurationService, i18n);
  recipesMap.put(RECIPE_ATTRIBUTES, attributeList.toArray());
  data.put(jsonNameSpace, recipesMap);
  return data;
 }
}
