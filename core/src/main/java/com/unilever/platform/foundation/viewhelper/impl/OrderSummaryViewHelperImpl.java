/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.foundation.viewhelper.impl;

import java.lang.annotation.Inherited;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;

import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.EGiftingHelper;
import com.unilever.platform.foundation.components.helper.ProductHelperExt;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * The class OrderSummaryViewHelperImpl
 */
@Component(description = "OrderSummaryViewHelperImpl", immediate = true, metatype = true, label = "OrderSummaryViewHelperImpl")
@Service(value = { OrderSummaryViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.ORDER_SUMMARY, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class OrderSummaryViewHelperImpl extends BaseViewHelper {
 
 /** Logger constant */
 private static final Logger LOGGER = org.slf4j.LoggerFactory.getLogger(OrderSummaryViewHelperImpl.class);
 
 /** Configuration Service Reference */
 @Reference
 ConfigurationService configurationService;
 
 /**
  * {@link Inherited}
  * 
  * @param content
  *         , - {@link Map} of all user input received as input <code>ViewHelper.onGetData</code> method
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return {@link Map} orderSummaryMap holding data.
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.info("Order Summary View Helper #processData called for processing fixed list.");
  String jsonNameSpace = MapUtils.getString(content, "jsonNameSpace");
  Map<String, Object> orderSummaryMap = new LinkedHashMap<String, Object>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  Map<String, Object> orderSummary = EGiftingHelper.getOrderSummaryMap(getI18n(resources));
  orderSummary.putAll(ProductHelperExt.getDeliveryDetailMap(getCurrentPage(resources)));
  orderSummaryMap.put(jsonNameSpace, orderSummary);
  
  return orderSummaryMap;
 }
 
}
