/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.template.dto;

/**
 * The Class MetaTag.
 */
public class MetaTag {
 
 /** The name. */
 private String name;
 
 /** The content. */
 private String content;
 
 /**
  * Gets the name.
  * 
  * @return the name
  */
 public String getName() {
  return name;
 }
 
 /**
  * Sets the name.
  * 
  * @param name
  *         the new name
  */
 public void setName(String name) {
  this.name = name;
 }
 
 /**
  * Gets the content.
  * 
  * @return the content
  */
 public String getContent() {
  return content;
 }
 
 /**
  * Sets the content.
  * 
  * @param content
  *         the new content
  */
 public void setContent(String content) {
  this.content = content;
 }
}
