/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.i18n.I18n;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.utils.SlingUtils;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.GlobalConfiguration;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * The Class FormV2ViewHelperImpl.
 */
@Component(description = "FormV2ViewHelperImpl", immediate = true, metatype = true, label = "FormV2ViewHelperImpl")
@Service(value = { FormV2ViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.FORM_V2, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class FormV2ViewHelperImpl extends BaseViewHelper {
 
 private static final String FORM_V2_NO_RESULT_MSG = "formV2.noResultMsg";
 
 /** The form element view helper impl V 2. */
 @Reference
 ViewHelper formElementViewHelperImplV2;
 
 /** The global configuration. */
 @Reference
 GlobalConfiguration globalConfiguration;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(FormV2ViewHelperImpl.class);
 
 /** The Constant IS_AJAX. */
 private static final String IS_AJAX = "isAjax";
 
 /** The Constant IS_CONTAINER. */
 private static final String IS_CONTAINER = "isContainer";
 
 /** The Constant DISPLAY_MESSAGE. */
 private static final String DISPLAY_MESSAGE = "display message";
 
 /** The Constant REDIRECT_TO. */
 private static final String REDIRECT_TO = "redirect to";
 
 /** The Constant DISPLAY_FORM_ON_DESKTOP. */
 private static final String DISPLAY_FORM_ON_DESKTOP = "displayFormOnDesktop";
 
 /** The Constant DISPLAY_FORM_ON_MOBILE. */
 private static final String DISPLAY_FORM_ON_MOBILE = "displayFormOnMobile";
 
 /** The Constant CODE. */
 private static final String CODE = "code";
 
 /** The Constant CAPTCHA_ERROR_I18N_KEY. */
 private static final String CAPTCHA_ERROR_I18N_KEY = "formElementV2Error.40";
 
 /** The Constant ERROR_I18N_KEY_INITIALS. */
 private static final String ERROR_I18N_KEY_INITIALS = "formElementV2Error";
 
 /** The Constant MESSAGE. */
 private static final String MESSAGE = "message";
 
 /** The Constant SIGNUP_READURL. */
 private static final String SIGNUP_READURL = "signUp.readurl";
 
 /** The Constant SIGNUP_WRITEURL. */
 private static final String SIGNUP_WRITEURL = "signUp.writeUrl";
 
 /** The Constant CONTACTUS_READURL. */
 private static final String CONTACTUS_READURL = "contactUs.readUrl";
 
 /** The Constant CONTACTUS_WRITEURL. */
 private static final String CONTACTUS_WRITEURL = "contactUs.writeUrl";
 
 /** The Constant REGISTRATION_FORM_READURL. */
 private static final String REGISTRATION_FORM_READURL = "registration.form.readUrl";
 
 /** The Constant REGISTRATION_FORM_WRITEURL. */
 private static final String REGISTRATION_FORM_WRITEURL = "registration.form.writeUrl";
 
 /** The Constant REGISTRATION_VALIDATE_READURL. */
 private static final String REGISTRATION_VALIDATE_READURL = "registration.validate.readUrl";
 
 /** The Constant REGISTRATION_VALIDATE_WRITEURL. */
 private static final String REGISTRATION_VALIDATE_WRITEURL = "registration.validate.writeUrl";
 
 /** The Constant REGISTRATION_LOGIN_READURL. */
 private static final String REGISTRATION_LOGIN_READURL = "registration.login.readUrl";
 
 /** The Constant REGISTRATION_LOGIN_WRITEURL. */
 private static final String REGISTRATION_LOGIN_WRITEURL = "registration.login.writeUrl";
 
 /** The Constant REGISTRATION_USERCREATE_READURL. */
 private static final String REGISTRATION_USERCREATE_READURL = "registration.usercreate.readUrl";
 
 /** The Constant REGISTRATION_USERCREATE_WRITEURL. */
 private static final String REGISTRATION_USERCREATE_WRITEURL = "registration.usercreate.writeUrl";
 
 /** The Constant CUSTOM. */
 private static final String CUSTOM = "custom";
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @SuppressWarnings("unchecked")
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  Map<String, Object> properties = getProperties(content);
  
  SlingHttpServletRequest request = getSlingRequest(resources);
  ResourceResolver resourceResolver = getResourceResolver(resources);
  String currentPath = request.getResource().getPath();
  
  Map<String, Object> formV2Data = new LinkedHashMap<String, Object>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  
  String formSubmissionType = MapUtils.getString(properties, "onSuccessfulFormSubmission");
  String successMessage = MapUtils.getString(properties, "successMessage", StringUtils.EMPTY);
  String failureMessage = MapUtils.getString(properties, "failureMessage", StringUtils.EMPTY);
  String redirectPath = MapUtils.getString(properties, "redirectPage", StringUtils.EMPTY);
  String formId = MapUtils.getString(properties, "formIdentifier", StringUtils.EMPTY);
  String servletPath = MapUtils.getString(properties, "servletPath", StringUtils.EMPTY);
  String formType = MapUtils.getString(properties, "formType", StringUtils.EMPTY);
  String aemCampaignId = MapUtils.getString(properties, "aemCampaignId", StringUtils.EMPTY);
  String customTypeName = MapUtils.getString(properties, "customTypeName", StringUtils.EMPTY);
  String customFormURL = StringUtils.EMPTY;
  String customFormSubmitPath = StringUtils.EMPTY;
  
  if (CUSTOM.equals(formType)) {
   Dictionary<String, Object> osgiConfigMap = globalConfiguration.getCommonGlobalConfigMap();
   Map<String, Object> map = globalConfiguration.valueOf(osgiConfigMap);
   
   for (String str : map.keySet()) {
    if (str.startsWith(customTypeName) && str.endsWith("readUrl")) {
     customFormURL = globalConfiguration.getCommonGlobalConfigValue(str);
    } else if (str.startsWith(customTypeName) && str.endsWith("writeUrl")) {
     customFormSubmitPath = globalConfiguration.getCommonGlobalConfigValue(str);
    } else if (str.startsWith(customTypeName) && (!str.endsWith("readUrl") || !str.endsWith("writeUrl"))) {
     String newKey = globalConfiguration.getCommonGlobalConfigValue(str);
     formV2Data.put(str.substring(str.lastIndexOf('.') + 1).trim(), resourceResolver.map(newKey));
    }
   }
  }
  
  Boolean isContainer = Boolean.valueOf(MapUtils.getBooleanValue(properties, IS_CONTAINER, true));
  formV2Data.put(IS_CONTAINER, isContainer);
  redirectPath = ComponentUtil.getFullURL(resourceResolver, redirectPath, request);
  if (DISPLAY_MESSAGE.equals(formSubmissionType)) {
   formV2Data.put("successConfig", "message");
   formV2Data.put("successMessage", successMessage);
  } else if (REDIRECT_TO.equals(formSubmissionType)) {
   formV2Data.put("successConfig", "redirect");
   formV2Data.put("redirect", redirectPath);
  }
  
  formV2Data.put("onSuccessfulFormSubmission", formSubmissionType);
  formV2Data.put("failureMessage", failureMessage);
  formV2Data.put("displayFormOnDesktop", MapUtils.getBoolean(properties, DISPLAY_FORM_ON_DESKTOP, false));
  formV2Data.put("displayFormOnMobile", MapUtils.getBoolean(properties, DISPLAY_FORM_ON_MOBILE, false));
  formV2Data.put("formIdentifier", formId);
  String formSubmitPath = getSubmitPath(formType, customFormSubmitPath);
  formV2Data.put("formSubmitPath", resourceResolver.map(formSubmitPath));
  String formDataURL = getFormDataURL(formType, customFormURL);
  formV2Data.put("formDataURL", resourceResolver.map(formDataURL));
  
  formV2Data.put("servletPath", servletPath);
  if (MapUtils.isNotEmpty(formV2Data)) {
   formV2Data.put(IS_AJAX, MapUtils.getBoolean(properties, IS_AJAX));
  } else {
   formV2Data.put(IS_AJAX, Boolean.valueOf(false));
  }
  
  formV2Data.put("messages", getErrorMessageMaps(resources));
  
  formV2Data.put("formElementsV2Config", addFormElementsV2Data(currentPath, resourceResolver, resources, isContainer, "form_par").toArray());
  
  formV2Data.put("formElementGroups", addFormGroupElementsData(currentPath, resourceResolver, resources, isContainer).toArray());
  
  formV2Data.put("formEndConfig", addFormEndData(currentPath, request.getResourceResolver()));
  formV2Data.put("preFillFormWithProfileData", MapUtils.getBoolean(properties, "preFillFormWithProfileData", false));
  
  formV2Data.put("noResultMsg", getI18n(resources).get(FORM_V2_NO_RESULT_MSG));
  formV2Data.put("aemCampaignId", aemCampaignId);
  Map<String, Object> finalDataMap = new HashMap<String, Object>();
  finalDataMap.put(getJsonNameSpace(content), formV2Data);
  return finalDataMap;
 }
 
 /**
  * This method returns list of error messages along with their code and message which is picked up from i18n key according to thier locales.
  * 
  * @param resources
  *         the resources
  * @return list of error messages
  */
 private List<Map<String, Object>> getErrorMessageMaps(Map<String, Object> resources) {
  List<Map<String, Object>> errorMessageList = new ArrayList<Map<String, Object>>();
  I18n i18n = getI18n(resources);
  
  Map<String, Object> captchaErrorMessageMap = new LinkedHashMap<String, Object>();
  captchaErrorMessageMap.put(CODE, CAPTCHA_ERROR_I18N_KEY.substring(ERROR_I18N_KEY_INITIALS.length() + 1));
  captchaErrorMessageMap.put(MESSAGE, i18n.get(CAPTCHA_ERROR_I18N_KEY));
  errorMessageList.add(captchaErrorMessageMap);
  
  return errorMessageList;
 }
 
 /**
  * Gets the ignore key list.
  * 
  * @param isRequired
  *         the is required
  * @return the ignore key list
  */
 private List<String> getIgnoreKeyList(boolean isRequired) {
  List<String> ignoreKeyList = new ArrayList<String>();
  if (isRequired) {
   LOGGER.debug("Adding the to be ignored elements to list.");
   ignoreKeyList.add(JcrConstants.JCR_TITLE);
   ignoreKeyList.add(JcrConstants.JCR_DESCRIPTION);
  }
  
  LOGGER.debug("The ignoreList produced is :- " + ignoreKeyList);
  return ignoreKeyList;
 }
 
 /**
  * Gets the key list.
  * 
  * @return the key list
  */
 private List<String> getKeyList() {
  List<String> keyList = new ArrayList<String>();
  keyList.add("^jcr:.*$");
  LOGGER.debug("The keyList is :- " + keyList);
  return keyList;
 }
 
 /**
  * Adds the form end data.
  * 
  * @param currentPath
  *         the current path
  * @param resourceResolver
  *         the resource resolver
  * @return the map
  */
 private Map<String, Object> addFormEndData(String currentPath, ResourceResolver resourceResolver) {
  Map<String, Object> formEndDataMap = new HashMap<String, Object>();
  
  List<String> childPaths = new ArrayList<String>();
  
  List<String> inputPaths = new ArrayList<String>();
  
  inputPaths.add(currentPath);
  
  childPaths = SlingUtils.getPathsWithAPattern(resourceResolver, inputPaths, "end", "");
  
  if (org.apache.commons.collections.CollectionUtils.isNotEmpty(childPaths)) {
   formEndDataMap = com.sapient.platform.iea.aem.core.utils.CollectionUtils.removeUnwantedKeysFromMap(
     com.sapient.platform.iea.aem.core.utils.CollectionUtils.getResourceValueMapasMap((String) childPaths.get(0), resourceResolver), getKeyList(),
     getIgnoreKeyList(true));
  }
  
  LOGGER.debug("The endFormData is :- " + formEndDataMap);
  return formEndDataMap;
 }
 
 /**
  * Adds the form group elements data.
  * 
  * @param currentPath
  *         the current path
  * @param resourceResolver
  *         the resource resolver
  * @param resources
  *         the resources
  * @param isContainer
  *         the is container
  * @return the list
  */
 private List<Map<String, Object>> addFormGroupElementsData(String currentPath, ResourceResolver resourceResolver, Map<String, Object> resources,
   Boolean isContainer) {
  List<String> childPaths = SlingUtils.getChildPathsUnderParentNodeWithAPattern(resourceResolver, currentPath, "form_par",
    "foundation/components/parsys");
  List<Map<String, Object>> formElementsGroupList = new LinkedList<Map<String, Object>>();
  
  Iterator<String> iterator = childPaths.iterator();
  while (iterator.hasNext()) {
   Map<String, Object> formElementsGroup = new HashMap<String, Object>();
   String childPath = (String) iterator.next();
   Resource resource = resourceResolver.resolve(childPath);
   ValueMap formElementsGroupProp = resource.getValueMap();
   String resourcePath = resourceResolver.getResource(childPath).getResourceType();
   if (resourcePath.endsWith("formElementsGroup")) {
    formElementsGroup.put("groupId", MapUtils.getString(formElementsGroupProp, "groupId", ""));
    formElementsGroup.put("groupHeadingText", MapUtils.getString(formElementsGroupProp, "groupHeadingText", ""));
    formElementsGroup.put("style", BaseViewHelper.getStyleProperties(formElementsGroupProp));
    formElementsGroup.put("formElements", addFormElementsV2Data(childPath, resourceResolver, resources, isContainer, "form_elements_group_par")
      .toArray());
    formElementsGroupList.add(formElementsGroup);
   }
  }
  return formElementsGroupList;
 }
 
 /**
  * Adds the form elements v2 data.
  * 
  * @param currentPath
  *         the current path
  * @param resourceResolver
  *         the resource resolver
  * @param resources
  *         the resources
  * @param isContainer
  *         the is container
  * @param parsysName
  *         the parsys name
  * @return the list
  */
 private List<Map<String, Object>> addFormElementsV2Data(String currentPath, ResourceResolver resourceResolver, Map<String, Object> resources,
   Boolean isContainer, String parsysName) {
  List<Map<String, Object>> formElementsV2List = new LinkedList<Map<String, Object>>();
  
  List<String> childPaths = SlingUtils.getChildPathsUnderParentNodeWithAPattern(resourceResolver, currentPath, parsysName,
    "foundation/components/parsys");
  
  Map<String, Object> content = null;
  
  if (org.apache.commons.collections.CollectionUtils.isNotEmpty(childPaths)) {
   LOGGER.debug("Getting the form elements data");
   Iterator<String> iterator = childPaths.iterator();
   while (iterator.hasNext()) {
    String childPath = (String) iterator.next();
    String formElementV2ResourcePath = "/apps/" + resourceResolver.getResource(childPath).getResourceType();
    String formElementV2JsonNameSpace = null;
    try {
     this.formElementViewHelperImplV2 = super.getComponentViewAndViewHelperCache().retrieveViewHelper(formElementV2ResourcePath, null);
     formElementV2JsonNameSpace = super.getComponentViewAndViewHelperCache().retrieveJsonNameSpace(formElementV2ResourcePath);
     
    } catch (Exception e) {
     LOGGER.error("Exception occured in getting the view helper for formElement " + e.getMessage() + "Exception caught :: " + e);
    }
    
    if (null != this.formElementViewHelperImplV2) {
     
     Map<String, Object> formElementsV2Map = new LinkedHashMap<String, Object>();
     
     content = new HashMap<String, Object>();
     content.put("properties", com.sapient.platform.iea.aem.core.utils.CollectionUtils.removeUnwantedKeysFromMap(
       com.sapient.platform.iea.aem.core.utils.CollectionUtils.getResourceValueMapasMap(childPath, resourceResolver), getKeyList(),
       getIgnoreKeyList(true)));
     
     content.put(IS_CONTAINER, isContainer);
     
     content.put("jsonNameSpace", formElementV2JsonNameSpace);
     content.put("currentPath", childPath);
     formElementsV2Map = this.formElementViewHelperImplV2.processData(content, resources);
     
     LOGGER.debug("Map obtainde from form element's view helper is :- " + formElementsV2Map);
     if (MapUtils.isNotEmpty(formElementsV2Map)) {
      formElementsV2List.add(formElementsV2Map);
     }
    }
    
   }
  }
  
  return formElementsV2List;
 }
 
 /**
  * Gets the submit path.
  * 
  * @param formType
  *         the form type
  * @param customTypeName
  *         the custom type name
  * @return the submit path
  */
 private String getSubmitPath(String formType, String customFormSubmitPath) {
  String submitPath = StringUtils.EMPTY;
  
  switch (formType) {
   case "signUp":
    submitPath = globalConfiguration.getCommonGlobalConfigValue(SIGNUP_WRITEURL);
    break;
   case "contactUs":
    submitPath = globalConfiguration.getCommonGlobalConfigValue(CONTACTUS_WRITEURL);
    break;
   case "registration-email":
    submitPath = globalConfiguration.getCommonGlobalConfigValue(REGISTRATION_VALIDATE_WRITEURL);
    break;
   case "registration-password":
    submitPath = globalConfiguration.getCommonGlobalConfigValue(REGISTRATION_LOGIN_WRITEURL);
    break;
   case "registration-details":
    submitPath = globalConfiguration.getCommonGlobalConfigValue(REGISTRATION_FORM_WRITEURL);
    break;
   case "registration-preferences":
    submitPath = globalConfiguration.getCommonGlobalConfigValue(REGISTRATION_USERCREATE_WRITEURL);
    break;
   case CUSTOM:
    submitPath = customFormSubmitPath;
    break;
   default:
    submitPath = "/bin/ts/public/form";
    break;
  }
  return submitPath;
  
 }
 
 /**
  * Gets the form data URL.
  * 
  * @param formType
  *         the form type
  * @param customFormURL
  *         the custom form URL
  * @return the form data URL
  */
 private String getFormDataURL(String formType, String customFormURL) {
  String submitPath = StringUtils.EMPTY;
  
  switch (formType) {
   case "signUp":
    submitPath = globalConfiguration.getCommonGlobalConfigValue(SIGNUP_READURL);
    break;
   case "contactUs":
    submitPath = globalConfiguration.getCommonGlobalConfigValue(CONTACTUS_READURL);
    break;
   case "registration-email":
    submitPath = globalConfiguration.getCommonGlobalConfigValue(REGISTRATION_VALIDATE_READURL);
    break;
   case "registration-password":
    submitPath = globalConfiguration.getCommonGlobalConfigValue(REGISTRATION_LOGIN_READURL);
    break;
   case "registration-details":
    submitPath = globalConfiguration.getCommonGlobalConfigValue(REGISTRATION_FORM_READURL);
    break;
   case "registration-preferences":
    submitPath = globalConfiguration.getCommonGlobalConfigValue(REGISTRATION_USERCREATE_READURL);
    break;
   case CUSTOM:
    submitPath = customFormURL;
    break;
   default:break;
  }
  return submitPath;
  
 }
 
}
