/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.components.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class UnileverDateHelper.
 */
public class UnileverDateHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(UnileverDateHelper.class);
 
 /** The Constant DATE_FORMATTER. */
 private static final String DATE_FORMATTER = "yyyy-MM-dd'T'HH:mm:ss";
 
 private UnileverDateHelper() {
  
 }
 
 /**
  * Get the Date in String.
  * 
  * @param properties
  *         the properties
  * @param date
  *         the date
  * @return
  */
 public static String getDateString(ValueMap properties, String date) {
  String dateString = StringUtils.EMPTY;
  if (MapUtils.getObject(properties, date) instanceof GregorianCalendar) {
   dateString = ((MapUtils.getObject(properties, date)) != null) ? ComponentUtil.getFormattedDateString(
     (GregorianCalendar) MapUtils.getObject(properties, date), null) : StringUtils.EMPTY;
  } else {
   SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMATTER);
   String publishDaate = MapUtils.getString(properties, date, StringUtils.EMPTY);
   try {
    if (StringUtils.isNotBlank(publishDaate)) {
     Date dateObject = dateFormat.parse(publishDaate);
     Calendar calendar = Calendar.getInstance();
     calendar.setTime(dateObject);
     dateString = ((MapUtils.getObject(properties, date)) != null) ? ComponentUtil.getFormattedDateString((GregorianCalendar) calendar, null)
       : StringUtils.EMPTY;
    }
   } catch (ParseException e) {
    LOGGER.warn("Error while parsing date", e);
   }
  }
  return dateString;
 }
 
}
