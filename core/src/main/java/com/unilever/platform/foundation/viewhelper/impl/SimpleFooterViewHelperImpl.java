/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * This class prepares the data map for simple footer Component It reads the values from the dialog and passes the info as a json.
 */

@Component(description = "SimpleFooter", immediate = true, metatype = true, label = "Simple Footer")
@Service(value = { SimpleFooterViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.SIMPLE_FOOTER, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class SimpleFooterViewHelperImpl extends BaseViewHelper {
 
 /** multifield key name for json */
 private static final String FOOTER_LINKS = "footerLinks";
 
 /** target link of logo, key for json */
 private static final String TARGET = "target";
 
 /** alt text for logo */
 private static final String ALT_IMAGE = "altImage";
 
 /** copyright key for json */
 private static final String COPYRIGHT = "copyright";
 
 /** logo target link read from component */
 private static final String LOGO_LINK_URL = "globalLogoLinkUrl";
 
 /** logo alt text read from component */
 private static final String LOGO_ALT_TEXT = "globalLogoImageAltText";
 
 /** logo read from component */
 private static final String LOGO_SIMPLE_FOOTER = "globalLogoImage";
 
 /** The Constant WINDOW_TYPE. */
 private static final String WINDOW_TYPE = "windowType";
 
 /** The Constant LINK_LABEL. */
 private static final String LINK_LABEL = "linkLabel";
 
 /** The Constant LABEL. */
 private static final String LABEL = "label";
 
 /** The Constant LINK_URL. */
 private static final String LINK_URL = "linkUrl";
 
 /** The Constant FOOTER_INFO. */
 private static final String FOOTER_INFO = "footerInfo";
 
 /** The Constant LINK. */
 private static final String LINK = "link";
 
 /** The Constant OPEN_IN_NEW_WINDOW. */
 private static final String OPEN_IN_NEW_WINDOW = "openInNewWindow";
 
 /** The Constant WINDOW_TYPE_LOGO. */
 private static final String WINDOW_TYPE_LOGO = "windowTypeLogo";
 
 /** The Constant URL. */
 private static final String IMAGE_URL = "imageUrl";
 
 /** The Constant COPY_RIGHT_TEXT. */
 private static final String TEXT = "text";
 
 /** The Constant LOGO. */
 private static final String LOGO = "logo";
 
 /** The Constant COPY_RIGHT_TEXT. */
 private static final String COPY_RIGHT_TEXT = "copyrightText";
 
 /**
  * logger object.
  **/
 private static final Logger LOGGER = LoggerFactory.getLogger(SimpleFooterViewHelperImpl.class);
 
 @Reference
 ConfigurationService configurationService;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @SuppressWarnings("unchecked")
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("Executing simpleFooterViewHelperImpl. Inside processData method ");
  Map<String, Object> componentProperties = (Map<String, Object>) content.get(PROPERTIES);
  Map<String, Object> simpleFooterProperties = new HashMap<String, Object>();
  
  simpleFooterProperties = getfooterProperties(getResourceResolver(resources), getCurrentResource(resources), getSlingRequest(resources));
  simpleFooterProperties.put(COPYRIGHT, getCopyrightProperties(componentProperties, getResourceResolver(resources), getSlingRequest(resources)));
  
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE), simpleFooterProperties);
  return data;
  
 }
 
 /**
  * Gets the copyright properties.
  * 
  * @param componentProperties
  *         the component properties
  * @param resourceResolver
  *         the resource resolver
  * @return the copyright properties
  */
 private Map<String, Object> getCopyrightProperties(Map<String, Object> componentProperties, ResourceResolver resourceResolver,
   SlingHttpServletRequest slingRequest) {
  Map<String, Object> copyrightProperties = new LinkedHashMap<String, Object>();
  
  copyrightProperties.put(TEXT,
    ComponentUtil.getCopyrightSignWithYear() + UnileverConstants.SPACE + MapUtils.getString(componentProperties, COPY_RIGHT_TEXT, StringUtils.EMPTY));
  copyrightProperties.put(LOGO, getLogoProperties(componentProperties, resourceResolver, slingRequest));
  
  LOGGER.debug("Copyright Properties map : " + copyrightProperties);
  return copyrightProperties;
 }
 
 /**
  * Gets the logo properties.
  * 
  * @param componentProperties
  *         the component properties
  * @param resourceResolver
  *         the resource resolver
  * @return the logo properties
  */
 private Map<String, Object> getLogoProperties(Map<String, Object> componentProperties, ResourceResolver resourceResolver,
   SlingHttpServletRequest slingRequest) {
  
  String logo = StringUtils.EMPTY;
  String logoAltText = StringUtils.EMPTY;
  String logoLink = StringUtils.EMPTY;
  String newWindow = StringUtils.EMPTY;
  
  logo = componentProperties.get(LOGO_SIMPLE_FOOTER) == null ? StringUtils.EMPTY : componentProperties.get(LOGO_SIMPLE_FOOTER).toString();
  logoAltText = componentProperties.get(LOGO_ALT_TEXT) == null ? StringUtils.EMPTY : componentProperties.get(LOGO_ALT_TEXT).toString();
  logoLink = componentProperties.get(LOGO_LINK_URL) == null ? StringUtils.EMPTY : componentProperties.get(LOGO_LINK_URL).toString();
  newWindow = componentProperties.get(WINDOW_TYPE_LOGO) == null ? StringUtils.EMPTY : componentProperties.get(WINDOW_TYPE_LOGO).toString();
  
  Map<String, Object> logoProperties = new LinkedHashMap<String, Object>();
  logoProperties.put(OPEN_IN_NEW_WINDOW, newWindow);
  logoProperties.put(ALT_IMAGE, logoAltText);
  logoProperties.put(IMAGE_URL, logo);
  logoProperties.put(TARGET, ComponentUtil.getFullURL(resourceResolver, logoLink, slingRequest));
  if (StringUtils.isBlank(logoLink)) {
   logoLink = logoAltText;
  }
  LOGGER.debug("Unilever Logo Properties map : " + logoProperties);
  return logoProperties;
 }
 
 /**
  * Gets the footer properties.
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param currentResource
  *         the current resource
  * @return the footer properties
  */
 private Map<String, Object> getfooterProperties(ResourceResolver resourceResolver, Resource currentResource, SlingHttpServletRequest slingRequest) {
  List<Map<String, Object>> footerList = new ArrayList<Map<String, Object>>();
  
  List<String> footerFieldPropertyName = new ArrayList<String>();
  footerFieldPropertyName.add(LINK_LABEL);
  footerFieldPropertyName.add(LINK_URL);
  footerFieldPropertyName.add(FOOTER_INFO);
  footerFieldPropertyName.add(WINDOW_TYPE);
  
  LOGGER.debug("footer field properties : " + footerFieldPropertyName);
  
  List<Map<String, String>> list = ComponentUtil.getNestedMultiFieldProperties(currentResource, "footerJson");
  Map<String, Object> footerMap = null;
  for (Map<String, String> map : list) {
   footerMap = new LinkedHashMap<String, Object>();
   footerMap.put(LABEL, StringUtils.defaultString(map.get(LINK_LABEL), StringUtils.EMPTY));
   String linkUrl = StringUtils.defaultString(map.get(LINK_URL), StringUtils.EMPTY);
   linkUrl = ComponentUtil.getFullURL(resourceResolver, linkUrl, slingRequest);
   
   footerMap.put(LINK, linkUrl);
   footerMap.put(OPEN_IN_NEW_WINDOW, StringUtils.defaultString(map.get(WINDOW_TYPE), StringUtils.EMPTY));
   String footerInfo = StringUtils.defaultString(map.get(FOOTER_INFO), StringUtils.EMPTY);
   if (StringUtils.isBlank(footerInfo)) {
    footerInfo = StringUtils.defaultString(map.get(LINK_LABEL), StringUtils.EMPTY);
   }
   footerList.add(footerMap);
  }
  
  // adding list to list of map
  Map<String, Object> footerLinkProperties = new LinkedHashMap<String, Object>();
  footerLinkProperties.put(FOOTER_LINKS, footerList.toArray());
  LOGGER.debug("Footer Link Navigation Properties : " + footerLinkProperties.size());
  return footerLinkProperties;
 }
 
}
