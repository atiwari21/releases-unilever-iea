/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;

/**
 * The Class CSVExtractor.
 * 
 */
@Component(metatype = true)
@Service(value = VirtualAgentLinkExtractor.class)
@Properties({
  @Property(name = "virtualAgent.SuggestionLink", value = { "" }, label = "Virtual Agent Suggestion Link", description = "Virtual Agent Suggestions Link"),
  @Property(name = "virtualAgent.ResultsLink", value = { "" }, label = "Virtual Agent Results Link", description = "Virtual Agent Results Link") })
public class VirtualAgentLinkExtractor {
 
 /** The property map taken from osgi configuration. */
 protected Map<String, String> props = new LinkedHashMap<String, String>();
 
 /** The column mappings. */
 protected String[] suggestionUrl;
 protected String[] resultsUrl;
 
 /** The default column mappings in case no configuration is present. */
 private String[] defaultColumnMappings = { "" };
 
 /** The Constant PROPERTY_MAPPINGS. */
 
 private static final String SUGGESTIONS = "virtualAgent.SuggestionLink";
 private static final String RESULTS = "virtualAgent.ResultsLink";
 
 /**
  * Activator for the service. Reads default mappings and store them in map.
  * 
  * @param ctx
  *         the ctx
  */
 @Activate
 protected void activate(ComponentContext ctx) {
  suggestionUrl = PropertiesUtil.toStringArray(ctx.getProperties().get(SUGGESTIONS), defaultColumnMappings);
  resultsUrl = PropertiesUtil.toStringArray(ctx.getProperties().get(RESULTS), defaultColumnMappings);
 }
 
 public String[] getSuggestionUrl() {
  return suggestionUrl;
  
 }
 
 public String[] getResultsUrl() {
  return resultsUrl;
  
 }
 
}
