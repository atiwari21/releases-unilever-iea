/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfiguration;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.aem.foundation.utils.AdaptiveUtil;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.kritique.config.KritiqueReviewsCommentsService;
import com.unilever.platform.foundation.template.dto.KritiqueDTO;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * The Class ReviewsViewHelperImpl.
 */
@Component(description = "ReviewsViewHelperImpl", immediate = true, metatype = true, label = "ReviewsViewHelperImpl")
@Service({ ReviewsViewHelperImpl.class, ViewHelper.class })
@Properties({
  @org.apache.felix.scr.annotations.Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = {
    UnileverComponents.REVIEW }, propertyPrivate = true),
  @org.apache.felix.scr.annotations.Property(name = Constants.SERVICE_RANKING, intValue = { 5000 }, propertyPrivate = true) })
public class ReviewsViewHelperImpl extends BaseViewHelper {
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The Constant globalConfigCat. */
 private static final String GLOBAL_CONFIG_CAT = "productListing";
 /** The kritique service. */
 @Reference
 KritiqueReviewsCommentsService kritiqueReviewsCommentsService;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ReviewsViewHelperImpl.class);
 /** The Constant Reviews. */
 private static final String MAP_REVIEWS = "Reviews";
 /** The Constant Page num */
 private static final String PAGE_NUM = "PageNum";
 /** The Constant EQUALS. */
 private static final String EQUALS = "=";
 /** name */
 private static final String NAME = "name";
 /** social media key used in map */
 private static final String SOCIAL_MEDIA = "socialMedia";
 /** headline key used in map */
 private static final String HEADLINE = "headline";
 /** i18n key */
 private static final String ADDTHIS_SOCIAL_SHARE_TITLE_I18N = "socialShareAdaptive.headline";
 /** The Constant SOCIAL_SHARING_CATEGORY. */
 private static final String SOCIAL_SHARING_CATEGORY = "addThisWidget";
 private static final int INTEGER_TWO = 2;
 private static final String CONTENT_TYPE = "contentType";
 private static final String RATING_REVIEWS = "ratingReviews";
 private static final String PRODUCT_PAGE = "productPage";
 private static final String SINGLE_PRODUCT_VIEW = "singleProductView";
 private static final String MULTIPLE_PRODUCT_VIEW = "multipleProductView";
 private static final String SINGLE_PRODUCT_VIEW_SUPPORTED = "singleProductViewSupported";
 private static final String MULTIPLE_PRODUCT_VIEW_SUPPORTED = "multipleProductViewSupported";
 private static final String HTML_CODE = "htmlCode";
 private static final String HTML_CODE_SNIPPET = "htmlCodeSnippet";
 private static final String DISABLE_WIDGET = "disableWidget";
 private static final String GENERAL_CONFIG_MAP = "generalConfig";
 private static final String BAZAAR_VOICE = "bazaarvoice";
 private static final String KRITIQUE = "kritique";
 private static final String SERVICE_PROVIDER_NAME = "serviceProviderName";
 
 @Override
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData (java.util.Map, java.util.Map)
  */
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.info("Review Helper #processData called .");
  ResourceResolver resourceResolver = (ResourceResolver) resources.get(ProductConstants.RESOURCE_RESOLVER);
  Map<String, Object> data = new HashMap<String, Object>();
  Map<String, Object> details = new HashMap<String, Object>();
  Map<String, Object> reviewsPropertiesMap = getProperties(content);
  SlingHttpServletRequest request = getSlingRequest(resources);
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  I18n i18n = getI18n(resources);
  Page productPage = getCurrentPage(resources);
  
  String reviewHeading = i18n.get("reviewSection.heading");
  String reviewDescription = i18n.get("reviewSection.description");
  String productPagePath = MapUtils.getString(reviewsPropertiesMap, PRODUCT_PAGE);
  UNIProduct currentProduct = null;
  if (StringUtils.isNotBlank(productPagePath)) {
   currentProduct = ProductHelper.getUniProduct(pageManager.getPage(productPagePath));
  } else {
   currentProduct = ProductHelper.getUniProduct(productPage, getSlingRequest(resources));
  }
  if (currentProduct != null && AdaptiveUtil.isAdaptive(request)) {
   LOGGER.info("Feature phone view called and product page path is " + productPage.getPath());
   
   KritiqueDTO kritiqueparams = new KritiqueDTO(productPage, configurationService);
   int resultPerPage = kritiqueparams.getNoOfRecords();
   if (currentProduct != null && kritiqueparams != null) {
    kritiqueparams.setSiteProductId(currentProduct.getSKU());
    Map<String, Object> kritiquemap = new HashMap<String, Object>();
    kritiquemap = kritiqueReviewsCommentsService.getReviewsService(kritiqueparams, configurationService);
    int noOfRecords = MapUtils.isNotEmpty(kritiquemap) ? MapUtils.getInteger(processReviewMap(kritiquemap), "ReviewCount") : 0;
    if (noOfRecords > kritiqueparams.getNoOfRecords()) {
     kritiqueparams.setNoOfRecords(noOfRecords);
     kritiquemap = kritiqueReviewsCommentsService.getReviewsService(kritiqueparams, configurationService);
    }
    details.put("paginationmap", getPaginationResults(request, resultPerPage, kritiquemap));
    details.put("reviews", processLimitedReviewMap(kritiquemap, resources, resultPerPage));
    details.put("reviewHeading", reviewHeading);
    details.put("reviewDescription", reviewDescription);
    Map<String, Object> socialShareProperties = new HashMap<String, Object>();
    socialShareProperties = addThisWidget(resources, configurationService);
    details.put("socialSharing", socialShareProperties);
    LOGGER.info("reviews map created successfully for Reviews component");
   }
  } else {
   String contentType = MapUtils.getString(productPage.getProperties(), CONTENT_TYPE, StringUtils.EMPTY);
   if (currentProduct != null) {
    String uniqueId = currentProduct.getSKU();
    String entityType = "product";
    
    if (StringUtils.isBlank(uniqueId)) {
     uniqueId = "";
    }
    
    String viewType = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, productPage, "productOverview", "reviewViewType");
    
    details.put("uniqueId", uniqueId);
    details.put("identifierType", ProductHelper.getProperty(currentProduct, "identifierType"));
    details.put("identifierValue", ProductHelper.getProperty(currentProduct, "identifierValue"));
    details.put("entityType", entityType);
    details.put("viewType", viewType);
   } else {
    details.put("entityType", contentType.toLowerCase());
   }
   details.put("review", getReviewMapProperties(productPage));
  }
  setGeneralConfigDataMap(details, reviewsPropertiesMap);
  data.put("reviews", details);
  return data;
 }
 
 private Map<String, String> getReviewMapProperties(Page page) {
  Map<String, String> reviewMapProperties = new HashMap<String, String>();
  String serviceProviderName = StringUtils.EMPTY;
  String serviceProviderNameReview = StringUtils.EMPTY;
  Resource resource = page.getContentResource();
  InheritanceValueMap valueMap = new HierarchyNodeInheritanceValueMap(resource);
  if (valueMap != null) {
   serviceProviderName = (String) valueMap.getInherited(RATING_REVIEWS, StringUtils.EMPTY);
  }
  if (StringUtils.isNotBlank(serviceProviderName)) {
   serviceProviderNameReview = serviceProviderName;
  } else {
   serviceProviderNameReview = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, page, "ratingAndReviews",
     SERVICE_PROVIDER_NAME);
  }
  reviewMapProperties = GlobalConfigurationUtility.getMapFromConfiguration(configurationService, page, serviceProviderNameReview);
  if (reviewMapProperties != null) {
   GlobalConfiguration globalConfiguration = page.adaptTo(GlobalConfiguration.class);
   String[] excludedConfig = globalConfiguration.getCommonGlobalConfigValue("excludedSEOProperties").split(",");
   for (String excludeKey : excludedConfig) {
    if (reviewMapProperties.containsKey(excludeKey)) {
     reviewMapProperties.remove(excludeKey);
    }
   }
   reviewMapProperties.put("enabled",
     GlobalConfigurationUtility.getValueFromConfiguration(configurationService, page, GLOBAL_CONFIG_CAT, "reviewEnable"));
   if (BAZAAR_VOICE.equalsIgnoreCase(serviceProviderNameReview) || serviceProviderNameReview.startsWith(BAZAAR_VOICE)) {
    reviewMapProperties.put(SERVICE_PROVIDER_NAME, BAZAAR_VOICE);
   } else if (KRITIQUE.equalsIgnoreCase(serviceProviderNameReview) || serviceProviderNameReview.startsWith(KRITIQUE)) {
    reviewMapProperties.put(SERVICE_PROVIDER_NAME, KRITIQUE);
   }
  }
  
  LOGGER.debug("Review Map Properties : " + reviewMapProperties);
  return reviewMapProperties;
 }
 
 /**
  * @param reviewMap
  */
 private Map<String, Object> processReviewMap(Map<String, Object> reviewMap) {
  Map<String, Object> map = new HashMap<String, Object>();
  map = (HashMap<String, Object>) reviewMap;
  Map<String, Object> updatedmap = new HashMap<String, Object>();
  updatedmap = (HashMap<String, Object>) reviewMap;
  
  if ((map != null) && (map.containsKey(MAP_REVIEWS) && (map.get(MAP_REVIEWS) != null))) {
   List<Map<String, Object>> reviews = new ArrayList<Map<String, Object>>();
   List<Map<String, Object>> updatedreviews = new ArrayList<Map<String, Object>>();
   
   reviews = (List<Map<String, Object>>) map.get(MAP_REVIEWS);
   
   SimpleDateFormat formatter = new SimpleDateFormat("MMM dd,yyyy");
   SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
   getPageReviews(reviews, updatedreviews, formatter, formatter1, 0, reviews.size());
   updatedmap.put(MAP_REVIEWS, updatedreviews);
  }
  
  return updatedmap;
 }
 
 /**
  * @param reviews
  * @param updatedreviews
  * @param formatter
  * @param formatter1
  */
 private void getPageReviews(List<Map<String, Object>> reviews, List<Map<String, Object>> updatedreviews, SimpleDateFormat formatter,
   SimpleDateFormat formatter1, int startIndex, int endIndex) {
  Map<String, Object> revkeyvals;
  Map<String, Object> newrevkeyvals;
  String formatteddate;
  for (int i = startIndex; i < endIndex; i++) {
   revkeyvals = reviews.get(i);
   newrevkeyvals = new HashMap<String, Object>();
   for (Map.Entry<String, Object> reviewsListObject : revkeyvals.entrySet()) {
    String key = reviewsListObject.getKey();
    Object value = reviewsListObject.getValue();
    newrevkeyvals.put(key, value);
    if (("RatedOn").equals(key)) {
      String ratedOn = (value!=null && StringUtils.isNotBlank(value.toString()))?value.toString():StringUtils.EMPTY;
      if (StringUtils.isNotBlank(ratedOn)) {
      try {
       Date date = formatter1.parse(value.toString());
       formatteddate = formatter.format(date);
       newrevkeyvals.put("FormattedRatedOn", formatteddate);
       
      } catch (ParseException e) {
       LOGGER.warn("Date ParseException", e);
      }
      
     }
    }
    
   }
   
   updatedreviews.add(newrevkeyvals);
   
  }
 }
 
 /**
  * @param slingRequest
  * @param resultPerPage
  * @param Kritiquemap
  */
 private Map<String, Object> getPaginationResults(SlingHttpServletRequest request, int resultPerPage, Map<String, Object> kritiquemap) {
  
  Map<String, Object> results = new HashMap<String, Object>();
  Map<String, Object> map = new HashMap<String, Object>();
  map = (HashMap<String, Object>) kritiquemap;
  List<Map<String, Object>> reviews = new ArrayList<Map<String, Object>>();
  
  try {
   if ((map != null) && (!map.isEmpty()) && (map.containsKey(MAP_REVIEWS))) {
    
    reviews = (List<Map<String, Object>>) map.get(MAP_REVIEWS);
    int totalResults = reviews.size();
    if (totalResults > 0) {
     int noOfPages = totalResults / resultPerPage;
     int remainder = totalResults % resultPerPage;
     if (remainder > 0) {
      noOfPages++;
     }
     
     int queryStringPageNo = Integer.parseInt(request.getParameter(PAGE_NUM) == null ? "0" : request.getParameter(PAGE_NUM));
     
     Map<String, String> newMap = getQueryStringParamMap(request);
     
     String queryString = "?";
     for (String key : newMap.keySet()) {
      queryString = queryString + key + "=" + newMap.get(key) + "&";
     }
     
     results.put("extremePageLinks", getExtremePageLinksMap(request, queryString, noOfPages, queryStringPageNo));
     results.put("pagecount", noOfPages);
     results.put("paginationLinks", getPaginationLinksList(request, queryString, noOfPages, queryStringPageNo).toArray());
    }
   }
  } catch (Exception e) {
   LOGGER.error("Exception in getting Review Pagination Result Map" + ExceptionUtils.getStackTrace(e));
  }
  
  return results;
 }
 
 private Map<String, String> getQueryStringParamMap(SlingHttpServletRequest request) {
  Map<String, String> map = new HashMap<String, String>();
  Map parametersMap = request.getParameterMap();
  
  for (Object key : parametersMap.keySet()) {
   Object[] row = (Object[]) parametersMap.get(key);
   map.put((String) key, (String) row[0]);
  }
  
  if (map.containsKey(PAGE_NUM)) {
   map.remove(PAGE_NUM);
  }
  return map;
 }
 
 private List<Map<String, Object>> getPaginationLinksList(SlingHttpServletRequest request, String queryString, int noOfPages, int pageNumber) {
  
  List<Map<String, Object>> paginationLinks = new ArrayList<Map<String, Object>>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  int queryStringPageNo = pageNumber;
  for (int i = 1; i <= noOfPages; i++) {
   String url = request.getRequestURI() + queryString + PAGE_NUM + EQUALS + String.valueOf(i);
   if (queryStringPageNo == 0) {
    paginationLinks.add(getPageURLMap(i, url, true));
    queryStringPageNo++;
   } else if (queryStringPageNo == i) {
    paginationLinks.add(getPageURLMap(i, url, true));
   } else {
    paginationLinks.add(getPageURLMap(i, url, false));
   }
  }
  
  return paginationLinks;
 }
 
 /**
  * @param slingRequest
  * @param querystring
  * @param noofPages
  * @param queryStringPageNo
  */
 private Map<String, Object> getExtremePageLinksMap(SlingHttpServletRequest request, String queryString, int noOfPages, int queryStringPageNo) {
  Map<String, Object> extremePageMap = new HashMap<String, Object>();
  
  String firstPageUrl = request.getRequestURI() + queryString + "PageNum=1";
  String lastPageUrl = request.getRequestURI() + queryString + PAGE_NUM + EQUALS + String.valueOf(noOfPages);
  String nextPageUrl = StringUtils.EMPTY;
  String previousPageUrl = StringUtils.EMPTY;
  
  if ((queryStringPageNo != 0) && (queryStringPageNo <= noOfPages)) {
   if (queryStringPageNo == 1) {
    nextPageUrl = request.getRequestURI() + queryString + PAGE_NUM + EQUALS + String.valueOf(queryStringPageNo + 1);
    previousPageUrl = request.getRequestURI() + queryString + PAGE_NUM + EQUALS + String.valueOf(queryStringPageNo);
   } else if (queryStringPageNo == noOfPages) {
    nextPageUrl = request.getRequestURI() + queryString + PAGE_NUM + EQUALS + String.valueOf(noOfPages);
    previousPageUrl = request.getRequestURI() + queryString + PAGE_NUM + EQUALS + String.valueOf(queryStringPageNo - 1);
   } else {
    nextPageUrl = request.getRequestURI() + queryString + PAGE_NUM + EQUALS + String.valueOf(queryStringPageNo + 1);
    previousPageUrl = request.getRequestURI() + queryString + PAGE_NUM + EQUALS + String.valueOf(queryStringPageNo - 1);
   }
  } else if (queryStringPageNo == 0) {
   if (noOfPages > 1) {
    nextPageUrl = request.getRequestURI() + queryString + PAGE_NUM + EQUALS + String.valueOf(queryStringPageNo + INTEGER_TWO);
    previousPageUrl = request.getRequestURI() + queryString + PAGE_NUM + EQUALS + String.valueOf(queryStringPageNo + 1);
   } else {
    nextPageUrl = request.getRequestURI() + queryString + PAGE_NUM + EQUALS + String.valueOf(queryStringPageNo + 1);
    previousPageUrl = request.getRequestURI() + queryString + PAGE_NUM + EQUALS + String.valueOf(queryStringPageNo + 1);
   }
  }
  
  extremePageMap.put("firstPage", getExtremePageURLMap(firstPageUrl));
  extremePageMap.put("lastPage", getExtremePageURLMap(lastPageUrl));
  extremePageMap.put("nextPage", getExtremePageURLMap(nextPageUrl));
  extremePageMap.put("previousPage", getExtremePageURLMap(previousPageUrl));
  
  return extremePageMap;
 }
 
 /**
  * @param URL
  */
 private Map<String, Object> getExtremePageURLMap(String url) {
  
  Map<String, Object> pageUrlMap = new HashMap<String, Object>();
  
  pageUrlMap.put("url", url);
  pageUrlMap.put("isenabled", true);
  
  return pageUrlMap;
 }
 
 /**
  * @param URL
  * @param flag
  */
 private Map<String, Object> getPageURLMap(int pageNo, String url, Boolean flag) {
  
  Map<String, Object> pageUrlMap = new HashMap<String, Object>();
  
  pageUrlMap.put("label", pageNo);
  pageUrlMap.put("url", url);
  pageUrlMap.put("iscurrentPage", flag);
  
  return pageUrlMap;
 }
 
 /**
  * Called from SocialSharingViewHelperImpl class. Reads the social media details from the config files . Passes it in a map
  * 
  * @param content
  *         the content
  * @param resources
  *         the resources
  * @param configurationService
  *         the configuration service
  * @return properties map
  */
 public static Map<String, Object> addThisWidget(Map<String, Object> resources, ConfigurationService configurationService) {
  LOGGER.debug("Executing SocialShare functionality in ReviewViewHelper.java");
  Map<String, Object> properties = new HashMap<String, Object>();
  ResourceResolver resourceResolver = BaseViewHelper.getResourceResolver(resources);
  Resource currentResource = BaseViewHelper.getCurrentResource(resources);
  if (currentResource != null) {
   PageManager pm = resourceResolver.adaptTo(PageManager.class);
   Page currentPage = pm.getContainingPage(currentResource);
   
   Map<String, String> addthisWidgetOptions = null;
   
   List<Object> socialShareList = new ArrayList<Object>();
   
   String title = StringUtils.EMPTY;
   
   I18n i18n = BaseViewHelper.getI18n(resources);
   title = i18n.get(ADDTHIS_SOCIAL_SHARE_TITLE_I18N);
   try {
    addthisWidgetOptions = configurationService.getCategoryConfiguration(currentPage, SOCIAL_SHARING_CATEGORY);
    LOGGER.debug("Configured Options are :" + addthisWidgetOptions);
   } catch (ConfigurationNotFoundException e) {
    LOGGER.error("Configuration for social links Not found:", e);
   }
   
   getSocialShareList(addthisWidgetOptions, socialShareList, currentPage, resourceResolver, BaseViewHelper.getSlingRequest(resources));
   properties.put(HEADLINE, title);
   properties.put(SOCIAL_MEDIA, socialShareList.toArray());
   LOGGER.debug("No of social channels fetched: {}", socialShareList.size());
   
  } else {
   LOGGER.info("Component cannot be found for current page");
  }
  
  return properties;
  
 }
 
 /**
  * Gets the list of of maps social media links and container maps .
  * 
  * @param configurationService
  *         the configuration service
  * @param currentPage
  *         the current page
  * @param addthisWidgetOptions
  *         the addthis widget options
  * @param socialShareList
  *         the social share list
  * @param currentPage
  * @return the social share list
  */
 private static void getSocialShareList(Map<String, String> addthisWidgetOptions, List<Object> socialShareList, Page currentPage,
   ResourceResolver resourceResolver, SlingHttpServletRequest slingRequest) {
  LOGGER.debug("Fetching the social channels");
  Map<String, Object> socialShareMap;
  Set<String> set = addthisWidgetOptions.keySet();
  Iterator<String> itr = set.iterator();
  int limit = 0;
  while (itr.hasNext() && limit < INTEGER_TWO) {
   socialShareMap = new HashMap<String, Object>();
   String socialLink = itr.next();
   String name = addthisWidgetOptions.get(socialLink);
   String[] names = name.split("#");
   String newName = name;
   if (newName.contains("#") && names.length <= INTEGER_TWO) {
    name = names[0];
   }
   Map<String, Object> containerTagMap = ComponentUtil.getContainerTagMap(currentPage, socialLink, name);
   String url = ComponentUtil.getFullURL(resourceResolver, currentPage.getPath(), slingRequest);
   StringBuilder stringBuilder = new StringBuilder();
   socialShareMap.put(NAME, name);
   String encodedUrl = StringUtils.EMPTY;
   String currentPageTitle = currentPage.getTitle();
   try {
    url = URLEncoder.encode(url, "UTF-8");
    currentPageTitle = URLEncoder.encode(currentPageTitle, "UTF-8");
   } catch (UnsupportedEncodingException e) {
    LOGGER.error("Error encoding current page", e);
   }
   if (newName.contains("#") && names.length <= INTEGER_TWO) {
    encodedUrl = stringBuilder.append(names[1]).append(url).append("&text=").append(currentPageTitle).toString();
    socialShareMap.put("url", encodedUrl);
   }
   socialShareMap.put(UnileverConstants.CONTAINER_TAG, containerTagMap);
   socialShareList.add(socialShareMap);
   limit++;
   
  }
 }
 
 /**
  * @param reviewMap
  */
 private Map<String, Object> processLimitedReviewMap(Map<String, Object> reviewMap, Map<String, Object> resources, int resultPerPage) {
  Map<String, Object> map = new HashMap<String, Object>();
  map = (HashMap<String, Object>) reviewMap;
  Map<String, Object> updatedmap = new HashMap<String, Object>();
  updatedmap = (HashMap<String, Object>) reviewMap;
  
  if ((map != null) && (map.containsKey(MAP_REVIEWS) && (map.get(MAP_REVIEWS) != null))) {
   List<Map<String, Object>> updatedreviews = new ArrayList<Map<String, Object>>();
   List<Map<String, Object>> reviews = new ArrayList<Map<String, Object>>();
   SimpleDateFormat formatter = new SimpleDateFormat("MMM dd,yyyy");
   SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
   SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) resources.get(ProductConstants.REQUEST);
   int totalResults = reviews.size();
   int noOfPages = 0;
   int queryStringPageNo = 0;
   Map<String, Object> resultMap = getTotalResult(reviewMap, slingRequest, resultPerPage);
   totalResults = MapUtils.getIntValue(resultMap, "totalResults");
   noOfPages = MapUtils.getIntValue(resultMap, "noOfPages");
   queryStringPageNo = MapUtils.getIntValue(resultMap, "queryPageNo");
   if (totalResults > 0) {
    reviews = (List<Map<String, Object>>) map.get(MAP_REVIEWS);
    if ((queryStringPageNo == 0 || queryStringPageNo == 1) && totalResults > resultPerPage) {
     getPageReviews(reviews, updatedreviews, formatter, formatter1, 0, resultPerPage);
    } else if (queryStringPageNo == noOfPages || noOfPages == 1) {
     getPageReviews(reviews, updatedreviews, formatter, formatter1, (noOfPages * resultPerPage) - resultPerPage, totalResults);
    } else {
     getPageReviews(reviews, updatedreviews, formatter, formatter1, queryStringPageNo * resultPerPage - resultPerPage,
       queryStringPageNo * resultPerPage);
    }
    updatedmap.put(MAP_REVIEWS, updatedreviews.toArray());
   }
  }
  return updatedmap;
 }
 
 private Map<String, Object> getTotalResult(Map<String, Object> reviewMap, SlingHttpServletRequest slingRequest, int resultPerPage) {
  Map<String, Object> map = new HashMap<String, Object>();
  map = (HashMap<String, Object>) reviewMap;
  List<Map<String, Object>> reviews = new ArrayList<Map<String, Object>>();
  int totalResults = 0;
  int noOfPages = 0;
  int queryStringPageNo = 0;
  if ((map != null) && (map.containsKey(MAP_REVIEWS) && (map.get(MAP_REVIEWS) != null))) {
   reviews = (List<Map<String, Object>>) map.get(MAP_REVIEWS);
   totalResults = reviews.size();
   if (totalResults > 0) {
    noOfPages = totalResults / resultPerPage;
    int remainder = totalResults % resultPerPage;
    noOfPages = remainder > 0 ? noOfPages + 1 : noOfPages;
   }
   queryStringPageNo = Integer.parseInt(slingRequest.getParameter(PAGE_NUM) == null ? "0" : slingRequest.getParameter(PAGE_NUM));
  }
  Map<String, Object> resultMap = new LinkedHashMap<String, Object>();
  resultMap.put("totalResults", totalResults);
  resultMap.put("noOfPages", noOfPages);
  resultMap.put("queryPageNo", queryStringPageNo);
  return resultMap;
 }
 
 private static Map<String, Object> setGeneralConfigDataMap(Map<String, Object> details, Map<String, Object> reviewsPropertiesMap) {
  Map<String, Object> generalConfigDataMap = new LinkedHashMap<String, Object>();
  generalConfigDataMap.put(SINGLE_PRODUCT_VIEW_SUPPORTED,
    MapUtils.getBoolean(reviewsPropertiesMap, SINGLE_PRODUCT_VIEW) != null ? MapUtils.getBoolean(reviewsPropertiesMap, SINGLE_PRODUCT_VIEW) : false);
  generalConfigDataMap.put(MULTIPLE_PRODUCT_VIEW_SUPPORTED, MapUtils.getBoolean(reviewsPropertiesMap, MULTIPLE_PRODUCT_VIEW) != null
    ? MapUtils.getBoolean(reviewsPropertiesMap, MULTIPLE_PRODUCT_VIEW) : false);
  generalConfigDataMap.put(HTML_CODE_SNIPPET, MapUtils.getString(reviewsPropertiesMap, HTML_CODE, StringUtils.EMPTY));
  generalConfigDataMap.put(DISABLE_WIDGET,
    MapUtils.getBoolean(reviewsPropertiesMap, DISABLE_WIDGET) != null ? MapUtils.getBoolean(reviewsPropertiesMap, DISABLE_WIDGET) : false);
  details.put(GENERAL_CONFIG_MAP, generalConfigDataMap);
  return details;
  
 }
 
}
