/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.cache.ComponentViewAndViewHelperCache;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.CategoryOverviewHelper;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.ImageHelper;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.Image;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * <p>
 * Responsible for reading author input for CountryLanguage Selector component and generating a list of Link objects and setting in pageContext for
 * the view.
 * </p>
 */
@Component(description = "GlobalNavigationViewHelperImpl", immediate = true, metatype = true, label = "GlobalNavigationViewHelperImpl")
@Service(value = { GlobalNavigationViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.GLOBAL_NAVIGATION, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class GlobalNavigationViewHelperImpl extends BaseViewHelper {
 
 /** The Constant GLOBAL_NAVIGATION_SEPARATOR_LABEL. */
 private static final String GLOBAL_NAVIGATION_SEPARATOR_LABEL = "globalNavigation.separator.label";
 
 /** The Constant NUMBER_OF_TABS. */
 
 private static final String NUMBER_OF_TABS = "tabCount";
 
 /** The Constant BROWSE_ALL. */
 private static final String BROWSE_ALL = "browseAll";
 
 /** The Constant MAIN_LOGO. */
 private static final String MAIN_LOGO = "mainLogo";
 
 /** The Constant ALT_TEXT. */
 private static final String ALT_TEXT = "altText";
 
 /** The Constant REDIRECT_URL. */
 private static final String REDIRECT_URL = "redirectUrl";
 
 /** The Constant LABEL. */
 private static final String LABEL_TEXT = "label";
 
 /** The Constant URL. */
 private static final String URL = "url";
 
 /** The Constant SEARCH. */
 private static final String SEARCH = "search";
 
 /** The Constant HEADLINE. */
 private static final String HEADLINE = "headline";
 
 /** The Constant SEARCH_INPUT. */
 private static final String SEARCH_INPUT = "searchInput";
 
 /** The Constant FORM_NAME. */
 private static final String FORM_NAME = "formName";
 
 /** The Constant PLACEHOLDER. */
 private static final String PLACEHOLDER = "placeHolder";
 
 /** The Constant KEYWORD. */
 private static final String KEYWORD = "keyword";
 
 /** The Constant SEARCH_LOGO. */
 private static final String SEARCH_LOGO = "logo";
 
 /** The Constant SEPARATOR. */
 private static final String SEPARATOR = "separator";
 
 /** The Constant Nav_Bar. */
 private static final String NAV_BAR = "navbar";
 
 /** The Constant BACK. */
 private static final String BACK = "back";
 
 /** The Constant LEVEL. */
 private static final String LEVEL = "level";
 
 /** The Constant LEVEl_ONE. */
 private static final String LEVEL_ONE = "level1";
 
 /** The Constant LEVEL_TWO. */
 private static final String LEVEL_TWO = "level2";
 
 /** The Constant LEVEL_THREE. */
 private static final String LEVEL_THREE = "level3";
 
 /** The Constant FILENAME. */
 private static final String FILENAME = "fileName";
 
 /** The Constant FILE_REFERENCE. */
 private static final String FILE_REFERENCE = "fileReference";
 
 /** The Constant EXTENSION. */
 private static final String EXTENSION = "extension";
 
 /** The Constant TARGET. */
 private static final String TARGET = "target";
 
 /** The Constant IMAGE_URL. */
 private static final String IMAGE_URL = "imageUrl";
 
 /** The Constant BRAND_LOGO_IMAGE. */
 private static final String BRAND_LOGO_IMAGE = "brandLogoImage";
 
 /** The Constant BRAND_ALT_TEXT. */
 private static final String BRAND_ALT_TEXT = "brandLogoAltText";
 
 /** The Constant SEARCH_LOGO_PATH. */
 private static final String SEARCH_LOGO_PATH = "searchLogoPath";
 
 /** The Constant SEARCH_LOGO_ALT_TEXT. */
 private static final String SEARCH_LOGO_ALT_TEXT = "searchLogoAltText";
 /** The Constant DROPDOWN_LABEL_MAP. */
 private static final String DROPDOWN_LABEL_MAP = "dropDownLabel";
 
 /** The Constant CONTENT_TYPE. */
 private static final String CONTENT_TYPE = "contentType";
 
 /** The Constant CUSTOM. */
 private static final String CUSTOM = "custom";
 
 /** The Constant MANUAL. */
 private static final String MANUAL = "manual";
 
 /** The Constant AUTO. */
 private static final String AUTO = "auto";
 
 /** The Constant TITLE. */
 private static final String TITLE = "title";
 
 /** The Constant LINK_URL. */
 private static final String LINK_URL = "linkUrl";
 
 /** The Constant PARENT_PAGE. */
 private static final String PARENT_PAGE = "parentPage";
 
 /** The Constant CUSTOM_SLIDE_TYPE. */
 private static final String CUSTOM_SLIDE_TYPE = "slideTypeCustom";
 /** The Constant SUGGESTION_SERVLET. */
 private static final String SUGGESTION_SERVLET = "requestServlet";
 
 /**
  * Logger for the GlobalNavigationViewHelperImpl.
  */
 private static final Logger LOGGER = LoggerFactory.getLogger(GlobalNavigationViewHelperImpl.class);
 
 /** The Constant SELECT_TAB. */
 private static final String SELECT_TAB = "selectTab";
 
 /** The Constant MAXIMUM_DEPTH. */
 private static final String MAXIMUM_DEPTH = "maxDepth";
 
 /** The Constant RELATIVE_URL. */
 private static final String RELATIVE_URL = "relativeURL";
 
 /** The Constant SEARCH_PAGE_KEY_NAME. */
 private static final String SEARCH_PAGE_KEY_NAME = "searchPage";
 
 /** The Constant FP_MAIN_NAV. */
 private static final String FP_MAIN_NAV = "showMainNav";
 
 /** The Constant FP_CTA. */
 private static final String FP_CTA = "cta";
 
 /** The Constant HOME_LABEL_PATH. */
 private static final String HOME_LABEL_PATH = "homeLabel";
 
 /** The Constant CLOSE_LABEL_PATH. */
 private static final String CLOSE_LABEL_PATH = "closeLabel";
 
 /** The Constant BACK_TO_TOP_LABEL_PATH. */
 private static final String BACK_TO_TOP_LABEL_PATH = "backToTopLabel";
 
 /** The Constant HOME. */
 private static final String HOME = "home";
 
 /** The Constant CLOSE. */
 private static final String CLOSE = "close";
 
 /** The Constant BACK_TO_TOP. */
 private static final String BACK_TO_TOP = "backtotop";
 
 /** The Constant HTML_EXTENSION. */
 private static final String HTML_EXTENSION = ".html";
 
 /** The Constant HTML_FULL_MENU_EXTENTION. */
 private static final String HTML_FULL_MENU_EXTENTION = ".fullmenu.html";
 
 /** The Constant HOME_DEFAULT_VALUE. */
 private static final String HOME_DEFAULT_VALUE = "Home";
 
 /** The Constant CLOSE_DEFAULT_VALUE. */
 private static final String CLOSE_DEFAULT_VALUE = "Close";
 
 /** The Constant BACK_TO_TOP_DEFAULT_VALUE. */
 private static final String BACK_TO_TOP_DEFAULT_VALUE = "Back To Top";
 
 /** The Constant FULL_MENU_SELECTOR. */
 private static final String FULL_MENU_SELECTOR = "fullmenu";
 
 /** The Constant COUNTRYCODE. */
 public static final String COUNTRYCODE = "countryCode";
 
 /** The Constant ENABLED. */
 public static final String ENABLED = "enabled";
 
 /** The Constant STATIC_TEXT. */
 public static final String STATIC_TEXT = "staticText";
 
 /** The Constant SMARTLABEL. */
 public static final String SMARTLABEL = "smartlabel";
 
 /** The Constant CURRENCY_SYMBOL. */
 public static final String CURRENCY_SYMBOL = "currencySymbol";
 
 /** The Constant CURRENCY. */
 public static final String CURRENCY = "currency";
 
 /** The Constant LOGO. */
 private static final String LOGO = "logo";
 
 /** The Constant OPEN_IN_NEW_WINDOW. */
 private static final String OPEN_IN_NEW_WINDOW = "windowType";
 
 /** The Constant CTA_LABEL. */
 private static final String CTA_LABEL = "ctaLabel";
 
 /** The Constant TEASER_IMAGE. */
 private static final String TEASER_IMAGE = "teaserImage";
 
 /** The Constant BACKGROUND_IMAGE. */
 private static final String BACKGROUND_IMAGE = "backgroundImage";
 
 /** The Constant BACKGROUND_IMAGE. */
 private static final String BACKGROUND_IMAGE_ALT_TEXT = "backgroundImageAltText";
 
 /** The Constant DISPLAY_BACKGROUND_IMAGE. */
 private static final String DISPLAY_BACKGROUND_IMAGE = "displayBackgroundImage";
 
 /** The Constant OPEN_IN_NEW_WINDOW_TEXT. */
 private static final String OPEN_IN_NEW_WINDOW_TEXT = "openInNewWindow";
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /* reference for cache object */
 
 /** The component view and view helper cache. */
 @Reference
 ComponentViewAndViewHelperCache componentViewAndViewHelperCache;
 
 @Override
 /*
  * (non-Javadoc)
  * 
  * @see com.sapient.platform.iea.aem.core.viewhelper.impl.DefaultBaseViewHelper #processData(java.util.Map, java.util.Map)
  */
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  
  LOGGER.debug("Entering #processData of Global Navigation View Helper.");
  Map<String, Object> globalMapProperties = new LinkedHashMap<String, Object>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  Page currentPage = getCurrentPage(resources);
  SlingHttpServletRequest slingRequest = getSlingRequest(resources);
  I18n i18n = getI18n(resources);
  ResourceResolver resourceResolver = getResourceResolver(resources);
  String jsonNameSpace = getJsonNameSpace(content);
  Map<String, Object> dialogProperties = getProperties(content);
  Boolean showMainNav = true;
  String[] selectors = slingRequest.getRequestPathInfo().getSelectors();
  
  String fullMenuUrl = ComponentUtil.getFullURL(resourceResolver, currentPage.getPath(), getSlingRequest(resources));
  globalMapProperties.put(MAIN_LOGO, getBrandLogoProperties(content, resources));
  globalMapProperties.put(SEARCH, getSearchProperties(content, resources));
  globalMapProperties.put(SEPARATOR, i18n.get(GLOBAL_NAVIGATION_SEPARATOR_LABEL));
  Map<String, Object> navBarMap = new HashMap<String, Object>();
  if (!StringUtils.isEmpty(fullMenuUrl) && (fullMenuUrl.contains(HTML_EXTENSION))) {
   fullMenuUrl = fullMenuUrl.replace(HTML_EXTENSION, HTML_FULL_MENU_EXTENTION);
  }
  navBarMap.put(LABEL_TEXT, i18n.get("globalNavigation.menu"));
  navBarMap.put(URL, fullMenuUrl);
  
  globalMapProperties.put(NAV_BAR, navBarMap);
  globalMapProperties.put(BACK, i18n.get("globalNavigation.back"));
  List<Map<String, Object>> level1Items = getNavigationItems(content, resources, i18n.get("globalNavigation.browse.all"));
  if (level1Items != null && !level1Items.isEmpty()) {
   globalMapProperties.put(LEVEL_ONE, level1Items.toArray());
  }
  for (String selector : selectors) {
   if (selector.equals(FULL_MENU_SELECTOR)) {
    showMainNav = false;
    break;
   }
  }
  globalMapProperties.put(FP_MAIN_NAV, showMainNav);
  globalMapProperties.put(FP_CTA, getCTAMAP(dialogProperties, currentPage, resourceResolver, slingRequest));
  
  Boolean smartLabelEnabled = false;
  try {
   smartLabelEnabled = Boolean.parseBoolean(configurationService.getConfigValue(currentPage, SMARTLABEL, ENABLED));
   if (smartLabelEnabled) {
    Map<String, String> smartLabelconfigMap = ComponentUtil.getConfigMap(currentPage, SMARTLABEL);
    Map<String, Object> properties = new HashMap<String, Object>();
    properties.put(LOGO, smartLabelconfigMap.get(LOGO));
    properties.put(ALT_TEXT, smartLabelconfigMap.get(ALT_TEXT));
    properties.put(REDIRECT_URL, smartLabelconfigMap.get(REDIRECT_URL));
    
    globalMapProperties.put(SMARTLABEL, properties);
   }
  } catch (ConfigurationNotFoundException e) {
   LOGGER.debug("could not find configuration for shop now", e);
  }
  LOGGER.info("The Navigation list corresponding to Global Navigation is successfully generated");
  Map<String, Object> finalData = new HashMap<String, Object>();
  finalData.put(jsonNameSpace, globalMapProperties);
  return finalData;
 }
 
 /**
  * Gets the ctamap.
  * 
  * @param properties
  *         the properties
  * @param currentPage
  *         the current page
  * @param resourceResolver
  *         the resource resolver
  * @param request
  *         the request
  * @return the ctamap
  */
 private Map<String, Object> getCTAMAP(Map<String, Object> properties, Page currentPage, ResourceResolver resourceResolver,
   SlingHttpServletRequest request) {
  
  Map<String, Object> ctaMap = new HashMap<String, Object>();
  String homeLabel = MapUtils.getString(properties, HOME_LABEL_PATH);
  String closeLabel = MapUtils.getString(properties, CLOSE_LABEL_PATH);
  String backToTopLabel = MapUtils.getString(properties, BACK_TO_TOP_LABEL_PATH);
  
  String homePageUrl = ComponentUtil.getFullURL(resourceResolver, currentPage.getPath(), request);
  if (StringUtils.isNotBlank(homePageUrl)) {
   homePageUrl = StringUtils.substringBefore(homePageUrl, currentPage.getPath());
  }
  if (StringUtils.isEmpty(homePageUrl)) {
   homePageUrl = UnileverConstants.FORWARD_SLASH;
  }
  
  if (StringUtils.isBlank(homeLabel)) {
   homeLabel = HOME_DEFAULT_VALUE;
  }
  
  if (StringUtils.isBlank(closeLabel)) {
   closeLabel = CLOSE_DEFAULT_VALUE;
  }
  
  if (StringUtils.isBlank(backToTopLabel)) {
   backToTopLabel = BACK_TO_TOP_DEFAULT_VALUE;
  }
  ctaMap.put(HOME, getlabelURLMap(homeLabel, homePageUrl));
  ctaMap.put(CLOSE, getlabelURLMap(closeLabel, homePageUrl));
  ctaMap.put(BACK_TO_TOP, backToTopLabel);
  return ctaMap;
 }
 
 /**
  * Gets the label url map.
  * 
  * @param label
  *         the label
  * @param url
  *         the url
  * @return the label url map
  */
 private Map<String, Object> getlabelURLMap(String label, String url) {
  Map<String, Object> labelUrlMap = new HashMap<String, Object>();
  labelUrlMap.put(LABEL_TEXT, label);
  labelUrlMap.put("url", url);
  return labelUrlMap;
 }
 
 /**
  * Gets the serach path.
  * 
  * @param currentPage
  *         the current page
  * @param key
  *         the key
  * @return the serach path
  */
 private String getSerachPath(Page currentPage, String key) {
  String searchPath = StringUtils.EMPTY;
  try {
   searchPath = configurationService.getConfigValue(currentPage, RELATIVE_URL, key);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Configuration Not found for Category: " + RELATIVE_URL + " and key: " + SEARCH_PAGE_KEY_NAME);
   LOGGER.error(ExceptionUtils.getStackTrace(e));
  }
  return searchPath;
 }
 
 /**
  * List children.
  * 
  * @param page
  *         the page
  * @param maxDepth
  *         the max depth
  * @param currentDepth
  *         the current depth
  * @param resources
  *         the resources
  * @param excludedContentTypes
  *         the excluded content types
  * @param browseAll
  *         the browse all
  * @param openInNewWindow
  *         the open in new window
  * @param ctaLabel
  *         the cta label
  * @param displayBackgroundImage
  * @return the map
  */
 private Map<String, Object> listChildren(Page page, int maxDepth, int currentDepth, final Map<String, Object> resources,
   String[] excludedContentTypes, String browseAll, String openInNewWindow, String ctaLabel, Boolean displayBackgroundImage) {
  Map<String, Object> map = null;
  int depth = currentDepth;
  int depthMax = maxDepth;
  Map<String, Object> currentPageProperties = new HashMap<String, Object>();
  if (page != null) {
   Iterator<Page> iterator = page.listChildren();
   List<Map<String, Object>> listMap = null;
   ResourceResolver resourceResolver = getResourceResolver(resources);
   if (depthMax > 1) {
    int counter = --depthMax;
    listMap = new ArrayList<Map<String, Object>>();
    int currentLevel = depth++;
    while (iterator.hasNext()) {
     Map<String, Object> childMap = listChildren(iterator.next(), counter, depth, resources, excludedContentTypes, browseAll, openInNewWindow,
       StringUtils.EMPTY, displayBackgroundImage);
     addChildMapToList(listMap, childMap);
    }
    Map<String, Object> map1 = getMap(page, resourceResolver, excludedContentTypes, ctaLabel, resources);
    if (map1 != null && map1.size() > 0) {
     currentPageProperties = page.getProperties();
     map1.put(BROWSE_ALL, browseAll);
     map1.put(OPEN_IN_NEW_WINDOW_TEXT, openInNewWindow);
     map1.put(DISPLAY_BACKGROUND_IMAGE, displayBackgroundImage);
     map1.put(LEVEL + currentLevel, listMap.toArray());
     Image image = new Image((String) currentPageProperties.get(TEASER_IMAGE), StringUtils.EMPTY, StringUtils.EMPTY, getCurrentPage(resources),
       getSlingRequest(resources));
     map1.put(TEASER_IMAGE, image.convertToMap());
     map = map1;
    }
    
   } else {
    map = getMap(page, resourceResolver, excludedContentTypes, ctaLabel, resources);
   }
  }
  return map;
 }
 
 /**
  * Adds the child map to list.
  * 
  * @param listMap
  *         the list map
  * @param childMap
  *         the child map
  */
 private void addChildMapToList(List<Map<String, Object>> listMap, Map<String, Object> childMap) {
  if (childMap != null && childMap.size() > 0) {
   listMap.add(childMap);
  }
 }
 
 /**
  * List children.
  * 
  * @param page
  *         the page
  * @param maxDepth
  *         the max depth
  * @param currentDepth
  *         the current depth
  * @param content
  *         the content
  * @param resources
  *         the resources
  * @param excludedContentTypes
  *         the excluded content types
  * @param browseAll
  *         the browse all
  * @param displayBackgroundImage
  * @return the map
  */
 private Map<String, Object> listChildren(Page page, int maxDepth, int currentDepth, final Map<String, Object> content,
   final Map<String, Object> resources, String[] excludedContentTypes, String browseAll, Boolean displayBackgroundImage) {
  Map<String, Object> map = null;
  int depth = currentDepth;
  int depthMax = maxDepth;
  Map<String, Object> currentPageProperties = new HashMap<String, Object>();
  if (page != null) {
   Iterator<Page> iterator = page.listChildren();
   List<Map<String, Object>> listMap = null;
   if (depthMax > 1) {
    int counter = --depthMax;
    listMap = new ArrayList<Map<String, Object>>();
    int currentLevel = depth++;
    while (iterator.hasNext()) {
     Map<String, Object> childMap = listChildren(iterator.next(), counter, depth, content, resources, excludedContentTypes, browseAll,
       displayBackgroundImage);
     addChildMapToList(listMap, childMap);
    }
    Map<String, Object> map1 = getMap(page, getResourceResolver(resources), excludedContentTypes, StringUtils.EMPTY, resources);
    if (map1 != null && map1.size() > 0) {
     map1.put(BROWSE_ALL, browseAll);
     map1.put(DISPLAY_BACKGROUND_IMAGE, displayBackgroundImage);
     map1.put(LEVEL + currentLevel, listMap.toArray());
     Image image = new Image((String) currentPageProperties.get(TEASER_IMAGE), StringUtils.EMPTY, StringUtils.EMPTY, getCurrentPage(resources),
       getSlingRequest(resources));
     map1.put(TEASER_IMAGE, image.convertToMap());
     map = map1;
    }
    
   } else {
    map = getMap(page, getResourceResolver(resources), excludedContentTypes, StringUtils.EMPTY, resources);
   }
  }
  return map;
 }
 
 /**
  * Gets the map.
  * 
  * @param page
  *         the page
  * @param resourceResolver
  *         the resource resolver
  * @param excludedContentTypes
  *         the excluded content types
  * @param ctaLabel
  *         the cta label
  * @param resources
  *         the resources
  * @return the map
  */
 private Map<String, Object> getMap(Page page, ResourceResolver resourceResolver, String[] excludedContentTypes, String ctaLabel,
   Map<String, Object> resources) {
  Map<String, Object> map = new HashMap<String, Object>();
  Map<String, Object> currentPageProperties = page.getProperties();
  if (isPageValidForGlobalnavigation(page, excludedContentTypes)) {
   String title = page.getNavigationTitle();
   if (StringUtils.isBlank(title)) {
    title = page.getTitle();
   }
   if (StringUtils.isEmpty(ctaLabel)) {
    map.put(LABEL_TEXT, title);
   } else {
    map.put(LABEL_TEXT, ctaLabel);
   }
   String fullPagePath = ComponentUtil.getFullURL(resourceResolver, page.getPath(), getSlingRequest(resources));
   map.put(URL, fullPagePath);
   Image image = new Image((String) currentPageProperties.get(TEASER_IMAGE), StringUtils.EMPTY, StringUtils.EMPTY, getCurrentPage(resources),
     getSlingRequest(resources));
   map.put(TEASER_IMAGE, image.convertToMap());
  }
  return map;
 }
 
 /**
  * Gets the search properties.
  * 
  * @param content
  *         the content
  * @param resources
  *         the resources
  * @return the search properties
  */
 private Map<String, Object> getSearchProperties(final Map<String, Object> content, final Map<String, Object> resources) {
  Map<String, Object> searchProperties = new HashMap<String, Object>();
  String searchLogoPath = MapUtils.getString(getProperties(content), SEARCH_LOGO_PATH);
  String searchLogoAlt = MapUtils.getString(getProperties(content), SEARCH_LOGO_ALT_TEXT);
  Map<String, Object> searchLogoProperties = new LinkedHashMap<String, Object>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  if (StringUtils.isNotBlank(searchLogoPath)) {
   searchLogoProperties.put(FILE_REFERENCE,
     ComponentUtil.getStaticFileFullPath(searchLogoPath, getCurrentPage(resources), getSlingRequest(resources)));
   searchLogoProperties.put(ALT_TEXT, searchLogoAlt);
   searchLogoProperties.put(FILENAME, ImageHelper.fetchImageFileName(searchLogoPath));
   searchLogoProperties.put(EXTENSION, ImageHelper.getExtensionFromMimeType(ImageHelper.getFileExtension(searchLogoPath)));
   searchProperties.put(SEARCH_LOGO, searchLogoProperties);
  }
  searchProperties.put(HEADLINE, getI18n(resources).get("search.headingtext"));
  searchProperties.put(SEARCH_INPUT,
    getSearchInputProperties(getI18n(resources), getResourceResolver(resources), getCurrentPage(resources), getSlingRequest(resources)));
  searchProperties.put(DROPDOWN_LABEL_MAP, getDropDownLabels(getI18n(resources)));
  LOGGER.debug("Search Properties map corresponding to Global Navigation is: " + searchProperties);
  return searchProperties;
 }
 
 /**
  * Gets the drop down labels.
  * 
  * @param i18n
  *         the i18n
  * @return the drop down labels
  */
 private Map<String, Object> getDropDownLabels(I18n i18n) {
  Map<String, Object> filterMap = new HashMap<String, Object>();
  filterMap.put("inEverything", i18n.get("searchListing.everythingLabel"));
  filterMap.put("inProducts", i18n.get("searchListing.productLabel"));
  filterMap.put("inArticles", i18n.get("searchListing.articleLabel"));
  return filterMap;
 }
 
 /**
  * Gets the brand logo properties.
  * 
  * @param content
  *         the content
  * @param resources
  *         the resources
  * @return the brand logo properties
  */
 private Map<String, Object> getBrandLogoProperties(final Map<String, Object> content, final Map<String, Object> resources) {
  
  Map<String, Object> brandLogoProperties = new LinkedHashMap<String, Object>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  
  String brandLogoPath = MapUtils.getString(getProperties(content), BRAND_LOGO_IMAGE, StringUtils.EMPTY);
  brandLogoPath = ComponentUtil.getStaticFileFullPath(brandLogoPath, getCurrentPage(resources), getSlingRequest(resources));
  String brandLogoAlt = MapUtils.getString(getProperties(content), BRAND_ALT_TEXT, StringUtils.EMPTY);
  brandLogoProperties.put(ALT_TEXT, brandLogoAlt);
  brandLogoProperties.put(EXTENSION, ImageHelper.getExtensionFromMimeType(ImageHelper.getFileExtension(brandLogoPath)));
  brandLogoProperties.put(FILENAME, ImageHelper.fetchImageFileName(brandLogoPath));
  brandLogoProperties.put(FILE_REFERENCE, brandLogoPath);
  brandLogoProperties.put(IMAGE_URL, brandLogoPath);
  String homePageUrl = ComponentUtil.getFullURL(getResourceResolver(resources), getCurrentPage(resources).getPath(), getSlingRequest(resources));
  if (StringUtils.isNotBlank(homePageUrl)) {
   homePageUrl = StringUtils.substringBefore(homePageUrl, getCurrentPage(resources).getPath());
  }
  if (StringUtils.isEmpty(homePageUrl)) {
   homePageUrl = UnileverConstants.FORWARD_SLASH;
  }
  brandLogoProperties.put(TARGET, homePageUrl);
  LOGGER.debug("Brand Logo Properties map corresponding to Global Navigation is: " + brandLogoProperties);
  
  return brandLogoProperties;
 }
 
 /**
  * Gets the search input properties.
  * 
  * @param i18n
  *         the i18n
  * @param resourceResolver
  *         the resource resolver
  * @param currentPage
  *         the current page
  * @param request
  *         the request
  * @return the search input properties
  */
 private Map<String, String> getSearchInputProperties(I18n i18n, ResourceResolver resourceResolver, Page currentPage, SlingHttpServletRequest request) {
  String searchPath = getSerachPath(currentPage, SEARCH_PAGE_KEY_NAME);
  searchPath = ComponentUtil.getFullURL(resourceResolver, searchPath, request);
  Map<String, String> countryConfig = new HashMap<String, String>();
  Map<String, String> searchInput = new LinkedHashMap<String, String>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  try {
   String suggestionHandlerUrl = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, "searchInput",
     "searchSuggestionHandler");
   countryConfig = configurationService.getCategoryConfiguration(currentPage, "countryConfiguration");
   searchInput.put(LABEL_TEXT, i18n.get("search.label"));
   searchInput.put(PLACEHOLDER, i18n.get("search.type.something.here"));
   if (StringUtils.isNotEmpty(suggestionHandlerUrl)) {
    searchInput.put(SUGGESTION_SERVLET, suggestionHandlerUrl);
   } else {
    searchInput.put(SUGGESTION_SERVLET, "/bin/search/suggestionhandler");
   }
   searchInput.put(TARGET, searchPath);
   searchInput.put(KEYWORD, "");
   searchInput.put(FORM_NAME, "Global Search");
   searchInput.put(COUNTRYCODE, countryConfig.get("code"));
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Exception in READING CONFIGURATIONS  .............." + ExceptionUtils.getStackTrace(e));
  }
  return searchInput;
 }
 
 /**
  * Gets the navigation items.
  * 
  * @param content
  *         the content
  * @param resources
  *         the resources
  * @param browseAll
  *         the browse all
  * @return the navigation items
  */
 private List<Map<String, Object>> getNavigationItems(final Map<String, Object> content, final Map<String, Object> resources, String browseAll) {
  LOGGER.debug("Inside getNavigationItems method of Global Navigation to generate navigation items");
  
  Map<String, Object> dialogProperties = getProperties(content);
  String[] excludedContentTypes = getExcludedContentTypes(getCurrentResource(resources));
  String maximumDepth = MapUtils.getString(getProperties(content), MAXIMUM_DEPTH);
  int maxDepth = Integer.valueOf(StringUtils.defaultString(maximumDepth, "0"));
  ValueMap valueMap = getCurrentResource(resources).adaptTo(ValueMap.class);
  PageManager pageManager = getResourceResolver(resources).adaptTo(PageManager.class);
  List<Map<String, Object>> level1List = new ArrayList<Map<String, Object>>();
  for (int navCount = 0; navCount < valueMap.get(NUMBER_OF_TABS, Integer.class); navCount++) {
   String selectTab = valueMap.get(SELECT_TAB + (navCount + 1), String.class);
   Map<String, Object> map = null;
   Boolean displayBackgroundImage = MapUtils.getBoolean(dialogProperties, DISPLAY_BACKGROUND_IMAGE + (navCount + 1), false);
   
   if (StringUtils.isNotEmpty(selectTab) && selectTab.equals(MANUAL)) {
    map = getManualLinkProperties(navCount, maxDepth, resources, excludedContentTypes, browseAll, displayBackgroundImage);
    LOGGER.debug("Manual Navigation list corresponding to Global Navigation is successfully generated");
   } else if (AUTO.equals(selectTab)) {
    String parentPage = MapUtils.getString(getProperties(content), PARENT_PAGE + (navCount + 1));
    Page page = pageManager.getPage(parentPage);
    map = listChildren(page, maxDepth, com.unilever.platform.aem.foundation.constants.CommonConstants.TWO, content, resources, excludedContentTypes,
      browseAll, displayBackgroundImage);
    LOGGER.debug("Auto Navigation list corresponding to Global Navigation is successfully generated");
   } else {
    map = getCustomLinkProperties(valueMap, getResourceResolver(resources), navCount, browseAll, resources, displayBackgroundImage);
    LOGGER.debug("Custom Navigation list corresponding to Global Navigation is successfully generated");
   }
   
   if ((map != null) && (!map.isEmpty())) {
    map.put("backgroundImage", getBackgroundImageMap(navCount, content, resources));
    level1List.add(map);
   }
  }
  
  return level1List;
 }
 
 /**
  * Gets the background image map.
  * 
  * @param navCount
  *         the nav count
  * @param content
  *         the content
  * @param resources
  *         the resources
  * @return the background image map
  */
 private Map<String, Object> getBackgroundImageMap(int navCount, Map<String, Object> content, Map<String, Object> resources) {
  Map<String, Object> dialogProperties = getProperties(content);
  Map<String, Object> backgroundImageMap = new HashMap<String, Object>();
  Page currentPage = getCurrentPage(resources);
  
  Boolean displayBackgroundImage = MapUtils.getBoolean(dialogProperties, DISPLAY_BACKGROUND_IMAGE + (navCount + 1), false);
  backgroundImageMap.put("displayBackgroundImage", displayBackgroundImage);
  if (displayBackgroundImage.equals(true)) {
   String backgroundImagePath = MapUtils.getString(dialogProperties, BACKGROUND_IMAGE + (navCount + 1), StringUtils.EMPTY);
   String backgroundImageAltText = MapUtils.getString(dialogProperties, BACKGROUND_IMAGE_ALT_TEXT + (navCount + 1), StringUtils.EMPTY);
   Image image = new Image(backgroundImagePath, backgroundImageAltText, StringUtils.EMPTY, currentPage, getSlingRequest(resources));
   backgroundImageMap.put("image", image.convertToMap());
  }
  
  return backgroundImageMap;
 }
 
 /**
  * Gets the excluded content types.
  * 
  * @param currentResource
  *         the current resource
  * @return excludedContentTypes
  */
 private String[] getExcludedContentTypes(Resource currentResource) {
  
  String[] excludedContentTypes = null;
  List<Map<String, String>> manualList = ComponentUtil.getNestedMultiFieldProperties(currentResource, "contentTypeMultifield");
  if (!manualList.isEmpty()) {
   int size = manualList.size();
   excludedContentTypes = new String[size];
   
   for (Map<String, String> map : manualList) {
    excludedContentTypes[--size] = map.get(CONTENT_TYPE);
   }
  }
  if (ArrayUtils.isEmpty(excludedContentTypes)) {
   excludedContentTypes = ArrayUtils.EMPTY_STRING_ARRAY;
  }
  return excludedContentTypes;
 }
 
 /**
  * Gets the manual link properties.
  * 
  * @param navCount
  *         the nav count
  * @param maxDepth
  *         the max depth
  * @param resources
  *         the resources
  * @param excludedContentTypes
  *         the excluded content types
  * @param browseAll
  *         the browse all
  * @param displayBackgroundImage
  * @return the manual link properties
  */
 private Map<String, Object> getManualLinkProperties(int navCount, int maxDepth, final Map<String, Object> resources, String[] excludedContentTypes,
   String browseAll, Boolean displayBackgroundImage) {
  
  List<Map<String, Object>> manualNavList = new ArrayList<Map<String, Object>>();
  Map<String, Object> manualNavigationProperties = new LinkedHashMap<String, Object>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  ValueMap valueMap = getCurrentResource(resources).adaptTo(ValueMap.class);
  PageManager pageManager = getPageManager(resources);
  
  List<Map<String, String>> manualList = ComponentUtil.getNestedMultiFieldProperties(getCurrentResource(resources), "navItemLink" + (navCount + 1));
  if (manualList != null) {
   int navManualFiedlSize = manualList.size();
   for (int i = 0; i < navManualFiedlSize; i++) {
    String linkUrl = manualList.get(i).get(LINK_URL + (navCount + 1));
    String openInNewWindow = manualList.get(i).get(OPEN_IN_NEW_WINDOW + (navCount + 1));
    String ctaLabel = manualList.get(i).get(CTA_LABEL + (navCount + 1));
    Page page = pageManager.getPage(linkUrl);
    Map<String, Object> map = new HashMap<String, Object>();
    
    // if linkUrl is external url, page will be null. So getExternalUrlMap will be used to create map in this case.
    if (page != null) {
     map = listChildren(page, maxDepth, com.unilever.platform.aem.foundation.constants.CommonConstants.THREE, resources, excludedContentTypes,
       browseAll, openInNewWindow, ctaLabel, displayBackgroundImage);
    } else {
     map = getExternalUrlMap(linkUrl, openInNewWindow, ctaLabel, browseAll, displayBackgroundImage);
    }
    addChildMapToList(manualNavList, map);
   }
  }
  if (valueMap != null) {
   manualNavigationProperties.put(LABEL_TEXT, valueMap.get(TITLE + (navCount + 1), String.class));
  }
  manualNavigationProperties.put(BROWSE_ALL, browseAll);
  if (manualNavList != null && !manualNavList.isEmpty()) {
   manualNavigationProperties.put(LEVEL_TWO, manualNavList.toArray());
  }
  
  return manualNavigationProperties;
 }
 
 /**
  * This method returns Map for external URLs.
  * 
  * @param linkUrl
  *         the urls of external link
  * @param openInNewWindow
  *         the flag open in new window
  * @param ctaLabel
  *         the label of the link
  * @param browseAll
  *         the browse all
  * @param displayBackgroundImage
  * @return externalUrlMap Map for external url
  */
 private Map<String, Object> getExternalUrlMap(String linkUrl, String openInNewWindow, String ctaLabel, String browseAll,
   Boolean displayBackgroundImage) {
  Map<String, Object> externalUrlMap = new LinkedHashMap<String, Object>();
  
  externalUrlMap.put("browseAll", browseAll);
  externalUrlMap.put("url", linkUrl);
  externalUrlMap.put(DISPLAY_BACKGROUND_IMAGE, displayBackgroundImage);
  externalUrlMap.put(LABEL_TEXT, ctaLabel);
  externalUrlMap.put(OPEN_IN_NEW_WINDOW_TEXT, openInNewWindow);
  
  return externalUrlMap;
 }
 
 /**
  * Gets the custom link properties.
  * 
  * @param valueMap
  *         the value map
  * @param resourceResolver
  *         the resource resolver
  * @param navCount
  *         the nav count
  * @param browseAll
  *         the browse all
  * @param resources
  *         the resources
  * @param displayBackgroundImage
  * @return the custom link properties
  */
 private Map<String, Object> getCustomLinkProperties(ValueMap valueMap, ResourceResolver resourceResolver, int navCount, String browseAll,
   Map<String, Object> resources, Boolean displayBackgroundImage) {
  Map<String, Object> level2Map = new HashMap<String, Object>();
  String slideType = valueMap.get(CUSTOM_SLIDE_TYPE + (navCount + 1), String.class);
  if (StringUtils.isNotBlank(slideType)) {
   
   Resource resource = resourceResolver.getResource(slideType);
   List<Map<String, Object>> columnList = new ArrayList<Map<String, Object>>();
   if (resource != null) {
    String resourceType = resource.getResourceType();
    String resourceTypeName = resourceType != null ? StringUtils.substring(resourceType, resourceType.lastIndexOf("/") + 1, resourceType.length())
      : "";
    level2Map.put(CUSTOM, true);
    level2Map.put(BROWSE_ALL, browseAll);
    level2Map.put(DISPLAY_BACKGROUND_IMAGE, displayBackgroundImage);
    switch (resourceTypeName) {
     case "categoryOverview":
      handleCategoryOverview(browseAll, resources, level2Map, resource, columnList, displayBackgroundImage);
      break;
     
     case "sectionNavigationV2":
      handleSectionNavigation(browseAll, resources, level2Map, resource, columnList, displayBackgroundImage);
      break;
     default: break;
    }
   }
   
  }
  return level2Map;
 }
 
 /**
  * Handle section navigation.
  * 
  * @param browseAll
  *         the browse all
  * @param resources
  *         the resources
  * @param level2Map
  *         the level2 map
  * @param resource
  *         the resource
  * @param columnList
  *         the column list
  * @param displayBackgroundImage
  */
 private void handleSectionNavigation(String browseAll, Map<String, Object> resources, Map<String, Object> level2Map, Resource resource,
   List<Map<String, Object>> columnList, Boolean displayBackgroundImage) {
  ValueMap sectionProp = resource.adaptTo(ValueMap.class);
  
  String heading = MapUtils.getString(sectionProp, "heading", "");
  level2Map.put(LABEL_TEXT, heading);
  String buildNavigationUsing = MapUtils.getString(sectionProp, "buildNavigationUsing", "");
  
  if (StringUtils.equals(buildNavigationUsing, "fixedList")) {
   handleSectionNavigationFixedList(browseAll, resources, level2Map, resource, columnList, displayBackgroundImage);
  } else if (StringUtils.equals(buildNavigationUsing, "childPages")) {
   handleSectionNavigationChildPages(browseAll, resources, level2Map, resource, columnList, displayBackgroundImage);
  }
 }
 
 /**
  * Handle section navigation child pages.
  * 
  * @param browseAll
  *         the browse all
  * @param resources
  *         the resources
  * @param level2Map
  *         the level2 map
  * @param resource
  *         the resource
  * @param columnList
  *         the column list
  * @param sectionProp
  *         the section prop
  * @param displayBackgroundImage
  */
 private void handleSectionNavigationChildPages(String browseAll, Map<String, Object> resources, Map<String, Object> level2Map, Resource resource,
   List<Map<String, Object>> columnList, Boolean displayBackgroundImage) {
  List<Map<String, String>> primaryList = ComponentUtil.getNestedMultiFieldProperties(resource, "sections");
  List<Map<String, String>> excludeContentTypeList = ComponentUtil.getNestedMultiFieldProperties(resource, "contentTypeJson");
  Iterator<Map<String, String>> itr = excludeContentTypeList.iterator();
  List<String> excludedContentTypeList = new ArrayList<String>();
  if (itr.hasNext()) {
   Map<String, String> excludeContentTypeMap = itr.next();
   String excludeContentType = MapUtils.getString(excludeContentTypeMap, "excludeContentType");
   excludedContentTypeList.add(excludeContentType);
  }
  
  for (Map<String, String> map : primaryList) {
   String subHeading = MapUtils.getString(map, "navigationListTitle", "");
   Map<String, Object> columnMap = new LinkedHashMap<String, Object>();
   columnMap.put(LABEL_TEXT, subHeading);
   columnMap.put(BROWSE_ALL, browseAll);
   columnMap.put(DISPLAY_BACKGROUND_IMAGE, displayBackgroundImage);
   List<Map<String, Object>> list = getSectionLinks(map, resources, excludedContentTypeList);
   if (CollectionUtils.isNotEmpty(list)) {
    columnMap.put(LEVEL_THREE, list.toArray());
   }
   
   columnList.add(columnMap);
  }
  if (CollectionUtils.isNotEmpty(columnList)) {
   level2Map.put(LEVEL_TWO, columnList.toArray());
  }
  
 }
 
 /**
  * Gets the section links.
  * 
  * @param map
  *         the map
  * @param resources
  *         the resources
  * @param excludedContentTypeList
  *         the excluded content type list
  * @return the section links
  */
 private List<Map<String, Object>> getSectionLinks(Map<String, String> map, Map<String, Object> resources, List<String> excludedContentTypeList) {
  
  String parentPageUrl = MapUtils.getString(map, "parentPageUrl");
  PageManager pageManager = getPageManager(resources);
  Page parentPage = pageManager.getPage(parentPageUrl);
  List<Map<String, Object>> customLinks = new ArrayList<Map<String, Object>>();
  if (parentPage != null) {
   Iterator<Page> itr = parentPage.listChildren();
   while (itr.hasNext()) {
    Page childPage = itr.next();
    ValueMap properties = childPage.getProperties();
    String contentType = MapUtils.getString(properties, "contentType", "");
    if (("").equals(contentType) || !excludedContentTypeList.contains(contentType)) {
     Map<String, Object> linkMap = new LinkedHashMap<String, Object>();
     linkMap.put(LABEL_TEXT, childPage.getTitle());
     linkMap.put("url", ComponentUtil.getFullURL(getResourceResolver(resources), childPage.getPath(), getSlingRequest(resources)));
     linkMap.put(OPEN_IN_NEW_WINDOW_TEXT, "No");
     String teaserImagePath = MapUtils.getString(childPage.getProperties(), UnileverConstants.TEASER_IMAGE, "");
     String teaserImageAltText = MapUtils.getString(childPage.getProperties(), UnileverConstants.TEASER_IMAGE_ALT_TEXT, "");
     String teaserTitle = MapUtils.getString(childPage.getProperties(), UnileverConstants.TEASER_TITLE, "");
     Image teaserImage = new Image(teaserImagePath, teaserImageAltText, teaserTitle, getCurrentPage(resources), getSlingRequest(resources));
     linkMap.put("teaserImage", teaserImage.convertToMap());
     customLinks.add(linkMap);
    }
    
   }
  }
  
  return customLinks;
 }
 
 /**
  * Handle section navigation fixed list.
  * 
  * @param browseAll
  *         the browse all
  * @param resources
  *         the resources
  * @param level2Map
  *         the level2 map
  * @param resource
  *         the resource
  * @param columnList
  *         the column list
  * @param sectionProp
  *         the section prop
  * @param displayBackgroundImage
  */
 private void handleSectionNavigationFixedList(String browseAll, Map<String, Object> resources, Map<String, Object> level2Map, Resource resource,
   List<Map<String, Object>> columnList, Boolean displayBackgroundImage) {
  List<Map<String, String>> primaryList = ComponentUtil.getNestedMultiFieldProperties(resource, "fixedList");
  for (Map<String, String> map : primaryList) {
   String subHeading = MapUtils.getString(map, "navigationLinkTitle", "");
   List<Map<String, Object>> linkList = CategoryOverviewHelper.convertToListMap(map.get("listLinks"), resources);
   
   Map<String, Object> columnMap = new LinkedHashMap<String, Object>();
   columnMap.put(LABEL_TEXT, subHeading);
   columnMap.put(BROWSE_ALL, browseAll);
   columnMap.put(DISPLAY_BACKGROUND_IMAGE, displayBackgroundImage);
   List<Map<String, Object>> list = getLinks(linkList);
   addListToColumnMap(columnMap, list);
   columnList.add(columnMap);
  }
  if (columnList != null && !columnList.isEmpty()) {
   level2Map.put(LEVEL_TWO, columnList.toArray());
  }
 }
 
 /**
  * Handle category overview.
  * 
  * @param browseAll
  *         the browse all
  * @param resources
  *         the resources
  * @param level2Map
  *         the level2 map
  * @param resource
  *         the resource
  * @param columnList
  *         the column list
  * @param displayBackgroundImage
  */
 private void handleCategoryOverview(String browseAll, Map<String, Object> resources, Map<String, Object> level2Map, Resource resource,
   List<Map<String, Object>> columnList, Boolean displayBackgroundImage) {
  ValueMap categoryProperties = resource.adaptTo(ValueMap.class);
  
  String heading = categoryProperties.get("heading", String.class);
  
  List<Map<String, String>> primaryList = ComponentUtil.getNestedMultiFieldProperties(resource, "navigationLinks");
  for (Map<String, String> map : primaryList) {
   String subHeading = map.get("subHeading");
   List<Map<String, Object>> categoryLinkList = CategoryOverviewHelper.convertToListMap(map.get(CategoryOverviewHelper.CATEGORY_OVERVIEW_LINKS),
     resources);
   
   Map<String, Object> columnMap = new LinkedHashMap<String, Object>();
   columnMap.put(LABEL_TEXT, subHeading);
   columnMap.put(BROWSE_ALL, browseAll);
   columnMap.put(DISPLAY_BACKGROUND_IMAGE, displayBackgroundImage);
   List<Map<String, Object>> list = getLinks(categoryLinkList);
   addListToColumnMap(columnMap, list);
   columnList.add(columnMap);
  }
  
  level2Map.put(LABEL_TEXT, heading);
  
  if (columnList != null && !columnList.isEmpty()) {
   level2Map.put(LEVEL_TWO, columnList.toArray());
  }
 }
 
 /**
  * Adds the list to column map.
  * 
  * @param columnMap
  *         the column map
  * @param list
  *         the list
  */
 private void addListToColumnMap(Map<String, Object> columnMap, List<Map<String, Object>> list) {
  if (list != null && !list.isEmpty()) {
   columnMap.put(LEVEL_THREE, list.toArray());
  }
 }
 
 /**
  * Gets the category links.
  * 
  * @param categoryLinkList
  *         the category link list
  * @return the category links
  */
 @SuppressWarnings("unchecked")
 private List<Map<String, Object>> getLinks(List<Map<String, Object>> categoryLinkList) {
  LOGGER.debug("Inside getCategoryLinks method of Global Navigation to fetch category overview navigation list");
  
  List<Map<String, Object>> customLinks = new ArrayList<Map<String, Object>>();
  for (Map<String, Object> map2 : categoryLinkList) {
   Object linkMap = map2.get("link");
   if (linkMap instanceof Map<?, ?>) {
    customLinks.add((Map<String, Object>) linkMap);
   }
  }
  
  return customLinks;
 }
 
 /**
  * Checks if is page valid for globalnavigation.
  * 
  * @param page
  *         the page
  * @param excludedContentTypes
  *         the excluded content types
  * @return true, if is page valid for globalnavigation
  */
 private boolean isPageValidForGlobalnavigation(Page page, String[] excludedContentTypes) {
  boolean isValidPage = true;
  if (page != null && page.getContentResource() != null) {
   ValueMap pageProperties = page.getContentResource().adaptTo(ValueMap.class);
   
   String pageContentType = pageProperties.get(CONTENT_TYPE, String.class);
   if ((StringUtils.isNotBlank(pageContentType) && ArrayUtils.indexOf(excludedContentTypes, pageContentType) != -1) || page.isHideInNav()) {
    isValidPage = false;
   }
  } else {
   isValidPage = false;
  }
  return isValidPage;
 }
}
