/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.components.helper;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;
import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.aem.foundation.dam.DAMAssetUtility;
import com.unilever.platform.foundation.template.dto.SocialTabQueryDTO;

/**
 * The Class DAMAssetsUtility.
 */

@Service(value = { SocialTABHelper.class })
public final class SocialTABHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(SocialTABHelper.class);
 
 /** The Constant HASH_TAG. */
 private static final String HASH_TAG = "hashTag";
 
 /** The Constant SOCIAL_CHANNELS. */
 private static final String SOCIAL_CHANNELS = "socialChannels";
 
 private static final String OPEN_IN_NEW_WINDOW = "openInNewWindow";
 
 /**
  * Instantiates a new social tab helper.
  */
 private SocialTABHelper() {
  LOGGER.info("Social Tab Helper Constructor Called.");
 }
 
 /**
  * Gets the image resource.
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param imagePath
  *         the image path
  * @return the image resource
  */
 public static Asset getImageResource(ResourceResolver resourceResolver, String imagePath) {
  
  Asset asset = null;
  Resource resource = resourceResolver.getResource(imagePath);
  if (resource != null) {
   asset = resource.adaptTo(Asset.class);
  }
  return asset;
 }
 
 /**
  * Gets the image map with metadata.
  * 
  * @param imagePath
  *         the image path
  * @param page
  *         the page
  * @param resourceResolver
  *         the resource resolver
  * @param slingRequest
  *         the sling request
  * @return the image map with metadata
  */
 public static Map<String, Object> getImageMapWithMetadata(String imagePath, Page page, ResourceResolver resourceResolver,
   SlingHttpServletRequest slingRequest) {
  Map<String, Object> imgMetaMap = new LinkedHashMap<String, Object>();
  
  Asset asset = getImageResource(resourceResolver, imagePath);
  if (asset != null) {
   imgMetaMap = DAMAssetUtility.getImageMetadataMapFromAsset(asset, page, resourceResolver, slingRequest);
  }
  return imgMetaMap;
 }
 
 /**
  * Adds the retrieval query parametrs map.
  * 
  * @param map
  *         the map
  * @param socialTabQuery
  *         the social tab query
  * @param urlFlag
  *         the urlFlag
  */
 public static void addRetrievalQueryParametrsMap(Map<String, Object> map, SocialTabQueryDTO socialTabQuery, boolean urlFlag) {
  
  Map<String, Object> requestServletUrlMap = new LinkedHashMap<String, Object>();
  
  requestServletUrlMap.put("numberOfPosts", socialTabQuery.getNoOfPost());
  requestServletUrlMap.put("maximumImages", socialTabQuery.getMaxImages());
  requestServletUrlMap.put("brandNameParameter", socialTabQuery.getBrandNameParameter());
  if (!urlFlag) {
   requestServletUrlMap.put("requestServletUrl", "/bin/public/cms/assetsearch" + ".damasset.json");
  } else {
   requestServletUrlMap.put("requestServletUrl", socialTabQuery.getPagePath());
  }
  
  addHashTagsMap(requestServletUrlMap, socialTabQuery.getHashTags());
  addSocialChannelsMap(requestServletUrlMap, socialTabQuery.getSocialChannels(), socialTabQuery.getPage(), socialTabQuery.getConfigurationService());
  
  map.put("retrivalQueryParams", requestServletUrlMap);
 }
 
 /**
  * Adds the retrieval query parametrs map.
  * 
  * @param map
  *         the map
  * @param socialTabQuery
  *         the social tab query
  */
 public static void addRetrievalQueryParametrsMap(Map<String, Object> map, SocialTabQueryDTO socialTabQuery) {
  addRetrievalQueryParametrsMap(map, socialTabQuery, false);
 }
 
 /**
  * Adds the hash tags map.
  * 
  * @param requestServletUrlMap
  *         the request servlet url map
  * @param hashTags
  *         the hash tags
  */
 private static void addHashTagsMap(Map<String, Object> requestServletUrlMap, List<Map<String, String>> hashTags) {
  List<Map<String, Object>> hashTagList = new ArrayList<Map<String, Object>>();
  
  for (Map<String, String> map : hashTags) {
   Map<String, Object> hashTagMap = new LinkedHashMap<String, Object>();
   
   String hashTag = StringUtils.defaultString(map.get(HASH_TAG), StringUtils.EMPTY);
   hashTag = hashTag.trim().replaceFirst("#", "");
   hashTagMap.put("Id", hashTag);
   
   hashTagList.add(hashTagMap);
  }
  
  requestServletUrlMap.put("hashTags", hashTagList.toArray());
 }
 
 /**
  * Adds the social channels map.
  * 
  * @param requestServletUrlMap
  *         the request servlet url map
  * @param socialChannels
  *         the social channels
  * @param currentPage
  *         the current page
  * @param configurationService
  *         the configuration service
  */
 public static void addSocialChannelsMap(Map<String, Object> requestServletUrlMap, List<String> socialChannels, Page currentPage,
   ConfigurationService configurationService) {
  List<Map<String, Object>> socialChannelsList = new ArrayList<Map<String, Object>>();
  
  for (String channel : socialChannels) {
   socialChannelsList.add(getSocialChannelMap(channel.toLowerCase(), currentPage, configurationService));
  }
  requestServletUrlMap.put("socialChannelNames", socialChannelsList.toArray());
 }
 
 /**
  * Gets the social channel map.
  * 
  * @param channel
  *         the channel
  * @param currentPage
  *         the current page
  * @param configurationService
  *         the configuration service
  * @return the social channel map
  */
 public static Map<String, Object> getSocialChannelMap(String channel, Page currentPage, ConfigurationService configurationService) {
  Map<String, Object> socialChannelsMap = new LinkedHashMap<String, Object>();
  Map<String, String> channelsConfigs = GlobalConfigurationUtility.getMapFromConfiguration(configurationService, currentPage, channel.toLowerCase());
  
  socialChannelsMap.put("name", channel);
  socialChannelsMap.put("url", channelsConfigs.get("Url"));
  socialChannelsMap.put("imagePath", channelsConfigs.get("Logo"));
  
  return socialChannelsMap;
 }
 
 /**
  * Gets the social channels configuration list.
  * 
  * @param configurationService
  *         the configuration service
  * @param currentPage
  *         the current page
  * @return the social channels configuration list
  */
 public static List<Map<String, Object>> getSocialChannelsConfigurationList(ConfigurationService configurationService, Page currentPage) {
  Map<String, String> channelMap = GlobalConfigurationUtility.getMapFromConfiguration(configurationService, currentPage, SOCIAL_CHANNELS);
  List<Map<String, Object>> brandSocialChannelList = new LinkedList<Map<String, Object>>();
  
  for (String key : channelMap.keySet()) {
   brandSocialChannelList.add(getSocialChannelMap(key, currentPage, configurationService));
  }
  return brandSocialChannelList;
 }
 
 /**
  * Show cta map.
  * 
  * @param properties
  *         the properties
  * @param tabQuery
  *         the tab query
  * @param ctaLabel
  *         the cta label
  * @param ctaUrl
  *         the cta url
  * @param configurationService
  *         the configuration service
  * @param currentPage
  *         the current page
  * @param openInNewWindow
  *         the open in new window
  * @return the map
  */
 public static Map<String, Object> showCtaMap(Map<String, Object> properties, String ctaLabel, String ctaUrl, String openInNewWindow) {
  Map<String, Object> ctaMap = new LinkedHashMap<String, Object>();
  
  String url = MapUtils.getString(properties, ctaUrl, StringUtils.EMPTY);
  if (StringUtils.isNotBlank(url)) {
   ctaMap.put("isShowCTA", true);
  } else {
   ctaMap.put("isShowCTA", false);
  }
  ctaMap.put("url", url);
  ctaMap.put("ctaLabel", ctaLabel);
  ctaMap.put(OPEN_IN_NEW_WINDOW, Boolean.valueOf(openInNewWindow));
  return ctaMap;
 }
}
