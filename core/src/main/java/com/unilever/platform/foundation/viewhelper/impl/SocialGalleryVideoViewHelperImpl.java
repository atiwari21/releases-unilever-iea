/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.SocialTABHelper;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.Image;
import com.unilever.platform.foundation.template.dto.SocialTabQueryDTO;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * This class prepares the data map for Social Gallery Image Video Component. It reads the values from the dialog and passes the info as a json.
 */

@Component(description = "SocialGalleryVideo", immediate = true, metatype = true, label = "Social Gallery Video")
@Service(value = { SocialGalleryVideoViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.SOCIAL_GALLERY_VIDEO, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class SocialGalleryVideoViewHelperImpl extends SocialTabBaseViewHelperImpl {
 
 /**
  * logger object.
  **/
 private static final Logger LOGGER = LoggerFactory.getLogger(SocialGalleryVideoViewHelperImpl.class);
 
 /** The Constant SOCIAL_GALLERY_VIDEOS. */
 private static final String SOCIAL_GALLERY_VIDEOS = "socialGalleryVideoPostsJson";
 /** The Constant SOCIAL_GALLERY_VIDEOS. */
 private static final String SOCIAL_GALLERY_SOCIAL_CHANNEL = "socialChannels";
 /** The Constant SOCIAL_GALLERY_VIDEO_ID. */
 private static final String SOCIAL_GALLERY_SOCIAL_VIDEO_ID = "videoId";
 /** The Constant READ_ARTICLE_CTA_URL. */
 private static final String READ_ARTICLE_CTA_URL = "readArticleCtaUrl";
 /** The Constant IS_IMAGE. */
 private static final String IS_IMAGE = "image";
 /** The Constant IS_VIDEO. */
 private static final String IS_VIDEO = "video";
 /** The Constant CHANNEL_OR_PLAYLIST_ID. */
 private static final String CHANNEL_OR_PLAYLIST_ID = "channelOrPlayListId";
 /** The Constant IMAGE. */
 private static final String IMAGE = "image";
 /** The Constant VIDEO. */
 private static final String VIDEO = "video";
 private static final String MEDIA_TYPE = "mediaType";
 /** The Constant ACCOUNT_ID. */
 /** The Constant SOCIAL_ARTICLE_PAGE_PATH. */
 private static final String SOCIAL_ARTICLE_PAGE_PATH = "socialArticlePagePath";
 /** The Constant SOCIAL_ARTICLE_PAGE_FOR_IMAGE. */
 private static final String SOCIAL_ARTICLE_URL = "socialArticleUrl";
 /** The Constant SOCIAL_IMAGE_POST_ID. */
 private static final String SOCIAL_IMAGE_POST_ID = "postId";
 /** The Constant OPEN_IN_NEW_WINDOW_IMG. */
 private static final String OPEN_IN_NEW_WINDOW_IMG = "openInNewWindowImg";
 /** The Constant OPEN_IN_NEW_WINDOW_VID. */
 private static final String OPEN_IN_NEW_WINDOW_VID = "openInNewWindowVid";
 /** The Constant SOCIAL_CHANNEL_IMG. */
 private static final String SOCIAL_CHANNEL_IMG = "socialChannellsImg";
 /** The Constant SOCIAL_CHANNEL_IMG. */
 private static final String SOCIAL_CHANNEL_VID = "socialChannellsVid";
 /** The Constant CHANNEL_OR_PLAYLIST_TYPE. */
 private static final String CHANNEL_OR_PLAYLIST_TYPE = "channelOrPlayListType";
 
 /** The Constant Type. */
 private static final String TYPE = "type";
 
 /** The Constant SHOW_PROFILE_PIC. */
 private static final String SHOW_PROFILE_PIC = "showProfilePic";
 
 private static final String PREVIEW_IMAGE_PATH = "previewImagePath";
 private static final String SHORT_SUB_HEADING = "shortSubHeading";
 private static final String VIDEO_CAPTION = "videoCaption";
 
 private static final String PREVIEW_IMAGE_PATH_MAP = "previewImagePathMap";
 
 @Reference
 ConfigurationService configurationService;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper# processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  
  LOGGER.debug("Executing SocialGalleryVideoViewHelperImpl. Inside processData method ");
  
  ResourceResolver resourceResolver = getResourceResolver(resources);
  String jsonNameSpace = getJsonNameSpace(content);
  I18n i18n = getI18n(resources);
  Resource currentResource = getCurrentResource(resources);
  Page currentPage = getCurrentPage(resources);
  Map<String, Object> properties = getProperties(content);
  
  String listType = MapUtils.getString(properties, TYPE, StringUtils.EMPTY);
  
  int numberOfPosts = GlobalConfigurationUtility.getIntegerValueFromConfiguration(UnileverConstants.SOCIAL_GALLERY_VIDEO_CONFIG_CAT,
    configurationService, currentPage, NUMBER_OF_POSTS);
  
  int maxImages = GlobalConfigurationUtility.getIntegerValueFromConfiguration(UnileverConstants.SOCIAL_GALLERY_VIDEO_CONFIG_CAT, configurationService,
    currentPage, MAXIMUM_IMAGES);
  String profilePicture = MapUtils.getString(properties, SHOW_PROFILE_PIC, StringUtils.EMPTY);
  String showProfilePicture = profilePicture != null && conditionCheck(profilePicture)
    ? com.unilever.platform.foundation.constants.CommonConstants.BOOLEAN_TRUE : ProductConstants.FALSE;
  
  SocialTabQueryDTO tabQuery = getSocialAggregatorQueryDataObj(resources, properties, currentPage, configurationService, numberOfPosts, maxImages);
  
  Map<String, Object> socialGalleryImageVideoMap = new LinkedHashMap<String, Object>();
  
  // adding retrieval query parameters
  addRetrievalQueryParametrsVideoMap(socialGalleryImageVideoMap, tabQuery, properties, resources, listType);
  
  // adding social images map
  addSocialImagesVideoMap(socialGalleryImageVideoMap, resourceResolver, currentPage, currentResource, resources, listType);
  // adding static map
  addStaticMapProperties(socialGalleryImageVideoMap, i18n);
  socialGalleryImageVideoMap.put("showProfilePicture", Boolean.valueOf(showProfilePicture));
  
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(jsonNameSpace, socialGalleryImageVideoMap);
  return data;
 }
 
 /**
  * 
  * @param socialGalleryImageVideoMap
  * @param currentResource
  * @param resources
  */
 private void addFixedVideosList(Map<String, Object> socialGalleryImageVideoMap, Resource currentResource, Map<String, Object> resources) {
  List<Map<String, Object>> fixedVideosFinalMap = new ArrayList<Map<String, Object>>();
  List<Map<String, String>> socialGalleryFixedVideoPostsJson = ComponentUtil.getNestedMultiFieldProperties(currentResource,
    "socialGalleryFixedVideoPostsJson");
  Iterator<Map<String, String>> propertiesNames = socialGalleryFixedVideoPostsJson.iterator();
  
  while (propertiesNames.hasNext()) {
   Map<String, Object> fixedVideosDetailsMap = new HashMap<String, Object>();
   Map<String, String> fixedVideoMap = propertiesNames.next();
   String fixedVideoId = null == fixedVideoMap.get("fixedVideoId") ? StringUtils.EMPTY : fixedVideoMap.get("fixedVideoId");
   String previewImagePath = null == fixedVideoMap.get(PREVIEW_IMAGE_PATH) ? StringUtils.EMPTY : fixedVideoMap.get(PREVIEW_IMAGE_PATH);
   String videoCaption = null == fixedVideoMap.get(VIDEO_CAPTION) ? StringUtils.EMPTY : fixedVideoMap.get(VIDEO_CAPTION);
   String shortSubHeading = null == fixedVideoMap.get(SHORT_SUB_HEADING) ? StringUtils.EMPTY : fixedVideoMap.get(SHORT_SUB_HEADING);
   
   fixedVideosDetailsMap.put("videoId", fixedVideoId);
   fixedVideosDetailsMap.put(PREVIEW_IMAGE_PATH, previewImagePath);
   Image image = new Image(previewImagePath, StringUtils.EMPTY, StringUtils.EMPTY, getCurrentPage(resources), getSlingRequest(resources));
   fixedVideosDetailsMap.put(PREVIEW_IMAGE_PATH_MAP, image.convertToMap());
   fixedVideosDetailsMap.put(VIDEO_CAPTION, videoCaption);
   fixedVideosDetailsMap.put(SHORT_SUB_HEADING, shortSubHeading);
   
   fixedVideosFinalMap.add(fixedVideosDetailsMap);
   
  }
  socialGalleryImageVideoMap.put("fixedVideoList", fixedVideosFinalMap.toArray());
  
 }
 
 /**
  * Condition check.
  * 
  * @param numberOfView
  *         the number of view
  * @return true, if successful
  */
 private static boolean conditionCheck(String numberOfView) {
  return "true".equals(numberOfView);
 }
 
 /**
  * Gets the social gallery posts.
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param page
  *         the page
  * @param currentResource
  *         the current resource
  * @param listType
  * @return the social gallery posts
  */
 private void addSocialImagesVideoMap(Map<String, Object> socialGalleryImageVideoMap, ResourceResolver resourceResolver, Page currentPage,
   Resource currentResource, Map<String, Object> resources, String listType) {
  
  List<Map<String, Object>> socialPostsListImages = new ArrayList<Map<String, Object>>();
  List<Map<String, Object>> socialPostsListVideos = new ArrayList<Map<String, Object>>();
  List<Map<String, String>> imagePostslist = getSocialGalleryImagesMap(currentResource);
  
  for (Map<String, String> map : imagePostslist) {
   String socialChannel = StringUtils.defaultString(map.get(SOCIAL_CHANNEL_IMG), StringUtils.EMPTY);
   String postId = StringUtils.defaultString(map.get(SOCIAL_IMAGE_POST_ID), StringUtils.EMPTY);
   String imagePath = StringUtils.defaultString(map.get(SOCIAL_ARTICLE_URL), StringUtils.EMPTY);
   String relatedArticlePagePath = ComponentUtil.getFullURL(resourceResolver, imagePath, getSlingRequest(resources));
   
   Map<String, Object> socialPostsMap = SocialTABHelper.getImageMapWithMetadata(imagePath, currentPage, resourceResolver, getSlingRequest(resources));
   socialPostsMap.put(SOCIAL_GALLERY_SOCIAL_CHANNEL, socialChannel);
   socialPostsMap.put(SOCIAL_IMAGE_POST_ID, postId);
   socialPostsMap.put(READ_ARTICLE_CTA_URL, relatedArticlePagePath);
   socialPostsMap.put(OPEN_IN_NEW_WINDOW, getNewWindowFromMultifieldImg(map));
   socialPostsListImages.add(socialPostsMap);
  }
  socialGalleryImageVideoMap.put("editorSocialImages", socialPostsListImages.toArray());
  if ("fixedList".equals(listType)) {
   addFixedVideosList(socialGalleryImageVideoMap, currentResource, resources);
   socialGalleryImageVideoMap.put("editorSocialVideos", socialPostsListVideos.toArray());
  } else {
   List<Map<String, String>> videoPostslist = getSocialGalleryVideoMap(currentResource);
   for (Map<String, String> map : videoPostslist) {
    String socialChannel = StringUtils.defaultString(map.get(SOCIAL_CHANNEL_VID), StringUtils.EMPTY);
    String videoID = StringUtils.defaultString(map.get(SOCIAL_GALLERY_SOCIAL_VIDEO_ID), StringUtils.EMPTY);
    String imagePath = StringUtils.defaultString(map.get(SOCIAL_ARTICLE_PAGE_PATH), StringUtils.EMPTY);
    String relatedArticlePagePath = ComponentUtil.getFullURL(resourceResolver, imagePath, getSlingRequest(resources));
    
    Map<String, Object> socialPostsMap = SocialTABHelper.getImageMapWithMetadata(imagePath, currentPage, resourceResolver,
      getSlingRequest(resources));
    socialPostsMap.put(SOCIAL_GALLERY_SOCIAL_CHANNEL, socialChannel);
    socialPostsMap.put(SOCIAL_GALLERY_SOCIAL_VIDEO_ID, videoID);
    socialPostsMap.put(READ_ARTICLE_CTA_URL, relatedArticlePagePath);
    socialPostsMap.put(OPEN_IN_NEW_WINDOW, getNewWindowFromMultifieldVid(map));
    socialPostsListVideos.add(socialPostsMap);
   }
   socialGalleryImageVideoMap.put("editorSocialVideos", socialPostsListVideos.toArray());
   
  }
  
 }
 
 /**
  * Gets the static map properties.
  * 
  * @param i18n
  *         the i18n
  * @return the static map properties
  */
 private static void addStaticMapProperties(Map<String, Object> socialGalleryImageVideoMap, I18n i18n) {
  
  Map<String, Object> staticMap = new HashMap<String, Object>();
  
  staticMap.put("showMoreCtaLabel", i18n.get("socialGalleryVideo.showMore"));
  staticMap.put("readArticleCtaLabel", i18n.get("socialGalleryVideo.readFullArticle"));
  staticMap.put("closeLabel", i18n.get("socialGalleryVideo.closeLabel"));
  socialGalleryImageVideoMap.put("static", staticMap);
 }
 
 /**
  * Gets the social gallery Video map.
  * 
  * @param currentResource
  *         the current resource
  * @return the social gallery Video map
  */
 public List<Map<String, String>> getSocialGalleryVideoMap(Resource currentResource) {
  return ComponentUtil.getNestedMultiFieldProperties(currentResource, SOCIAL_GALLERY_VIDEOS);
 }
 
 /**
  * Gets the new window for image section from multifield.
  * 
  * @param map
  *         the map
  * @return the new window for image section from multifield
  */
 private static boolean getNewWindowFromMultifieldImg(Map<String, String> map) {
  
  String newWindow = StringUtils.defaultString(map.get(OPEN_IN_NEW_WINDOW_IMG), StringUtils.EMPTY);
  String openInNewWindow = newWindow != null && "[true]".equals(newWindow) ? BOOLEAN_TRUE : BOOLEAN_FALSE;
  return Boolean.valueOf(openInNewWindow);
 }
 
 /**
  * Gets the new window for video section from multifield.
  * 
  * @param map
  *         the map
  * @return the new window for video section from multifield
  */
 private static boolean getNewWindowFromMultifieldVid(Map<String, String> map) {
  
  String newWindow = StringUtils.defaultString(map.get(OPEN_IN_NEW_WINDOW_VID), StringUtils.EMPTY);
  String openInNewWindow = newWindow != null && "[true]".equals(newWindow) ? BOOLEAN_TRUE : BOOLEAN_FALSE;
  return Boolean.valueOf(openInNewWindow);
 }
 
 /**
  * Adds the retrieval query parametrs map.
  * 
  * @param map
  *         the map
  * @param socialTabQuery
  *         the social tab query
  * @param listType
  * @param properties
  * @param resources
  */
 @SuppressWarnings("unchecked")
 public static void addRetrievalQueryParametrsVideoMap(Map<String, Object> map, SocialTabQueryDTO socialTabQuery, Map<String, Object> properties,
   Map<String, Object> resources, String listType) {
  
  SocialTABHelper.addRetrievalQueryParametrsMap(map, socialTabQuery, true);
  Map<String, Object> requestServletUrlMap = (Map<String, Object>) map.get("retrivalQueryParams");
  
  addMediaTypes(requestServletUrlMap, properties);
  addParametersToSocialChannels(requestServletUrlMap, resources);
  requestServletUrlMap.put(CHANNEL_OR_PLAYLIST_TYPE, listType);
  if ("fixedList".equals(listType)) {
   requestServletUrlMap.put(CHANNEL_OR_PLAYLIST_ID, StringUtils.EMPTY);
  } else {
   requestServletUrlMap.put(CHANNEL_OR_PLAYLIST_ID, MapUtils.getString(properties, CHANNEL_OR_PLAYLIST_ID, StringUtils.EMPTY));
  }
  
  map.put("retrivalQueryParams", requestServletUrlMap);
 }
 
 private static void addMediaTypes(Map<String, Object> requestServletUrlMap, Map<String, Object> properties) {
  List<String> mediaType = new ArrayList<String>();
  if (MapUtils.getBoolean(properties, IS_IMAGE, Boolean.FALSE)) {
   mediaType.add(IMAGE);
  }
  if (MapUtils.getBoolean(properties, IS_VIDEO, Boolean.FALSE)) {
   mediaType.add(VIDEO);
  }
  requestServletUrlMap.put(MEDIA_TYPE, mediaType.toArray());
 }
}
