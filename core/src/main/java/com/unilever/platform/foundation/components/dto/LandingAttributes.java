/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.components.dto;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.UnileverDateHelper;
import com.unilever.platform.foundation.constants.TaggingComponentsConstants;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.Image;

/**
 * The Class LandingAttributesDTO.
 */
public class LandingAttributes {
 private static final String PAGE_READ_TIME = "pageReadTime";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(LandingAttributes.class);
 
 /** The Constant TITLE. */
 private static final String TITLE = "title";
 
 /** The Constant URL. */
 private static final String URL = "url";
 
 /** The Constant COPY_TEXT. */
 private static final String COPY_TEXT = "copyText";
 
 /** The Constant LONG_COPY. */
 private static final String LONG_COPY = "longCopy";
 
 /** The Constant IMAGE. */
 private static final String IMAGE = "image";
 
 /** The Constant PARENT. */
 private static final String PARENT = "parent";
 
 /** The Constant CONTENT_TYPE. */
 private static final String CONTENT_TYPE = "contentType";
 
 /** The Constant AUTHOR_NAME. */
 private static final String AUTHOR_NAME = "authorName";
 
 /** The Constant AUTHOR. */
 private static final String AUTHOR = "author";
 
 /** The Constant PUBLISH_DATE. */
 private static final String PUBLISH_DATE = "publishDate";
 
 /** The Constant PUBLISH_DATE. */
 private static final String LAST_REVIEW_DATE = "lastReviewDate";
 
 /** The Constant DATE_FORMATTER. */
 private static final String DATE_FORMATTER = "yyyy-MM-dd'T'HH:mm:ss";
 
 /** The Constant CUSTOM_BODY_CLASS. */
 private static final String CUSTOM_BODY_CLASS = "customBodyClass";
 
 /** The Constant SUB_CONTENT_TYPE. */
 private static final String SUB_CONTENT_TYPE = "subContentType";
 
 /** The title. */
 private String title;
 
 /** The short copy. */
 private String shortCopy;
 
 /** The long copy. */
 private String longCopy;
 
 /** The image. */
 private Image image;
 
 /** The launch date. */
 private Date publichDate;
 
 /** The url. */
 private String url;
 
 /** The content type. */
 private String contentType;
 
 /** The author name. */
 private String authorName;
 
 /** The read time. */
 private int readTime;
 
 /** The parent. */
 private Map<String, Object> parent;
 
 /** The Formatted Date. */
 private String formattedDate;

 private String customBodyClass;
 
 /** The sub content type. */
 private String subContentType;
 
 private String formattedReviewDate;
 /**
  * Instantiates a new landing attributes.
  */
 public LandingAttributes() {
  
 }
 
 /**
  * Instantiates a new landing attributes dto.
  * 
  * @param page
  *         the page
  * @param request
  *         the request
  * @param configMap
  *         the config map
  */
 public LandingAttributes(Page page, SlingHttpServletRequest request, Map<String, String> configMap) {
  this.setData(page, request, configMap);
 }
 
 public String getCustomBodyClass() {
  return customBodyClass;
 }
 
 public void setCustomBodyClass(String customBodyClass) {
  this.customBodyClass = customBodyClass;
 }
 
 /**
  * Sets the data.
  * 
  * @param page
  *         the page
  * @param request
  *         the request
  * @param configMap
  *         the config map
  */
 public void setData(Page page, SlingHttpServletRequest request, Map<String, String> configMap) {
  LOGGER.info("Inside setData of LandingAttributesDTO");
  if (page != null) {
   ResourceResolver resourceResolver = page.getContentResource().getResourceResolver();
   ValueMap properties = page.getProperties();
   
   String imagePath = MapUtils.getString(properties, UnileverConstants.TEASER_IMAGE, "");
   String imageAltText = MapUtils.getString(properties, UnileverConstants.TEASER_IMAGE_ALT_TEXT, "");
   String dateStr = UnileverDateHelper.getDateString(properties, UnileverConstants.PUBLISH_DATE);
   String reviewdateStr = UnileverDateHelper.getDateString(properties, UnileverConstants.LAST_REVIEW_DATE);
   
   String teaserFormattedDate = ComponentUtil.getformattedDateByConfig(dateStr, configMap);
   String teaserFormattedReviewDate = ComponentUtil.getformattedDateByConfig(reviewdateStr, configMap);
   
   String contentTypeVal = properties.get(UnileverConstants.CONTENT_TYPE) != null ? properties.get(UnileverConstants.CONTENT_TYPE).toString()
     : StringUtils.EMPTY;
   String subContentTypeVal = properties.get(UnileverConstants.SUB_CONTENT_TYPE) != null ? properties.get(UnileverConstants.SUB_CONTENT_TYPE)
     .toString() : StringUtils.EMPTY;
   String authorNameVal = properties.get(AUTHOR_NAME) != null ? properties.get(AUTHOR_NAME).toString() : StringUtils.EMPTY;
   String customBodyClassValue = properties.get(CUSTOM_BODY_CLASS) != null ? properties.get(CUSTOM_BODY_CLASS).toString() : StringUtils.EMPTY;
   
   int readTimeValue = MapUtils.getIntValue(properties, PAGE_READ_TIME, 0);
   Page parentPage = page.getParent();
   
   setTitle(MapUtils.getString(properties, UnileverConstants.TEASER_TITLE, ""));
   setShortCopy(MapUtils.getString(properties, UnileverConstants.TEASER_COPY, ""));
   setLongCopy(MapUtils.getString(properties, UnileverConstants.TEASER_LONG_COPY, ""));
   setUrl(ComponentUtil.getFullURL(resourceResolver, page.getPath(), request));
   setImage(new Image(imagePath, imageAltText, "", page, request));
   setFormattedDate(teaserFormattedDate);
   setFormattedReviewDate(teaserFormattedReviewDate);
   setAuthorName(authorNameVal);
   setReadTime(readTimeValue);
   setContentType(contentTypeVal);
   setSubContentType(subContentTypeVal);
   setCustomBodyClass(customBodyClassValue);
   setParent(resourceResolver, parentPage, configMap, request);
  }
  
 }
 
 /**
  * Convert to map.
  * 
  * @param flagMap
  *         the flag map
  * @return the map
  */
 @SuppressWarnings("unchecked")
 public Map<String, Object> convertToMap(Map<String, Boolean> flagMap) {
  Map<String, Object> map = convertToMap();
  if (!MapUtils.getBooleanValue(flagMap, TaggingComponentsConstants.IMAGE_FLAG, false)) {
   map.put(IMAGE, null);
  }
  if (!MapUtils.getBooleanValue(flagMap, TaggingComponentsConstants.COPY_FLAG, false)) {
   map.put(COPY_TEXT, "");
  }
  if (!MapUtils.getBooleanValue(flagMap, TaggingComponentsConstants.LONG_COPY_FLAG, false)) {
   map.put(LONG_COPY, "");
  }
  if (!MapUtils.getBooleanValue(flagMap, TaggingComponentsConstants.PUBLISH_DATE_FLAG, false)) {
   map.put(PUBLISH_DATE, "");
  }
  if (!MapUtils.getBooleanValue(flagMap, TaggingComponentsConstants.LAST_REVIEW_DATE_FLAG, false)) {
   map.put(LAST_REVIEW_DATE, "");
  }
  if (!MapUtils.getBooleanValue(flagMap, TaggingComponentsConstants.READ_TIME_FLAG, false)) {
   map.put(PAGE_READ_TIME, 0);
  }
  if (!MapUtils.getBooleanValue(flagMap, TaggingComponentsConstants.AUTHOR_NAME_FLAG, false)) {
   map.put(AUTHOR, "");
  }
  Map<String, Object> parentMap = MapUtils.getMap(map, PARENT);
  if (MapUtils.getBooleanValue(flagMap, TaggingComponentsConstants.PARETNT_PAGE_DETAILS_FLAG, false)) {
   
   if (!MapUtils.getBooleanValue(flagMap, TaggingComponentsConstants.IMAGE_FLAG, false)) {
    parentMap.put(IMAGE, null);
   }
   if (!MapUtils.getBooleanValue(flagMap, TaggingComponentsConstants.COPY_FLAG, false)) {
    parentMap.put(COPY_TEXT, "");
   }
   if (!MapUtils.getBooleanValue(flagMap, TaggingComponentsConstants.LONG_COPY_FLAG, false)) {
    parentMap.put(LONG_COPY, "");
   }
   if (!MapUtils.getBooleanValue(flagMap, TaggingComponentsConstants.PUBLISH_DATE_FLAG, false)) {
    parentMap.put(UnileverConstants.PUBLISH_DATE, "");
   }
   if (!MapUtils.getBooleanValue(flagMap, TaggingComponentsConstants.READ_TIME_FLAG, false)) {
    map.put(PAGE_READ_TIME, 0);
   }
  } else {
   parentMap.put(IMAGE, null);
   parentMap.put(COPY_TEXT, "");
   parentMap.put(LONG_COPY, "");
   parentMap.put(PAGE_READ_TIME, 0);
   map.put(PARENT, parentMap);
  }
  
  return map;
 }
 
 /**
  * Convert to map.
  * 
  * @return the map
  */
 public Map<String, Object> convertToMap() {
  Map<String, Object> map = new LinkedHashMap<String, Object>();
  map.put(TITLE, getTitle());
  map.put(COPY_TEXT, getShortCopy());
  map.put(LONG_COPY, getLongCopy());
  map.put(URL, getUrl());
  map.put(IMAGE, getImage().convertToMap());
  map.put(PARENT, getParent());
  map.put(CONTENT_TYPE, getContentType());
  map.put(SUB_CONTENT_TYPE, getSubContentType());
  map.put(AUTHOR, getAuthorName());
  map.put(CUSTOM_BODY_CLASS, getCustomBodyClass());
  map.put(PUBLISH_DATE, this.formattedDate);
  map.put(LAST_REVIEW_DATE, this.formattedReviewDate);
  map.put(PAGE_READ_TIME, getReadTime());
  return map;
 }
 
 /**
  * Sets the parent.
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param parentPage
  *         the parent page
  * @param configMap
  *         the config map
  * @param request
  *         the request
  */
 private void setParent(ResourceResolver resourceResolver, Page parentPage, Map<String, String> configMap, SlingHttpServletRequest request) {
  String parentPageUrl = ComponentUtil.getFullURL(resourceResolver, parentPage.getPath(), request);
  
  ValueMap properties = parentPage.getProperties();
  String dateStr = StringUtils.EMPTY;
  String reviewDateStr = StringUtils.EMPTY;
  
  String articleTeaserImage = properties.get(UnileverConstants.TEASER_IMAGE,String.class);
  String articleTeaserImageAltText = properties.get(UnileverConstants.TEASER_IMAGE_ALT_TEXT, StringUtils.EMPTY);
  String teaserTitle = properties.get(UnileverConstants.TEASER_TITLE, StringUtils.EMPTY);
  Image teaserImage = new Image(articleTeaserImage, articleTeaserImageAltText, teaserTitle, parentPage, request);
  
  String teaserCopyText = properties.get(UnileverConstants.TEASER_COPY,StringUtils.EMPTY);
  String teaserLongCopy = properties.get(UnileverConstants.TEASER_LONG_COPY, StringUtils.EMPTY);
  
  if (MapUtils.getObject(properties, UnileverConstants.PUBLISH_DATE) instanceof GregorianCalendar) {
   dateStr = ((MapUtils.getObject(properties, UnileverConstants.PUBLISH_DATE)) != null) ? ComponentUtil.getFormattedDateString(
     (GregorianCalendar) MapUtils.getObject(properties, UnileverConstants.PUBLISH_DATE), null) : StringUtils.EMPTY;
  } else {
   SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMATTER);
   String publishDaate = MapUtils.getString(properties, UnileverConstants.PUBLISH_DATE, StringUtils.EMPTY);
   try {
    if (StringUtils.isNotBlank(publishDaate)) {
     Date dateObject = dateFormat.parse(publishDaate);
     Calendar calendar = Calendar.getInstance();
     calendar.setTime(dateObject);
     dateStr = ((MapUtils.getObject(properties, UnileverConstants.PUBLISH_DATE)) != null) ? ComponentUtil.getFormattedDateString(
       (GregorianCalendar) calendar, null) : StringUtils.EMPTY;
    }
   } catch (ParseException e) {
    LOGGER.warn("Error while parsing publish date", e);
   }
  }
  if (MapUtils.getObject(properties, UnileverConstants.LAST_REVIEW_DATE) instanceof GregorianCalendar) {
   reviewDateStr = ((MapUtils.getObject(properties, UnileverConstants.LAST_REVIEW_DATE)) != null) ? ComponentUtil.getFormattedDateString(
     (GregorianCalendar) MapUtils.getObject(properties, UnileverConstants.LAST_REVIEW_DATE), null) : StringUtils.EMPTY;
  } else {
   SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMATTER);
   String reviewDate = MapUtils.getString(properties, UnileverConstants.LAST_REVIEW_DATE, StringUtils.EMPTY);
   if(StringUtils.isNotBlank(reviewDate)){
   try {
    Date dateObject = dateFormat.parse(reviewDate);
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(dateObject);
    reviewDateStr = ((MapUtils.getObject(properties, UnileverConstants.LAST_REVIEW_DATE)) != null) ? ComponentUtil.getFormattedDateString(
      (GregorianCalendar) calendar, null) : StringUtils.EMPTY;
   } catch (ParseException e) {
    LOGGER.warn("Error while parsing last review date", e);
   }
   }
  }
  String teaserFormattedDate = ComponentUtil.getformattedDateByConfig(dateStr, configMap);
  String teaserFormattedReviewDate = ComponentUtil.getformattedDateByConfig(reviewDateStr, configMap);
  
  int readTimeVal = properties.get(PAGE_READ_TIME) != null ? Integer.parseInt((String) properties.get(PAGE_READ_TIME)) : 0;
  
  Map<String, Object> parentMap = new HashMap<String, Object>();
  parentMap.put(TITLE, teaserTitle);
  parentMap.put(COPY_TEXT, teaserCopyText);
  parentMap.put(LONG_COPY, teaserLongCopy);
  parentMap.put(URL, parentPageUrl);
  parentMap.put(IMAGE, teaserImage);
  parentMap.put(PUBLISH_DATE, teaserFormattedDate);
  parentMap.put(LAST_REVIEW_DATE, teaserFormattedReviewDate);
  parentMap.put(PAGE_READ_TIME, readTimeVal);
  setParent(parentMap);
 }
 
 /**
  * Gets the title.
  * 
  * @return the title
  */
 public String getTitle() {
  return title;
 }
 
 /**
  * Sets the title.
  * 
  * @param title
  *         the new title
  */
 public void setTitle(String title) {
  this.title = title;
 }
 
 /**
  * Gets the short copy.
  * 
  * @return the short copy
  */
 public String getShortCopy() {
  return shortCopy;
 }
 
 /**
  * Sets the short copy.
  * 
  * @param shortCopy
  *         the new short copy
  */
 public void setShortCopy(String shortCopy) {
  this.shortCopy = shortCopy;
 }
 
 /**
  * Gets the long copy.
  * 
  * @return the long copy
  */
 public String getLongCopy() {
  return longCopy;
 }
 
 /**
  * Sets the long copy.
  * 
  * @param longCopy
  *         the new long copy
  */
 public void setLongCopy(String longCopy) {
  this.longCopy = longCopy;
 }
 
 /**
  * Gets the image.
  * 
  * @return the image
  */
 public Image getImage() {
  
  return image;
 }
 
 /**
  * Sets the image.
  * 
  * @param image
  *         the new image
  */
 public void setImage(Image image) {
  this.image = image;
 }
 
 /**
  * Gets the publich date.
  * 
  * @return the publich date
  */
 public Date getPublichDate() {
  return publichDate;
 }
 
 /**
  * Sets the publich date.
  * 
  * @param publichDate
  *         the new publich date
  */
 public void setPublichDate(Date publichDate) {
  this.publichDate = publichDate;
 }
 
 /**
  * Sets the url.
  * 
  * @param url
  *         the new url
  */
 public void setUrl(String url) {
  this.url = url;
 }
 
 /**
  * Gets the content type.
  * 
  * @return the content type
  */
 public String getContentType() {
  return contentType;
 }
 
 /**
  * Gets the subContent type.
  * 
  * @return the subContent type
  */
 public String getSubContentType() {
  return subContentType;
 }
 
 /**
  * Sets the Sub Content Type.
  * 
  * @param subContentTypeVal
  *         the sub content type
  */
 private void setSubContentType(String subContentTypeVal) {
  this.subContentType = subContentTypeVal;
  
 }
 
 /**
  * Sets the content type.
  * 
  * @param contentType
  *         the new content type
  */
 public void setContentType(String contentType) {
  this.contentType = contentType;
 }
 
 /**
  * Gets the author name.
  * 
  * @return the author name
  */
 public String getAuthorName() {
  return authorName;
 }
 
 /**
  * Sets the author name.
  * 
  * @param authorName
  *         the new author name
  */
 public void setAuthorName(String authorName) {
  this.authorName = authorName;
 }
 
 /**
  * Gets the read time.
  * 
  * @return the read time
  */
 public int getReadTime() {
  return readTime;
 }
 
 /**
  * Sets the read time.
  * 
  * @param readTime
  *         the new read time
  */
 public void setReadTime(int readTime) {
  this.readTime = readTime;
 }
 
 /**
  * Gets the parent.
  * 
  * @return the parent
  */
 public Map<String, Object> getParent() {
  return parent;
 }
 
 /**
  * Sets the parent.
  * 
  * @param parent
  *         the parent
  */
 public void setParent(Map<String, Object> parent) {
  this.parent = parent;
 }
 
 /**
  * Gets the url.
  * 
  * @return the url
  */
 public String getUrl() {
  return url;
 }
 
 /**
  * Gets the formatted date.
  * 
  * @return the formatted date
  */
 public String getFormattedDate() {
  return formattedDate;
 }
 
 /**
  * Sets the formatted date.
  * 
  * @param formattedDate
  *         the new formatted date
  */
 public void setFormattedDate(String formattedDate) {
  this.formattedDate = formattedDate;
 }
 
 /**
  * Sets the formatted review date.
  * 
  * @param formattedDate
  *         the new formatted review date
  */
 public void setFormattedReviewDate(String formattedReviewDate) {
  this.formattedReviewDate = formattedReviewDate;
 }
 
}
