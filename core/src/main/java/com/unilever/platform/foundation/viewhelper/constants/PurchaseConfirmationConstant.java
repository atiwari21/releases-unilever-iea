/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.constants;

/**
 * The Class ShoppingBasketConstant.
 */
public class PurchaseConfirmationConstant {
 
 /** The Constant THANK_YOU_LABEL. */
 public static final String THANK_YOU_LABEL = "thankYouLabel";
 
 /** The Constant DISCLAIMER_STATEMENT. */
 public static final String DISCLAIMER_STATEMENT = "disclaimerStatement";
 
 /** The Constant ORDER_NO_STATEMENT. */
 public static final String ORDER_NO_STATEMENT = "orderNoStatement";
 
 /** The Constant EMAIL_STATEMENT. */
 public static final String EMAIL_STATEMENT = "emailStatement";
 
 /** The Constant DELIVERY_STATEMENT. */
 public static final String DELIVERY_STATEMENT = "deliveryStatement";
 
 /** The Constant HELP_STATEMENT. */
 public static final String HELP_STATEMENT = "helpStatement";
 
 /** The Constant TELEPHOEN_STATEMENT. */
 public static final String TELEPHONE_STATEMENT = "telephoneLabel";
 
 /** The Constant EMAIL_LABEL. */
 public static final String EMAIL_LABEL = "emailLabel";
 
 /** The Constant DOWNLOAD_BUTTON_CTA_LABEL. */
 public static final String DOWNLOAD_BUTTON_CTA_LABEL = "downloadButtonCTALabel";
 
 /** The Constant RETURN_BUTTON. */
 public static final String RETURN_BUTTON = "returnButton";
 
 /** The Constant CUSTOMER_SERVICE_PHONE. */
 public static final String CUSTOMER_SERVICE_PHONE = "customerServicePhone";
 
 /** The Constant CUSTOMER_SERVICE_EMAIL. */
 public static final String CUSTOMER_SERVICE_EMAIL = "customerServiceEmail";
 
 /** The Constant THANK_YOU_LABEL_FIELD. */
 public static final String THANK_YOU_LABEL_FIELD = "thankYouLabel";
 
 /** The Constant DISCLAIMER_STATEMENT_FIELD. */
 public static final String DISCLAIMER_STATEMENT_FIELD = "disclaimerStatement";
 
 /** The Constant ORDER_NO_STATEMENT_FIELD. */
 public static final String ORDER_NO_STATEMENT_FIELD = "orderNoStatement";
 
 /** The Constant EMAIL_STATEMENT_FIELD. */
 public static final String EMAIL_STATEMENT_FIELD = "emailStatement";
 
 /** The Constant DELIVERY_STATEMENT_FIELD. */
 public static final String DELIVERY_STATEMENT_FIELD = "deliveryStatement";
 
 /** The Constant HELP_STATEMENT_FIELD. */
 public static final String HELP_STATEMENT_FIELD = "helpStatement";
 
 /** The Constant TELEPHONE_STATEMENT_FIELD. */
 public static final String TELEPHONE_STATEMENT_FIELD = "telephoneLabel";
 
 /** The Constant EMAIL_LABEL_FIELD. */
 public static final String EMAIL_LABEL_FIELD = "emailLabel";
 
 /** The Constant DOWNLOAD_BUTTON_CTA_LABEL_FIELD. */
 public static final String DOWNLOAD_BUTTON_CTA_LABEL_FIELD = "downloadButtonCTALabel";
 
 /** The Constant RETURN_BUTTON_FIELD. */
 public static final String RETURN_BUTTON_LABEL_FIELD = "returnButtonLabel";
 
 /** The Constant GIFTING_PAGE_PATH. */
 public static final String GIFTING_PAGE_PATH = "gifitingPagePath";
 
 /** The Constant CUSTOMER_SERVICE_PHONE_FIELD. */
 public static final String CUSTOMER_SERVICE_PHONE_FIELD = "customerServicePhone";
 
 /** The Constant CUSTOMER_SERVICE_EMAIL_FIELD. */
 public static final String CUSTOMER_SERVICE_EMAIL_FIELD = "customerServiceEmail";
 
 /** The Constant PDF_SERVLET_SELECTOR. */
 public static final String PDF_SERVLET_SELECTOR = ".generatepdf.html";
 
 /** The Constant TELEPHONE_NUMBER_FINE_PRINT. */
 public static final String TELEPHONE_NUMBER_FINE_PRINT = "telephoneNumberFinePrint";
 
 /** The Constant TELEPHONE_NUMBER_FINE_PRINT. */
 public static final String TELEPHONE_NUMBER_FINE_PRINT_FIELD = "telephoneNumberFinePrint";
 
 /** The Constant DELIVERY_TIME. */
 public static final String DELIVERY_TIME = "deliveryTime";
 
 /**
  * Instantiates a new shopping basket constant.
  */
 private PurchaseConfirmationConstant() {
  
 }
}
