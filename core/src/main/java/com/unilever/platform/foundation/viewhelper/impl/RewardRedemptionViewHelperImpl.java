/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Session;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.aem.foundation.core.helper.RewardHelper;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

@Component(description = "rewardRedemption Viewhelper Impl", immediate = true, metatype = true, label = "rewardRedemption helper Impl")
@Service(value = { RewardRedemptionViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.REWARD_REDEMPTION, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })

public class RewardRedemptionViewHelperImpl extends BaseViewHelper {
 
 private static final String PAGINATION_CTA_LABEL_I18N = "rewardRedemption.paginationCtaLabel";
 
 private static final String DEFAULT_REWARD_LOCATION = "/etc/commerce/products/unilever";
 
 private static final String REWARD_LIST = "rewardList";
 
 private static final String GENERAL_CONFIG = "generalConfig";
 
 /** The Constant HIDE_EXPIRED_REWARDS_FROM_THE_LIST. */
 private static final String HIDE_EXPIRED_REWARDS_FROM_THE_LIST = "hideExpiredRewards";
 
 /** The Constant PAGINATION_KEY. */
 public static final String PAGINATION_KEY = "pagination";
 
 /** The Constant PAGINATION_TYPE. */
 public static final String PAGINATION_TYPE = "paginationType";
 
 /** The Constant ITEMS_PER_PAGE. */
 public static final String ITEMS_PER_PAGE = "itemsPerPage";
 
 /** The Constant PAGINATION_CTA_LABEL. */
 public static final String PAGINATION_CTA_LABEL = "paginationCtaLabel";
 
 /** The Constant STATIC. */
 public static final String STATIC = "static";
 
 /** The Constant PAGE_LISTING_PAGINATION_FIRST. */
 public static final String PAGE_LISTING_PAGINATION_FIRST = "pageListing.pagination.first";
 
 /** The Constant PAGE_LISTING_PAGINATION_LAST. */
 public static final String PAGE_LISTING_PAGINATION_LAST = "pageListing.pagination.last";
 
 /** The Constant PAGE_LISTING_PAGINATION_PREVIOUS. */
 public static final String PAGE_LISTING_PAGINATION_PREVIOUS = "pageListing.pagination.previous";
 
 /** The Constant PAGE_LISTING_PAGINATION_NEXT. */
 public static final String PAGE_LISTING_PAGINATION_NEXT = "pageListing.pagination.next";
 
 /** The Constant FIRST_LABEL. */
 public static final String FIRST_LABEL = "firstLabel";
 
 /** The Constant LAST_LABEL. */
 public static final String LAST_LABEL = "lastLabel";
 
 /** The Constant PREVIOUS_LABEL. */
 public static final String PREVIOUS_LABEL = "previousLabel";
 
 /** The Constant NEXT_LABEL. */
 public static final String NEXT_LABEL = "nextLabel";
 
 /** The Constant AWARD. */
 public static final String REWARD_REDEMPTION = "rewardRedemption";
 
 /** The Constant URL. */
 public static final String DEFAULT_PAGINATION = "defaultPagination";
 
 /** The Constant REWARD. */
 public static final String REWARD = "rewardConfig";
 
 /** The Constant LOCATION. */
 private static final String LOCATION = "location";
 
 /** The Constant AJAX_URL. */
 private static final String SERVLET_URL = "servletUrl";
 
 private static final String AJAX_URL = "/bin/public/cms/rewards";
 
 /** The Constant SERVICE_URL. */
 private static final String SERVICE_URL = "serviceUrl";
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /**
  * Process data.
  * 
  * @param content
  *         , - {@link Map} of all user input received as input <code>ViewHelper.onGetData</code> method
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return {@link Map} holding the data
  */
 
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  Map<String, Object> properties = getProperties(content);
  
  I18n i18n = getI18n(resources);
  SlingHttpServletRequest request = getSlingRequest(resources);
  ResourceResolver resolver = getResourceResolver(resources);
  Session session = resolver.adaptTo(Session.class);
  Page currentPage = getCurrentPage(resources);
  Map<String, Object> rewardRedemptionMap = new LinkedHashMap<String, Object>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  
  String rewardLocation = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, REWARD, LOCATION);
  if (StringUtils.isBlank(rewardLocation)) {
   rewardLocation = DEFAULT_REWARD_LOCATION;
  }
  
  int itemsPerPage = getItemsPerPage(currentPage, properties);
  
  List<Map<String, Object>> rewardList = RewardHelper.getRewardList(rewardLocation, resolver, session, currentPage, request, 0, itemsPerPage,
    rewardRedemptionMap);
  
  rewardRedemptionMap.put(GENERAL_CONFIG, getGeneralConfigDataMap(properties, resolver, request, i18n, currentPage, configurationService));
  rewardRedemptionMap.put(PAGINATION_KEY, getPaginationProperties(properties, i18n, configurationService, currentPage));
  rewardRedemptionMap.put(REWARD_LIST, rewardList);
  
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(getJsonNameSpace(content), rewardRedemptionMap);
  
  return data;
 }
 
 /**
  * Gets the general config data map.
  * 
  * @param properties
  *         the properties
  * @param resourceResolver
  *         the resource resolver
  * @param slingRequest
  *         the sling request
  * @return the general config data map /** Gets the i18nMap
  * 
  * @param i18n
  * @return
  */
 protected Map<String, Object> getGeneralConfigDataMap(Map<String, Object> properties, ResourceResolver resourceResolver,
   SlingHttpServletRequest slingRequest, I18n i18n, Page currentPage, ConfigurationService configurationService) {
  Map<String, Object> generalConfigMap = new HashMap<String, Object>();
  
  String serviceUrl = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, REWARD_REDEMPTION, SERVICE_URL);
  generalConfigMap.put(SERVICE_URL, serviceUrl);
  generalConfigMap.put("stepOneDescription", MapUtils.getString(properties, "stepOneDescription", StringUtils.EMPTY));
  generalConfigMap.put("stepTwoDescription", MapUtils.getString(properties, "stepTwoDescription", StringUtils.EMPTY));
  generalConfigMap.put("nextButtonLabel", MapUtils.getString(properties, "nextButtonLabel", StringUtils.EMPTY));
  generalConfigMap.put("previousButtonLabel", MapUtils.getString(properties, "previousButtonLabel", StringUtils.EMPTY));
  generalConfigMap.put("rewardExpiryMessage", MapUtils.getString(properties, "rewardExpiryMessage", StringUtils.EMPTY));
  generalConfigMap.put("noRewardsAvailableMessage", MapUtils.getString(properties, "noRewardsAvailableMessage", StringUtils.EMPTY));
  generalConfigMap.put("nothingAvailableMessage", MapUtils.getString(properties, "nothingAvailableMessage", StringUtils.EMPTY));
  generalConfigMap.put("stepOneLabel", MapUtils.getString(properties, "stepOneLabel", StringUtils.EMPTY));
  generalConfigMap.put("stepTwoLabel", MapUtils.getString(properties, "stepTwoLabel", StringUtils.EMPTY));
  generalConfigMap.put("stepOneTitle", MapUtils.getString(properties, "stepOneTitle", StringUtils.EMPTY));
  generalConfigMap.put("stepTwoTitle", MapUtils.getString(properties, "stepTwoTitle", StringUtils.EMPTY));
  generalConfigMap.put("selectionNoRewards", i18n.get("rewardRedemption.selectionNoRewards"));
  generalConfigMap.put("selectionOneReward", i18n.get("rewardRedemption.selectionOneReward"));
  generalConfigMap.put("selectionMultipleRewards", i18n.get("rewardRedemption.selectionMultipleRewards"));
  generalConfigMap.put("rewardExpiry", i18n.get("rewardRedemption.rewardExpiry"));
  generalConfigMap.put("pointsLabel", i18n.get("rewardRedemption.pointsLabel"));
  generalConfigMap.put("rewardNotAvailable", i18n.get("rewardRedemption.rewardNotAvailable"));
  generalConfigMap.put("selectOptionLabel", i18n.get("rewardRedemption.selectOptionLabel"));
  generalConfigMap.put("selectRewardCta", i18n.get("rewardRedemption.selectRewardCta"));
  generalConfigMap.put("selectedRewardCta", i18n.get("rewardRedemption.selectedRewardCta"));
  generalConfigMap.put("notEnoughPointsCta", i18n.get("rewardRedemption.notEnoughPointsCta"));
  generalConfigMap.put("itemSummaryYourRewardsHeading", i18n.get("rewardRedemption.itemSummaryYourRewardsHeading"));
  generalConfigMap.put("itemSummaryUnavailableRewardsHeading", i18n.get("rewardRedemption.itemSummaryUnavailableRewardsHeading"));
  generalConfigMap.put("itemSummaryRewardsLabel", i18n.get("rewardRedemption.itemSummaryRewardsLabel"));
  generalConfigMap.put("itemSummaryCostLabel", i18n.get("rewardRedemption.itemSummaryCostLabel"));
  generalConfigMap.put("itemSummaryVariantsLabel", i18n.get("rewardRedemption.itemSummaryVariantsLabel"));
  generalConfigMap.put("notAvailableError", i18n.get("rewardRedemption.notAvailableError"));
  generalConfigMap.put("noVariantsAvailable", i18n.get("rewardRedemption.noVariantsAvailable"));
  generalConfigMap.put("noRewardsAvailable", i18n.get("rewardRedemption.noRewardsAvailable"));
  generalConfigMap.put(SERVLET_URL, AJAX_URL);
  generalConfigMap.put("pagePath", currentPage.getPath());
  
  boolean hideExpiredRewardsFromTheList = MapUtils.getBoolean(properties, "hideExpiredRewards", false);
  generalConfigMap.put(HIDE_EXPIRED_REWARDS_FROM_THE_LIST, hideExpiredRewardsFromTheList);
  
  return generalConfigMap;
 }
 
 /**
  * Gets the pagination properties.
  *
  * @param properties
  *         the properties
  * @param i18n
  *         the i18n
  * @return the pagination properties
  */
 private Map<String, Object> getPaginationProperties(Map<String, Object> properties, I18n i18n, ConfigurationService configurationService,
   Page currentPage) {
  Map<String, Object> paginationMap = new LinkedHashMap<String, Object>();
  String paginationCTALabel = MapUtils.getString(properties, PAGINATION_CTA_LABEL, StringUtils.EMPTY);
  String paginationType = MapUtils.getString(properties, PAGINATION_TYPE, StringUtils.EMPTY);
  
  if (StringUtils.isBlank(paginationCTALabel)) {
   paginationCTALabel = i18n.get(PAGINATION_CTA_LABEL_I18N);
  }
  
  paginationMap.put(PAGINATION_TYPE, paginationType);
  paginationMap.put(ITEMS_PER_PAGE, getItemsPerPage(currentPage, properties));
  paginationMap.put(PAGINATION_CTA_LABEL, paginationCTALabel);
  paginationMap.put(STATIC, getStaticData(i18n));
  
  return paginationMap;
 }
 
 private int getItemsPerPage(Page currentPage, Map<String, Object> properties) {
  String itemsPerPageValue = MapUtils.getString(properties, ITEMS_PER_PAGE, StringUtils.EMPTY);
  int itemsPerPage = 0;
  if (StringUtils.isBlank(itemsPerPageValue)) {
   itemsPerPage = GlobalConfigurationUtility.getIntegerValueFromConfiguration(REWARD_REDEMPTION, configurationService, currentPage,
     DEFAULT_PAGINATION);
  } else {
   itemsPerPage = Integer.parseInt(itemsPerPageValue);
  }
  return itemsPerPage;
 }
 
 private Map<String, Object> getStaticData(I18n i18n) {
  Map<String, Object> staticMap = new LinkedHashMap<String, Object>();
  staticMap.put(FIRST_LABEL, i18n.get(PAGE_LISTING_PAGINATION_FIRST));
  staticMap.put(LAST_LABEL, i18n.get(PAGE_LISTING_PAGINATION_LAST));
  staticMap.put(PREVIOUS_LABEL, i18n.get(PAGE_LISTING_PAGINATION_PREVIOUS));
  staticMap.put(NEXT_LABEL, i18n.get(PAGE_LISTING_PAGINATION_NEXT));
  return staticMap;
 }
 
}
