/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.constants;

/**
 * The Class ProductTagsConstant.
 */
public class ProductTagsConstant {
 /**
  * Identifier for title for category overview component.
  */
 public static final String TITLE = "title";
 
 /** The Constant PARENT_TAG. */
 public static final String PARENT_TAG = "parentTag";
 
 /** The Constant TEXT. */
 public static final String TEXT = "text";
 
 /** The Constant URL. */
 public static final String URL = "url";
 
 /** The Constant LINKS. */
 public static final String LINKS = "links";
 
 /** The Constant SITE_CONFIG_CATEGORY. */
 public static final String SITE_CONFIG_CATEGORY = "productTag";
 
 /** The Constant MINIMUM_LINKS. */
 public static final String MINIMUM_LINKS = "minimumLinkTags";
 
 /** The Constant MAXIMUM_LINKS. */
 public static final String MAXIMUM_LINKS = "maximumLinkTags";
 
 /** The Constant SEARCH_PAGE_RELATIVE_KEY. */
 public static final String RELATIVE_URL_CONFIG_CAT = "relativeURL";
 
 /** The Constant SEARCH_PAGE_RELATIVE_KEY. */
 public static final String SEARCH_PAGE_KEY = "searchPage";
 
 /**
  * Instantiates a new product tags constant.
  */
 private ProductTagsConstant() {
  
 }
}
