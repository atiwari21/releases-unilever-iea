/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.constants;

/**
 * The Class ShoppingBasketConstant.
 */
public class ShoppingBasketConstant {
 
 /** The Constant BASKET_TITLE. */
 public static final String BASKET_TITLE_KEY = "shoppingBasket.basketTitle";
 
 /** The Constant EMTPY_BASKET_COPY. */
 public static final String EMTPY_BASKET_COPY_KEY = "shoppingBasket.emptyBasketCopy";
 
 /** The Constant RECIPENT_NAME. */
 public static final String RECIPENT_NAME_KEY = "shoppingBasket.recipientName";
 
 /** The Constant GREETING. */
 public static final String GREETING_KEY = "shoppingBasket.greeting";
 
 /** The Constant QANTITY. */
 public static final String QANTITY_KEY = "shoppingBasket.quantity";
 
 /** The Constant GIFT_WARP_SET_LABEL. */
 public static final String GIFT_WARP_SET_LABEL_KEY = "shoppingBasket.giftWrapSetLabel";
 
 /** The Constant EDIT. */
 public static final String EDIT_KEY = "shoppingBasket.edit";
 
 /** The Constant DELETE. */
 public static final String DELETE_KEY = "shoppingBasket.delete";
 
 /** The Constant SUB_TOTAL_KEY. */
 public static final String SUB_TOTAL_KEY = "shoppingBasket.subTotal";
 
 /** The Constant BASKET_TITLE. */
 public static final String BASKET_TITLE = "basketTitle";
 
 /** The Constant EMTPY_BASKET_COPY. */
 public static final String EMTPY_BASKET_COPY = "emptyBasketCopy";
 
 /** The Constant RECIPENT_NAME. */
 public static final String RECIPENT_NAME = "recipientNameLabel";
 
 /** The Constant GREETING. */
 public static final String GREETING = "greetingLabel";
 
 /** The Constant GIFT_WARP_SET_LABEL_KEY. */
 public static final String GIFT_WARP_SET_LABEL = "giftWrapSetLabel";
 
 /** The Constant QANTITY. */
 public static final String QANTITY = "quantityLabel";
 
 /** The Constant EDIT. */
 public static final String EDIT = "editButtonLabel";
 
 /** The Constant DELETE. */
 public static final String DELETE = "deleteButtonLabel";
 
 /** The Constant SUB_TOTAL. */
 public static final String SUB_TOTAL = "subTotal";
 
 /** The Constant NAME_AND_MESSAGE_LABEL. */
 public static final String NAME_AND_MESSAGE_LABEL = "nameAndMessageLabel";
 
 /** The Constant NAME_AND_MESSAGE_KEY. */
 public static final String NAME_AND_MESSAGE_KEY = "shoppingBasket.nameAndMessage";
 
 /** The Constant ERROR_MESSAGE_LABEL. */
 public static final String ERROR_MESSAGE_LABEL = "errorMessageLabel";
 
 /** The Constant ERROR_MESSAGE_KEY. */
 public static final String ERROR_MESSAGE_KEY = "shoppingBasket.errorMessage";
 
 /**
  * Instantiates a new shopping basket constant.
  */
 private ShoppingBasketConstant() {
  
 }
}
