/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.WCMMode;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.ImageHelper;
import com.unilever.platform.foundation.components.helper.SocialCommunityHelper;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.QuickPollConstants;

/**
 * This class prepares the data map for Quick Poll ComponentComponent It reads the values from the dialog and passes the info as a json.
 */

/**
 * @author rhariv
 * 
 */
@Component(description = "QuickPoll", immediate = true, metatype = true, label = "QuickPoll")
@Service(value = { QuickPollViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.QUICK_POLL, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class QuickPollViewHelperImpl extends BaseViewHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(QuickPollViewHelperImpl.class);
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The poll helper. */
 @Reference
 QuestionViewHelperImpl questionViewHelperImpl;
 
 /** The Constant CONFIG_CATEGORY. */
 private static final String CONFIG_CATEGORY = "quickPoll";
 
 /** The Constant CSV_URL. */
 private static final String CSV_URL = "csvURL";
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("Executing QuickPollViewHelperImpl. Inside processData method ");
  
  Map<String, Object> properties = new HashMap<String, Object>();
  
  /* reading properties of the component from the dialog */
  properties = getQuickPollDetails(content, resources);
  Map<String, Object> responseMap = getQuickPollResponse(content, resources);
  
  properties.put("response", responseMap);
  String entity = "";
  try {
   entity = configurationService.getConfigValue(getCurrentPage(resources), CONFIG_CATEGORY, "entity");
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Error in getting entity for quick poll", e);
  }
  properties.put("entity", entity);
  
  properties.put(UnileverConstants.ANCHOR_NAVIGATION, ComponentUtil.getAnchorLinkMap(getProperties(content)));
  
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(getJsonNameSpace(content), properties);
  return data;
  
 }
 
 /**
  * Gets the quick poll response.
  * 
  * @param content
  *         the content
  * @param resources
  *         this method return the response details of the quick poll component
  * @return Map<String,Object> responseMap
  */
 private Map<String, Object> getQuickPollResponse(Map<String, Object> content, Map<String, Object> resources) {
  
  Map<String, Object> responseMap = new HashMap<String, Object>();
  List<Map<String, Object>> recommendedListMap = new ArrayList<Map<String, Object>>();
  
  Map<String, Object> quickPollResponseProperties = getProperties(content);
  
  String responseHeadline = null == quickPollResponseProperties.get(QuickPollConstants.RESPONSE_HEADLINE) ? StringUtils.EMPTY
    : quickPollResponseProperties.get(QuickPollConstants.RESPONSE_HEADLINE).toString();
  String responseSubHeadline = null == quickPollResponseProperties.get(QuickPollConstants.RESPONSE_SUB_HEADLINE) ? StringUtils.EMPTY
    : quickPollResponseProperties.get(QuickPollConstants.RESPONSE_SUB_HEADLINE).toString();
  String dataPresentation = null == quickPollResponseProperties.get(QuickPollConstants.DATA_PRESENTATION) ? StringUtils.EMPTY
    : quickPollResponseProperties.get(QuickPollConstants.DATA_PRESENTATION).toString();
  String responseCopy = null == quickPollResponseProperties.get(QuickPollConstants.RESPONSE_COPY) ? StringUtils.EMPTY : quickPollResponseProperties
    .get(QuickPollConstants.RESPONSE_COPY).toString();
  
  responseMap.put("headline", responseHeadline);
  responseMap.put("subHeadline", responseSubHeadline);
  responseMap.put("copy", responseCopy);
  responseMap.put("dataPresentation", dataPresentation);
  
  if (QuickPollConstants.RECOMMENDED_CONTENT.equals(dataPresentation)) {
   String recommendedIntroText = null == quickPollResponseProperties.get(QuickPollConstants.RECOMMENDED_INTRO_TEXT) ? StringUtils.EMPTY
     : quickPollResponseProperties.get(QuickPollConstants.RECOMMENDED_INTRO_TEXT).toString();
   recommendedListMap = getRecommendedContentList(resources);
   responseMap.put("recommendedIntroText", recommendedIntroText);
   responseMap.put("recommendedContent", recommendedListMap.toArray());
  }
  return responseMap;
 }
 
 /**
  * Gets the recommended content list.
  * 
  * @param dialogProps
  *         the dialog props
  * @param resources
  *         this method return the list of the recommended content pages(witht he details- title, description and image)
  * @return List<Map<String, Object>> recommendedContentList
  */
 private List<Map<String, Object>> getRecommendedContentList(Map<String, Object> resources) {
  
  List<Map<String, Object>> recommendedContentFinalList = new ArrayList<Map<String, Object>>();
  
  List<Map<String, String>> recommendedPages = ComponentUtil.getNestedMultiFieldProperties(getCurrentResource(resources), "recommendContent");
  
  Iterator<Map<String, String>> iterator = recommendedPages.iterator();
  while (iterator.hasNext()) {
   Map<String, Object> pageDetailsMap = new HashMap<String, Object>();
   Map<String, String> pageMap = iterator.next();
   String recommendedPagePath = null == pageMap.get(QuickPollConstants.RECOMMENDED_CONTENT) ? StringUtils.EMPTY : pageMap
     .get(QuickPollConstants.RECOMMENDED_CONTENT);
   boolean newWindow = false;
   String openInNewWindow = null == pageMap.get(QuickPollConstants.OPEN_IN_NEW_WINDOW) ? StringUtils.EMPTY : pageMap
     .get(QuickPollConstants.OPEN_IN_NEW_WINDOW);
   if (openInNewWindow.equals(QuickPollConstants.TRUE)) {
    newWindow = true;
   }
   
   PageManager pageManager = getPageManager(resources);
   Page recommendedPage = pageManager.getPage(recommendedPagePath);
   if (null != recommendedPage) {
    ValueMap pageProperties = recommendedPage.getProperties();
    
    String title = null == recommendedPage.getPageTitle() ? StringUtils.EMPTY : recommendedPage.getPageTitle();
    String imagePath = null == recommendedPage.getProperties(QuickPollConstants.IMAGE).get(QuickPollConstants.FILE_REFERENCE) ? StringUtils.EMPTY
      : recommendedPage.getProperties(QuickPollConstants.IMAGE).get(QuickPollConstants.FILE_REFERENCE).toString();
    String teaserImagePath = MapUtils.getString(pageProperties, UnileverConstants.TEASER_IMAGE, imagePath);
    String teaserImageAlt = MapUtils.getString(pageProperties, UnileverConstants.TEASER_IMAGE_ALT_TEXT,
      ImageHelper.fetchImageFileName(teaserImagePath));
    String description = null == recommendedPage.getDescription() ? StringUtils.EMPTY : recommendedPage.getDescription();
    Map<String, Object> imageMap = SocialCommunityHelper.getImageMapFromPath(resources, recommendedPage, teaserImageAlt, teaserImagePath);
    pageDetailsMap.put("title", title);
    pageDetailsMap.put("description", description);
    pageDetailsMap.put("image", imageMap);
    pageDetailsMap.put("url", ComponentUtil.getFullURL(getResourceResolver(resources), recommendedPagePath, getSlingRequest(resources)));
    pageDetailsMap.put("openInNewWindow", newWindow);
   }
   
   recommendedContentFinalList.add(pageDetailsMap);
  }
  
  return recommendedContentFinalList;
  
 }
 
 /**
  * Gets the quick poll details.
  * 
  * @param content
  *         the content
  * @param resources
  *         this method returns the quick poll details, read from the dialog
  * @return Map<String, Object> quickPollMap
  */
 private Map<String, Object> getQuickPollDetails(Map<String, Object> content, Map<String, Object> resources) {
  
  I18n i18n = getI18n(resources);
  Map<String, Object> quickPollProperties = getProperties(content);
  Map<String, Object> quickPollMap = new HashMap<String, Object>();
  String resultChartType = MapUtils.getString(quickPollProperties, QuickPollConstants.RESULT_CHART_TYPE, StringUtils.EMPTY);
  String headline = MapUtils.getString(quickPollProperties, QuickPollConstants.QUICK_POLL_HEADLINE, StringUtils.EMPTY);
  String questionPath = MapUtils.getString(quickPollProperties, QuickPollConstants.QUESTION_PATH, StringUtils.EMPTY);
  String readMoreLabel = null != i18n.get(QuickPollConstants.QUICK_POLL_READ_MORE_LABEL) ? i18n.get(QuickPollConstants.QUICK_POLL_READ_MORE_LABEL)
    : StringUtils.EMPTY;
  String readLessLabel = null == i18n.get(QuickPollConstants.QUICK_POLL_READ_LESS_LABEL) ? StringUtils.EMPTY : i18n.get(
    QuickPollConstants.QUICK_POLL_READ_LESS_LABEL).toString();
  String quickPollSubmit = null == i18n.get(QuickPollConstants.QUICK_POLL_SUBMIT_LABEL) ? StringUtils.EMPTY : i18n.get(
    QuickPollConstants.QUICK_POLL_SUBMIT_LABEL).toString();
  
  ResourceResolver resourceResolver = getResourceResolver(resources);
  Resource questionResource = resourceResolver.getResource(questionPath);
  Map<String, Object> question = SocialCommunityHelper.getQuestionResourceProperties(resources, questionResource);
  String pollId = questionResource != null && questionResource.getValueMap() != null ? (String) questionResource.getValueMap().get("id") : "";
  Map<String, Object> overrideConfigMap = GlobalConfigurationUtility.getOverriddenConfigMap(CONFIG_CATEGORY, getCurrentPage(resources),
    quickPollProperties);
  
  mapUrlsInConfig(overrideConfigMap, resourceResolver);
  
  Map<String, Object> exportMap = new HashMap<String, Object>();
  exportMap.put("label", i18n.get("quickPoll.export"));
  exportMap.put("downloadCsvURL", overrideConfigMap.get(CSV_URL));
  if (WCMMode.fromRequest(getSlingRequest(resources)) == WCMMode.EDIT || WCMMode.fromRequest(getSlingRequest(resources)) == WCMMode.DESIGN) {
   exportMap.put("display", true);
  } else {
   exportMap.put("display", false);
  }
  quickPollMap.put("exportButton", exportMap);
  quickPollMap.put(QuickPollConstants.RESULT_CHART_TYPE, resultChartType);
  quickPollMap.put("pollId", pollId);
  quickPollMap.put("headline", headline);
  quickPollMap.put("readMore", readMoreLabel);
  quickPollMap.put("readLess", readLessLabel);
  quickPollMap.put("submit", quickPollSubmit);
  quickPollMap.put("question", question);
  quickPollMap.put("configuration", overrideConfigMap);
  
  return quickPollMap;
 }
 
 /**
  * Map urls in config.
  * 
  * @param overrideConfigMap
  *         the override config map
  * @param resourceResolver
  *         the resource resolver
  */
 private void mapUrlsInConfig(Map<String, Object> overrideConfigMap, ResourceResolver resourceResolver) {
  overrideConfigMap.put("getPollUrl", resourceResolver.map(MapUtils.getString(overrideConfigMap, "getPollUrl", "")));
  overrideConfigMap.put("postPollUrl", resourceResolver.map(MapUtils.getString(overrideConfigMap, "postPollUrl", "")));
  overrideConfigMap.put(CSV_URL, resourceResolver.map(MapUtils.getString(overrideConfigMap, CSV_URL, "")));
 }
 
}
