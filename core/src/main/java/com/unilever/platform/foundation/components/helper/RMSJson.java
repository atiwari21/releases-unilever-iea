/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.components.helper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.osgi.services.HttpClientBuilderFactory;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.RecipeConfigDTO;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * The Class RMSJson.
 */
public class RMSJson {
 
 private static final String ENV = "env";
 
 /** The Constant LOCALE. */
 private static final String LOCALE = "locale";
 
 /** The Constant BRAND. */
 private static final String BRAND = "brand";
 
 /** The Constant DATA. */
 private static final String DATA = "data";
 
 private static final String ERROR_PAGEPATH = "errorPagePath";
 
 private static final String RECIPE_ID = "recipeId";
 
 private static final String RMS_RMS_SERVICE_ERROR = "rms.rmsServiceError";
 
 private static final String RMS_SERVICE_DOWN = "rmsServiceDown";
 
 private static final String RMS_SERVICE_ERROR = "rmsServiceError";
 
 private static final String RECIPE_CONFIG_RESOURCETYPE = "unilever-aem/components/content/recipeConfig";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(RMSJson.class);
 
 private RMSJson() {
  
 }
 
 /**
  * Gets the recipe json from page.
  * 
  * @param resources
  *         the resources
  * @param recipeProperties
  *         the recipe properties
  * @param recipesMap
  *         the recipes map
  * @param recipeConfig
  *         the recipe config
  * @param factory
  *         the factory
  * @return the recipe json from page
  */
 public static void getRecipeJsonFromPage(Map<String, Object> resources, Map<String, Object> recipeProperties, Map<String, Object> recipesMap,
   RecipeConfigDTO recipeConfig, HttpClientBuilderFactory factory) {
  
  LOGGER.info("Inside getRecipeJsonFromPage method to get recipe json based on page");
  JSONObject jsonObj = null;
  
  String recipePath = MapUtils.getString(recipeProperties, UnileverConstants.RECIPE);
  
  ResourceResolver resourceResolver = BaseViewHelper.getResourceResolver(resources);
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) resources.get(ProductConstants.REQUEST);
  Resource compResource = slingRequest.getResource();
  Page currentPage = pageManager.getContainingPage(compResource);
  
  Page recipePage = recipePath != null ? currentPage.getPageManager().getPage(recipePath) : currentPage;
  
  ValueMap properties = recipePage.getProperties();
  String productPropertyAttribute = properties.get(UnileverConstants.RECIPE_ID) != null ? properties.get(UnileverConstants.RECIPE_ID).toString()
    : StringUtils.EMPTY;
  
  try {
   LOGGER.info("Inside Try Block of get recipe json based on page");
   
   String newUrl = MessageFormat.format(recipeConfig.getEndPointUrl(), productPropertyAttribute);
   jsonObj = getRecipe(recipeConfig.getTimeout(), factory, newUrl, recipeConfig.getBrand(), recipeConfig.getLocale(), recipeConfig);
   
   Map<String, Object> finalmap = new HashMap<String, Object>();
   if (jsonObj != null && !jsonObj.has("error")) {
    finalmap = RecipeHelper.toMap(jsonObj);
    setSortedFinalMap(finalmap, "nutrients", "nutrients", "nutrients", currentPage, "name");
    setSortedFinalMap(finalmap, "nutrients", "nutrientsPerServing", "nutrients", currentPage, "name");
    if (MapUtils.isNotEmpty(finalmap)) {
     recipesMap.put(UnileverConstants.RECIPES, finalmap);
    }
   }
   
  } catch (Exception e1) {
   LOGGER.info("Inside Catch Block of get recipe json based on page");
   LOGGER.error("error --> ", e1);
  }
 }
 
 
 /**
  * Gets the recipe.
  * 
  * @param timeout
  *         the timeout
  * @param factory
  *         the factory
  * @param newUrl
  *         the new url
  * @param brand
  *         the brand
  * @param locale
  *         the locale
  *         @param recipeConfigDTO
  * @return the recipe
  */
 public static JSONObject getRecipe(int timeout, HttpClientBuilderFactory factory, String newUrl, String brand, String locale,
   RecipeConfigDTO recipeConfigDTO) {
  
  JSONObject dataObj = null;
  
  RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(timeout).setConnectionRequestTimeout(timeout).setSocketTimeout(timeout)
    .build();
  
  CloseableHttpClient httpclient = factory.newBuilder().disableAutomaticRetries().disableConnectionState().disableRedirectHandling()
    .setDefaultRequestConfig(requestConfig).build();
  
  HttpGet request = new HttpGet(newUrl);
  request.addHeader("content-type", "application/json");
  request.addHeader(BRAND, brand);
  request.addHeader(LOCALE, locale);
  if (recipeConfigDTO.isCalmenu()) {
   request.addHeader("isCalcmenu", "true");
  }
  CloseableHttpResponse response = null;
  try {
   LOGGER.error("request is" + request.toString());
   response = httpclient.execute(request);
   LOGGER.error("response is" + response.toString());
   if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
    LOGGER.error("Connection Established successfully.");
   } else {
    LOGGER.error("Connection do not Established successfully.");
   }
   
   if (response != null) {
    HttpEntity httpEntity = response.getEntity();
    if (httpEntity != null) {
     InputStream inputStream = httpEntity.getContent();
     
     BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
     StringBuilder response1 = new StringBuilder();
     String inputLine;
     while ((inputLine = in.readLine()) != null) {
      response1.append(inputLine);
     }
     in.close();
     
     JSONObject jsonobj = new JSONObject(response1.toString());
     
     if (jsonobj != null && jsonobj.has(DATA)) {
      LOGGER.debug("Get data of api json.");
      dataObj = getDataObjectTryCatchBlock(dataObj, jsonobj);
     }
    }
   }
   
  } catch (ClientProtocolException cpe) {
   dataObj = handleExceptions("Error while getting json (cpe) : ", cpe);
  } catch (IOException ioe) {
   dataObj = handleExceptions("Error while getting json (ioe) : ", ioe);
  } catch (JSONException jse) {
   dataObj = handleExceptions("Error while getting json (jse) : ", jse);
  } finally {
   try {
    if (response != null) {
     response.close();
    }
    httpclient.close();
   } catch (IOException ioe) {
    LOGGER.error("Could not release connection", ioe);
   }
   
  }
  return dataObj;
 }
 
 private static JSONObject getDataObjectTryCatchBlock(JSONObject dataObj, JSONObject jsonobj) throws JSONException {
  JSONObject localDataObj = dataObj;
  if (!jsonobj.getBoolean("hasErrors")) {
   if (StringUtils.equalsIgnoreCase(jsonobj.getString(DATA), "null")) {
    throw new JSONException("RMS data is null");
   } else {
    localDataObj = (JSONObject) jsonobj.get(DATA);
   }
  } else {
   throw new JSONException("RMS data has error");
  }
  return localDataObj;
 }
 
 /**
  * Gets the recipe by POST.
  * 
  * @param requestBody
  *         the request body
  * @param timeout
  *         the timeout
  * @param factory
  *         the factory
  * @param newUrl
  *         the new url
  * @param brand
  *         the brand
  * @param locale
  *         the locale
  * @param author
  * @return the recipe by POST
  */
 public static JSONObject getRecipeByPOST(JSONObject requestBody, int timeout, HttpClientBuilderFactory factory, String newUrl, String brand,
   String locale, boolean author) {
  
  JSONObject dataObj = null;
  
  RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(timeout).setConnectionRequestTimeout(timeout).setSocketTimeout(timeout)
    .build();
  
  CloseableHttpClient httpclient = factory.newBuilder().disableAutomaticRetries().disableConnectionState().disableRedirectHandling()
    .setDefaultRequestConfig(requestConfig).build();
  
  HttpPost request = new HttpPost(newUrl);
  
  StringEntity params = null;
  try {
   params = new StringEntity(requestBody.toString());
   LOGGER.debug("params are=" + params);
  } catch (UnsupportedEncodingException uee) {
   LOGGER.error("Email template error UnsupportedEncodingException : ", uee);
  }
  
  request.addHeader("content-type", "application/json");
  request.addHeader(BRAND, brand);
  request.addHeader(LOCALE, locale);
  if (author) {
   request.addHeader(ENV, "author");
  }
  request.setEntity(params);
  CloseableHttpResponse response = null;
  try {
   LOGGER.debug("request is" + request.toString());
   response = httpclient.execute(request);
   LOGGER.debug("response is" + response.toString());
   if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
    LOGGER.debug("Connection Established successfully.");
   } else {
    LOGGER.error("Connection do not Established successfully.");
   }
   
   if (response != null) {
    HttpEntity httpEntity = response.getEntity();
    if (httpEntity != null) {
     InputStream inputStream = httpEntity.getContent();
     
     BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
     StringBuilder response1 = new StringBuilder();
     String inputLine;
     while ((inputLine = in.readLine()) != null) {
      response1.append(inputLine);
     }
     in.close();
     
     JSONObject jsonobj = new JSONObject(response1.toString());
     
     if (jsonobj != null && jsonobj.has(DATA)) {
      LOGGER.debug("Get data of api json.");
      dataObj = getDataObjectTryCatchBlock(dataObj, jsonobj);
     }
    }
   }
   
  } catch (ClientProtocolException cpe) {
   dataObj = handleExceptions("Error while getting json (cpe) : ", cpe);
  } catch (IOException ioe) {
   dataObj = handleExceptions("Error while getting json (ioe) : ", ioe);
  } catch (JSONException jse) {
   dataObj = handleExceptions("Error while getting json (jse) : ", jse);
  } finally {
   try {
    if (response != null) {
     response.close();
    }
    httpclient.close();
   } catch (IOException ioe) {
    LOGGER.error("Could not release connection", ioe);
   }
   
  }
  return dataObj;
 }
 
 /**
  * gets the error page path of RMS
  * 
  * @param resources
  * @param configurationService
  * @return
  * @throws ConfigurationNotFoundException
  */
 public static String getErrorPage(Map<String, Object> resources, ConfigurationService configurationService) throws ConfigurationNotFoundException {
  String errorPage = configurationService.getConfigValue(BaseViewHelper.getCurrentPage(resources), "rmsEndPoint", "errorPages");
  errorPage = ComponentUtil.getFullURL(BaseViewHelper.getResourceResolver(resources), errorPage, BaseViewHelper.getSlingRequest(resources));
  return errorPage;
 }
 
 /**
  * sets the rmsServiceDownKey.
  * 
  * @param resources
  *         the resources
  * @param recipesMap
  *         the recipes map
  * @param configurationService
  *         the configuration service
  * @param i18n
  *         the i 18 n
  */
 public static void setRMSServiceDownKey(Map<String, Object> resources, Map<String, Object> recipesMap, ConfigurationService configurationService,
   I18n i18n) {
  boolean rmsServiceDown = false;
  boolean allow = true;
  Page currentPage = BaseViewHelper.getCurrentPage(resources);
  String recipeId = (String) currentPage.getProperties().getOrDefault(RECIPE_ID, StringUtils.EMPTY);
  if (StringUtils.isEmpty(recipeId) && getRecipeLandingPages(configurationService, currentPage).contains(currentPage.getPath())) {
    allow = false;
  }
  if (MapUtils.isEmpty(recipesMap) && allow) {
   rmsServiceDown = true;
   try {
    recipesMap.put(ERROR_PAGEPATH, getErrorPage(resources, configurationService));
    recipesMap.put(RMS_SERVICE_ERROR, i18n.get(RMS_RMS_SERVICE_ERROR));
   } catch (ConfigurationNotFoundException e) {
    LOGGER.error("cannot read errorPagesPath ", e);
   }
  }
  recipesMap.put(RMS_SERVICE_DOWN, rmsServiceDown);
 }
 
 /**
  * 
  * @param resources
  * @param recipesMap
  * @param finalMap
  * @param configurationService
  * @param i18n
  * @param error
  */
 public static void setRMSServiceDownKey(Map<String, Object> resources, Map<String, Object> recipesMap, ConfigurationService configurationService,
   I18n i18n, boolean error) {
  boolean rmsServiceDown = false;
  Page currentPage = BaseViewHelper.getCurrentPage(resources);
  String recipeId = (String) currentPage.getProperties().getOrDefault(RECIPE_ID, StringUtils.EMPTY);
  boolean allow = true;
  if (StringUtils.isEmpty(recipeId) && getRecipeLandingPages(configurationService, currentPage).contains(currentPage.getPath())) {
    allow = false;
  }
  if (error && allow) {
   rmsServiceDown = true;
   try {
    recipesMap.put(ERROR_PAGEPATH, getErrorPage(resources, configurationService));
    recipesMap.put(RMS_SERVICE_ERROR, i18n.get(RMS_RMS_SERVICE_ERROR));
   } catch (ConfigurationNotFoundException e) {
    LOGGER.error("cannot read errorPagesPath.", e);
   }
  }
  recipesMap.put(RMS_SERVICE_DOWN, rmsServiceDown);
 }
 
 /**
  * creates a list of recipe detail template pages from recipeConfig
  * 
  * @param configurationService
  * @param currentPage
  * @return
  */
 public static List<String> getRecipeLandingPages(ConfigurationService configurationService, Page currentPage) {
  List<String> detailPagePath = new ArrayList<String>();
  String recipeConfigPagePath = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, "recipeConfig", "url");
  if (StringUtils.isNotBlank(recipeConfigPagePath)) {
   Page recipeConfigpage = currentPage.getPageManager().getPage(recipeConfigPagePath);
   if (recipeConfigpage != null) {
    Iterable<Resource> child = recipeConfigpage.getContentResource().getChild("par").getChildren();
    
    for (Resource res : child) {
     if (RECIPE_CONFIG_RESOURCETYPE.equals(res.getResourceType())) {
      ValueMap properties = res.getValueMap();
      
      String basicTemplatePage = MapUtils.getString(properties, "basicRecipeDetailTemplatePagePath", StringUtils.EMPTY);
      String enhancedTemplatePage = MapUtils.getString(properties, "enhancedRecipeDetailTemplatePagePath", StringUtils.EMPTY);
      String recipeLandingPage = MapUtils.getString(properties, "recipeLandingPagePath", StringUtils.EMPTY);
      if (StringUtils.isNotEmpty(basicTemplatePage)) {
       detailPagePath.add(basicTemplatePage);
      }
      if (StringUtils.isNotEmpty(enhancedTemplatePage)) {
       detailPagePath.add(enhancedTemplatePage);
      }
      if (StringUtils.isNotEmpty(recipeLandingPage)) {
       detailPagePath.add(recipeLandingPage);
      }
     }
    }
   }
  }
  return detailPagePath;
 }
 
 /**
  * method to initialize dataJSON object, and print appropriate cause of exception.
  * 
  * @param message
  * @param e
  * @return
  */
 public static JSONObject handleExceptions(String message, Exception e) {
  JSONObject dataObj = null;
  try {
   dataObj = new JSONObject("{\"error\":" + true + "}");
  } catch (JSONException jsonExc) {
   LOGGER.error("error while creating error json", jsonExc.getMessage(),jsonExc);
  }
  String causeMessage = e.getMessage();
  Throwable cause = e.getCause();
  if (cause != null) {
   causeMessage = causeMessage + "and cause is" + cause.getMessage();
  }
  LOGGER.error(message + "  " + causeMessage);
  return dataObj;
 }
 
 /**
  * @param resources
  * 
  * @param recipesMap
  * 
  * @param finalMap
  * 
  * @param configurationService
  * 
  * @param jsonObject
  * 
  * @param i18n
  */
 public static void setRMSServiceDownKey(Map<String, Object> resources, Map<String, Object> recipesMap, JSONObject jsonObject,
   ConfigurationService configurationService, I18n i18n) {
  boolean rmsServiceDown = false;
  boolean allow = true;
  Page currentPage = BaseViewHelper.getCurrentPage(resources);
  String recipeId = (String) currentPage.getProperties().getOrDefault(RECIPE_ID, StringUtils.EMPTY);
  if (StringUtils.isEmpty(recipeId) && getRecipeLandingPages(configurationService, currentPage).contains(currentPage.getPath())) {
   allow = false;
  }
  if (jsonObject != null && jsonObject.has("error") && allow) {
   rmsServiceDown = true;
   try {
    recipesMap.put(ERROR_PAGEPATH, getErrorPage(resources, configurationService));
    recipesMap.put(RMS_SERVICE_ERROR, i18n.get(RMS_RMS_SERVICE_ERROR));
   } catch (ConfigurationNotFoundException e) {
    LOGGER.error("cannot read errorPagesPath", e);
   }
  }
  recipesMap.put(RMS_SERVICE_DOWN, rmsServiceDown);
 }
 
 @SuppressWarnings("unchecked")
 public static void setSortedFinalMap(Map<String, Object> finalmap, String categoryKey, String key1, String key2, Page currentPage, String sortKey){
  String sortingOrder = GlobalConfigurationUtility.getValueFromConfiguration(currentPage.adaptTo(ConfigurationService.class), currentPage,
    "sortOrderCategory", categoryKey);
  if (StringUtils.isNotBlank(sortingOrder)) {
   List<String> sortingOrderList = Arrays.asList(sortingOrder.split(","));
   List<Map<String, Object>> nutrients = finalmap.containsKey(key1) ? (List<Map<String, Object>>) finalmap.get(key1) : Collections
     .emptyList();
   for (int i = 0; i < nutrients.size(); i++) {
    List<Map<String, Object>> actualAttributesMap = (List<Map<String, Object>>) nutrients.get(i).get(key2);
    actualAttributesMap = ComponentUtil.sortListByConfigOrder(actualAttributesMap, sortKey, sortingOrderList);
    Map<String, Object>[] attArray = new Map[actualAttributesMap.size()];
    nutrients.get(i).put(key2, actualAttributesMap.toArray(attArray));
   }
  }
 }
}
