/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.sapient.platform.iea.aem.utils.constants.CommonConstants;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.FormElementConstants;

/**
 * This class prepares the data map for file upload component used for campaigning forms.
 */
@Component(description = "FormCheckBoxViewHelperImpl", immediate = true, metatype = true, label = "FormCheckBoxViewHelperImpl")
@Service(value = { FormCheckBoxViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.CHECKBOX_COMPONENT, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class FormCheckBoxViewHelperImpl extends BaseViewHelper {
 
 /**
  * logger object.
  */
 private static final Logger LOGGER = LoggerFactory.getLogger(FormCheckBoxViewHelperImpl.class);
 
 public static final String NAME = "name";
 public static final String DESCRIPTION = "description";
 public static final String RULES = "rules";
 public static final String MESSAGE = "msg";
 public static final String MSGFORMAT = "msgFormat";
 public static final String REQUIRED = "required";
 public static final String LABEL = "label";
 public static final String LABELVALUE = "File upload";
 public static final String CHECKBOX = "checkbox";
 public static final String SHOWMANDATORY = "showMandatory";
 public static final String TRUE = "true";
 public static final String CHECKED = "checked";
 
 /**
  * <p>
  * This method returns image upload configuration map.
  * </p>
  * 
  * @param content
  *         {@link Map} of all user input received as input
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return Map holding the result data.
  **/
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  LOGGER.info("inside breadcrumb viewhelper processData");
  Map<String, Object> checkboxDataMap = new HashMap<String, Object>();
  Map<String, Object> data = new HashMap<String, Object>();
  Map<String, Object> properties = (Map<String, Object>) content.get(PROPERTIES);
  String msg = MapUtils.getString(properties, MESSAGE);
  String checked = MapUtils.getString(properties, CHECKED);
  String required = MapUtils.getString(properties, FormElementConstants.REQUIRED_FIELD);
  checkboxDataMap.put(NAME, MapUtils.getString(properties, FormElementConstants.ELEMENT_ID));
  checkboxDataMap.put(DESCRIPTION, MapUtils.getString(properties, DESCRIPTION));
  checkboxDataMap.put(SHOWMANDATORY, Boolean.parseBoolean(required));
  checkboxDataMap.put(CHECKED, Boolean.parseBoolean(checked));
  checkboxDataMap.put(RULES, (getValidationMap(msg, required)).toArray());
  checkboxDataMap.put(FormElementConstants.VIEW_TYPE, CHECKBOX);
  checkboxDataMap.put(UnileverConstants.RANDOM_NUMBER, MapUtils.getString(properties, UnileverConstants.RANDOM_NUMBER, StringUtils.EMPTY));
  String jsonNameSpace = MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE);
  data.put(jsonNameSpace, checkboxDataMap);
  LOGGER.debug("The Image data map returned is " + checkboxDataMap);
  return data;
 }
 
 /**
  * Gets the validation map.
  * 
  * @param msg
  *         the msg
  * @param required
  *         the required
  * @return the validation map
  */
 private List<Map<String, Object>> getValidationMap(String msg, String required) {
  List<Map<String, Object>> rulesList = new LinkedList<Map<String, Object>>();
  Map<String, Object> requiredMap = new HashMap<String, Object>();
  requiredMap.put(MESSAGE, msg);
  requiredMap.put(REQUIRED, Boolean.parseBoolean(required));
  rulesList.add(requiredMap);
  return rulesList;
  
 }
 
}
