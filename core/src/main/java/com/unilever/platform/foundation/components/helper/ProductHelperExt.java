/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.components.helper;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Session;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.api.CommerceException;
import com.adobe.cq.commerce.api.Product;
import com.day.cq.commons.ImageResource;
import com.day.cq.tagging.Tag;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.unilever.d2.platform.foundation.core.crypto.CryptoFactory;
import com.unilever.d2.platform.foundation.core.crypto.Cryptor;
import com.unilever.platform.aem.foundation.commerce.api.CustomProduct;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.commerce.api.WrapperInfo;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.core.dto.BrandMarketBean;
import com.unilever.platform.foundation.constants.ContainerTagConstants;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.Image;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * The Class ProductHelperExt.
 */
public class ProductHelperExt {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ProductHelperExt.class);
 
 /** The Constant PRODUCt_SELECT_CTA_LABEL. */
 private static final String PRODUCT_SELECT_CTA_LABEL = "storeSearch.productSelectCtaLabel";
 /** The Constant PRODUCT_SELECT_CTA_KEY. */
 private static final String PRODUCT_SELECT_CTA_KEY = "productSelectCtaLabel";
 /** The Constant TWO. */
 private static final int TWO = 2;
 /**
  * Instantiates a new product helper ext.
  */
 private ProductHelperExt() {
  
 }
 
 /**
  * Find product.
  * 
  * @param currentPage
  *         the current page
  * @param productId
  *         the product id
  * @return the UNI product
  */
 public static UNIProduct findProduct(Page currentPage, String productId) {
  LOGGER.debug("Inside findProduct of ProductHelperExt");
  UNIProduct currentProduct = null;
  try {
   if (currentPage != null) {
    ConfigurationService configurationService = currentPage.adaptTo(ConfigurationService.class);
    String productPath = null;
    productPath = getProductPath(currentPage, configurationService);
    LOGGER.debug("Product path from configuration : {} ", productPath);
    if (!StringUtils.isBlank(productPath)) {
     Resource contentResource = currentPage.getContentResource();
     ResourceResolver resourceResolver = contentResource != null ? contentResource.getResourceResolver() : null;
     Node productNode = getNodeFromPath(resourceResolver, productPath, productId);
     String productNodeName = productNode != null ? productNode.toString() : "";
     LOGGER.debug("productNode in findProduct: {}", productNodeName);
     String productFinalPath = productNode != null ? productNode.getPath() : null;
     LOGGER.debug("productFinalPath in findProduct: {}", productFinalPath);
     currentProduct = getProductByPath(resourceResolver, productFinalPath);
    }
   }
  } catch (Exception e) {
   LOGGER.error("Error in findProduct for {} ", productId, e);
  }
  return currentProduct;
 }
 
 /**
  * Gets the product by path.
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param productFinalPath
  *         the product final path
  * @return the product by path
  */
 public static UNIProduct getProductByPath(ResourceResolver resourceResolver, String productFinalPath) {
  Resource productResource = resourceResolver != null ? resourceResolver.getResource(productFinalPath) : null;
  Product product = productResource != null ? productResource.adaptTo(Product.class) : null;
  LOGGER.debug("product in getProductByPath : {}", product);
  UNIProduct currentProduct = null;
  if (null != product && product instanceof UNIProduct) {
   currentProduct = (UNIProduct) product;
  }
  return currentProduct;
 }
 
 /**
  * Gets the product path.
  * 
  * @param currentPage
  *         the current page
  * @param configurationService
  *         the configuration service
  * @return the product path
  */
 private static String getProductPath(Page currentPage, ConfigurationService configurationService) {
  String productPath = StringUtils.EMPTY;
  try {
   productPath = configurationService.getConfigValue(currentPage, "brand", UnileverConstants.PRODUCT_PATH_CONFIG_KEY);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Error in finding configuration for productPath ", e);
  }
  return productPath;
 }
 
 /**
  * Gets the node from path.
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param path
  *         the path
  * @param nodeName
  *         the node name
  * @return the node from path
  */
 public static Node getNodeFromPath(ResourceResolver resourceResolver, String path, String nodeName) {
  LOGGER.debug("Inside getProductNodePath of ProductHelperExt");
  Node resultNode = null;
  try {
   Session session = resourceResolver.adaptTo(Session.class);
   // Set the query & Obtain the query manager for the session ...
   QueryManager queryManager = session.getWorkspace().getQueryManager();
   // Setup the query to get all tags of a parent tag at provided path
   String sqlStatement = "SELECT * FROM [nt:base] AS s WHERE ISDESCENDANTNODE([" + path + "]) and NAME() = '" + nodeName + "'";
   Query query = queryManager.createQuery(sqlStatement, "JCR-SQL2");
   // Execute the query and get the results
   
   QueryResult results = query.execute();
   // Iterate over the nodes in the results
   NodeIterator itr = results != null ? results.getNodes() : null;
   if (null != itr && itr.getSize() > 0) {
    resultNode = itr.nextNode();
   }
   
  } catch (Exception e) {
   LOGGER.error("Error in getProductNodePath of ProductHelperExt in finding path of {} ", nodeName, e);
  }
  return resultNode;
 }
 
 /**
  * Checks if is customizable product.
  * 
  * @param product
  *         the product
  * @param configurationService
  *         the configuration service
  * @param page
  *         the page
  * @return true, if is customizable product
  */
 public static boolean isCustomizableProduct(UNIProduct product, ConfigurationService configurationService, Page page) {
  boolean flag = false;
  if (page != null && product != null) {
   ResourceResolver resourceResolver = page.getContentResource().getResourceResolver();
   Tag[] tags = product.getTags(resourceResolver);
   String customizableTag = StringUtils.EMPTY;
   try {
    customizableTag = configurationService.getConfigValue(page, UnileverConstants.EGIFTING_CONFIG_CAT, "customizableTag");
   } catch (ConfigurationNotFoundException e) {
    LOGGER.error("could not find customizableTag key in egifting category", e);
   }
   
   flag = getCustomisableFlag(tags, customizableTag);
  }
  return flag;
  
 }
 
 /**
  * Gets the customisable flag.
  * 
  * @param tags
  *         the tags
  * @param customizableTag
  *         the customizable tag
  * @return the customisable flag
  */
 public static boolean getCustomisableFlag(Tag[] tags, String customizableTag) {
  boolean flag = false;
  if (tags != null && StringUtils.isNotBlank(customizableTag)) {
   for (Tag tag : tags) {
    String tagId = tag != null ? tag.getTagID() : "";
    if (tagId.equals(customizableTag)) {
     flag = true;
     break;
    }
   }
  }
  return flag;
 }
 
 /**
  * Gets the customise data map.
  * 
  * @param customProduct
  *         the custom product
  * @return the customise data map
  */
 public static Map<String, Object> getCustomiseDataMap(CustomProduct customProduct) {
  Map<String, Object> customiseDataMap = new HashMap<String, Object>();
  String personalisationDesc = customProduct.getCustomizeDescription() != null ? customProduct.getCustomizeDescription() : "";
  customiseDataMap.put(ProductConstants.PERSONALISED_DESC, personalisationDesc);
  LOGGER.debug("customised  Data Map Properties : " + customiseDataMap);
  return customiseDataMap;
 }
 
 /**
  * Sets the product gift map.
  * 
  * @param uniProduct
  *         the uni product
  * @param currentPage
  *         the current page
  * @param slingRequest
  *         the sling request
  * @return the product gift map
  */
 public static Map<String, Object> getProductGiftMap(UNIProduct uniProduct, Page currentPage, SlingHttpServletRequest slingRequest) {
  Map<String, Object> productGiftMap = new HashMap<String, Object>();
  CustomProduct customeProduct = uniProduct.getCustomProduct();
  ConfigurationService configurationService = currentPage.adaptTo(ConfigurationService.class);
  String encriptionKey = "";
  
  try {
   encriptionKey = configurationService.getConfigValue(currentPage, ProductConstants.EGIFTING_CAT, ProductConstants.ENCRYPTION_KEY);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find encriptionKey key in configuration ", e);
  }
  LOGGER.debug("encriptionKey used for encryption of Egifting prices: " + encriptionKey);
  if (customeProduct != null) {
   DecimalFormat df = new DecimalFormat("#0.00");
   String customTotalPrice = customeProduct.getCustomizeProductPrice() != null ? df.format(customeProduct.getCustomizeProductPrice()) : "0.0";
   String wrappingPrice = customeProduct.getWrappingPrice() != null ? df.format(customeProduct.getWrappingPrice()) : "0.0";
   String wrappingDescriptionText = customeProduct.getCustomizeGiftWrapDescription() != null ? customeProduct.getCustomizeGiftWrapDescription()
     : StringUtils.EMPTY;
   Cryptor cryptor = CryptoFactory.getInstance(encriptionKey, uniProduct.getSKU());
   String encryptedPrice = cryptor.crypt(UnileverConstants.ENCRYPT_MODE, customTotalPrice);
   String encryptedWrappingPrice = cryptor.crypt(UnileverConstants.ENCRYPT_MODE, wrappingPrice);
   productGiftMap.put(ProductConstants.CUSTOMISED_PRICE, StringUtils.isNotBlank(customTotalPrice) ? customTotalPrice : StringUtils.EMPTY);
   productGiftMap.put(ProductConstants.CUSTOMISED_WRAPPING_PRICE, StringUtils.isNotBlank(wrappingPrice) ? wrappingPrice : StringUtils.EMPTY);
   productGiftMap.put(ProductConstants.WRAPPING_DESC_TEXT, wrappingDescriptionText);
   productGiftMap.put(ProductConstants.ENCRYPTED_WRAPPING_PRICE, encryptedWrappingPrice);
   productGiftMap.put(ProductConstants.ENCRYPTED_PRICE, encryptedPrice);
   productGiftMap.put(ProductConstants.GIFT_WRAPS, getGiftWrappersList(customeProduct, currentPage, slingRequest));
  }
  return productGiftMap;
 }
 
 /**
  * Gets the gift wrappers list.
  * 
  * @param customeProduct
  *         the custome product
  * @param currentPage
  *         the current page
  * @param slingRequest
  *         the sling request
  * @return the gift wrappers list
  */
 public static Object[] getGiftWrappersList(CustomProduct customeProduct, Page currentPage, SlingHttpServletRequest slingRequest) {
  List<WrapperInfo> wrappersInfoList = customeProduct.getWrapperInfo();
  List<Map<String, Object>> giftWrappersList = new ArrayList<Map<String, Object>>();
  
  if (wrappersInfoList != null && !wrappersInfoList.isEmpty()) {
   for (WrapperInfo wrapperInfo : wrappersInfoList) {
    Map<String, Object> giftWrapperMap = new HashMap<String, Object>();
    giftWrapperMap.put(UnileverConstants.ID, StringUtils.isNotBlank(wrapperInfo.getId()) ? wrapperInfo.getId() : StringUtils.EMPTY);
    giftWrapperMap.put(UnileverConstants.TITLE, StringUtils.isNotBlank(wrapperInfo.getText()) ? wrapperInfo.getText() : StringUtils.EMPTY);
    ImageResource imageResource = wrapperInfo.getWrapperImageResource();
    String altImage = wrapperInfo.getImageAltText();
    if (imageResource != null) {
     Image image = new Image(imageResource.getPath(), altImage, "", currentPage, slingRequest);
     giftWrapperMap.put(UnileverConstants.IMAGE, image.convertToMap());
    } else {
     Image fallbackImage = new Image("", "", "", currentPage, slingRequest);
     giftWrapperMap.put(UnileverConstants.IMAGE, fallbackImage.convertToMap());
    }
    
    giftWrappersList.add(giftWrapperMap);
   }
  }
  return giftWrappersList.toArray();
 }
 
 /**
  * <p>
  * This method returns the delivery data map containing encrypted delivery price, delivery condition copy, and delivery tooltip text which is fetched
  * from the siteConfig
  * </p>
  * .
  * 
  * @param currentPage
  *         the current page
  * @return {@link Map}
  */
 public static Map<String, Object> getDeliveryDetailMap(Page currentPage) {
  ConfigurationService configurationService = currentPage.adaptTo(ConfigurationService.class);
  String encriptionKey = "";
  
  try {
   encriptionKey = configurationService.getConfigValue(currentPage, ProductConstants.EGIFTING_CAT, ProductConstants.ENCRYPTION_KEY);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find encriptionKey key in configuration ", e);
  }
  LOGGER.debug("encriptionKey used for encryption of Egifting prices: " + encriptionKey);
  BrandMarketBean brandMarket = new BrandMarketBean(currentPage);
  String delievryPriceEncriptionSalt = brandMarket.getBrand() + "_" + brandMarket.getMarket();
  
  Cryptor cryptor = CryptoFactory.getInstance(encriptionKey, delievryPriceEncriptionSalt);
  
  Map<String, Object> deliveryDetailMap = new HashMap<String, Object>();
  Map<String, Object> deliveryDetail = new HashMap<String, Object>();
  String deliveryPrice = "0.0";
  String deliveryConditionCopy = StringUtils.EMPTY;
  String deliveryTooltip = StringUtils.EMPTY;
  try {
   deliveryPrice = configurationService.getConfigValue(currentPage, ProductConstants.EGIFTING_CAT, ProductConstants.DELIVERY_PRICE);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find deliverPrice key in configuration ", e);
  }
  try {
   deliveryConditionCopy = configurationService.getConfigValue(currentPage, ProductConstants.EGIFTING_CAT, ProductConstants.DELIVERY_CONDITIONS_COPY);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find deliverConditionCopy key in configuration ", e);
  }
  try {
   deliveryTooltip = configurationService.getConfigValue(currentPage, ProductConstants.EGIFTING_CAT, ProductConstants.DELIVERY_TOOL_TIP);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not find deliveryTooltip key in configuration ", e);
  }
  String encryptedDeliveryPrice = cryptor.crypt(UnileverConstants.ENCRYPT_MODE, deliveryPrice);
  deliveryDetailMap.put(ProductConstants.ENCRYPTED_DELIVERY_PRICE, encryptedDeliveryPrice);
  deliveryDetailMap.put(ProductConstants.DELIVERY_PRICE, deliveryPrice);
  deliveryDetailMap.put(ProductConstants.DELIVERY_CONDITIONS_COPY, deliveryConditionCopy);
  deliveryDetailMap.put(ProductConstants.DELIVERY_TOOL_TIP, deliveryTooltip);
  deliveryDetail.put(ProductConstants.DELIVERY_DETAIL, deliveryDetailMap);
  return deliveryDetail;
  
 }
 
 /**
  * Gets the product teaser data.
  * 
  * @param productPage
  *         the product page
  * @param currentPage
  *         the current page
  * @param httpServletRequest
  *         the httpServletRequest
  * @return the product teaser data
  */
 public static Map<String, Object> getProductTeaserData(Page productPage, Page currentPage, SlingHttpServletRequest httpServletRequest) {
  Map<String, Object> dataMap = new HashMap<String, Object>();
  String url = ComponentUtil.getFullURL(productPage.getContentResource().getResourceResolver(), productPage.getPath(), httpServletRequest);
  if (productPage != null) {
   UNIProduct uniProduct = ProductHelper.getUniProduct(productPage);
   if (uniProduct != null) {
    dataMap = getProductTeaserData(uniProduct, currentPage, httpServletRequest);
    dataMap.put("url", url);
    dataMap.put(PRODUCT_SELECT_CTA_KEY, BaseViewHelper.getI18n(httpServletRequest, currentPage).get(PRODUCT_SELECT_CTA_LABEL));
   }
  }
  return dataMap;
 }
 
 /**
  * Gets the product teaser data
  * 
  * @param uniProduct
  *         the uni product
  * @param currentPage
  *         the current page
  * @param httpServletRequest
  *         the httpServletRequest
  * @return the product teaser data map
  */
 public static Map<String, Object> getProductTeaserData(UNIProduct uniProduct, Page currentPage, SlingHttpServletRequest httpServletRequest) {
  return getProductTeaserData(uniProduct, currentPage, httpServletRequest, true);
 }
 
 /**
  * Gets the product teaser data
  * 
  * @param uniProduct
  *         the uni product
  * @param currentPage
  *         the current page
  * @param httpServletRequest
  *         the httpServletRequest
  * @param containerTagFlag
  *         the containerTagFlag
  * @return the product teaser data
  */
 public static Map<String, Object> getProductTeaserData(UNIProduct uniProduct, Page currentPage, SlingHttpServletRequest httpServletRequest,
   boolean containerTagFlag) {
  
  Map<String, Object> productProperties = new LinkedHashMap<String, Object>();
  if (uniProduct != null) {
   ConfigurationService configurationService = currentPage.adaptTo(ConfigurationService.class);
   productProperties.putAll(getModalProductDetails(uniProduct, currentPage, httpServletRequest));
   List<Map<String, Object>> variantList = new ArrayList<Map<String, Object>>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
   String productSkuId = uniProduct.getSKU();
   Map<String, Object> variants = ProductHelper.getProductDataMap(uniProduct, currentPage, configurationService, httpServletRequest,
     BaseViewHelper.getI18n(httpServletRequest, currentPage));
   
   variantList.add(variants);
   try {
    Iterator<Product> iterator = uniProduct.getVariants();
    
    setVarientList(currentPage, variantList, productSkuId, iterator, httpServletRequest, configurationService);
    
   } catch (CommerceException ce) {
    LOGGER.error("CommerceException while getting the product variants:", ce);
   }
   productProperties.put(ProductConstants.PRODUCT_DETAIL_MAP, variantList.toArray());
   if (containerTagFlag) {
    Map<String, Object> containerTagMap = new HashMap<String, Object>();
    containerTagMap.put(ContainerTagConstants.SHOP_NOW, ProductHelper.getShopNowMap(currentPage, uniProduct));
    productProperties.put(UnileverConstants.CONTAINER_TAG, containerTagMap);
   } else {
    productProperties.remove("image");
   }
  }
  
  return productProperties;
 }
 
 /**
  * Sets the varient list.
  * 
  * @param currentPage
  *         the current page
  * @param variantList
  *         the variant list
  * @param productSkuId
  *         the product sku id
  * @param iterator
  *         the iterator
  * @param configurationService
  */
 private static void setVarientList(Page currentPage, List<Map<String, Object>> variantList, String productSkuId, Iterator<Product> iterator,
   SlingHttpServletRequest httpServletRequest, ConfigurationService configurationService) {
  Map<String, Object> variants;
  while (iterator.hasNext()) {
   
   UNIProduct productVariant = (UNIProduct) iterator.next();
   String productVariantSkuId = productVariant.getSKU();
   
   if (StringUtils.isNotBlank(productSkuId) && StringUtils.isNotBlank(productVariantSkuId) && !productSkuId.equals(productVariantSkuId)) {
    variants = ProductHelper.getProductDataMap(productVariant, currentPage, configurationService, httpServletRequest,
      BaseViewHelper.getI18n(httpServletRequest, currentPage));
    
    variantList.add(variants);
   }
  }
 }
 
 /**
  * Gets the modal product details.
  * 
  * @param currentProduct
  *         the current product
  * @param currentPage
  *         the current page
  * @param httpServletRequest
  *         the httpServletRequest
  * @return the modal product details
  */
 public static Map<String, Object> getModalProductDetails(UNIProduct currentProduct, Page currentPage, SlingHttpServletRequest httpServletRequest) {
  Map<String, Object> modalProductProperties = new HashMap<String, Object>();
  modalProductProperties.put(ProductConstants.TITLE, currentProduct.getName());
  modalProductProperties.put(ProductConstants.ILS_PRODUCT_NAME, currentProduct.getIlsProductName());
  modalProductProperties.put(ProductConstants.CATEGORY, currentProduct.getCategory());
  modalProductProperties.put(ProductConstants.DESCRIPTION, currentProduct.getShortPageDescription());
  modalProductProperties.put(ProductConstants.PRODUCT_ID, currentProduct.getSKU());
  
  ImageResource imageResource = CollectionUtils.isNotEmpty(currentProduct.getImages()) ? currentProduct.getImages().get(0) : null;
  
  modalProductProperties.put("image", new Image(imageResource, currentPage, httpServletRequest).convertToMap());
  return modalProductProperties;
 }
 
 /**
  * Gets the product id.
  * 
  * @param selectors
  *         the selectors
  * @return the product id
  */
 public static String getProductId(String[] selectors) {
  String productId = null;
  if (selectors != null) {
   if (selectors.length >= 1 && !"data".equals(selectors[0])) {
    productId = selectors[0];
   } else if (selectors.length >= TWO) {
    productId = selectors[1];
   }
  }
  
  return productId;
 }
}
