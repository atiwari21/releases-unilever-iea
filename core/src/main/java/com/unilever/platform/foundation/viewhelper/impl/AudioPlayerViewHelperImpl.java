/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.http.client.utils.URIBuilder;
import org.apache.sling.api.SlingConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.sapient.platform.iea.aem.utils.constants.CommonConstants;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * The Class AudioPlayerViewHelperImpl.
 */
@Component(description = "AudioPlayerViewHelperImpl", immediate = true, metatype = true, label = "AudioPlayerViewHelperImpl")
@Service(value = { AudioPlayerViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.AUDIOPLAYER, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class AudioPlayerViewHelperImpl extends BaseViewHelper {
 
 /** The Constant TRUE. */
 private static final String TRUE = "true";
 
 /** The Constant VISUAL. */
 private static final String VISUAL = "visual";
 
 /** The Constant FALSE. */
 private static final String FALSE = "false";
 
 /** The Constant SOUND_CLOUD. */
 private static final String SOUND_CLOUD = "soundCloud";
 
 /** The Constant TRACKS. */
 private static final String TRACKS = "tracks";
 
 /**
  * logger object.
  */
 private static final Logger LOGGER = LoggerFactory.getLogger(BreadcrumbViewHelperImpl.class);
 
 /** The Constant GLOBAL_CONFIG_CAT. */
 private static final String GLOBAL_CONFIG_CAT = "audioPlayerSoundCloudAttributes";
 
 /** The Constant SOUND_CLOUD_URL. */
 private static final String SOUND_CLOUD_URL = "https://w.soundcloud.com/player/?url=";
 
 /** The Constant SOUND_CLOUD_API_PATTERN. */
 private static final String SOUND_CLOUD_API_PATTERN = "http://api.soundcloud.com/%s/%s";
 
 /** The Constant AUTO_PLAY. */
 private static final String AUTO_PLAY = "autoPlay";
 
 /** The Constant LIKING. */
 private static final String LIKING = "liking";
 
 /** The Constant SHARING. */
 private static final String SHARING = "sharing";
 
 /** The Constant DOWNLOAD. */
 private static final String DOWNLOAD = "download";
 
 /** The Constant SHOW_USER. */
 private static final String SHOW_USER = "showUser";
 
 /** The Constant SHOW_ARTWORK. */
 private static final String SHOW_ARTWORK = "showArtwork";
 
 /** The Constant SHOW_COMMENTS. */
 private static final String SHOW_COMMENTS = "showComments";
 
 /** The Constant SHOW_PLAY_COUNT. */
 private static final String SHOW_PLAY_COUNT = "showPlaycount";
 
 /** The Constant BUYING. */
 private static final String BUYING = "buying";
 
 /** The Constant HIDE_RELATED. */
 private static final String HIDE_RELATED = "hideRelated";
 
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  Page currentPage = getCurrentPage(resources);
  Map<String, Object> finalMap = new HashMap<String, Object>();
  Map<String, Object> properties = getProperties(content);
  String jsonNameSpace = getJsonNameSpace(content);
  Map<String, Object> audioMap = new HashMap<String, Object>();
  String[] attributes = { "headingText", "shortSubheadingText", "longSubheadingText", "audioSource", AUTO_PLAY };
  ComponentUtil.putPropertiesInMap(audioMap, properties, attributes);
  Map<String, Object> audioSourceMap = new HashMap<String, Object>();
  String audioSource = MapUtils.getString(audioMap, "audioSource", StringUtils.EMPTY);
  
  if (StringUtils.isNotBlank(audioSource) && StringUtils.equalsIgnoreCase(audioSource, SOUND_CLOUD)) {
   Map<String, Object> soundCloudAttributes = new HashMap<String, Object>();
   Map<String, String> soundCloudAttrConfigMap = ComponentUtil.getConfigMap(currentPage, GLOBAL_CONFIG_CAT);
   
   Map<String, Object> soundCloudAttrOverriddenConfigMap = new HashMap<String, Object>();
   boolean overrideGlobalConfig = MapUtils.getBooleanValue(properties, "overrideGlobalConfig");
   
   if (overrideGlobalConfig) {
    Map<String, Object> soundCloudAttrMapFromDialog = getSoundCloudAttrFromDialog(properties);
    soundCloudAttrOverriddenConfigMap = GlobalConfigurationUtility.getOverriddenConfigMap(soundCloudAttrConfigMap, soundCloudAttrMapFromDialog);
    soundCloudAttributes = getSoundCloudAtrributesMap(soundCloudAttrOverriddenConfigMap);
   } else {
    soundCloudAttributes = getSoundCloudAtrributesMap((Map) soundCloudAttrConfigMap);
   }
   
   String embeddedPlayerType = MapUtils.getString(properties, "embeddedPlayerType", StringUtils.EMPTY);
   
   if (VISUAL.equals(embeddedPlayerType)) {
    soundCloudAttributes.put(VISUAL, TRUE);
    soundCloudAttributes.remove("color");
   }
   
   audioSourceMap.put("soundCloudAttributes", soundCloudAttributes);
   createUrl(properties, soundCloudAttributes, audioSourceMap, resources);
   audioMap.put(SOUND_CLOUD, audioSourceMap);
  }
  
  finalMap.put(jsonNameSpace, audioMap);
  return finalMap;
 }
 
 /**
  * This method creates the map of SoundCloud attributes fetched from dialog.
  * 
  * @param properties
  *         the properties map
  * @return soundCloudAttrMapFromDialog
  */
 private Map<String, Object> getSoundCloudAttrFromDialog(Map<String, Object> properties) {
  Map<String, Object> soundCloudAttrMapFromDialog = new HashMap<String, Object>();
  soundCloudAttrMapFromDialog.put(AUTO_PLAY, MapUtils.getString(properties, AUTO_PLAY, FALSE));
  soundCloudAttrMapFromDialog.put(LIKING, MapUtils.getString(properties, LIKING, FALSE));
  soundCloudAttrMapFromDialog.put(SHARING, MapUtils.getString(properties, SHARING, FALSE));
  soundCloudAttrMapFromDialog.put(DOWNLOAD, MapUtils.getString(properties, DOWNLOAD, FALSE));
  soundCloudAttrMapFromDialog.put(SHOW_USER, MapUtils.getString(properties, SHOW_USER, FALSE));
  soundCloudAttrMapFromDialog.put(SHOW_ARTWORK, MapUtils.getString(properties, SHOW_ARTWORK, FALSE));
  soundCloudAttrMapFromDialog.put(SHOW_COMMENTS, MapUtils.getString(properties, SHOW_COMMENTS, FALSE));
  soundCloudAttrMapFromDialog.put(SHOW_PLAY_COUNT, MapUtils.getString(properties, SHOW_PLAY_COUNT, FALSE));
  soundCloudAttrMapFromDialog.put(BUYING, MapUtils.getString(properties, BUYING, FALSE));
  soundCloudAttrMapFromDialog.put(HIDE_RELATED, MapUtils.getString(properties, HIDE_RELATED, FALSE));
  return soundCloudAttrMapFromDialog;
 }
 
 /**
  * This method gets sound cloud attribute map from config.
  * 
  * @param soundCloudAttrConfigMap
  *         sound cloud attributes map (either directly from config or overridden)
  * @return soundCloudAttributes
  */
 private Map<String, Object> getSoundCloudAtrributesMap(Map<String, Object> soundCloudAttrConfigMap) {
  Map<String, Object> soundCloudAttributes = new HashMap<String, Object>();
  for (Map.Entry<String, Object> entry : soundCloudAttrConfigMap.entrySet()) {
   String key = getSoundCloudName(entry.getKey());
   Object value = entry.getValue();
   soundCloudAttributes.put(key, value);
  }
  return soundCloudAttributes;
 }
 
 /**
  * This method replaces name in config with equivalent names compatible with sound cloud.
  * 
  * @param key
  *         the key
  * @return keySoundCloudName
  */
 private String getSoundCloudName(String key) {
  String keySoundCloudName = key;
  switch (keySoundCloudName) {
   case AUTO_PLAY:
    keySoundCloudName = "auto_play";
    break;
   case "showArtwork":
    keySoundCloudName = "show_artwork";
    break;
   case "showComments":
    keySoundCloudName = "show_comments";
    break;
   case "showPlaycount":
    keySoundCloudName = "show_playcount";
    break;
   case "showUser":
    keySoundCloudName = "show_user";
    break;
   case "hideRelated":
    keySoundCloudName = "hide_related";
    break;
   case "colour":
    keySoundCloudName = "color";
    break;
   default:
    break;
  }
  return keySoundCloudName;
 }
 
 /**
  * Creates the URL for sound cloud integration.
  *
  * @param properties
  *         the properties
  * @param attributes
  *         the attributes
  * @param audioSourceMap
  *         the audio source map
  * @param resources
  *         the resources
  * @return the string
  */
 public static String createUrl(Map<String, Object> properties, Map<String, Object> attributes, Map<String, Object> audioSourceMap,
   Map<String, Object> resources) {
  // the final widget type map
  final Map<String, String> widgetTypeMap = new HashMap<String, String>();
  Page currentPage = getCurrentPage(resources);
  Map<String, String> soundCloudGlobalConfig = GlobalConfigurationUtility.getMapFromConfiguration(currentPage.adaptTo(ConfigurationService.class),
    currentPage, SOUND_CLOUD);
  audioSourceMap.put("soundCloudWidgetAPIPath", MapUtils.getString(soundCloudGlobalConfig, "widgetJSPath", StringUtils.EMPTY));
  audioSourceMap.put("soundCloudAPIPath", MapUtils.getString(soundCloudGlobalConfig, "apiPath", StringUtils.EMPTY));
  String url = SOUND_CLOUD_URL + SOUND_CLOUD_API_PATTERN;
  widgetTypeMap.put("PlayList Widget", "playlists");
  widgetTypeMap.put("Sound Widget", TRACKS);
  widgetTypeMap.put("User Widget", "users");
  String completeUrl = StringUtils.EMPTY;
  String widgetType = MapUtils.getString(properties, "widgetType", StringUtils.EMPTY);
  String widgetTypeFromMap = MapUtils.getString(widgetTypeMap, widgetType);
  String id = StringUtils.EMPTY;
  if (StringUtils.equalsIgnoreCase(widgetTypeFromMap, TRACKS)) {
   id = MapUtils.getString(properties, "trackId");
  }
  if (StringUtils.equalsIgnoreCase(widgetTypeFromMap, "playlists")) {
   id = MapUtils.getString(properties, "playlistId");
   putTrackNumberToAttributes(properties, attributes);
  }
  if (StringUtils.equals(widgetTypeFromMap, "users")) {
   id = MapUtils.getString(properties, "userId");
   putTrackNumberToAttributes(properties, attributes);
  }
  
  url = String.format(url, widgetTypeFromMap, id);
  
  try {
   URIBuilder formedUrl = new URIBuilder(url);
   for (String key : attributes.keySet()) {
    if (attributes.get(key) instanceof String) {
     formedUrl.addParameter(key, (String) attributes.get(key));
    }
   }
   completeUrl = formedUrl.build().toString();
   
  } catch (URISyntaxException e) {
   LOGGER.error("error while creating url" + e.getMessage() + " and resaon is " + e.getReason(), e);
  }
  audioSourceMap.put("soundCloudCompleteUrl", completeUrl);
  audioSourceMap.put("soundCloudWidgetType", widgetType);
  audioSourceMap.put("soundCloudWidgetTypeParam", widgetTypeFromMap);
  audioSourceMap.put("soundCloudId", id);
  return completeUrl;
  
 }
 
 /**
  * This method put track number to sound cloud attributes map.
  * 
  * @param properties
  *         the properties map
  * @param attributes
  *         the sound cloud attributes map
  */
 private static void putTrackNumberToAttributes(Map<String, Object> properties, Map<String, Object> attributes) {
  String trackNumber = StringUtils.EMPTY;
  trackNumber = MapUtils.getString(properties, "startingTrack", StringUtils.EMPTY);
  if (StringUtils.isNotBlank(trackNumber)) {
   int trackNo = Integer.parseInt(trackNumber);
   if (trackNo >= 1) {
    attributes.put("start_track", Integer.toString(trackNo - 1));
   }
  }
 }
 
}
