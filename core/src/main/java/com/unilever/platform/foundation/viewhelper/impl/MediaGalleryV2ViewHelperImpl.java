/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.VideoHelper;
import com.unilever.platform.foundation.template.dto.Image;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * This class prepares map for mediaGalleryV2 Component.
 * 
 */
@Component(description = "MediaGalleryV2", immediate = true, metatype = true, label = "MediaGalleryV2ViewHelperImpl")
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.MEDIA_GALLERY_V2, propertyPrivate = true), })
public class MediaGalleryV2ViewHelperImpl extends BaseViewHelper {
 
 /** The Constant IMAGE_ALT_TEXT. */
 private static final String IMAGE_ALT_TEXT = "alt";
 
 /** The Constant VIDEO_CONTROLS. */
 private static final String VIDEO_CONTROLS = "videoControls";
 
 /** The Constant LIMIT_RESULTS_TO_LABEL. */
 private static final String LIMIT_RESULTS_TO_LABEL = "limitResultsTo";
 
 /** The Constant VIDEO_SOURCE_LABEL. */
 private static final String VIDEO_SOURCE_LABEL = "videoSource";
 
 /** The Constant VIDEO_ID_LABEL. */
 private static final String VIDEO_ID_LABEL = "videoId";
 
 /** The Constant VIDEO_CAPTION_LABEL. */
 private static final String VIDEO_CAPTION_LABEL = "caption";
 
 /** The Constant SHORT_SUBHEADING_TEXT_LABEL. */
 private static final String SHORT_SUBHEADING_TEXT_LABEL = "shortSubheadingText";
 
 /** The Constant IMAGE_CAPTION_LABEL. */
 private static final String IMAGE_CAPTION_LABEL = "caption";
 
 /** The Constant IMAGE_LABEL. */
 private static final String IMAGE_LABEL = "image";
 
 /** The Constant VIDEOS. */
 private static final String VIDEOS = "videos";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(MediaGalleryV2ViewHelperImpl.class);
 
 /** The Constant GALLERY_TYPE. */
 private static final String GALLERY_TYPE = "galleryType";
 
 /** The Constant HEADING_TEXT. */
 private static final String HEADING_TEXT = "headingText";
 
 /** The Constant SHORT_SUBHEADING_TEXT. */
 private static final String SHORT_SUBHEADING_TEXT = "shortSubheadingText";
 
 /** The Constant LONG_SUBHEADING_TEXT. */
 private static final String LONG_SUBHEADING_TEXT = "longSubheadingText";
 
 /** The Constant PRIMARY_CTA_LABEL. */
 private static final String PRIMARY_CTA_LABEL = "primaryCtaLabel";
 
 /** The Constant PRIMARY_CTA_LABEL. */
 private static final String PRIMARY_CTA_URL = "primaryCtaUrl";
 
 /** The Constant PRIMARY_OPEN_IN_NEW_WINDOW. */
 private static final String PRIMARY_OPEN_IN_NEW_WINDOW = "primaryOpenInNewWindow";
 
 /** The Constant SECONDARY_CTA_LABEL. */
 private static final String SECONDARY_CTA_LABEL = "secondaryCtaLabel";
 
 /** The Constant SECONDARY_CTA_URL. */
 private static final String SECONDARY_CTA_URL = "secondaryCtaUrl";
 
 /** The Constant SECONDARY_OPEN_IN_NEW_WINDOW. */
 private static final String SECONDARY_OPEN_IN_NEW_WINDOW = "secondaryOpenInNewWindow";
 
 /** The Constant IMAGE_URL. */
 private static final String IMAGE_URL = "imageUrl";
 
 /** The Constant IMAGE_CAPTION. */
 private static final String IMAGE_CAPTION = "imageCaption";
 
 /** The Constant VIDEO_SOURCE. */
 private static final String VIDEO_SOURCE = "videoSource";
 
 /** The Constant CONFIGURE_USING. */
 private static final String CONFIGURE_USING = "configureUsing";
 
 /** The Constant PLAYLIST_ID. */
 private static final String PLAYLIST_ID = "playlistId";
 
 /** The Constant ZERO. */
 private static final Integer ZERO = 0;
 
 /** The Constant LIMIT_RESULTS_TO. */
 private static final String LIMIT_RESULTS_TO = "limitResultsTo";
 
 /** The Constant CHANNEL_ID. */
 private static final String CHANNEL_ID = "channelId";
 
 /** The Constant CHANNEL_ID. */
 private static final String VIDEO_ID = "videoId";
 
 /** The Constant VIDEO_CAPTION. */
 private static final String VIDEO_CAPTION = "videoCaption";
 
 /** The Constant PREVIEW_IMAGE. */
 private static final String PREVIEW_IMAGE = "previewImage";
 
 /** The Constant DISPLAY_IN_OVERLAY. */
 private static final String DISPLAY_IN_OVERLAY = "displayInOverlay";
 
 /** The Constant AUTO_PLAY. */
 private static final String AUTO_PLAY = "autoplay";
 
 /** The Constant HIDE_VIDEO_CONTROLS. */
 private static final String HIDE_VIDEO_CONTROLS = "hideVideoControls";
 
 /** The Constant MUTE_VIDEO_AUDIO. */
 private static final String MUTE_VIDEO_AUDIO = "muteVideoAudio";
 
 /** The Constant LOOP_VIDEO_PLAYBACK. */
 private static final String LOOP_VIDEO_PLAYBACK = "loopVideoPlayback";
 
 /** The Constant RELATED_VIDEO_AT_END. */
 private static final String RELATED_VIDEO_AT_END = "relatedVideoAtEnd";
 
 /** The Constant TYPE_MIX_TAB. */
 private static final String TYPE_MIX_TAB = "typeMixTab";
 
 /** The Constant MEDIA_GALLERY. */
 private static final String MEDIA_GALLERY_V2 = "mediaGalleryV2";
 
 /** The Constant VIDEO_PREFIX_URLS. */
 private static final String VIDEO_PREFIX_URLS = "videoPrefixURLs";
 
 /** The Constant SHORT_SUBHEADING_TEXT_IMAGE. */
 private static final String SHORT_SUBHEADING_TEXT_IMAGE = "shortSubheadingTextImage";
 
 /** The Constant SHORT_SUBHEADING_TEXT_MIXED. */
 private static final String SHORT_SUBHEADING_TEXT_MIXED = "shortSubheadingMixed";
 
 /** The Constant SHORT_SUBHEADING_TEXT_VIDEO. */
 private static final String SHORT_SUBHEADING_TEXT_VIDEO = "shortSubheadingTextVideo";
 
 /** The Constant VIDEO_SOURCE_MIXED_TAB. */
 private static final String VIDEO_SOURCE_MIXED_TAB = "videoSourceMixedTab";
 
 /** The Constant VIDEO_CAPTION_MIX_TAB. */
 private static final String VIDEO_CAPTION_MIX_TAB = "videoCaptionMixTab";
 
 /** The Constant VIDEO_ID_MIX_TAB. */
 private static final String VIDEO_ID_MIX_TAB = "videoIdMixTab";
 
 /** The Constant PREVIEW_IMAGE_MIX_TAB. */
 private static final String PREVIEW_IMAGE_MIX_TAB = "previewImageMixTab";
 
 /** The Constant IMAGE_CAPTION_MIX_TAB. */
 private static final String IMAGE_CAPTION_MIX_TAB = "imageCaptionMixTab";
 
 /** The Constant IMAGE_CAPTION_MIX_TAB. */
 private static final String SELECT_IMAGE_MIX_TAB = "selectImageMixTab";
 
 /** The Constant MEDIA. */
 private static final String MEDIA = "media";
 
 /** The Constant MEDIA_LIST. */
 private static final String MEDIA_LIST = "mediaList";
 
 /** The Constant MEDIA_TYPE. */
 private static final String MEDIA_TYPE = "mediaType";
 
 /** The Constant IMAGE. */
 private static final String IMAGE = "image";
 
 /** The Constant VIDEO. */
 private static final String VIDEO = "video";
 
 /** The Constant API_KEY. */
 private static final String API_KEY = "apiKey";
 
 /** The Constant COUNT. */
 private static final String COUNT = "count";
 
 /** The Constant NINE. */
 private static final int NINE = 9;
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /**
  * This is overriden method from base view helper which prepares data map by calling all required methods.
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("Executing MediaGalleryViewHelperImpl.java. Inside processData method");
  
  Map<String, Object> mediaGalleryMap = new LinkedHashMap<String, Object>();
  Map<String, Object> properties = getProperties(content);
  Map<String, String> configMap = new LinkedHashMap<String, String>();
  
  String galleryType = MapUtils.getString(properties, GALLERY_TYPE, StringUtils.EMPTY);
  Resource resource = getCurrentResource(resources);
  Page currentPage = getCurrentPage(resources);
  SlingHttpServletRequest slingRequest = getSlingRequest(resources);
  ResourceResolver resourceResolver = getResourceResolver(resources);
  I18n i18n = getI18n(resources);
  
  try {
   configMap = configurationService.getCategoryConfiguration(currentPage, MEDIA_GALLERY_V2);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.debug("Error in getting configuration", e);
  }
  
  mediaGalleryMap.put("generalConfig", getGeneralConfigMap(resourceResolver, properties, slingRequest));
  
  if (("images").equals(galleryType)) {
   mediaGalleryMap.put(MEDIA, getImagesMap(resource, currentPage, slingRequest));
   
  } else if (VIDEOS.equals(galleryType)) {
   mediaGalleryMap.put(MEDIA, getVideosMap(resource, properties, currentPage, slingRequest));
   
  } else {
   mediaGalleryMap.put(MEDIA, getMixedMap(properties, resource, currentPage, slingRequest));
  }
  
  mediaGalleryMap.put("videoPrefixUrls", getVideoPrefixUrls(configMap));
  mediaGalleryMap.put("apiKey", MapUtils.getString(configMap, API_KEY, StringUtils.EMPTY));
  mediaGalleryMap.put("count", MapUtils.getIntValue(configMap, COUNT, NINE));
  mediaGalleryMap.put("showMoreCtaLabel", i18n.get("mediaGalleryV2.showMoreCtaLabel"));
  mediaGalleryMap.put("closeLabel", i18n.get("mediaGalleryV2.closeLabel"));
  
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  data.put(getJsonNameSpace(content), mediaGalleryMap);
  
  return data;
 }
 
 /**
  * This method generates map of video prefix urls by reading it from config.
  * 
  * @param configMap
  *         the configuration map
  * @return videoPrefixUrlsMap the map containing video prefix urls
  */
 private Map<String, Object> getVideoPrefixUrls(Map<String, String> configMap) {
  Map<String, Object> videoPrefixUrlsMap = new LinkedHashMap<String, Object>();
  String videoPrefixUrls = MapUtils.getString(configMap, VIDEO_PREFIX_URLS, StringUtils.EMPTY);
  String[] videoPrefixUrlsArray = videoPrefixUrls.split(",");
  
  videoPrefixUrlsMap.put("url", videoPrefixUrlsArray);
  
  return videoPrefixUrlsMap;
 }
 
 /**
  * This method generate map of videos as entered by author.
  * 
  * @param resource
  * @param properties
  * @param currentPage
  * @param slingRequest
  * @return videosMap
  */
 private Map<String, Object> getVideosMap(Resource resource, Map<String, Object> properties, Page currentPage, SlingHttpServletRequest slingRequest) {
  Map<String, Object> videosMap = new LinkedHashMap<String, Object>();
  String configureUsing = MapUtils.getString(properties, CONFIGURE_USING, StringUtils.EMPTY);
  int limitResultsTo = MapUtils.getIntValue(properties, LIMIT_RESULTS_TO, ZERO);
  
  videosMap.put("configureUsing", configureUsing);
  videosMap.put(VIDEO_SOURCE_LABEL, MapUtils.getString(properties, VIDEO_SOURCE, StringUtils.EMPTY).toLowerCase());
  
  if (("fixedList").equals(configureUsing)) {
   videosMap.put(MEDIA_LIST, getVideosList(properties, resource, currentPage, slingRequest));
   
  } else if (("playlist").equals(configureUsing)) {
   
   videosMap.put("playlistId", MapUtils.getString(properties, PLAYLIST_ID, StringUtils.EMPTY));
   videosMap.put(LIMIT_RESULTS_TO_LABEL, limitResultsTo);
   videosMap.put(MEDIA_TYPE, VIDEO);
   
  } else {
   
   videosMap.put("channelId", MapUtils.getString(properties, CHANNEL_ID, StringUtils.EMPTY));
   videosMap.put(LIMIT_RESULTS_TO_LABEL, limitResultsTo);
   videosMap.put(MEDIA_TYPE, VIDEO);
  }
  
  videosMap.put(VIDEO_CONTROLS, VideoHelper.getVideoControls(properties, MapUtils.getString(properties, VIDEO_SOURCE, StringUtils.EMPTY)));
  
  return videosMap;
 }
 
 /**
  * This method generates video controls map.
  * 
  * @param properties
  * @return videoControlsMap
  */
 private Map<String, Object> getVideoControlsMap(Map<String, Object> properties) {
  Map<String, Object> videoControlsMap = new LinkedHashMap<String, Object>();
  
  videoControlsMap.put("displayInOverlay", MapUtils.getBoolean(properties, DISPLAY_IN_OVERLAY, false));
  videoControlsMap.put("autoplay", MapUtils.getBoolean(properties, AUTO_PLAY, false));
  videoControlsMap.put("hideVideoControls", MapUtils.getBoolean(properties, HIDE_VIDEO_CONTROLS, false));
  videoControlsMap.put("muteVideoAudio", MapUtils.getBoolean(properties, MUTE_VIDEO_AUDIO, false));
  videoControlsMap.put("loopVideoPlayback", MapUtils.getBoolean(properties, LOOP_VIDEO_PLAYBACK, false));
  videoControlsMap.put("relatedVideoAtEnd", MapUtils.getBoolean(properties, RELATED_VIDEO_AT_END, false));
  
  return videoControlsMap;
 }
 
 /**
  * This method generates map of mixed type input containing both images and videos.
  * 
  * @param properties
  *         
  * @param resource
  * @param properties
  * @param currentPage
  * @param slingRequest
  * @return
  */
 private Map<String, Object> getMixedMap(Map<String, Object> properties, Resource resource, Page currentPage, SlingHttpServletRequest slingRequest) {
  Map<String, Object> mixedMap = new LinkedHashMap<String, Object>();
  List<Map<String, String>> mixedList = ComponentUtil.getNestedMultiFieldProperties(resource, "mediaGalleryImageAndVideo");
  List<Map<String, Object>> imagesAndVideosList = new ArrayList<Map<String, Object>>();
  String type = StringUtils.EMPTY;
  
  if (mixedList != null) {
   for (int i = 0; i < mixedList.size(); i++) {
    Map<String, Object> map = new HashMap<String, Object>();
    
    type = MapUtils.getString(mixedList.get(i), TYPE_MIX_TAB, StringUtils.EMPTY);
    
    if (IMAGE_LABEL.equals(type)) {
     String imageUrl = MapUtils.getString(mixedList.get(i), SELECT_IMAGE_MIX_TAB, StringUtils.EMPTY);
     String altText = MapUtils.getString(mixedList.get(i), IMAGE_ALT_TEXT, StringUtils.EMPTY);
     Image image = new Image(imageUrl, StringUtils.EMPTY, StringUtils.EMPTY, currentPage, slingRequest);
     
     map.put(MEDIA_TYPE, IMAGE);
     map.put(IMAGE_LABEL, image);
     map.put(IMAGE_ALT_TEXT, altText);
     map.put(IMAGE_CAPTION_LABEL, MapUtils.getString(mixedList.get(i), IMAGE_CAPTION_MIX_TAB, StringUtils.EMPTY));
     map.put(SHORT_SUBHEADING_TEXT_LABEL, MapUtils.getString(mixedList.get(i), SHORT_SUBHEADING_TEXT_MIXED, StringUtils.EMPTY));
    } else {
     String previewImageUrl = MapUtils.getString(mixedList.get(i), PREVIEW_IMAGE_MIX_TAB, StringUtils.EMPTY);
     Image previewImage = new Image(previewImageUrl, StringUtils.EMPTY, StringUtils.EMPTY, currentPage, slingRequest);
     
     map.put(MEDIA_TYPE, VIDEO);
     map.put(IMAGE_LABEL, previewImage);
     map.put(VIDEO_SOURCE_LABEL, MapUtils.getString(mixedList.get(i), VIDEO_SOURCE_MIXED_TAB, StringUtils.EMPTY).toLowerCase());
     map.put(VIDEO_ID_LABEL, MapUtils.getString(mixedList.get(i), VIDEO_ID_MIX_TAB, StringUtils.EMPTY));
     map.put(VIDEO_CAPTION_LABEL, MapUtils.getString(mixedList.get(i), VIDEO_CAPTION_MIX_TAB, StringUtils.EMPTY));
     map.put(SHORT_SUBHEADING_TEXT_LABEL, MapUtils.getString(mixedList.get(i), SHORT_SUBHEADING_TEXT_MIXED, StringUtils.EMPTY));
    }
    
    if (map != null && map.size() > 0) {
     imagesAndVideosList.add(map);
    }
   }
   mixedMap.put(MEDIA_LIST, imagesAndVideosList.toArray());
   mixedMap.put(VIDEO_CONTROLS, getVideoControlsMap(properties));
  }
  
  return mixedMap;
  
 }
 
 /**
  * This method generates videos map.
  * 
  * @param properties
  * @param resource
  * @param currentPage
  * @param slingRequest
  * @return videosMap
  */
 private Object[] getVideosList(Map<String, Object> properties, Resource resource, Page currentPage, SlingHttpServletRequest slingRequest) {
  List<Map<String, String>> videoList = ComponentUtil.getNestedMultiFieldProperties(resource, "mediaGalleryVideo");
  List<Map<String, Object>> videosList = new ArrayList<Map<String, Object>>();
  if (videoList != null) {
   for (int i = 0; i < videoList.size(); i++) {
    Map<String, Object> map = new HashMap<String, Object>();
    
    String previewImageUrl = MapUtils.getString(videoList.get(i), PREVIEW_IMAGE, StringUtils.EMPTY);
    Image previewImage = new Image(previewImageUrl, StringUtils.EMPTY, StringUtils.EMPTY, currentPage, slingRequest);
    
    map.put(MEDIA_TYPE, VIDEO);
    map.put(VIDEO_SOURCE_LABEL, MapUtils.getString(properties, VIDEO_SOURCE, StringUtils.EMPTY).toLowerCase());
    map.put(IMAGE_LABEL, previewImage);
    map.put(VIDEO_ID_LABEL, MapUtils.getString(videoList.get(i), VIDEO_ID, StringUtils.EMPTY));
    map.put(VIDEO_CAPTION_LABEL, MapUtils.getString(videoList.get(i), VIDEO_CAPTION, StringUtils.EMPTY));
    map.put(SHORT_SUBHEADING_TEXT_LABEL, MapUtils.getString(videoList.get(i), SHORT_SUBHEADING_TEXT_VIDEO, StringUtils.EMPTY));
    
    if (map != null && map.size() > 0) {
     videosList.add(map);
    }
   }
  }
  
  return videosList.toArray();
 }
 
 /**
  * This method generates map of images
  * 
  * @param resource
  * @param properties
  * @param currentPage
  * @param slingRequest
  * @return imagesMap
  */
 private Map<String, Object> getImagesMap(Resource resource, Page currentPage, SlingHttpServletRequest slingRequest) {
  List<Map<String, String>> imageList = ComponentUtil.getNestedMultiFieldProperties(resource, "mediaGalleryImage");
  List<Map<String, Object>> imagesList = new ArrayList<Map<String, Object>>();
  Map<String, Object> imagesMap = new LinkedHashMap<String, Object>();
  
  if (imageList != null) {
   for (int i = 0; i < imageList.size(); i++) {
    Map<String, Object> map = new HashMap<String, Object>();
    String imageUrl = MapUtils.getString(imageList.get(i), IMAGE_URL, StringUtils.EMPTY);
    Image image = new Image(imageUrl, StringUtils.EMPTY, StringUtils.EMPTY, currentPage, slingRequest);
    String altText = MapUtils.getString(imageList.get(i), IMAGE_ALT_TEXT, StringUtils.EMPTY);
    map.put(MEDIA_TYPE, IMAGE);
    map.put(IMAGE_LABEL, image);
    map.put(IMAGE_ALT_TEXT, altText);
    map.put(IMAGE_CAPTION_LABEL, MapUtils.getString(imageList.get(i), IMAGE_CAPTION, StringUtils.EMPTY));
    map.put(SHORT_SUBHEADING_TEXT_LABEL, MapUtils.getString(imageList.get(i), SHORT_SUBHEADING_TEXT_IMAGE, StringUtils.EMPTY));
    
    if (map != null && map.size() > 0) {
     imagesList.add(map);
    }
   }
   imagesMap.put(MEDIA_LIST, imagesList.toArray());
  }
  
  return imagesMap;
 }
 
 /**
  * This method generates map containing general properties of component.
  * 
  * @param resourceResolver
  * @param properties
  * @return generalConfigMap
  */
 private Map<String, Object> getGeneralConfigMap(ResourceResolver resourceResolver, Map<String, Object> properties,
   SlingHttpServletRequest slingRequest) {
  Map<String, Object> generalConfigMap = new LinkedHashMap<String, Object>();
  
  generalConfigMap.put("galleryType", MapUtils.getString(properties, GALLERY_TYPE, StringUtils.EMPTY));
  generalConfigMap.put("headingText", MapUtils.getString(properties, HEADING_TEXT, StringUtils.EMPTY));
  generalConfigMap.put(SHORT_SUBHEADING_TEXT_LABEL, MapUtils.getString(properties, SHORT_SUBHEADING_TEXT, StringUtils.EMPTY));
  generalConfigMap.put("longSubheadingText", MapUtils.getString(properties, LONG_SUBHEADING_TEXT, StringUtils.EMPTY));
  generalConfigMap.put("primaryCtaLabel", MapUtils.getString(properties, PRIMARY_CTA_LABEL, StringUtils.EMPTY));
  generalConfigMap.put("primaryCtaUrl",
    ComponentUtil.getFullURL(resourceResolver, MapUtils.getString(properties, PRIMARY_CTA_URL, StringUtils.EMPTY), slingRequest));
  generalConfigMap.put("primaryOpenInNewWindow", MapUtils.getString(properties, PRIMARY_OPEN_IN_NEW_WINDOW, StringUtils.EMPTY));
  generalConfigMap.put("secondaryCtaLabel", MapUtils.getString(properties, SECONDARY_CTA_LABEL, StringUtils.EMPTY));
  generalConfigMap.put("secondaryCtaUrl",
    ComponentUtil.getFullURL(resourceResolver, MapUtils.getString(properties, SECONDARY_CTA_URL, StringUtils.EMPTY), slingRequest));
  generalConfigMap.put("secondaryOpenInNewWindow", MapUtils.getString(properties, SECONDARY_OPEN_IN_NEW_WINDOW, StringUtils.EMPTY));
  
  return generalConfigMap;
 }
 
}
