/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.components.helper;

import java.lang.reflect.Constructor;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Value;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.components.dto.SearchFilterOptionsDTO;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.Image;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.impl.RecipeListingViewHelperImpl;
import com.unilever.platform.foundation.viewhelper.impl.SearchInputV2ViewHelperImpl;

/**
 * @author asri54 The Class SearchHelper.
 */
public class SearchHelper {
 
 private static final String SUB_SEARCH_LOGO_IMAGE = "subSearchLogoImage";
 
 public static final String OR_KEYWORD = "OR";
 
 public static final String SUB_BRAND_NAME_KEY = "subBrandName";
 
 private static final String SUB_BRANDS = "subBrands";
 
 public static final String FQ_CONTENT_TYPE_FORMAT = "fq=ContentType:";
 
 public static final String OR = " OR ";
 
 private static final String DEFAULT_CONTENT_TYPES = "defaultContentTypes";
 
 public static final String STRING_CLASS_NAME = "String";
 
 /** The Constant LOG. */
 private static final Logger LOG = LoggerFactory.getLogger(SearchHelper.class);
 
 /** The Constant DISABLE_AUTO_SUGGEST. */
 public static final String DISABLE_AUTO_SUGGEST = "disableAutoSuggest";
 
 /** The Constant DISABLE_AUTO_COMPLETE_JSON_LABEL. */
 public static final String DISABLE_AUTO_COMPLETE_JSON_LABEL = "disableAutoComplete";
 
 /** The Constant AUTO_SUGGEST_RESULT_COUNT. */
 public static final String AUTO_SUGGEST_RESULT_COUNT = "autoSuggestResultCount";
 
 /** The Constant MIN_CHAR_TO_TRIGGER_SEARCH. */
 public static final String MIN_CHAR_TO_TRIGGER_SEARCH = "minCharToTriggerAutoSuggest";
 
 /** The Constant MIN_CHAR_TO_TRIGGER_SEARCH_JSON_LABEL. */
 public static final String MIN_CHAR_TO_TRIGGER_SEARCH_JSON_LABEL = "minCharToTriggerAutoSuggest";
 
 /** The Constant DISABLE_CONTENT_TYPE_FILTER. */
 public static final Object DISABLE_CONTENT_TYPE_FILTER = "disableContentTypeFilter";
 
 /** The Constant DISABLE_CONTENT_TYPE_FILTER_JSON_LABEL. */
 private static final int RESULTS_PER_PAGE_DEFAULT = 10;
 
 /** The content type format. */
 public static final String CONTENT_TYPE_FORMAT = "searchListing.%s.contentTab";
 
 /** The query param format. */
 public static final String QUERY_PARAM_FORMAT = "fq=ContentType:%s";
 
 /** The Constant SEARCH_LOGO_IMAGE. */
 public static final String SEARCH_LOGO_IMAGE = "searchLogoImage";
 
 /** The Constant PROPERTIES. */
 public static final String PROPERTIES = "properties";
 
 /** The Constant SEARCH_INPUT. */
 public static final String SEARCH_INPUT = "searchInput";
 
 /** The Constant THREE. */
 public static final int THREE = 3;
 
 /** The Constant CLASS. */
 public static final String CLASS = "class";
 
 /** The Constant DISABLE_AUTO_COMPLETE. */
 public static final String DISABLE_AUTO_COMPLETE = "disableAutoComplete";
 
 /** The Constant AUTO_SUGGEST_RESULT_COUNT_JSON_LABEL. */
 public static final String AUTO_SUGGEST_RESULT_COUNT_JSON_LABEL = "autoSuggestResultCount";
 
 /** The Constant DISABLE_CONTENT_TYPE_FILTER_JSON_LABEL. */
 public static final String DISABLE_CONTENT_TYPE_FILTER_JSON_LABEL = "disableContentTypeFilter";
 
 /** The Constant SEARCH_SUGGESTION_HANDLER_JSON_LABEL. */
 public static final String SEARCH_SUGGESTION_HANDLER_JSON_LABEL = "searchSuggestionHandler";
 
 /** The Constant SEARCH_SUGGESTION_HANDLER. */
 public static final String SEARCH_SUGGESTION_HANDLER = "searchSuggestionHandler";
 
 /** The Constant REQUEST_SERVLET_JSON_LABEL. */
 public static final String REQUEST_SERVLET_JSON_LABEL = "requestServlet";
 
 /** The Constant REQUEST_SERVLET. */
 public static final String REQUEST_SERVLET = "requestServlet";
 
 /** The Constant RMS_TAG_PATTERN. */
 public static final String RMS_TAG_PATTERN = "/recipes/rms/";
 
 /** The Constant FIVE. */
 public static final int FIVE = 5;
 
 /** The Constant TWO. */
 private static final int TWO = 2;
 
 /** The Constant FOUR. */
 private static final int FOUR = 4;
 
 /** The Constant DISABLE_AUTO_SUGGEST_JSON_LABEL. */
 public static final String DISABLE_AUTO_SUGGEST_JSON_LABEL = "disableAutoSuggest";
 
 /** The Constant CONTENT_TYPE. */
 public static final String CONTENT_TYPE = "contentType";
 
 /** The Constant CTALABEL. */
 public static final String CTALABEL = "ctaLabel";
 
 /** The Constant CTALABEL. */
 public static final String IMAGEALTTEXT = "imageAltText";
 
 /** The Constant SEARCH_HEADING_TEXT. */
 public static final String SEARCH_HEADING_TEXT = "searchHeadingText";
 
 /** The Constant SEARCH_INPUT_PLACEHOLDER_TEXT. */
 public static final String SEARCH_INPUT_PLACEHOLDER_TEXT = "searchInputPlaceholderText";
 
 public static final String TAG_ID = "tagID";
 
 public static final String TITLE = "title";
 
 public static final String HASH_MAP = "HashMap";
 
 public static final String STAGS = "stags";
 
 /**
  * {@link Constructor}
  */
 private SearchHelper() {
  
 }
 
 /**
  * Gets the search configurations.
  * 
  * @param content
  *         the content
  * @param resources
  *         the resources
  * @param category
  *         the category
  * @return the search configurations
  */
 public static Map<String, Object> getSearchConfigurations(Map<String, Object> content, Map<String, Object> resources) {
  Map<String, Object> finalMap = new LinkedHashMap<String, Object>();
  Map<String, Object> properties = BaseViewHelper.getProperties(content);
  Page currentPage = BaseViewHelper.getCurrentPage(resources);
  Map<String, String> configMap = ComponentUtil.getConfigMap(currentPage, SEARCH_INPUT);
  Boolean overrideGlobalConfig = MapUtils.getBoolean(properties, "overrideGlobalConfig", false);
  
  // If override configuration is set to true, get overriden config map or
  // else put values from dialog
  if (overrideGlobalConfig) {
   finalMap = GlobalConfigurationUtility.getOverriddenConfigMap(configMap, properties);
   boolean value = MapUtils.getBooleanValue(finalMap, DISABLE_AUTO_SUGGEST_JSON_LABEL);
   finalMap.remove(DISABLE_AUTO_SUGGEST_JSON_LABEL);
   finalMap.put(DISABLE_AUTO_SUGGEST_JSON_LABEL, value);
   value = MapUtils.getBooleanValue(finalMap, DISABLE_AUTO_COMPLETE_JSON_LABEL);
   finalMap.remove(DISABLE_AUTO_COMPLETE_JSON_LABEL);
   finalMap.put(DISABLE_AUTO_COMPLETE_JSON_LABEL, value);
   value = MapUtils.getBooleanValue(finalMap, DISABLE_CONTENT_TYPE_FILTER_JSON_LABEL);
   finalMap.remove(DISABLE_CONTENT_TYPE_FILTER_JSON_LABEL);
   finalMap.put(DISABLE_CONTENT_TYPE_FILTER_JSON_LABEL, value);
  } else {
   finalMap.put(MIN_CHAR_TO_TRIGGER_SEARCH_JSON_LABEL, MapUtils.getInteger(configMap, MIN_CHAR_TO_TRIGGER_SEARCH, THREE));
   finalMap.put(DISABLE_AUTO_SUGGEST_JSON_LABEL, MapUtils.getBoolean(configMap, DISABLE_AUTO_SUGGEST, false));
   finalMap.put(DISABLE_AUTO_COMPLETE_JSON_LABEL, MapUtils.getBoolean(configMap, DISABLE_AUTO_COMPLETE, false));
   finalMap.put(AUTO_SUGGEST_RESULT_COUNT_JSON_LABEL, MapUtils.getInteger(configMap, AUTO_SUGGEST_RESULT_COUNT, FIVE));
   finalMap.put(DISABLE_CONTENT_TYPE_FILTER_JSON_LABEL, MapUtils.getBoolean(configMap, DISABLE_CONTENT_TYPE_FILTER, false));
   finalMap.put(SEARCH_SUGGESTION_HANDLER_JSON_LABEL, MapUtils.getString(configMap, SEARCH_SUGGESTION_HANDLER_JSON_LABEL, ""));
   finalMap.put(REQUEST_SERVLET, MapUtils.getString(configMap, REQUEST_SERVLET, ""));
  }
  finalMap.put("perContentCount", MapUtils.getInteger(configMap, "perContentCount", THREE));
  finalMap.put("recipeSearchEndPoint", MapUtils.getString(configMap, "recipeSearchUrl"));
  return finalMap;
  
 }
 
 /**
  * Gets the content type.
  * 
  * @param configMap
  *         the config map
  * @param currentPage
  *         the current page
  * @param i18n
  *         the i 18 n
  * @param isExternal
  *         the is external
  * @return the content type
  */
 public static List<Map<String, Object>> getContentType(Map<String, String> configMap, Page currentPage, I18n i18n, boolean isExternal) {
  Set<String> contentTypeSet = configMap.keySet();
  Iterator<String> iterator = contentTypeSet.iterator();
  List<Map<String, Object>> contentTypeMapList = new ArrayList<Map<String, Object>>();
  
  // Iterate through each key and put it in contentTypeMap.
  while (iterator.hasNext()) {
   String key = iterator.next();
   Map<String, Object> propertyMap = new LinkedHashMap<String, Object>();
   Map<String, String> externalContentTypeMap = ComponentUtil.getConfigMap(currentPage, "externalContentTypes");
   boolean externalContent = false;
   if (externalContentTypeMap.containsKey(key)) {
    externalContent = true;
   }
   if (StringUtils.equalsIgnoreCase(key, "everything")) {
    Map<String, String> contentMap = new HashMap<String, String>();
    contentMap.put(CONTENT_TYPE, i18n.get("searchListing.everything.contentTab"));
    contentMap.put("contentName", key);
    contentMap.put("displayCount", "");
    contentMap.put("type", "");
    contentMap.put("queryParams", "");
    propertyMap.putAll(contentMap);
   } else {
    if (isExternal && externalContent) {
     propertyMap.putAll(getPropertyMap(configMap, currentPage, key, i18n, isExternal));
    } else if (isExternal && !externalContent) {
     propertyMap.putAll(getPropertyMap(configMap, currentPage, key, i18n, isExternal));
    } else if (!isExternal && !externalContent) {
     propertyMap.putAll(getPropertyMap(configMap, currentPage, key, i18n, isExternal));
    }
   }
   contentTypeMapList.add(propertyMap);
  }
  
  return contentTypeMapList;
 }
 
 /**
  * Gets the property map.
  * 
  * @param configMap
  *         the config map
  * @param currentPage
  *         the current page
  * @param key
  *         the key
  * @param i18n
  *         the i 18 n
  * @param isExternal
  *         the is external
  * @return the property map
  */
 public static Map<String, Object> getPropertyMap(Map<String, String> configMap, Page currentPage, String key, I18n i18n, boolean isExternal) {
  Map<String, Object> propertyMap = new LinkedHashMap<String, Object>();
  String externalUrlFormat = "%sEndPoint";
  Map<String, String> externalContentTypeMap = ComponentUtil.getConfigMap(currentPage, "externalContentTypes");
  Map<String, String> displayContentTypes = ComponentUtil.getConfigMap(currentPage, "displayContentTypes");
  propertyMap.put(CONTENT_TYPE, i18n.get(String.format(CONTENT_TYPE_FORMAT, StringUtils.deleteWhitespace(key))));
  propertyMap.put("contentName", key);
  propertyMap.put("displayCount", MapUtils.getInteger(displayContentTypes, key, THREE));
  if (externalContentTypeMap.containsKey(key)) {
   if (isExternal) {
    String type = externalContentTypeMap.getOrDefault(key, "external");
    propertyMap.put("type", type);
    Map<String, String> externalURLMap = ComponentUtil.getConfigMap(currentPage, SEARCH_INPUT);
    propertyMap.put("url", externalURLMap.getOrDefault(String.format(externalUrlFormat, key.toLowerCase()), StringUtils.EMPTY));
   }
  } else {
   String value = configMap.get(key);
   propertyMap.put("type", "internal");
   if (StringUtils.isNotEmpty(value)) {
    String[] contentTypeValues = value.split(",");
    propertyMap.put("queryParams", createContentTypeParams(contentTypeValues));
   }
  }
  
  return propertyMap;
 }
 
 /**
  * 
  * @param contentTypeValues
  * @return
  */
 public static String createContentTypeParams(String[] contentTypeValues){
  String queryParams = StringUtils.EMPTY;
  if (contentTypeValues.length > 1) {
   for (String contentTypeValue : contentTypeValues) {
    queryParams += contentTypeValue + OR;
   }
   queryParams = String.format(QUERY_PARAM_FORMAT, queryParams.substring(0, queryParams.length() - FOUR));
  } else if (!ArrayUtils.isEmpty(contentTypeValues)) {
   queryParams += FQ_CONTENT_TYPE_FORMAT + contentTypeValues[0];
  }
  return queryParams;
 }
 
 /**
  * Gets the filter options.
  * 
  * @param resources
  *         the resources
  * @param propertyName
  *         the property name
  * @param configMap
  *         the config map
  * @param resolver
  *         the resolver
  * @param i18n
  *         the i 18 n
  * @param externalConfigMap
  *         the external config map
  * @param isExternal
  *         the is external
  * @return the filter options
  */
 @SuppressWarnings("unchecked")
 public static List<SearchFilterOptionsDTO> getFilterOptions(Map<String, Object> resources, String propertyName, Map<String, String> configMap,
   ResourceResolver resolver, I18n i18n, Map<String, String> externalConfigMap, boolean isExternal) {
  
  List<SearchFilterOptionsDTO> searchFilter = new ArrayList<SearchFilterOptionsDTO>();
  List<Map<String, String>> filterOptions = ComponentUtil.getNestedMultiFieldProperties(BaseViewHelper.getCurrentResource(resources), propertyName);
  Page currentPage = BaseViewHelper.getCurrentPage(resources);
  for (int i = 0; i < filterOptions.size(); i++) {
   String contentType = filterOptions.get(i).get(CONTENT_TYPE);
   if (externalConfigMap.containsKey(contentType) && !isExternal) {
    continue;
   }
   Map<String, Object> propertyMap = getPropertyMap(configMap, currentPage, contentType, i18n, isExternal);
   if (propertyMap.containsKey(CONTENT_TYPE)) {
    propertyMap.remove(CONTENT_TYPE);
   }
   SearchFilterOptionsDTO dto = new SearchFilterOptionsDTO();
   Map<String, String> contentTypeMap = new HashMap<String, String>();
   contentTypeMap.put(i18n.get(String.format(CONTENT_TYPE_FORMAT, contentType)), contentType);
   
   dto.setContentType(contentTypeMap);
   try {
    List<Map<String, Object>> configOptions = new ArrayList<Map<String, Object>>();
    JSONArray array = new JSONArray(filterOptions.get(i).get("configureOptions"));
    for (int j = 0; j < array.length(); j++) {
     Map<String, Object> configOption = new HashMap<String, Object>();
     Map<String, String> object = ComponentUtil.toMap(array.getJSONObject(j));
     Object removed = object.containsKey("filterParentTagOne@Delete") ? object.remove("filterParentTagOne@Delete") : null;
     LOG.debug("key removed" + removed);
     configOption.putAll(object);
     String parentTag = object.getOrDefault("filterParentTagOne", StringUtils.EMPTY);
     List<Map<String, String>> filters = null;
     filters = externalConfigMap.containsKey(contentType) ? (List<Map<String, String>>) getTagChildren(new HashMap<String, String>().getClass(),
       resolver, currentPage, false, parentTag) : (List<Map<String, String>>) getTagChildren(new HashMap<String, String>().getClass(), resolver,
       currentPage, true, parentTag);
     filters = CollectionUtils.isEmpty(filters) ? new ArrayList<Map<String, String>>() : filters;
     configOption.put("filters", filters);
     configOption.put("clearFilterKey", i18n.get("search.clearFilter.label"));
     configOptions.add(configOption);
    }
    dto.setConfigureOptions(configOptions);
   } catch (JSONException e) {
    LOG.error("", e);
   }
   searchFilter.add(dto);
   
  }
  return searchFilter;
  
 }
 
 /**
  * Gets the pagination map.
  * 
  * @param properties
  *         the properties
  * @param i18n
  * @return the pagination map
  */
 public static Map<String, Object> getPaginationMap(Map<String, Object> properties, I18n i18n) {
  Map<String, Object> paginationInfo = new HashMap<String, Object>();
  paginationInfo.put("paginationType", (String) properties.getOrDefault("paginationType", StringUtils.EMPTY));
  paginationInfo.put("itemsPerPage", Integer.parseInt(properties.getOrDefault("itemsPerPage", RESULTS_PER_PAGE_DEFAULT).toString()));
  paginationInfo.put("paginationCtaLabel", (String) properties.getOrDefault("paginationCtaLabel", StringUtils.EMPTY));
  paginationInfo.put("static", RecipeListingViewHelperImpl.getStaticData(i18n));
  return paginationInfo;
 }
 
 /**
  * Gets the tag children.
  * 
  * @param className
  *         the class name
  * @param resolver
  *         the resolver
  * @param currentPage
  *         the current page
  * @param separate
  *         the separate
  * @param args
  *         the args
  * @return the tag children
  */
 public static Object getTagChildren(Class<? extends Object> className, ResourceResolver resolver, Page currentPage,
   Boolean separate, String... args) {
  String parentTag = getParentTag(args);
  TagManager manager = resolver.adaptTo(TagManager.class);
  if (StringUtils.isNotEmpty(parentTag)) {
   Tag tag = manager.resolve(parentTag);
   Locale locale = currentPage.getLanguage(false);
   Iterator<Tag> children = tag!=null?tag.listChildren():null;
   
   if (StringUtils.equalsIgnoreCase(className.getSimpleName(), HASH_MAP)) {
    List<Map<String, String>> filters = new ArrayList<Map<String, String>>();
    populateFilters(children, locale, separate, filters);
    return filters;
   }
   if (StringUtils.equalsIgnoreCase(className.getSimpleName(), STRING_CLASS_NAME)) {
    List<String> filters = new ArrayList<String>();
    populateFilters(children, separate, filters);
    return filters;
   }
  }
  return Collections.emptyList();
 }
 
 /**
  * Gets the default content types.
  * 
  * @param searchConfig
  *         the search config
  * @return the default content types
  */
 public static String getDefaultContentTypes(Map<String, String> searchConfig) {
  String defaultContent = searchConfig.getOrDefault(DEFAULT_CONTENT_TYPES, StringUtils.EMPTY);
  String[] defaultContentTypes = defaultContent.split(UnileverConstants.COMMA);
  String queryParams = StringUtils.EMPTY;
  if (defaultContentTypes.length > 1) {
   for (String contentTypeValue : defaultContentTypes) {
    queryParams += contentTypeValue + OR;
   }
   queryParams = String.format(QUERY_PARAM_FORMAT, queryParams.substring(0, queryParams.length() - FOUR));
  } else if (!ArrayUtils.isEmpty(defaultContentTypes)) {
   queryParams += FQ_CONTENT_TYPE_FORMAT + defaultContentTypes[0];
  }
  return StringUtils.isEmpty(defaultContent) ? StringUtils.EMPTY : queryParams;
 }
 
 /**
  * @param subBrandName
  * @param resources
  * @param path
  * @param selectedBrandClass
  * @param selectedBrand
  * @return Object[]
  */
 public static Object[] getSubBrandProperties(String[] subBrandName, Map<String, Object> resources, String path, StringBuilder selectedBrandClass,
   String selectedBrand) {
  List<Map<String, Object>> propertiesList = new ArrayList<Map<String, Object>>();
  Resource resource = getSearchInputResource(path, resources);
  int listLength = 0;
  List<Map<String, String>> subBrandsList = new ArrayList<Map<String, String>>();
  if (resource != null) {
   subBrandsList = ComponentUtil.getNestedMultiFieldProperties(resource, SUB_BRANDS);
  }
  listLength = CollectionUtils.isNotEmpty(subBrandsList) ? subBrandsList.size() : 0;
  for (Map<String, String> subBrand : subBrandsList) {
   setBrandProperties(selectedBrandClass, subBrand, propertiesList, path, resources, selectedBrand, subBrandName);
  }
  
  Object[] arrayToReturn = ArrayUtils.EMPTY_OBJECT_ARRAY;
  if (CollectionUtils.isEmpty(propertiesList) || listLength == 0) {
   return arrayToReturn;
  } else {
   arrayToReturn = propertiesList.toArray();
  }
  return arrayToReturn;
 }
 
 /**
  * 
  * @param properties
  * @param resources
  * @return
  */
 public static Map<String, Object> getSearchInputV2Properties(ValueMap properties) {
  Map<String, Object> searchImageMap = new HashMap<String, Object>();
  searchImageMap.put(SEARCH_HEADING_TEXT, MapUtils.getString(properties, SearchInputV2ViewHelperImpl.SEARCH_HEADING_TEXT, StringUtils.EMPTY));
  searchImageMap.put(SEARCH_INPUT_PLACEHOLDER_TEXT, MapUtils.getString(properties, SearchInputV2ViewHelperImpl.SEARCH_INPUT_PLACEHOLDER_TEXT, ""));
  searchImageMap.put(CTALABEL, MapUtils.getString(properties, SearchInputV2ViewHelperImpl.CTA_LABEL, ""));
  searchImageMap.put(IMAGEALTTEXT, MapUtils.getString(properties, "logoImageAltText"));
  String imgPath = MapUtils.getString(properties, SearchHelper.SEARCH_LOGO_IMAGE, "");
  searchImageMap.put("imgPath", imgPath);
  searchImageMap.put(CLASS, MapUtils.getString(properties, CLASS, ""));
  return searchImageMap;
 }
 
 /**
  * @param searchInput
  * @param subBrandName
  * @param resolver
  * @param externalSystem
  * @param array
  * @param properties 
  * @return JSONArray
  * @throws JSONException
  */
 public static JSONArray setRMSTagsQueryForSubBrands(Resource searchInput, String subBrandName, ResourceResolver resolver, String externalSystem,
   JSONArray array, Map<String, Object> properties) throws JSONException {
  List<Map<String, String>> resourceMap = getDialogProperties(searchInput, SUB_BRANDS);
  for (Map<String, String> resourceData : resourceMap) {
   String subBrand = MapUtils.getString(resourceData, SUB_BRAND_NAME_KEY);
   if (StringUtils.equals(subBrandName, subBrand)) {
    String filterDisplayFormat = MapUtils.getString(resourceData, "tagsRelation");
    String inclusionTags = MapUtils.getString(resourceData, "inclusionTags");
    String matchingTags = MapUtils.getString(resourceData, "matchingTag");
    setRMSTagRelation(inclusionTags, "and", resolver, externalSystem, false, array);
    setRMSTagRelation(matchingTags, filterDisplayFormat, resolver, externalSystem, true, array);
   }
  }
  if (array.length() > 0) {
   properties.put("RMSTagsQuery", array.toString());
  }
  return array;
  
 }
 
 /**
  * @param tagId
  * @param tagRel
  * @param resolver
  * @param externalSystem
  * @param children
  * @param array
  * @return JSONArray
  * @throws JSONException
  */
 public static JSONArray setRMSTagRelation(String tagId, String tagRel, ResourceResolver resolver, String externalSystem, boolean children,
   JSONArray array) throws JSONException {
  String[] tagIds = ArrayUtils.EMPTY_STRING_ARRAY;
  if (StringUtils.isNotBlank(tagId)) {
   tagIds = tagId.split(",");
  }
  TagManager tagManager = resolver.adaptTo(TagManager.class);
  for (String tag : tagIds) {
   JSONArray jsonArray = new JSONArray();
   if (StringUtils.contains(tag, externalSystem)) {
    Tag tagID = tagManager.resolve(tag);
    if (tagID != null) {
     String path = tagID.getPath();
     iterateRMSTags(path, children, tagRel, jsonArray, array, tagID, resolver);
    }
   }
   if (!StringUtils.equalsIgnoreCase(tagRel, OR_KEYWORD) && jsonArray.length() > 0) {
    array.put(jsonArray);
   }
  }
  return array;
 }
 
 /**
  * @param path
  * @param children
  * @param tagRel
  * @param jsonArray
  * @param array
  * @param tagID
  * @param resolver
  */
 public static void iterateRMSTags(String path, boolean children, String tagRel, JSONArray jsonArray, JSONArray array, Tag tagID,
   ResourceResolver resolver) {
  Resource tagsResource = resolver.resolve(path);
  if (children && tagsResource.hasChildren()) {
   Iterator<Tag> tagChildren = tagID.listChildren();
   while (tagChildren.hasNext()) {
    String childTag = StringUtils.substringAfterLast(tagChildren.next().getPath(), UnileverConstants.FORWARD_SLASH);
    setTagIntegerValinArr(childTag, jsonArray, array, tagRel);
   }
  } else {
   setTagIntegerValinArr(StringUtils.substringAfterLast(path, UnileverConstants.FORWARD_SLASH), jsonArray, array, tagRel);
  }
 }
 
 /**
  * <p>
  * Adds RMS tags to final JSON array for Tags Query.
  * </p>
  * <p>
  * If the tag is of RMS, it converts the tag name to integer and then adds.
  * </p>
  * 
  * @param tagChild
  * @param jsonArray
  * @param array
  * @param tagRel
  */
 public static void setTagIntegerValinArr(String tagChild, JSONArray jsonArray, JSONArray array, String tagRel) {
  int intChild = 0;
  try {
   intChild = Integer.parseInt(tagChild);
  } catch (NumberFormatException exception) {
   LOG.warn("child tag is not integer");
  }
  if (intChild > 0) {
   if (StringUtils.equalsIgnoreCase(tagRel, OR_KEYWORD)) {
    array.put(intChild);
   } else {
    jsonArray.put(intChild);
   }
  }
  
 }
 
 /**
  * Gets the nested multi field properties.
  * 
  * @param resource
  *         the resource
  * @param propertyName
  *         the property name
  * @return the nested multi field properties
  */
 public static List<Map<String, String>> getDialogProperties(Resource resource, String propertyName) {
  
  List<Map<String, String>> list = new ArrayList<Map<String, String>>();
  
  if (resource != null && propertyName != null) {
   Node currentNode = resource.adaptTo(Node.class);
   try {
    Property property = null;
    
    if (currentNode.hasProperty(propertyName)) {
     property = currentNode.getProperty(propertyName);
    }
    
    Value[] values = property != null ? getValueArray(property) : new Value[] {};
    for (Value val : values) {
     JSONObject rootObject = new JSONObject(val.getString());
     Map<String, String> topMap = toMap(rootObject);
     list.add(topMap);
    }
   } catch (PathNotFoundException e) {
    LOG.debug("the input path cannot be casted to url/path", e);
    LOG.warn("the input path cannot be casted to url/path" + e.getMessage());
   } catch (RepositoryException e) {
    LOG.debug("the search input path is not found", e);
    LOG.warn("the search input path is not found" + e.getMessage());
   } catch (JSONException e) {
    LOG.debug("JSON cannot be parsed", e);
    LOG.warn("JSON cannot be parsed" + e.getMessage());
   }
  }
  return list;
 }
 
 /**
  * Gets the value array.
  * 
  * @param property
  *         the property
  * @return the value array
  * @throws RepositoryException
  *          the repository exception
  */
 private static Value[] getValueArray(Property property) throws RepositoryException {
  Value[] values = null;
  if (property.isMultiple()) {
   values = property.getValues();
  } else {
   values = new Value[1];
   values[0] = property.getValue();
  }
  
  return values;
 }
 
 /**
  * To map.
  * 
  * @param object
  *         the object
  * @return the map
  * @throws JSONException
  *          the JSON exception
  */
 public static Map<String, String> toMap(JSONObject object) throws JSONException {
  Map<String, String> map = new HashMap<String, String>();
  
  Iterator<String> keysItr = object.keys();
  while (keysItr.hasNext()) {
   String key = keysItr.next();
   Object value = object.get(key);
   map.put(key, value.toString());
  }
  return map;
 }
 
 /**
  * 
  * @param resources
  * @param searchResultsMap
  */
 @SuppressWarnings({ "deprecation" })
 public static void setSearchTagsParams(Map<String, Object> resources, Map<String, Object> searchResultsMap) {
  SlingHttpServletRequest httpServletRequest = BaseViewHelper.getSlingRequest(resources);
  boolean stags = httpServletRequest.getParameterMap().containsKey(STAGS) ? true : false;
  ResourceResolver resolver = BaseViewHelper.getResourceResolver(resources);
  TagManager manager = resolver.adaptTo(TagManager.class);
  String resultSummaryTextTagFormat = " %s %s";
  if (stags) {
   Map<String, String> searchTagsParams = new HashMap<String, String>();
   String tag = httpServletRequest.getParameter(STAGS);
   tag = URLDecoder.decode(tag);
   String tagID = StringUtils.EMPTY;
   if (StringUtils.isNotBlank(tag)) {
    if (StringUtils.contains(tag, RMS_TAG_PATTERN)) {
     searchTagsParams.put("searchKeyword", "*");
     searchTagsParams.put("type", "RMS");
     searchTagsParams.put(TAG_ID, tag);
    } else {
     searchTagsParams.put("searchKeyword", "*:*");
     searchTagsParams.put("type", "internal");
     tagID = BaseViewHelper.getTagManager(resources).resolve(tag).getLocalTagID();
     searchTagsParams.put(TAG_ID, tagID);
    }
    searchTagsParams.put("searchKeywordDisplay", StringUtils.EMPTY);
    Tag actTag = manager.resolve(tag);
    if (actTag != null) {
     String tagTitle = actTag.getTitle(BaseViewHelper.getLocale(BaseViewHelper.getCurrentPage(resources)));
     searchTagsParams.put("tagTitle", tagTitle);
     I18n i18n = BaseViewHelper.getI18n(resources);
     String resultSummaryTextTag = String.format(resultSummaryTextTagFormat, i18n.get("search.tagresult.label"),
       tagTitle);
     searchTagsParams.put("resultSummaryTextTag", resultSummaryTextTag);
    }
    searchResultsMap.put("searchTagsParams", searchTagsParams);
   }
  }
 }
 
 /**
  * 
  * @param properties
  * @param componentProperties
  * @param key
  * @param prop
  * @param subBrand
  * @param flag
  * @return
  */
 public static String setCommonProperties(Map<String, Object> properties, Map<String, Object> componentProperties, String key, String prop,
   Map<String, String> subBrand, boolean flag) {
  String value = (StringUtils.isNotEmpty(MapUtils.getString(subBrand, key))) ? MapUtils.getString(subBrand, key) : MapUtils.getString(
    componentProperties, prop, StringUtils.EMPTY);
  if (flag) {
   properties.put(prop, value);
  }
  return value;
 }
 
 /**
  * 
  * @param selectedBrandClass
  * @param cssClass
  * @param componentProperties
  * @param subBrand
  * @param propertiesList
  * @param path
  * @param resource
  * @param resources
  * @param selectedBrand
  * @param subBrandName
  */
 public static void setBrandProperties(StringBuilder selectedBrandClass, Map<String, String> subBrand, List<Map<String, Object>> propertiesList,
   String path, Map<String, Object> resources, String selectedBrand, String[] subBrandName) {
  
  ResourceResolver resolver = BaseViewHelper.getResourceResolver(resources);
  String subBrandNameFromMap = MapUtils.getString(subBrand, SUB_BRAND_NAME_KEY);
  Map<String, Object> componentProperties = new HashMap<String, Object>();
  Resource resource = getSearchInputResource(path, resources);
  if (resource != null) {
   componentProperties = getSearchInputV2Properties(resource.adaptTo(ValueMap.class));
  }
  if (ArrayUtils.contains(subBrandName, subBrandNameFromMap)) {
   Map<String, Object> properties = new HashMap<String, Object>();
   Map<String, String> keyPropMap = new HashMap<String, String>();
   keyPropMap.put("subsearchHeadingText", SEARCH_HEADING_TEXT);
   keyPropMap.put("subSearchInputPlaceholderText", SEARCH_INPUT_PLACEHOLDER_TEXT);
   keyPropMap.put("subImageAltText", IMAGEALTTEXT);
   keyPropMap.put("subCtaLabel", CTALABEL);
   keyPropMap.put("cssStyleClass", CLASS);
   keyPropMap.put(SUB_SEARCH_LOGO_IMAGE, "imgPath");
   String imgPath = StringUtils.EMPTY;
   String cssClass = StringUtils.EMPTY;
   for (String key : keyPropMap.keySet()) {
    String prop = keyPropMap.get(key);
    String value = StringUtils.equalsIgnoreCase(key, SUB_SEARCH_LOGO_IMAGE) ? setCommonProperties(properties, componentProperties, key, prop,
      subBrand, false) : setCommonProperties(properties, componentProperties, key, prop, subBrand, true);
    if (StringUtils.equalsIgnoreCase(key, "cssStyleClass")) {
     cssClass = value;
    }
    if (StringUtils.equalsIgnoreCase(key, SUB_SEARCH_LOGO_IMAGE)) {
     imgPath = value;
    }
   }
   properties.put("name", subBrandNameFromMap);
   if (StringUtils.equalsIgnoreCase(subBrandNameFromMap, selectedBrand)) {
    selectedBrandClass.append(cssClass);
   }
   String subBrandAltImgText = (StringUtils.isNotEmpty(MapUtils.getString(properties, "imageAltText"))) ? MapUtils.getString(properties, "imageAltText") : StringUtils.EMPTY;
   Image image = new Image(imgPath, subBrandAltImgText, StringUtils.EMPTY, BaseViewHelper.getCurrentPage(resources),
     BaseViewHelper.getSlingRequest(resources));
   properties.put("searchLogoImage", image.convertToMap());
   if (StringUtils.isNotEmpty(path)) {
    JSONArray array = new JSONArray();
    try {
     setRMSTagsQueryForSubBrands(resource, subBrandNameFromMap, resolver, "rms", array, properties);
    } catch (JSONException e) {
     LOG.error("json exception", e);
    }
   }
   propertiesList.add(properties);
   
  }
 }
 
 /**
  * 
  * @param path
  * @param resources
  * @return
  */
 public static Resource getSearchInputResource(String path, Map<String, Object> resources) {
  Resource resource = null;
  if (StringUtils.isNotEmpty(path)) {
   resource = BaseViewHelper.getResourceResolver(resources).resolve(path);
  } else {
   resource = BaseViewHelper.getCurrentResource(resources);
  }
  return resource;
 }
 
 /**
  * 
  * @param children
  * @param locale
  * @param separate
  * @param filters
  */
 public static void populateFilters(Iterator<Tag> children, Locale locale, boolean separate, List<Map<String, String>> filters){
  while (children != null && children.hasNext()) {
  Tag tagChild = children.next();
  Map<String, String> filter = new HashMap<String, String>();
  String tagTitle = tagChild.getTitle(locale) != null ? tagChild.getTitle(locale) : tagChild.getTitle();
  
  filter.put(TITLE, tagTitle);
  String tagId = separate ? tagChild.getLocalTagID() : tagChild.getTagID();
  filter.put(TAG_ID, tagId);
  filters.add(filter);
  }
 }
 
 /**
  * 
  * @param children
  * @param separate
  * @param filters
  */
 public static void populateFilters(Iterator<Tag> children, boolean separate, List<String> filters){
  while (children.hasNext()) {
   Tag tagChild = children.next();
   String tagId = separate ? tagChild.getTagID().split(":")[1] : tagChild.getTagID();
   filters.add(tagId);
   
  }
 }
 
 /**
  * 
  * @param args
  * @return
  */
 public static String getParentTag(String...args ){
  String parentTag="";
  String brand="";
  if (args.length >= 1) {
   parentTag = args[0];
  }
  if (args.length >= TWO) {
   String contentType = args[1];
   parentTag = contentType + "/" + parentTag;
  }
  if (args.length >= THREE) {
   brand = args[TWO];
   parentTag = "unilever:" + brand + "/" + parentTag;
   
  }
  return parentTag;
 }
}
