/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.components.helper.TurnToHelper;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * This class prepares the data map .
 */
@Component(description = "TurnTo", immediate = true, metatype = true, label = "TurnToViewHelperImpl")
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.TURN_TO, propertyPrivate = true), })
public class TurnToViewHelperImpl extends BaseViewHelper {
 
 private static final String TITLE = "title";
 private static final String TURN_TO_TITLE = "turnToTitle";
 private static final String INTRO_COPY = "introductionCopy";
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(TurnToViewHelperImpl.class);
 
 /*
  * (Javadoc)
  * 
  * This helper class reads all the data from the turn to component.It also reads the configurations from the golabl configs and passesit in the json
  * 
  * For details on json and component structure,please visit https://tools.sapient .com/confluence/display/UU/CP-03+Anchor+Link+Navigation
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("Executing AnchorLinkViewHelperImpl.java. Inside processData method");
  Map<String, String> properties = new HashMap<String, String>();
  Page currentPage = getCurrentPage(resources);
  SlingHttpServletRequest slingRequest = getSlingRequest(resources);
  Map<String, Object> turnToDialogProperties = getProperties(content);
  UNIProduct uniProduct = ProductHelper.getUniProduct(currentPage, slingRequest);
  String productId = uniProduct != null ? uniProduct.getSKU() : "";
  properties = TurnToHelper.getTurnToConfig(getCurrentPage(resources), configurationService, productId);
  
  String title = MapUtils.getString(turnToDialogProperties, TURN_TO_TITLE, StringUtils.EMPTY);
  String introductionCopy = MapUtils.getString(turnToDialogProperties, INTRO_COPY, StringUtils.EMPTY);
  
  properties.put(TITLE, title);
  properties.put("productID", productId);
  properties.put("introCopy", introductionCopy);
  LOGGER.info("Exiting anchor link view helper ");
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE), properties);
  return data;
  
 }
}
