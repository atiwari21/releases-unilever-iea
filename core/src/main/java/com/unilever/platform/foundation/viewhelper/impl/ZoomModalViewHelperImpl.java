/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.components.UnileverComponents;

/**
 * <p>
 * This component don't have dialog. The Goal of this helper is to pass the entire value map excluding properties starting with
 * "cq:", "jcr:" and "sling:"
 * </p>
 */
@Component(description = "Zoom Modal", immediate = true, metatype = true, label = "zoomModal")
@Service(value = { ZoomModalViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.ZOOM_MODAL, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class ZoomModalViewHelperImpl extends BaseViewHelper{
 /**
  * logger object.
  **/
 private static final Logger LOGGER = LoggerFactory.getLogger(ZoomModalViewHelperImpl.class);

 private static final String CQ_COLON = "cq:";
 
 private static final String JCR_COLON = "jcr:";
 
 private static final String SLING_COLON = "sling:";
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.info("Inside processData method of ZoomModalViewHelperImpl");
  Map<String, Object> zoomModalMap = new LinkedHashMap<String, Object>();
  Map<String, Object> zoomModalProperties = getProperties(content);
  Iterator<String> iterator = zoomModalProperties.keySet().iterator();
  while(iterator.hasNext()){
   String key = iterator.next();
   if(!(key.startsWith(CQ_COLON) || key.startsWith(JCR_COLON) || key.startsWith(SLING_COLON))){
    zoomModalMap.put(key, zoomModalProperties.get(key));
   }
  }

  Map<String, Object> data = new LinkedHashMap<String, Object>();
  data.put(getJsonNameSpace(content), zoomModalMap);
  return data;
 }
 
}
