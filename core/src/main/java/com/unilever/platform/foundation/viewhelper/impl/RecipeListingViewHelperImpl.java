/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.jcr.Session;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.http.osgi.services.HttpClientBuilderFactory;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestParameterMap;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.i18n.I18n;
import com.day.cq.replication.Replicator;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.aem.foundation.constants.CommonConstants;
import com.unilever.platform.aem.foundation.constants.SearchConstants;
import com.unilever.platform.aem.foundation.core.dto.SearchDTO;
import com.unilever.platform.aem.foundation.core.dto.TaggingSearchCriteria;
import com.unilever.platform.aem.foundation.core.service.SearchParam;
import com.unilever.platform.aem.foundation.core.service.SearchService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.LandingPageListHelper;
import com.unilever.platform.foundation.components.helper.RMSJson;
import com.unilever.platform.foundation.components.helper.RecipeHelper;
import com.unilever.platform.foundation.components.helper.RecipeListingPageComparator;
import com.unilever.platform.foundation.components.helper.SearchHelper;
import com.unilever.platform.foundation.components.helper.TaggingComponentsHelper;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.RecipeConfigDTO;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * The Class RecipeListingViewHelperImpl.
 */
@Component(description = "RecipeListingViewHelperImpl", immediate = true, metatype = true, label = "RecipeListingViewHelperImpl")
@Service(value = { RecipeListingViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.RECIPE_LISTING, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class RecipeListingViewHelperImpl extends BaseViewHelper {
 
 /** The Constant ALPHABETICAL. */
 private static final String ALPHABETICAL = "alphabetical";
 
 /** The Constant THREE. */
 private static final int THREE = 3;
 
 /** The Constant TWO. */
 private static final int TWO = 2;
 
 /** The Constant FILTER_CONFIG. */
 private static final String FILTER_CONFIG = "filterConfig";
 
 /** The Constant DEFAULT_SORTING_OPTION. */
 private static final String DEFAULT_SORTING_OPTION = "defaultSortingOption";
 
 /** The Constant AEM_PROPERTY_DESCRIPTION. */
 private static final String AEM_PROPERTY_DESCRIPTION = "description";
 
 /** The Constant AEM_PROPERTY_PATH. */
 private static final String AEM_PROPERTY_PATH = "path";
 
 /** The Constant AEM_PROPERTIES. */
 private static final String AEM_PROPERTIES = "aemProperties";
 
 /** The Constant RECIPES_LIST. */
 private static final String RECIPES_LIST = "recipesList";
 
 /** The Constant GET_RECIPE_BY_ID_LIST. */
 private static final String GET_RECIPE_BY_ID_LIST = "getRecipeByIdList";
 
 /** The Constant RECIPE. */
 private static final String RECIPE = "recipe";
 
 /** The Constant RECIPE_PAGE_DETAILS. */
 private static final String RECIPE_PAGE_DETAILS = "recipePageDetails";
 
 /** The Constant TAGS. */
 private static final String TAGS = "tags";
 
 /** The Constant TAG_NAME. */
 private static final String TAG_NAME = "tagName";
 
 /** The Constant TAG_PATH. */
 private static final String TAG_PATH = "tagPath";
 
 /** The Constant TITLE. */
 private static final String TITLE = "title";
 
 /** The Constant PAGE_PATH. */
 private static final String PAGE_PATH = "pagePath";
 
 /** The Constant RELATED_RECIPES. */
 private static final String RELATED_RECIPES = "relatedRecipes";
 
 /** The Constant RECIPE_LISTING_STICKY_NAV_ADDITIONAL_FILTERS_CTA_LABEL. */
 private static final String RECIPE_LISTING_STICKY_NAV_ADDITIONAL_FILTERS_CTA_LABEL = "recipeListing.stickyNavAdditionalFiltersCtaLabel";
 
 /** The Constant RECIPE_LISTING_STICKY_NAV_HEADING. */
 private static final String RECIPE_LISTING_STICKY_NAV_HEADING = "recipeListing.stickyNavHeading";
 
 /** The Constant RECIPE_LISTING_RESET_FILTERS_CTA_LABEL. */
 private static final String RECIPE_LISTING_RESET_FILTERS_CTA_LABEL = "recipeListing.resetFiltersCtaLabel";
 
 /** The Constant RECIPE_LISTING_CANCEL_FILTER_CTA_LABEL. */
 private static final String RECIPE_LISTING_CANCEL_FILTER_CTA_LABEL = "recipeListing.cancelFilterCtaLabel";
 
 /** The Constant RECIPE_LISTING_APPLY_FILTER_CTA_LABEL. */
 private static final String RECIPE_LISTING_APPLY_FILTER_CTA_LABEL = "recipeListing.applyFilterCtaLabel";
 
 /** The Constant RECIPE_LISTING_ADDITIONAL_FILTERS_CTA_LABEL. */
 private static final String RECIPE_LISTING_ADDITIONAL_FILTERS_CTA_LABEL = "recipeListing.additionalFiltersCtaLabel";
 
 /** The Constant RECIPE_LISTING_FILTERS_SUMMARY_PREFIX. */
 private static final String RECIPE_LISTING_FILTERS_SUMMARY_PREFIX = "recipeListing.filtersSummaryPrefix";
 
 /** The Constant RECIPE_LISTING_RESULT_SUMMARY_TEXT. */
 private static final String RECIPE_LISTING_RESULT_SUMMARY_TEXT = "recipeListing.resultSummaryText";
 
 /** The Constant RECIPE_LISTING_ALTERNATE_RESULTS_MESSAGE. */
 private static final String RECIPE_LISTING_ALTERNATE_RESULTS_MESSAGE = "recipeListing.alternateResultsMessage";
 
 /** The Constant RECIPE_LISTING_NO_RESULTS_MESSAGE. */
 private static final String RECIPE_LISTING_NO_RESULTS_MESSAGE = "recipeListing.noResultsMessage";
 
 /** The Constant STICKY_NAV_ADDITIONAL_FILTERS_CTA_LABEL. */
 private static final String STICKY_NAV_ADDITIONAL_FILTERS_CTA_LABEL = "stickyNavAdditionalFiltersCtaLabel";
 
 /** The Constant STICKY_NAV_HEADING. */
 private static final String STICKY_NAV_HEADING = "stickyNavHeading";
 
 /** The Constant RESET_FILTERS_CTA_LABEL. */
 private static final String RESET_FILTERS_CTA_LABEL = "resetFiltersCtaLabel";
 
 /** The Constant CANCEL_FILTER_CTA_LABEL. */
 private static final String CANCEL_FILTER_CTA_LABEL = "cancelFilterCtaLabel";
 
 /** The Constant APPLY_FILTER_CTA_LABEL. */
 private static final String APPLY_FILTER_CTA_LABEL = "applyFilterCtaLabel";
 
 /** The Constant ADDITIONAL_FILTERS_CTA_LABEL. */
 private static final String ADDITIONAL_FILTERS_CTA_LABEL = "additionalFiltersCtaLabel";
 
 /** The Constant FILTERS_SUMMARY_PREFIX. */
 private static final String FILTERS_SUMMARY_PREFIX = "filtersSummaryPrefix";
 
 /** The Constant RESULT_SUMMARY_TEXT. */
 private static final String RESULT_SUMMARY_TEXT = "resultSummaryText";
 
 /** The Constant ALTERNATE_RESULTS_MESSAGE. */
 private static final String ALTERNATE_RESULTS_MESSAGE = "alternateResultsMessage";
 
 /** The Constant NO_RESULTS_MESSAGE. */
 private static final String NO_RESULTS_MESSAGE = "noResultsMessage";
 
 /** The Constant RECIPE_ID. */
 private static final String RECIPE_ID = "recipeId";
 
 /** The Constant RECIPE_RATING. */
 private static final String RECIPE_RATING = "recipeRating";
 
 /** The Constant PAGE_DESC. */
 private static final String PAGE_DESC = "pageDesc";
 
 /** The Constant PAGE_URL. */
 private static final String PAGE_URL = "pageUrl";
 
 /** The Constant STRING_SLASH. */
 private static final String STRING_SLASH = "/";
 
 /** The Constant HTML. */
 private static final String HTML = ".html";
 
 /** The Constant SLING_ALIAS. */
 private static final String SLING_ALIAS = "sling:alias";
 
 /** The Constant RECIPES. */
 private static final String RECIPES = "recipes";
 
 /** The Constant TOTAL_COUNT. */
 private static final String TOTAL_COUNT = "totalCount";
 
 /** The Constant ONE. */
 private static final String ONE = "1";
 
 /** The Constant STRING_ZERO. */
 private static final String STRING_ZERO = "0";
 
 /** The Constant ITEMS_PER_PAGE. */
 private static final String ITEMS_PER_PAGE = "itemsPerPage";
 
 /** The Constant FL. */
 private static final String FL = "fl";
 
 /** The Constant FEATURE_TAGS. */
 private static final String FEATURE_TAGS = "featureTags";
 
 /** The Constant COMPONENT_USAGE_CONTEXT. */
 private static final String COMPONENT_USAGE_CONTEXT = "componentUsageContext";
 
 /** The Constant PAGINATION_CTA_LABEL. */
 private static final String PAGINATION_CTA_LABEL = "paginationCtaLabel";
 
 /** The Constant PAGINATION_TYPE. */
 private static final String PAGINATION_TYPE = "paginationType";
 
 /** The Constant CTA_LABEL. */
 private static final String CTA_LABEL = "ctaLabel";
 
 /** The Constant SUB_HEADING_TEXT. */
 private static final String SUB_HEADING_TEXT = "text";
 
 /** The Constant HEADING_TEXT. */
 private static final String HEADING_TEXT = "headingText";
 
 /** The Constant RECIPE_DIETARY_ATTRIBUTES_LIST. */
 private static final String RECIPE_DIETARY_ATTRIBUTES_LIST = "recipeDietaryAttributesList";
 
 /** The Constant RECIPE_LISTING_SORTING_OPTIONS. */
 private static final String RECIPE_LISTING_SORTING_OPTIONS = "recipeListingSortingOptions";
 
 /** The Constant RECIPE_LISTING_CONFIG. */
 private static final String RECIPE_LISTING_CONFIG = "recipeListing";
 
 /** The Constant SLASH. */
 private static final char SLASH = '/';
 
 /** The Constant COMMA_SEPARATED. */
 private static final String COMMA_SEPARATED = "\\s*,\\s*";
 
 /** The Constant SORT_ASCENDING. */
 private static final String SORT_ASCENDING = "sortAscending";
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The factory. */
 @Reference
 HttpClientBuilderFactory factory;
 
 /** The builder. */
 @Reference
 private QueryBuilder builder;
 
 /** The search service. */
 @Reference
 SearchService searchService;
 
 @Reference
 Replicator replicator;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(RecipeListingViewHelperImpl.class);
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @SuppressWarnings("unchecked")
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  
  Map<String, Object> data = new HashMap<String, Object>();
  Map<String, Object> recipeListingMap = new LinkedHashMap<String, Object>();
  Map<String, Object> recipesMap = new LinkedHashMap<String, Object>();
  List<Map<String, Object>> featureTagsList = new LinkedList<Map<String, Object>>();
  List<Map<String, Object>> recipeList = new LinkedList<Map<String, Object>>();
  Map<String, Object> productListingMap = new LinkedHashMap<String, Object>();
  List<String> recipesPagePathList = new LinkedList<String>();
  Map<String, Object> recipeConfigMap = new LinkedHashMap<String, Object>();
  
  Map<String, Object> properties = getProperties(content);
  TagManager tagMgr = getTagManager(resources);
  Page currentPage = getCurrentPage(resources);
  String jsonNameSpace = getJsonNameSpace(content);
  ResourceResolver resourceResolver = getResourceResolver(resources);
  I18n i18n = getI18n(resources);
  
  Map<String, String> globalConfigMap = ComponentUtil.getConfigMap(currentPage, RECIPE_LISTING_CONFIG);
  Map<String, String> recipeListingSortingGlobalConfigMap = ComponentUtil.getConfigMap(currentPage, RECIPE_LISTING_SORTING_OPTIONS);
  Map<String, String> globalConfigMapDietary = ComponentUtil.getConfigMap(currentPage, RECIPE_DIETARY_ATTRIBUTES_LIST);
  Map<String, String> globalConfigMapAttribute = ComponentUtil.getConfigMap(currentPage, "recipeAttributesList");
  String headingText = MapUtils.getString(properties, HEADING_TEXT, StringUtils.EMPTY);
  String subheadingText = MapUtils.getString(properties, SUB_HEADING_TEXT, StringUtils.EMPTY);
  String ctaLabel = MapUtils.getString(properties, CTA_LABEL, StringUtils.EMPTY);
  
  String paginationType = MapUtils.getString(properties, PAGINATION_TYPE, StringUtils.EMPTY);
  String paginationCtaLabel = MapUtils.getString(properties, PAGINATION_CTA_LABEL, StringUtils.EMPTY);
  
  String componentUsageContext = MapUtils.getString(properties, COMPONENT_USAGE_CONTEXT, StringUtils.EMPTY);
  
  String[] featureTagNameSpaces = StringUtils.isNotBlank(MapUtils.getString(properties, FEATURE_TAGS))
    ? ComponentUtil.getPropertyValueArray(properties, FEATURE_TAGS) : new String[] {};
  
  getTagList(featureTagsList, tagMgr, featureTagNameSpaces);
  
  RecipeConfigDTO recipeConfigDTO = RecipeHelper.getRecipeConfigurations(configurationService, currentPage, jsonNameSpace);
  
  if ("fixedList".equals(componentUsageContext)) {
   
   getFixedList(resources, recipesMap, properties, recipeList, recipeConfigDTO, recipeConfigMap, recipeListingMap);
   
  } else if ("recipeDetailsPage".equals(componentUsageContext)) {
   
   boolean error = recipeJsonList(resources, recipesMap, currentPage, recipeList, recipeConfigDTO, currentPage.getPath());
   
   getRelatedProductIds(recipesMap, resourceResolver, currentPage, resources);
   RMSJson.setRMSServiceDownKey(resources, recipeListingMap, configurationService, getI18n(resources), error);
   
  } else if ("categoryOrLandingPages".equals(componentUsageContext)) {
   
   List<String> promotionTagNameSpaceList = new LinkedList<String>();
   
   recipesPagePathList = getPageBySearchTags(properties, globalConfigMap, currentPage, resourceResolver, resources, recipeConfigMap);
   
   getRecipeListFromRMS(resources, recipesMap, recipeList, recipesPagePathList, properties, recipeConfigDTO, promotionTagNameSpaceList,
     recipeConfigMap, recipeListingMap);
   
  } else if ("productDetailsPage".equals(componentUsageContext)) {
   
   List<String> promotionTagNameSpaceList = new LinkedList<String>();
   recipesPagePathList = getPageByTagSearch(properties, globalConfigMap, promotionTagNameSpaceList, resources, recipeConfigMap);
   
   getRecipeListFromRMS(resources, recipesMap, recipeList, recipesPagePathList, properties, recipeConfigDTO, promotionTagNameSpaceList,
     recipeConfigMap, recipeListingMap);
   
  }
  Object[] recipeArray = !recipeList.isEmpty() ? recipeList.toArray() : ArrayUtils.EMPTY_STRING_ARRAY;
  recipeListingMap.put(RECIPES, recipeArray);
  
  boolean isEnabledFeatureTagInteraction = ComponentUtil.isFeatureTagInteractionEnabled(currentPage);
  
  Map<String, Object>[] featureTagsArray = (Map<String, Object>[]) new Map[featureTagsList.size()];
  recipeConfigMap.put(FEATURE_TAGS, featureTagsList.toArray(featureTagsArray));
  recipeConfigMap.put(HEADING_TEXT, headingText);
  recipeConfigMap.put("subHeadingText", subheadingText);
  recipeConfigMap.put(CTA_LABEL, ctaLabel);
  recipeConfigMap.put(COMPONENT_USAGE_CONTEXT, componentUsageContext);
  recipeConfigMap.put(UnileverConstants.IS_ENABLED_FEATURE_TAG_INTERACTION, isEnabledFeatureTagInteraction);
  if (isEnabledFeatureTagInteraction) {
   recipeConfigMap.put("interactionFeatureTags", ComponentUtil.getFeatureTagList(resources, featureTagNameSpaces));
  }
  
  recipeConfigMap.put(PAGINATION_TYPE, paginationType);
  recipeConfigMap.put(PAGINATION_CTA_LABEL, paginationCtaLabel);
  
  getFilterProperties(content, resources, productListingMap);
  
  recipeConfigMap.put(FILTER_CONFIG, productListingMap.get(FILTER_CONFIG));
  recipeConfigMap.put("static", getStaticData(getI18n(resources)));
  recipeConfigMap.put("pagination", getPaginationData(properties));
  
  List<Map<String, Object>> dietryAttributeList = RecipeHelper.getAttributesDetails(i18n, properties, globalConfigMapDietary,
    "recipeDietaryAttributes.recipe", "DataDisplayLabel");
  List<Map<String, Object>> attributeList = getAttributesDetails(i18n, properties, globalConfigMapAttribute, "recipeAttributes.recipe",
    "DataDisplayLabel");
  
  boolean recipeRating = getRecipeFlag(properties, globalConfigMap);
  
  recipeConfigMap.put(RECIPE_RATING, recipeRating);
  
  Object[] dietryAttributeListArray = !dietryAttributeList.isEmpty() ? dietryAttributeList.toArray() : ArrayUtils.EMPTY_STRING_ARRAY;
  recipeConfigMap.put("dietaryAttributes", dietryAttributeListArray);
  
  Object[] attributeListArray = !attributeList.isEmpty() ? attributeList.toArray() : ArrayUtils.EMPTY_STRING_ARRAY;
  recipeConfigMap.put("recipeAttributes", attributeListArray);
  
  getSortingOptionsEnabledFields(recipeConfigMap, i18n, properties, recipeListingSortingGlobalConfigMap, "recipeListing.sortBy");
  
  getI18KeysValue(i18n, recipeConfigMap);
  
  recipeListingMap.put("recipeConfig", recipeConfigMap);
  
  data.put(jsonNameSpace, recipeListingMap);
  return data;
 }
 
 /**
  * Gets the attributes details.
  * 
  * @param i18n
  *         the i 18 n
  * @param properties
  *         the properties
  * @param globalConfigMap
  *         the global config map
  * @param prefix
  *         the prefix
  * @param postfix
  *         the postfix
  * @return the attributes details
  */
 public static List<Map<String, Object>> getAttributesDetails(I18n i18n, Map<String, Object> properties, Map<String, String> globalConfigMap,
   String prefix, String postfix) {
  
  LOGGER.debug("Inside get dietary attribute method");
  
  List<Map<String, Object>> attributeList = new LinkedList<Map<String, Object>>();
  
  boolean overrideGlobalConfig = MapUtils.getBooleanValue(properties, UnileverConstants.OVERRIDE_GLOBAL_CONFIG, false);
  
  for (String key : globalConfigMap.keySet()) {
   String value = StringUtils.EMPTY;
   if (overrideGlobalConfig) {
    if (properties.containsKey("attributes")) {
     
     value = RecipeHelper.getAttributeMapFromDialog(properties, key, value, false, false);
    }
   } else {
    value = MapUtils.getString(globalConfigMap, key);
   }
   
   if (StringUtils.isNotBlank(value)) {
    String[] attributeArray = value.split(COMMA_SEPARATED);
    Map<String, Object> attributesMap = RecipeHelper.getAttributeMap(prefix, postfix, i18n, key, attributeArray);
    if (MapUtils.isNotEmpty(attributesMap)) {
     attributeList.add(attributesMap);
    }
   }
  }
  
  return attributeList;
 }
 
 /**
  * Gets the i 18 keys value.
  * 
  * @param i18n
  *         the i 18 n
  * @param recipeListingMap
  *         the recipe listing map
  * @return the i 18 keys value
  */
 private static void getI18KeysValue(I18n i18n, Map<String, Object> recipeListingMap) {
  
  recipeListingMap.put(NO_RESULTS_MESSAGE, i18n.get(RECIPE_LISTING_NO_RESULTS_MESSAGE));
  recipeListingMap.put(ALTERNATE_RESULTS_MESSAGE, i18n.get(RECIPE_LISTING_ALTERNATE_RESULTS_MESSAGE));
  recipeListingMap.put(RESULT_SUMMARY_TEXT, i18n.get(RECIPE_LISTING_RESULT_SUMMARY_TEXT));
  recipeListingMap.put(FILTERS_SUMMARY_PREFIX, i18n.get(RECIPE_LISTING_FILTERS_SUMMARY_PREFIX));
  
  recipeListingMap.put(ADDITIONAL_FILTERS_CTA_LABEL, i18n.get(RECIPE_LISTING_ADDITIONAL_FILTERS_CTA_LABEL));
  recipeListingMap.put(APPLY_FILTER_CTA_LABEL, i18n.get(RECIPE_LISTING_APPLY_FILTER_CTA_LABEL));
  recipeListingMap.put(CANCEL_FILTER_CTA_LABEL, i18n.get(RECIPE_LISTING_CANCEL_FILTER_CTA_LABEL));
  recipeListingMap.put(RESET_FILTERS_CTA_LABEL, i18n.get(RECIPE_LISTING_RESET_FILTERS_CTA_LABEL));
  
  recipeListingMap.put(STICKY_NAV_HEADING, i18n.get(RECIPE_LISTING_STICKY_NAV_HEADING));
  recipeListingMap.put(STICKY_NAV_ADDITIONAL_FILTERS_CTA_LABEL, i18n.get(RECIPE_LISTING_STICKY_NAV_ADDITIONAL_FILTERS_CTA_LABEL));
 }
 
 /**
  * Gets the sorting options enabled fields.
  * 
  * @param recipeListingMap
  *         the recipe listing map
  * @param i18n
  *         the i 18 n
  * @param properties
  *         the properties
  * @param globalConfigMap
  *         the global config map
  * @param prefix
  *         the prefix
  * @return the sorting options enabled fields
  */
 private static void getSortingOptionsEnabledFields(Map<String, Object> recipeListingMap, I18n i18n, Map<String, Object> properties,
   Map<String, String> globalConfigMap, String prefix) {
  
  LOGGER.debug("Inside get Sorting Options Enabled Fields method");
  
  List<Map<String, Object>> attributeList = new LinkedList<Map<String, Object>>();
  
  Map<String, Object> map = new HashMap<String, Object>();
  
  Map<String, Object> map3 = new HashMap<String, Object>();
  
  boolean overrideGlobalConfig = MapUtils.getBooleanValue(properties, UnileverConstants.OVERRIDE_GLOBAL_CONFIG, false);
  
  for (String key : globalConfigMap.keySet()) {
   String value = StringUtils.EMPTY;
   if (overrideGlobalConfig) {
    
    if (properties.containsKey("sortingOptions")) {
     
     value = RecipeHelper.getAttributeMapFromDialog(properties, key, value, true, true);
    }
    
    if (properties.containsKey(key)) {
     value = MapUtils.getString(globalConfigMap, key);
    }
   } else {
    value = MapUtils.getString(globalConfigMap, key);
   }
   
   if (StringUtils.isNotBlank(value)) {
    String[] attributeArray = value.split(COMMA_SEPARATED);
    attributeList.add(getAttributeMap(prefix, i18n, attributeArray));
   }
  }
  
  Object[] attributeListArray = !attributeList.isEmpty() ? attributeList.toArray() : ArrayUtils.EMPTY_STRING_ARRAY;
  map.put("sortOptions", attributeListArray);
  
  map3.put("recipeIdList", "ril");
  map3.put("sortingCategory", "sc");
  map3.put("isSortInAsendingOrder", "ao");
  map3.put("isSortingRequired", "sr");
  map3.put("pageSize", "ps");
  map3.put("pageIndex", "pi");
  
  map.put("sortParams", map3);
  map.put("defaultSortOption", MapUtils.getString(properties, "defaultSortingOption", ALPHABETICAL));
  
  recipeListingMap.put("sortConfig", map);
  
 }
 
 /**
  * Gets the attribute map.
  * 
  * @param prefix
  *         the prefix
  * @param i18n
  *         the i 18 n
  * @param key
  *         the key
  * @param attributeArray
  *         the attribute array
  * @return the attribute map
  */
 private static Map<String, Object> getAttributeMap(String prefix, I18n i18n, String[] attributeArray) {
  
  Map<String, Object> attributeMap = new LinkedHashMap<String, Object>();
  boolean flag = (attributeArray.length > 1) ? Boolean.valueOf(attributeArray[1]) : true;
  
  if (flag && attributeArray.length > 0) {
   
   attributeMap.put("sortlabel", i18n.get(prefix + attributeArray[0].substring(0, 1).toUpperCase() + attributeArray[0].substring(1)));
   attributeMap.put("sortValue", attributeArray[0]);
  }
  
  return attributeMap;
 }
 
 /**
  * Gets the recipe flag.
  * 
  * @param properties
  *         the properties
  * @param globalConfigMap
  *         the global config map
  * @return the recipe flag
  */
 private static boolean getRecipeFlag(Map<String, Object> properties, Map<String, String> globalConfigMap) {
  boolean overrideGlobalConfig = MapUtils.getBooleanValue(properties, UnileverConstants.OVERRIDE_GLOBAL_CONFIG, false);
  boolean recipeRating;
  if (overrideGlobalConfig) {
   recipeRating = MapUtils.getBooleanValue(properties, RECIPE_RATING, false);
  } else {
   recipeRating = MapUtils.getBooleanValue(globalConfigMap, RECIPE_RATING, false);
  }
  return recipeRating;
 }
 
 /**
  * Gets the recipe list from RMS.
  * 
  * @param resources
  *         the resources
  * @param recipesMap
  *         the recipes map
  * @param recipeList
  *         the recipe list
  * @param recipesPagePathList
  *         the recipes page path list
  * @param properties
  *         the properties
  * @param recipeConfigDTO
  *         the recipe config DTO
  * @param promotionTagNameSpaceList
  *         the promotion tag name space list
  * @return the recipe list from RMS
  */
 private void getRecipeListFromRMS(Map<String, Object> resources, Map<String, Object> recipesMap, List<Map<String, Object>> recipeList,
   List<String> recipesPagePathList, Map<String, Object> properties, RecipeConfigDTO recipeConfigDTO, List<String> promotionTagNameSpaceList,
   Map<String, Object> recipeConfigMap, Map<String, Object> recipeListingMap) {
  
  Page currentPage = getCurrentPage(resources);
  
  ResourceResolver resourceResolver = getResourceResolver(resources);
  
  List<String> pageIdList = new LinkedList<String>();
  
  setRecipeIdList(recipesPagePathList, currentPage, pageIdList, resources);
  
  int count = getResponseBody(resources, properties, pageIdList, recipesMap, recipeList, recipeConfigDTO, factory, recipeListingMap);
  List<String> pagepathList = new LinkedList<String>();
  
  getAEMPropertyPagePathList(recipeList, resourceResolver, pagepathList);
  
  String[] promotionTags = new String[promotionTagNameSpaceList.size()];
  promotionTags = promotionTagNameSpaceList.toArray(promotionTags);
  
  Collections.sort(pagepathList, new RecipeListingPageComparator(promotionTags, getPageManager(resources)));
  
  List<Map<String, Object>> updatedJsonList = new LinkedList<Map<String, Object>>();
  
  getUpdatedJsonList(recipeList, resourceResolver, pagepathList, updatedJsonList);
  if (updatedJsonList != null && !updatedJsonList.isEmpty()) {
   recipeList.clear();
   recipeList.addAll(updatedJsonList);
  }
  
  SlingHttpServletRequest slingHttpServletRequest = getSlingRequest(resources);
  Map<String, String> globalConfigMap = ComponentUtil.getConfigMap(currentPage, RECIPE_LISTING_CONFIG);
  int itemsPerPage = MapUtils.getIntValue(properties, ITEMS_PER_PAGE);
  itemsPerPage = (itemsPerPage == 0) ? Integer.parseInt(MapUtils.getString(globalConfigMap, ITEMS_PER_PAGE, STRING_ZERO)) : itemsPerPage;
  String pageNoStr = StringUtils.isNotBlank(slingHttpServletRequest.getParameter("pn")) ? slingHttpServletRequest.getParameter("pn") : ONE;
  int pageNo = 0;
  try {
   pageNo = Integer.parseInt(pageNoStr);
  } catch (NumberFormatException ne) {
   LOGGER.warn("Page Number is not a number", ne);
  }
  List<Map<String, Object>> copyList = new LinkedList<Map<String, Object>>();
  copyList.addAll(recipeList);
  List<Map<String, Object>> updatedList = LandingPageListHelper.getSubListByPage(copyList, pageNo, itemsPerPage);
  
  if (updatedList != null && !updatedList.isEmpty()) {
   recipeList.clear();
   recipeList.addAll(updatedList);
  }
  recipeConfigMap.put(TOTAL_COUNT, count);
 }
 
 /**
  * Gets the page by search tags.
  * 
  * @param properties
  *         the properties
  * @param currentPage
  *         the current page
  * @param resourceResolver
  *         the resource resolver
  * @param resources
  *         the resources
  * @param recipeListingMap
  *         the recipe listing map
  * @return the page by search tags
  */
 private List<String> getPageBySearchTags(Map<String, Object> properties, Map<String, String> configMap, Page currentPage,
   ResourceResolver resourceResolver, Map<String, Object> resources, Map<String, Object> recipeListingMap) {
  
  List<TaggingSearchCriteria> criteriaList = new ArrayList<TaggingSearchCriteria>();
  List<String> qualifiedTagIdListForSearch = MapUtils.getObject(properties, "searchTags") != null
    ? ComponentUtil.getListFromStringArray(ComponentUtil.getPropertyValueArray(properties, "searchTags")) : new ArrayList<String>();
  
  Collections.sort(qualifiedTagIdListForSearch);
  
  List<String> secondlastList = new LinkedList<String>();
  
  for (String qualifiedTagId : qualifiedTagIdListForSearch) {
   String[] qualifiedTagIdArray = qualifiedTagId.split(STRING_SLASH);
   String secondLast = qualifiedTagIdArray[qualifiedTagIdArray.length - TWO];
   
   secondlastList.add(secondLast);
  }
  
  int count = 1, index = 0;
  boolean flag = false;
  
  List<String> qualifiedTagIdListUsingCriteria = new LinkedList<String>();
  List<String> finalQualifiedTagIdListUsingCriteria = new LinkedList<String>();
  
  if (secondlastList.size() > 1) {
   for (int initialize = 1; initialize <= secondlastList.size(); initialize++) {
    
    if (initialize < secondlastList.size() && secondlastList.get(initialize - 1).equals(secondlastList.get(initialize))) {
     count++;
    } else {
     flag = true;
    }
    
    if (initialize == secondlastList.size() - 1 || flag) {
     
     int counter = 0;
     qualifiedTagIdListUsingCriteria = new LinkedList<String>();
     while (counter < count && index < secondlastList.size()) {
      qualifiedTagIdListUsingCriteria.add(qualifiedTagIdListForSearch.get(index));
      if (counter != count - 1) {
       TaggingComponentsHelper.updateCriteriaList(qualifiedTagIdListUsingCriteria, criteriaList, SearchConstants.SEARCH_OR);
      } else if (count == 1) {
       TaggingComponentsHelper.updateCriteriaList(qualifiedTagIdListUsingCriteria, criteriaList, SearchConstants.SEARCH_OR);
      }
      counter++;
      index++;
     }
     finalQualifiedTagIdListUsingCriteria.addAll(qualifiedTagIdListUsingCriteria);
     
     count = 1;
     flag = false;
    }
   }
  } else if (secondlastList.size() == 1) {
   qualifiedTagIdListUsingCriteria.add(qualifiedTagIdListForSearch.get(0));
   TaggingComponentsHelper.updateCriteriaList(qualifiedTagIdListUsingCriteria, criteriaList, SearchConstants.SEARCH_OR);
  }
  
  List<String> pageList = getPageListOnSearchCriteria(configMap, currentPage, resourceResolver, criteriaList);
  
  boolean overrideGlobalConfig = MapUtils.getBooleanValue(properties, UnileverConstants.OVERRIDE_GLOBAL_CONFIG, false);
  int max = overrideGlobalConfig ? MapUtils.getIntValue(properties, UnileverConstants.LIMIT, 0)
    : MapUtils.getIntValue(configMap, "max", CommonConstants.FIVE);
  List<String> pagePathSubList = pageList.size() > max ? pageList.subList(0, max) : pageList;
  
  SlingHttpServletRequest slingHttpServletRequest = getSlingRequest(resources);
  String[] filters = slingHttpServletRequest.getParameterValues(FL) != null ? slingHttpServletRequest.getParameterValues(FL) : new String[] {};
  
  List<String> updatedPagePath = new LinkedList<String>();
  for (String pagePathItem : pagePathSubList) {
   Page listPage = getPageManager(resources).getContainingPage(pagePathItem);
   if (listPage != null && TaggingComponentsHelper.isEligbleFromFilter(listPage, filters)) {
    updatedPagePath.add(listPage.getPath());
   }
  }
  
  recipeListingMap.put(TOTAL_COUNT, updatedPagePath.size());
  
  return updatedPagePath;
 }
 
 /**
  * Gets the page list on search criteria.
  * 
  * @param currentPage
  *         the current page
  * @param resourceResolver
  *         the resource resolver
  * @param criteriaList
  *         the criteria list
  * @return the page list on search criteria
  */
 private List<String> getPageListOnSearchCriteria(Map<String, String> configMap, Page currentPage, ResourceResolver resourceResolver,
   List<TaggingSearchCriteria> criteriaList) {
  int offset = 0, limit = CommonConstants.FIFTY;
  String searchLimit = MapUtils.getString(configMap, UnileverConstants.LIMIT, StringUtils.EMPTY);
  
  try {
   limit = StringUtils.isNotBlank(searchLimit) ? Integer.parseInt(searchLimit) : CommonConstants.FIFTY;
  } catch (NumberFormatException nfe) {
   LOGGER.error("limit for page list must be number", nfe);
  }
  Map<String, List<String>> propMap = new HashMap<String, List<String>>();
  List<String> propValueList = new ArrayList<String>();
  propValueList.add(RECIPE);
  propMap.put(UnileverConstants.JCR_CONTENT_CONTENT_TYPE, propValueList);
  List<String> recipeExistsList = new ArrayList<String>();
  recipeExistsList.add("exists");
  propMap.put("jcr:content/recipeId", recipeExistsList);
  List<String> pageList = new ArrayList<String>();
  
  if (CollectionUtils.isNotEmpty(criteriaList)) {
   
   List<SearchDTO> list = searchService.getMultiTagResults(criteriaList, new LinkedList<String>(), currentPage.getPath(), propMap,
     new SearchParam(offset, limit, UnileverConstants.DESCENDING), resourceResolver);
   
   for (SearchDTO obj : list) {
    String page = obj.getResultNodePath();
    pageList.add(page);
   }
  }
  
  return pageList;
 }
 
 /**
  * Gets the AEM property page path list.
  * 
  * @param recipeList
  *         the recipe list
  * @param resourceResolver
  *         the resource resolver
  * @param pagepathList
  *         the pagepath list
  * @return the AEM property page path list
  */
 @SuppressWarnings("unchecked")
 private static void getAEMPropertyPagePathList(List<Map<String, Object>> recipeList, ResourceResolver resourceResolver, List<String> pagepathList) {
  for (Map<String, Object> singleRecipeMap : recipeList) {
   
   Map<String, Object> aemPropertyMap = singleRecipeMap.get(AEM_PROPERTIES) != null ? (Map<String, Object>) singleRecipeMap.get(AEM_PROPERTIES)
     : MapUtils.EMPTY_MAP;
   
   String tempAemPagePath = (String) aemPropertyMap.get(AEM_PROPERTY_PATH);
   
   if (StringUtils.contains(tempAemPagePath, "://")) {
    int c = StringUtils.ordinalIndexOf(tempAemPagePath, STRING_SLASH, THREE);
    tempAemPagePath = tempAemPagePath.substring(c);
   }
   String aemPagePath = StringUtils.EMPTY;
   if (tempAemPagePath != null) {
    aemPagePath = resourceResolver.resolve(tempAemPagePath).getPath();
   }
   
   pagepathList.add(aemPagePath);
  }
 }
 
 /**
  * Gets the updated json list.
  * 
  * @param recipeList
  *         the recipe list
  * @param resourceResolver
  *         the resource resolver
  * @param pagepathList
  *         the pagepath list
  * @param updatedJsonList
  *         the updated json list
  * @return the updated json list
  */
 @SuppressWarnings("unchecked")
 private static void getUpdatedJsonList(List<Map<String, Object>> recipeList, ResourceResolver resourceResolver, List<String> pagepathList,
   List<Map<String, Object>> updatedJsonList) {
  for (String pagePath : pagepathList) {
   
   for (Map<String, Object> eachJson : recipeList) {
    LOGGER.debug("recipeList full map is {}", eachJson);
    Map<String, Object> aemPropertyMap = (Map<String, Object>) eachJson.get(AEM_PROPERTIES);
    
    String aemPagePath2 = (String) aemPropertyMap.get(AEM_PROPERTY_PATH);
    
    if (StringUtils.contains(aemPagePath2, "://")) {
     int c = StringUtils.ordinalIndexOf(aemPagePath2, STRING_SLASH, THREE);
     aemPagePath2 = aemPagePath2.substring(c);
    }
    String aemPagePath = StringUtils.EMPTY;
    if (aemPagePath2 != null) {
     aemPagePath = resourceResolver.resolve(aemPagePath2).getPath();
    }
    
    if (pagePath.equals(aemPagePath)) {
     updatedJsonList.add(eachJson);
     break;
    }
    
   }
   
  }
 }
 
 /**
  * Sets the recipe id list.
  * 
  * @param recipesPagePathList
  *         the recipes page path list
  * @param currentPage
  *         the current page
  * @param pageIdList
  *         the page id list
  */
 private void setRecipeIdList(List<String> recipesPagePathList, Page currentPage, List<String> pageIdList, Map<String, Object> resources) {
  for (String individualpage : recipesPagePathList) {
   
   Page recipeIndividualPage = currentPage.getPageManager().getPage(individualpage);
   Session session = getResourceResolver(resources).adaptTo(Session.class);
   ValueMap individualpageProperties = recipeIndividualPage.getProperties();
   Set<String> runModes = getRunModes();
   boolean isPublish = runModes.contains("publish") ? true : false;
   String productPropertyAttribute = individualpageProperties.get(UnileverConstants.RECIPE_ID) != null
     ? individualpageProperties.get(UnileverConstants.RECIPE_ID).toString() : StringUtils.EMPTY;
   if (!isPublish) {
    if (!individualpageProperties.containsKey("isRecipeDeleted")
      && replicator.getReplicationStatus(session, recipeIndividualPage.getPath()) != null) {
     pageIdList.add(productPropertyAttribute);
    }
   } else {
    if (!individualpageProperties.containsKey("isRecipeDeleted")) {
     pageIdList.add(productPropertyAttribute);
    }
    
   }
  }
 }
 
 public static Set<String> getRunModes() {
  BundleContext bundleContext = FrameworkUtil.getBundle(RecipeListingViewHelperImpl.class).getBundleContext();
  ServiceReference serviceReference = bundleContext.getServiceReference(SlingSettingsService.class.getName());
  SlingSettingsService slingSettingsService = (SlingSettingsService) bundleContext.getService(serviceReference);
  return slingSettingsService.getRunModes();
 }
 
 /**
  * Gets the pagination data.
  * 
  * @param properties
  *         the properties
  * @return the pagination data
  */
 private static Map<String, Object> getPaginationData(Map<String, Object> properties) {
  
  Map<String, Object> paginationMap = new LinkedHashMap<String, Object>();
  paginationMap.put(ITEMS_PER_PAGE, MapUtils.getInteger(properties, ITEMS_PER_PAGE, 0));
  paginationMap.put(PAGINATION_TYPE, MapUtils.getString(properties, PAGINATION_TYPE));
  paginationMap.put(PAGINATION_CTA_LABEL, MapUtils.getString(properties, PAGINATION_CTA_LABEL));
  
  return paginationMap;
  
 }
 
 /**
  * Gets the static data.
  * 
  * @param i18n
  *         the i 18 n
  * @return the static data
  */
 public static Map<String, Object> getStaticData(I18n i18n) {
  Map<String, Object> staticMap = new LinkedHashMap<String, Object>();
  staticMap.put("firstLabel", i18n.get("recipeListing.pagination.first"));
  staticMap.put("lastLabel", i18n.get("recipeListing.pagination.last"));
  staticMap.put("previousLabel", i18n.get("recipeListing.pagination.previous"));
  staticMap.put("nextLabel", i18n.get("recipeListing.pagination.next"));
  return staticMap;
 }
 
 /**
  * Gets the response body.
  * 
  * @param resources
  *         the resources
  * @param properties
  *         the properties
  * @param pageIdList
  *         the page id list
  * @param recipesMap
  *         the recipes map
  * @param recipeList
  *         the recipe list
  * @param recipeConfigDTO
  *         the recipe config DTO
  * @param factory
  *         the factory
  * @return the response body
  */
 @SuppressWarnings("unchecked")
 private int getResponseBody(Map<String, Object> resources, Map<String, Object> properties, List<String> pageIdList, Map<String, Object> recipesMap,
   List<Map<String, Object>> recipeList, RecipeConfigDTO recipeConfigDTO, HttpClientBuilderFactory factory, Map<String, Object> recipeListingMap) {
  int count = 0;
  Page currentPage = getCurrentPage(resources);
  Map<String, String> sortCatOrderFromConfig = GlobalConfigurationUtility.getMapFromConfiguration(configurationService, currentPage,
    "recipeListingSortingCategoryOrder");
  SlingHttpServletRequest slingHttpServletRequest = getSlingRequest(resources);
  
  RequestParameterMap requestParameters = slingHttpServletRequest.getRequestParameterMap();
  
  List<String> newPageidList = slingHttpServletRequest.getParameterValues("ril") != null ? (List<String>) requestParameters.getValue("ril")
    : pageIdList;
  String sortingCategory = slingHttpServletRequest.getParameterValues("sc") != null ? requestParameters.getValue("sc").toString()
    : MapUtils.getString(properties, DEFAULT_SORTING_OPTION, ALPHABETICAL);
  Map<String, Object> sortCatOrder = getSortCatOrder(sortCatOrderFromConfig, sortingCategory);
  boolean ascendingOrder = slingHttpServletRequest.getParameterValues("ao") != null ? Boolean.valueOf(requestParameters.getValue("ao").toString())
    : MapUtils.getBoolean(sortCatOrder, SORT_ASCENDING, true);
  boolean sortingRequired = slingHttpServletRequest.getParameterValues("sr") != null ? Boolean.valueOf(requestParameters.getValue("sr").toString())
    : MapUtils.getBoolean(sortCatOrder, "isSortRequired", true);
  
  Map<String, String> globalConfigMap = ComponentUtil.getConfigMap(currentPage, RECIPE_LISTING_CONFIG);
  int itemsPerPage = MapUtils.getIntValue(properties, ITEMS_PER_PAGE);
  itemsPerPage = (itemsPerPage == 0) ? Integer.parseInt(MapUtils.getString(globalConfigMap, ITEMS_PER_PAGE, STRING_ZERO)) : itemsPerPage;
  
  int pageSize = slingHttpServletRequest.getParameterValues("ps") != null ? Integer.parseInt(requestParameters.getValue("ps").toString())
    : itemsPerPage;
  int pageIndex = slingHttpServletRequest.getParameterValues("pi") != null ? Integer.parseInt(requestParameters.getValue("pi").toString()) : 1;
  
  count = getRecipeJsonFromIdByPOST(resources, recipesMap, recipeList, recipeConfigDTO, factory, newPageidList, sortingCategory, ascendingOrder,
    sortingRequired, pageSize, pageIndex, recipeListingMap);
  return count;
 }
 
 /**
  * Gets the recipe json from id by POST.
  * 
  * @param resources
  *         the resources
  * @param recipesMap
  *         the recipes map
  * @param recipeList
  *         the recipe list
  * @param recipeConfig
  *         the recipe config
  * @param factory
  *         the factory
  * @param pageIdList
  *         the page id list
  * @param sortingCategory
  *         the sorting category
  * @param ascendingOrder
  *         the ascending order
  * @param sortingRequired
  *         the sorting required
  * @param pageSize
  *         the page size
  * @param pageIndex
  *         the page index
  * @param recipeListingMap
  * @return the recipe json from id by POST
  */
 @SuppressWarnings("unchecked")
 public static int getRecipeJsonFromIdByPOST(Map<String, Object> resources, Map<String, Object> recipesMap, List<Map<String, Object>> recipeList,
   RecipeConfigDTO recipeConfig, HttpClientBuilderFactory factory, List<String> pageIdList, String sortingCategory, boolean ascendingOrder,
   boolean sortingRequired, int pageSize, int pageIndex, Map<String, Object> recipeListingMap) {
  int count = 0;
  LOGGER.debug("Inside getRecipeJsonFromIdByPOST method to get recipe json based on page");
  JSONObject jsonObj = null;
  
  Page currentPage = getCurrentPage(resources);
  
  try {
   LOGGER.debug("Inside Try Block of get recipe json based on page");
   
   Map<String, String> globalConfigMap = ComponentUtil.getConfigMap(currentPage, "rmsEndPoint");
   
   String newUrl = MapUtils.getString(globalConfigMap, GET_RECIPE_BY_ID_LIST, StringUtils.EMPTY);
   JSONObject requestObject = null;
   requestObject = prepareRecipeRequest(pageIdList, sortingCategory, ascendingOrder, sortingRequired, pageSize, pageIndex);
   LOGGER.error("request object is" + requestObject.toString());
   
   if (!pageIdList.isEmpty()) {
    if (getRunModes().contains("author")) {
     jsonObj = RMSJson.getRecipeByPOST(requestObject, recipeConfig.getTimeout(), factory, newUrl, recipeConfig.getBrand(), recipeConfig.getLocale(),
       true);
    } else {
     jsonObj = RMSJson.getRecipeByPOST(requestObject, recipeConfig.getTimeout(), factory, newUrl, recipeConfig.getBrand(), recipeConfig.getLocale(),
       false);
    }
   }
   Map<String, Object> finalmap = jsonObj != null ? RecipeHelper.toMap(jsonObj) : MapUtils.EMPTY_MAP;
   LOGGER.debug("json object recipeList is" + finalmap.toString());
   RMSJson.setRMSServiceDownKey(resources, recipeListingMap, jsonObj, currentPage.adaptTo(ConfigurationService.class), getI18n(resources));
   List<Map<String, Object>> recipelistFullMap = (List<Map<String, Object>>) ((List<Map<String, Object>>) finalmap.get(RECIPES_LIST) != null
     ? (List<Map<String, Object>>) finalmap.get(RECIPES_LIST) : new LinkedList<Map<String, Object>>());
   count = MapUtils.getIntValue(finalmap, "recipeCount", 0);
   LOGGER.debug("recipeList full map is {}", recipelistFullMap);
   for (Map<String, Object> eachMap : recipelistFullMap) {
    Map<String, Object> aemPropertyMap = (Map<String, Object>) eachMap.get(AEM_PROPERTIES) != null ? (Map<String, Object>) eachMap.get(AEM_PROPERTIES)
      : MapUtils.EMPTY_MAP;
    
    String aemPagePath = aemPropertyMap.containsKey(AEM_PROPERTY_PATH) ? (String) aemPropertyMap.get(AEM_PROPERTY_PATH) : StringUtils.EMPTY;
    String description = (String) aemPropertyMap.get(AEM_PROPERTY_DESCRIPTION);
    if (getRunModes().contains("publish")) {
     eachMap.put(PAGE_URL, aemPagePath);
    } else {
     String pagePath = aemPagePath;
     if (StringUtils.contains(pagePath, "://")) {
      int c = StringUtils.ordinalIndexOf(pagePath, STRING_SLASH, THREE);
      pagePath = pagePath.substring(c);
     }
     ResourceResolver resolver = BaseViewHelper.getResourceResolver(resources);
     String path = resolver.resolve(pagePath).getPath();
     eachMap.put(PAGE_URL, ComponentUtil.getFullURL(resolver, path));
    }
    eachMap.put(PAGE_DESC, description);
    if (StringUtils.isNotEmpty(aemPagePath)) {
     addRecipeRatingMap(resources, eachMap, aemPagePath);
    }
   }
   
   recipeList.addAll(recipelistFullMap);
   
   Object[] recipeListArray = !recipeList.isEmpty() ? recipeList.toArray() : ArrayUtils.EMPTY_STRING_ARRAY;
   recipesMap.put(UnileverConstants.RECIPES, recipeListArray);
   
  } catch (Exception e1) {
   LOGGER.debug("Inside Catch Block of get recipe json based on page");
   LOGGER.error("error --> ", e1);
  }
  return count;
 }
 
 /**
  * Adds the recipe rating map.
  * 
  * @param resources
  *         the resources
  * @param eachMap
  *         the each map
  * @param aemPagePath
  *         the aem page path
  */
 private static void addRecipeRatingMap(Map<String, Object> resources, Map<String, Object> eachMap, String aemPagePath) {
  String aemPath = aemPagePath;
  if (StringUtils.contains(aemPath, "://")) {
   int c = StringUtils.ordinalIndexOf(aemPath, STRING_SLASH, THREE);
   aemPath = aemPath.substring(c);
  }
  ResourceResolver resolver = BaseViewHelper.getResourceResolver(resources);
  LOGGER.debug(
    "Path Returned By Resolver of addRecipeRatingMap Is {} " + BaseViewHelper.getPageManager(resources).getPage(resolver.resolve(aemPath).getPath()));
  LOGGER.debug("Page Returned By parent of addRecipeRatingMap Is {} "
    + BaseViewHelper.getPageManager(resources).getPage(resolver.resolve(aemPath).getPath()).getParent());
  
  String parentPath = BaseViewHelper.getPageManager(resources).getPage(resolver.resolve(aemPath).getPath()).getParent() != null
    ? ComponentUtil.getFullURL(resolver, BaseViewHelper.getPageManager(resources).getPage(resolver.resolve(aemPath).getPath()).getParent().getPath(),
      getSlingRequest(resources))
    : StringUtils.EMPTY;
  
  LOGGER.debug("Parent Path of addRecipeRatingMap is {} ", parentPath);
  
  Map<String, Object> recipeRatingMap = RecipeHelper.getReviewAndRatingMap(aemPagePath, parentPath, (String) eachMap.get(RECIPE_ID), resources);
  recipeRatingMap.put(ProductConstants.PRODUCT_RATINGS_MAP, getProductRatingsMap((String) eachMap.get(RECIPE_ID), "primary"));
  eachMap.put(RECIPE_RATING, recipeRatingMap);  
 }
 
 /**
  * Gets the product ratings map.
  *
  * @param recipeId
  *         the recipe id
  * @param viewType
  *         the view type
  * @return the product ratings map
  */
 private static Map<String, Object> getProductRatingsMap(String recipeId, String viewType) {
  Map<String, Object> details = new HashMap<String, Object>();
  
  details.put(ProductConstants.RATING_UNIQUE_ID, recipeId);
  details.put(ProductConstants.RATING_IDENTIFIER_TYPE, StringUtils.EMPTY);
  details.put(ProductConstants.RATING_IDENTIFIER_VALUE, recipeId);
  details.put(ProductConstants.RATING_ENTITY_TYPE, ProductConstants.PRODUCT_MAP);
  details.put(ProductConstants.RATING_VIEW_TYPE, viewType);
  
  LOGGER.debug("Recipe Ratings Map Properties : " + details);
  return details;
 }
 
 /**
  * Prepare recipe request.
  * 
  * @param pageIdList
  *         the page id list
  * @param sortingCategory
  *         the sorting category
  * @param ascendingOrder
  *         the ascending order
  * @param sortingRequired
  *         the sorting required
  * @param pageSize
  *         the page size
  * @param pageIndex
  *         the page index
  * @return the JSON object
  */
 private static JSONObject prepareRecipeRequest(List<String> pageIdList, String sortingCategory, boolean ascendingOrder, boolean sortingRequired,
   int pageSize, int pageIndex) {
  JSONObject jsonRequest = new JSONObject();
  
  try {
   jsonRequest.put("RecipeIdList", pageIdList);
   jsonRequest.put("SortingCategory", sortingCategory);
   jsonRequest.put("IsSortInAssendingOrder", ascendingOrder);
   jsonRequest.put("IsSortingRequired", sortingRequired);
   jsonRequest.put("PageSize", pageSize);
   jsonRequest.put("pageIndex", pageIndex);
   LOGGER.error("JSON request=" + jsonRequest.toString());
  } catch (JSONException je) {
   LOGGER.error("Email template json error : ", je);
  }
  
  return jsonRequest;
 }
 
 /**
  * Gets the updated pages list.
  * 
  * @param content
  *         the content
  * @param resources
  *         the resources
  * @param recipeListingMap
  *         the product listing map
  * @param productsPagePathList
  *         the products page path list
  * @param pagePathList
  *         the page path list
  * @return the updated pages list
  */
 private void getFilterProperties(Map<String, Object> content, Map<String, Object> resources, Map<String, Object> recipeListingMap) {
  
  Map<String, Object> properties = getProperties(content);
  Page currentPage = getCurrentPage(resources);
  
  Map<String, Object> filterMaps = getFilterProperties(properties, currentPage, getCurrentResource(resources), RECIPE_LISTING_CONFIG);
  filterMaps.put("filterParamName", FL);
  filterMaps.put("pageNoParamName", "pn");
  String resPath = getCurrentResource(resources).getPath();
  
  String ajaxURL = getResourceResolver(resources).map(resPath + ".data.json");
  filterMaps.put("ajaxURL", ajaxURL);
  recipeListingMap.put(FILTER_CONFIG, filterMaps);
 }
 
 /**
  * Gets the filter properties.
  * 
  * @param properties
  *         the properties
  * @param articlePagePathList
  *         the article page path list
  * @param currentPage
  *         the current page
  * @param tagManager
  *         the tag manager
  * @param cuResource
  *         the cu resource
  * @param globalConfigCatName
  *         the gloabl config cat name
  * @return the filter properties
  */
 public static Map<String, Object> getFilterProperties(Map<String, Object> properties, Page currentPage, Resource cuResource,
   String globalConfigCatName) {
  Map<String, String> configMap = ComponentUtil.getConfigMap(currentPage, globalConfigCatName);
  List<Map<String, String>> filterList = ComponentUtil.getNestedMultiFieldProperties(cuResource, UnileverConstants.FILTER_OPTION_JSON);
  
  String filterParentHeading = MapUtils.getString(properties, UnileverConstants.FILTER_PARENT_HEADING, StringUtils.EMPTY);
  filterParentHeading = StringUtils.isNotBlank(filterParentHeading) ? filterParentHeading
    : MapUtils.getString(configMap, UnileverConstants.FILTER_PARENT_HEADING, StringUtils.EMPTY);
  Map<String, Object> filterMaps = new LinkedHashMap<String, Object>();
  
  filterMaps.put(UnileverConstants.HEADING, filterParentHeading);
  List<Map<String, Object>> parentFilterList = new LinkedList<Map<String, Object>>();
  Map<String, String> rmsEndPointMap = ComponentUtil.getConfigMap(currentPage, "recipeListing");
  String brand = rmsEndPointMap.get("brand");
  if (CollectionUtils.isNotEmpty(filterList)) {
   setFiltersListMap(properties, currentPage, cuResource, filterList, parentFilterList, brand);
  } else {
   
   List<Map<String, String>> globalConfigFiltersList = RecipeHelper.getFilterFromGlobalConfig(configMap);
   
   setFiltersListMap(properties, currentPage, cuResource, globalConfigFiltersList, parentFilterList, brand);
  }
  
  Object[] recipeArray = !parentFilterList.isEmpty() ? parentFilterList.toArray() : ArrayUtils.EMPTY_STRING_ARRAY;
  filterMaps.put(UnileverConstants.FILTER_OPTIONS, recipeArray);
  return filterMaps;
 }
 
 /**
  * Sets the filters list map.
  * 
  * @param properties
  *         the properties
  * @param currentPage
  *         the current page
  * @param cuResource
  *         the cu resource
  * @param filterList
  *         the filter list
  * @param parentFilterList
  *         the parent filter list
  * @param brand
  *         the brand
  */
 @SuppressWarnings("unchecked")
 private static void setFiltersListMap(Map<String, Object> properties, Page currentPage, Resource cuResource, List<Map<String, String>> filterList,
   List<Map<String, Object>> parentFilterList, String brand) {
  for (Map<String, String> filterMap : filterList) {
   Map<String, Object> newMap = new LinkedHashMap<String, Object>();
   String filterHeading = MapUtils.getString(filterMap, UnileverConstants.FILTER_HEADING, StringUtils.EMPTY);
   String filterSubHeading = MapUtils.getString(filterMap, UnileverConstants.FILTER_SUB_HEADING, StringUtils.EMPTY);
   String displayFormat = MapUtils.getString(filterMap, UnileverConstants.FILTER_DISPLAY_FORMAT, StringUtils.EMPTY);
   String defaultValue = MapUtils.getString(filterMap, UnileverConstants.FILTER_DEFAULT_VALUE, StringUtils.EMPTY);
   String cssClass = MapUtils.getString(filterMap, UnileverConstants.CSS_STYLE_CLASS, StringUtils.EMPTY);
   newMap.put(UnileverConstants.HEADING, filterHeading);
   newMap.put(UnileverConstants.SUB_HEADING, filterSubHeading);
   newMap.put(UnileverConstants.DISPLAY_FORMAT, displayFormat);
   newMap.put(UnileverConstants.DEFAULT_VALUE, defaultValue);
   newMap.put(UnileverConstants.CLASS, cssClass);
   
   String parentFilterTag = MapUtils.getString(filterMap, "filterDisplayOption", StringUtils.EMPTY);
   List<Map<String, Object>> tagChilds = new LinkedList<Map<String, Object>>();
   
   if (StringUtils.equalsIgnoreCase(MapUtils.getString(properties, "componentName"), RECIPE_LISTING_CONFIG)) {
    tagChilds = (List<Map<String, Object>>) SearchHelper.getTagChildren(new HashMap().getClass(), cuResource.getResourceResolver(), currentPage,
      false, parentFilterTag, RECIPES, brand);
    
   }
   Object[] tagChildArray = new Object[0];
   if (CollectionUtils.isNotEmpty(tagChilds)) {
    tagChildArray = tagChilds.toArray();
   }
   newMap.put(UnileverConstants.FILTERS, tagChildArray);
   parentFilterList.add(newMap);
  }
 }
 
 /**
  * Gets the page by tag search.
  * 
  * @param properties
  *         the properties
  * @param configMap
  *         the config map
  * @param promotionTagNameSpaceList
  *         the promotion tag name space list
  * @param resources
  *         the resources
  * @param recipeListingMap
  *         the recipe listing map
  * @return the page by tag search
  */
 public List<String> getPageByTagSearch(Map<String, Object> properties, Map<String, String> configMap, List<String> promotionTagNameSpaceList,
   Map<String, Object> resources, Map<String, Object> recipeListingMap) {
  
  List<String> propValueList = new ArrayList<String>();
  propValueList.add(RECIPE);
  
  List<String> pagePathList = getSortedPagePathList(properties, configMap, propValueList, searchService, promotionTagNameSpaceList, resources,
    recipeListingMap);
  
  boolean overrideGlobalConfig = MapUtils.getBooleanValue(properties, UnileverConstants.OVERRIDE_GLOBAL_CONFIG, false);
  int max = overrideGlobalConfig ? MapUtils.getIntValue(properties, UnileverConstants.LIMIT, 0)
    : MapUtils.getIntValue(configMap, "max", CommonConstants.FIVE);
  
  return pagePathList.size() > max ? pagePathList.subList(0, max) : pagePathList;
  
 }
 
 /**
  * Gets the sorted page path list.
  * 
  * @param properties
  *         the properties
  * @param configMap
  *         the config map
  * @param propValueList
  *         the prop value list
  * @param searchService
  *         the search service
  * @param promotionTagNameSpaceList
  *         the promotion tag name space list
  * @param resources
  *         the resources
  * @param recipeListingMap
  *         the recipe listing map
  * @return the sorted page path list
  */
 public static List<String> getSortedPagePathList(Map<String, Object> properties, Map<String, String> configMap, List<String> propValueList,
   SearchService searchService, List<String> promotionTagNameSpaceList, Map<String, Object> resources, Map<String, Object> recipeListingMap) {
  
  ResourceResolver resourceResolver = getResourceResolver(resources);
  
  Page currentPage = getCurrentPage(resources);
  
  List<String> pageList = new ArrayList<String>();
  
  String tagBaseSearchType = MapUtils.getString(properties, UnileverConstants.TAG_SOURCE, StringUtils.EMPTY);
  
  Map<String, Object> overridenConfigMap = GlobalConfigurationUtility.getOverriddenConfigMap(configMap, properties);
  
  String[] promotionTags = StringUtils.isNotBlank(MapUtils.getString(overridenConfigMap, UnileverConstants.PROMOTION_TAGS, StringUtils.EMPTY))
    ? ComponentUtil.getPropertyValueArray(overridenConfigMap, UnileverConstants.PROMOTION_TAGS) : new String[] {};
  
  promotionTagNameSpaceList.addAll(ComponentUtil.getListFromStringArray(promotionTags));
  
  if (UnileverConstants.CURRENT_PAGE_TAGS.equals(tagBaseSearchType)) {
   pageList = getPagePathList(currentPage, properties, configMap, propValueList, true, searchService, resourceResolver);
   
  } else if (UnileverConstants.SEARCH_TAGS.equals(tagBaseSearchType)) {
   pageList = getPagePathList(currentPage, properties, configMap, propValueList, false, searchService, resourceResolver);
  }
  
  SlingHttpServletRequest slingHttpServletRequest = getSlingRequest(resources);
  String[] filters = slingHttpServletRequest.getParameterValues(FL) != null ? slingHttpServletRequest.getParameterValues(FL) : new String[] {};
  List<String> updatedPagePath = new LinkedList<String>();
  for (String pagePathItem : pageList) {
   Page listPage = getPageManager(resources).getContainingPage(pagePathItem);
   if (listPage != null && TaggingComponentsHelper.isEligbleFromFilter(listPage, filters)) {
    updatedPagePath.add(listPage.getPath());
   }
  }
  
  recipeListingMap.put(TOTAL_COUNT, updatedPagePath.size());
  
  return pageList;
 }
 
 /**
  * Gets the page path list.
  * 
  * @param currentPage
  *         the current page
  * @param valueMap
  *         the value map
  * @param configMap
  *         the config map
  * @param propValueList
  *         the prop value list
  * @param currentPageTagFlag
  *         the current page tag flag
  * @param searchService
  *         the search service
  * @param resourceResolver
  *         the resource resolver
  * @return the page path list
  */
 public static List<String> getPagePathList(Page currentPage, Map<String, Object> valueMap, Map<String, String> configMap, List<String> propValueList,
   boolean currentPageTagFlag, SearchService searchService, ResourceResolver resourceResolver) {
  int offset = 0, limit = CommonConstants.FIFTY;
  Map<String, List<String>> propMap = new HashMap<String, List<String>>();
  propMap.put(UnileverConstants.JCR_CONTENT_CONTENT_TYPE, propValueList);
  Map<String, Object> overridenConfig = GlobalConfigurationUtility.getOverriddenConfigMap(configMap, valueMap);
  boolean overrideGlobalConfig = MapUtils.getBooleanValue(valueMap, UnileverConstants.OVERRIDE_GLOBAL_CONFIG, false);
  int max = overrideGlobalConfig ? MapUtils.getIntValue(valueMap, UnileverConstants.LIMIT, 0)
    : MapUtils.getIntValue(configMap, "max", CommonConstants.FIVE);
  String searchLimit = String.valueOf(max);
  
  try {
   limit = StringUtils.isNotBlank(searchLimit) ? Integer.parseInt(searchLimit) : CommonConstants.FIFTY;
  } catch (NumberFormatException nfe) {
   LOGGER.error("limit for page list must be number", nfe);
  }
  List<TaggingSearchCriteria> criteriaList = new ArrayList<TaggingSearchCriteria>();
  List<String> qualifiedTagIdListForSearch = new ArrayList<String>();
  if (currentPageTagFlag) {
   String[] includeNameSpaceList = StringUtils.isNotBlank(MapUtils.getString(overridenConfig, UnileverConstants.MATCHING_TAGS))
     ? ComponentUtil.getPropertyValueArray(overridenConfig, UnileverConstants.MATCHING_TAGS) : new String[] {};
   for (String includeNameSpace : includeNameSpaceList) {
    qualifiedTagIdListForSearch = TaggingComponentsHelper.getQualifiedTagListFromPage(includeNameSpace, currentPage);
    TaggingComponentsHelper.updateCriteriaList(qualifiedTagIdListForSearch, criteriaList, SearchConstants.SEARCH_OR);
   }
  } else {
   qualifiedTagIdListForSearch = MapUtils.getObject(overridenConfig, UnileverConstants.MATCHING_TAGS) != null
     ? ComponentUtil.getListFromStringArray(ComponentUtil.getPropertyValueArray(overridenConfig, UnileverConstants.MATCHING_TAGS))
     : new ArrayList<String>();
   TaggingComponentsHelper.updateCriteriaList(qualifiedTagIdListForSearch, criteriaList, SearchConstants.SEARCH_OR);
  }
  
  List<String> pageList = new ArrayList<String>();
  if (CollectionUtils.isNotEmpty(criteriaList)) {
   String[] excludeTagNameSpaces = StringUtils.isNotBlank(MapUtils.getString(overridenConfig, UnileverConstants.EXCLUSION_TAGS))
     ? ComponentUtil.getPropertyValueArray(overridenConfig, UnileverConstants.EXCLUSION_TAGS) : new String[] {};
   List<String> excludeTagNameSpaceList = ComponentUtil.getListFromStringArray(excludeTagNameSpaces);
   
   String[] filterTagsNamespace = StringUtils.isNotBlank(MapUtils.getString(overridenConfig, UnileverConstants.INCLUSION_TAGS))
     ? ComponentUtil.getPropertyValueArray(overridenConfig, UnileverConstants.INCLUSION_TAGS) : new String[] {};
   TaggingComponentsHelper.updateCriteriaList(ComponentUtil.getListFromStringArray(filterTagsNamespace), criteriaList, SearchConstants.SEARCH_AND);
   List<SearchDTO> list = searchService.getMultiTagResults(criteriaList, excludeTagNameSpaceList, currentPage.getPath(), propMap,
     new SearchParam(offset, limit, UnileverConstants.DESCENDING), resourceResolver);
   
   for (SearchDTO obj : list) {
    String page = obj.getResultNodePath();
    pageList.add(page);
   }
  }
  
  return pageList;
 }
 
 /**
  * Gets the cq pages.
  * 
  * @param session
  *         the session
  * @param relatedRecipes
  *         the related recipes
  * @param currentPage
  *         the current page
  * @return the cq pages
  */
 private List<Page> getCqPages(Session session, String relatedRecipes, Page currentPage) {
  
  List<Page> cqPageList = new ArrayList<Page>();
  String path = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, RECIPE_LISTING_CONFIG, PAGE_PATH);
  
  cqPageList = getRecipePages(session, path, relatedRecipes);
  
  return cqPageList;
 }
 
 /**
  * Gets the recipe pages.
  * 
  * @param session
  *         the session
  * @param path
  *         the path
  * @param relatedRecipes
  *         the related recipes
  * @return the recipe pages
  */
 public List<Page> getRecipePages(Session session, String path, String relatedRecipes) {
  
  Map<String, String> map = new HashMap<String, String>();
  List<Page> recipePages = new ArrayList<Page>();
  
  List<Page> pageList = new ArrayList<Page>();
  
  map.put(AEM_PROPERTY_PATH, path);
  map.put("type", NameConstants.NT_PAGE);
  map.put("property", "jcr:content/recipeId");
  map.put("property.value", relatedRecipes);
  
  if (builder != null) {
   pageList = getResultfromQuery(session, map, builder);
   recipePages.addAll(pageList);
  }
  return recipePages;
 }
 
 /**
  * Gets the resultfrom query.
  * 
  * @param session
  *         the session
  * @param map
  *         the map
  * @param builder
  *         the builder
  * @return the resultfrom query
  */
 public static List<Page> getResultfromQuery(Session session, Map<String, String> map, QueryBuilder builder) {
  
  com.day.cq.search.Query query = null;
  SearchResult result = null;
  List<Page> pageList = new ArrayList<Page>();
  
  query = builder.createQuery(PredicateGroup.create(map), session);
  query.setHitsPerPage(0);
  if (query != null) {
   result = query.getResult();
   
   Iterator<Resource> rsIterator = result.getResources();
   while (rsIterator.hasNext()) {
    Page page = rsIterator.next().adaptTo(Page.class);
    if (page != null) {
     pageList.add(page);
    }
   }
  }
  return pageList;
 }
 
 /**
  * Gets the related product ids.
  * 
  * @param recipesMap
  *         the recipes map
  * @param resourceResolver
  *         the resource resolver
  * @param currentPage
  *         the current page
  * @param resources
  *         the resources
  * @return the related product ids
  */
 @SuppressWarnings("unchecked")
 public void getRelatedProductIds(Map<String, Object> recipesMap, ResourceResolver resourceResolver, Page currentPage,
   Map<String, Object> resources) {
  
  Map<String, Object> recipeDetailPages = new LinkedHashMap<String, Object>();
  
  Map<String, Object> dataMap = RecipeHelper.getChildMap(recipesMap, RECIPES);
  
  if (MapUtils.isNotEmpty(dataMap) && dataMap.containsKey(RELATED_RECIPES)) {
   
   List<Map<String, Object>> relatedRecipesList = (List<Map<String, Object>>) dataMap.get(RELATED_RECIPES);
   
   for (Map<String, Object> relatedRecipeMap : relatedRecipesList) {
    
    if (MapUtils.isNotEmpty(relatedRecipeMap) && relatedRecipeMap.containsKey(RECIPE_ID)) {
     String recipeId = (String) relatedRecipeMap.get(RECIPE_ID);
     
     List<Page> cqPages = getCqPages(resourceResolver.adaptTo(Session.class), recipeId, currentPage);
     
     Map<String, Object> newTagMap = new LinkedHashMap<String, Object>();
     List<Map<String, Object>> tagDataList = new LinkedList<Map<String, Object>>();
     
     if (!cqPages.isEmpty()) {
      Page firstCqPage = cqPages.get(0);
      firstCqPage.getTitle();
      firstCqPage.getDescription();
      firstCqPage.getTags();
      
      ValueMap pageProperties = firstCqPage.getProperties();
      
      recipeDetailPages = new LinkedHashMap<String, Object>();
      
      getAliasPageUrl(resources, firstCqPage.getPath(), recipeDetailPages, pageProperties);
      
      recipeDetailPages.put(TITLE, firstCqPage.getTitle());
      recipeDetailPages.put(PAGE_DESC, firstCqPage.getDescription());
      
      Tag[] tagList = firstCqPage.getTags();
      
      if (tagList.length > 0) {
       for (Tag eachTag : tagList) {
        
        newTagMap = new LinkedHashMap<String, Object>();
        newTagMap.put(TAG_PATH, eachTag.getTagID());
        newTagMap.put(TAG_NAME, eachTag.getTitle());
        
        tagDataList.add(newTagMap);
       }
      } else {
       getEmptyTagList(newTagMap, tagDataList);
      }
      recipeDetailPages.put(TAGS, tagDataList);
      relatedRecipeMap.put(RECIPE_PAGE_DETAILS, recipeDetailPages);
     } else {
      recipeDetailPages = new LinkedHashMap<String, Object>();
      
      recipeDetailPages.put(TITLE, StringUtils.EMPTY);
      recipeDetailPages.put(PAGE_DESC, StringUtils.EMPTY);
      getEmptyTagList(newTagMap, tagDataList);
      recipeDetailPages.put(TAGS, tagDataList);
      recipeDetailPages.put(PAGE_URL, StringUtils.EMPTY);
      relatedRecipeMap.put(RECIPE_PAGE_DETAILS, recipeDetailPages);
     }
    }
    
   }
  }
 }
 
 /**
  * Gets the tag list.
  * 
  * @param newTagMap
  *         the new tag map
  * @param tagDataList
  *         the tag data list
  * @return the tag list
  */
 private void getEmptyTagList(Map<String, Object> newTagMap, List<Map<String, Object>> tagDataList) {
  newTagMap.put(TAG_PATH, StringUtils.EMPTY);
  newTagMap.put(TAG_NAME, StringUtils.EMPTY);
  
  tagDataList.add(newTagMap);
 }
 
 /**
  * Gets the tag list.
  * 
  * @param featureTagsList
  *         the feature tags list
  * @param tagMgr
  *         the tag mgr
  * @param featureTagNameSpaces
  *         the feature tag name spaces
  * @return the tag list
  */
 private void getTagList(List<Map<String, Object>> featureTagsList, TagManager tagMgr, String[] featureTagNameSpaces) {
  
  Map<String, Object> featureTagsMap = new LinkedHashMap<String, Object>();
  for (String tagName : featureTagNameSpaces) {
   
   featureTagsMap = new LinkedHashMap<String, Object>();
   
   if (tagMgr != null) {
    Tag tag = tagMgr.resolve(tagName);
    if (tag != null) {
     String title = tagMgr.resolve(tagName).getTitle();
     featureTagsMap.put(TAG_PATH, tagName);
     featureTagsMap.put(TAG_NAME, title);
     
     featureTagsList.add(featureTagsMap);
    }
   }
  }
 }
 
 /**
  * Gets the fixed list.
  * 
  * @param resources
  *         the resources
  * @param recipesMap
  *         the recipes map
  * @param properties
  *         the properties
  * @param currentPage
  *         the current page
  * @param jsonNameSpace
  *         the json name space
  * @param list
  *         the list
  * @param recipeConfigDTO
  *         the recipe config DTO
  * @param recipeListingMap
  *         the recipe listing map
  * @return the fixed list
  */
 private void getFixedList(Map<String, Object> resources, Map<String, Object> recipesMap, Map<String, Object> properties,
   List<Map<String, Object>> list, RecipeConfigDTO recipeConfigDTO, Map<String, Object> recipeListingMap, Map<String, Object> recipeListMap) {
  
  Page currentPage = getCurrentPage(resources);
  
  String[] recipePath = StringUtils.isNotBlank(MapUtils.getString(properties, RECIPE)) ? ComponentUtil.getPropertyValueArray(properties, RECIPE)
    : new String[] {};
  
  SlingHttpServletRequest slingHttpServletRequest = getSlingRequest(resources);
  Map<String, String> globalConfigMap = ComponentUtil.getConfigMap(currentPage, RECIPE_LISTING_CONFIG);
  String[] filters = slingHttpServletRequest.getParameterValues(FL) != null ? slingHttpServletRequest.getParameterValues(FL) : new String[] {};
  int itemsPerPage = MapUtils.getIntValue(properties, ITEMS_PER_PAGE);
  itemsPerPage = (itemsPerPage == 0) ? Integer.parseInt(MapUtils.getString(globalConfigMap, ITEMS_PER_PAGE, STRING_ZERO)) : itemsPerPage;
  String pageNoStr = StringUtils.isNotBlank(slingHttpServletRequest.getParameter("pn")) ? slingHttpServletRequest.getParameter("pn") : ONE;
  int pageNo = 0;
  try {
   pageNo = Integer.parseInt(pageNoStr);
  } catch (NumberFormatException ne) {
   LOGGER.warn("Page Number is not a number", ne);
  }
  
  List<String> updatedPagePath = new LinkedList<String>();
  for (String pagePathItem : recipePath) {
   Page listPage = getPageManager(resources).getContainingPage(pagePathItem);
   if (listPage != null && TaggingComponentsHelper.isEligbleFromFilter(listPage, filters)) {
    updatedPagePath.add(listPage.getPath());
   }
  }
  
  recipeListingMap.put(TOTAL_COUNT, updatedPagePath.size());
  
  List<String> pageIdList = new LinkedList<String>();
  
  setRecipeIdList(updatedPagePath, currentPage, pageIdList, resources);
  
  List<Map<String, Object>> newUpdatedList = new LinkedList<Map<String, Object>>();
  
  getResponseBody(resources, properties, pageIdList, recipesMap, newUpdatedList, recipeConfigDTO, factory, recipeListMap);
  
  List<Map<String, Object>> finalListAfterSublist = LandingPageListHelper.getSubListByPage(newUpdatedList, pageNo, itemsPerPage);
  
  if (finalListAfterSublist != null && !finalListAfterSublist.isEmpty()) {
   list.addAll(finalListAfterSublist);
  } else {
   list.addAll(newUpdatedList);
  }
 }
 
 /**
  * Recipe json list.
  * 
  * @param resources
  *         the resources
  * @param recipesMap
  *         the recipes map
  * @param properties
  *         the properties
  * @param currentPage
  *         the current page
  * @param list
  *         the list
  * @param recipeConfigDTO
  *         the recipe config dto
  * @param path
  *         the path
  * @return
  */
 @SuppressWarnings("unchecked")
 public boolean recipeJsonList(Map<String, Object> resources, Map<String, Object> recipesMap, Page currentPage, List<Map<String, Object>> list,
   RecipeConfigDTO recipeConfigDTO, String path) {
  
  Map<String, Object> tempRecipeMap = new LinkedHashMap<String, Object>();
  if (!getRecipeJsonFromPage(resources, recipesMap, recipeConfigDTO, factory, path)) {
   if (recipesMap.containsKey(RECIPES)) {
    
    tempRecipeMap = (Map<String, Object>) recipesMap.get(RECIPES);
    
    Page recipePage = path != null ? currentPage.getPageManager().getPage(path) : currentPage;
    
    ValueMap pageProperties = recipePage.getProperties();
    
    getAliasPageUrl(resources, path, tempRecipeMap, pageProperties);
    
    String pageDesc = pageProperties.containsKey(JcrConstants.JCR_DESCRIPTION) ? (String) pageProperties.get(JcrConstants.JCR_DESCRIPTION)
      : StringUtils.EMPTY;
    
    tempRecipeMap.put(PAGE_DESC, pageDesc);
    
    String parentUrl = BaseViewHelper.getPageManager(resources).getPage(path) != null
      ? ComponentUtil.getFullURL(BaseViewHelper.getResourceResolver(resources),
        BaseViewHelper.getPageManager(resources).getPage(path).getParent().getPath(), getSlingRequest(resources))
      : StringUtils.EMPTY;
    
    tempRecipeMap.put(RECIPE_RATING,
      RecipeHelper.getReviewAndRatingMap((String) tempRecipeMap.get(PAGE_URL), parentUrl, (String) tempRecipeMap.get(RECIPE_ID), resources));
    
    list.add(tempRecipeMap);
   }
  } else {
   return true;
  }
  return false;
 }
 
 /**
  * Gets the alias page url.
  * 
  * @param resources
  *         the resources
  * @param path
  *         the path
  * @param tempRecipeMap
  *         the temp recipe map
  * @param pageProperties
  *         the page properties
  * @return the alias page url
  */
 private void getAliasPageUrl(Map<String, Object> resources, String path, Map<String, Object> tempRecipeMap, ValueMap pageProperties) {
  String productPropertyAttribute = pageProperties.get(SLING_ALIAS) != null ? pageProperties.get(SLING_ALIAS).toString() : StringUtils.EMPTY;
  
  String url = ComponentUtil.getFullURL(getResourceResolver(resources), path, getSlingRequest(resources));
  
  String pageUrl = StringUtils.EMPTY;
  
  if (StringUtils.isNotEmpty(productPropertyAttribute)) {
   int index = url.lastIndexOf(SLASH);
   
   pageUrl = url.substring(0, index) + STRING_SLASH + productPropertyAttribute + HTML;
   
   tempRecipeMap.put(PAGE_URL, pageUrl);
  } else {
   tempRecipeMap.put(PAGE_URL, url);
  }
 }
 
 /**
  * Gets the recipe json from page.
  * 
  * @param resources
  *         the resources
  * @param recipeProperties
  *         the recipe properties
  * @param recipesMap
  *         the recipes map
  * @param recipeConfig
  *         the recipe config
  * @param factory
  *         the factory
  * @param recipePath
  *         the recipe path
  * @return the recipe json from page
  */
 public static boolean getRecipeJsonFromPage(Map<String, Object> resources, Map<String, Object> recipesMap, RecipeConfigDTO recipeConfig,
   HttpClientBuilderFactory factory, String recipePath) {
  
  LOGGER.debug("Inside getRecipeJsonFromPage method to get recipe json based on page");
  JSONObject jsonObj = null;
  ResourceResolver resourceResolver = BaseViewHelper.getResourceResolver(resources);
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) resources.get(ProductConstants.REQUEST);
  Resource compResource = slingRequest.getResource();
  Page currentPage = pageManager.getContainingPage(compResource);
  
  Page recipePage = recipePath != null ? currentPage.getPageManager().getPage(recipePath) : currentPage;
  
  ValueMap properties = recipePage.getProperties();
  String productPropertyAttribute = properties.get(UnileverConstants.RECIPE_ID) != null ? properties.get(UnileverConstants.RECIPE_ID).toString()
    : StringUtils.EMPTY;
  
  try {
   LOGGER.debug("Inside Try Block of get recipe json based on page");
   
   String newUrl = MessageFormat.format(recipeConfig.getEndPointUrl(), productPropertyAttribute);
   jsonObj = RMSJson.getRecipe(recipeConfig.getTimeout(), factory, newUrl, recipeConfig.getBrand(), recipeConfig.getLocale(), recipeConfig);
   Map<String, Object> finalmap = new HashMap<String, Object>();
   if (jsonObj != null && jsonObj.has("error")) {
    return true;
   } else {
    finalmap = RecipeHelper.toMap(jsonObj);
   }
   
   recipesMap.put(UnileverConstants.RECIPES, finalmap);
   
  } catch (Exception e1) {
   LOGGER.debug("Inside Catch Block of get recipe json based on page");
   LOGGER.error("error --> ", e1);
  }
  return false;
 }
 
 /**
  * Gets the recipe json from id by POST.
  * 
  * @param sortCatOrder
  *         the sortCatOrder
  * @param key
  *         the key
  * @return
  */
 public static Map<String, Object> getSortCatOrder(Map<String, String> sortCatOrder, String key) {
  Map<String, Object> result = new HashMap<String, Object>();
  if (sortCatOrder != null && MapUtils.isNotEmpty(sortCatOrder)) {
   String cat = MapUtils.getString(sortCatOrder, key);
   if (StringUtils.isNotBlank(cat)) {
    String[] catOptions = StringUtils.split(cat, ",");
    if (catOptions.length == TWO) {
     result.put(SORT_ASCENDING, catOptions[0]);
     result.put("isSortRequired", catOptions[1]);
    } else if (catOptions.length == 1) {
     result.put(SORT_ASCENDING, catOptions[0]);
    }
   }
  }
  return result;
 }
 
}
