/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.components.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.json.JSONArray;
import org.json.JSONObject;

import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.aem.foundation.core.service.SearchService;
import com.unilever.platform.foundation.components.bean.DiagnosticToolQuestion.Answer;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.DiagnosticToolHelper;
import com.unilever.platform.foundation.components.helper.ProductBundlingToBinUtil;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.Image;

/**
 * The Class ResultSection.
 */
public class DiagnosticToolResultSection {
 
 /** The tag ids. */
 private List<String> tagIds;
 
 /** The general configuration. */
 private Map<String, Object> generalConfiguration;
 
 /** The products section. */
 private Map<String, Object> productsSection;
 
 /** The articles section. */
 private Map<String, Object> articlesSection;
 
 /** The question to summarize. */
 private List<Map<String, Object>> questionsToSummarise;
 
 /** The personalized response. */
 private Map<String, Object> personalisedResponse;
 
 /** The display type. */
 private String displayType;
 
 /** The social share personalised text flag. */
 private boolean socialSharePersonalisedTextFlag;
 
 private static final String IMAGE = "image";
 /**
  * Instantiates a new result section.
  * 
  * @param resultSection
  *         the result section
  * @param currentPage
  *         the current page
  * @param httpServletRequest
  *         the http servlet request
  * @param tagIds
  *         the tag ids
  * @param searchService
  *         the search service
  * @param questionsMap
  *         the questions map
  * @param socialSharePersonalisedTextFlag
  *         the social share personalised text flag
  */
 
 public DiagnosticToolResultSection(Resource resultSection, Page currentPage, SlingHttpServletRequest httpServletRequest, List<String> tagIds,
   SearchService searchService, Map<String, DiagnosticToolQuestion> questionsMap, boolean socialSharePersonalisedTextFlag) {
  ValueMap properties = resultSection.getValueMap();
  this.tagIds = tagIds;
  
  ResourceResolver resourceResolver = httpServletRequest.getResourceResolver();
  setGeneralConfiguration(properties, currentPage, resourceResolver, httpServletRequest);
  setPersonalizedResponse(resultSection, questionsMap, currentPage, httpServletRequest, properties);
  setProductsSection(resultSection, searchService, currentPage, httpServletRequest, properties);
  setArticlesSection(resultSection, searchService, currentPage, httpServletRequest, properties);
  setQuestionToSummarize(resultSection, questionsMap, resourceResolver, properties);
  setDisplayType(properties);
  setSocialSharePersonalisedTextFlag(socialSharePersonalisedTextFlag);
  
 }
 
 /**
  * Gets the general configuration.
  * 
  * @return the general configuration
  */
 public Map<String, Object> getGeneralConfiguration() {
  return generalConfiguration;
 }
 
 /**
  * Sets the general configuration.
  * 
  * @param properties
  *         the properties
  * @param currentPage
  *         the current page
  * @param resourceResolver
  *         the resource resolver
  * @param slingRequest
  *         the sling request
  */
 public void setGeneralConfiguration(Map<String, Object> properties, Page currentPage, ResourceResolver resourceResolver,
   SlingHttpServletRequest slingRequest) {
  generalConfiguration = new HashMap<String, Object>();
  generalConfiguration.put("headingText", MapUtils.getString(properties, "headingText", ""));
  generalConfiguration.put("subHeadingText", MapUtils.getString(properties, "subheadingText", ""));
  generalConfiguration.put("longSubHeadingText", MapUtils.getString(properties, "longSubheadingText", ""));
  
  String backgroundImagePath = MapUtils.getString(properties, "backgroundImage", "");
  Image backgroundImage = new Image(backgroundImagePath, "", "", currentPage, slingRequest);
  
  generalConfiguration.put("backgroundImage", backgroundImage.convertToMap());
  
  Map<String, Object> primaryCtaMap = new HashMap<String, Object>();
  primaryCtaMap.put("label", MapUtils.getString(properties, "primaryCtaLabel", ""));
  primaryCtaMap.put("url", ComponentUtil.getFullURL(resourceResolver, MapUtils.getString(properties, "primaryCtaUrl", ""), slingRequest));
  primaryCtaMap.put("openInNewWindow", MapUtils.getBoolean(properties, "primaryOpenInNewWindow", false));
  
  generalConfiguration.put("primaryCTA", primaryCtaMap);
  
  Map<String, Object> secondryCtaMap = new HashMap<String, Object>();
  secondryCtaMap.put("label", MapUtils.getString(properties, "secondaryCtaLabel", ""));
  secondryCtaMap.put("url", ComponentUtil.getFullURL(resourceResolver, MapUtils.getString(properties, "secondaryCtaUrl", ""), slingRequest));
  secondryCtaMap.put("openInNewWindow", MapUtils.getBoolean(properties, "secondaryOpenInNewWindow", false));
  
  generalConfiguration.put("secondryCTA", secondryCtaMap);
 }
 
 /**
  * Sets the products.
  * 
  * @param resultSection
  *         the result section
  * @param searchService
  *         the search service
  * @param currentPage
  *         the current page
  * @param httpServletRequest
  *         the http servlet request
  * @param properties
  *         the properties
  */
 public void setProductsSection(Resource resultSection, SearchService searchService, Page currentPage, SlingHttpServletRequest httpServletRequest,
   Map<String, Object> properties) {
  if (MapUtils.getBoolean(properties, "productsFlag", false)) {
   List<Map<String, Object>> productListSections = new ArrayList<Map<String, Object>>();
   ConfigurationService configurationService = currentPage.adaptTo(ConfigurationService.class);
   if (CollectionUtils.isNotEmpty(tagIds)) {
    int searchLimit = GlobalConfigurationUtility.getIntegerValueFromConfiguration("diagnosticTool", configurationService, currentPage,
      "productSearchLimit");
    List<Map<String, String>> productsProp = ComponentUtil.getNestedMultiFieldProperties(resultSection, "products");
    Iterator<Map<String, String>> itr = productsProp.iterator();
    while (itr.hasNext()) {
     Map<String, String> productProp = itr.next();
     Map<String, Object> productListSection = new HashMap<String, Object>();
     String listHeading = MapUtils.getString(productProp, "productListHeading", "");
     List<Map<String, Object>> productList = DiagnosticToolHelper.getProducts(productProp, searchService, this.tagIds, searchLimit, currentPage,
       httpServletRequest);
     productListSection.put("listHeading", listHeading);
     productListSection.put("products", productList.toArray());
     productListSections.add(productListSection);
    }
   }
   if (CollectionUtils.isNotEmpty(productListSections)) {
    this.productsSection = new HashMap<String, Object>();
    productsSection.put("heading", MapUtils.getString(properties, "productsSectionHeading", ""));
    productsSection.put("productListSections", productListSections.toArray());
                Map<String, String> configMap = ComponentUtil.getConfigMap(currentPage, "diagnosticToolResultSection");
                ProductBundlingToBinUtil.setMultiBuyProductsConfigProperties(properties, productsSection, configMap,
                        MapUtils.getBooleanValue(properties, UnileverConstants.OVERRIDE_GLOBAL_CONFIG, true),configurationService,currentPage);
                ProductBundlingToBinUtil.setMultiBuyProductsCtaLabels(httpServletRequest, productsSection, currentPage);
   }
  }
 }
 
 /**
  * Sets the articles.
  * 
  * @param resultSection
  *         the result section
  * @param searchService
  *         the search service
  * @param currentPage
  *         the current page
  * @param httpServletRequest
  *         the http servlet request
  * @param properties
  *         the properties
  */
 public void setArticlesSection(Resource resultSection, SearchService searchService, Page currentPage, SlingHttpServletRequest httpServletRequest,
   Map<String, Object> properties) {
  if (MapUtils.getBoolean(properties, "articlesFlag", false)) {
   List<Map<String, Object>> articleListSections = new ArrayList<Map<String, Object>>();
   if (CollectionUtils.isNotEmpty(tagIds)) {
    ConfigurationService configurationService = currentPage.adaptTo(ConfigurationService.class);
    int searchLimit = GlobalConfigurationUtility.getIntegerValueFromConfiguration("diagnosticTool", configurationService, currentPage,
      "articleSearchLimit");
    List<Map<String, String>> articlesProp = ComponentUtil.getNestedMultiFieldProperties(resultSection, "articles");
    Iterator<Map<String, String>> itr = articlesProp.iterator();
    while (itr.hasNext()) {
     Map<String, String> articleProp = itr.next();
     Map<String, Object> articleListSection = new HashMap<String, Object>();
     String listHeading = MapUtils.getString(articleProp, "articleListHeading", "");
     List<Map<String, Object>> articleList = DiagnosticToolHelper.getArticles(articleProp, searchService, this.tagIds, currentPage,
       httpServletRequest, searchLimit);
     articleListSection.put("listHeading", listHeading);
     articleListSection.put("articles", articleList.toArray());
     articleListSections.add(articleListSection);
    }
   }
   if (CollectionUtils.isNotEmpty(articleListSections)) {
    this.articlesSection = new HashMap<String, Object>();
    articlesSection.put("heading", MapUtils.getString(properties, "articlesSectionHeading", ""));
    articlesSection.put("articleListSections", articleListSections.toArray());
   }
  }
 }
 
 /**
  * Sets the question to summarize.
  * 
  * @param resultSection
  *         the question to summarize
  * @param questionsMap
  *         the questions map
  * @param resourceResolver
  *         the resource resolver
  * @param properties
  *         the properties
  */
 public void setQuestionToSummarize(Resource resultSection, Map<String, DiagnosticToolQuestion> questionsMap, ResourceResolver resourceResolver,
   ValueMap properties) {
  
  if (MapUtils.getBoolean(properties, "summaryOfSelectedResponses", false)) {
   questionsToSummarise = new ArrayList<Map<String, Object>>();
   List<Map<String, String>> responseSummaryList = ComponentUtil.getNestedMultiFieldProperties(resultSection, "responseSummaryJson");
   Iterator<Map<String, String>> itr = responseSummaryList.iterator();
   while (itr.hasNext()) {
    Map<String, String> responseSummary = itr.next();
    String summaryQuestionPath = responseSummary.get("questionSummary");
    if (StringUtils.isNotBlank(summaryQuestionPath)) {
     Resource diagnosticToolQuestion = resourceResolver.getResource(summaryQuestionPath);
     DiagnosticToolQuestion q = new DiagnosticToolQuestion(diagnosticToolQuestion);
     if (questionsMap.containsKey(q.getId())) {
      Map<String, Object> idMap = new HashMap<String, Object>();
      idMap.put("id", q.getId());
      questionsToSummarise.add(idMap);
     }
    }
   }
  }
 }
 
 /**
  * Gets the questions to summarise.
  * 
  * @return the questions to summarise
  */
 public List<Map<String, Object>> getQuestionsToSummarise() {
  return questionsToSummarise;
 }
 
 /**
  * Gets the personalised response.
  * 
  * @return the personalised response
  */
 public Map<String, Object> getPersonalisedResponse() {
  return personalisedResponse;
 }
 
 /**
  * Sets the personalized response.
  * 
  * @param resultSection
  *         the result section
  * @param questionsMap
  *         the questions map
  * @param currentPage
  *         the current page
  * @param httpServletRequest
  *         the sling request
  * @param properties
  *         the properties
  */
 public void setPersonalizedResponse(Resource resultSection, Map<String, DiagnosticToolQuestion> questionsMap, Page currentPage,
   SlingHttpServletRequest httpServletRequest, ValueMap properties) {
  if (MapUtils.getBoolean(properties, "personalisedText", false)) {
   this.personalisedResponse = new HashMap<String, Object>();
   List<String> selectedQuesAnsList = getQuesAnsList(questionsMap);
   List<Map<String, String>> personalisedTextRules = ComponentUtil.getNestedMultiFieldProperties(resultSection, "personalisedTextRules");
   Iterator<Map<String, String>> itr = personalisedTextRules.iterator();
   String defaultTextResponse = MapUtils.getString(properties, "defaultTextResponse", "");
   String defaultImageResponse = MapUtils.getString(properties, "defaultImageResponse", "");
   String personalisedText = defaultTextResponse;
   String defaultImageResponseAltText = MapUtils.getString(properties, "defaultImageResponseAltText", StringUtils.EMPTY);
   Image personalisedImage = new Image(defaultImageResponse, defaultImageResponseAltText, null, currentPage, httpServletRequest);
   
   TreeMap<Integer, Map<String, String>> relevanceMap = getPersonalisedRelevenceMap(selectedQuesAnsList, itr, defaultTextResponse,
     defaultImageResponse);
   if (MapUtils.isNotEmpty(relevanceMap)) {
    Map<String, String> lastMap = relevanceMap.get(relevanceMap.lastKey());
    personalisedText = lastMap.get("text");
	String altText = lastMap.get("imageAlt") != null ? lastMap.get("imageAlt") : defaultImageResponseAltText;
    personalisedImage = new Image(lastMap.get(IMAGE), altText, lastMap.get("text"), currentPage, httpServletRequest);
    
   }
   this.personalisedResponse.put("text", personalisedText);
   this.personalisedResponse.put(IMAGE, personalisedImage);
   
  }
  
 }
 
 /**
  * Gets the personalised relevence map.
  * 
  * @param selectedQuesAnsList
  *         the selected ques ans list
  * @param itr
  *         the itr
  * @param defaultTextResponse
  *         the default text response
  * @param defaultImageResponse
  *         the default image response
  * @return the personalised relevence map
  */
 private TreeMap<Integer, Map<String, String>> getPersonalisedRelevenceMap(List<String> selectedQuesAnsList, Iterator<Map<String, String>> itr,
   String defaultTextResponse, String defaultImageResponse) {
  boolean matchRuleFlag = false;
  TreeMap<Integer, Map<String, String>> relevanceMap = new TreeMap<Integer, Map<String, String>>();
  while (itr.hasNext()) {
   Map<String, String> personalisedTextRule = itr.next();
   matchRuleFlag = true;
   String personalisedQuestions = personalisedTextRule.getOrDefault("personalisedQuestions", "[]");
   Map<String, String> personalisedMap = new HashMap<String, String>();
   JSONArray arr = new JSONArray(personalisedQuestions);
   int count = 0;
   inner: for (int i = 0; i < arr.length(); ++i) {
    JSONObject obj = arr.getJSONObject(i);
    String quesAnsStr = obj.getString("question") + "." + obj.getString("answer");
    if (selectedQuesAnsList.indexOf(quesAnsStr) == -1) {
     matchRuleFlag = false;
     count = 0;
     break inner;
    } else {
     count++;
    }
   }
   if (matchRuleFlag) {
    personalisedMap.put("text", MapUtils.getString(personalisedTextRule, "personalisedTextResponse", ""));
    personalisedMap.put(IMAGE, MapUtils.getString(personalisedTextRule, "personalisedImageResponse", ""));
	personalisedMap.put("imageAlt", MapUtils.getString(personalisedTextRule, "personalisedImageResponseAltText", ""));
   } else {
    personalisedMap.put("text", defaultTextResponse);
    personalisedMap.put(IMAGE, defaultImageResponse);
   }
   relevanceMap.put(count, personalisedMap);
  }
  return relevanceMap;
 }
 
 /**
  * Gets the ques ans list.
  * 
  * @param questionsMap
  *         the questions map
  * @return the ques ans list
  */
 private List<String> getQuesAnsList(Map<String, DiagnosticToolQuestion> questionsMap) {
  List<String> quesAnsList = new ArrayList<String>();
  Iterator<String> quesItr = questionsMap.keySet().iterator();
  while (quesItr.hasNext()) {
   String questionId = quesItr.next();
   DiagnosticToolQuestion question = questionsMap.get(questionId);
   List<Answer> answerList = question.getSelectedAnswerList() != null ? question.getSelectedAnswerList() : new ArrayList<Answer>();
   Iterator<Answer> ansItr = answerList.iterator();
   while (ansItr.hasNext()) {
    Answer ans = ansItr.next();
    String str = questionId + "." + ans.getId();
    quesAnsList.add(str);
   }
  }
  return quesAnsList;
 }
 
 /**
  * Gets the products section.
  * 
  * @return the products section
  */
 public Map<String, Object> getProductsSection() {
  return productsSection;
 }
 
 /**
  * Gets the articles section.
  * 
  * @return the articles section
  */
 public Map<String, Object> getArticlesSection() {
  return articlesSection;
 }
 
 /**
  * Sets the tag ids.
  * 
  * @param tagIds
  *         the new tag ids
  */
 public void setTagIds(List<String> tagIds) {
  this.tagIds = tagIds;
 }
 
 /**
  * Gets the display type.
  * 
  * @return the display type
  */
 public String getDisplayType() {
  return displayType;
 }
 
 /**
  * Sets the display type.
  * 
  * @param properties
  *         the new display type
  */
 public void setDisplayType(Map<String, Object> properties) {
  this.displayType = MapUtils.getString(properties, "viewType", "defaultView");
 }
 
 /**
  * Gets the social share personalised text flag.
  * 
  * @return the social share personalised text flag
  */
 public boolean getSocialSharePersonalisedTextFlag() {
  return socialSharePersonalisedTextFlag;
 }
 
 /**
  * Sets the social share personalised text flag.
  * 
  * @param socialSharePersonalisedTextFlag
  *         the new social share personalised text flag
  */
 public void setSocialSharePersonalisedTextFlag(boolean socialSharePersonalisedTextFlag) {
  this.socialSharePersonalisedTextFlag = socialSharePersonalisedTextFlag;
 }
 
}
