/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.SocialTABHelper;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.SocialTabQueryDTO;

/**
 * This class prepares the data map for simple footer Component It reads the values from the dialog and passes the info as a json.
 */

@Component(description = "SocialGallery", immediate = true, metatype = true, label = "Social Gallery")
@Service(value = { SocialGalleryViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.SOCIAL_GALLERY, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class SocialGalleryViewHelperImpl extends SocialTabBaseViewHelperImpl {
 
 /**
  * logger object.
  **/
 private static final Logger LOGGER = LoggerFactory.getLogger(SocialGalleryViewHelperImpl.class);
 
 @Reference
 ConfigurationService configurationService;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper# processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("Executing SocialGalleryBaseViewHelperImpl. Inside processData method ");
  
  ResourceResolver resourceResolver = getResourceResolver(resources);
  I18n i18n = getI18n(resources);
  Resource currentResource = getCurrentResource(resources);
  Page currentPage = getCurrentPage(resources);
  
  SocialTabQueryDTO tabQuery = new SocialTabQueryDTO();
  
  int numberOfPosts = Integer.parseInt(GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
    UnileverConstants.SOCIAL_GALLERY_TAB_CONFIG_CAT, NUMBER_OF_POSTS));
  
  tabQuery.setBrandNameParameter(getBrandNameParameter(configurationService, currentPage));
  tabQuery.setConfigurationService(configurationService);
  tabQuery.setHashTags(getHashTags(currentResource));
  tabQuery.setMaxImages(0);
  tabQuery.setNoOfPost(numberOfPosts);
  tabQuery.setPage(currentPage);
  tabQuery.setPagePath(resourceResolver.map(currentPage.getPath()));
  tabQuery.setSocialChannels(getSocialChannels(getProperties(content)));
  
  Map<String, Object> socialGalleryMap = new LinkedHashMap<String, Object>();
  
  // adding social images map
  addSocialImagesMap(socialGalleryMap, resourceResolver, currentPage, currentResource, resources);
  
  // adding retrieval query parameters
  SocialTABHelper.addRetrievalQueryParametrsMap(socialGalleryMap, tabQuery);
  // adding static map
  addStaticMapProperties(socialGalleryMap, i18n);
  socialGalleryMap.put("closeLabel", i18n.get("socialGalleryTAB.closeLabel"));
  
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(getJsonNameSpace(content), socialGalleryMap);
  return data;
  
 }
 
 /**
  * Gets the social gallery posts.
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param page
  *         the page
  * @param currentResource
  *         the current resource
  * @return the social gallery posts
  */
 private void addSocialImagesMap(Map<String, Object> socialGalleryMap, ResourceResolver resourceResolver, Page page, Resource currentResource,
   Map<String, Object> resources) {
  
  List<Map<String, Object>> socialPostsList = new ArrayList<Map<String, Object>>();
  List<Map<String, String>> list = getSocialGalleryImagesMap(currentResource);
  for (Map<String, String> map : list) {
   String imagePath = getImagePath(map);
   String relatedArticlePagePath = ComponentUtil.getFullURL(resourceResolver, getPagePath(map), getSlingRequest(resources));
   
   Map<String, Object> socialPostsMap = SocialTABHelper.getImageMapWithMetadata(imagePath, page, resourceResolver, getSlingRequest(resources));
   socialPostsMap.put("readArticleCtaUrl", relatedArticlePagePath);
   socialPostsMap.put(OPEN_IN_NEW_WINDOW, getNewWindowFromMultifield(map));
   socialPostsList.add(socialPostsMap);
  }
  socialGalleryMap.put("editorSocialImages", socialPostsList.toArray());
 }
 
 /**
  * Gets the static map properties.
  * 
  * @param i18n
  *         the i18n
  * @return the static map properties
  */
 private static void addStaticMapProperties(Map<String, Object> socialGalleryMap, I18n i18n) {
  
  Map<String, Object> staticMap = new HashMap<String, Object>();
  
  staticMap.put("showMoreCtaLabel", i18n.get("socialGalleryTAB.showMore"));
  staticMap.put("readArticleCtaLabel", i18n.get("socialGalleryTAB.readFullArticle"));
  
  socialGalleryMap.put("static", staticMap);
 }
 
}
