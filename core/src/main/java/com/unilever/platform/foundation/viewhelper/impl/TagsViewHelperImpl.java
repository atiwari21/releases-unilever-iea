/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.cache.ComponentViewAndViewHelperCache;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.constants.SearchConstants;
import com.unilever.platform.aem.foundation.utils.AdaptiveUtil;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.constants.CommonConstants;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * <p>
 * Responsible for reading author input for tags component and get the get all the tags on the page and generate a list of links. Also read the site
 * configuration for minimum and maximum number configured for the links as well as from the dialog.
 * </p>
 */
@Component(description = "TagsViewHelperImpl", immediate = true, metatype = true, label = "TagsViewHelperImpl")
@Service(value = { TagsViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.TAGS, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = 5000, propertyPrivate = true) })
public class TagsViewHelperImpl extends BaseViewHelper {
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(TagsViewHelperImpl.class);
 
 private static final int NUMERIC_TWO = 2;
 private static final int NUMERIC_THREE = 3;
 private static final String SLASH_CHAR_LITERAL = "/";
 private static final String URL_BRAND = "&BrandName=";
 private static final String URL_LOCALE = "&Locale=";
 
 private static final String HEADING_TEXT = "headingText";
 
 private static final String SUB_HEADING_TEXT = "subheadingText";
 
 private static final String PARENT_PAGE_TAGS = "Parent page tags";
 
 private static final String MANUAL_TAGS = "Manual tags";
 
 private static final String MINIMUM_TAGS_TO_DISPLAY = "minTagsToDisplay";
 
 private static final String MAXIMUM_TAGS_TO_DISPLAY = "maxTagsToDisplay";
 
 private static final String BUILD_TAGS_USING = "buildTagsUsing";
 
 private static final String TAGS_HIERARCHIES_TO_USE = "tagHierarchiesToUse";
 
 private static final String TAGS_TO_DISPLAY = "tagsToDisplay";
 
 /** The component view and view helper cache. */
 /*
  * reference for cache object
  */
 @Reference
 ComponentViewAndViewHelperCache componentViewAndViewHelperCache;
 /*
  * reference for site configuration service
  */
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /**
  * The method processData overrides method is responsible validating author input and returning a list of links and minimum and maximum number for
  * the links from dialog or site configuration to be displayed on the page.
  * 
  * @param content
  *         {@link Map} of all user input received as input
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return Map holding the result data.
  */
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  LOGGER.debug("inside TagsViewHelperImpl processData");
  Map<String, Object> data = new HashMap<String, Object>();
  try {
   ResourceResolver resourceResolver = getResourceResolver(resources);
   SlingHttpServletRequest slingRequest = getSlingRequest(resources);
   Resource compResource = slingRequest.getResource();
   
   Page currentPage = getCurrentPage(resources);
   TagManager tagManager = getTagManager(resources);
   
   Map<String, String> tagsSiteConfigs = ComponentUtil.getConfigMap(currentPage, "tags");
   LOGGER.debug("Config Map for tags = " + tagsSiteConfigs);
   
   Map<String, Object> properties = compResource.getValueMap();
   LOGGER.debug("properties for tags = " + properties);
   
   String searchPagePath = getSearchPagePath(resourceResolver, currentPage, slingRequest);
   LOGGER.debug("searchPagePath for tags = " + searchPagePath);
   
   Map<String, Object> tagsFinalMap = new HashMap<String, Object>();
   String buildTagsUsing = MapUtils.getString(properties, BUILD_TAGS_USING);
   if (PARENT_PAGE_TAGS.equals(buildTagsUsing)) {
    tagsFinalMap = generateParentPageTagsMap(tagsSiteConfigs, properties, searchPagePath, resources);
   } else if (MANUAL_TAGS.equals(buildTagsUsing)) {
    tagsFinalMap = generateManualTagsMap(properties, searchPagePath, tagManager, resources);
   }
   
   tagsFinalMap.put("buildTagsUsing", buildTagsUsing);
   
   data.put(getJsonNameSpace(content), tagsFinalMap);
  } catch (Exception e) {
   LOGGER.error("Error At TagsViewHelperImpl " + ExceptionUtils.getStackTrace(e));
  }
  return data;
 }
 
 /**
  * Gets the search page path.
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param currentPage
  *         the current page
  * @return the search page path
  */
 private String getSearchPagePath(ResourceResolver resourceResolver, Page currentPage, SlingHttpServletRequest slingRequest) {
  String searchPagePath = StringUtils.EMPTY;
  try {
   searchPagePath = configurationService.getConfigValue(currentPage, "relativeURL", "searchPage");
   searchPagePath = ComponentUtil.getFullURL(resourceResolver, searchPagePath, slingRequest);
   
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Configuration Not found for Category: " + "relativeURL" + " and key: " + "searchPage", e);
  }
  return searchPagePath;
 }
 
 /**
  * Generate parent page tags map.
  * 
  * @param currentPage
  *         the current page
  * @param tagsSiteConfigs
  *         the tags site configs
  * @param properties
  *         the properties
  * @param searchPagePath
  *         the search page path
  * @param httpServletRequest
  *         the http servlet request
  * @return the map
  */
 private Map<String, Object> generateParentPageTagsMap(Map<String, String> tagsSiteConfigs, Map<String, Object> properties, String searchPagePath,
   Map<String, Object> resources) {
  Page currentPage = getCurrentPage(resources);
  SlingHttpServletRequest httpServletRequest = getSlingRequest(resources);
  
  Map<String, Object> parentPageTagsMap = new HashMap<String, Object>();
  
  List<Map<String, Object>> parentPageTagsList = new ArrayList<Map<String, Object>>();
  
  String[] parentTagsArr = getParentTagsArray(properties, tagsSiteConfigs);
  
  Map<String, Tag> tagMapForParentTags = getTagMap(parentTagsArr, currentPage);
  
  int minTagsToDisplay = MapUtils.getIntValue(properties, MINIMUM_TAGS_TO_DISPLAY);
  int maxTagsToDisplay = MapUtils.getIntValue(properties, MAXIMUM_TAGS_TO_DISPLAY);
  if (minTagsToDisplay == 0 && maxTagsToDisplay == 0) {
   minTagsToDisplay = tagsSiteConfigs.get(MINIMUM_TAGS_TO_DISPLAY) != null ? Integer.parseInt(tagsSiteConfigs.get(MINIMUM_TAGS_TO_DISPLAY)) : 0;
   maxTagsToDisplay = tagsSiteConfigs.get(MAXIMUM_TAGS_TO_DISPLAY) != null ? Integer.parseInt(tagsSiteConfigs.get(MAXIMUM_TAGS_TO_DISPLAY)) : 0;
  }
  
  parentPageTagsMap.put("minimumTagsToDisplay", minTagsToDisplay);
  parentPageTagsMap.put("maximumTagsToDisplay", maxTagsToDisplay);
  if (tagMapForParentTags.size() >= minTagsToDisplay) {
   parentPageTagsList = addTagData(searchPagePath, tagMapForParentTags, maxTagsToDisplay, httpServletRequest, parentPageTagsMap, resources);
  }
  
  commonProperties(parentPageTagsMap, properties);
  
  parentPageTagsMap.put("links", parentPageTagsList.toArray());
  return parentPageTagsMap;
 }
 
 /**
  * Generate manual tags map.
  * 
  * @param properties
  *         the properties
  * @param searchPagePath
  *         the search page path
  * @param httpServletRequest
  *         the http servlet request
  * @param resources
  * @param tagsSiteConfigs
  *         the tags site configs
  * 
  * @return the map
  */
 private Map<String, Object> generateManualTagsMap(Map<String, Object> properties, String searchPagePath, TagManager tagManager,
   Map<String, Object> resources) {
  
  Map<String, Object> manualTagsMap = new HashMap<String, Object>();
  Page currentPage = getCurrentPage(resources);
  Locale locale = BaseViewHelper.getLocale(currentPage);
  List<Map<String, Object>> manualTagsList = new ArrayList<Map<String, Object>>();
  String[] manualTagsArr = getManualTagsArray(properties);
  Map<String, Tag> tagMapForManualTags = new TreeMap<String, Tag>();
  if (manualTagsArr != null) {
   for (String tagId : manualTagsArr) {
    Tag tag = tagManager.resolve(tagId);
    if (tag != null) {
     tagMapForManualTags.put(tag.getTitle(locale), tag);
    }
   }
  }
  
  manualTagsList = addTagData(searchPagePath, tagMapForManualTags, manualTagsMap, resources);
  
  commonProperties(manualTagsMap, properties);
  
  manualTagsMap.put("links", manualTagsList.toArray());
  return manualTagsMap;
 }
 
 /**
  * Gets the parent tags array.
  * 
  * @param properties
  *         the properties
  * @param tagsSiteConfigs
  *         the tags site configs
  * @return the parent tags array
  */
 private String[] getParentTagsArray(Map<String, Object> properties, Map<String, String> tagsSiteConfigs) {
  
  String[] parentTagsArr = ComponentUtil.getPropertyValueArray(properties, TAGS_HIERARCHIES_TO_USE);
  String[] defaultParentTagsarr = tagsSiteConfigs.get(TAGS_HIERARCHIES_TO_USE) != null ? tagsSiteConfigs.get(TAGS_HIERARCHIES_TO_USE).split(
    CommonConstants.COMMA_CHAR) : new String[] {};
  
  parentTagsArr = parentTagsArr != null && parentTagsArr.length > 0 ? parentTagsArr : defaultParentTagsarr;
  LOGGER.debug("Value of ParentTagsArr = " + parentTagsArr);
  
  return parentTagsArr;
 }
 
 /**
  * Gets the manual tags array.
  * 
  * @param properties
  *         the properties
  * @return the manual tags array
  */
 private String[] getManualTagsArray(Map<String, Object> properties) {
  String[] tagsArrayFromDialog = ComponentUtil.getPropertyValueArray(properties, TAGS_TO_DISPLAY);
  
  tagsArrayFromDialog = tagsArrayFromDialog != null && tagsArrayFromDialog.length > 0 ? tagsArrayFromDialog : new String[] {};
  
  LOGGER.debug("Value of TagsArrayFromDialog = " + tagsArrayFromDialog);
  return tagsArrayFromDialog;
 }
 
 /**
  * Gets the tag map.
  * 
  * @param parentTagsArr
  *         the parent tags arr
  * @param currentPage
  *         the current page
  * @return the tag map
  */
 private Map<String, Tag> getTagMap(String[] parentTagsArr, Page currentPage) {
  
  Map<String, Tag> tagMap = new TreeMap<String, Tag>();
  Tag[] tags = currentPage.getTags();
  if (parentTagsArr != null) {
   for (String parentTagId : parentTagsArr) {
    if (tags != null) {
     tagMap = getTags(tagMap, tags, parentTagId, BaseViewHelper.getLocale(currentPage));
    }
   }
  }
  return tagMap;
 }
 
 /**
  * Gets the tags.
  * 
  * @param tagMap
  *         the tag map
  * @param tags
  *         the tags
  * @param parentTagId
  *         the parent tag id
  * @param locale
  *         the locale of the page
  * @return the tags
  */
 private Map<String, Tag> getTags(Map<String, Tag> tagMap, Tag[] tags, String parentTagId, Locale locale) {
  for (Tag tag : tags) {
   if (tag != null) {
    String tagId = tag.getTagID();
    // all the tags will be added to the map which are
    // associated to the page and also falls in the hierarchy of the parent selected tag
    // in the dialog
    if (tagId.startsWith(parentTagId)) {
     tagMap.put(tag.getTitle(locale), tag);
    }
   }
  }
  return tagMap;
 }
 
 /**
  * Adds the tag data.
  * 
  * @param searchPagePath
  *         the search page path
  * @param tagMap
  *         the tag map
  * @param max
  *         the max
  * @param httpServletRequest
  *         the http servlet request
  * @return the list
  */
 private List<Map<String, Object>> addTagData(String searchPagePath, Map<String, Tag> tagMap, int max, SlingHttpServletRequest httpServletRequest,
   Map<String, Object> parentMap, Map<String, Object> resources) {
  List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
  Iterator<String> tagItr = tagMap.keySet().iterator();
  List<String> tagsIdList = new ArrayList<String>();
  int counter = 0;
  while (tagItr.hasNext() && counter < max) {
   String tagtitle = tagItr.next();
   Tag tag = tagMap.get(tagtitle);
   list.add(getTagDetails(searchPagePath, tagtitle, httpServletRequest));
   if (tag != null) {
    tagsIdList.add(tag.getTagID());
   }
   counter++;
  }
  boolean isEnabledFeatureTagInteraction = ComponentUtil.isFeatureTagInteractionEnabled(getCurrentPage(resources));
  parentMap.put(UnileverConstants.IS_ENABLED_FEATURE_TAG_INTERACTION, isEnabledFeatureTagInteraction);
  if (isEnabledFeatureTagInteraction) {
   String[] tagsIdArray = new String[tagsIdList.size()];
   tagsIdArray = tagsIdList.toArray(tagsIdArray);
   parentMap.put("interactionFeatureTags", ComponentUtil.getFeatureTagList(resources, tagsIdArray));
  }
  return list;
 }
 
 /**
  * Adds the tag data.
  * 
  * @param searchPagePath
  *         the search page path
  * @param tagMap
  *         the tag map
  * @param httpServletRequest
  *         the http servlet request
  * @return the list
  */
 private List<Map<String, Object>> addTagData(String searchPagePath, Map<String, Tag> tagMap, Map<String, Object> parentMap,
   Map<String, Object> resources) {
  List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
  SlingHttpServletRequest httpServletRequest = getSlingRequest(resources);
  Iterator<String> tagItr = tagMap.keySet().iterator();
  List<String> tagsIdList = new ArrayList<String>();
  while (tagItr.hasNext()) {
   String tagTitle = tagItr.next();
   list.add(getTagDetails(searchPagePath, tagTitle, httpServletRequest));
   Tag tag = tagMap.get(tagTitle);
   if (tag != null) {
    tagsIdList.add(tag.getTagID());
   }
  }
  boolean isEnabledFeatureTagInteraction = ComponentUtil.isFeatureTagInteractionEnabled(getCurrentPage(resources));
  parentMap.put(UnileverConstants.IS_ENABLED_FEATURE_TAG_INTERACTION, isEnabledFeatureTagInteraction);
  if (isEnabledFeatureTagInteraction) {
   String[] tagsIdArray = new String[tagsIdList.size()];
   tagsIdArray = tagsIdList.toArray(tagsIdArray);
   parentMap.put("interactionFeatureTags", ComponentUtil.getFeatureTagList(resources, tagsIdArray));
  }
  return list;
 }
 
 /**
  * Gets the tag details.
  * 
  * @param searchPagePath
  *         the search page path
  * @param tagItr
  *         the tag itr
  * @param httpServletRequest
  *         the http servlet request
  * @return the tag details
  */
 private Map<String, Object> getTagDetails(String searchPagePath, String tagTitle, SlingHttpServletRequest httpServletRequest) {
  Map<String, Object> map = new HashMap<String, Object>();
  map.put("text", tagTitle);
  
  String url = searchPagePath;
  String requestPath = httpServletRequest.getRequestPathInfo().getResourcePath();
  String[] pagePathArr = requestPath.split(SLASH_CHAR_LITERAL);
  String locale = pagePathArr.length > NUMERIC_THREE ? pagePathArr[NUMERIC_THREE] : "global";
  String brand = pagePathArr[NUMERIC_TWO];
  try {
   if (AdaptiveUtil.isAdaptive(httpServletRequest)) {
    url = searchPagePath + "?" + SearchConstants.SEARCH_KEYWORD + "=" + URLEncoder.encode(tagTitle, "UTF-8") + URL_BRAND + brand + URL_LOCALE
      + locale;
   } else {
    url = searchPagePath + "?" + SearchConstants.SEARCH_KEYWORD + "=" + URLEncoder.encode(tagTitle, "UTF-8");
   }
  } catch (UnsupportedEncodingException e) {
   LOGGER.error("UnsupportedEncodingException in Tags ", e);
  }
  map.put("url", url);
  return map;
  
 }
 
 /**
  * Common properties.
  * 
  * @param finalProperties
  *         the final properties
  * @param properties
  *         the properties
  * @return the map
  */
 private Map<String, Object> commonProperties(Map<String, Object> finalProperties, Map<String, Object> properties) {
  
  String headingText = MapUtils.getString(properties, HEADING_TEXT, StringUtils.EMPTY);
  String subHeadingText = MapUtils.getString(properties, SUB_HEADING_TEXT, StringUtils.EMPTY);
  
  finalProperties.put("headingText", headingText);
  finalProperties.put("subHeadingText", subHeadingText);
  LOGGER.debug("created Map for common fields used by tags, size of this map is " + finalProperties.size());
  return finalProperties;
  
 }
 
}
