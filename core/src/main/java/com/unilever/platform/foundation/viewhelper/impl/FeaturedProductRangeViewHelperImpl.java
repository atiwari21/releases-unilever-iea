/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.core.service.SearchService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.constants.CommonConstants;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.Image;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * <p>
 * Responsible for reading author input for featured product range component and returns a featured product.
 * 
 * </p>
 */
@Component(description = "FeaturedProductRangeViewHelperImpl", immediate = true, metatype = true, label = "FeaturedProductRangeViewHelperImpl")
@Service(value = { FeaturedProductRangeViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.FEATURED_PRODUCT_RANGE, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class FeaturedProductRangeViewHelperImpl extends BaseViewHelper {
 
 /**
  * Constant for retrieving anchor link navigation label.
  */
 private static final String ANCHOR_MAP = "anchorLinkNavigation";
 
 /**
  * Constant for retrieving featured product type.
  */
 private static final String FEATURED_TYPE = "featuredProductRangeType";
 
 /**
  * Constant for retrieving product name label.
  */
 
 private static final String PRODUCT_NAME = "productName";
 
 /**
  * Constant for retrieving product description label.
  */
 private static final String PRODUCT_COPY = "productShortCopy";
 
 /**
  * Constant for setting default text position.
  */
 private static final String PRODUCT_TEXT_POSITION = "Center";
 
 /**
  * Constant for setting image title label.
  */
 
 private static final String TITLE = "title";
 
 /**
  * Constant for setting extension label.
  */
 
 /**
  * Constant for setting featured product type auto.
  */
 
 private static final String AUTO = "Auto";
 
 /**
  * Constant for retrieving mannual product page.
  */
 
 private static final String PRODUCT_PAGE = "productRangePage";
 
 /**
  * Constant for retrieving background.
  */
 
 private static final String FEATURED_BACKGROUND = "backgroundRangeImage";
 
 /**
  * Constant for retrieving alt text for background.
  */
 
 private static final String ALT_BACKGROUND = "altBackgroundImage";
 
 /**
  * Constant for retrieving type label.
  */
 
 private static final String TYPE = "type";
 
 /**
  * Constant for setting cta label.
  */
 
 private static final String LABEL_TEXT = "labelText";
 
 /**
  * Constant for setting navigation label.
  */
 
 private static final String NAVIGATION_URL = "navigationURL";
 
 /**
  * Constant for retrieving product image label.
  */
 private static final String PRODUCT_IMAGE = "productImage";
 
 /**
  * Constant for setting product info position.
  */
 
 private static final String PRODUCT_INFO_POSITION = "imagePosition";
 
 /**
  * Constant for setting product info position.
  */
 
 private static final String TEXT_POSITION = "textPosition";
 
 /**
  * Constant for setting alt text for image.
  */
 
 private static final String ALT_IMAGE = "altImage";
 
 /**
  * Constant for setting option for open link in new window.
  */
 
 private static final String NEW_WINDOW = "newWindow";
 
 /**
  * Constant for setting option for open link in new window label for map.
  */
 
 private static final String OPEN_IN_NEW_WINDOW = "openInWindow";
 
 /**
  * Constant for setting cta map.
  */
 
 private static final String CTA = "cta";
 
 /**
  * logger object.
  **/
 
 @Reference
 ConfigurationService configurationService;
 
 /** The search service. */
 @Reference
 SearchService searchService;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(FeaturedProductRangeViewHelperImpl.class);
 
 /**
  * The method processData overrides method is responsible validating author input and returning a map Map can be accessed in JSP in consistent
  * way.This method also validates the link url provided by author.
  * 
  * @param content
  *         , - {@link Map} of all user input received as input <code>ViewHelper.onGetData</code> method
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return {@link Map} holding the data
  */
 @Override
 @SuppressWarnings("unchecked")
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.info("Featured product range View Helper #processData called for processing fixed list.");
  Map<String, Object> featuredProductRangeProperties = (Map<String, Object>) content.get(PROPERTIES);
  Map<String, Object> featuredProductRangeFinalMap = new LinkedHashMap<String, Object>();
  ResourceResolver resourceResolver = (ResourceResolver) resources.get(ProductConstants.RESOURCE_RESOLVER);
  
  String jsonNameSpace = MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE);
  
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  
  SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) resources.get(ProductConstants.REQUEST);
  Resource compResource = slingRequest.getResource();
  Page currentPage = pageManager.getContainingPage(compResource);
  
  String productType = MapUtils.getString(featuredProductRangeProperties, FEATURED_TYPE);
  
  // checking type of data
  if (StringUtils.isNotEmpty(productType)) {
   
   if (AUTO.equals(productType)) {
    featuredProductRangeFinalMap = getProductsAutomatic(content, featuredProductRangeProperties, pageManager, resourceResolver, currentPage,
      resources);
   } else {
    featuredProductRangeFinalMap = getProductsManual(jsonNameSpace, resourceResolver, featuredProductRangeProperties, currentPage, resources);
   }
  } else {
   List<Map<String, Object>> blankFeaturedProductRangeList = new ArrayList<Map<String, Object>>();
   featuredProductRangeFinalMap.put(jsonNameSpace, blankFeaturedProductRangeList.toArray());
  }
  return featuredProductRangeFinalMap;
 }
 
 /**
  * This method used to get required information about product automatically by reading product page of user selection.
  * 
  * @param content
  *         the content
  * @param featuredProductProperties
  *         the featured product properties
  * @param pageManager
  *         the page manager
  * @param resourceResolver
  *         the resource resolver
  * @param currentPage
  *         the current page
  * @return the products automatic
  */
 private Map<String, Object> getProductsAutomatic(Map<String, Object> content, Map<String, Object> featuredProductProperties,
   PageManager pageManager, ResourceResolver resourceResolver, Page currentPage, Map<String, Object> resources) {
  Map<String, Object> properties = new LinkedHashMap<String, Object>();
  Map<String, Object> ctaMap = new LinkedHashMap<String, Object>();
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  String tagPage = MapUtils.getString(featuredProductProperties, PRODUCT_PAGE);
  properties = commonProperties(featuredProductProperties, currentPage, resources);
  LOGGER.debug("created Map for common fields used by featured product range auto and manual,size of this map is " + properties.size());
  Page currentProductPage = pageManager.getContainingPage(tagPage);
  if (currentProductPage != null) {
   Map<String, Object> currentRangePageProperties = currentProductPage.getProperties();
   String rangeName = MapUtils.getString(currentRangePageProperties, UnileverConstants.TEASER_TITLE);
   String rangeCopy = MapUtils.getString(currentRangePageProperties, UnileverConstants.TEASER_COPY);
   String rangeImage = MapUtils.getString(currentRangePageProperties, UnileverConstants.TEASER_IMAGE);
   String rangeImageAltText = MapUtils.getString(currentRangePageProperties, UnileverConstants.TEASER_IMAGE_ALT_TEXT);
   Map<String, Object> rangeImageMap = new LinkedHashMap<String, Object>();
   if (StringUtils.isNotEmpty(rangeImage)) {
    rangeImageMap = getRangeImageProperties(rangeImage, rangeImageAltText, currentPage, resources);
    LOGGER.debug("created Map for featured product range image auto and size of this Map is " + rangeImageMap.size());
   }
   String ctaLabel = MapUtils.getString(featuredProductProperties, TITLE);
   String openInNewWindow = checkNullValues(MapUtils.getString(featuredProductProperties, NEW_WINDOW));
   String rangePagePath = ComponentUtil.getFullURL(resourceResolver, tagPage, getSlingRequest(resources));
   if (StringUtils.isNotEmpty(rangeName)) {
    properties.put(PRODUCT_NAME, rangeName);
    properties.put(PRODUCT_COPY, rangeCopy);
    properties.put(PRODUCT_IMAGE, rangeImageMap);
    if (StringUtils.isNotEmpty(rangePagePath)) {
     ctaMap.put(LABEL_TEXT, ctaLabel);
     ctaMap.put(NAVIGATION_URL, rangePagePath);
    }
   }
   
   if (openInNewWindow.equals(CommonConstants.BOOLEAN_TRUE)) {
    ctaMap.put(OPEN_IN_NEW_WINDOW, CommonConstants.BOOLEAN_TRUE);
   } else {
    ctaMap.put(OPEN_IN_NEW_WINDOW, "false");
   }
   properties.put(CTA, ctaMap);
  }
  data.put(MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE), properties);
  return data;
 }
 
 /**
  * This method used to get all product information from the product page and user entries which user selects.
  * 
  * @param jsonNameSpace
  *         the json name space
  * @param resourceResolver
  *         the resource resolver
  * @param properties
  *         the properties
  * @param currentPage
  *         the current page
  * @return the products manual
  */
 private Map<String, Object> getProductsManual(String jsonNameSpace, ResourceResolver resourceResolver, Map<String, Object> properties,
   Page currentPage, Map<String, Object> resources) {
  
  Map<String, Object> finalProperties = new LinkedHashMap<String, Object>();
  
  Map<String, Object> productImageMap = getProductImageProperties(properties, currentPage, resources);
  LOGGER.debug("created featured product range image manual map ,size of the Map is " + productImageMap.size());
  String productName = MapUtils.getString(properties, PRODUCT_NAME);
  String productCopy = checkNullValues(MapUtils.getString(properties, PRODUCT_COPY));
  // cta map
  Map<String, Object> ctaMap = new LinkedHashMap<String, Object>();
  String ctaText = MapUtils.getString(properties, TITLE);
  String pagePath = MapUtils.getString(properties, NAVIGATION_URL);
  String navigationURL = ComponentUtil.getFullURL(resourceResolver, pagePath, getSlingRequest(resources));
  String openInNewWindow = checkNullValues(MapUtils.getString(properties, NEW_WINDOW));
  ctaMap.put(LABEL_TEXT, ctaText);
  ctaMap.put(NAVIGATION_URL, navigationURL);
  if (openInNewWindow.equals(CommonConstants.BOOLEAN_TRUE)) {
   ctaMap.put(OPEN_IN_NEW_WINDOW, CommonConstants.BOOLEAN_TRUE);
  } else {
   ctaMap.put(OPEN_IN_NEW_WINDOW, "false");
  }
  
  finalProperties = commonProperties(properties, currentPage, resources);
  LOGGER.debug("created Map for common fields used by featured product range auto and manual,size of this map is " + finalProperties.size());
  finalProperties.put(PRODUCT_IMAGE, productImageMap);
  finalProperties.put(PRODUCT_NAME, productName);
  finalProperties.put(PRODUCT_COPY, productCopy);
  finalProperties.put(CTA, ctaMap);
  Map<String, Object> dataManual = new LinkedHashMap<String, Object>();
  dataManual.put(jsonNameSpace, finalProperties);
  return dataManual;
 }
 
 /**
  * Common properties.
  * 
  * @param featuredProductRangeProperties
  *         the featured product range properties
  * @param page
  *         the page
  * @return the map
  */
 private Map<String, Object> commonProperties(Map<String, Object> featuredProductRangeProperties, Page page, Map<String, Object> resources) {
  Map<String, Object> featuredProductRangeAutoMap = new LinkedHashMap<String, Object>();
  String productType = MapUtils.getString(featuredProductRangeProperties, FEATURED_TYPE);
  String productImagePosition = MapUtils.getString(featuredProductRangeProperties, PRODUCT_INFO_POSITION);
  String themeBackground = MapUtils.getString(featuredProductRangeProperties, CommonConstants.THEME);
  if (StringUtils.isEmpty(productImagePosition)) {
   productImagePosition = "Left";
  }
  String backgroundImage = MapUtils.getString(featuredProductRangeProperties, FEATURED_BACKGROUND);
  backgroundImage = checkNullValues(backgroundImage);
  
  Map<String, Object> backgroundImageMap = getImageProperties(featuredProductRangeProperties, page, getSlingRequest(resources));
  LOGGER.debug("created map for featured product range background image, size of the image is " + backgroundImageMap.size());
  Map<String, String> anchorLinkMap = ComponentUtil.getAnchorLinkMap(featuredProductRangeProperties);
  featuredProductRangeAutoMap.put(ANCHOR_MAP, anchorLinkMap);
  featuredProductRangeAutoMap.put(TYPE, productType.toLowerCase());
  featuredProductRangeAutoMap.put(CommonConstants.THEME, themeBackground.toLowerCase());
  featuredProductRangeAutoMap.put(TEXT_POSITION, PRODUCT_TEXT_POSITION);
  featuredProductRangeAutoMap.put("productImagePosition", productImagePosition);
  
  if (MapUtils.isNotEmpty(backgroundImageMap)) {
   featuredProductRangeAutoMap.put("backgroundImage", backgroundImageMap);
  }
  return featuredProductRangeAutoMap;
  
 }
 
 /**
  * This method returns an empty string if the key is null or undefined otherwise it returns key.
  * 
  * @param key
  *         the key
  * @return key
  */
 private String checkNullValues(Object key) {
  String value = "";
  if (null == key) {
   value = "";
  } else if ("".equals(key)) {
   value = "";
  } else {
   value = key.toString();
  }
  
  return value;
  
 }
 
 /**
  * This method is used to get product image properties(in case of manual) like path,alt text for image extension etc.
  * 
  * @param properties
  *         the properties
  * @param page
  *         the page
  * @return the product image properties
  */
 private Map<String, Object> getProductImageProperties(Map<String, Object> properties, Page page, Map<String, Object> resources) {
  Map<String, Object> productImageMap = new LinkedHashMap<String, Object>();
  String altImage = checkNullValues(MapUtils.getString(properties, ALT_IMAGE));
  String productImage = checkNullValues(MapUtils.getString(properties, PRODUCT_IMAGE));
  Image productBackgroundImage = new Image(productImage, altImage, altImage, page, getSlingRequest(resources));
  productImageMap = productBackgroundImage.convertToMap();
  return productImageMap;
  
 }
 
 /**
  * This method is used to get product range image properties(in case of auto) like path,alt text for image extension etc.
  * 
  * @param rangeImage
  *         the range image
  * @param rangeImageAltText
  * @param page
  *         the page
  * @return the range image properties
  */
 private Map<String, Object> getRangeImageProperties(String rangeImage, String rangeImageAltText, Page page, Map<String, Object> resources) {
  Image productRangeImage = new Image(rangeImage, rangeImageAltText, "", page, getSlingRequest(resources));
  return productRangeImage.convertToMap();
  
 }
 
 /**
  * This method is used to get background image properties like path,alt text for image extension etc.
  * 
  * @param properties
  *         the properties
  * @param page
  *         the page
  * @return the image properties
  */
 private Map<String, Object> getImageProperties(Map<String, Object> properties, Page page, SlingHttpServletRequest request) {
  Map<String, Object> backgroundImageMap = new LinkedHashMap<String, Object>();
  String altImage = checkNullValues(MapUtils.getString(properties, ALT_BACKGROUND));
  String backgroundImage = checkNullValues(MapUtils.getString(properties, FEATURED_BACKGROUND));
  Image productBackgroundImage = new Image(backgroundImage, altImage, altImage, page, request);
  backgroundImageMap = productBackgroundImage.convertToMap();
  return backgroundImageMap;
  
 }
}
