/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.api.Product;
import com.adobe.cq.commerce.common.CommerceHelper;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.i18n.I18n;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfiguration;
import com.unilever.platform.aem.foundation.constants.SearchConstants;
import com.unilever.platform.aem.foundation.core.dto.SearchDTO;
import com.unilever.platform.aem.foundation.core.dto.TaggingSearchCriteria;
import com.unilever.platform.aem.foundation.core.service.SearchParam;
import com.unilever.platform.aem.foundation.core.service.SearchService;
import com.unilever.platform.aem.foundation.utils.AdaptiveUtil;
import com.unilever.platform.foundation.components.ProductResultComparator;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.ProductBundlingToBinUtil;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.constants.ContainerTagConstants;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.kritique.config.KritiqueReviewsCommentsService;
import com.unilever.platform.foundation.template.dto.KritiqueDTO;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;
import com.unilever.platform.foundation.viewhelper.constants.ResourceCommonConstants;
import com.unilever.platform.foundation.viewhelper.productoverview.config.ConfigureProductImageRenditionService;

/**
 * <p>
 * Responsible for reading author input for prduct copy component and generating a list of collapsible sections setting in pageContext for the view.
 * Input can be manual or reading property attributes from product data.
 * </p>
 */
@Component(description = "ProductListingViewHelperImpl", immediate = true, metatype = true, label = "ProductListingViewHelperImpl")
@Service(value = { ProductListingViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.PRODUCT_LISTING, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class ProductListingViewHelperImpl extends BaseViewHelper {

 @Reference
 ConfigurationService configurationService;
 
 /** The search service. */
 @Reference
 SearchService searchService;
 
 /** The configure product image rendition service. */
 @Reference
 ConfigureProductImageRenditionService configureProductImageRenditionService;
 
 @Reference
 KritiqueReviewsCommentsService kritiqueReviewsCommentsService;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ProductListingViewHelperImpl.class);
 
 /** The Constant PRODUCT_COUNT. */
 private static final String PRODUCT_COUNT = "totalCount";
 
 /** The Constant FILTER_ONE_LIST. */
 private static final String FILTER_ONE_LIST = "list";
 
 /** The Constant FILTER_ONE. */
 private static final String FILTER_ONE = "filterOne";
 
 /** The Constant FILTER_ONE_NAMESPACE. */
 private static final String FILTER_ONE_NAMESPACE = "filterOneNamespace";
 
 /** The Constant FILTER_TWO_NAMESPACE. */
 private static final String FILTER_TWO_NAMESPACE = "filterTwoNamespace";
 
 /** The Constant FILTER_TWO_LIST. */
 private static final String FILTER_TWO_LIST = "list";
 
 /** The Constant FILTER_TWO. */
 private static final String FILTER_TWO = "filterTwo";
 
 /** The Constant FILTER_ONE_LABEL. */
 private static final String FILTER_LABEL = "label";
 
 /** The Constant COMPONENT_TITLE. */
 private static final String COMPONENT_TITLE = "title";
 
 /** The Constant FILTER_DEFAULT. */
 private static final String FILTER_DEFAULT = "defaultOption";
 
 /** The Constant START_RECORD_NO. */
 private static final String START_RECORD_NO = "startRecordNo";
 
 /** The Constant MAX_RESULT_COUNT. */
 private static final String MAX_RESULT_COUNT = "maxResultCount";
 
 /** The Constant globalConfigCat. */
 private static final String GLOBAL_CONFIG_CAT = "productListing";
 
 /** The Constant SEARCH_CRITERIA. */
 private static final String SEARCH_CRITERIA = "searchCriteria";
 
 /** The Constant FILTER_NOT_APPLIED. */
 private static final String FILTER_NOT_APPLIED = "filterNotApplied";
 
 /** The Constant START_RECORD_NO_VAL. */
 private static final String START_RECORD_NO_VAL = "0";
 
 /** The Constant MAX_RESULT_COUNT_VAL. */
 private static final String MAX_RESULT_COUNT_VAL = "50";
 
 /** The Constant START_RECORD. */
 private static final int START_RECORD = 0;
 
 /** The Constant DISPLAY_COUNT_VAL. */
 private static final String DISPLAY_COUNT_VAL = "displayCountVal";
 
 /** The Constant FILTERONE_DEFAULT_VAL. */
 private static final String FILTERONE_DEFAULT_VAL = "productListing.filterOneDefaultVal";
 
 /** The Constant FILTERONE_LABEL. */
 private static final String FILTERONE_LABEL = "productListing.filterOneLabel";
 
 /** The Constant FILTERTWO_DEFAULT_VAL. */
 private static final String FILTERTWO_DEFAULT_VAL = "productListing.filterTwoDefaultVal";
 
 /** The Constant FILTERTWO_LABEL. */
 private static final String FILTERTWO_LABEL = "productListing.filterTwoLabel";
 
 /** The Constant FILTER_FLAG. */
 private static final String FILTER_FLAG = "isFilterContainsOneValue";
 
 /** The Constant PRODUCT_COUNT_LABEL. */
 private static final String PRODUCT_COUNT_LABEL = "productListing.productCountLabel";
 
 /** The Constant FILTER_PRODUCTS_LABEL. */
 private static final String FILTER_PRODUCTS_LABEL = "productListing.filterProductsLabel";
 
 /** The Constant HIDE_FILTERS_LABEL. */
 private static final String HIDE_FILTERS_LABEL = "productListing.hideFiltersLabel";
 
 /** The Constant MAX_VAL. */
 private static final int MAX_VAL = 65;
 
 /** The Constant FP_MORE_BUTTON. */
 private static final String FP_MORE_BUTTON = "showMoreButton";
 
 /** The Constant SHOW_MORE_SELECTOR. */
 private static final String SHOW_MORE_SELECTOR = "fullproductlisting";
 
 /** The Constant HTML_EXTENSION. */
 private static final String HTML_EXTENSION = ".html";
 
 /** The Constant HTML_FULL_LISTING_EXTENTION. */
 private static final String HTML_FULL_LISTING_EXTENTION = ".fullproductlisting.html";
 
 /** The Constant TWO. */
 private static final int TWO = 2;
 
 /** The Constant SIX. */
 private static final int SIX = 6;
 
 /** The Constant FIFTY. */
 private static final int FIFTY = 50;
 
 /** The ConstantFILTER_ONE_DEFAULT_LABEL */
 private static final String FILTER_ONE_DEFAULT_LABEL = "filterOneDefaultLabel";
 
 /** The ConstantFILTER_TWO_DEFAULT_LABEL */
 private static final String FILTER_TWO_DEFAULT_LABEL = "filterTwoDefaultLabel";
 
 /** The Constant DEBUG_STRING. */
 private static final String DEBUG_STRING = "could not find filterNamespaces key in productListing category";
 private static final String RATING_REVIEWS = "ratingReviews";
 private static final String BAZAARVOICE = "bazaarvoice";
 private static final String KRITIQUE = "kritique";
 private static final String SERVICE_PROVIDER_NAME = "serviceProviderName";
 private static final String SHOP_NOW = "shopNow";
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData (java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("Product Listing View Helper #processData called .");
  
  SlingHttpServletRequest slingRequest = getSlingRequest(resources);
  Page currentPge = getCurrentPage(resources);
  Map<String, Object> dialogProperties = getProperties(content);
  
  Map<String, Object> properties = new HashMap<String, Object>();
  List<Map<String, Object>> productPropertiesList = new ArrayList<Map<String, Object>>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  String jsonNameSapce = MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE);
  Boolean showMoreButton = true;
  

  Map<String,String> configMap = ComponentUtil.getConfigMap(currentPge, GLOBAL_CONFIG_CAT);
  // check for ajax request
  RequestPathInfo reqPathInfo = slingRequest.getRequestPathInfo();
  String[] selectors = reqPathInfo.getSelectors();
  Boolean isAjax = isAjax(selectors);
  // is AJAX request
  if (isAjax) {
   List<TaggingSearchCriteria> pageTagIdListForOrTags = new ArrayList<TaggingSearchCriteria>();
   
   if (slingRequest.getParameter(FILTER_ONE) != null) {
    pageTagIdListForOrTags = addFilterCriteriaToForTags(slingRequest, pageTagIdListForOrTags, FILTER_ONE, SearchConstants.SEARCH_OR, null);
   }
   if (slingRequest.getParameter(FILTER_TWO) != null) {
    
    pageTagIdListForOrTags = addFilterCriteriaToForTags(slingRequest, pageTagIdListForOrTags, FILTER_TWO, SearchConstants.SEARCH_OR, null);
   }
   int startRecordNo = slingRequest.getParameter(START_RECORD_NO) != null ? Integer.parseInt(slingRequest.getParameter(START_RECORD_NO)) : 1;
   int maxResultCount = slingRequest.getParameter(MAX_RESULT_COUNT) != null ? Integer.parseInt(slingRequest.getParameter(MAX_RESULT_COUNT)) : MAX_VAL;
   if (pageTagIdListForOrTags != null && !pageTagIdListForOrTags.isEmpty()) {
    List<String> productPagePathList = getFilteredProductSet(pageTagIdListForOrTags, startRecordNo, maxResultCount, resources);
    productPagePathList = ProductBundlingToBinUtil.getProductsListForMultipleBuy(productPagePathList, currentPge, configurationService);
    productPropertiesList = ProductHelper.getProductListWithTag(productPagePathList, resources, jsonNameSapce, null);
    properties.put(ProductConstants.PRODUCT_LIST_MAP, productPropertiesList.toArray());
    properties.put(PRODUCT_COUNT, productPagePathList.size());
   } else {
    properties.put(PRODUCT_COUNT, 0);
    properties.put(ProductConstants.PRODUCT_LIST_MAP, productPropertiesList.toArray());
   }
   properties.put(SEARCH_CRITERIA, getSearchCriteriaMap(slingRequest, isAjax));
   
  } else {
   setProductsProperties(resources, properties, productPropertiesList, content);
  }
  
  showMoreButton = isShowMoreButtonEnabled(showMoreButton, selectors);
  properties.put(FP_MORE_BUTTON, showMoreButton);
  if(StringUtils.isNotBlank(MapUtils.getString(dialogProperties, UnileverConstants.RANDOM_NUMBER))){
   ProductBundlingToBinUtil.setMultiBuyProductsConfigProperties(dialogProperties,properties, configMap,
           MapUtils.getBooleanValue(dialogProperties, UnileverConstants.OVERRIDE_GLOBAL_CONFIG, false), configurationService, currentPge);
   ProductBundlingToBinUtil.setMultiBuyProductsCtaLabels(resources, properties);
  }
  Map<String, Object> data = new HashMap<String, Object>();
  data.put(jsonNameSapce, properties);
  return data;
 }

/**returns false in case selector contains SHOW_MORE_SELECTOR
 * @param showMoreButton
 * @param selectors
 * @return
 */
 private boolean isShowMoreButtonEnabled(Boolean showMoreButton, String[] selectors) {
  boolean localShowMoreButton = showMoreButton;
  for (String selector : selectors) {
   if (selector.equals(SHOW_MORE_SELECTOR)) {
    localShowMoreButton = false;
    break;
       }
      }
  return localShowMoreButton;
}
    
    /**sets products properties
     * @param resources
     * @param properties
     * @param productPropertiesList
     * @param content
     */
 private void setProductsProperties(Map<String, Object> resources, Map<String, Object> properties,
            List<Map<String, Object>> productPropertiesList, Map<String, Object> content) {
        // not an AJAX request
  Map<String, Object> dialogProperties = getProperties(content);
  PageManager pageManager = getPageManager(resources);
  TagManager tagManager = getTagManager(resources);
  I18n i18n = getI18n(resources);
  Page currentPge = getCurrentPage(resources);
  ResourceResolver resourceResolver = getResourceResolver(resources);
  SlingHttpServletRequest slingRequest = getSlingRequest(resources);
  String filterOneDefaultVal = StringUtils.EMPTY;
  String filterTwoDefaultVal = StringUtils.EMPTY;
  filterOneDefaultVal = MapUtils.getString(dialogProperties, FILTER_ONE_DEFAULT_LABEL, i18n.get(FILTERONE_DEFAULT_VAL));
  filterTwoDefaultVal = MapUtils.getString(dialogProperties, FILTER_TWO_DEFAULT_LABEL, i18n.get(FILTERTWO_DEFAULT_VAL));
  String[] filternameSpaces = getFilterNameSpace(currentPge, dialogProperties);
  String filterOneNamespace = StringUtils.isNotBlank(filternameSpaces[0]) ? filternameSpaces[0] : StringUtils.EMPTY;
  String filterTwoNamespace = StringUtils.isNotBlank(filternameSpaces[1]) ? filternameSpaces[1] : StringUtils.EMPTY;
  String filterOneLabel = i18n.get(FILTERONE_LABEL);
  String filterTwoLabel = i18n.get(FILTERTWO_LABEL);
  Map<String, Object> filterOneMapProperties = new HashMap<String, Object>();
  Map<String, Object> filterTwoMapProperties = new HashMap<String, Object>();
  Map<String, Object> masterFilterProperties = new HashMap<String, Object>();
  Set<Map<String, String>> filterOneList = new HashSet<Map<String, String>>();
  Set<Map<String, String>> filterTwoList = new HashSet<Map<String, String>>();
  String featureTag = getValueFromConfig(currentPge, GLOBAL_CONFIG_CAT, "featureTag");
  String[] featuredTags = StringUtils.isNotBlank(featureTag) ? featureTag.split(CommonConstants.COMMA) : new String[] {};
  String includeSegmentRulesProp = MapUtils.getString(dialogProperties, "includeSegmentRules", StringUtils.EMPTY);
  boolean includeSegmentRules = ("true").equalsIgnoreCase(includeSegmentRulesProp) ? true : false;
  featuredTags = ComponentUtil.getUpdatedIncludedArrayFromResources(resources, featuredTags, includeSegmentRules);
  String customizableTag = getValueFromConfig(currentPge, GLOBAL_CONFIG_CAT, "customizableTag");
  List<String> filterChildTagIdList = new ArrayList<String>();
  List<TaggingSearchCriteria> pageTagIdListForAndTags = new ArrayList<TaggingSearchCriteria>();
  List<String> namespaceForAndTags = getNamespaceForAndCondList(currentPge, dialogProperties);
  List<String> namespaceForOrTags = getNamespaceForOrCondList(currentPge);
  pageTagIdListForAndTags = populatePageTagIdList(pageTagIdListForAndTags, namespaceForAndTags, namespaceForOrTags, currentPge);
  populateFilterChildTagIdList(filterChildTagIdList, filternameSpaces, tagManager);
  int limit = getDisplayCountVal(currentPge, FIFTY, UnileverConstants.LIMIT);
  List<Map<String, Object>> localProductPropertiesList = productPropertiesList;
  if (pageTagIdListForAndTags != null && !pageTagIdListForAndTags.isEmpty()) {
            // get Product pages Path List
   List<String> productPagePathList = getFilteredProductSet(pageTagIdListForAndTags, START_RECORD, limit, resources);
            
   if (productPagePathList != null && !productPagePathList.isEmpty()) {
    Collections.sort(productPagePathList, new ProductResultComparator(featuredTags, customizableTag, resourceResolver));
            }
   productPagePathList = ProductBundlingToBinUtil.getProductsListForMultipleBuy(productPagePathList, currentPge, configurationService);
            // generate Product Properties Map
   localProductPropertiesList = ProductHelper.getProductListWithTag(productPagePathList, resources, getJsonNameSpace(content), null);
            // generate Filters
            
   generateFilters(productPagePathList, filterOneNamespace, filterTwoNamespace, filterChildTagIdList, filterOneList, filterTwoList,
                    pageManager);
            
            // Populate JSON
   filterOneMapProperties.put(FILTER_ONE_LIST, filterOneList.toArray());
   filterOneMapProperties.put(FILTER_LABEL, filterOneLabel);
   filterOneMapProperties.put(FILTER_DEFAULT, filterOneDefaultVal);
   filterTwoMapProperties.put(FILTER_TWO_LIST, filterTwoList.toArray());
   filterTwoMapProperties.put(FILTER_LABEL, filterTwoLabel);
   filterTwoMapProperties.put(FILTER_DEFAULT, filterTwoDefaultVal);
            
   masterFilterProperties.put(FILTER_ONE, filterOneMapProperties);
   masterFilterProperties.put(FILTER_TWO, filterTwoMapProperties);
   masterFilterProperties.put(FILTER_FLAG, isFilter(filterOneList, filterTwoList));
            
   properties.put(ProductConstants.MASTER_FILTER, masterFilterProperties);
            
   getAdaptiveRating(currentPge, slingRequest, pageManager, properties, localProductPropertiesList, productPagePathList,
                    configurationService);
            
        } else {
   properties.put(ProductConstants.PRODUCT_LIST_MAP, localProductPropertiesList.toArray());
        }
        
  properties.put(PRODUCT_COUNT, localProductPropertiesList.size());
        
  properties.put(ProductConstants.COMPONENT_TITLE, MapUtils.getString(dialogProperties, COMPONENT_TITLE));
  properties.put(ProductConstants.DISPLAY_COUNT, getDisplayCountVal(currentPge, SIX, DISPLAY_COUNT_VAL));
        
  properties.put(ProductConstants.STATIC_MAP, getStaticMapProperties(i18n, currentPge, resourceResolver, slingRequest));
  properties.put(ProductConstants.SHOPNOW_MAP, getReviewShopNowMapProperties(currentPge, SHOP_NOW));
  properties.put(ProductConstants.REVIEW_MAP, getReviewShopNowMapProperties(currentPge, "review"));
  properties.put(ProductConstants.ANCHOR_LINK, ComponentUtil.getAnchorLinkMap(dialogProperties));
  properties.put(SEARCH_CRITERIA, getSearchCriteriaMap(slingRequest, false));
  properties.put(ProductConstants.PRICE_ENABLED, ProductHelper.isPriceEnabled(configurationService, currentPge));
  Map<String, Object> containerTagMap = new LinkedHashMap<String, Object>();
  containerTagMap
                .put(ContainerTagConstants.FILTER, ComponentUtil.getContainerTagMap(currentPge, ContainerTagConstants.PRODUCT_LIST_FILTER, ""));
  containerTagMap.put(ContainerTagConstants.LOAD_MORE,
  ComponentUtil.getContainerTagMap(currentPge, ContainerTagConstants.LOAD_MORE, ContainerTagConstants.PRODUCT));
        
  properties.put(UnileverConstants.CONTAINER_TAG, containerTagMap);
    }

 /**
  * @param currentPage
  * @param slingRequest
  * @param pageManager
  * @param page
  * @param properties
  * @param productPropertiesList
  * @param productPagePathList
  * @param configurationService2
  */
 private void getAdaptiveRating(Page currentPage, SlingHttpServletRequest slingRequest, PageManager pageManager, Map<String, Object> properties,
   List<Map<String, Object>> productPropertiesList, List<String> productPagePathList, ConfigurationService configurationService2) {
  List<Map<String, Object>> productRatingPropertiesList;
  if (AdaptiveUtil.isAdaptive(slingRequest)) {
   
   String prodids = StringUtils.EMPTY;
   KritiqueDTO kritiqueparams = new KritiqueDTO(currentPage, configurationService2);
   LOGGER.info("Feature phone view called and kritique api is going to be called");
   for (String pagePath : productPagePathList) {
    
    Page productPage = pageManager.getPage(pagePath);
    UNIProduct product = null;
    Product pageProduct = CommerceHelper.findCurrentProduct(productPage);
    if (pageProduct instanceof UNIProduct) {
     product = (UNIProduct) pageProduct;
    }
    
    prodids = setProductId(prodids, product);
   }
   
   if (prodids != null) {
    kritiqueparams.setSiteProductId(prodids);
   }
   
   List<Map<String, Object>> kqresponse = kritiqueReviewsCommentsService.getRatingsService(kritiqueparams, configurationService);
   LOGGER.debug("Kritique response generated and size of the review list is " + kqresponse.size());
   productRatingPropertiesList = ComponentUtil.mapKritiqueProductRating(kqresponse, productPropertiesList);
   
   properties.put(ProductConstants.PRODUCT_LIST_MAP, productRatingPropertiesList.toArray());
   
  } else {
   properties.put(ProductConstants.PRODUCT_LIST_MAP, productPropertiesList.toArray());
  }
 }
 
 /**sets product id to kritiqueservice
  * @param prodids
  * @param product
  * @param kritiqueparams
  */
 private String setProductId(String prodids, UNIProduct product) {
  String productIds = prodids;
  if(product != null){
   productIds += product.getSKU() + ",";
     }
  return productIds;
 }
 
 /**
  * Checks if is filter.
  * @param filterOneList
  *         the filter one list
  * @param filterTwoList
  *         the filter two list
  * @return the object
  */
 private Object isFilter(Set<Map<String, String>> filterOneList, Set<Map<String, String>> filterTwoList) {
  boolean isFilterContainsOneValue = false;
  if (filterOneList.size() == 1 && filterTwoList.size() == 1) {
   isFilterContainsOneValue = true;
  }
  return isFilterContainsOneValue;
 }
 
 /**
  * Gets the display count val.
  * @param page
  *         the page
  * @return the display count val
  */
 private int getDisplayCountVal(Page page, int deafaultValue, String key) {
  int displayCountVal = deafaultValue;
  try {
   String displayCountTemp = configurationService.getConfigValue(page, GLOBAL_CONFIG_CAT, key);
   displayCountVal = StringUtils.isNotBlank(displayCountTemp) ? Integer.parseInt(displayCountTemp) : displayCountVal;
  } catch (ConfigurationNotFoundException e) {
   LOGGER.debug(DEBUG_STRING, e);
  }
  return displayCountVal;
 }
 
 /**
  * Populate filter child tag id list.
  * @param filterChildTagIdList
  *         the filter child tag id list
  * @param filternameSpaces
  *         the filtername spaces
  * @param tagManger
  *         the tag manger
  */
 private void populateFilterChildTagIdList(List<String> filterChildTagIdList, String[] filternameSpaces, TagManager tagManger) {
  for (String record : filternameSpaces) {
   Tag tag = tagManger.resolve(record);
   filterChildTagIdList.addAll(getChildrenTagList(tag));
  }
  
 }
 
 /**
  * Gets the namespace for or cond list.
  * @param page
  *         the page
  * @return the namespace for or cond list
  */
 private List<String> getNamespaceForOrCondList(Page page) {
  List<String> namespaceForOrCondList = new ArrayList<String>();
  setNameSpaceListFromConfig(page, namespaceForOrCondList,"namespaceForOrTags", SearchConstants.SEARCH_OR);
  LOGGER.debug("Namespaces for OR Condition List   = " + namespaceForOrCondList);
  return namespaceForOrCondList;
 }
 
 /**
  * Gets the namespace for and cond list.
  * @param page
  *         the page
  * @param dialogProperties
  *         the dialog properties
  * @return the namespace for and cond list
  */
 private List<String> getNamespaceForAndCondList(Page page, Map<String, Object> dialogProperties) {
  String[] prop = ComponentUtil.getPropertyValueArray(dialogProperties, "namespaceForAndTags");
  List<String> namespaceForAndCondList = new ArrayList<String>();
  setNameSpaceForAndCondList(prop, namespaceForAndCondList);
  
  setNameSpaceListFromConfig(page, namespaceForAndCondList, "namespaceForAndTags", SearchConstants.SEARCH_AND);
  LOGGER.debug("Namespaces for And Condition List   = " + namespaceForAndCondList);
  return namespaceForAndCondList;
 }

/**sets name space for and or tags list
 * @param page
 * @param namespaceForAndCondList
 */
 private void setNameSpaceListFromConfig(Page page, List<String> namespaceForAndOrCondList, String key, String searchOperator) {
  if (namespaceForAndOrCondList.isEmpty()) {
   String val = getValueFromConfig(page, GLOBAL_CONFIG_CAT, key);
   String[] localValArr = StringUtils.isNotBlank(val)?val.split(UnileverConstants.COMMA):null;
   String[] valArr = SearchConstants.SEARCH_AND.equals(searchOperator)?val.split(UnileverConstants.COMMA):localValArr;
   setNameSpaceForAndCondList(valArr, namespaceForAndOrCondList);
      }
}
    
    /**
     * @param prop
     * @param namespaceForAndCondList
     */
 private void setNameSpaceForAndCondList(String[] prop, List<String> namespaceForAndCondList) {
  if (prop != null) {
   for (String string : prop) {
    namespaceForAndCondList.add(string.isEmpty() ? "" : string);
            }
        }
    }

 /**
  * Gets the search criteria map.
  * @param slingRequest
  *         the sling request
  * @param isAjax
  *         the is ajax
  * @return the search criteria map
  */
 private Object getSearchCriteriaMap(SlingHttpServletRequest slingRequest, Boolean isAjax) {
  Map<String, String> searchCriteriaMap = new HashMap<String, String>();
  String filterOne = FILTER_NOT_APPLIED;
  String filterTwo = FILTER_NOT_APPLIED;
  String startRecordNumber = START_RECORD_NO_VAL;
  String maxResultCount = MAX_RESULT_COUNT_VAL;
  if (isAjax) {
   filterOne = slingRequest.getParameter(FILTER_ONE);
   filterTwo = slingRequest.getParameter(FILTER_TWO);
   startRecordNumber = slingRequest.getParameter(START_RECORD_NO);
   maxResultCount = slingRequest.getParameter(MAX_RESULT_COUNT);
  }
  searchCriteriaMap.put(FILTER_ONE, filterOne);
  searchCriteriaMap.put(FILTER_TWO, filterTwo);
  searchCriteriaMap.put(START_RECORD_NO, startRecordNumber);
  searchCriteriaMap.put(MAX_RESULT_COUNT, maxResultCount);
  return searchCriteriaMap;
 }
 
 /**
  * Populate tag id list.
  * @param pageTagIdListForOrTags
  *         the page tag id for or cond list
  * @param pageTagIdListForAndTags
  *         the page tag id for and cond list
  * @param namespaceForAndTags
  *         the namespace for and cond list
  * @param namespaceForOrTags
  *         the namespace for or tags
  * @param page
  *         the page
  */
 private List<TaggingSearchCriteria> populatePageTagIdList(List<TaggingSearchCriteria> pageTagIdListForAndTags, List<String> namespaceForAndTags,
   List<String> namespaceForOrTags, Page page) {
  Tag[] pageTagList = page.getTags();
  List<String> pageTagIdList = new ArrayList<String>();
  addTagToList(null, null, pageTagList, pageTagIdList, null, null);
  List<TaggingSearchCriteria> localPageTagIdListForAndTags = pageTagIdListForAndTags;
  localPageTagIdListForAndTags = getPageTagIdList(pageTagIdListForAndTags, namespaceForAndTags, pageTagIdList, SearchConstants.SEARCH_AND);
  localPageTagIdListForAndTags = getPageTagIdList(localPageTagIdListForAndTags, namespaceForOrTags, pageTagIdList, SearchConstants.SEARCH_OR);
  return localPageTagIdListForAndTags;
 }

/**returns pageTagIdList
 * @param pageTagIdListForAndTags
 * @param namespaceForOperatorTags
 * @param pageTagIdList
 * @param searchOperator
 * @return
 */
 private List<TaggingSearchCriteria> getPageTagIdList(List<TaggingSearchCriteria> pageTagIdListForAndTags, List<String> namespaceForOperatorTags,
        List<String> pageTagIdList, String searchOperator) {
  List<TaggingSearchCriteria> localPageTagIdListForAndTags = pageTagIdListForAndTags;
  for (String namespaceForOperatorTag : namespaceForOperatorTags) {
   List<String> tagList = new ArrayList<String>();
   for (String tagId : pageTagIdList) {
    if (StringUtils.startsWith(tagId, namespaceForOperatorTag.trim())) {
     tagList.add(tagId);
        }
       }
   if (!tagList.isEmpty()) {
    localPageTagIdListForAndTags = addFilterCriteriaToForTags(null, localPageTagIdListForAndTags, null, searchOperator, tagList);
       }
      }
  return localPageTagIdListForAndTags;
}
 
 /**returns pageTagIdList for and or tags
 * @param slingRequest
 * @param pageTagIdListForOrTags
 * @param filterKey
 * @param searchOperator
 * @param tagList
 * @return
 */
 private List<TaggingSearchCriteria> addFilterCriteriaToForTags(SlingHttpServletRequest slingRequest,
        List<TaggingSearchCriteria> pageTagIdListForOrTags, String filterKey, String searchOperator, List<String> tagList) {
  List<TaggingSearchCriteria> localPageTagIdListForOrTags = pageTagIdListForOrTags;
  TaggingSearchCriteria criteria = new TaggingSearchCriteria();

  List<String> localTagList = tagList;
  if(!(localTagList != null)){
   localTagList = new ArrayList<String>();
   localTagList.add(slingRequest.getParameter(filterKey));
     }
  criteria.setTagList(localTagList);
  criteria.setOperator(searchOperator);
     
  localPageTagIdListForOrTags.add(criteria);
  return localPageTagIdListForOrTags;
 }

 /**
  * Gets the feature tag.
  * @param page
  *         the page
  * @return the feature tag
  */
 private String getValueFromConfig(Page page,String category, String key) {
  try {
   String featureTag = StringUtils.EMPTY;
   featureTag = configurationService.getConfigValue(page, category, key);
   return featureTag;
  } catch (ConfigurationNotFoundException e) {
   LOGGER.debug(DEBUG_STRING, e);
   return null;
  }
 }
 
 /**
  * Checks if is ajax.
  * @param selectors
  *         the selectors
  * @return the boolean
  */
 private Boolean isAjax(String[] selectors) {
  boolean isAjax = false;
  
  for (String string : selectors) {
   
   if ("ajax".equals(string)) {
    isAjax = true;
    break;
   }
  }
  
  return isAjax;
  
 }
 
 /**
  * Gets the filter name space.
  * @param page
  *         the page
  * @param dialogProperties
  *         the dialog properties
  * @return the filter name space
  */
 private String[] getFilterNameSpace(Page page, Map<String, Object> dialogProperties) {
  String[] prop = ComponentUtil.getPropertyValueArray(dialogProperties, FILTER_ONE_NAMESPACE);
  String[] filterNamespaces = new String[TWO];
  filterNamespaces[0] = prop != null && prop.length > 0 ? prop[0] : null;
  
  prop = ComponentUtil.getPropertyValueArray(dialogProperties, FILTER_TWO_NAMESPACE);
  filterNamespaces[1] = prop != null && prop.length > 0 ? prop[0] : null;
  
  setFilterOneTwoSpaceName(page, filterNamespaces, ResourceCommonConstants.ZERO);
  setFilterOneTwoSpaceName(page, filterNamespaces, ResourceCommonConstants.ONE);
  LOGGER.debug("Filter Namespaces   = " + filterNamespaces);
  return filterNamespaces;
 }
    
    /**
     * sets filter name space for index
     * @param page
     * @param filterNamespaces
     * @param index
     */
 private void setFilterOneTwoSpaceName(Page page, String[] filterNamespaces, int index) {
  String key = (index == ResourceCommonConstants.ZERO) ? "filterOneNamespace" : "filterTwoNamespace";
  try {
   if (filterNamespaces[index] == null) {
    filterNamespaces[index] = configurationService.getConfigValue(page, GLOBAL_CONFIG_CAT, key);
            }
        } catch (ConfigurationNotFoundException e) {
   LOGGER.debug("could not find filterNamespaces key in productListing category", e);
        }
    }

 /**
  * Gets the filtered product set.
  * @param pageTagIdListForOrTags
  *         the page tag id for or cond list
  * @param pageTagIdListForAndTags
  *         the page tag id for and cond list
  * @param startRecordNo
  *         the start record no
  * @param maxResultCount
  *         the max result count
  * @param currentPage
  *         the current page
  * @return the filtered product set
  */
 private List<String> getFilteredProductSet(List<TaggingSearchCriteria> pageTagIdListForAndTags, int startRecordNo, int maxResultCount,
   Map<String, Object> resources) {
  List<String> taggedPagesList = new ArrayList<String>();
  List<String> negateTag = new ArrayList<String>();
  Page currentPage = getCurrentPage(resources);
  String currentPagePath = currentPage.getPath();
  Map<String, List<String>> propMap = new HashMap<String, List<String>>();
  List<String> propValueList = new ArrayList<String>();
  propValueList.add(UnileverConstants.PRODUCT);
  propMap.put(UnileverConstants.JCR_CONTENT_CONTENT_TYPE, propValueList);
  int limit = maxResultCount;
  List<SearchDTO> searchResult = searchService.getMultiTagResults(pageTagIdListForAndTags, negateTag, currentPagePath, propMap, new SearchParam(
    startRecordNo, limit, UnileverConstants.DESCENDING), getResourceResolver(resources));
  for (SearchDTO searchDTO : searchResult) {
   taggedPagesList.add(searchDTO.getResultNodePath());
  }
  LOGGER.debug("Valid Pages Path List  = " + taggedPagesList.size());
  return taggedPagesList;
 }
 
 /**
  * Generate filters.
  * @param productPagePathList
  *         the product page path list
  * @param filterOneNamespace
  *         the filter one namespace
  * @param filterTwoNamespace
  *         the filter two namespace
  * @param filterChildTagIdList
  *         the page tag children id list
  * @param filterOneList
  *         the filter one list
  * @param filterTwoList
  *         the filter two list
  * @param pageManager
  *         the page manager
  */
 private void generateFilters(List<String> productPagePathList, String filterOneNamespace, String filterTwoNamespace,
   List<String> filterChildTagIdList, Set<Map<String, String>> filterOneList, Set<Map<String, String>> filterTwoList, PageManager pageManager) {
  LOGGER.debug("ProductPagePathList{} filterOneNamespace and filterTwoNamespace{} ", productPagePathList.toString(), filterOneNamespace
    + filterTwoNamespace);
  LOGGER.debug("pageTagChildrenIdList{}", filterChildTagIdList.toString());
  
  Iterator<String> iterator = productPagePathList.iterator();
  while (iterator.hasNext()) {
   Page productPage = pageManager.getPage(iterator.next());
   Tag[] tags = productPage.getTags();
   String[] filterNameSpaces = new String[ResourceCommonConstants.TWO];
   filterNameSpaces[ResourceCommonConstants.ZERO] = filterOneNamespace;
   filterNameSpaces[ResourceCommonConstants.ONE] = filterTwoNamespace;
   addTagToList(filterOneList, filterTwoList, tags, null, filterNameSpaces, productPage);
  }
  LOGGER.debug("Product Filters for ProductListing Component filterOneList is this {}filterTwoList is this{} ", filterOneList.toString(),
    filterTwoList.toString());
  
 }
    
    /**
     * sets filters with name spaces
     * @param filterOneList
     * @param filterTwoList
     * @param tags
     * @param pageTagIdList
     * @param filterNameSpaces
     * @param productPage
     */
 private void addTagToList(Set<Map<String, String>> filterOneList, Set<Map<String, String>> filterTwoList, Tag[] tags, List<String> pageTagIdList,
            String[] filterNameSpaces, Page productPage) {
  if (tags != null) {
   for (Tag record : tags) {
    if (record != null && pageTagIdList == null) {
     setFilterOneTwoList(filterNameSpaces, filterOneList, filterTwoList, productPage, record);
                } else if (record != null) {
     pageTagIdList.add(record.getTagID());
                }
            }
        }
    }

    /**
     * sets filter list with tag if tag id starts with filterNameSpace
     * @param filterNamespace
     * @param filterList
     * @param productPage
     * @param record
     */
 private void setFilterOneTwoList(String[] filterNamespace, Set<Map<String, String>> filterOneList,
         Set<Map<String, String>> filterTwoList, Page productPage, Tag record) {
  if (StringUtils.startsWithIgnoreCase(record.getTagID(), filterNamespace[0])) {
   filterOneList.add(getFilterPropertiesMap(record.getTagID(), record.getTitle(getLocale(productPage))));
        }else if(StringUtils.startsWithIgnoreCase(record.getTagID(), filterNamespace[1])) {
   filterTwoList.add(getFilterPropertiesMap(record.getTagID(), record.getTitle(getLocale(productPage)))); 
        }
    }

    /**
     * Gets the filter properties map.
     * @param tagID
     *         the tag id
     * @param title
     *         the title
     * @return the filter properties map
     */
 private Map<String, String> getFilterPropertiesMap(String tagID, String title) {
  Map<String, String> filterListMapProperties = new HashMap<String, String>();
  filterListMapProperties = new HashMap<String, String>();
  filterListMapProperties.put("id", tagID);
  filterListMapProperties.put("label", title);
     
  return filterListMapProperties;
    }
    
    /**
     * Gets the static map properties.
     * @param i18n
     *         the i18n
     * @param currentPage
     *         the current page
     * @param resourceResolver
     *         the resource resolver
     * @return the static map properties
     */
 private Map<String, Object> getStaticMapProperties(I18n i18n, Page currentPage, ResourceResolver resourceResolver,
      SlingHttpServletRequest slingRequest) {
  Map<String, Object> staticMapProperties = new HashMap<String, Object>();
  staticMapProperties.put(ProductConstants.PRODUCT_COUNT_LABEL, i18n.get(PRODUCT_COUNT_LABEL));
  staticMapProperties.put(ProductConstants.FILTER_PRODUCTS_LABEL, i18n.get(FILTER_PRODUCTS_LABEL));
  staticMapProperties.put(ProductConstants.HIDE_FILTERS_LABEL, i18n.get(HIDE_FILTERS_LABEL));
  staticMapProperties.put(ProductConstants.MORELINK_MAP, getMoreLinkMapProperties(i18n, currentPage, resourceResolver, slingRequest));
  LOGGER.debug("Static Map Properties : " + staticMapProperties);
  return staticMapProperties;
    }
    
    /**
     * Gets the more link map properties.
     * @param i18n
     *         the i18n
     * @param currentPage
     *         the current page
     * @param resourceResolver
     *         the resource resolver
     * @return the more link map properties
     */
 private Map<String, Object> getMoreLinkMapProperties(I18n i18n, Page currentPage, ResourceResolver resourceResolver,
      SlingHttpServletRequest slingRequest) {
  Map<String, Object> moreLinkMapProperties = new HashMap<String, Object>();
  String fullListingUrl = ComponentUtil.getFullURL(resourceResolver, currentPage.getPath(), slingRequest);
     
  if (!StringUtils.isEmpty(fullListingUrl) && (fullListingUrl.contains(HTML_EXTENSION))) {
   fullListingUrl = fullListingUrl.replace(HTML_EXTENSION, HTML_FULL_LISTING_EXTENTION);
     }
     
  moreLinkMapProperties.put(ProductConstants.LABEL, i18n.get("productListing.showAllLabel"));
  moreLinkMapProperties.put(ProductConstants.URL, fullListingUrl);
  LOGGER.debug("More Link Map Properties : " + moreLinkMapProperties);
  return moreLinkMapProperties;
    }
    
    /**
     * Gets the review map properties.
     * @param page
     *         the page
     * @return the review map properties
     */
 private Map<String, String> getReviewShopNowMapProperties(Page page, String key) {
  Map<String, String> mapProperties = new HashMap<String, String>();
  String serviceProviderNameReview = null;
  String serviceProviderName = StringUtils.EMPTY;
  String enabledKey = "reviewEnable";
  if(SHOP_NOW.equals(key)){
   serviceProviderName = getValueFromConfig(page, SHOP_NOW, SERVICE_PROVIDER_NAME);
   enabledKey = "shopNowEnable";
   serviceProviderNameReview = serviceProviderName;
   LOGGER.debug("ShopNow Map Properties : " + mapProperties);
     }else{
   Resource resource = page.getContentResource();
   InheritanceValueMap valueMap = new HierarchyNodeInheritanceValueMap(resource);
   if (valueMap != null) {
    serviceProviderName = (String) valueMap.getInherited(RATING_REVIEWS, StringUtils.EMPTY);
      }
   if (StringUtils.isNotBlank(serviceProviderName)) {
    serviceProviderNameReview = serviceProviderName;
      } else {
    serviceProviderNameReview = getValueFromConfig(page, "ratingAndReviews", SERVICE_PROVIDER_NAME);
      }
     }
  mapProperties = getReviewShopNowProperties(page, serviceProviderNameReview, GLOBAL_CONFIG_CAT, enabledKey);
  
  LOGGER.debug("Review Map Properties : " + mapProperties);
  return mapProperties;
    }
       
       /**
        * @param page
        * @param serviceProviderName
        * @param category
        * @param key
        * @return
        */
 private Map<String, String> getReviewShopNowProperties(Page page, String serviceProviderName, String category, String key) {
  Map<String, String> mapProperties = new LinkedHashMap<String, String>();
  try {
   mapProperties = configurationService.getCategoryConfiguration(page, serviceProviderName);
   GlobalConfiguration globalConfiguration = page.adaptTo(GlobalConfiguration.class);
   String[] excludedConfig = globalConfiguration.getCommonGlobalConfigValue("excludedSEOProperties").split(",");
   for (String excludeKey : excludedConfig) {
    if (mapProperties.containsKey(excludeKey)) {
     mapProperties.remove(excludeKey);
    }
   }
   mapProperties.put("enabled", getValueFromConfig(page, category, key));
           } catch (ConfigurationNotFoundException e) {
   LOGGER.error("could not found config for " + category + "key " + key, e);
           }
  if(!key.contains(SHOP_NOW)){
   mapProperties.put(SERVICE_PROVIDER_NAME, getServiceProviderName(serviceProviderName));
  }
  return mapProperties;
       }
       
       /**
        * returns serviceprovidername
        * @param mapProperties
        * @param serviceProviderNameReview
        */
 private String getServiceProviderName(String serviceProviderNameReview) {
  String serviceProviderName = serviceProviderNameReview;
  if (BAZAARVOICE.equalsIgnoreCase(serviceProviderNameReview) || serviceProviderNameReview.startsWith(BAZAARVOICE)) {
   serviceProviderName = BAZAARVOICE;
           } else if (KRITIQUE.equalsIgnoreCase(serviceProviderNameReview) || serviceProviderNameReview.startsWith(KRITIQUE)) {
   serviceProviderName = KRITIQUE;
           }
  return serviceProviderName;
       }

    /**
     * Gets the children tag list.
     * @param tag
     *         the tag
     * @return the children tag list
     */
 private List<String> getChildrenTagList(Tag tag) {
  List<String> childrenTagList = new ArrayList<String>();
  if (tag != null) {
   Iterator<Tag> iterator = tag.listChildren();
   while (iterator.hasNext()) {
    Tag child = iterator.next();
    childrenTagList.add(child.getTagID());
      }
     }
  return childrenTagList;
    }
}
