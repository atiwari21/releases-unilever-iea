/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.components;

import java.util.Comparator;
import java.util.List;

import javax.jcr.RepositoryException;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unilever.platform.aem.foundation.core.dto.SearchDTO;

/**
 * The Class SearchResultComparator.
 */
public class ProductRangeComparator implements Comparator<Object> {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ProductRangeComparator.class);
 
 /** The tags list. */
 private List<String> tagsList;
 
 /**
  * Instantiates a new product range comparator.
  * 
  * @param tagsCollection
  *         the tags collection
  */
 public ProductRangeComparator(List<String> tagsCollection) {
  super();
  tagsList = tagsCollection;
  
 }
 
 @Override
 /*
  * (non-Javadoc)
  * 
  * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
  */
 public int compare(Object resultNodeObj1, Object resultNodeObj2) {
  int count1 = 0;
  int count2 = 0;
  SearchDTO resultNode1 = (SearchDTO) resultNodeObj1;
  SearchDTO resultNode2 = (SearchDTO) resultNodeObj2;
  
  int shortestTagLength1 = Integer.MAX_VALUE;
  int shortestTagLength2 = Integer.MAX_VALUE;
  
  try {
   for (Value tag1 : resultNode1.getResultNodeTags()) {
    if (tagsList.contains(tag1.getString())) {
     count1++;
     shortestTagLength1 = getShortestTagLength(tag1, shortestTagLength1);
    }
    
   }
   
   for (Value tag2 : resultNode2.getResultNodeTags()) {
    if (tagsList.contains(tag2.getString())) {
     count2++;
     shortestTagLength2 = getShortestTagLength(tag2, shortestTagLength2);
    }
    
   }
   
  } catch (ValueFormatException e) {
   LOGGER.error(ExceptionUtils.getStackTrace(e));
  } catch (IllegalStateException e) {
   LOGGER.error(ExceptionUtils.getStackTrace(e));
  } catch (RepositoryException e) {
   LOGGER.error(ExceptionUtils.getStackTrace(e));
  }
  
  int res = shortestTagLength1 - shortestTagLength2;
  if (res == 0) {
   res = count2 - count1;
   
  }
  
  return res;
 }
 
 private int getShortestTagLength(Value tag, int shortestTagLength) throws RepositoryException {
  
  int tagLength = shortestTagLength;
  
  String[] temp = tag.getString().split("/");
  tagLength = temp.length < shortestTagLength ? temp.length : shortestTagLength;
  
  return tagLength;
 }
}
