/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.cache.ComponentViewAndViewHelperCache;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.constants.CommonConstants;
import com.unilever.platform.aem.foundation.core.dto.SearchDTO;
import com.unilever.platform.aem.foundation.core.service.SearchService;
import com.unilever.platform.aem.foundation.utils.AdaptiveUtil;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.LandingPageListHelper;
import com.unilever.platform.foundation.components.helper.ProductBundlingToBinUtil;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.components.helper.TaggingComponentsHelper;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.kritique.config.KritiqueReviewsCommentsService;
import com.unilever.platform.foundation.template.dto.KritiqueDTO;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.MultipleRelatedProductsConstants;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * <p>
 * Responsible for reading author input for relatedProductsAlternate component and get all the details of products which are tagged to the current
 * page and generate a product list. Also read the site configuration for maximum number configured for the product list.
 * </p>
 * .
 */
@Component(description = "RelatedProductsViewHelperImpl", immediate = true, metatype = true, label = "RelatedProductsViewHelperImpl")
@Service(value = { RelatedProductsViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.RELATED_PRODUCT, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class RelatedProductsViewHelperImpl extends BaseViewHelper {
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(RelatedProductsViewHelperImpl.class);
 
 /** The component view and view helper cache. */
 /*
  * reference for cache object
  */
 @Reference
 ComponentViewAndViewHelperCache componentViewAndViewHelperCache;
 /*
  * reference for site configuration service
  */
 /** The kritique service. */
 @Reference
 KritiqueReviewsCommentsService kritiqueReviewsCommentsService;
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The search service. */
 @Reference
 SearchService searchService;
 
 /** The Constant TEASER IMAGE FLAG. */
 private static final String PRODUCT_RATING = "productRating";
 /** The Constant TEASER COPY FLAG. */
 private static final String QUICK_VIEW = "quickView";
 /** The Constant TEASER LONG COPY. */
 private static final String BUY_IT_NOW = "buyItNow";
 /** The Constant OVERRIDE GLOBAL CONFIG. */
 private static final String OVERRIDE_GLOBAL_CONFIG = "overrideGlobalConfig";
 /** The Constant heading. */
 private static final String HEADING = "heading";
 /** The Constant subheading. */
 private static final String SUBHEADING = "subHeading";
 /** The Constant cta. */
 private static final String CTA_LABEL = "ctaLabel";
 /** The Constant FIXED_LIST. */
 private static final String FIXED_LIST = "Fixed List";
 /** The Constant CONTAINER_TAG. */
 private static final String CONTAINER_TAG = "containertag";
 /** The Constant RATINGS. */
 private static final String RATINGS = "ratings";
 /** The Constant PROD_QUICK_VIEW. */
 private static final String PROD_QUICK_VIEW = "productQuickView";
 private static final String FEATURE_TAGS = "featureTags";
 
 /**
  * The method processData overrides method is responsible validating author input and returning a list of products, which are tagged to the current
  * page and maximum number for the links from site configuration to be displayed on the page.
  * 
  * @param content
  *         {@link Map} of all user input received as input
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return Map holding the result data.
  */
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  LOGGER.debug("Executing RelatedProductsBaseViewHelperImpl.java. Inside processData method");
  Map<String, Object> data = new HashMap<String, Object>();
  Map<String, Object> properties = getProperties(content);
  List<SearchDTO> productsResultList = new ArrayList<SearchDTO>();
  List<String> productsPagePathList = new ArrayList<String>();
  
  ResourceResolver resourceResolver = (ResourceResolver) resources.get(ProductConstants.RESOURCE_RESOLVER);
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  Page currentPage = getCurrentPage(resources);
  String gloablConfigCatName = MapUtils.getString(properties, UnileverConstants.GLOBAL_CONFIGURATION_CATEGORY_NAME);
  String[] featureTagNameSpaces = StringUtils.isNotBlank(MapUtils.getString(properties, FEATURE_TAGS)) ? ComponentUtil.getPropertyValueArray(
    properties, FEATURE_TAGS) : new String[] {};
  String buildType = MapUtils.getString(properties, "buildList") != null ? MapUtils.getString(properties, "buildList") : StringUtils.EMPTY;
  String headingtext = MapUtils.getString(properties, HEADING) != null ? MapUtils.getString(properties, HEADING) : StringUtils.EMPTY;
  String subheadingtext = MapUtils.getString(properties, "text") != null ? MapUtils.getString(properties, "text") : StringUtils.EMPTY;
  String ctaLabeltext = MapUtils.getString(properties, CTA_LABEL) != null ? MapUtils.getString(properties, CTA_LABEL) : StringUtils.EMPTY;
  LOGGER.debug("Global Configuration category for RelatedProductsBaseViewHelperImpl = " + gloablConfigCatName);
  String jsonNameSpace = MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE);
  Map<String, String> configMap = ComponentUtil.getConfigMap(currentPage, gloablConfigCatName);
  Map<String, Object> finalProperties = new LinkedHashMap<String, Object>();
  if (buildType.equals(FIXED_LIST)) {
   productsPagePathList = getFixedProducts(pageManager, getCurrentResource(resources));
  } else {
   List<String> propValueList = new ArrayList<String>();
   propValueList.add(UnileverConstants.PRODUCT);
   Map<String, List<String>> propMap = new HashMap<String, List<String>>();
   propMap.put(UnileverConstants.JCR_CONTENT_CONTENT_TYPE, propValueList);
   productsPagePathList = TaggingComponentsHelper.getTaggedPageList(properties, configMap, currentPage, pageManager, propMap, searchService,
     resources);
   
  }
  LOGGER.debug("RelatedProductsBaseViewHelperImpl configMap = " + configMap);
        if (StringUtils.isNotBlank(buildType)) {
            productsPagePathList = ProductBundlingToBinUtil.getProductsListForMultipleBuy(productsPagePathList, currentPage, configurationService);
            ProductBundlingToBinUtil.setMultiBuyProductsConfigProperties(properties, finalProperties, configMap,
                    MapUtils.getBooleanValue(properties, UnileverConstants.OVERRIDE_GLOBAL_CONFIG, false), configurationService, currentPage);
            ProductBundlingToBinUtil.setMultiBuyProductsCtaLabels(resources, finalProperties);
            LOGGER.debug("RelatedProductsBaseViewHelperImpl productsResultList = " + productsResultList);
   List<Map<String, Object>> list = ProductHelper.getProductListWithTag(productsPagePathList, resources, jsonNameSpace, StringUtils.EMPTY);
   int pageCount = 0;
   for (Map<String, Object> updatedPageMap : list) {
    LandingPageListHelper.addFeatureTagsData(featureTagNameSpaces, updatedPageMap,
      pageManager.getContainingPage(productsPagePathList.get(pageCount)), resources);
    pageCount++;
   }
   setOverridenLandingAttribute(properties, configMap, list);
   Map<String, Object> productMap = getProductMap(resources, productsPagePathList, properties, list, configMap);
   
   finalProperties.put(HEADING, headingtext);
   finalProperties.put(SUBHEADING, subheadingtext);
   finalProperties.put(CTA_LABEL, ctaLabeltext);
   finalProperties.put("products", productMap);
  }
  
  data.put(jsonNameSpace, finalProperties);
  return data;
 }
 
 private List<String> getFixedProducts(PageManager pageManager, Resource resource) {
  List<String> propertyNames = new ArrayList<String>();
  propertyNames.add("productPath");
  List<Map<String, String>> prodSectionsList = ComponentUtil.getNestedMultiFieldProperties(resource, "sections");
  return prepareSectionList(prodSectionsList, pageManager);
 }
 
 private List<String> prepareSectionList(List<Map<String, String>> productSectionsList, PageManager pageManager) {
  List<String> finalSectionsList = new ArrayList<String>();
  for (Map<String, String> sectionMap : productSectionsList) {
   String prodPath = sectionMap.get("productPath");
   Page page = StringUtils.isNotBlank(prodPath) && pageManager != null ? pageManager.getContainingPage(prodPath) : null;
   if (ProductHelper.getUniProduct(page) != null) {
    finalSectionsList.add(prodPath);
   }
  }
  
  return finalSectionsList;
 }
 
 /**
  * Gets the product map.
  * 
  * @param productsPagePathList
  * @param currentPage
  * @param slingRequest
  *         the sling request
  * @param properties
  *         the properties
  * @param configMap
  *         the config map
  * @param list
  *         the list
  * @param pageManager
  * 
  * @return the product map
  */
 protected Map<String, Object> getProductMap(final Map<String, Object> resources, List<String> productsPagePathList, Map<String, Object> properties,
   List<Map<String, Object>> list, Map<String, String> configMap) {
  Map<String, Object> productMap = new HashMap<String, Object>();
  
  List<Map<String, Object>> productRatingPropertiesList = new ArrayList<Map<String, Object>>();
  if (AdaptiveUtil.isAdaptive(getSlingRequest(resources))) {
   
   String prodids = StringUtils.EMPTY;
   KritiqueDTO kritiqueparams = new KritiqueDTO(getCurrentPage(resources), configurationService);
   LOGGER.info("Feature phone view called and kritique api is going to be called");
   for (String pagePath : productsPagePathList) {
    
    if (ProductHelper.getUniProduct(getPageManager(resources).getPage(pagePath)) != null) {
     prodids += ProductHelper.getUniProduct(getPageManager(resources).getPage(pagePath)).getSKU() + ",";
    }
   }
   if (prodids != null) {
    kritiqueparams.setSiteProductId(prodids);
   }
   
   List<Map<String, Object>> kqresponse = kritiqueReviewsCommentsService.getRatingsService(kritiqueparams, configurationService);
   LOGGER.debug("Kritique response generated and size of the review list is " + kqresponse.size());
   productRatingPropertiesList = ComponentUtil.mapKritiqueProductRating(kqresponse, list);
   productMap.put(ProductConstants.PRODUCT_LIST_MAP, productRatingPropertiesList.toArray());
   
  } else {
   productMap.put(ProductConstants.PRODUCT_LIST_MAP, list.toArray());
  }
  
  Map<String, Object> staticMap = new HashMap<String, Object>();
  Map<String, Object> zoomMapProperties = new LinkedHashMap<String, Object>();
  staticMap.put(ProductConstants.PRODUCT_COUNT_LABEL, getI18n(resources).get(ProductConstants.PRODUCT_COUNT_LABEL));
  boolean overrideGlobalConfig = MapUtils.getBooleanValue(properties, OVERRIDE_GLOBAL_CONFIG, false);
  boolean buyItNowFlag = isProductAttributeTrue(properties, configMap, overrideGlobalConfig, BUY_IT_NOW);
  Map<String, String> shopNowMap = (buyItNowFlag) ? ProductHelper.getshopNowProperties(configurationService, getCurrentPage(resources),
    getI18n(resources), getResourceResolver(resources), getSlingRequest(resources)) : null;
  productMap.put(ProductConstants.SHOPNOW_MAP, shopNowMap);
  productMap.put(ProductConstants.REVIEW_MAP, ProductHelper.getReviewMapProperties(configurationService, getCurrentPage(resources)));
  String title = MapUtils.getString(properties, MultipleRelatedProductsConstants.TITLE, StringUtils.EMPTY);
  productMap.put(MultipleRelatedProductsConstants.TITLE, title);
  staticMap = ProductHelper.getStaticMapProperties(staticMap, zoomMapProperties, configurationService, getI18n(resources), getCurrentPage(resources));
  productMap.put(ProductConstants.STATIC_MAP, staticMap);
  getGlobalOrOverridenConfig(properties, configMap, productMap);
  return productMap;
 }
 
 /**
  * Gets the global or overriden config.
  * 
  * @param properties
  *         the properties
  * @param configMap
  *         the config map
  * @param productMap
  *         the product map
  * @return the global or overriden config
  */
 private void getGlobalOrOverridenConfig(Map<String, Object> properties, Map<String, String> configMap, Map<String, Object> productMap) {
  String overrideGlobalConfig = MapUtils.getString(properties, UnileverConstants.OVERRIDE_GLOBAL_CONFIG, StringUtils.EMPTY);
  int limit = 0;
  if ("true".equals(overrideGlobalConfig)) {
   productMap.put(PRODUCT_RATING, MapUtils.getBoolean(properties, PRODUCT_RATING) != null ? MapUtils.getBoolean(properties, PRODUCT_RATING) : false);
   productMap.put(BUY_IT_NOW, MapUtils.getBoolean(properties, BUY_IT_NOW) != null ? MapUtils.getBoolean(properties, BUY_IT_NOW) : false);
   productMap.put(QUICK_VIEW, MapUtils.getBoolean(properties, QUICK_VIEW) != null ? MapUtils.getBoolean(properties, QUICK_VIEW) : false);
   productMap.put(UnileverConstants.LIMIT,
     MapUtils.getInteger(properties, UnileverConstants.LIMIT) != null ? MapUtils.getInteger(properties, UnileverConstants.LIMIT)
       : CommonConstants.ZERO);
  } else {
   Boolean productRating = Boolean.parseBoolean(configMap.get(PRODUCT_RATING));
   Boolean buyItNow = Boolean.parseBoolean(configMap.get(BUY_IT_NOW));
   Boolean quickView = Boolean.parseBoolean(configMap.get(QUICK_VIEW));
   String searchLimit = MapUtils.getString(configMap, UnileverConstants.LIMIT, StringUtils.EMPTY);
   limit = StringUtils.isNotBlank(searchLimit) ? Integer.parseInt(searchLimit) : CommonConstants.FIFTY;
   if (null != productRating) {
    productMap.put(PRODUCT_RATING, productRating);
   }
   productMap.put(BUY_IT_NOW, buyItNow);
   productMap.put(QUICK_VIEW, quickView);
   productMap.put(UnileverConstants.LIMIT, limit);
  }
 }
 
 /**
  * setting enable for teaser title,copy,image and publish date
  * 
  * @param properties
  * @param list
  */
 private void setOverridenLandingAttribute(Map<String, Object> properties, Map<String, String> configMap, List<Map<String, Object>> list) {
  boolean overrideGlobalConfig = MapUtils.getBooleanValue(properties, OVERRIDE_GLOBAL_CONFIG, false);
  boolean productRatingFlag = isProductAttributeTrue(properties, configMap, overrideGlobalConfig, PRODUCT_RATING);
  boolean quickViewFlag = isProductAttributeTrue(properties, configMap, overrideGlobalConfig, QUICK_VIEW);
  boolean buyItNowFlag = isProductAttributeTrue(properties, configMap, overrideGlobalConfig, BUY_IT_NOW);
  setUpdatedProductListData(list, productRatingFlag, false, RATINGS);
  setUpdatedProductListData(list, quickViewFlag, true, PROD_QUICK_VIEW);
  setUpdatedProductListData(list, buyItNowFlag, false, ProductConstants.SHOPNOW_MAP);
 }
 
 /**
  * returns boolean flag based on oveerriden global config flag
  * 
  * @param properties
  * @param configMap
  * @param overrideGlobalConfig
  * @param productKey
  * @return
  */
 private boolean isProductAttributeTrue(Map<String, Object> properties, Map<String, String> configMap, boolean overrideGlobalConfig, String productKey) {
  if (overrideGlobalConfig) {
   return MapUtils.getBooleanValue(properties, productKey, false);
  } else {
   return MapUtils.getBooleanValue(configMap, productKey, false);
  }
 }
 
 /**
  * sets the product attributes ratings,quick view and buy now enable or disable based on flag
  * 
  * @param productListData
  * @param productFlag
  * @param containerRemovalFlag
  * @param buyNowEmptyFlag
  * @param keyToBeNull
  */
 private void setUpdatedProductListData(List<Map<String, Object>> productListData, boolean productFlag, boolean quickViewFlag, String keyToBeNull) {
  
  if (!productFlag) {
   for (Map<String, Object> productListDataMap : productListData) {
    if (quickViewFlag) {
     productListDataMap.put(ProductConstants.QUICK_VIEW_LABEL, StringUtils.EMPTY);
    } else {
     productListDataMap.put(keyToBeNull, null);
    }
    disableContainerTagMap(keyToBeNull, productListDataMap);
   }
  }
  
 }
 
 /**
  * puts the map to be null for container tag in case of false flag
  * 
  * @param keyToBeNull
  * @param productListDataMap
  */
 private void disableContainerTagMap(String keyToBeNull, Map<String, Object> productListDataMap) {
  if (productListDataMap.get(CONTAINER_TAG) != null && !(RATINGS).equals(keyToBeNull)) {
   Map<String, Object> conatinerDataMap = (Map<String, Object>) productListDataMap.get(CONTAINER_TAG);
   conatinerDataMap.put(keyToBeNull, null);
  }
 }
 
 /**
  * Gets the search service.
  * 
  * @return the search service
  */
 protected SearchService getSearchService() {
  
  return this.searchService;
 }
 
 /**
  * Gets the config service.
  * 
  * @return the config service
  */
 protected ConfigurationService getConfigService() {
  
  return this.configurationService;
 }
 
}
