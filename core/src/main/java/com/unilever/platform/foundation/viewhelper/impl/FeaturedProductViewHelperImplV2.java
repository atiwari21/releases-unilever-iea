/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.api.Product;
import com.adobe.cq.commerce.common.CommerceHelper;
import com.day.cq.i18n.I18n;
import com.day.cq.tagging.Tag;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.core.service.SearchService;
import com.unilever.platform.aem.foundation.utils.AdaptiveUtil;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.LandingPageListHelper;
import com.unilever.platform.foundation.components.helper.ProductHelper;
import com.unilever.platform.foundation.components.helper.StoreLocatorUtility;
import com.unilever.platform.foundation.components.helper.TaggingComponentsHelper;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.kritique.config.KritiqueReviewsCommentsService;
import com.unilever.platform.foundation.template.dto.Image;
import com.unilever.platform.foundation.template.dto.KritiqueDTO;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * <p>
 * Responsible for reading author input for featured product component and
 * returns a featured product.
 *
 * </p>
 */
/**
 * @author ksaty1
 * 
 */
@Component(description = "Featured Product ViewHelper V2 Impl", immediate = true, metatype = true, label = "Featured Product V2 View Helper")
@Service(value = { FeaturedProductViewHelperImplV2.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.FEATURED_PRODUCT_V2, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class FeaturedProductViewHelperImplV2 extends BaseViewHelper {
 
 private static final String COMPONENT_NAME = "componentName";
 
 private static final String GLOBAL_CONFIG_CATEGORY = "featuredProductV2";
 
 /**
  * Constants for retrieving FEATURED_PRODUCT_TYPE value from dialog fields
  */
 private static final String FEATURED_PRODUCT_TYPE = "featuredProductType";
 /**
  * Constants for retrieving FEATURED_BACKGROUND_IMAGE value from dialog fields
  */
 private static final String FEATURED_BACKGROUND_IMAGE = "featuredBackgroundImage";
 /**
  * Constant for BACKGROUND_IMAGE_ALT_TEXT.
  */
 private static final String FEATURED_BACKGROUND_IMAGE_ALT_TEXT = "featuredBackgroundImgAltText";
 /**
  * Constants for retrieving CTA_LABEL value from dialog fields
  */
 private static final String CTA_LABEL = "ctaLabel";
 /**
  * Constants for retrieving PRODUCT_RATING value from dialog fields
  */
 private static final String PRODUCT_RATING = "productRating";
 /**
  * Constants for retrieving BUY_IT_NOW value from dialog fields
  */
 private static final String BUY_IT_NOW = "buyItNow";
 /**
  * Constants for retrieving PRODUCT_PAGE value from dialog fields
  */
 private static final String PRODUCT_PAGE = "productPage";
 /**
  * Constants for retrieving SUBHEADING_TEXT value from dialog fields
  */
 private static final String SUBHEADING_TEXT = "subHeadingText";
 private static final String FEATURE_TAGS = "featureTags";
 /**
  * Constant for retrieving background image label.
  */
 
 private static final String BACKGROUND = "backgroundImage";
 
 /**
  * Constant for retrieving product list label.
  */
 
 private static final String PRODUCT_LIST = "productList";
 
 /**
  * Constant for setting featured product type auto.
  */
 
 private static final String AUTO = "Automatically";
 
 /**
  * Constant for retrieving type label.
  */
 
 private static final String TYPE = "type";
 
 /**
  * Constant for setting alt text for background image.
  */
 
 private static final String CTA = "textLink";
 
 /**
  * Constant for setting productRatingFlag.
  */
 
 private boolean productRatingFlag = false;
 
 /**
  * Constant for setting buyItNowFlag.
  */
 
 private boolean buyItNowFlag = false;
 
 @Reference
 ConfigurationService configurationService;
 
 /** The search service. */
 @Reference
 SearchService searchService;
 
 /** The kritique service. */
 @Reference
 KritiqueReviewsCommentsService kritiqueReviewsCommentsService;
 
 I18n i18n;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(FeaturedProductViewHelperImpl.class);
 
 /**
  * The method processData overrides method is responsible validating author input and returning a map Map can be accessed in JSP in consistent
  * way.This method also validates the link url provided by author.
  * 
  * @param content
  *         , - {@link Map} of all user input received as input <code>ViewHelper.onGetData</code> method
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return {@link Map} holding the data
  */
 @Override
 @SuppressWarnings("unchecked")
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.info("Featured product View Helper #processData called for processing fixed list.");
  i18n = getI18n(resources);
  Map<String, Object> featuredProductProperties = (Map<String, Object>) content.get(PROPERTIES);
  Map<String, Object> featuredProductFinalMap = new LinkedHashMap<String, Object>();
  ResourceResolver resourceResolver = (ResourceResolver) resources.get(ProductConstants.RESOURCE_RESOLVER);
  SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) resources.get(ProductConstants.REQUEST);
  String jsonNameSpace = MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE);
  Resource currentResource = slingRequest.getResource();
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  
  Page currentPage = pageManager.getContainingPage(currentResource);
  String productType = MapUtils.getString(featuredProductProperties, FEATURED_PRODUCT_TYPE);
  String componentName = "";
  ValueMap componentProperties = currentResource.getValueMap();
  if (componentProperties != null) {
   componentName = componentProperties.get(COMPONENT_NAME, String.class);
  }
  Map<String, String> configMap = ComponentUtil.getConfigMap(currentPage, GLOBAL_CONFIG_CATEGORY);
  setBooleanFlags(featuredProductProperties, configMap);
  LOGGER.debug("created Map for featured product configuration ,size of this Map is " + configMap.size());
  
  // checking type of data
  if (StringUtils.isNotEmpty(productType)) {
   if (AUTO.equals(productType)) {
    List<String> propValueList = new ArrayList<String>();
    propValueList.add("Product");
    Map<String, List<String>> propMap = new HashMap<String, List<String>>();
    propMap.put(UnileverConstants.JCR_CONTENT_CONTENT_TYPE, propValueList);
    List<String> productsPagePathList = new ArrayList<String>();
    productsPagePathList = TaggingComponentsHelper.getTaggedPageList(featuredProductProperties, configMap, currentPage, pageManager, propMap,
      searchService, resources);
    LOGGER.debug("created list of pages after sorting for featured product ,size of the list is " + productsPagePathList.size());
    List<Map<String, Object>> list = ProductHelper.getProductList(productsPagePathList, resources, componentName, productRatingFlag);
    LOGGER.debug("product list of PDP pages for featured product has been created ,size of the list is " + list.size());
    
    Map<String, Object> productMap = getProductMap(slingRequest, productsPagePathList, featuredProductProperties, configMap, list, currentPage,
      resources);
    featuredProductFinalMap.put(jsonNameSpace, productMap);
    return featuredProductFinalMap;
   } else {
    featuredProductFinalMap = getProductsManual(resourceResolver, slingRequest, content, featuredProductProperties, currentPage, resources);
   }
   
   setStoreLocatorMap(resources, slingRequest, featuredProductFinalMap);
   
  } else {
   List<Map<String, Object>> blankFeaturedProductListFinal = new ArrayList<Map<String, Object>>();
   featuredProductFinalMap.put(jsonNameSpace, blankFeaturedProductListFinal.toArray());
  }
  return featuredProductFinalMap;
 }
 
 /**
  * Update store locator map.
  * 
  * @param resources
  *         the resources
  * @param currentPage
  *         the current page
  * @param slingRequest
  *         the sling request
  * @param storeLocatorMap
  *         the store locator map
  */
 @SuppressWarnings("unchecked")
 private void setStoreLocatorMap(Map<String, Object> resources, SlingHttpServletRequest slingRequest,
   Map<String, Object> featuredProductFinalMap) {
  
  Resource currentResource = slingRequest.getResource();
  String componentName = StringUtils.EMPTY;
  ValueMap componentProperties = currentResource.getValueMap();
  if (componentProperties != null) {
   componentName = MapUtils.getString(componentProperties, "componentName", StringUtils.EMPTY);
  }
  Map<String, Object> featuredProductMap = MapUtils.getMap(featuredProductFinalMap, "featuredProductV2", MapUtils.EMPTY_MAP);
  featuredProductMap
    .put("storeLocator", StoreLocatorUtility.getStoreLocatorMapV2(getI18n(resources), resources, configurationService, componentName));
 }
 
 /**
  * Sets the boolean flags.
  * 
  * @param featuredProductProperties
  *         the featured product properties
  * @param configMap
  *         the config map
  */
 private void setBooleanFlags(Map<String, Object> featuredProductProperties, Map<String, String> configMap) {
  Boolean overriddenGlobalConfig = MapUtils.getBoolean(featuredProductProperties, UnileverConstants.OVERRIDE_GLOBAL_CONFIG, false);
  Boolean pRating = false;
  Boolean buyIt = false;
  if (overriddenGlobalConfig) {
   pRating = MapUtils.getBoolean(featuredProductProperties, PRODUCT_RATING) != null ? (boolean) MapUtils.getBoolean(featuredProductProperties,
     PRODUCT_RATING) : false;
   buyIt = MapUtils.getBoolean(featuredProductProperties, BUY_IT_NOW) != null ? (boolean) MapUtils.getBoolean(featuredProductProperties, BUY_IT_NOW)
     : false;
   if (pRating) {
    productRatingFlag = true;
   } else {
    productRatingFlag = false;
   }
   if (buyIt) {
    buyItNowFlag = true;
   } else {
    buyItNowFlag = false;
   }
  } else {
   String productRating = configMap.get(PRODUCT_RATING);
   String buyItNow = configMap.get(BUY_IT_NOW);
   if (StringUtils.isNotEmpty(productRating) && "true".equals(productRating)) {
    productRatingFlag = true;
   } else {
    productRatingFlag = false;
   }
   if (StringUtils.isNotEmpty(buyItNow) && "true".equals(buyItNow)) {
    buyItNowFlag = true;
   } else {
    buyItNowFlag = false;
   }
  }
 }
 
 /**
  * Gets the product map.
  * 
  * @param slingRequest
  *         the sling request
  * @param properties
  *         the properties
  * @param configMap
  *         the config map
  * @param list
  *         the list
  * 
  * @return the product map
  */
 protected Map<String, Object> getProductMap(SlingHttpServletRequest slingRequest, List<String> productsPagePathList, Map<String, Object> properties,
   Map<String, String> configMap, List<Map<String, Object>> list, Page currentPage, Map<String, Object> resources) {
  
  ResourceResolver resourceResolver = getResourceResolver(resources);
  
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  Map<String, Object> productMap = new LinkedHashMap<String, Object>();
  boolean ratingFlag = false;
  Page productPage = null;
  if (CollectionUtils.isNotEmpty(productsPagePathList)) {
   productPage = pageManager.getContainingPage(productsPagePathList.get(0));
   KritiqueDTO kritiqueparams = new KritiqueDTO(currentPage, configurationService);
   List<Map<String, Object>> listwithrating = new ArrayList<Map<String, Object>>();
   ratingFlag = getKQForFeaturePhone(slingRequest, list, productMap, productPage, kritiqueparams, listwithrating);
  }
  if (!ratingFlag) {
   List<Map<String, Object>> list1 = new LinkedList<Map<String, Object>>();
   if (!list.isEmpty()) {
    list1.add(list.get(0));
   }
   productMap.put(ProductConstants.PRODUCT_LIST_MAP, list1.toArray());
  }
  Map<String, Object> backgroundImageAutoMap = getImageProperties(properties, currentPage, slingRequest);
  LOGGER.debug("created a Map for background image auto for featured product ,size of th Map is " + backgroundImageAutoMap.size());
  productMap.put(BACKGROUND, backgroundImageAutoMap);
  String title = properties.get(CTA_LABEL) != null ? (String) properties.get(CTA_LABEL) : StringUtils.EMPTY;
  productMap.put(TYPE, MapUtils.getString(properties, FEATURED_PRODUCT_TYPE).toLowerCase());
  if (productPage != null) {
   productMap.put(ProductConstants.REVIEW_MAP, ProductHelper.getReviewMapProperties(configurationService, productPage));
  } else {
   productMap.put(ProductConstants.REVIEW_MAP, new HashMap<String, String>());
   LOGGER.debug("Product Page is not available");
  }
  String[] featureTagNameSpaces = StringUtils.isNotBlank(MapUtils.getString(properties, FEATURE_TAGS)) ? ComponentUtil.getPropertyValueArray(
    properties, FEATURE_TAGS) : new String[] {};
  LandingPageListHelper.addFeatureTagsData(featureTagNameSpaces, productMap, productPage, resources);
  productMap.put(CTA_LABEL, title);
  productMap.put(SUBHEADING_TEXT, properties.get(SUBHEADING_TEXT));
  boolean disableBuyItNow = ProductHelper.isDisableBuyItNow(productPage, currentPage, configurationService);
  if (buyItNowFlag && !disableBuyItNow) {
   productMap.put(ProductConstants.SHOPNOW_MAP,
     ProductHelper.getshopNowProperties(configurationService, currentPage, i18n, resourceResolver, slingRequest));
  }
  getGlobalOrOverridenConfig(properties, configMap, productMap, disableBuyItNow);
  return productMap;
 }
 
 /**
  * Gets the product tag list from page.
  * 
  * @param currentPage
  *         the current page
  * @return the product tag list from page
  */
 public static List<String> getProductTagListFromPage(Page currentPage) {
  List<String> qualifiedTagListFromPage = new ArrayList<String>();
  Tag[] pageTags = currentPage.getTags();
  for (Tag pageTag : pageTags) {
   String tagId = pageTag.getTagID();
   
   qualifiedTagListFromPage.add(tagId);
   
  }
  return qualifiedTagListFromPage;
 }
 
 /**
  * This method used to provide global or overridden config for rating and buy it now.
  * 
  * @param properties
  *         the properties
  * @param configMap
  *         the configMap
  * @param productMap
  *         the productMap
  * @return void
  */
 private void getGlobalOrOverridenConfig(Map<String, Object> properties, Map<String, String> configMap, Map<String, Object> productMap,
   boolean disableBuyItNow) {
  String overrideGlobalConfig = MapUtils.getString(properties, UnileverConstants.OVERRIDE_GLOBAL_CONFIG, StringUtils.EMPTY);
  if ("true".equals(overrideGlobalConfig)) {
   productMap.put(PRODUCT_RATING, MapUtils.getBoolean(properties, PRODUCT_RATING) != null ? MapUtils.getBoolean(properties, PRODUCT_RATING) : false);
   if (disableBuyItNow) {
    productMap.put(BUY_IT_NOW, false);
   } else {
    productMap.put(BUY_IT_NOW, MapUtils.getBoolean(properties, BUY_IT_NOW) != null ? MapUtils.getBoolean(properties, BUY_IT_NOW) : false);
   }
  } else {
   Boolean productRating = Boolean.parseBoolean(configMap.get(PRODUCT_RATING));
   Boolean buyItNow = Boolean.parseBoolean(configMap.get(BUY_IT_NOW));
   if (null != productRating) {
    productMap.put(PRODUCT_RATING, productRating);
   }
   if (disableBuyItNow) {
    productMap.put(BUY_IT_NOW, false);
   } else if (buyItNow != null) {
    productMap.put(BUY_IT_NOW, buyItNow);
   }
  }
 }
 
 /**
  * This method used to get all product information from the product page in which user selects.
  * 
  * @param content
  *         the content
  * @param featuredProductProperties
  *         the featured product properties
  * @param pageManager
  *         the page manager
  * @param currentPage
  *         the current page
  * @param category
  *         the category
  * @param action
  *         the action
  * @param resources
  *         the resources
  * @return the products manual
  */
 private Map<String, Object> getProductsManual(ResourceResolver resourceResolver, SlingHttpServletRequest slingRequest, Map<String, Object> content,
   Map<String, Object> featuredProductProperties, Page currentPage, Map<String, Object> resources) {
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  Map<String, Object> productListFinalMap = new LinkedHashMap<String, Object>();
  String jsonNameSpace = MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE);
  Resource currentResource = slingRequest.getResource();
  ValueMap componentProperties = currentResource.getValueMap();
  String componentName = componentProperties.get(COMPONENT_NAME, StringUtils.EMPTY);
  List<Map<String, Object>> productList = new ArrayList<Map<String, Object>>();
  Map<String, Object> productListMap = new LinkedHashMap<String, Object>();
  String tagPage = MapUtils.getString(featuredProductProperties, PRODUCT_PAGE);
  String featuredType = MapUtils.getString(featuredProductProperties, FEATURED_PRODUCT_TYPE);
  Map<String, Object> backgroundImageMap = getImageProperties(featuredProductProperties, currentPage, slingRequest);
  Map<String, String> globalConfigMap = ComponentUtil.getConfigMap(currentPage, GLOBAL_CONFIG_CATEGORY);
  Map<String, Object> ctaMap = new LinkedHashMap<String, Object>();
  String ctaLabel = MapUtils.getString(featuredProductProperties, CTA_LABEL);
  Page currentProductPage = pageManager.getPage(tagPage);
  UNIProduct uniProduct = ProductHelper.getUniProduct(currentProductPage);
  
  String currentProductPagePath = ComponentUtil.getFullURL(resourceResolver, tagPage, slingRequest);
  // container map for analytics
  ctaMap.put(UnileverConstants.URL, currentProductPagePath);
  KritiqueDTO kritiqueparams = new KritiqueDTO(currentPage, configurationService);
  if (currentPage != null && uniProduct != null) {
   productListMap = ProductHelper.getProductMap(uniProduct, componentName, resources, currentPage, productRatingFlag);
   
   if (productListMap != null) {
    LOGGER.debug("created a Map of product for featured product,size of the Map is " + productListMap.size());
   }
   getAdaptiveRating(slingRequest, productListMap, uniProduct, kritiqueparams);
   if (MapUtils.isNotEmpty(productListMap)) {
    productListMap.put(CTA, ctaMap);
    Boolean isNewProduct = ProductHelper.isNewProduct(currentProductPage, currentPage);
    if (isNewProduct) {
     productListMap.put(ProductConstants.ISNEWPRODUCT, i18n.get(ProductConstants.NEW_PRODUCT_LABEL));
    } else {
     productListMap.put(ProductConstants.ISNEWPRODUCT, StringUtils.EMPTY);
    }
   }
   productListFinalMap.put(ProductConstants.REVIEW_MAP, ProductHelper.getReviewMapProperties(configurationService, currentPage));
  }
  if (MapUtils.isNotEmpty(productListMap)) {
   productList.add(productListMap);
  }
  productListFinalMap.put(PRODUCT_LIST, productList.toArray());
  productListFinalMap.put(BACKGROUND, backgroundImageMap);
  String[] featureTagNameSpaces = StringUtils.isNotBlank(MapUtils.getString(featuredProductProperties, FEATURE_TAGS)) ? ComponentUtil
    .getPropertyValueArray(featuredProductProperties, FEATURE_TAGS) : new String[] {};
  LandingPageListHelper.addFeatureTagsData(featureTagNameSpaces, productListFinalMap, currentPage, resources);
  productListFinalMap.put(TYPE, featuredType.toLowerCase());
  productListFinalMap.put(CTA_LABEL, ctaLabel);
  productListFinalMap.put(SUBHEADING_TEXT, featuredProductProperties.get(SUBHEADING_TEXT));
  boolean disableBuyItNow = ProductHelper.isDisableBuyItNow(pageManager.getPage(tagPage), currentPage, configurationService);
  if (buyItNowFlag && !disableBuyItNow) {
   productListFinalMap.put(ProductConstants.SHOPNOW_MAP,
     ProductHelper.getshopNowProperties(configurationService, currentPage, i18n, resourceResolver, slingRequest));
  }
  getGlobalOrOverridenConfig(featuredProductProperties, globalConfigMap, productListFinalMap, disableBuyItNow);
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  data.put(jsonNameSpace, productListFinalMap);
  return data;
 }
 
 /**
  * @param slingRequest
  * @param productListMap
  * @param currentProductPage
  * @param kritiqueparams
  */
 private void getAdaptiveRating(SlingHttpServletRequest slingRequest, Map<String, Object> productListMap, UNIProduct product,
   KritiqueDTO kritiqueparams) {
  if (AdaptiveUtil.isAdaptive(slingRequest)) {
   
   if (product != null) {
    kritiqueparams.setSiteProductId(product.getSKU());
   }
   try {
    List<Map<String, Object>> kqMap = kritiqueReviewsCommentsService.getRatingsService(kritiqueparams, configurationService);
    if (!(kqMap.isEmpty())) {
     Map<String, Object> reviewMap = kqMap.get(0);
     productListMap.put(ProductConstants.PRODUCT_RATINGS_MAP, reviewMap);
    }
    LOGGER.debug("Product list map created successfully for featured product");
   } catch (Exception e) {
    LOGGER.error("Exception while getting kritique response ", e);
   }
  }
 }
 
 /**
  * This method returns an empty string if the key is null or undefined otherwise it returns key.
  * 
  * @param key
  *         the key
  * @return key
  */
 private String checkNullValues(Object key) {
  String value = StringUtils.EMPTY;
  if (null == key) {
   value = StringUtils.EMPTY;
  } else if (StringUtils.EMPTY.equals(key)) {
   value = StringUtils.EMPTY;
  } else {
   value = key.toString();
  }
  
  return value;
  
 }
 
 /**
  * This method is used to get image properties like path,alt text for image extension etc.
  * 
  * @param slingRequest
  * 
  * @param altText
  *         the alt text
  * @param imageOne
  *         the image one
  * @param imageOneArray
  *         the image one array
  * @return the image properties
  */
 
 private Map<String, Object> getImageProperties(Map<String, Object> properties, Page currentPage, SlingHttpServletRequest slingRequest) {
  Map<String, Object> backgroundImageMap = new LinkedHashMap<String, Object>();
  String backgroundImage = checkNullValues(MapUtils.getString(properties, FEATURED_BACKGROUND_IMAGE));
  Image productBackgroundImage = new Image(backgroundImage, MapUtils.getString(properties, FEATURED_BACKGROUND_IMAGE_ALT_TEXT, StringUtils.EMPTY), "", currentPage, slingRequest);
  backgroundImageMap = productBackgroundImage.convertToMap();
  return backgroundImageMap;
  
 }
 
 /**
  * Gets the config service.
  * 
  * @return configurationService
  */
 protected ConfigurationService getConfigService() {
  
  return this.configurationService;
 }
 
 /**
  * Gets the search service.
  * 
  * @return searchService
  */
 protected SearchService getSearchService() {
  
  return this.searchService;
 }
 
 /**
  * @param slingRequest
  * @param list
  * @param productMap
  * @param ratingFlag
  * @param productpage
  * @param ratingMap
  * @param kritiqueparams
  * @param listwithrating
  * @return
  */
 private boolean getKQForFeaturePhone(SlingHttpServletRequest slingRequest, List<Map<String, Object>> list, Map<String, Object> productMap,
   Page productpage, KritiqueDTO kritiqueparams, List<Map<String, Object>> listwithrating) {
  boolean ratingFlag = false;
  if (AdaptiveUtil.isAdaptive(slingRequest)) {
   
   UNIProduct product = null;
   Product pageProduct = CommerceHelper.findCurrentProduct(productpage);
   if (pageProduct instanceof UNIProduct) {
    product = (UNIProduct) pageProduct;
   }
   LOGGER.info("Feature phone view called and product page path is " + productpage.getPath());
   if (pageProduct != null && product != null) {
    kritiqueparams.setSiteProductId(product.getSKU());
   }
   Map<String, Object> ratingMap = null;
   for (int i = 0; i < list.size(); i++) {
    ratingMap = list.get(i);
   }
   try {
    List<Map<String, Object>> kqMap = kritiqueReviewsCommentsService.getRatingsService(kritiqueparams, configurationService);
    if (!(kqMap.isEmpty())) {
     ratingMap.put(ProductConstants.PRODUCT_RATINGS_MAP, kqMap.get(0));
     listwithrating.add(ratingMap);
     ratingFlag = true;
     LOGGER.debug("List created successfully for featured product and size of the list is " + listwithrating.size());
     productMap.put(ProductConstants.PRODUCT_LIST_MAP, listwithrating);
    }
   } catch (Exception e) {
    LOGGER.error("Exception while getting kritique response ", e);
   }
   
  }
  return ratingFlag;
 }
 
}
