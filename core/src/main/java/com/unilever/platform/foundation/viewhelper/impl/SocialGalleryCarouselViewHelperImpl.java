/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.SocialTABHelper;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.SocialTabQueryDTO;

/**
 * <p>
 * Responsible for reading author input for Social Gallery Carousel and generating a list of Tweet Channel List in pageContext for the view.
 * 
 * </p>
 */
@Component(description = "SocialGalleryCarouselViewHelperImpl", immediate = true, metatype = true, label = "SocialGalleryCarouselViewHelperImpl")
@Service(value = { SocialGalleryCarouselViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.SOCIAL_GALLERY_CAROUSEL, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class SocialGalleryCarouselViewHelperImpl extends SocialTabBaseViewHelperImpl {
 
 /**
  * Constant for title.
  */
 private static final String TITLE = "title";
 
 /**
  * Constant for Introduction Copy.
  */
 private static final String INTRO_COPY = "introCopy";
 
 /**
  * Constant for cta.
  */
 private static final String CTA = "cta";
 
 /**
  * Constant for label.
  */
 private static final String LABEL = "label";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(SocialGalleryCarouselViewHelperImpl.class);
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.impl.SocialTabBaseViewHelperImpl#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.info("Social Gallery Carousel View Helper #processData called.");
  
  Map<String, Object> properties = getProperties(content);
  Page currentPage = getCurrentPage(resources);
  SocialTabQueryDTO tabQuery = new SocialTabQueryDTO();
  
  Map<String, Object> socialGalleryCarouselMap = new LinkedHashMap<String, Object>();
  
  String socialTitle = getTitle(properties);
  String introCopy = getText(properties);
  String labelText = getCtaText(properties);
  String linkText = getCtaUrl(properties, resources);
  String newWindowCta = MapUtils.getString(properties, OPEN_IN_NEW_WINDOW, StringUtils.EMPTY);
  
  int numberOfPosts = Integer.parseInt(GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
    UnileverConstants.SOCIAL_GALLERY_CAROUSEL_TAB_CONFIG_CAT, NUMBER_OF_POSTS));
  int maxImages = Integer.parseInt(GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
    UnileverConstants.SOCIAL_GALLERY_CAROUSEL_TAB_CONFIG_CAT, MAXIMUM_IMAGES));
  
  tabQuery.setBrandNameParameter(getBrandNameParameter(configurationService, currentPage));
  tabQuery.setConfigurationService(configurationService);
  tabQuery.setHashTags(getHashTags(getCurrentResource(resources)));
  tabQuery.setMaxImages(maxImages);
  tabQuery.setNoOfPost(numberOfPosts);
  tabQuery.setPage(currentPage);
  tabQuery.setPagePath(getResourceResolver(resources).map(currentPage.getPath()));
  tabQuery.setSocialChannels(getSocialChannels(properties));
  
  socialGalleryCarouselMap.put(TITLE, socialTitle);
  socialGalleryCarouselMap.put(INTRO_COPY, introCopy);
  
  addCTAMap(socialGalleryCarouselMap, labelText, linkText, newWindowCta);
  
  // adding retrieval query parameters
  SocialTABHelper.addRetrievalQueryParametrsMap(socialGalleryCarouselMap, tabQuery);
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  data.put(getJsonNameSpace(content), socialGalleryCarouselMap);
  
  return data;
 }
 
 /**
  * Adds the cta map.
  * 
  * @param socialGalleryCarouselMap
  *         the social gallery carousel map
  * @param labelText
  *         the label text
  * @param linkText
  *         the link text
  * @param newWindowCta
  *         the new window cta
  */
 private static void addCTAMap(Map<String, Object> socialGalleryCarouselMap, String labelText, String linkText, String newWindowCta) {
  Map<String, Object> ctaMap = new HashMap<String, Object>();
  
  ctaMap.put(LABEL, labelText);
  ctaMap.put(URL, linkText);
  if (newWindowCta.equals(BOOLEAN_TRUE)) {
   ctaMap.put(OPEN_IN_NEW_WINDOW, Boolean.valueOf(BOOLEAN_TRUE));
  } else {
   ctaMap.put(OPEN_IN_NEW_WINDOW, Boolean.valueOf(BOOLEAN_FALSE));
  }
  
  socialGalleryCarouselMap.put(CTA, ctaMap);
 }
}
