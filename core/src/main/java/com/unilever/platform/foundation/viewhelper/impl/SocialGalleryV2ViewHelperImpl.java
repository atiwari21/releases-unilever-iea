/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.Image;
import com.unilever.platform.foundation.template.dto.SocialTabQueryDTO;
import com.unilever.platform.foundation.viewhelper.constants.SocialGalleryV2Constants;

/**
 * The Class SocialGalleryV2ViewHelperImpl.
 */
@Component(description = "SocialGalleryV2ViewHelperImpl", immediate = true, metatype = true, label = "SocialGalleryV2ViewHelperImpl")
@Service(value = { SocialGalleryV2ViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = "/apps/unilever-iea/components/socialGalleryV2", propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class SocialGalleryV2ViewHelperImpl extends SocialTabBaseViewHelperImpl {
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(SocialGalleryV2ViewHelperImpl.class);
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.impl.SocialTabBaseViewHelperImpl #processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.debug("SocialGalleryV2ViewHelperImpl #processData called .");
  Map<String, Object> properties = getProperties(content);
  Map<String, Object> socialGalleryV2Map = new LinkedHashMap<String, Object>();
  List<Map<String, Object>> socialGalleryFinalList = new ArrayList<Map<String, Object>>();
  
  Resource currentResource = getCurrentResource(resources);
  Page currentPage = getCurrentPage(resources);
  
  String aggregationMode = MapUtils.getString(properties, SocialGalleryV2Constants.AGGREGATION_MODE, StringUtils.EMPTY);
  String aggregatorServiceUrl = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
    UnileverConstants.SOCIAL_AGGREGATOR_SERVICE, "url");
  
  socialGalleryV2Map.put("pagination", getPaginationProperties(properties, resources));
  
  SocialTabQueryDTO tabQuery = new SocialTabQueryDTO();
  tabQuery.setBrandNameParameter(getBrandNameParameter(configurationService, currentPage));
  tabQuery.setConfigurationService(configurationService);
  tabQuery.setHashTags(getHashTagListMap(properties, currentResource));
  tabQuery.setPage(currentPage);
  tabQuery.setSocialChannels(getSocialChannelsNames(properties, currentResource));
  tabQuery.setPagePath(getResourceResolver(resources).map(aggregatorServiceUrl));
  
  addRetrievalQueryParametrsMap(socialGalleryV2Map, tabQuery, true, aggregationMode, properties, resources);
  
  socialGalleryV2Map.put(SocialGalleryV2Constants.STATIC, getGeneralConfigurationDataMap(properties, resources, getI18n(resources)));
  if (SocialGalleryV2Constants.FIXED_LIST.equals(aggregationMode)) {
   addSocialMapFixedList(currentResource, socialGalleryFinalList, resources);
  } else if (SocialGalleryV2Constants.AUTOMATED_LIST.equals(aggregationMode)) {
   
   if (MapUtils.getBoolean(properties, SocialGalleryV2Constants.FACEBOOK, false)) {
    socialGalleryFinalList.addAll(getFaceBookSocialMap(currentResource, resources));
   }
   if (MapUtils.getBoolean(properties, SocialGalleryV2Constants.TWITTER, false)) {
    socialGalleryFinalList.addAll(getTwitterSocialMap(currentResource, resources));
   }
   if (MapUtils.getBoolean(properties, SocialGalleryV2Constants.INSTAGRAM, false)) {
    socialGalleryFinalList.addAll(getInstagramSocialMap(currentResource, resources));
   }
  }
  socialGalleryV2Map.put(SocialGalleryV2Constants.SOCIAL_CONFIGURATION_KEY, socialGalleryFinalList.toArray());
  Map<String, Object> finalDataMap = new HashMap<String, Object>();
  finalDataMap.put(getJsonNameSpace(content), socialGalleryV2Map);
  return finalDataMap;
 }
 
 /**
  * Gets the general configuration data map.
  * 
  * @param properties
  *         the properties
  * @param i18n
  *         the i18n
  * @return the general configuration data map
  */
 private static Map<String, Object> getGeneralConfigurationDataMap(Map<String, Object> properties, Map<String, Object> resources, I18n i18n) {
  Map<String, Object> generalConfigDataMap = new LinkedHashMap<String, Object>();
  ResourceResolver resourceResolver = getResourceResolver(resources);
  generalConfigDataMap.put(SocialGalleryV2Constants.HEADING_TEXT_KEY,
    MapUtils.getString(properties, SocialGalleryV2Constants.HEADING_TEXT, StringUtils.EMPTY));
  generalConfigDataMap.put(SocialGalleryV2Constants.SUBHEADING_TEXT_KEY,
    MapUtils.getString(properties, SocialGalleryV2Constants.SUB_HEADING_TEXT, StringUtils.EMPTY));
  generalConfigDataMap.put(SocialGalleryV2Constants.LONG_SUBHEADING_TEXT,
    MapUtils.getString(properties, SocialGalleryV2Constants.TEXT, StringUtils.EMPTY));
  generalConfigDataMap.put(SocialGalleryV2Constants.CTA_LABEL_KEY,
    MapUtils.getString(properties, SocialGalleryV2Constants.CTA_LABEL, StringUtils.EMPTY));
  generalConfigDataMap.put("ctaUrl", ComponentUtil.getFullURL(resourceResolver,
    MapUtils.getString(properties, SocialGalleryV2Constants.CTA_URL, StringUtils.EMPTY), getSlingRequest(resources)));
  generalConfigDataMap.put(SocialGalleryV2Constants.OPEN_IN_NEW_WINDOW_KEY, MapUtils.getBoolean(properties, OPEN_IN_NEW_WINDOW, false));
  generalConfigDataMap.put(SocialGalleryV2Constants.AGGREGATION_MODE_KEY, MapUtils.getString(properties, SocialGalleryV2Constants.AGGREGATION_MODE));
  generalConfigDataMap.put("closeLabel", i18n.get("socialGalleryVideo.closeLabel"));
  generalConfigDataMap.put(SocialGalleryV2Constants.SOCIAL_POST_LIKES_SUFFIX,
    i18n.get(SocialGalleryV2Constants.SOCIAL_GALLERY_SOCIAL_POST_LIKES_SUFFIX));
  generalConfigDataMap.put(SocialGalleryV2Constants.VIEW_ARTICLE_HEADING_TEXT,
    i18n.get(SocialGalleryV2Constants.SOCIAL_GALLERY_VIEW_ARTICLE_HEADING_TEXT));
  generalConfigDataMap.put(SocialGalleryV2Constants.VIEW_ARTICLE_CTA_LABEL, i18n.get(SocialGalleryV2Constants.SOCIAL_GALLERY_VIEW_ARTICLE_CTA_LABEL));
  generalConfigDataMap.put(SocialGalleryV2Constants.VIEW_PRODUCT_HEADING_TEXT,
    i18n.get(SocialGalleryV2Constants.SOCIAL_GALLERY_VIEW_PRODUCT_HEADING_TEXT));
  generalConfigDataMap.put(SocialGalleryV2Constants.VIEW_PRODUCT_CTA_LABEL, i18n.get(SocialGalleryV2Constants.SOCIAL_GALLERY_VIEW_PRODUCT_CTA_LABEL));
  return generalConfigDataMap;
  
 }
 
 /**
  * Gets the face book social map.
  * 
  * @param currentResource
  *         the current resource
  * @param resources
  *         the resources
  * 
  * @return the face book social map
  */
 private List<Map<String, Object>> getFaceBookSocialMap(Resource currentResource, Map<String, Object> resources) {
  Page page = getCurrentPage(resources);
  List<Map<String, Object>> linkPostsListFinalMap = new ArrayList<Map<String, Object>>();
  List<Map<String, String>> linkPostsList = ComponentUtil.getNestedMultiFieldProperties(currentResource, "linkPostsFacebookJson");
  for (Map<String, String> map : linkPostsList) {
   Map<String, Object> linkPostToContentProperties = new LinkedHashMap<String, Object>();
   String pagePath = MapUtils.getString(map, SocialGalleryV2Constants.LINK_POST_TO_FACEBOOK, StringUtils.EMPTY);
   
   Page associatedPage = page.getPageManager().getPage(pagePath);
   linkPostToContentProperties.put(SocialGalleryV2Constants.SOCIAL_CHANNELS, "Facebook");
   linkPostToContentProperties.put(SocialGalleryV2Constants.POST_ID_KEY,
     MapUtils.getString(map, SocialGalleryV2Constants.POST_ID_FACEBOOK, StringUtils.EMPTY));
   String linkType = MapUtils.getString(map, SocialGalleryV2Constants.LINK_TYPE_FACEBOOK, StringUtils.EMPTY);
   linkPostToContentProperties.put(SocialGalleryV2Constants.LINK_TYPE, linkType);
   if (SocialGalleryV2Constants.CUSTOM.equals(linkType)) {
    linkPostToContentProperties.put(SocialGalleryV2Constants.HEADING_TEXT_KEY,
      MapUtils.getString(map, SocialGalleryV2Constants.HEADING_TEXT_CUSTOM_FACEBOOK, StringUtils.EMPTY));
    linkPostToContentProperties.put(SocialGalleryV2Constants.CTA_LABEL_KEY,
      MapUtils.getString(map, SocialGalleryV2Constants.CTA_LABEL_CUSTOM_FACEBOOK, StringUtils.EMPTY));
   }
   addCTAMap(linkPostToContentProperties, linkType, pagePath, associatedPage, getI18n(resources), resources);
   Boolean openInNewWindow = false;
   if (SocialGalleryV2Constants.TRUE.equals(MapUtils.getString(map, SocialGalleryV2Constants.OPEN_IN_NEW_WINDOW_FACEBOOK))) {
    openInNewWindow = true;
   }
   linkPostToContentProperties.put(SocialGalleryV2Constants.OPEN_IN_NEW_WINDOW_KEY, openInNewWindow);
   linkPostsListFinalMap.add(linkPostToContentProperties);
   
  }
  
  return linkPostsListFinalMap;
 }
 
 /**
  * Gets the twitter social map.
  * 
  * @param currentResource
  *         the current resource
  * @param resources
  *         the resources
  * 
  * @return the twitter social map
  */
 private List<Map<String, Object>> getTwitterSocialMap(Resource currentResource, Map<String, Object> resources) {
  Page page = getCurrentPage(resources);
  List<Map<String, Object>> linkPostsListFinalMap = new ArrayList<Map<String, Object>>();
  
  List<Map<String, String>> linkPostsList = ComponentUtil.getNestedMultiFieldProperties(currentResource, "linkPostsTwitterJson");
  
  for (Map<String, String> map : linkPostsList) {
   Map<String, Object> linkPostToContentProperties = new LinkedHashMap<String, Object>();
   String pagePath = MapUtils.getString(map, "linkPostToTwitter", StringUtils.EMPTY);
   
   Page associatedPage = page.getPageManager().getPage(pagePath);
   linkPostToContentProperties.put(SocialGalleryV2Constants.SOCIAL_CHANNELS, "Twitter");
   linkPostToContentProperties.put(SocialGalleryV2Constants.POST_ID_KEY, MapUtils.getString(map, "postIdTwitter", StringUtils.EMPTY));
   linkPostToContentProperties.put(SocialGalleryV2Constants.LINK_TYPE_KEY, MapUtils.getString(map, "linkTypeTwitter", StringUtils.EMPTY));
   String linkType = MapUtils.getString(map, "linkTypeTwitter", StringUtils.EMPTY);
   linkPostToContentProperties.put(SocialGalleryV2Constants.LINK_TYPE_KEY, linkType);
   if (SocialGalleryV2Constants.CUSTOM.equals(linkType)) {
    linkPostToContentProperties.put("headingText", MapUtils.getString(map, "headingTextCustomTwitter", StringUtils.EMPTY));
    linkPostToContentProperties.put(SocialGalleryV2Constants.CTA_LABEL_KEY, MapUtils.getString(map, "ctaLabelCustomTwitter", StringUtils.EMPTY));
   }
   addCTAMap(linkPostToContentProperties, linkType, pagePath, associatedPage, getI18n(resources), resources);
   Boolean openInNewWindow = false;
   if (SocialGalleryV2Constants.TRUE.equals(MapUtils.getString(map, SocialGalleryV2Constants.OPEN_IN_NEW_WINDOW_TWITTER))) {
    openInNewWindow = true;
   }
   linkPostToContentProperties.put(SocialGalleryV2Constants.OPEN_IN_NEW_WINDOW_KEY, openInNewWindow);
   linkPostsListFinalMap.add(linkPostToContentProperties);
   
  }
  
  return linkPostsListFinalMap;
 }
 
 /**
  * Gets the instagram social map.
  * 
  * @param currentResource
  *         the current resource
  * @param resources
  *         the resources
  * 
  * @return the instagram social map
  */
 private List<Map<String, Object>> getInstagramSocialMap(Resource currentResource, Map<String, Object> resources) {
  Page page = getCurrentPage(resources);
  List<Map<String, Object>> linkPostsListFinalMap = new ArrayList<Map<String, Object>>();
  List<Map<String, String>> linkPostsList = ComponentUtil.getNestedMultiFieldProperties(currentResource, "linkPostsInstagramJson");
  for (Map<String, String> map : linkPostsList) {
   Map<String, Object> linkPostToContentProperties = new LinkedHashMap<String, Object>();
   String pagePath = MapUtils.getString(map, SocialGalleryV2Constants.LINK_POST_TO_INSTAGRAM, StringUtils.EMPTY);
   Page associatedPage = page.getPageManager().getPage(pagePath);
   linkPostToContentProperties.put(SocialGalleryV2Constants.SOCIAL_CHANNELS, "Instagram");
   linkPostToContentProperties.put(SocialGalleryV2Constants.POST_ID_KEY,
     MapUtils.getString(map, SocialGalleryV2Constants.POST_ID_INSTAGRAM, StringUtils.EMPTY));
   linkPostToContentProperties.put(SocialGalleryV2Constants.LINK_TYPE_KEY,
     MapUtils.getString(map, SocialGalleryV2Constants.LINK_TYPE_INSTAGRAM, StringUtils.EMPTY));
   String linkType = MapUtils.getString(map, SocialGalleryV2Constants.LINK_TYPE_INSTAGRAM, StringUtils.EMPTY);
   linkPostToContentProperties.put(SocialGalleryV2Constants.LINK_TYPE_KEY, linkType);
   addCTAMap(linkPostToContentProperties, linkType, pagePath, associatedPage, getI18n(resources), resources);
   if (SocialGalleryV2Constants.CUSTOM.equals(linkType)) {
    linkPostToContentProperties.put(SocialGalleryV2Constants.HEADING_TEXT_KEY,
      MapUtils.getString(map, SocialGalleryV2Constants.HEADING_TEXT_CUSTOM_INSTAGRAM, StringUtils.EMPTY));
    linkPostToContentProperties.put(SocialGalleryV2Constants.CTA_LABEL_KEY,
      MapUtils.getString(map, SocialGalleryV2Constants.CTA_LABEL_CUSTOM_INSTAGRAM, StringUtils.EMPTY));
   }
   Boolean openInNewWindow = false;
   if (SocialGalleryV2Constants.TRUE.equals(MapUtils.getString(map, SocialGalleryV2Constants.OPEN_IN_NEW_WINDOW_INSTAGRAM))) {
    openInNewWindow = true;
   }
   linkPostToContentProperties.put(SocialGalleryV2Constants.OPEN_IN_NEW_WINDOW_KEY, openInNewWindow);
   linkPostsListFinalMap.add(linkPostToContentProperties);
  }
  return linkPostsListFinalMap;
 }
 
 /**
  * Adds the social map fixed list.
  * 
  * @param currentResource
  *         the current resource
  * @param socialGalleryFinalList
  *         the social gallery final list
  * @param resources
  *         the resources
  */
 private void addSocialMapFixedList(Resource currentResource, List<Map<String, Object>> socialGalleryFinalList, Map<String, Object> resources) {
  List<Map<String, String>> list = ComponentUtil.getNestedMultiFieldProperties(currentResource, SocialGalleryV2Constants.SOCIAL_POST_TO_DISPLAY_JSON);
  
  Page page = getCurrentPage(resources);
  for (Map<String, String> map : list) {
   Map<String, Object> socialMapFixedProperties = new LinkedHashMap<String, Object>();
   socialMapFixedProperties.put(SocialGalleryV2Constants.SOCIAL_CHANNELS,
     MapUtils.getString(map, SocialGalleryV2Constants.SOCIAL_CHANNELS_FIXED, StringUtils.EMPTY));
   socialMapFixedProperties.put(SocialGalleryV2Constants.POST_ID_KEY,
     MapUtils.getString(map, SocialGalleryV2Constants.POST_ID_FIXED, StringUtils.EMPTY));
   
   String pagePath = MapUtils.getString(map, SocialGalleryV2Constants.LINK_POST_TO_FIXED, StringUtils.EMPTY);
   
   Page associatedPage = page.getPageManager().getPage(pagePath);
   String linkTypeFixed = MapUtils.getString(map, SocialGalleryV2Constants.LINK_TYPE_FIXED, StringUtils.EMPTY);
   socialMapFixedProperties.put(SocialGalleryV2Constants.LINK_TYPE_KEY, linkTypeFixed);
   addCTAMap(socialMapFixedProperties, linkTypeFixed, pagePath, associatedPage, getI18n(resources), resources);
   if (SocialGalleryV2Constants.CUSTOM.equals(linkTypeFixed)) {
    socialMapFixedProperties.put(SocialGalleryV2Constants.HEADING_TEXT_KEY,
      MapUtils.getString(map, SocialGalleryV2Constants.HEADING_TEXT_CUSTOM_FIXED, StringUtils.EMPTY));
    socialMapFixedProperties.put(SocialGalleryV2Constants.CTA_LABEL_KEY,
      MapUtils.getString(map, SocialGalleryV2Constants.CTA_LABEL_CUSTOM_FIXED, StringUtils.EMPTY));
   }
   Boolean openInNewWindow = false;
   if (SocialGalleryV2Constants.TRUE.equals(MapUtils.getString(map, SocialGalleryV2Constants.OPEN_IN_NEW_WINDOW_FIXED))) {
    openInNewWindow = true;
   }
   socialMapFixedProperties.put(SocialGalleryV2Constants.OPEN_IN_NEW_WINDOW_KEY, openInNewWindow);
   socialGalleryFinalList.add(socialMapFixedProperties);
  }
  
 }
 
 /**
  * Adds the retrieval query parametrs map.
  * 
  * @param map
  *         the map
  * @param socialTabQuery
  *         the social tab query
  * @param urlFlag
  *         the url flag
  * @param aggregationMode
  *         the aggregation mode
  * @param properties
  *         the properties
  * @param resources
  *         the resources
  */
 public void addRetrievalQueryParametrsMap(Map<String, Object> map, SocialTabQueryDTO socialTabQuery, boolean urlFlag, String aggregationMode,
   Map<String, Object> properties, Map<String, Object> resources) {
  
  Map<String, Object> requestServletUrlMap = new LinkedHashMap<String, Object>();
  
  requestServletUrlMap.put(SocialGalleryV2Constants.BRAND_NAME_PARAMETER, socialTabQuery.getBrandNameParameter());
  setGlobalConfigAttributes(getCurrentPage(resources), requestServletUrlMap, properties);
  if (!urlFlag) {
   requestServletUrlMap.put(SocialGalleryV2Constants.REQUEST_SERVLET_URL, socialTabQuery.getPagePath() + ".damasset.json");
  } else {
   requestServletUrlMap.put(SocialGalleryV2Constants.REQUEST_SERVLET_URL, socialTabQuery.getPagePath());
  }
  if (aggregationMode.equals(SocialGalleryV2Constants.AUTOMATIC)) {
   addHashTagsMap(requestServletUrlMap, socialTabQuery.getHashTags());
  }
  addSocialChannelsMap(requestServletUrlMap, socialTabQuery.getSocialChannels(), socialTabQuery.getPage(), socialTabQuery.getConfigurationService(),
    properties);
  map.put("retrivalQueryParams", requestServletUrlMap);
  
 }
 
 /**
  * Adds the retrieval query parameters map.
  * 
  * @param properties
  *         the properties
  * @param currentResource
  *         the current resource
  * @return the hash tag list map
  */
 
 private List<Map<String, String>> getHashTagListMap(Map<String, Object> properties, Resource currentResource) {
  Boolean facebookEnabled = MapUtils.getBoolean(properties, SocialGalleryV2Constants.FACEBOOK, false);
  Boolean twitterEnabled = MapUtils.getBoolean(properties, SocialGalleryV2Constants.TWITTER, false);
  Boolean instagramEnabled = MapUtils.getBoolean(properties, SocialGalleryV2Constants.INSTAGRAM, false);
  
  List<Map<String, String>> newList = new LinkedList<Map<String, String>>();
  if (facebookEnabled) {
   List<Map<String, String>> facebookHashTagList = ComponentUtil.getNestedMultiFieldProperties(currentResource, "hashTagFacebookJson");
   for (Map<String, String> map : facebookHashTagList) {
    String hashTag = (String) map.get(SocialGalleryV2Constants.HASH_TAG_FACEBOOK);
    map.remove(SocialGalleryV2Constants.HASH_TAG_FACEBOOK);
    map.put(SocialGalleryV2Constants.HASH_TAGS_KEY, hashTag);
    newList.add(map);
   }
  }
  if (twitterEnabled) {
   List<Map<String, String>> twitterHashTagList = ComponentUtil.getNestedMultiFieldProperties(currentResource, "hashTagTwitterJson");
   for (Map<String, String> map : twitterHashTagList) {
    String hashTag = (String) map.get(SocialGalleryV2Constants.HASH_TAG_TWITTER);
    map.remove(SocialGalleryV2Constants.HASH_TAG_TWITTER);
    map.put(SocialGalleryV2Constants.HASH_TAGS_KEY, hashTag);
    newList.add(map);
   }
  }
  if (instagramEnabled) {
   List<Map<String, String>> instagramHashTagList = ComponentUtil.getNestedMultiFieldProperties(currentResource, "hashTagInstagramJson");
   for (Map<String, String> map : instagramHashTagList) {
    String hashTag = (String) map.get(SocialGalleryV2Constants.HASH_TAG_INSTAGRAM);
    map.remove(SocialGalleryV2Constants.HASH_TAG_INSTAGRAM);
    map.put(SocialGalleryV2Constants.HASH_TAGS_KEY, hashTag);
    newList.add(map);
   }
  }
  return newList;
 }
 
 /**
  * Adds the hash tags map.
  * 
  * @param requestServletUrlMap
  *         the request servlet url map
  * @param hashTags
  *         the hash tags
  */
 private static void addHashTagsMap(Map<String, Object> requestServletUrlMap, List<Map<String, String>> hashTags) {
  List<Map<String, Object>> hashTagList = new ArrayList<Map<String, Object>>();
  
  for (Map<String, String> map : hashTags) {
   Map<String, Object> hashTagMap = new LinkedHashMap<String, Object>();
   
   String hashTag = StringUtils.defaultString(map.get(SocialGalleryV2Constants.HASH_TAGS_KEY), StringUtils.EMPTY);
   hashTag = hashTag.trim().replaceFirst("#", "");
   hashTagMap.put(SocialGalleryV2Constants.ID_KEY, hashTag);
   
   hashTagList.add(hashTagMap);
  }
  
  requestServletUrlMap.put(SocialGalleryV2Constants.HASH_TAGS_KEY, hashTagList.toArray());
 }
 
 /**
  * Gets the social channels names.
  * 
  * @param properties
  *         the properties
  * @return the social channels names
  */
 private static List<String> getSocialChannelsNames(Map<String, Object> properties, Resource currentResource) {
  List<String> channelNameList = new ArrayList<String>();
  Boolean facebookEnabled = MapUtils.getBoolean(properties, SocialGalleryV2Constants.FACEBOOK, false);
  Boolean twitterEnabled = MapUtils.getBoolean(properties, SocialGalleryV2Constants.TWITTER, false);
  Boolean instagramEnabled = MapUtils.getBoolean(properties, SocialGalleryV2Constants.INSTAGRAM, false);
  String aggregationMode = MapUtils.getString(properties, SocialGalleryV2Constants.AGGREGATION_MODE, StringUtils.EMPTY);
  if (SocialGalleryV2Constants.FIXED_LIST.equals(aggregationMode)) {
   channelNameList = addSocialChannelNamesFixed(currentResource);
  } else {
   if (facebookEnabled) {
    
    channelNameList.add("Facebook");
   }
   if (twitterEnabled) {
    channelNameList.add("Twitter");
   }
   if (instagramEnabled) {
    channelNameList.add("Instagram");
   }
  }
  return channelNameList;
 }
 
 private static List<String> addSocialChannelNamesFixed(Resource currentResource) {
  List<String> channelNameList = new ArrayList<String>();
  List<Map<String, String>> list = ComponentUtil.getNestedMultiFieldProperties(currentResource, SocialGalleryV2Constants.SOCIAL_POST_TO_DISPLAY_JSON);
  
  for (Map<String, String> map : list) {
   channelNameList.add(MapUtils.getString(map, SocialGalleryV2Constants.SOCIAL_CHANNELS_FIXED, StringUtils.EMPTY));
  }
  List<String> newList = new ArrayList<String>(new HashSet<String>(channelNameList));
  return newList;
 }
 
 /**
  * Adds the social channels map.
  * 
  * @param requestServletUrlMap
  *         the request servlet url map
  * @param socialChannels
  *         the social channels
  * @param currentPage
  *         the current page
  * @param configurationService
  *         the configuration service
  * @param properties
  *         the properties
  */
 public static void addSocialChannelsMap(Map<String, Object> requestServletUrlMap, List<String> socialChannels, Page currentPage,
   ConfigurationService configurationService, Map<String, Object> properties) {
  List<Map<String, Object>> socialChannelsList = new ArrayList<Map<String, Object>>();
  
  for (String channel : socialChannels) {
   socialChannelsList.add(getSocialChannelMap(channel.toLowerCase(), currentPage, configurationService, properties));
  }
  requestServletUrlMap.put(SocialGalleryV2Constants.SOCIAL_CHANNEL_NAMES, socialChannelsList.toArray());
 }
 
 /**
  * Gets the social channel map.
  * 
  * @param channel
  *         the channel
  * @param currentPage
  *         the current page
  * @param configurationService
  *         the configuration service
  * @param properties
  *         the properties
  * @return the social channel map
  */
 public static Map<String, Object> getSocialChannelMap(String channel, Page currentPage, ConfigurationService configurationService,
   Map<String, Object> properties) {
  Map<String, Object> socialChannelsMap = new LinkedHashMap<String, Object>();
  Map<String, String> channelsConfigs = GlobalConfigurationUtility.getMapFromConfiguration(configurationService, currentPage, channel.toLowerCase());
  
  socialChannelsMap.put(SocialGalleryV2Constants.NAME_KEY, channel);
  socialChannelsMap.put("url", channelsConfigs.get("Url"));
  socialChannelsMap.put("imagePath", channelsConfigs.get("Logo"));
  if ("facebook".equals(channel)) {
   socialChannelsMap.put(SocialGalleryV2Constants.ACCOUNT_ID_KEY, MapUtils.getString(properties, "facebookAccountId", StringUtils.EMPTY));
  } else if ("twitter".equals(channel)) {
   socialChannelsMap.put(SocialGalleryV2Constants.ACCOUNT_ID_KEY, MapUtils.getString(properties, "twitterAccountId", StringUtils.EMPTY));
  } else if ("instagram".equals(channel)) {
   socialChannelsMap.put(SocialGalleryV2Constants.ACCOUNT_ID_KEY, MapUtils.getString(properties, "instagramAccountId", StringUtils.EMPTY));
  }
  return socialChannelsMap;
 }
 
 /**
  * Gets the pagination properties.
  * 
  * @param properties
  *         the properties
  * @param resources
  *         the resources
  * @return the pagination properties
  */
 public Map<String, Object> getPaginationProperties(Map<String, Object> properties, Map<String, Object> resources) {
  Map<String, Object> paginationMap = new LinkedHashMap<String, Object>();
  String paginationType = MapUtils.getString(properties, SocialGalleryV2Constants.PAGINATION_TYPE, StringUtils.EMPTY);
  paginationMap.put(SocialGalleryV2Constants.PAGINATION_TYPE,
    MapUtils.getString(properties, SocialGalleryV2Constants.PAGINATION_TYPE, StringUtils.EMPTY));
  paginationMap.put(SocialGalleryV2Constants.PAGINATION_TYPE,
    MapUtils.getString(properties, SocialGalleryV2Constants.PAGINATION_TYPE, StringUtils.EMPTY));
  paginationMap.put(SocialGalleryV2Constants.ITEMS_PER_PAGE2,
    Integer.parseInt(MapUtils.getString(properties, SocialGalleryV2Constants.ITEMS_PER_PAGE2, StringUtils.EMPTY)));
  String paginationCTALabel = MapUtils.getString(properties, SocialGalleryV2Constants.PAGINATION_CTA_LABEL, StringUtils.EMPTY);
  if ("pagination".equals(paginationType)) {
   paginationMap.put(SocialGalleryV2Constants.STATIC, getStaticData(getI18n(resources), paginationCTALabel));
  } else {
   Map<String, Object> staticMap = new LinkedHashMap<String, Object>();
   if ("".equals(paginationCTALabel)) {
    staticMap
      .put(SocialGalleryV2Constants.PAGINATION_CTA_LABEL, getI18n(resources).get(SocialGalleryV2Constants.SOCIAL_GALLERY_PAGINATION_CTA_LABEL));
   } else {
    staticMap.put(SocialGalleryV2Constants.PAGINATION_CTA_LABEL, paginationCTALabel);
   }
   paginationMap.put(SocialGalleryV2Constants.STATIC, staticMap);
  }
  return paginationMap;
 }
 
 /**
  * Gets the static data.
  * 
  * @param i18n
  *         the i18n
  * @return the static data
  */
 private Map<String, Object> getStaticData(I18n i18n, String paginationCTALabel) {
  Map<String, Object> staticMap = new LinkedHashMap<String, Object>();
  staticMap.put(SocialGalleryV2Constants.FIRST_LABEL, i18n.get(SocialGalleryV2Constants.PAGE_LISTING_PAGINATION_FIRST));
  staticMap.put(SocialGalleryV2Constants.LAST_LABEL, i18n.get(SocialGalleryV2Constants.PAGE_LISTING_PAGINATION_LAST));
  staticMap.put(SocialGalleryV2Constants.PREVIOUS_LABEL, i18n.get(SocialGalleryV2Constants.PAGE_LISTING_PAGINATION_PREVIOUS));
  staticMap.put(SocialGalleryV2Constants.NEXT_LABEL, i18n.get(SocialGalleryV2Constants.PAGE_LISTING_PAGINATION_NEXT));
  if ("".equals(paginationCTALabel)) {
   staticMap.put(SocialGalleryV2Constants.PAGINATION_CTA_LABEL, i18n.get(SocialGalleryV2Constants.SOCIAL_GALLERY_PAGINATION_CTA_LABEL));
  } else {
   staticMap.put(SocialGalleryV2Constants.PAGINATION_CTA_LABEL, paginationCTALabel);
  }
  return staticMap;
 }
 
 /**
  * Sets the global config attributes.
  * 
  * @param currentPage
  *         the current page
  * @param socialGalleryTabV2Map
  *         the social gallery tab v2 map
  * @param properties
  *         the properties
  */
 private void setGlobalConfigAttributes(Page currentPage, Map<String, Object> socialGalleryTabV2Map, Map<String, Object> properties) {
  boolean overrideGlobalConfig = MapUtils.getBoolean(properties, UnileverConstants.OVERRIDE_GLOBAL_CONFIG) != null ? MapUtils.getBoolean(properties,
    UnileverConstants.OVERRIDE_GLOBAL_CONFIG) : false;
  
  int limitResultsTo;
  boolean expandablePanelFlag = false;
  
  if (overrideGlobalConfig) {
   limitResultsTo = MapUtils.getIntValue(properties, SocialGalleryV2Constants.LIMIT, 0);
   expandablePanelFlag = MapUtils.getBoolean(properties, SocialGalleryV2Constants.EXPANDABLE_PANEL) != null ? MapUtils.getBoolean(properties,
     SocialGalleryV2Constants.EXPANDABLE_PANEL) : false;
  } else {
   limitResultsTo = GlobalConfigurationUtility.getIntegerValueFromConfiguration(SocialGalleryV2Constants.GLOBAL_CONFIG, configurationService,
     currentPage, SocialGalleryV2Constants.LIMIT_RESULTS_TO);
   expandablePanelFlag = Boolean.parseBoolean(GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage,
     SocialGalleryV2Constants.GLOBAL_CONFIG, SocialGalleryV2Constants.EXPANDABLE_PANEL_FLAG));
  }
  socialGalleryTabV2Map.put(SocialGalleryV2Constants.LIMIT_RESULTS_TO, limitResultsTo);
  socialGalleryTabV2Map.put(SocialGalleryV2Constants.EXPANDABLE_PANEL_FLAG, expandablePanelFlag);
 }
 
 /**
  * Adds the cta map.
  * 
  * @param socialCarouselMap
  *         the social carousel map
  * @param ctaType
  *         the cta type
  * @param pagePath
  *         the page path
  * @param associatedPage
  *         the associated page
  * @param i18n
  *         the i18n
  * @param resources
  *         the resources
  */
 private void addCTAMap(Map<String, Object> socialCarouselMap, String ctaType, String pagePath, Page associatedPage, I18n i18n,
   Map<String, Object> resources) {
  socialCarouselMap.put(SocialGalleryV2Constants.POST_LINKED_TO_URL,
    ComponentUtil.getFullURL(getResourceResolver(resources), pagePath, getSlingRequest(resources)));
  Map<String, Object> productMap = new HashMap<String, Object>();
  if ("article".equals(ctaType)) {
   socialCarouselMap.put(SocialGalleryV2Constants.CTA_LABEL_KEY, i18n.get("socialGalleryV2.viewArticleCtaLabel"));
   socialCarouselMap.put(SocialGalleryV2Constants.HEADING_TEXT_KEY, i18n.get("socialGalleryV2.viewArticleHeadingText"));
   socialCarouselMap.put(SocialGalleryV2Constants.ASSOCIATED_PAGE, getArticleMap(associatedPage, resources));
  } else if ("product".equals(ctaType)) {
   socialCarouselMap.put(SocialGalleryV2Constants.CTA_LABEL_KEY, i18n.get("socialGalleryV2.viewProductCtaLabel"));
   socialCarouselMap.put(SocialGalleryV2Constants.HEADING_TEXT_KEY, i18n.get("socialGalleryV2.viewProductHeadingText"));
   if (null != associatedPage) {
    socialCarouselMap.put(SocialGalleryV2Constants.ASSOCIATED_PAGE, getProductMap(associatedPage, resources));
   } else {
    socialCarouselMap.put(SocialGalleryV2Constants.ASSOCIATED_PAGE, productMap);
   }
  } else {
   socialCarouselMap.put(SocialGalleryV2Constants.ASSOCIATED_PAGE, getArticleMap(associatedPage, resources));
  }
 }
 
 /**
  * Gets the article map.
  * 
  * @param articlePage
  *         the article page
  * @param resources
  *         the resources
  * @return the article map
  */
 private Map<String, Object> getArticleMap(Page articlePage, Map<String, Object> resources) {
  Map<String, Object> articleMap = new HashMap<String, Object>();
  Image image = null;
  
  if (null != articlePage) {
   Map<String, Object> pageProperties = articlePage.getProperties();
   String imagePath = (String) pageProperties.get(SocialGalleryV2Constants.TEASER_IMAGE) != null ? (String) pageProperties
     .get(SocialGalleryV2Constants.TEASER_IMAGE) : StringUtils.EMPTY;
   String altText = (String) pageProperties.get(SocialGalleryV2Constants.TEASER_ALT_TEXT) != null ? (String) pageProperties
     .get(SocialGalleryV2Constants.TEASER_ALT_TEXT) : StringUtils.EMPTY;
   String title = (String) pageProperties.get(SocialGalleryV2Constants.TEASER_TITLE) != null ? (String) pageProperties
     .get(SocialGalleryV2Constants.TEASER_TITLE) : StringUtils.EMPTY;
   image = new Image(imagePath, altText, title, articlePage, getSlingRequest(resources));
   articleMap.put(SocialGalleryV2Constants.NAME_KEY, articlePage.getName());
  }
  if (image != null) {
   articleMap.put(SocialGalleryV2Constants.IMAGE, image.convertToMap());
  }
  
  return articleMap;
 }
 
}
