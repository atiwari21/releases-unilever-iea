/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.sapient.platform.iea.aem.core.cache.ComponentViewAndViewHelperCache;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.utils.CollectionUtils;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.CategoryOverviewHelper;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.Image;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * <p>
 * Responsible for reading author input for category overview component and generating a list of collapsible sections setting in pageContext for the
 * view. Input can be manual or reading property attributes from product data.
 * </p>
 */
@Component(description = "SectionNavigationV2ViewHelperImpl", immediate = true, metatype = true, label = "SectionNavigationV2ViewHelperImpl")
@Service(value = { SectionNavigationV2ViewHelperImpl.class, ViewHelper.class })
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.SECTION_NAVIGATION_V2, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class SectionNavigationV2ViewHelperImpl extends BaseViewHelper {
 private static final int ZERO = 0;
 private static final int ONE = 1;
 private static final String LEVEL = "level";
 private static final String LEVELS = "levels";
 private static final String LEVELS_STRING = "levelsString";
 private static final String TOTAL_RESULTS = "totalResults";
 private static final String CONTENT_TYPE_JSON = "contentTypeJson";
 
 private static final String LIST_LINKS = "listLinks";
 
 /**
  * Identifier for sub heading for each categoryOverview column.
  */
 private static final String SUB_HEADING = "subHeading";
 /**
  * Identifier for heading for category overview component.
  */
 private static final String HEADING = "heading";
 
 /**
  * Identifier for menu link URL.
  */
 public static final String MENU_LINK = "url";
 
 /**
  * Identifier for menu label.
  */
 public static final String MENU_LABEL = "label";
 
 /**
  * Identifier for css class.
  */
 public static final String CLASS = "class";
 
 /**
  * Identifier for css class.
  */
 public static final String CSS_CLASS = "cssClass";
 
 /**
  * Identifier for all menu items.
  */
 private static final String LINK_LIST = "linkedList";
 
 /**
  * Identifier for categories list.
  */
 private static final String NAVIGATION_LIST = "navigationList";
 
 private static final String NAVIGATION_LINKS = "fixedList";
 
 private static final String NAVIGATION_CLASS = "class";
 private static final String OPEN_IN_NEW_WINDOW = "openInNewWindow";
 private static final String NAVIGATION_LINK_TITLE = "navigationLinkTitle";
 private static final String BUILD_NAVIGATION_USING = "buildNavigationUsing";
 private static final String FIXED_LIST = "fixedList";
 private static final String CHILD_PAGES = "childPages";
 private static final String LIMIT_DEPTH = "limitDepth";
 private static final String LIMIT_DEPTH_STRING = "limitDepthString";
 private static final String EXCLUDE_CONTENT_TYPE = "excludeContentType";
 private static final String NAVIGATION_LIST_TITLE = "navigationListTitle";
 private static final String PARENT_PAGE_URL = "parentPageUrl";
 private static final String LINK = "link";
 private static final String CTA_LABEL_TEXT = "sectionNavigationV2.ctaLabelText";
 /**
  * logger object.
  */
 private static final Logger LOGGER = LoggerFactory.getLogger(SectionNavigationV2ViewHelperImpl.class);
 
 /** The component view and view helper cache. */
 /*
  * reference for cache object
  */
 @Reference
 ComponentViewAndViewHelperCache componentViewAndViewHelperCache;
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /**
  * The method processData overrides method is responsible validating author input and returning a list of sections containing text, links This method
  * converts the multiwidget input (a String Array) into sections of maps so that it can be accessed in JSP in consistent way.This method also
  * validates the link url provided by author.
  * 
  * @param content
  *         {@link Map} of all user input received as input
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return Map holding the result data.
  **/
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  LOGGER.debug("Section Navigation View Helper #processData called for processing fixed list.");
  ResourceResolver resourceResolver = getResourceResolver(resources);
  SlingHttpServletRequest slingRequest = getSlingRequest(resources);
  Resource compResource = slingRequest.getResource();
  PageManager pageManager = getPageManager(resources);
  I18n i18n = getI18n(resources);
  List<Map<String, Object>> columnList = new ArrayList<Map<String, Object>>();
  Map<String, Object> sectionNavigation = new HashMap<String, Object>();
  Map<String, Object> propertiesActual = getProperties(content);
  Map<String, Object> properties = CollectionUtils.getJcrPropertyAsMap(propertiesActual);
  String jsonNameSpace = MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE);
  String heading = MapUtils.getString(properties, HEADING, StringUtils.EMPTY);
  String subHeading = MapUtils.getString(properties, SUB_HEADING, StringUtils.EMPTY);
  int limitDepth = Integer.parseInt(MapUtils.getString(properties, LIMIT_DEPTH, "0"));
  String buildNavigationUsing = MapUtils.getString(properties, BUILD_NAVIGATION_USING, StringUtils.EMPTY);
  Map<String, Object> sectionNavigationLinksMap = new HashMap<String, Object>();
  if (FIXED_LIST.equals(buildNavigationUsing)) {
   setComponentPropertiesAsList(properties, UnileverConstants.LABEL);
   setComponentPropertiesAsList(properties, NAVIGATION_CLASS);
   setComponentPropertiesAsList(properties, UnileverConstants.URL);
   setComponentPropertiesAsList(properties, OPEN_IN_NEW_WINDOW);
   LOGGER.debug("The properties map for section navigation component is " + properties);
   
   List<Map<String, String>> primaryList = ComponentUtil.getNestedMultiFieldProperties(compResource, NAVIGATION_LINKS);
   LOGGER.info("The number of section navigation list defined is " + primaryList.size());
   for (Map<String, String> map : primaryList) {
    Map<String, Object> columnMap = new LinkedHashMap<String, Object>();
    String navigationLinkTitle = StringUtils.defaultString(map.get(NAVIGATION_LINK_TITLE), StringUtils.EMPTY);
    navigationLinkTitle = StringUtils.isNotBlank(navigationLinkTitle) ? navigationLinkTitle : getCurrentPage(resources).getTitle();
    List<Map<String, Object>> sectinNavigationLinkList = convertToNavListMap(map, resourceResolver, pageManager, getCurrentPage(resources),
      getSlingRequest(resources));
    navigationLinkTitle = (sectinNavigationLinkList.size() > ZERO) ? navigationLinkTitle : StringUtils.EMPTY;
    columnMap.put(UnileverConstants.TITLE, navigationLinkTitle);
    columnMap.put(LINK_LIST, sectinNavigationLinkList);
    columnMap.put(TOTAL_RESULTS, sectinNavigationLinkList.size());
    LOGGER.debug("The column map corresponding to the section navigation is " + columnMap);
    columnList.add(columnMap);
   }
   sectionNavigation.put(LIMIT_DEPTH, ONE);
   sectionNavigation.put(LIMIT_DEPTH_STRING, String.valueOf(ONE));
   sectionNavigation.put(LEVELS, ZERO);
   sectionNavigation.put(LEVELS_STRING, String.valueOf(ZERO));
   
  } else if (CHILD_PAGES.equals(buildNavigationUsing)) {
   columnList = getNavigationItems(content, resources);
   sectionNavigation.put(LIMIT_DEPTH, limitDepth);
   sectionNavigation.put(LIMIT_DEPTH_STRING, String.valueOf(limitDepth));
   if (limitDepth == ZERO) {
    sectionNavigation.put(LEVELS, limitDepth);
    sectionNavigation.put(LEVELS_STRING, String.valueOf(limitDepth));
   } else {
    sectionNavigation.put(LEVELS, limitDepth - ONE);
    sectionNavigation.put(LEVELS_STRING, String.valueOf(limitDepth - ONE));
   }
  }
  commonProperties(sectionNavigation, heading, subHeading);
  sectionNavigation.put("ctaLabelText", i18n.get(CTA_LABEL_TEXT));
  sectionNavigation.put("placeholderText", i18n.get("sectionNavigationV2.placeholderText"));
  sectionNavigation.put(NAVIGATION_LIST, columnList.toArray());
  sectionNavigationLinksMap.put(jsonNameSpace, sectionNavigation);
  LOGGER.debug("The section navigation final data map returned is " + sectionNavigationLinksMap.size());
  return sectionNavigationLinksMap;
 }
 
 /**
  * change the instance of string array to list in component properties
  * 
  * @param properties
  * @param propertyKey
  */
 @SuppressWarnings("unchecked")
 private void setComponentPropertiesAsList(Map<String, Object> properties, String propertyKey) {
  if (properties.get(propertyKey) instanceof List) {
   properties.put(propertyKey, ((ArrayList<String>) properties.get(propertyKey)).toArray());
  }
 }
 
 /**
  * adds common proerties to map
  * 
  * @param sectionNavigation
  * @param heading
  * @param subHeading
  * @param ctaLabel
  */
 private void commonProperties(Map<String, Object> sectionNavigation, String heading, String subHeading) {
  sectionNavigation.put(HEADING, heading);
  sectionNavigation.put(SUB_HEADING, subHeading);
 }
 
 /**
  * Gets the navigation items.
  * 
  * @param properties
  *         the properties
  * @param currentResource
  *         the current resource
  * @param resourceResolver
  *         the resource resolver
  * @param currentPage
  *         the current page
  * @param browseAll
  *         the browse all
  * @return the navigation items
  */
 private List<Map<String, Object>> getNavigationItems(final Map<String, Object> content, final Map<String, Object> resources) {
  LOGGER.debug("Inside section navigation method of Section Navigation to generate navigation items");
  
  String[] excludedContentTypes = getExcludedContentTypes(getCurrentResource(resources));
  
  PageManager pageManager = getResourceResolver(resources).adaptTo(PageManager.class);
  List<Map<String, Object>> level1List = new ArrayList<Map<String, Object>>();
  Map<String, Object> map = new LinkedHashMap<String, Object>();
  int limitDepth = Integer.parseInt(MapUtils.getString(getProperties(content), LIMIT_DEPTH, "1"));
  List<String> propertyNames = new ArrayList<String>();
  propertyNames.add(NAVIGATION_LIST_TITLE);
  propertyNames.add(PARENT_PAGE_URL);
  List<Map<String, String>> childPagesList = ComponentUtil.getNestedMultiFieldProperties(getCurrentResource(resources), "sections");
  if (org.apache.commons.collections.CollectionUtils.isNotEmpty(childPagesList)) {
   for (Map<String, String> childMap : childPagesList) {
    map = new LinkedHashMap<String, Object>();
    String parentPage = MapUtils.getString(childMap, PARENT_PAGE_URL);
    Page page = pageManager.getPage(parentPage);
    String navigationListTitle = MapUtils.getString(childMap, NAVIGATION_LIST_TITLE, StringUtils.EMPTY);
    map = listChildren(page, limitDepth, ONE, content, resources, excludedContentTypes, getCurrentPage(resources));
    String navigationTitle = StringUtils.isNotBlank(navigationListTitle) ? navigationListTitle : page.getTitle();
    navigationTitle = (MapUtils.getIntValue(map, TOTAL_RESULTS, ZERO) > ZERO) ? navigationTitle : StringUtils.EMPTY;
    map.put(UnileverConstants.TITLE, navigationTitle);
    LOGGER.debug("Auto Navigation list corresponding to Section Navigation is successfully generated");
    if ((map != null) && (!map.isEmpty())) {
     level1List.add(map);
    }
   }
  } else {
   map = listChildren(getCurrentPage(resources), limitDepth, ONE, content, resources, excludedContentTypes, getCurrentPage(resources));
   String title = (MapUtils.getIntValue(map, TOTAL_RESULTS, ZERO) > ZERO) ? getCurrentPage(resources).getTitle() : StringUtils.EMPTY;
   map.put(UnileverConstants.TITLE, title);
   if ((map != null) && (!map.isEmpty())) {
    level1List.add(map);
   }
  }
  
  return level1List;
 }
 
 private String[] getExcludedContentTypes(Resource currentResource) {
  
  String[] excludedContentTypes = null;
  List<Map<String, String>> manualList = ComponentUtil.getNestedMultiFieldProperties(currentResource, CONTENT_TYPE_JSON);
  if (!manualList.isEmpty()) {
   int size = manualList.size();
   excludedContentTypes = new String[size];
   
   for (Map<String, String> map : manualList) {
    excludedContentTypes[--size] = map.get(EXCLUDE_CONTENT_TYPE);
   }
  }
  if (ArrayUtils.isEmpty(excludedContentTypes)) {
   excludedContentTypes = ArrayUtils.EMPTY_STRING_ARRAY;
  }
  return excludedContentTypes;
 }
 
 /**
  * List children.
  * 
  * @param page
  *         the page
  * @param maxDepth
  *         the max depth
  * @param currentDepth
  *         the current depth
  * @param resourceResolver
  *         the resource resolver
  * @param properties
  *         the properties
  * @param currentPage
  *         the current page
  * @param excludedContentTypes
  *         the excluded content types
  * @param current
  *         page the current page
  * @return the map
  */
 private Map<String, Object> listChildren(Page page, int maxDepth, int currentDepth, final Map<String, Object> content,
   final Map<String, Object> resources, String[] excludedContentTypes, Page currentPage) {
  Map<String, Object> map = new LinkedHashMap<String, Object>();
  int depth = currentDepth;
  int depthMax = maxDepth;
  Map<String, Object> map1 = new LinkedHashMap<String, Object>();
  if (page != null) {
   Iterator<Page> iterator = page.listChildren();
   List<Map<String, Object>> listMap = null;
   if (depthMax > ONE) {
    int counter = --depthMax;
    listMap = new ArrayList<Map<String, Object>>();
    while (iterator.hasNext()) {
     Map<String, Object> childMap = listChildren(iterator.next(), counter, depth, content, resources, excludedContentTypes, currentPage);
     if (childMap != null && childMap.size() > ZERO) {
      listMap.add(childMap);
     }
    }
    
    List<Map<String, Object>> updatedList = getList(page, getResourceResolver(resources), excludedContentTypes, currentPage,
      getSlingRequest(resources));
    for (int i = ZERO; i < updatedList.size(); i++) {
     updatedList.get(i).put(LEVEL, listMap.get(i));
    }
    map1.remove(LINK_LIST);
    map1.put(TOTAL_RESULTS, updatedList.size());
    map1.put(LINK_LIST, updatedList.toArray());
    map = map1;
    
   } else {
    List<Map<String, Object>> currentPageChildList = getList(page, getResourceResolver(resources), excludedContentTypes, currentPage,
      getSlingRequest(resources));
    map1.put(TOTAL_RESULTS, currentPageChildList.size());
    map1.put(LINK_LIST, currentPageChildList.toArray());
    map = map1;
   }
  }
  return map;
 }
 
 /**
  * Gets the map.
  * 
  * @param page
  *         the page
  * @param resourceResolver
  *         the resource resolver
  * @param excludedContentTypes
  *         the excluded content types
  * @param slingHttpServletRequest
  * @return the map
  */
 private List<Map<String, Object>> getList(Page page, ResourceResolver resourceResolver, String[] excludedContentTypes, Page currentPage,
   SlingHttpServletRequest slingHttpServletRequest) {
  Iterator<Page> childPages = page.listChildren();
  List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
  Map<String, Object> childMap = new LinkedHashMap<String, Object>();
  while (childPages.hasNext()) {
   childMap = new LinkedHashMap<String, Object>();
   Page childPage = childPages.next();
   String title = childPage.getTitle();
   String url = ComponentUtil.getFullURL(resourceResolver, childPage.getPath(), slingHttpServletRequest);
   String teaserImagePath = MapUtils.getString(childPage.getProperties(), UnileverConstants.TEASER_IMAGE, StringUtils.EMPTY);
   String teaserImageAltText = MapUtils.getString(childPage.getProperties(), UnileverConstants.TEASER_IMAGE_ALT_TEXT, StringUtils.EMPTY);
   String teaserTitle = MapUtils.getString(childPage.getProperties(), UnileverConstants.TEASER_TITLE, StringUtils.EMPTY);
   Image image = new Image(teaserImagePath, teaserImageAltText, teaserTitle, currentPage, slingHttpServletRequest);
   if (isPageValidForGlobalnavigation(childPage, excludedContentTypes)) {
    childMap.put(UnileverConstants.LABEL, title);
    childMap.put(CategoryOverviewHelper.CSS_CLASS, StringUtils.EMPTY);
    childMap.put(UnileverConstants.URL, url);
    childMap.put(UnileverConstants.IMAGE, image.convertToMap());
    childMap.put(CategoryOverviewHelper.OPEN_IN_NEW_WINDOW, "No");
    Map<String, Object> linkMap = new LinkedHashMap<String, Object>();
    linkMap.put(LINK, childMap);
    list.add(linkMap);
   }
  }
  return list;
 }
 
 /**
  * Checks if is page valid for section navigation.
  * 
  * @param page
  *         the page
  * @param excludedContentTypes
  *         the excluded content types
  * @return true, if is page valid for section navigation
  */
 private boolean isPageValidForGlobalnavigation(Page page, String[] excludedContentTypes) {
  boolean isValidPage = true;
  if (page != null && page.getContentResource() != null) {
   ValueMap pageProperties = page.getContentResource().adaptTo(ValueMap.class);
   
   String pageContentType = pageProperties.get(UnileverConstants.CONTENT_TYPE, String.class);
   if (StringUtils.isNotBlank(pageContentType) && ArrayUtils.indexOf(excludedContentTypes, pageContentType) != -ONE) {
    isValidPage = false;
   }
  } else {
   isValidPage = false;
  }
  return isValidPage;
 }
 
 /**
  * Convert to section nav list map.
  * 
  * @param resourceResolver
  *         the resource resolver
  * @param slingHttpServletRequest
  * @param jsonString
  *         the json string
  * @param linkmMap
  * @param pageManager
  * @param currentPage
  * 
  * @return the list
  */
 public static List<Map<String, Object>> convertToNavListMap(Map<String, String> linkmMap, ResourceResolver resourceResolver,
   PageManager pageManager, Page currentPage, SlingHttpServletRequest slingHttpServletRequest) {
  
  List<Map<String, Object>> menuMapList = new ArrayList<Map<String, Object>>();
  List<Map<String, String>> listMap = new ArrayList<Map<String, String>>();
  if (StringUtils.isNotBlank(linkmMap.get(LIST_LINKS))) {
   listMap = ComponentUtil.convertJsonArrayToListOfStringMap(linkmMap.get(LIST_LINKS));
  }
  
  for (Map<String, String> map : listMap) {
   Map<String, Object> menuMap = new HashMap<String, Object>();
   
   // this condition adds only those menu items for which label
   // and link both are configured
   if (StringUtils.isNotEmpty(map.get(MENU_LABEL)) || StringUtils.isNotEmpty(map.get(MENU_LINK))) {
    LOGGER.debug("Both menu label and menu link are configured");
    String newWindow = map.get(OPEN_IN_NEW_WINDOW);
    String openInNewWindow = newWindow != null && ("[true]".equals(newWindow) || "[yes]".equals(newWindow)) ? "Yes" : "No";
    String linkUrl = map.get(MENU_LINK);
    Page currentNavigationPage = pageManager.getContainingPage(linkUrl);
    Map<String, Object> currentNavigationPageProperties = (currentNavigationPage != null) ? currentNavigationPage.getProperties()
      : new LinkedHashMap<String, Object>();
    String currentNavigationPagetitle = getPageLandingAttribute(currentNavigationPageProperties, StringUtils.EMPTY, true, currentNavigationPage);
    String teaserImagePath = getPageLandingAttribute(currentNavigationPageProperties, UnileverConstants.TEASER_IMAGE, false, currentNavigationPage);
    String teaserImageAltText = getPageLandingAttribute(currentNavigationPageProperties, UnileverConstants.TEASER_IMAGE_ALT_TEXT, false,
      currentNavigationPage);
    String teaserTilte = getPageLandingAttribute(currentNavigationPageProperties, UnileverConstants.TEASER_TITLE, false, currentNavigationPage);
    Image image = new Image(teaserImagePath, teaserImageAltText, teaserTilte, currentPage, slingHttpServletRequest);
    String updatedLabel = StringUtils.isNotBlank(map.get(MENU_LABEL)) ? map.get(MENU_LABEL) : currentNavigationPagetitle;
    menuMap.put(MENU_LABEL, updatedLabel);
    menuMap.put(CSS_CLASS, map.get(CLASS));
    menuMap.put(MENU_LINK, ComponentUtil.getFullURL(resourceResolver, linkUrl, slingHttpServletRequest));
    menuMap.put(OPEN_IN_NEW_WINDOW, openInNewWindow);
    menuMap.put(UnileverConstants.IMAGE, image.convertToMap());
    LOGGER.debug("The menuMap prepared is" + menuMap);
    Map<String, Object> linkMap = new HashMap<String, Object>();
    linkMap.put(LINK, menuMap);
    menuMapList.add(linkMap);
   }
  }
  LOGGER.debug("The number of menu items of section navigation for this column is" + menuMapList.size());
  return menuMapList;
 }
 
 /**
  * returns property vakue of page
  * 
  * @param pageProperties
  * @param key
  * @param isTitle
  * @param page
  * @return
  */
 private static String getPageLandingAttribute(Map<String, Object> pageProperties, String key, boolean isTitle, Page page) {
  String returnKey = StringUtils.EMPTY;
  if (isTitle && page != null) {
   returnKey = page.getTitle();
  } else if (page != null) {
   returnKey = MapUtils.getString(pageProperties, key, StringUtils.EMPTY);
  }
  return returnKey;
 }
}
