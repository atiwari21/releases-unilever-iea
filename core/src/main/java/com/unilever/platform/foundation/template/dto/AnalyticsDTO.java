/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.template.dto;

import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;

/**
 * The Class AnalyticsDTO.
 */
public class AnalyticsDTO {
 
 /** The non prod domain. */
 private String nonProdDomain;
 
 /** The prod domain. */
 private String prodDomain;
 
 /** The non prod gid. */
 private String nonProdGid;
 
 /** The prod gid. */
 private String prodGid;
 
 /** The non prod track tool id. */
 private String nonProdTrackToolId;
 
 /** The prod track tool id. */
 private String prodTrackToolId;
 
 /** The access categories domains. */
 private String accessCategoriesDomains;
 
 /** The globalbrand. */
 private String globalbrand;
 
 /** The localbrand. */
 private String localbrand;
 
 /** The channel. */
 private String channel;
 
 /** The country. */
 private String country;
 
 /** The sitetype. */
 private String sitetype;
 
 /** The category. */
 private String category;
 
 /** The prod env. */
 private boolean isProdEnv;
 
 /**
  * Instantiates a new analytics dto.
  * 
  * @param paramMap
  *         the param map
  * @param domainList
  *         the domain list
  */
 public AnalyticsDTO(Map<String, String> paramMap, Map<String, String> domainList) {
  this.nonProdDomain = MapUtils.getString(domainList, "nonProdDomain", StringUtils.EMPTY);
  this.prodDomain = MapUtils.getString(domainList, "prodDomain", StringUtils.EMPTY);
  this.nonProdGid = MapUtils.getString(paramMap, "non-prod-gid", StringUtils.EMPTY);
  this.prodGid = MapUtils.getString(paramMap, "prod-gid", StringUtils.EMPTY);
  this.nonProdTrackToolId = MapUtils.getString(paramMap, "nonProdTrackToolId", StringUtils.EMPTY);
  this.prodTrackToolId = MapUtils.getString(paramMap, "prodTrackToolId", StringUtils.EMPTY);
  this.accessCategoriesDomains = MapUtils.getString(paramMap, "accessCategoriesDomains", StringUtils.EMPTY);
  this.globalbrand = MapUtils.getString(paramMap, "globalbrand", StringUtils.EMPTY);
  this.localbrand = MapUtils.getString(paramMap, "localbrand", StringUtils.EMPTY);
  this.channel = MapUtils.getString(paramMap, "channel", StringUtils.EMPTY);
  this.country = MapUtils.getString(paramMap, "country", StringUtils.EMPTY);
  this.sitetype = MapUtils.getString(paramMap, "sitetype", StringUtils.EMPTY);
  this.category = MapUtils.getString(paramMap, "category", StringUtils.EMPTY);
  
 }
 
 /**
  * Gets the non prod domain.
  * 
  * @return the non prod domain
  */
 public String getNonProdDomain() {
  return nonProdDomain;
 }
 
 /**
  * Sets the non prod domain.
  * 
  * @param nonProdDomain
  *         the new non prod domain
  */
 public void setNonProdDomain(String nonProdDomain) {
  this.nonProdDomain = nonProdDomain;
 }
 
 /**
  * Gets the prod domain.
  * 
  * @return the prod domain
  */
 public String getProdDomain() {
  return prodDomain;
 }
 
 /**
  * Sets the prod domain.
  * 
  * @param prodDomain
  *         the new prod domain
  */
 public void setProdDomain(String prodDomain) {
  this.prodDomain = prodDomain;
 }
 
 /**
  * Gets the non prod gid.
  * 
  * @return the non prod gid
  */
 public String getNonProdGid() {
  return nonProdGid;
 }
 
 /**
  * Sets the non prod gid.
  * 
  * @param nonProdGid
  *         the new non prod gid
  */
 public void setNonProdGid(String nonProdGid) {
  this.nonProdGid = nonProdGid;
 }
 
 /**
  * Gets the prod gid.
  * 
  * @return the prod gid
  */
 public String getProdGid() {
  return prodGid;
 }
 
 /**
  * Sets the prod gid.
  * 
  * @param prodGid
  *         the new prod gid
  */
 public void setProdGid(String prodGid) {
  this.prodGid = prodGid;
 }
 
 /**
  * Gets the non prod track tool id.
  * 
  * @return the non prod track tool id
  */
 public String getNonProdTrackToolId() {
  return nonProdTrackToolId;
 }
 
 /**
  * Sets the non prod track tool id.
  * 
  * @param nonProdTrackToolId
  *         the new non prod track tool id
  */
 public void setNonProdTrackToolId(String nonProdTrackToolId) {
  this.nonProdTrackToolId = nonProdTrackToolId;
 }
 
 /**
  * Gets the prod track tool id.
  * 
  * @return the prod track tool id
  */
 public String getProdTrackToolId() {
  return prodTrackToolId;
 }
 
 /**
  * Sets the prod track tool id.
  * 
  * @param prodTrackToolId
  *         the new prod track tool id
  */
 public void setProdTrackToolId(String prodTrackToolId) {
  this.prodTrackToolId = prodTrackToolId;
 }
 
 /**
  * Gets the globalbrand.
  * 
  * @return the globalbrand
  */
 public String getGlobalbrand() {
  return globalbrand;
 }
 
 /**
  * Sets the globalbrand.
  * 
  * @param globalbrand
  *         the new globalbrand
  */
 public void setGlobalbrand(String globalbrand) {
  this.globalbrand = globalbrand;
 }
 
 /**
  * Gets the localbrand.
  * 
  * @return the localbrand
  */
 public String getLocalbrand() {
  return localbrand;
 }
 
 /**
  * Sets the localbrand.
  * 
  * @param localbrand
  *         the new localbrand
  */
 public void setLocalbrand(String localbrand) {
  this.localbrand = localbrand;
 }
 
 /**
  * Gets the channel.
  * 
  * @return the channel
  */
 public String getChannel() {
  return channel;
 }
 
 /**
  * Sets the channel.
  * 
  * @param channel
  *         the new channel
  */
 public void setChannel(String channel) {
  this.channel = channel;
 }
 
 /**
  * Gets the country.
  * 
  * @return the country
  */
 public String getCountry() {
  return country;
 }
 
 /**
  * Sets the country.
  * 
  * @param country
  *         the new country
  */
 public void setCountry(String country) {
  this.country = country;
 }
 
 /**
  * Gets the sitetype.
  * 
  * @return the sitetype
  */
 public String getSitetype() {
  return sitetype;
 }
 
 /**
  * Sets the sitetype.
  * 
  * @param sitetype
  *         the new sitetype
  */
 public void setSitetype(String sitetype) {
  this.sitetype = sitetype;
 }
 
 /**
  * Gets the category.
  * 
  * @return the category
  */
 public String getCategory() {
  return category;
 }
 
 /**
  * Sets the category.
  * 
  * @param category
  *         the new category
  */
 public void setCategory(String category) {
  this.category = category;
 }
 
 /**
  * Gets the checks if is prod env.
  * 
  * @return the checks if is prod env
  */
 public boolean getIsProdEnv() {
  return isProdEnv;
 }
 
 /**
  * Sets the checks if is prod env.
  * 
  * @param isProdEnv
  *         the new checks if is prod env
  */
 public void setIsProdEnv(boolean isProdEnv) {
  this.isProdEnv = isProdEnv;
 }
 
 /**
  * Gets the access categories domains.
  * 
  * @return the access categories domains
  */
 public String getAccessCategoriesDomains() {
  return accessCategoriesDomains;
 }
 
 /**
  * Sets the access categories domains.
  * 
  * @param accessCategoriesDomains
  *         the new access categories domains
  */
 public void setAccessCategoriesDomains(String accessCategoriesDomains) {
  this.accessCategoriesDomains = accessCategoriesDomains;
 }
 
}
