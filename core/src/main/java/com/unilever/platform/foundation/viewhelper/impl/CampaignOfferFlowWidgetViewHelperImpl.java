/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.sling.api.SlingConstants;

import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * This class prepared the data map for Campaign Offer Flow Widget component.
 * 
 */
@Component(description = "CampaignOfferFlowWidget", immediate = true, metatype = true, label = "CampaignOfferFlowWidgetViewHelperImpl")
@Properties({ @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.CAMPAIGN_OFFER_FLOW_WIDGET, propertyPrivate = true), })
public class CampaignOfferFlowWidgetViewHelperImpl extends BaseViewHelper {
 
 /** The Constant HTML_CODE_SNIPPET. */
 private static final Object HTML_CODE_SNIPPET = "htmlCodeSnippet";
 
 /** The Constant DISABLE_WIDGET. */
 private static final Object DISABLE_WIDGET = "disableWidget";
 
 /**
  * This is overriden method from base view helper which prepares data map by calling all required methods.
  * 
  * @param content
  *         the content
  * @param resources
  *         the resources
  * @return the map
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  Map<String, Object> campaignOfferFlowWidgetMap = new LinkedHashMap<String, Object>();
  Map<String, Object> properties = getProperties(content);
  
  campaignOfferFlowWidgetMap.put("htmlCodeSnippet", MapUtils.getString(properties, HTML_CODE_SNIPPET, StringUtils.EMPTY));
  campaignOfferFlowWidgetMap.put("disableWidget", MapUtils.getBoolean(properties, DISABLE_WIDGET, false));
  
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  data.put(getJsonNameSpace(content), campaignOfferFlowWidgetMap);
  
  return data;
 }
 
}
