/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;

/**
 * The Class ZipFileDownloadViewHelperImpl.
 */
@Component(description = "ZipFileDownloadViewHelperImpl", immediate = true, metatype = true, label = "Zip File Download Component")
@Service(value = { ZipFileDownloadViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.ZIP_FILE_DOWNLOAD, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class ZipFileDownloadViewHelperImpl extends BaseViewHelper {
 
 /** The Constant ENDS_WITH_ZIP. */
 private static final String ENDS_WITH_ZIP = "zip";
 
 /** The Constant DOWNLOAD. */
 private static final String DOWNLOAD = "dwnld";
 
 /** The Constant DOT. */
 private static final String DOT = ".";
 
 /** The Constant URL. */
 private static final String URL = "url";
 
 /** The Constant LABEL. */
 private static final String LABEL = "label";
 
 /** The Constant ATTACHMENTS. */
 private static final String ATTACHMENTS = "attachments";
 
 /** The Constant CTA. */
 private static final String CTA = "cta";
 
 /** The Constant ZIP_FILE_NAME. */
 private static final String ZIP_FILE_NAME = "zipFileName";
 
 /** The Constant ZIP. */
 private static final String ZIP = ".zip";
 
 /** The Constant FILES_FOR_DOWNLOAD. */
 private static final String FILES_FOR_DOWNLOAD = "filesForDownload";
 
 /** The Constant DOWNLOAD_FILE_NAME. */
 private static final String DOWNLOAD_FILE_NAME = "downloadFileName";
 
 /** The Constant PREFIX_TEXT. */
 private static final String PREFIX_TEXT = "prefixText";
 
 /** The Constant ZIP_FILE_DOWNLOAD_CTA_LABEL. */
 private static final String ZIP_FILE_DOWNLOAD_CTA_LABEL = "zipFileDownloadCtaLabel";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(FeaturedInstagramViewHelperImpl.class);
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.base.BaseViewHelper#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.info("Zip File Download View Helper #processData called for processing fixed list.");
  
  Map<String, Object> properties = getProperties(content);
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  
  String jsonNameSpace = getJsonNameSpace(content);
  
  ResourceResolver resolver = getResourceResolver(resources);
  Page currentPage = getCurrentPage(resources);
  
  String prefixText = MapUtils.getString(properties, PREFIX_TEXT, StringUtils.EMPTY);
  String zipFileDownloadCtaLabel = MapUtils.getString(properties, ZIP_FILE_DOWNLOAD_CTA_LABEL, StringUtils.EMPTY);
  String downloadFileName = MapUtils.getString(properties, DOWNLOAD_FILE_NAME, StringUtils.EMPTY);
  
  if (!downloadFileName.endsWith(ZIP)) {
   downloadFileName = downloadFileName + ZIP;
  }
  List<Map<String, String>> filesForDownload = ComponentUtil.getNestedMultiFieldProperties(getCurrentResource(resources), FILES_FOR_DOWNLOAD);
  
  Map<String, Object> zipFileDownloadMap = new HashMap<String, Object>();
  Map<String, Object> ctaMap = new HashMap<String, Object>();
  
  ctaMap.put(LABEL, zipFileDownloadCtaLabel);
  ctaMap.put(URL, resolver.map(currentPage.getPath()) + DOT + DOWNLOAD + DOT + ENDS_WITH_ZIP);
  
  zipFileDownloadMap.put(CTA, ctaMap);
  zipFileDownloadMap.put(PREFIX_TEXT, prefixText);
  zipFileDownloadMap.put(ZIP_FILE_NAME, downloadFileName);
  zipFileDownloadMap.put(ATTACHMENTS, filesForDownload.toArray());
  
  data.put(jsonNameSpace, zipFileDownloadMap);
  return data;
 }
}
