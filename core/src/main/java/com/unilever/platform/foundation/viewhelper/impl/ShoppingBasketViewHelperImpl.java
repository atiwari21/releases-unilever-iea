/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.cache.ComponentViewAndViewHelperCache;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.viewhelper.base.BaseViewHelper;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;
import com.unilever.platform.foundation.viewhelper.constants.ShoppingBasketConstant;

/**
 * <p>
 * Responsible for reading author input, I18N and configuration for shopping basket component and generate a map. Also read the site configuration for
 * maximum items configured for the site.
 * </p>
 */
@Component(description = "ShoppingBasketViewHelperImpl", immediate = true, metatype = true, label = "ShoppingBasketViewHelperImpl")
@Service(value = { ShoppingBasketViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.SHOPPING_BASKET, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class ShoppingBasketViewHelperImpl extends BaseViewHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ShoppingBasketViewHelperImpl.class);
 
 /** The component view and view helper cache. */
 /*
  * reference for cache object
  */
 @Reference
 ComponentViewAndViewHelperCache componentViewAndViewHelperCache;
 /*
  * reference for site configuration service
  */
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /**
  * The method processData overrides method is responsible validating author input and returning a map and maximum number for the items in shopping
  * basket for a site from site configuration .
  * 
  * @param content
  *         {@link Map} of all user input received as input
  * @param resources
  *         holding the objects like sling, resource resolver etc.
  * @return Map holding the result data.
  */
 @Override
 public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
  LOGGER.debug("inside ShoppingBasketViewHelperImpl processData");
  Map<String, Object> data = new HashMap<String, Object>();
  try {
   Map<String, Object> dataMap = new HashMap<String, Object>();
   I18n i18n = getI18n(getSlingRequest(resources), getCurrentPage(resources));
   dataMap.put("static", getStaticValuesMap(i18n));
   dataMap.put("configs", getConfigs(getCurrentPage(resources), getResourceResolver(resources), getSlingRequest(resources)));
   String jsonNameSpace = MapUtils.getString(content, UnileverComponents.JSON_NAME_SPACE);
   data.put(jsonNameSpace, dataMap);
  } catch (Exception e) {
   LOGGER.error("Error At ShoppingBasketViewHelperImpl " + ExceptionUtils.getStackTrace(e));
  }
  return data;
 }
 
 private Map<String, String> getConfigs(Page currentPage, ResourceResolver resourceResolver, SlingHttpServletRequest request) {
  String maximumItemPerProduct = StringUtils.EMPTY;
  String maximumItemAllProduct = StringUtils.EMPTY;
  String giftingPageFullURL = StringUtils.EMPTY;
  Map<String, String> configMap = new HashMap<String, String>();
  try {
   maximumItemPerProduct = configurationService.getConfigValue(currentPage, UnileverConstants.EGIFTING_CONFIG_CAT, "maximumItemPerProduct");
   maximumItemAllProduct = configurationService.getConfigValue(currentPage, UnileverConstants.EGIFTING_CONFIG_CAT, "maximumItemAllProduct");
   String giftingPagePath = configurationService.getConfigValue(currentPage, UnileverConstants.EGIFTING_CONFIG_CAT, ProductConstants.TX1_URL);
   giftingPageFullURL = ComponentUtil.getFullURL(resourceResolver, giftingPagePath, request);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Error in getting the configuration for  egifting ", e);
  }
  configMap.put("maximumItemPerProduct", maximumItemPerProduct);
  configMap.put("maximumItemAllProduct", maximumItemAllProduct);
  configMap.put("giftingPageURL", giftingPageFullURL);
  return configMap;
 }
 
 /**
  * Gets the static values map.
  * 
  * @param i18n
  *         the i18n
  * @return the static values map
  */
 private Map<String, String> getStaticValuesMap(I18n i18n) {
  Map<String, String> staticMap = new HashMap<String, String>();
  staticMap.put(ShoppingBasketConstant.BASKET_TITLE, i18n.get(ShoppingBasketConstant.BASKET_TITLE_KEY));
  staticMap.put(ShoppingBasketConstant.EMTPY_BASKET_COPY, i18n.get(ShoppingBasketConstant.EMTPY_BASKET_COPY_KEY));
  staticMap.put(ShoppingBasketConstant.RECIPENT_NAME, i18n.get(ShoppingBasketConstant.RECIPENT_NAME_KEY));
  staticMap.put(ShoppingBasketConstant.GREETING, i18n.get(ShoppingBasketConstant.GREETING_KEY));
  staticMap.put(ShoppingBasketConstant.QANTITY, i18n.get(ShoppingBasketConstant.QANTITY_KEY));
  staticMap.put(ShoppingBasketConstant.GIFT_WARP_SET_LABEL, i18n.get(ShoppingBasketConstant.GIFT_WARP_SET_LABEL_KEY));
  staticMap.put(ShoppingBasketConstant.EDIT, i18n.get(ShoppingBasketConstant.EDIT_KEY));
  staticMap.put(ShoppingBasketConstant.DELETE, i18n.get(ShoppingBasketConstant.DELETE_KEY));
  staticMap.put(ShoppingBasketConstant.SUB_TOTAL, i18n.get(ShoppingBasketConstant.SUB_TOTAL_KEY));
  staticMap.put(ShoppingBasketConstant.NAME_AND_MESSAGE_LABEL, i18n.get(ShoppingBasketConstant.NAME_AND_MESSAGE_KEY));
  staticMap.put(ShoppingBasketConstant.ERROR_MESSAGE_LABEL, i18n.get(ShoppingBasketConstant.ERROR_MESSAGE_KEY));
  staticMap.put(ProductConstants.EGIFTING_FREE_LABEL, i18n.get(ProductConstants.EGIFTING_FREE_KEY));
  
  return staticMap;
 }
}
