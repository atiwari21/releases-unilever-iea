/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.viewhelper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.foundation.components.UnileverComponents;
import com.unilever.platform.foundation.components.helper.ComponentUtil;
import com.unilever.platform.foundation.components.helper.SocialTABHelper;
import com.unilever.platform.foundation.constants.CommonConstants;
import com.unilever.platform.foundation.constants.UnileverConstants;
import com.unilever.platform.foundation.template.dto.SocialTabQueryDTO;
import com.unilever.platform.foundation.viewhelper.constants.ProductConstants;

/**
 * The Class SocialCarouselViewHelperImpl.
 */
@Component(description = "SocialCarouselViewHelperImpl", immediate = true, metatype = true, label = "Social Carousel")
@Service(value = { SocialCarouselViewHelperImpl.class, ViewHelper.class })
@Properties({
  @Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = UnileverComponents.SOCIAL_CAROUSEL, propertyPrivate = true),
  @Property(name = Constants.SERVICE_RANKING, intValue = com.sapient.platform.iea.aem.core.constants.CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })
public class SocialCarouselViewHelperImpl extends SocialTabBaseViewHelperImpl {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(SocialCarouselViewHelperImpl.class);
 
 /**
  * Constant for title.
  */
 private static final String TITLE = "title";
 
 /**
  * Constant for Introduction Copy.
  */
 private static final String INTRO_COPY = "introCopy";
 
 /**
  * Constant for cta.
  */
 private static final String CTA = "cta";
 
 /**
  * Constant for CTA_NAV_PATH.
  */
 private static final String CTA_LINK_URL = "ctaLinkUrl";
 
 /**
  * Constant for CTA_LABEL_TEXT.
  */
 private static final String CTA_VIEW_TEXT = "ctaViewText";
 /**
  * Constant for CTA_LABEL_TEXT.
  */
 private static final String CTA_LABEL = "ctaLabel";
 
 /**
  * Constant for IS_EXPANEDE_VIEW.
  */
 private static final String IS_EXPANDED_VIEW = "isExpandedView";
 /**
  * Constant for IS_EXPANEDE_VIEW.
  */
 private static final String IS_EXPANDED_VIEW_REQUIRED = "isExpandedViewRequired";
 
 /**
  * Constant for IS_EXPANEDE_VIEW.
  */
 private static final String OPEN_NEW_WINDOW = "openInNewWindow";
 /**
  * Constant for IS_EXPANEDE_VIEW.
  */
 private static final String OPEN_IN_NEW_WINDOW = "isOpenInNewWindow";
 /** The Constant SOCIAL_CHANNEL_IMG. */
 private static final String SOCIAL_CHANNEL_IMG = "socialChannellsImg";
 
 /** The Constant SOCIAL_CHANNEL_IMG. */
 private static final String SOCIAL_CAROUSEL_CHANNEL = "socialChannel";
 /** The Constant SOCIAL_IMAGE_POST_ID. */
 private static final String SOCIAL_IMAGE_POST_ID = "postId";
 /**
  * Constant for OPEN_IN_NEW_WINDOW_EXPANDED.
  */
 private static final String OPEN_IN_NEW_WINDOW_EXPANDED = "openInNewWindowExpanded";
 /** The Constant SOCIAL_GALLERY_EXPANDED_IMAGES. */
 private static final String SOCIAL_GALLERY_EXPANDED_IMAGES_JSON = "socialCarouselPostsJson";
 /** The Constant SHOW_PROFILE_PIC. */
 private static final String SHOW_PROFILE_PIC = "showProfilePic";
 
 /**
  * Constant for CTA_NAV_PATH.
  */
 private static final String CTA_RELATED_PATH = "ctaRelatedPath";
 @Reference
 ConfigurationService configurationService;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.viewhelper.impl.SocialTabBaseViewHelperImpl#processData(java.util.Map, java.util.Map)
  */
 @Override
 public Map<String, Object> processData(Map<String, Object> content, Map<String, Object> resources) {
  LOGGER.info("Social Carousel View Helper #processData called");
  
  Map<String, Object> properties = getProperties(content);
  ResourceResolver resourceResolver = getResourceResolver(resources);
  Page currentPage = getCurrentPage(resources);
  
  Map<String, Object> socialCarouselMap = new LinkedHashMap<String, Object>();
  
  int numberOfPosts = GlobalConfigurationUtility.getIntegerValueFromConfiguration(UnileverConstants.SOCIAL_CAROUSEL_CAT, configurationService,
    currentPage, NUMBER_OF_POSTS);
  int maxImages = GlobalConfigurationUtility.getIntegerValueFromConfiguration(UnileverConstants.SOCIAL_CAROUSEL_CAT, configurationService,
    currentPage, MAXIMUM_IMAGES);
  String profilePicture = MapUtils.getString(properties, SHOW_PROFILE_PIC, StringUtils.EMPTY);
  String showProfilePicture = profilePicture != null && conditionCheck(profilePicture) ? CommonConstants.BOOLEAN_TRUE : ProductConstants.FALSE;
  
  SocialTabQueryDTO tabQuery = getSocialAggregatorQueryDataObj(resources, properties, currentPage, configurationService, numberOfPosts, maxImages);
  
  socialCarouselMap.put(TITLE, getTitle(properties));
  socialCarouselMap.put(INTRO_COPY, getText(properties));
  
  addCTAMap(socialCarouselMap, getCtaText(properties), getCtaUrl(properties, resources), CTA, resourceResolver,
    MapUtils.getBoolean(properties, OPEN_NEW_WINDOW, Boolean.FALSE));
  
  // adding social images map
  addSocialImagesMap(socialCarouselMap, resources);
  
  // adding retrieval query parameters
  addRetrievalQueryParametrsCarouselMap(socialCarouselMap, tabQuery, properties, resources);
  
  socialCarouselMap.put("closeLabel", getI18n(resources).get("socialGalleryTAB.closeLabel"));
  socialCarouselMap.put("showProfilePicture", Boolean.valueOf(showProfilePicture));
  
  Map<String, Object> data = new LinkedHashMap<String, Object>();
  data.put(getJsonNameSpace(content), socialCarouselMap);
  
  return data;
 }
 
 /**
  * Condition check.
  * 
  * @param numberOfView
  *         the number of view
  * @return true, if successful
  */
 private static boolean conditionCheck(String numberOfView) {
  return "true".equals(numberOfView);
 }
 
 /**
  * @param socialGalleryMap
  * @param resourceResolver
  * @param page
  * @param currentResource
  * @param i18n
  */
 private void addSocialImagesMap(Map<String, Object> socialGalleryMap, Map<String, Object> resources) {
  ResourceResolver resourceResolver = getResourceResolver(resources);
  Page page = getCurrentPage(resources);
  List<Map<String, Object>> socialPostsList = new ArrayList<Map<String, Object>>();
  List<Map<String, String>> list = getSocialCarouselMap(getCurrentResource(resources));
  for (Map<String, String> map : list) {
   String imagePath = getImagePath(map);
   String pagePath = getPagePath(map);
   String socialChannel = StringUtils.defaultString(map.get(SOCIAL_CHANNEL_IMG), StringUtils.EMPTY);
   String postId = StringUtils.defaultString(map.get(SOCIAL_IMAGE_POST_ID), StringUtils.EMPTY);
   String relatedCtaPagePath = ComponentUtil.getFullURL(resourceResolver, pagePath, getSlingRequest(resources));
   String relatedArticleCtaType = getRelatedCtaLabel(map);
   Page associatedPage = page.getPageManager().getPage(pagePath);
   Map<String, Object> socialPostsMap = SocialTABHelper.getImageMapWithMetadata(imagePath, page, resourceResolver, getSlingRequest(resources));
   socialPostsMap.put(SOCIAL_CAROUSEL_CHANNEL, socialChannel);
   socialPostsMap.put(SOCIAL_IMAGE_POST_ID, postId);
   addCTAMap(socialPostsMap, relatedArticleCtaType, resourceResolver.map(relatedCtaPagePath), "realtedCta", associatedPage, getI18n(resources),
     getNewWindowFromMultifieldImg(map), resources);
   socialPostsList.add(socialPostsMap);
  }
  socialGalleryMap.put("editorSocialImages", socialPostsList.toArray());
 }
 
 /**
  * Adds the cta map.
  * 
  * @param socialGalleryCarouselMap
  *         the social gallery carousel map
  * @param ctaType
  *         the label text
  * @param pagePath
  *         the link text
  */
 private void addCTAMap(Map<String, Object> socialCarouselMap, String ctaType, String pagePath, String key, Page associatedPage, I18n i18n,
   Boolean openInNewWindow, Map<String, Object> resources) {
  Map<String, Object> ctaMap = new HashMap<String, Object>();
  String ctaLabelText = StringUtils.EMPTY;
  
  if ("article".equals(ctaType)) {
   ctaLabelText = i18n.get("socialTabBase.viewArticle");
  } else if ("product".equals(ctaType)) {
   ctaLabelText = i18n.get("socialTabBase.viewProduct");
   ctaMap.put("associatedProduct", getProductMap(associatedPage, resources));
  }
  
  ctaMap.put(CTA_VIEW_TEXT, ctaLabelText);
  ctaMap.put(CTA_RELATED_PATH, pagePath);
  ctaMap.put(OPEN_IN_NEW_WINDOW, openInNewWindow);
  
  socialCarouselMap.put(key, ctaMap);
 }
 
 private static void addCTAMap(Map<String, Object> socialCarouselMap, String ctaText, String ctaPath, String key, ResourceResolver resourceResolver,
   Boolean openInNewWindow) {
  Map<String, Object> ctaMap = new HashMap<String, Object>();
  
  ctaMap.put(CTA_LABEL, ctaText);
  ctaMap.put(CTA_LINK_URL, resourceResolver.map(ctaPath));
  ctaMap.put(OPEN_IN_NEW_WINDOW, openInNewWindow);
  socialCarouselMap.put(key, ctaMap);
 }
 
 /**
  * Adds the retrieval query parameters map.
  * 
  * @param map
  *         the map
  * @param socialTabQuery
  *         the social tab query
  * @param properties
  * @param resources
  */
 @SuppressWarnings("unchecked")
 public static void addRetrievalQueryParametrsCarouselMap(Map<String, Object> map, SocialTabQueryDTO socialTabQuery, Map<String, Object> properties,
   Map<String, Object> resources) {
  
  SocialTABHelper.addRetrievalQueryParametrsMap(map, socialTabQuery, true);
  Map<String, Object> requestServletUrlMap = (Map<String, Object>) map.get("retrivalQueryParams");
  
  requestServletUrlMap.put(IS_EXPANDED_VIEW_REQUIRED, MapUtils.getBoolean(properties, IS_EXPANDED_VIEW, Boolean.FALSE));
  
  addParametersToSocialChannels(requestServletUrlMap, resources);
  
  map.put("retrivalQueryParams", requestServletUrlMap);
 }
 
 /**
  * Gets the new window for expanded view from multifield.
  * 
  * @param map
  *         the map
  * @return the new window for expanded view from multifield
  */
 private static boolean getNewWindowFromMultifieldImg(Map<String, String> map) {
  
  String newWindow = StringUtils.defaultString(map.get(OPEN_IN_NEW_WINDOW_EXPANDED), StringUtils.EMPTY);
  String openInNewWindow = newWindow != null && ("[true]".equals(newWindow) || "[yes]".equals(newWindow)) ? BOOLEAN_TRUE : BOOLEAN_FALSE;
  return Boolean.valueOf(openInNewWindow);
 }
 
 /**
  * Gets the social carousel map.
  * 
  * @param currentResource
  *         the current resource
  * @return the social carousel map
  */
 public List<Map<String, String>> getSocialCarouselMap(Resource currentResource) {
  return ComponentUtil.getNestedMultiFieldProperties(currentResource, SOCIAL_GALLERY_EXPANDED_IMAGES_JSON);
 }
}
