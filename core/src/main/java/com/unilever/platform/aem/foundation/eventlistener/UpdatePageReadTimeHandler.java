/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.eventlistener;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Dictionary;
import java.util.Hashtable;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.version.VersionException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.html.HTMLEditorKit;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.apache.sling.engine.SlingRequestProcessor;
import org.apache.sling.event.jobs.JobProcessor;
import org.apache.sling.event.jobs.JobUtil;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.contentsync.handler.util.RequestResponseFactory;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.WCMMode;
import com.unilever.platform.aem.foundation.configuration.ConfigurationHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.aem.foundation.constants.CommonConstants;
import com.unilever.platform.foundation.constants.UnileverConstants;

/**
 * The Class UpdatePageReadTimeHandler.
 */
@SuppressWarnings("deprecation")
@Component(configurationFactory = true, label = "UpdatePageReadTimeHandler", description = "UpdatePageReadTimeHandler", metatype = false, immediate = true, enabled = true)
@Service(value = EventHandler.class)
public class UpdatePageReadTimeHandler implements EventHandler, JobProcessor {
 
 private static final String PAGE_READ_TIME = "pageReadTime";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(UpdatePageReadTimeHandler.class);
 
 /** The resourceTypes. */
 private String[] resourceTypes;
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 /** The factory. */
 @Reference
 org.apache.http.osgi.services.HttpClientBuilderFactory factory;
 
 /** The resolver factory. */
 // Inject a Sling ResourceResolverFactory
 @Reference
 private ResourceResolverFactory resolverFactory;
 @Reference
 private SlingRequestProcessor requestProcessor;
 @Reference
 private RequestResponseFactory requestResponseFactory;
 private ServiceRegistration handlerRegistration;
 
 /**
  * 
  * @param ctx
  *         the ctx
  */
 @Activate
 public void activate(ComponentContext ctx) {
  
  resourceTypes = PropertiesUtil.toStringArray(ctx.getProperties().get("resourceTypes"), null);
  StringBuilder eventFilterSB = new StringBuilder("(|");
  if (resourceTypes != null) {
   for (String path : resourceTypes) {
    eventFilterSB.append("(").append("resourceType").append("=").append(path).append(")");
    
   }
   eventFilterSB.append(")");
   
   Dictionary<String, Object> properties = new Hashtable<>();
   
   properties.put(EventConstants.EVENT_TOPIC, "org/apache/sling/api/resource/Resource/CHANGED");
   properties.put(EventConstants.EVENT_FILTER, eventFilterSB.toString());
   
   this.handlerRegistration = FrameworkUtil.getBundle(UpdatePageReadTimeHandler.class).getBundleContext()
     .registerService(EventHandler.class.getName(), this, properties);
   
  }
 }
 
 /**
  * 
  * @param ctx
  *         the ctx
  */
 @Deactivate
 public void deactivate(ComponentContext ctx) {
  if (this.handlerRegistration != null) {
   this.handlerRegistration.unregister();
   this.handlerRegistration = null;
  }
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.event.jobs.JobProcessor#process(org.osgi.service.event.Event)
  */
 @Override
 public boolean process(Event event) {
  ResourceResolver resolver = null;
  try {
   resolver = ConfigurationHelper.getResourceResolver(resolverFactory, "readService");
   String path = (String) event.getProperty(SlingConstants.PROPERTY_PATH);
   PageManager pageManager = resolver.adaptTo(PageManager.class);
   Page page = pageManager.getContainingPage(path);
   try {
    if (page != null) {
     String pageContentType = MapUtils.getString(page.getProperties(), UnileverConstants.CONTENT_TYPE, StringUtils.EMPTY);
     String contentTypes = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, page, PAGE_READ_TIME, "contentTypes");
     boolean overwriteReadTime = MapUtils.getBooleanValue(page.getProperties(), "overwriteReadTime", false);
     if (ArrayUtils.indexOf(contentTypes.split(CommonConstants.COMMA), pageContentType) > -1 && !overwriteReadTime) {
      Resource resource = resolver.getResource(path);
      Node node = resource.adaptTo(Node.class);
      int averageReadTime = GlobalConfigurationUtility.getIntegerValueFromConfiguration(PAGE_READ_TIME, configurationService, page, "averageSpeed");
      float averageReadTimeFloat = averageReadTime;
      int wordCountInt = getwordCount(page.getPath(), resolver);
      float wordCount = wordCountInt;
      try {
       float readTime = (averageReadTimeFloat > CommonConstants.ZERO) ? wordCount / averageReadTimeFloat : CommonConstants.ZERO;
       int pageReadTime = (wordCountInt == CommonConstants.ZERO) ? CommonConstants.ZERO : (int) Math.ceil(readTime);
       node.setProperty(PAGE_READ_TIME, "" + pageReadTime);
      } catch (ValueFormatException e) {
       LOGGER.error("Value FormatException:", e.getMessage(),e);
      } catch (VersionException e) {
       LOGGER.error("VersionException:", e.getMessage(),e);
      } catch (LockException e) {
       LOGGER.error("LockException:", e.getMessage(),e);
      } catch (ConstraintViolationException e) {
       LOGGER.error("ConstraintViolationException:", e.getMessage(),e);
      } catch (RepositoryException e) {
       LOGGER.error("RepositoryException:", e.getMessage(),e);
      }
     }
    }
   } catch (BadLocationException e) {
    LOGGER.error("BadLocationException:", e.getMessage(),e);
   }
   try {
    resolver.commit();
   } catch (PersistenceException e) {
    LOGGER.error("PersistenceException:", e.getMessage(),e);
   }
   
  } catch (LoginException le) {
   LOGGER.error("UpdatePageReadTimeHandler LoginException : ", le.getMessage(),le);
  } finally {
   if (resolver != null) {
    resolver.close();
   }
  }
  
  return true;
 }
 
 /**
  * Gets the word count.
  * 
  * @param url
  *         the url
  * @param resolver
  *         the resolver
  * @return the word count
  * @throws BadLocationException
  *          the bad location exception
  */
 private int getwordCount(String url, ResourceResolver resolver) throws BadLocationException {
  EditorKit kit = new HTMLEditorKit();
  Document doc = kit.createDefaultDocument();
  
  // The Document class does not yet handle charset's properly.
  doc.putProperty("IgnoreCharsetDirective", Boolean.TRUE);
  String text = StringUtils.EMPTY;
  HttpServletRequest httpServletRequest = requestResponseFactory.createRequest("GET", url + ".content.html");
  httpServletRequest.setAttribute(WCMMode.REQUEST_ATTRIBUTE_NAME, WCMMode.DISABLED);
  httpServletRequest.setAttribute("nofooter", "true");
  ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
  HttpServletResponse httpServletResponse = requestResponseFactory.createResponse(arrayOutputStream);
  try {
   requestProcessor.processRequest(httpServletRequest, httpServletResponse, resolver);
   InputStream isFromFirstData = new ByteArrayInputStream(arrayOutputStream.toByteArray());
   Reader rd = new BufferedReader(new InputStreamReader(isFromFirstData));
   kit.read(rd, doc, 0);
   text = doc.getText(0, doc.getLength());
   text = text.replaceAll("\n", " ");
  } catch (ServletException e) {
   LOGGER.error("Servlet Exception:", e.getMessage(),e);
  } catch (IOException e) {
   LOGGER.error("IO Exception", e.getMessage(),e);
  }
  return countWord(text);
 }
 
 /**
  * Count word.
  * 
  * @param word
  *         the word
  * @return the int
  */
 public int countWord(String word) {
  if (word == null) {
   return 0;
  }
  String input = word.trim();
  return input.isEmpty() ? 0 : input.split("\\s+").length;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see org.osgi.service.event.EventHandler#handleEvent(org.osgi.service.event.Event)
  */
 @Override
 public void handleEvent(Event event) {
  JobUtil.processJob(event, this);
 }
 
}
