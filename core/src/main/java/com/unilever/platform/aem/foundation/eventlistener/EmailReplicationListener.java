/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.eventlistener;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.engine.SlingRequestProcessor;
import org.apache.sling.event.jobs.JobProcessor;
import org.apache.sling.event.jobs.JobUtil;
import org.apache.sling.jcr.resource.JcrResourceConstants;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.contentsync.handler.util.RequestResponseFactory;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.WCMMode;
import com.unilever.platform.aem.foundation.configuration.ConfigurationHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;

/**
 * The listener interface for receiving emailReplication events. The class that is interested in processing a emailReplication event implements this
 * interface, and the object created with that class is registered with a component using the component's <code>addEmailReplicationListener
 * <code> method. When the emailReplication event occurs, that object's appropriate method is invoked.
 * 
 * @see EmailReplicationEvent
 */
@Component(configurationFactory = true, label = "Unilever Email Replicate Service", description = "Unilever Email Replicate Service configurations", metatype = true, immediate = true, enabled = true)
@Service(value = EventHandler.class)
@Properties({ @Property(name = EventConstants.EVENT_TOPIC, value = { "org/apache/sling/api/resource/Resource/*" }, propertyPrivate = true),
  @Property(name = EventConstants.EVENT_FILTER, value = { "(&(resourceType=*/pagecomponents/emailPage))" }, propertyPrivate = true) })
public class EmailReplicationListener implements EventHandler, JobProcessor {
 
 /** The logger. */
 private static final Logger LOGGER = LoggerFactory.getLogger(EmailReplicationListener.class);
 
 /** the constant for ".html" */
 private static final String DOT_HTML = ".html";
 
 /** The Constant GET. */
 private static final String GET = "GET";
 
 /** The Constant EMAIL_TEMPLATE_CONFIG. */
 private static final String EMAIL_TEMPLATE_CONFIG = "emailTemplateConfig";
 
 /** The Constant LOCALE. */
 private static final String LOCALE = "locale";
 
 /** The factory. */
 @Reference
 org.apache.http.osgi.services.HttpClientBuilderFactory factory;
 
 /** The resolver factory. */
 // Inject a Sling ResourceResolverFactory
 @Reference
 private ResourceResolverFactory resolverFactory;
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The request response factory. */
 @Reference
 private RequestResponseFactory requestResponseFactory;
 
 /** The request processor. */
 @Reference
 private SlingRequestProcessor requestProcessor;
 
 /*
  * (non-Javadoc)
  * 
  * @see org.osgi.service.event.EventHandler#handleEvent(org.osgi.service.event.Event)
  */
 @Override
 public void handleEvent(Event event) {
  JobUtil.processJob(event, this);
 }
 
 /**
  * Gets the html.
  * 
  * @param url
  *         the url
  * @param resolver
  *         the resolver
  * @return the html
  */
 private String getHtml(String url, ResourceResolver resolver) {
  String html = StringUtils.EMPTY;
  HttpServletRequest httpServletRequest = requestResponseFactory.createRequest(GET, url + DOT_HTML);
  httpServletRequest.setAttribute(WCMMode.REQUEST_ATTRIBUTE_NAME, WCMMode.DISABLED);
  ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
  HttpServletResponse httpServletResponse = requestResponseFactory.createResponse(arrayOutputStream);
  
  try {
   requestProcessor.processRequest(httpServletRequest, httpServletResponse, resolver);
   html = arrayOutputStream.toString();
  } catch (ServletException e) {
   LOGGER.error("Servlet Exception:", e.getMessage(),e);
  } catch (IOException e) {
   LOGGER.error("IO Exception", e.getMessage(),e);
  }
  return html;
 }
 
 /**
  * Post email.
  * 
  * @param requestBody
  *         the request body
  * @param page
  *         the page
  * @param timeout
  *         the timeout
  */
 private void postEmail(JSONObject requestBody, Page page, int timeout) {
  RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(timeout).setConnectionRequestTimeout(timeout).setSocketTimeout(timeout)
    .build();
  
  CloseableHttpClient httpclient = factory.newBuilder().disableAutomaticRetries().disableConnectionState().disableRedirectHandling()
    .setDefaultRequestConfig(requestConfig).build();
  
  String endPoint = getTargetEndPoint(page);
  HttpPost request = new HttpPost(endPoint);
  StringEntity params = null;
   params = new StringEntity(requestBody.toString(),"UTF-8");
   LOGGER.error("params are=" + params);
  request.addHeader("content-type", "application/json");
  request.setEntity(params);
  CloseableHttpResponse response = null;
  try {
   LOGGER.error("request is" + request.toString());
   response = httpclient.execute(request);
   LOGGER.error("response is" + response.toString());
   if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
    LOGGER.error("Email template posted successfully.");
   } else {
    LOGGER.error("Email template could not be posted.");
   }
  } catch (ClientProtocolException cpe) {
   LOGGER.error("Error while posting email template (cpe) : ", cpe.getMessage(),cpe);
  } catch (IOException ioe) {
   LOGGER.error("Error while posting email template (ioe) : ", ioe.getMessage(),ioe);
  } finally {
   try {
    response.close();
    httpclient.close();
   } catch (IOException ioe) {
    LOGGER.error("Could not release connection", ioe.getMessage(),ioe);
   }
   
  }
 }
 
 /**
  * Prepare email request.
  * 
  * @param pageProeprties
  *         the page proeprties
  * @param emailTemplate
  *         the email template
  * @param currentPage
  *         the current page
  * @param emailConfigurations
  *         the email configurations
  * @return the JSON object
  */
 private JSONObject prepareEmailRequest(ValueMap pageProeprties, String emailTemplate, Page currentPage, Map<String, String> emailConfigurations) {
  JSONObject jsonRequest = new JSONObject();
  
  try {
   jsonRequest.put("templateHtml", emailTemplate);
   jsonRequest.put(LOCALE, getLocale(currentPage, emailConfigurations));
   jsonRequest.put("brand", getBrandName(currentPage));
   jsonRequest.put("templateName", pageProeprties.get("emailTemplateType", String.class));
   LOGGER.error("JSON request=" + jsonRequest.toString());
  } catch (JSONException je) {
   LOGGER.error("Email template json error : ", je.getMessage(),je);
  }
  
  return jsonRequest;
 }
 
 /**
  * Gets the page properties.
  * 
  * @param currentPage
  *         the current page
  * @return the page properties
  */
 private ValueMap getPageProperties(Page currentPage) {
  
  ValueMap pageProperties = null;
  if (currentPage != null) {
   pageProperties = currentPage.getProperties();
  }
  return pageProperties;
 }
 
 /**
  * Gets the target end point.
  * 
  * @param page
  *         the page
  * @return the target end point
  */
 private String getTargetEndPoint(Page page) {
  Map<String, String> emailConfigurations = null;
  String target = StringUtils.EMPTY;
  try {
   emailConfigurations = configurationService.getCategoryConfiguration(page, EMAIL_TEMPLATE_CONFIG);
  } catch (ConfigurationNotFoundException cnfe) {
   LOGGER.error("Email template ConfigurationNotFoundException :", cnfe);
  }
  if (emailConfigurations != null) {
   target = emailConfigurations.get("serviceUrl");
  }
  return target;
 }
 
 /**
  * Gets the brand name.
  * 
  * @param page
  *         the page
  * @return the brand name
  */
 private String getBrandName(Page page) {
  Map<String, String> emailConfigurations = null;
  String target = StringUtils.EMPTY;
  try {
   emailConfigurations = configurationService.getCategoryConfiguration(page, EMAIL_TEMPLATE_CONFIG);
  } catch (ConfigurationNotFoundException cnfe) {
   LOGGER.error("Email template ConfigurationNotFoundException: ", cnfe);
  }
  if (emailConfigurations != null) {
   target = emailConfigurations.get("brand");
  }
  return target;
 }
 
 /**
  * Gets the locale.
  * 
  * @param currentPage
  *         the current page
  * @param emailConfigurations
  *         the email configurations
  * @return the locale
  */
 private String getLocale(Page currentPage, Map<String, String> emailConfigurations) {
  String locales = StringUtils.EMPTY;
  String country = StringUtils.EMPTY;
  Locale locale = null;
  if (emailConfigurations != null) {
   
   locales = emailConfigurations.get(LOCALE) != null ? emailConfigurations.get(LOCALE) : StringUtils.EMPTY;
   country = emailConfigurations.get("country") != null ? emailConfigurations.get("country") : StringUtils.EMPTY;
  }
  if (StringUtils.isEmpty(locales) || StringUtils.isEmpty(country)) {
   
   if (currentPage != null) {
    locale = currentPage.getLanguage(false);
    return locale.toLanguageTag();
   }
  } else {
   locale = new Locale(locales, StringUtils.upperCase(country));
  }
  return locale.toLanguageTag();
 }
 
 /**
  * Gets the page.
  * 
  * @param path
  *         the path
  * @param resourceResolver
  *         the resource resolver
  * @return the page
  * @throws LoginException
  *          the login exception
  */
 private Page getPage(String path, ResourceResolver resourceResolver) throws LoginException {
  Resource resource = resourceResolver.getResource(path).getParent();
  Page currentPage = null;
  if (resource != null) {
   currentPage = resource.adaptTo(Page.class);
  }
  return currentPage;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.event.jobs.JobProcessor#process(org.osgi.service.event.Event)
  */
 @Override
 public boolean process(Event event) {
  
  Map<String, String> emailConfigurations = null;
  ResourceResolver resolver = null;
  int timeout = 0;
  LOGGER.info("Event Topic " + event.getTopic());
  String emailTemplateHtml = StringUtils.EMPTY;
  ValueMap pageProperties = null;
  
  Page page = null;
  try {
   resolver = ConfigurationHelper.getResourceResolver(resolverFactory, "readService");
   String path = (String) event.getProperty(SlingConstants.PROPERTY_PATH);
   page = getPage(path, resolver);
   LOGGER.error("path=" + page.getPath());
   emailTemplateHtml = getHtml(page.getPath(), resolver);
   LOGGER.error("email Html Snippet=" + emailTemplateHtml);
   emailConfigurations = configurationService.getCategoryConfiguration(page, EMAIL_TEMPLATE_CONFIG);
   if (emailConfigurations != null) {
    
    timeout = emailConfigurations.get("timeout") != null ? Integer.parseInt(emailConfigurations.get("timeout")) : 0;
   }
   
   pageProperties = getPageProperties(page);
   String resourceType = pageProperties.get(JcrResourceConstants.SLING_RESOURCE_TYPE_PROPERTY, String.class);
   if (StringUtils.contains(resourceType, "email")) {
    JSONObject requestObject = prepareEmailRequest(pageProperties, emailTemplateHtml, page, emailConfigurations);
    LOGGER.error("request object is" + requestObject.toString());
    postEmail(requestObject, page, timeout);
   }
  } catch (ConfigurationNotFoundException cnfe) {
   LOGGER.error("Email template ConfigurationNotFoundException : ", cnfe.getMessage(),cnfe);
  } catch (LoginException le) {
   LOGGER.error("Email template LoginException : ", le.getMessage(),le);
  } finally {
   if (resolver != null) {
    resolver.close();
   }
  }
  
  return true;
  
 }
}
