/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.eventlistener;

import java.security.SecureRandom;
import java.util.Iterator;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.version.VersionException;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.event.jobs.JobProcessor;
import org.apache.sling.event.jobs.JobUtil;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageEvent;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.PageModification;
import com.unilever.platform.aem.foundation.configuration.ConfigurationHelper;
import com.unilever.platform.foundation.constants.UnileverConstants;

/**
 * A Listener to handle PageIdCreationListener.
 */
@SuppressWarnings("deprecation")
@Component(name = "com.unilever.platform.aem.foundation.eventlistener.PageIdCreationListener", label = "Page Uniqure ID Creation Service", description = "A service for creating Unique page ID", immediate = true, metatype = true)
@Service(value = EventHandler.class)
@Property(name = org.osgi.service.event.EventConstants.EVENT_TOPIC, value = { PageEvent.EVENT_TOPIC }, label = "Events", description = "Event to be tracked")
public class PageIdCreationListener implements EventHandler, JobProcessor {
 
 /** The Constant log. */
 private static final Logger LOGGER = LoggerFactory.getLogger(PageIdCreationListener.class);
 
 /** The resolver factory. Inject a Sling ResourceResolverFactory */
 @Reference
 private ResourceResolverFactory resolverFactory;
 
 /** The Constant MAX. */
 private static final int MAX = 999;
 
 /** The Constant MIN. */
 private static final int MIN = 100;
 
 @Override
 public void handleEvent(Event event) {
  JobUtil.processJob(event, this);
  
 }
 
 @Override
 public boolean process(Event event) {
  ResourceResolver resolver = null;
  String pagePath = StringUtils.EMPTY;
  try {
   PageEvent pageEvent = PageEvent.fromEvent(event);
   if (pageEvent != null) {
    Iterator<PageModification> modifications = pageEvent.getModifications();
    
    while (modifications.hasNext()) {
     
     PageModification modification = modifications.next();
     pagePath = modification.getPath();
    }
   }
   resolver = ConfigurationHelper.getResourceResolver(resolverFactory, "readService");
   PageManager pageManager = resolver.adaptTo(PageManager.class);
   Page page = pageManager.getContainingPage(pagePath);
   if (page != null) {
    Resource resource = page.getContentResource();
    Node node = resource.adaptTo(Node.class);
    try {
     if (!page.getProperties().containsKey(UnileverConstants.UNIQUE_PAGE_ID)) {
      node.setProperty(UnileverConstants.UNIQUE_PAGE_ID, getUniquePageId(page.getPath().hashCode()));
     } else if (StringUtils.isEmpty(page.getProperties().get(UnileverConstants.UNIQUE_PAGE_ID).toString())) {
      node.setProperty(UnileverConstants.UNIQUE_PAGE_ID, getUniquePageId(page.getPath().hashCode()));
     }
    } catch (ValueFormatException e) {
     LOGGER.error("Value FormatException:", e.getMessage(),e);
    } catch (VersionException e) {
     LOGGER.error("VersionException:", e.getMessage(),e);
    } catch (LockException e) {
     LOGGER.error("LockException:", e.getMessage(),e);
    } catch (ConstraintViolationException e) {
     LOGGER.error("ConstraintViolationException:", e.getMessage(),e);
    } catch (RepositoryException e) {
     LOGGER.error("RepositoryException:", e.getMessage(),e);
    }
   }
   try {
    resolver.commit();
   } catch (PersistenceException e) {
    LOGGER.error("PersistenceException:", e.getMessage(),e);
   }
   
  } catch (Exception e) {
   LOGGER.error("Exception occured in PageIdCreationListener : ", e.getMessage(),e);
  } finally {
   if (resolver != null) {
    resolver.close();
   }
  }
  
  return true;
 }
 
 /**
  * Gets the uniqueValue string.
  * 
  * @param hashCode
  *         the hashCode
  * @return the uniqueValue
  */
 private static String getUniquePageId(int hashCode) {
  String uniqueValue = StringUtils.EMPTY;
  SecureRandom rand = new SecureRandom();
  int randomNum = MIN + rand.nextInt((MAX - MIN) + 1);
  uniqueValue = String.valueOf(Math.abs(hashCode)) + randomNum;
  return uniqueValue;
 }
 
}
