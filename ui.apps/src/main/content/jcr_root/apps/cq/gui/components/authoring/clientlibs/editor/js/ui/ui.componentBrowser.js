/*
 * ADOBE CONFIDENTIAL
 *
 * Copyright 2013 Adobe Systems Incorporated
 * All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Adobe Systems Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Adobe Systems Incorporated and its
 * suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 */
;(function ($, ns, channel, window, undefined) {

    /**
     *
     * @type {Object}
     */
    ns.ui.componentBrowser = (function () {
        var self = {},
                colorLoop = 0,
                colorMap = {},
                colors = [
                    //'lavender',
                    //'purple',
                    'cyan',
                    'ocher',
                    //'violet',
                    'mint',
                    'aqua',
                    //'magenta',
                    'frog',
                    'orange',
                    'blue',
                    'darkgreen',
                    //'fuchsia',
                    'coral',
                    'yellow'
                ];

        /**
         * @type {string}
         */
        self.idSidePanel = "SidePanel";
        self.$sidePanel = $("#" + self.idSidePanel);
        self.$editPanel = self.$sidePanel.find(".js-sidePanel-edit");

        /**
         *
         * @type {string}
         */
        self.id = "sidepanel-tab-components";

        /**
         *
         * @type {*|HTMLElement}
         */
        self.$el = self.$sidePanel.find(".js-sidePanel-edit ." + self.id);
        self.$header = self.$sidePanel.find('.sidepanel-header')

        /**
         * @type {jQuery} filter container
         */
        self.$elFilterPanel = self.$el.find('.filter-panel');

        /**
         * @type {jQuery} content container
         */
        self.$elContentPanel = self.$el.find('.content-panel');

        /**
         *
         * @type {*|HTMLElement}
         */
        self.$search = self.$elFilterPanel.find(".search");

        /**
         *
         * @type {*|HTMLElement}
         */
        self.$clear = self.$el.find("#components-filter .search").parent(".coral-DecoratedTextfield")
                .find('button, .coral-Icon--close');

        /**
         * @type {jQuery} container for the type selector
         */
        self.$elComponentGroup = self.$el.find('.componentfilter.group');

        /**
         *
         * @type {*|HTMLElement}
         */
        self.$grid = self.$elContentPanel.find(".components.grid");

        /**
         *
         * @type {Object} Component groups
         */
        self.COMPONENT_GROUPS = {
            ALL: "All",
            GENERAL: "General"
        };

        self.defaultGroup = self.COMPONENT_GROUPS["ALL"];
        self.currentGroup = self.defaultGroup;


        /**
         * Generate component cards from HTML ajax response
         */
        self.generateComponents = function (htmlResponse) {
            var components = [];

            var $htmlResponse = $(htmlResponse);

            // Push all components to the store
            $.each($htmlResponse, function (i, html) {
                if ($(this).is("article[data-type='" + ns.Component.prototype.getTypeName() + "']")) {
                    components.push(new ns.Component(html));
                }
            });

            ns.components.set(components);
        };

        /*
         Handler for the filtering of the components
         */
        self.handleFilterComponents = function (event) {
            var clearAll = (event.type === 'cq-components-filtered');

            self.filterComponents({
                keepItems: !clearAll,
                clearFilter: clearAll
            });
        };

        /**
         * Check if components is active tab
         * @returns {*}
         */
        self.tabIsActive = function (event) {
            return (self.$editPanel.is(":visible") && self.$el.hasClass("is-active"));
        };

        /**
         * Reset component groups list
         */
        channel.on('cq-components-store-cleaned', function() {
            self.$elComponentGroup.find("select").empty();
            self.$elComponentGroup.find("ul").empty();
        });

        /**
         * Refresh components groups list
         */
        channel.on('cq-components-filtered', function() {
            // Sort components by title and group
            ns.components.allowedComponents.sort(self._componentsComparator);

            // Empty list of component groups
            var $selectionList = self.$elComponentGroup.find("select");
            $selectionList.empty();

            var $groupList = self.$elComponentGroup.find("ul");
            $groupList.empty();

            // Add "All" component group
            appendGroup(self.COMPONENT_GROUPS["ALL"], { "data-all": true });

            // Add other groups
            var groups = [];
            $.each(ns.components.allowedComponents, function(i, component) {
                var group = component.getGroup();
                if (group && $.inArray(group, groups) === -1) {

                    appendGroup(group);

                    groups.push(group);

                    // set color for a category
                    colorMap[group] = colors[colorLoop];
                    colorLoop = colorLoop + 1 === colors.length ? 0 : colorLoop + 1;
                }

                $(component.html).find('a').addClass('card-' + (group === 'General' ? 'green' : colorMap[group]));
            });
            self._setSearchGroup(self.defaultGroup);
            // Event on card
            self.$grid.fipo("tap", "click", "article[class*='card-component']", function(event) {
                event.preventDefault();
            });
        });

        /**
         * Event: Set card view on editor-panel if accessed
         */
        channel.on("cq-components-filtered cq-sidepanel-tab-switched cq-sidepanel-resized", function (event) {
            // redraw if panel gets resized
            var panelRepositioned = event.type === 'cq-sidepanel-resized';
            if ($(".sidepanel-opened").length
                    && ((self.tabIsActive(event)) || panelRepositioned)) {
                // Filter components when editor panel becomes visible
                // to ensure card layout is fine (can't layout it if hidden)
                // But keep current items in that case
                self.handleFilterComponents(event);
            }

            // set header title
            if (self.$editPanel.is(":visible") && event.tabName === self.id) {
                self.$header.find('.sidepanel-header-title').hide();
                self.$header.find('.sidepanel-header-title-components').show();
            }
        });

        /*
         Event: Window resize
         */
        $(window).on('resize', function (event) {
            if ($(".sidepanel-opened").length) {
                self.handleFilterComponents(event);
            }
        });

        /**
         * Filter components by group
         */
        self.$elComponentGroup.on('click', function (event) {
            // Trigger the change event in any case
            // - as the select reacts on ul click or on native select
            if ($(event.target).data("value")) {
                self.$elComponentGroup.find("select").trigger("change");
            }
        });

        self.$elComponentGroup.find("select").on('change', function (event) {
            var group = $(event.target).find("option:selected").val();

            if (group) {
                self._setSearchGroup(group);

                self.filterComponents({
                    clearFilter: true
                });
            }
        });

        /**
         * Calculate the height if collapsible is getting active or inactive
         */
        self.$elFilterPanel.find('.collapsible').on('activate deactivate', function (event) {
            self._setContentPanelHeight();
        });

        /**
         * Filter components by keyword
         */
        self.$search
                .on("keydown", function(event) {
                    var keycode = Granite.Util.getKeyCode(event);
                    if (keycode == '13') {
                        self.filterComponents();
                    }
                })
                .on("keyup", function() {
                    // Start search if input value is empty
                    if (!$(this).val().trim().length) {
                        self.filterComponents();
                    }
                })
        ;
        self.$search.fipo("tap", "click", "button", function() {
            self.filterComponents();
        });

        self.$clear.fipo("tap", "click", function() {
            if (self._getSearchKeyword().length) {
                self.filterComponents({
                    clearFilter: true
                });
            }
        });

        self.filterComponents = function(options) {
            options = options || {};

            var group = self._getSearchGroup();
            var keyword;
            if (options.clearFilter) {
                keyword = "";
                self.$search.val(keyword);
            } else {
                keyword = self._getSearchKeyword();
            }

            // Set card view and eventually empty current items
            self._setCardView(!options.keepItems);

            // Append components to its container and update list of groups
            var cards = [];

            $.each(ns.components.allowedComponents, function(i, component) {
                var display = group === self.COMPONENT_GROUPS["ALL"] ||
                        group === component.getGroup();
                if (display && keyword) {
                    var regExp = new RegExp(".*" + keyword + ".*", "i");
                    display = regExp.test(Granite.I18n.getVar(component.getTitle()));
                }

                display = group.indexOf(".") !== 0 ? display : null;


                if (display) {
                    cards.push(component.toHtml());
                }
            });

            var hasResults = cards.length > 0;
            self.$el.find(".emptyresult").toggle(!hasResults);

            self._setContentPanelHeight();
            if (hasResults) {
                // Append components grid to container
                self.cardView.append(cards);
            }

            self._setCardView(false);
        };

        self._setSearchGroup = function(group) {
            self.currentGroup = self.$elComponentGroup.find('option[value="' + group + '"]').val();
        };

        self._getSearchGroup = function() {
            return self.$elComponentGroup.find('option:selected').val();
        };

        self._getSearchKeyword = function() {
            return self.$search.val().trim();
        };

        self._componentsComparator = function(c1, c2) {
            var title1 = Granite.I18n.getVar(c1.getTitle());
            var title2 = Granite.I18n.getVar(c2.getTitle());

            if (title1 === title2) {
                if (c1.getGroup() < c2.getGroup()) {
                    return -1;
                } else if (c1.getGroup() > c2.getGroup()) {
                    return 1;
                }

                return 0;
            }

            return title1 < title2 ? -1 : 1;
        };

        self._setCardView = function(removeItems) {
            // cardview
            self.cardView = CUI.CardView.get(self.$grid);
            self.cardView.setDisplayMode(CUI.CardView.DISPLAY_GRID);
            // adapt the grid layout
            self._adaptCardLayout();

            if (removeItems) {
                self.cardView.removeAllItems();
            }
        };

        /**
         * adapt the grid layout
         */
        self._adaptCardLayout = function() {
            self.cardView.layout( {
                colWidth: 145,
                gutterX: 14,
                gutterY: 14
            });
        };

        /**
         * Set the correct height for the content-panel container
         *
         * As the filter containers' height AND the
         * content containers' height is flexible
         * there is no technical possibility to solve this with pure css
         * @private
         */
        self._setContentPanelHeight = function () {
            var h = $(window).height(),
                    offset = self.$elContentPanel.offset();

            self.$elContentPanel.height(h - offset.top);
        };

        /**
         * Append available component groups, read out from response
         * @param group
         * @param attributes
         */
        function appendGroup(group, attributes) {
            var $li = $('<option/>', {
                value: group,
                text: Granite.I18n.getVar(group)
            });

            $li.appendTo(self.$elComponentGroup.find('select'));

            var $option = $('<li/>', {
                'class': 'coral-SelectList-item coral-SelectList-item--option',
                'data-value': group,
                'role': 'option',
                'text': Granite.I18n.getVar(group)
            });

            if ($.isPlainObject(attributes)) {
                for (var key in attributes) {
                    $option.attr(key, attributes[key]);
                }
            }

            $option.appendTo(self.$elComponentGroup.find('ul'));
        }

        return self;

    }());

}(jQuery, Granite.author, jQuery(document), this));
