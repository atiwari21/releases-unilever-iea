/*
 * ADOBE CONFIDENTIAL
 *
 * Copyright 2013 Adobe Systems Incorporated
 * All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Adobe Systems Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Adobe Systems Incorporated and its
 * suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 */
(function(document, ns, _g, $) {
    "use strict";
    
    MSM.Rollout = MSM.Rollout || (function() {
        var self = {};
        
        self.isPageRollout = true;
        
        self.init = function () {
            var $liveCopies = $(".live-copy-list article .select-rollout");
            var totalLiveCopies = $(".live-copy-list").data("live-copy-list-size");
            var $liveCopiesChecked = $(".live-copy-list article .select-rollout:checked");
            var $headerCheck = $(".live-copy-list header .select-rollout");
            
            if (totalLiveCopies == $liveCopiesChecked.length) {
                $headerCheck.prop("checked", true);
            }
            
            self.isPageRollout = MSM.MSMCommons.getSelection().length <= 0;
            
            if (self.isPageRollout) {
                $(".msm-rollout-deep-container").show();
            } else {
                $(".msm-rollout-deep-container").hide();
            }
            
            var dialogTitle = "";
            if (totalLiveCopies == $liveCopies.length) {
                dialogTitle = Granite.I18n.get("Showing {0} Live Copies", [totalLiveCopies]);
            } else {
                dialogTitle = Granite.I18n.get("Showing {0} out of {1} Live Copies", [$liveCopies.length, totalLiveCopies]);
                
                // we have a lot of live copies, more exist than displayed
                // mark flag so we do a background rollout
                $(".live-copy-list").data("bacground-rollout", true);
            }
            
            $(".msm-rollout-dialog-title").html(dialogTitle);
        };
        
        /**
         * Executes the rollout command
         * @param {String} commandPath: rollout command url
         * @param {String} blueprint: source path for the rollout operation (blueprint)
         * @param {Object} $targets: jquery array containing the rollout target paths
         * @param {Object} isBackgroundRollout: boolean indicating if a background rollout should be performed
         */
        self.doRollout = function(commandPath, blueprint, $targets, isBackgroundRollout, window) {
            var selectedEditables = MSM.MSMCommons.getSelection();
            var selectedComponentPaths = [];
            $.each(selectedEditables, function(index, editable) {
                selectedComponentPaths.push(editable.path);
            });
            
            var isDeepRollout = $(".msm-rollout-deep:checked").length > 0;
            
            var rolloutType = MSM.MSMCommons.Constants.ROLLOUT_TYPE_PAGE;
            if (!self.isPageRollout) {
                rolloutType = MSM.MSMCommons.Constants.ROLLOUT_TYPE_SELECTED;
            } else if (isDeepRollout) {
                rolloutType = MSM.MSMCommons.Constants.ROLLOUT_TYPE_DEEP;
            }
            
            if ($targets
                    && $targets.length > 0) {
                // send individual rollout commands for each selected target path
                var selectedCount = $targets.length;
                var successCount = 0;
                var errorCount = 0;

                $targets.each(function(index, item) {
                    var $selectedLC = $(item);
                    var $article = $selectedLC.parents("article");
                    var targetPath = $article.data("path");
                    
                    var rolloutCmdParams = {};
                    rolloutCmdParams[MSM.MSMCommons.Constants.PARAM_ROLLOUT_TYPE] = rolloutType;
                    rolloutCmdParams[MSM.MSMCommons.Constants.PROP_TARGET_PATH] = targetPath;

                    _sendRolloutXHR(commandPath, blueprint, rolloutCmdParams, selectedComponentPaths, function (data, textStatus, jqXHR) {
                        successCount++;
                        $.ajax({
                            url: "/bin/pageProperties/description" + "?rolloutPagePaths=" + blueprint + "," + targetPath,
                            async: false,
                            method: "POST",
                            success: function(data, textStatus, jQxhr) {

                            },
                            error: function(jqXhr, textStatus, errorThrown) {
                                console.log("Error occured while posting page description" + errorThrown);
                            }


                        });
                        _showRolloutResultNotification(selectedCount, errorCount, selectedCount, window);
                    }, function(jqXHR, textStatus, errorThrown){
                        errorCount++;
                        _showRolloutResultNotification(selectedCount, errorCount, selectedCount, window);
                    });
                });
            } else {
                var rolloutCmdParams = {};
                rolloutCmdParams[MSM.MSMCommons.Constants.PARAM_ROLLOUT_TYPE] = rolloutType;
                if (isBackgroundRollout === true) {
                    rolloutCmdParams["sling:bg"] = "true";
                }
                // one single background rollout command, will apply for all existing LC targets
                _sendRolloutXHR(commandPath, blueprint, rolloutCmdParams, selectedComponentPaths, function (data, textStatus, jqXHR) {
                    _showRolloutResultNotification(1, 0, 1, window);
                }, function(jqXHR, textStatus, errorThrown){
                    _showRolloutResultNotification(1, 1, 0, window);
                });
            }
            
            $(".rollout-popover").hide();
        };
        
        var _sendRolloutXHR = function(rolloutCommandPath, blueprint, params, selectedParagraphs, successHandler, errorHandler) {
            params[MSM.MSMCommons.Constants.PARAM_ROLLOUT_PARAGRAPHS] = selectedParagraphs;
            params[MSM.MSMCommons.Constants.PARAM_COMMAND] = MSM.MSMCommons.Constants.COMMAND_ROLLOUT;
            params[MSM.MSMCommons.Constants.PARAM_PATH] = blueprint;
            params["_charset_"] = "utf-8";
            
            $.ajax({
                type: "POST",
                url: rolloutCommandPath,
                traditional: true,
                data: params
            }).done(successHandler).fail(errorHandler);
        }
        
        var _showRolloutResultNotification = function(total, error, success, window) {
            if (error + success >= total) {
                var successMessage = self.isPageRollout
                                        ? Granite.I18n.get("The page has been rolled out successfully")
                                        : Granite.I18n.get("The component has been rolled out successfully")

                MSM.MSMCommons.updateSelection([]);
                var errorMessage = Granite.I18n.get("{0} Rollout operations failed", [error]);
                if(typeof ns !== "undefined"){
                 ns.ui.helpers.notify({
                    content: error == 0 
                            ? successMessage
                            : Granite.I18n.get("{0} Rollout operations failed", [error]),
                    type: error == 0
                            ? "info"
                            : "error",
                    closable: false,
                    heading: ""
                });
                }else if(success > 0){
                $(window).adaptTo("foundation-ui").alert("info", successMessage);
                }else if(error > 0){
                $(window).adaptTo("foundation-ui").alert("error", errorMessage);
                }

            }
        };
        
        return self;
    }());
        
    $(document).on("foundation-contentloaded", function (e) {
        MSM.Rollout.init();
        
        $(".live-copy-list header .select-rollout").off("tap.actions click.actions").fipo("tap.actions", "click.actions", function(event) {
            var selectAll = $(event.target).is(':checked');
            $(".live-copy-list article .select-rollout").prop("checked", selectAll);
            event.stopPropagation();
        });
        
        $(".live-copy-list article .select-rollout").off("tap.actions click.actions").fipo("tap.actions", "click.actions", function(event) {
            var numChecked = $(".live-copy-list article .select-rollout:checked").size();
            var totalLiveCopies = $(".live-copy-list").data("live-copy-list-size");
            $(".live-copy-list header .select-rollout").prop("checked", numChecked == totalLiveCopies);
            event.stopPropagation();
        });
        
        $(".live-copy-list article .select-rollout").next().off("tap.actions click.actions").fipo("tap.actions", "click.actions", function(event) {
            event.stopPropagation();
        });
        
        $(".msm-rollout-deep").off("tap click").fipo("tap.rollout-deep", "click.rollout-deep", function(event) {
            event.stopPropagation();
        })
        
        if ($(".msm-rollout-submit").length > 0) {
            // only use the dialog submit if this is a blueprint page
            $(document).off("submit", "form.msm-rollout-dialog.foundation-form").on("submit", "form.msm-rollout-dialog.foundation-form", function(e) {
                e.preventDefault();
            });

            $(".msm-rollout-submit").off("tap click").fipo("tap", "click", function(event){
                var form = $(this).parents("form");
                var action = form.prop("action");
                var blueprintPath = form.data("msm-blueprint-path");
                event.preventDefault();
                
                // send only one command if all checked
                var $headerChecked = $(".live-copy-list header .select-rollout:checked");
                var $selectedTargets = [];
                if ($headerChecked.length <= 0) {
                    $selectedTargets = $(".live-copy-list article .select-rollout:checked");
                }

                var isBg = $(".live-copy-list").data("bacground-rollout");

                MSM.Rollout.doRollout(action, blueprintPath, $selectedTargets, isBg, $(window));
                setTimeout(function(){
                form.trigger("foundation-form-submitted", [true]);
                                       }, 3000);
            });

        }
    });
    
    MSM.Rollout.init();
})(document, Granite.author, _g, Granite.$);
