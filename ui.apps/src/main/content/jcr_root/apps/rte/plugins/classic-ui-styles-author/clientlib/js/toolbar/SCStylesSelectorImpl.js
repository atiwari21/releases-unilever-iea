CQ.Ext.ns("SC.rte.plugins");
/*************************************************************************
*
* RTE Plugins
* ___________________
*
* Create ACT Style toolbar renderer for following plugins:
* 1. fontstyles
* 2. fontcolor
*
* @author Mohit K. Bansal
*
**************************************************************************/
SC.rte.plugins.SCStylesSelectorImpl = new Class({

    toString: "SCStyleSelectorImpl",

    extend: CUI.rte.ui.TbStyleSelector,

    // Interface implementation ------------------------------------------------------------

    addToToolbar: function(toolbar) {
        var com = CUI.rte.Common;

        this.toolbar = toolbar;
        if (com.ua.isIE) {
            // the regular way doesn't work for IE anymore with Ext 3.1.1, hence working
            // around
            var helperDom = document.createElement("span");
            helperDom.innerHTML = "<select class=\"x-font-select\">"
                    + this.createStyleOptions() + "</span>";
            this.styleSelector = CQ.Ext.get(helperDom.childNodes[0]);
        } else {
            this.styleSelector = CQ.Ext.get(CQ.Ext.DomHelper.createDom({
                tag: "select",
                cls: "x-font-select",
                html: this.createStyleOptions()
            }));
        }
        this.initializeSelector();
		var title = this.id.replace("font", "");
        toolbar.add(
            CQ.I18n.getMessage(title.charAt(0).toUpperCase() + title.slice(1)),
            " ",
            this.styleSelector.dom
        );
    },

    createToolbarDef: function() {
        return [ {
                "xtype": "panel",
                "itemId": this.id,
                "html": "<select class=\"x-font-select\">"
                    + this.createStyleOptions() + "</span>",
                "listeners": {
                    "afterrender": function() {
                        var item = this.toolbar.items.get(this.id);
                        if (item && item.body) {
                            this.styleSelector = CQ.Ext.get(item.body.dom.childNodes[0]);
                            this.initializeSelector();
                        }
                    },
                    "scope": this
                }
            }
        ];
    },

    initializeSelector: function() {
        this.styleSelector.on('change', function() {
            var style = this.styleSelector.dom.value;
            if (style.length > 0) {

            }
            this.plugin.execute(this.id);
        }, this);
        this.styleSelector.on('focus', function() {
            this.plugin.editorKernel.isTemporaryBlur = true;
        }, this);
        // fix for a Firefox problem that adjusts the combobox' height to the height
        // of the largest entry
        this.styleSelector.setHeight(19);
    },

    getSelectorDom: function() {
        return this.styleSelector.dom;
    },

    getSelectedStyle: function() {
        var style = this.styleSelector.dom.value;
        if (style.length > 0) {
            return style;
        }
        return null;
    },

    selectStyles: function(styles, selDef) {
        var indexToSelect;
        var styleableObject = selDef.selectedDom;
        var selectorDom = this.getSelectorDom();
        if (styles.length == 0) {
            indexToSelect = 0;
        } else if (styles.length > 1) {
            indexToSelect = -1;
        } else {
            if (selDef.isContinuousStyle || styleableObject) {
                var styleToSelect = styles[0];
                var options = selectorDom.options;
                for (var optIndex = 0; optIndex < options.length; optIndex++) {
                    var optionToCheck = options[optIndex];
                    if (optionToCheck.value == styleToSelect) {
                        indexToSelect = optIndex;
                        break;
                    }
                }
            } else {
                indexToSelect = -1;
            }
        }
        selectorDom.selectedIndex = indexToSelect;
        if (styleableObject != null) {
            selectorDom.disabled = false;
        } else if (selDef.isSelection) {
            selectorDom.disabled = false;
        } else {
            selectorDom.disabled = true;
        }
    }
});