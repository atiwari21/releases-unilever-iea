CQ.Ext.ns("SC.rte.commands");
/*************************************************************************
*
* RTE Plugins
* ___________________
*
* Styles Command manager for following plugins:
* 1. fontstyles
* 2. fontcolor
*
* @author Mohit K. Bansal
*
*
**************************************************************************/
SC.rte.commands.SCStyle = new Class({

    toString: "SCStyle",

    extend: CUI.rte.commands.Command,

    addStyle: function(execDef) {
        var sel = CUI.rte.Selection;
        var com = CUI.rte.Common;
        var styleName = execDef.value.val;
        var styleList = execDef.value.styles;
        var selection = execDef.selection;
        var context = execDef.editContext;
        // handle DOM elements
        var selectedDom = sel.getSelectedDom(context, selection);
        var styleableObjects = CUI.rte.plugins.StylesPlugin.STYLEABLE_OBJECTS;
        if (selectedDom && com.isTag(selectedDom, styleableObjects)) {
            com.removeAllClasses(selectedDom);
            com.addClass(selectedDom, styleName);
            return;
        }
        // handle text fragments
        var nodeList = execDef.nodeList;

        if (nodeList) {
            if(selection.startNode.parentNode.tagName == "SPAN") {
                var newStyles = [];
                var existingStyles = selection.startNode.parentNode.className.split(" ");
                for(var i=0; i<existingStyles.length;i++) {
                    var status = true;
                    for(var j=0; j<styleList.length; j++) {
                        if(existingStyles[i] == styleList[j].cssName) {
                            status = false;
                        }
                    }
                    if(status) {
                        newStyles.push(existingStyles[i]);
                    }
				}
                if(null != styleName) {
                    newStyles.push(styleName);
                }

                selection.startNode.parentNode.className = newStyles.join(" ");
            } else {
                nodeList.surround(execDef.editContext, "span", {
                    "className": styleName
                });
            }
        }
    },

    isCommand: function(cmdStr) {
        var cmdLC = cmdStr.toLowerCase();
        return (cmdLC == "applyscstyle");
    },

    getProcessingOptions: function() {
        var cmd = CUI.rte.commands.Command;
        return cmd.PO_BOOKMARK | cmd.PO_SELECTION | cmd.PO_NODELIST;
    },

    execute: function(execDef) {
        switch (execDef.command.toLowerCase()) {
            case "applyscstyle":
                this.addStyle(execDef);
                break;
        }
    },

    queryState: function(selectionDef, cmd) {
        // todo find a meaningful implementation -> list of span tags?
        return false;
    }

});


// register command
CUI.rte.commands.CommandRegistry.register("_scstyle", SC.rte.commands.SCStyle);