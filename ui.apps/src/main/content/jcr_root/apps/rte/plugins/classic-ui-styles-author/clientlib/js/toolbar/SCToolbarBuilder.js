/*************************************************************************
*
* RTE Plugins
* ___________________
*
* Extend toolbar builder to register different commands
*
* @author Mohit K. Bansal
*
**************************************************************************/
(function() {
    CQ.Ext.override(CUI.rte.ui.ext.ExtToolbarBuilder, {
        createSCStyleSelector: function(id, plugin, tooltip, styles) {
            return new SC.rte.plugins.SCStylesSelectorImpl(id, plugin, false, tooltip,
                    false, undefined, styles);
        }
    });
}());