/*************************************************************************
*
* RTE Plugins
* ___________________
*
* Create ACT Style toolbar renderer for following plugins:
* 1. fontstyles
* 2. fontcolor
*
* @author Mohit K. Bansal
*
**************************************************************************/

ACT.rte.ui.StyleSelectorImpl = new Class({

    toString: "ACTStyleSelectorImpl",

    extend: CUI.rte.ui.TbStyleSelector,

    // Helpers -----------------------------------------------------------------------------

	notifyGroupBorder: function() {
        if(ACT.rte.DEBUG.STYLESELECTOR || ACT.rte.DEBUG.ALL) {
           console.log("StyleSelectorImpl - notifyGroupBorder()");
        }
		// do nothing
	},

	_getStyleId: function($button) {
        if(ACT.rte.DEBUG.STYLESELECTOR || ACT.rte.DEBUG.ALL) {
        	console.log("StyleSelectorImpl - _getStyleId()");
        }
		var styleId = null;
		var targetId = $button.data("action");
		var hashPos = targetId.indexOf("#");
		if (hashPos >= 0) {
			styleId = targetId.substring(hashPos + 1);
		}
		return styleId;
	},

	_resetSelection: function() {
        if(ACT.rte.DEBUG.STYLESELECTOR || ACT.rte.DEBUG.ALL) {
        	console.log("StyleSelectorImpl - _resetSelection()");
        }
		var $selectorItems = this.$ui.find("i");
		$selectorItems.removeClass("coral-Icon--check");
	},

	_select: function(styleToSelect) {
        if(ACT.rte.DEBUG.STYLESELECTOR || ACT.rte.DEBUG.ALL) {
        	console.log("StyleSelectorImpl - _select()");
        }
		var self = this;
		this.$ui.each(function() {
			var $fmtItem = $(this);
			var styleId = self._getStyleId($fmtItem);
			if (styleId && (styleId === styleToSelect)) {
				$fmtItem.find("i").addClass("coral-Icon--check");
			}
		});
	},


	// Interface implementation ------------------------------------------------------------

	addToToolbar: function(toolbar) {
        if(ACT.rte.DEBUG.STYLESELECTOR || ACT.rte.DEBUG.ALL) {
        	console.log("StyleSelectorImpl - addToToolbar()");
        }

        var commandRef = "#" + this.id;
        toolbar.push({
            "ref": commandRef,
            "plugin": this.plugin.pluginId,
            "command": this.id
        });
	},

	notifyToolbar: function(toolbar, skipHandlers) {
        if(ACT.rte.DEBUG.STYLESELECTOR || ACT.rte.DEBUG.ALL) {
        	console.log("StyleSelectorImpl - notifyToolbar()");
        }
		this.toolbar = toolbar;
		var self = this;
		var pluginId = this.plugin.pluginId;
		var $cont = toolbar.getToolbarContainer();
		var tbType = toolbar.tbType;
		if (!this.plugin.hasStylesConfigured()) {
			var styles = [ ];
			var $popover = CUI.rte.UIUtils.getPopover(pluginId, tbType, $cont);
			var $styleItems = $popover.find("li");
			$styleItems.each(function() {
				var $button = $(this).find("button");
				var href = $button.data("action");
				var action = href.split("#");
				if ((action.length === 2) && (action[0] === pluginId)) {
					styles.push({
						"cssName": action[1],
						"text": $button.text()
					});
				}
			});
			this.plugin.setStyles(styles);
		}
		var $tbCont = CUI.rte.UIUtils.getToolbarContainer($cont, tbType);
		this.$trigger = $tbCont.find('button[data-action="#' + pluginId + '"]');
		this.$ui = $tbCont.find('div[data-id="' + pluginId + '"] button[data-action^="styles#"]');
		if (!skipHandlers) {
			this.$ui.on("click.rte-handler", function (e) {
				if (!self.$ui.hasClass(CUI.rte.Theme.TOOLBARITEM_DISABLED_CLASS)) {
					var targetId = $(this).data("action");
					var hashPos = targetId.indexOf("#");
					var style = targetId.substring(hashPos + 1);
					var editContext = self.plugin.editorKernel.getEditContext();
					editContext.setState("CUI.SelectionLock", 1);
					self.plugin.execute(ACT.rte.COMMAND.STYLES, style);
					self.plugin.editorKernel.enableFocusHandling();
					self.plugin.editorKernel.focus(editContext);
				}
				// e.stopPropagation();
			});
		}
	},

	createToolbarDef: function() {
        if(ACT.rte.DEBUG.STYLESELECTOR || ACT.rte.DEBUG.ALL) {
        	console.log("StyleSelectorImpl - createToolbarDef()");
        }
		return {
			"id": this.id,
			"element": this
		};
	},

	initializeSelector: function() {
        if(ACT.rte.DEBUG.STYLESELECTOR || ACT.rte.DEBUG.ALL) {
        	console.log("StyleSelectorImpl - initializeSelector()");
        }
		// TODO ...?
	},

	getSelectorDom: function() {
        if(ACT.rte.DEBUG.STYLESELECTOR || ACT.rte.DEBUG.ALL) {
        	console.log("StyleSelectorImpl - getSelectorDom()");
        }
		return this.$ui;
	},

	getSelectedStyle: function() {
        if(ACT.rte.DEBUG.STYLESELECTOR || ACT.rte.DEBUG.ALL) {
        	console.log("StyleSelectorImpl - getSelectedStyle()");
        }
		return null;
	},

	selectStyles: function(styles, selDef) {
        if(ACT.rte.DEBUG.STYLESELECTOR || ACT.rte.DEBUG.ALL) {
        	//console.log("StyleSelectorImpl - selectStyles()");
        }
		this.setSelected(styles.length > 0);
		this._resetSelection();
		for (var s = 0; s < styles.length; s++) {
			this._select(styles[s]);
		}
	},

	setDisabled: function(isDisabled) {
        if(ACT.rte.DEBUG.STYLESELECTOR || ACT.rte.DEBUG.ALL) {
        	//console.log("StyleSelectorImpl - setDisabled()");
        }
		var com = CUI.rte.Common;
		if (com.ua.isTouchInIframe) {
			// workaround for CUI-649; see ElementImpl#setDisabled for an explanation
			this.$trigger.css("display", "none");
		}
		if (isDisabled) {
			this.$trigger.addClass(CUI.rte.Theme.TOOLBARITEM_DISABLED_CLASS);
			this.$trigger.attr("disabled", "disabled");
		} else {
			this.$trigger.removeClass(CUI.rte.Theme.TOOLBARITEM_DISABLED_CLASS);
			this.$trigger.removeAttr("disabled");
		}
		if (com.ua.isTouchInIframe) {
			// part 2 of workaround for CUI-649
			var self = this;
			window.setTimeout(function() {
				self.$trigger.css("display", "inline-block");
			}, 1);
		}
	},

	setSelected: function(isSelected, suppressEvent) {
        if(ACT.rte.DEBUG.STYLESELECTOR || ACT.rte.DEBUG.ALL) {
        	//console.log("StyleSelectorImpl - setSelected()");
        }
		this._isSelected = isSelected;
		if (isSelected) {
			this.$trigger.addClass(CUI.rte.Theme.TOOLBARITEM_SELECTED_CLASS);
		} else {
			this.$trigger.removeClass(CUI.rte.Theme.TOOLBARITEM_SELECTED_CLASS);
		}
	},

	isSelected: function() {
        if(ACT.rte.DEBUG.STYLESELECTOR || ACT.rte.DEBUG.ALL) {
        	console.log("StyleSelectorImpl - isSelected()");
        }
		return this._isSelected;
	}
});