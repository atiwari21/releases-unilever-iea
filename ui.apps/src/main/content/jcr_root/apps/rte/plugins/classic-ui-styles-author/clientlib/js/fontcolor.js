CQ.Ext.ns("SC.rte.plugins");
/*************************************************************************
*
* RTE Plugins
* ___________________
*
* Font Color Plugin
*
* @author Mohit K. Bansal
*
*
**************************************************************************/
SC.rte.plugins.Fontcolor = new Class({
    toString: "Fontcolor",
    extend: CUI.rte.plugins.Plugin,
    /**
     * @private
     */
    cachedStyles: null,

    /**
     * @private
     */
    stylesUI: null,

    getFeatures: function() {
        return [ "fontcolor" ];
    },

	getStyles: function() {
        var com = CUI.rte.Common;
        if (!this.cachedStyles) {
            this.cachedStyles = this.config.styles;
            if (this.cachedStyles) {
                // take styles from config
                com.removeJcrData(this.cachedStyles);
                this.cachedStyles = com.toArray(this.cachedStyles, "cssName", "text");
            } else {
                this.cachedStyles = [ ];
            }
        }
        return this.cachedStyles;
    },

    setStyles: function(styles) {
        this.cachedStyles = styles;
    },

    hasStylesConfigured: function() {
        return !!this.config.styles;
    },

    initializeUI: function(tbGenerator) {
        var plg = CUI.rte.plugins;
        var ui = CUI.rte.ui;
        if (this.isFeatureEnabled("fontcolor")) {
            this.stylesUI = tbGenerator.createSCStyleSelector("fontcolor", this, null,
                    this.getStyles());
            tbGenerator.addElement("fontcolor", plg.Plugin.SORT_PARAFORMAT, this.stylesUI,
                    800);
        }
    },

	notifyPluginConfig: function(pluginConfig) {
        pluginConfig = pluginConfig || { };
        CUI.rte.Utils.applyDefaults(pluginConfig, { });
        this.config = pluginConfig;
    },

    execute: function(cmdId, styleDef) {
		if (!this.stylesUI) {
            return;
        } 
        var cmd = null;
        var value = null;
        switch (cmdId.toLowerCase()) {
            case "fontcolor":
                cmd = "applyscstyle";
                value = (styleDef != null ? styleDef : this.stylesUI.getSelectedStyle());
                break;
        }
        if (cmd) {
            var confValue = {
                val: value,
                styles: this.cachedStyles
            };
            this.editorKernel.relayCmd(cmd, confValue);
        }
    },

    updateState: function(selDef) {
        if (!this.stylesUI) {
            return;
        }
		var com = CUI.rte.Common;
        var styles = selDef.styles;
        var actualStyles = [ ];
        var s;
        var styleableObject = selDef.selectedDom;
		if (styleableObject) {
            if (!CUI.rte.Common.isTag(selDef.selectedDom,
                    CUI.rte.plugins.StylesPlugin.STYLEABLE_OBJECTS)) {
                styleableObject = null;
            }
        }
		var stylesDef = this.getStyles();
        var styleCnt = stylesDef.length;
		if (styleableObject) {
            for (s = 0; s < styleCnt; s++) {
                var styleName = stylesDef[s].cssName;
                if (com.hasCSS(styleableObject, styleName)) {
                    actualStyles.push({
                        "className": styleName
                    });
                }
            }
        } else {
            var checkCnt = styles.length;
            for (var c = 0; c < checkCnt; c++) {
                var styleToProcess = styles[c];
                var currentStyles = styleToProcess.className.split(" ");
                for(var j=0; j<currentStyles.length; j++) {
                    for (s = 0; s < styleCnt; s++) {
                        if (stylesDef[s].cssName == currentStyles[j]) {
                            actualStyles.push(currentStyles[j]);
                            break;
                        }
                    }
				}
            }
        }
        this.stylesUI.selectStyles(actualStyles, selDef);
    }

});


// register plugin
CUI.rte.plugins.PluginRegistry.register("fontcolor",
        SC.rte.plugins.Fontcolor);