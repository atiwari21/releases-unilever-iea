/*************************************************************************
*
* RTE Plugins
* ___________________
*
* Extend the toolkit implementation for custom toolbar builder and dialog manager
*
* @author Mohit K. Bansal
*
*
**************************************************************************/
ACT.rte.ui.ToolkitImpl = new Class({

    toString: "ACTToolkitImpl",
    
    extend: CUI.rte.ui.cui.ToolkitImpl,

    createToolbarBuilder: function() {
        if(ACT.rte.DEBUG.TOOLKITIMPL || ACT.rte.DEBUG.ALL) {
        	console.log("init ACT.rte.ui.ToolkitImpl");
        }
        return new ACT.rte.ui.CuiToolbarBuilder();
    }
});

CUI.rte.ui.ToolkitRegistry.register("cui", ACT.rte.ui.ToolkitImpl);