/*************************************************************************
*
* RTE Plugins
* ___________________
*
* Extend toolbar builder to register different commands
*
* @author Mohit K. Bansal
*
**************************************************************************/
ACT.rte.ui.CuiToolbarBuilder = new Class({
    
    toString: "ACTCuiToolbarBuilder",
    
    extend: CUI.rte.ui.cui.CuiToolbarBuilder,

    _getUISettings: function (options) {
        var uiSettings = this.superClass._getUISettings(options);

        if(ACT.rte.DEBUG.TOOLBARBUILDER || ACT.rte.DEBUG.ALL) {
        	console.log("ACTCuiToolbarBuilder - _getUISettings()");
        }

        //inline toolbar
        var items = uiSettings["inline"]["popovers"]["format"].items;


		//add plugins to fullscreen toolbar
        var toolbar = uiSettings["fullscreen"]["toolbar"];
		var popovers = uiSettings["fullscreen"]["popovers"];

         // Font Color
        if (toolbar.indexOf(ACT.rte.FEATURE.FONT_COLOR.ID) == -1) {
        	toolbar.splice(3, 0, ACT.rte.FEATURE.FONT_COLOR.ID);
        }
		if (!popovers.hasOwnProperty(ACT.rte.FEATURE.FONT_COLOR.NAME)) {
            popovers.fontcolor = {
            	"ref": ACT.rte.FEATURE.FONT_COLOR.NAME,
                "items": ACT.rte.FEATURE.FONT_COLOR.POPID
            };
        }
        if (!this._getClassesForCommand(ACT.rte.FEATURE.FONT_COLOR.ID)) {
        	this.registerAdditionalClasses(ACT.rte.FEATURE.FONT_COLOR.ID, ACT.rte.FEATURE.FONT_COLOR.ICON);
        }

        // Font Styles
        if (toolbar.indexOf(ACT.rte.FEATURE.FONT_STYLE.ID) == -1) {
        	toolbar.splice(3, 0, ACT.rte.FEATURE.FONT_STYLE.ID);
        }
		if (!popovers.hasOwnProperty(ACT.rte.FEATURE.FONT_STYLE.NAME)) {
            popovers.fontstyles = {
            	"ref": ACT.rte.FEATURE.FONT_STYLE.NAME,
                "items": ACT.rte.FEATURE.FONT_STYLE.POPID
            };
        }
        if (!this._getClassesForCommand(ACT.rte.FEATURE.FONT_STYLE.ID)) {
        	this.registerAdditionalClasses(ACT.rte.FEATURE.FONT_STYLE.ID, ACT.rte.FEATURE.FONT_STYLE.ICON);
        }
        
        return uiSettings;
    },

    // Styles dropdown builder
    createACTStyleSelector: function(id, plugin, tooltip, styles) {
        if(ACT.rte.DEBUG.TOOLBARBUILDER || ACT.rte.DEBUG.ALL) {
        	console.log("ACTCuiToolbarBuilder - createACTStyleSelector()");
        }
        return new ACT.rte.ui.StyleSelectorImpl(id, plugin, false, tooltip, false,
                                                    undefined, styles);
    }
});