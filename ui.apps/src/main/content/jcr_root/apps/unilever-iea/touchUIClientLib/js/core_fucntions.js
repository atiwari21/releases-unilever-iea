//Core functions related to the touch ui dialogs
function fillSelectBox(selectBoxName, data, selectedVal) {
    var selectBox = new CUI.Select({
        element: $("[name='" + selectBoxName + "']").closest(".coral-Select")
    });

    if (selectBox._selectList) {
        selectBox._selectList.children().remove();
    }
    var optionList = '';
    _.each(data, function(valueObj, index) {
        var newObj = {
            "value": valueObj.value,
            "display": valueObj.text
        };
        selectBox.addOption(newObj);
        optionList = optionList + '<option value="' + valueObj.value + '">' + valueObj.text + '</option>';
    });


    var selectedValue = '';
    if (typeof selectedVal !== "undefined" && !_.isEmpty(selectedVal)) {
        selectedValue = selectedVal;
        selectBox.setValue(selectedVal);
    } else {
        var value = $("[name='" + selectBoxName + "']").parent().attr('data-value');
        if (typeof value !== "undefined" && !_.isEmpty(value)) {
            selectBox.setValue(value);
            selectedValue = value;
        }

    }

    $("select[name='" + selectBoxName + "']").find('option').remove().end().append(optionList).val(selectedValue);

}

function loadSelectBox(FIELD_NAME) {
    var eleArr = $("[name='" + FIELD_NAME + "']").closest(".coral-Select");
    if (eleArr != null && eleArr.length > 0) {
        // get the selectBox widget
        var selectBox = new CUI.Select({
            element: eleArr
        });

        if (selectBox._selectList) {
            selectBox._selectList.children().remove();
        }
        // attaching change event function, which is provided in selectBox
        // node with value of propertyName=onSelectFunc
        var funcBody = selectBox.$element.attr("data-onSelectFunc");
        if (typeof funcBody !== "undefined" && funcBody.length != 0) {
            var func = eval("(" + funcBody + ")");
            if ($.isFunction(func)) {
                selectBox._selectList.on('selected.select', func);
            }
        }
        // end
        var $form = selectBox.$element.closest("form");
        var formName = $form.find("div.cq-dialog-content").attr('data-formName');            
        var url = $form.attr("action");
        
        // getting the selector from selectBox element for ajax call.
        var selector = selectBox.$element.data("selector");
        if (typeof selector !== "undefined" && selector.length != 0) {
            url = url + "." + selector;
        } else if(FIELD_NAME == './featurephoneViewType'){            
            url = url + ".displayFeatureOptions.json";
        } else if(FIELD_NAME == './socialChannels'){            
            url = url + ".socialchannels.json?cmp=" + formName;
        } else if(FIELD_NAME == './socialChannellsImg'){			            
            url = url + ".socialchannels.json?cmp=" + formName;
        } else if(FIELD_NAME == './socialChannellsVid'){			            
            url = url + ".socialchannels.json?cmp=" + formName;
        } else if(FIELD_NAME == './socialChannelForAcntId'){			            
            url = url + ".socialchannels.json?cmp=" + formName;
        } else if(FIELD_NAME == './configurejson'){			            
            url = url + ".configurejson.analyticContext.json";
        } else if(FIELD_NAME == './ratingReviews'){			            
            url = url + ".ratingReviewsServiceProviders.json";
        } else {
            url = url + ".displayOptions.json";
        }
        $.getJSON(url).done(function(data) {
            var selectBoxData = data;
            $.getJSON($form.attr("action") + ".json").done(function(data) {
                if (_.isEmpty(data)) {
                    return;
                }
                // strip ./
                var fName = FIELD_NAME.substring(2);
                fillSelectBox(FIELD_NAME, selectBoxData, data[fName]);
            })
        });
    }
}


function loadCheckBoxes(FIELD_NAME) {
    var fieldArr = $("[name='" + FIELD_NAME + "']");
    if (fieldArr != null && fieldArr.length > 0) {
        var cbLabelField = $("[name='" + FIELD_NAME + "']").closest(".coral-Checkbox");
        var $form = $("[name='" + FIELD_NAME + "']").closest("form");
        var formName = $form.find("div.cq-dialog-content").attr('data-formName');            
        var url = $form.attr("action");
        var selector = $(fieldArr).attr('data-selector');
        var fieldLabel = $(fieldArr).attr('data-fieldlabel');
    	if (CQ && CQ.I18n) {
    		fieldLabel = CQ.I18n.getMessage(fieldLabel);
	}
        if (typeof selector !== "undefined" && selector.length != 0) {
            url = url + "." + selector;
        } else if(FIELD_NAME == './socialChannels'){
            url = url + ".socialchannels.json?cmp=" + formName;
        } else if(FIELD_NAME == './socialChannellsImg'){			            
            url = url + ".socialchannels.json?cmp=" + formName;
        } else if(FIELD_NAME == './socialChannellsVid'){			            
            url = url + ".socialchannels.json?cmp=" + formName;
        } else if(FIELD_NAME == './socialChannelForAcntId'){			            
            url = url + ".socialchannels.json?cmp=" + formName;
        } else if(FIELD_NAME == './addThisOptions'){
            url = url + ".addThisOptions.json";
        } else if(FIELD_NAME == './dietaryAttributes'){
            url = url + ".optionPopulator.json?cmp=" + formName+"&ct=recipeDietaryAttributesList&i18=true";
        } else if(FIELD_NAME == './attributes'){
            url = url + ".optionPopulator.json?cmp=" + formName+"&ct=recipeAttributesList&i18=true";
        } else if(FIELD_NAME == './sortingOptions'){
            url = url + ".optionPopulator.json?cmp=" + formName+"&ct=recipeListingSortingOptions&i18=true";
        } else if(FIELD_NAME == './multipleProductView'){
            url = url + ".multipleProductViewSupported.json";
        } else if(FIELD_NAME == './soundCloudAttributes'){
            url = url + ".soundCloud.json?ct=audioPlayerSoundCloudAttributes&i18=true";
        } else {
            url = url + ".displayOptions.json";
        }
        var parent = cbLabelField.parent();
        parent.removeClass('coral-Form-fieldwrapper--singleline').addClass('coral-Form-fieldwrapper--vertical');
        var cbLabelFieldHTML = parent.html();
        var finalHTML = '';
        var fieldLabelHTML = '<label class="coral-Form-fieldlabel">' + fieldLabel + '</label>';
        finalHTML = finalHTML + fieldLabelHTML;
        parent.html('');
        $.getJSON(url).done(function(data) {
            var selectBoxData = data;            
            _.each(selectBoxData, function(valueObj, index) {				
                var temp = cbLabelFieldHTML.replace('DISPLAY_TEXT_PLACE_HOLDER', valueObj.text);
                temp = temp.replace('VALUE_PLCAE_HOLDER', valueObj.value);
				temp = temp.replace('data-click', "onclick");
                finalHTML = finalHTML + temp;                
            });
            parent.html(finalHTML);
        });
        $.ajax({
            url: $form.attr("action") + ".json",
            async: false,
            dataType: "json"
        }).done(function(data) {            
            if (_.isEmpty(data)) {
                return;
            }
            window.setTimeout(function() {
                var fName = FIELD_NAME.substring(2);
                var valArr = data[fName];
                if(valArr != null){
                if (valArr.constructor == Array) {
                    _.each(valArr, function(value, index) {
                        $("input[type='checkbox'][name='" + FIELD_NAME + "'][value='" + value + "']").click();
						$("input[type='checkbox'][name='" + FIELD_NAME + "']").attr("onclick");
                    });
                } else {
                    $("input[type='checkbox'][name='" + FIELD_NAME + "'][value='" + valArr.toString() + "']").click();
					$("input[type='checkbox'][name='" + FIELD_NAME + "']").attr("onclick");
                }
                }
            }, 100);
        });
    }
}

function attachOnSelectEventToSelectBox(selectBoxs, selectBoxSelector) {
    $($(selectBoxs)).each(function() {
		var selectBox=$(this);
        var selectorVal = $(selectBox).attr(selectBoxSelector);
		var eventAttached=$(selectBox).attr("eventAttached");
		if(eventAttached!="selected"){
			var selectObj = $("[" + selectBoxSelector + "='" + selectorVal + "']");
			if (selectorVal && $("[" + selectBoxSelector + "='" + selectorVal + "']").parent().attr("data-onselectfunc")) {
				var funcBody = $("[" + selectBoxSelector + "='" + selectorVal + "']").parent().attr("data-onselectfunc");
				if (typeof funcBody !== "undefined" && funcBody.length != 0) {
					var func = eval("(" + funcBody + ")");
					if ($.isFunction(func)) {
						$("[" + selectBoxSelector + "='" + selectorVal + "']").closest(".coral-Select").on('selected', function(event) {
							$(selectBox).attr("eventAttached","selected");
							func(event);
						});
					}
				}
			}
		}
    });
}



function attachOnLoadEventToSelectBox(selectObj, selectBoxVal) {
    var dialogElement = $('.cq-dialog-content');
    if (dialogElement) {
        var selectName = dialogElement.attr('data-selectName');
        if (selectObj != null && selectObj != undefined) {
            selectName = $(selectObj).attr('name');
        }
        var onloadFunc = dialogElement.attr('data-onloadfunc');
        var parentMultiField = $("[name='" + selectName + "']").closest(".coral-Multifield-list");
        if (typeof onloadFunc !== "undefined" && onloadFunc.length != 0) {
            var onloadFunction = eval("(" + onloadFunc + ")");
            if ($.isFunction(onloadFunction)) {
                if (selectObj != null && selectObj != undefined) {
                    onloadFunction(selectObj, selectBoxVal);
                } else {
                    if (selectName) {
                        var selectEle = $("[name='" + selectName + "']");
                        if (selectEle != undefined) {
                            if (selectEle != null && selectEle != undefined) {
                                if (selectEle.length > 1) {
                                    selectEle = selectEle[0];
                                }
                            }
                            if (selectBoxVal == null || selectBoxVal == undefined) {
                                var $form = $(selectEle).closest(".coral-Select").parent().closest("form");
                                $.getJSON($form.attr("action") + ".json").done(function(data) {
                                    if (_.isEmpty(data)) {
                                        return;
                                    }
                                    var fName = selectName.substring(2);
                                    selectBoxVal = data[fName];
                                    if (selectBoxVal) {
                                        onloadFunction(selectBoxVal);
                                    } else {
                                        onloadFunction();
                                    }

                                });
                            } else {
                                onloadFunction();
                            }
                        }
                    } else {
                        onloadFunction();
                    }
                }
            }

        }

    }
}

function attachOnClickEventToCheckBox() {
    $('input[type=checkbox]').each(function() {
        var funcBody = $(this).attr("data-onClick");
        if (typeof funcBody !== "undefined" && funcBody.length != 0) {
            var func = eval("(" + funcBody + ")");
            if ($.isFunction(func)) {
                $(this).on('click', function() {
                    func();
                });
            }
        }
    });
}

function attachOnClickEventToRadioButton() {
    $('input[type=radio]').each(function() {
        var funcBody = $(this).attr("data-onSelectRadio");
        if (typeof funcBody !== "undefined" && funcBody.length != 0) {
            var func = eval("(" + funcBody + ")");
            if ($.isFunction(func)) {
                $(this).on('change', function() {
                    func();
                });
            }
        }
    });
}


function attachOnLoadEventToCheckBox() {
    var dialogElement = $('.cq-dialog-content');
    if (dialogElement) {
        var checkBoxName = dialogElement.attr('data-checkboxname');
        if (checkBoxName) {
            var onloadFunc = dialogElement.attr('data-onloadfunc');
            if (typeof onloadFunc !== "undefined" && onloadFunc.length != 0) {
                var onloadFunction = eval("(" + onloadFunc + ")");
                if ($.isFunction(onloadFunction)) {
                    onloadFunction();
                }
            }
        }
    }
}

function loadImage(fileWidgetName) {
    //custom function to load the image in the dialog after reopening the saved dialog,
    // the requirement of this is  because there is a hardcoding of resourcetype in the fileuploadfield js file(if (data["sling:resourceType"] === "foundation/components/image") )
    //path of js ="/libs/cq/gui/components/authoring/dialog/fileuploadfield/clientlibs/fileuploadfield/js/fileuploadfield.js" line no=79
    var fileWidget = $('[name="' + fileWidgetName + '"]');

    var _thumbnailImgClass = "cq-FileUpload-thumbnail-img";
    if (fileWidget != null && fileWidget != undefined) {
        var form = $(fileWidget).closest("form");
        var $thumbnail = $(form).find("." + _thumbnailImgClass);
        // display image
        $.ajax({
            url: $(form).attr("action") + ".html",
            cache: false
        }).done(function(data) {
            var thumbnailDom = $(data).find("img");

            if (!thumbnailDom.hasClass("cq-placeholder")) {
                //self.widget.$element.addClass(_isFilledClass);
                $thumbnail.html('');
                $thumbnail.append(thumbnailDom);
            }
        });

    }

}

function attachDialogOnSubmitEvent() {
    var dialogElement = $('.cq-dialog-content');
    if (dialogElement) {
        var onSubmit = dialogElement.attr('data-onsubmit');
        if (typeof onSubmit !== "undefined" && onSubmit.length != 0) {
            var onSubmitFunction = eval("(" + onSubmit + ")");
            if ($.isFunction(onSubmitFunction)) {
                onSubmitFunction();
            }
        }
    }
}

function replaceAll(input, replace, replacewith) {
    return input.replace(new RegExp(replace, 'g'), replacewith);
}

function isValidHiddenFieldToGetIntoContent(fieldName){
if(fieldName == "randomNumber" || fieldName == "responseId"){
	return true;
}else{
	return false;
}
}

function getRichTextValueWithoutBlankPtags(rteValue){
	if(rteValue){
		if(rteValue.indexOf("<p>")>-1){
            var newRichText = replaceAll(rteValue,'<p>&nbsp;</p>','');
            var removedNewLineRichText = replaceAll(newRichText, '<p> </p>','');
                return removedNewLineRichText;
            }else{
            return rteValue;
            }
		}else{
            return rteValue;
        }
}

//Scroll up to the top of the dialog whenever the tab is switched.
$(document).on("click",".coral-TabPanel-tab",function(){
	  		$(".coral-TabPanel-content").scrollTop( 0 );
		});

//Refresh Page on change of Authoring modes
(function($, channel) {
    'use strict';
    $(function() {
        channel.on('cq-layer-activated', function(event) {

            var pageUrl = window.location.href;
            var ANNOTATE_LAYER = 'Annotate';
            if (event.layer === 'Edit') {
                $("iframe").contents().find(".cq-heading").show()
            } else {
                $("iframe").contents().find(".cq-heading").hide()
            }
        });
    });
})(Granite.$, jQuery(document));