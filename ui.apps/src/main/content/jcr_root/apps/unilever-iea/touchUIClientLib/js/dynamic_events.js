//Attaching events to select and check boxes of touch ui dialogs also dynamically populates the checkbox and select
(function($, $document) {
    "use strict";

    var FIELD_NAME1 = "./viewType";
    var FIELD_NAME2 = "./videoTypePanel";
    var FIELD_NAME3 = "./contentType_children";
    var FIELD_NAME4 = "./viewFormat";
    var FIELD_NAME5 = "./tagsMatch";
    var FIELD_NAME6 = "./contentType_tags";
    var FIELD_NAME7 = "./dateFormat";
    var FIELD_NAME8 = "./pattern";
    var FIELD_NAME9 = "./displayFilter";
    var FIELD_NAME10 = "./addThisOptions";
    var FIELD_NAME11 = "./actionType";
    var FIELD_NAME12 = "./featurephoneViewType";
    var FIELD_NAME13 = "./socialChannels";
    var FIELD_NAME14 = "./formid";
    var FIELD_NAME19 = "./languageCode";
    var FIELD_NAME18 = "./emailTemplateType";
    var FIELD_NAME20 = "./configurejson";
    var FIELD_NAME21 = "./contentType";
    var FIELD_NAME22 = "./formIdentifier";
    var FIELD_NAME23 = "./dietaryAttributes";
    var FIELD_NAME24 = "./attributes";
    var FIELD_NAME25 = "./sortingOptions";
	var FIELD_NAME26 = "./ratingReviews";
	var FIELD_NAME27 = "./multipleProductView";
	var FIELD_NAME28 = "./soundCloudAttributes";
    $document.on("dialog-ready", function() {
        function barTop() {
            $(".coral-TabPanel-content").scrollTop(0);
        }
        setTimeout(barTop, 1000);
		        if ($(".cq-dialog-floating")) {
			if ($(".cq-dialog-floating").attr(
					"data-foundation-form-ajax") && $(".cq-dialog-floating").attr(
							"data-foundation-form-ajax") == "true"
					&& $(".cq-dialog-floating").attr("action")
					&& $(".cq-dialog-floating").attr("action").indexOf(
							"_jcr_content") >= 0
					&& $(".cq-dialog-floating").attr("action").indexOf(
							"/content") >= 0
					&& $(".cq-dialog-floating").attr(
					"data-cq-dialog-pageeditor")
					&& $(".cq-dialog-floating").attr(
							"data-cq-dialog-pageeditor").indexOf(
							"/editor.html/content") >= 0) {
				//$(".cq-dialog-floating").css("top", "");
				$(".coral-TabPanel-content").css("height", "500px");
			}
		}
    });
    $document.ready(function() {
        $('.coral-RichText-editable').each(function(index) {
            var html = $(this).text();
            $(this).bind("DOMSubtreeModified", function() {
                var duplicateVal = $(this).html();
                var updatedRte = replaceAll(duplicateVal, '<p><br _rte_temp_br="brEOB"></p>', '');
                $(this).siblings('[type="hidden"].coral-Textfield').val(updatedRte);
            });
            $("button[type='submit'][form='propertiesform'][data-foundation-mode-group='cq-siteadmin-admin-properties']").on('click', function(e) {
                window.setTimeout(function() {
                    window.location.href = window.location.href;
                }, 1000);
            });
        });
        if($('.cq-commerce-catalogs-rolloutchanges-wizard') && $('.cq-commerce-catalogs-rolloutchanges-wizard').length>0){
        	var currentUrl = window.location.href;
        	var encodedUrlString = currentUrl.split("?");
        	var encodedUrlFirstString = encodedUrlString[0];
        	if(encodedUrlFirstString.indexOf("%2F")>=0 && currentUrl.indexOf("_charset")>=0 && currentUrl.indexOf("&item")>=0){
        		var redirectUrl = replaceAll(encodedUrlFirstString,'%2F','/')+'?'+encodedUrlString[1];
        		window.location.href = redirectUrl;
        	}
        }
    });
    var pageForm = $(".cq-dialog-page");
    if (pageForm != null && pageForm != undefined && pageForm.length > 0) {
        $(document).ready(function() {
            loadAll();

        });
    } else {
        $document.on("dialog-ready", function() {
            loadAll();
            $('#introCopy').keyup(function() {
                var the_introCopy_len;
                var the_introCopy;
                the_introCopy = $('#introCopy').val();
                the_introCopy_len = $('#introCopy').val().length;
                the_introCopy_len = 200 - the_introCopy_len;
                if (the_introCopy_len < 0) {
                    $('#introCopy').val($('#introCopy').val().slice(0, 200));
                }
            });
            $(document).on("input", $('.coral-RichText-editable').children('p'), function () {
            	$('.coral-RichText-editable').children('p').css("max-width","800px");
            });
        });
    }

    function loadAll() {
        loadSelectBox(FIELD_NAME1);
        loadSelectBox(FIELD_NAME2);
        loadCheckBoxes(FIELD_NAME3);
        loadSelectBox(FIELD_NAME4);
        loadSelectBox(FIELD_NAME5);
        loadCheckBoxes(FIELD_NAME6);
        loadSelectBox(FIELD_NAME7);
        loadSelectBox(FIELD_NAME8);
        loadCheckBoxes(FIELD_NAME9);
        loadCheckBoxes(FIELD_NAME10);
        loadSelectBox(FIELD_NAME11);
        loadSelectBox(FIELD_NAME12);
        loadSelectBox(FIELD_NAME13);
        loadSelectBox(FIELD_NAME19);
        loadCheckBoxes(FIELD_NAME13);
        generatePathBasedId(FIELD_NAME14);
        loadSelectBox(FIELD_NAME18);
        loadSelectBox(FIELD_NAME20);
        generatePathBasedId(FIELD_NAME22);
        loadCheckBoxes(FIELD_NAME23);
        loadCheckBoxes(FIELD_NAME24);
        loadCheckBoxes(FIELD_NAME25);
		loadSelectBox(FIELD_NAME26);
		loadCheckBoxes(FIELD_NAME27);
		loadCheckBoxes(FIELD_NAME28);
		//read time onload functionality
		if($('#overwriteReadTime') && document.getElementById('overwriteReadTime')){
			if (document.getElementById('overwriteReadTime').checked) {
				$('input[type="number"][name="./pageReadTime"]').prop('disabled',false);
		        $('input[type="number"][name="./pageReadTime"]').attr('readonly',false);
		        $('#pageReadTime button').prop('disabled', false);
			}else{
				$('input[type="number"][name="./pageReadTime"]').prop('disabled',true);
		        $('input[type="number"][name="./pageReadTime"]').attr('readonly',true);
		        $('#pageReadTime button').prop('disabled', true);
		    }
		        }
		//negate conditions added for diagnostic tool response component
        $('[data-init="multifield"][data-name!="questionmultifield"][id!="personalisedRulesMulti"]').on("click", ".coral-Multifield-add", function(e) {
			attachEvents($(this));
			var childMultifield = $(this).parent('[data-init="multifield"]').find('[data-init="multifield"]');
			$(childMultifield).each(function(index){
				$(this).on("click",".coral-Multifield-add", function(e) {
				
					attachEvents($(this));
				
				});
            
			});
			function barTop() {
				$(".coral-TabPanel-content").scrollTop(0);
			}
		});
        window.setTimeout(function() {
            var multiFieldWidget = $('[data-init="multifield"]');
            if (multiFieldWidget != null && multiFieldWidget != undefined) {
                var liArr = $('[data-init="multifield"]').children(".coral-Multifield-list").children();
                $($(liArr)).each(function(index) {
                    addOnLoadEventToMultiFieldCheckBox(this, index + 1);
                });
            }
            var multiFieldTag = $('.coral-Form-fieldset').children();
            var divNextToMultiField,fieldWrapperDiv,tagDiv;
            if(multiFieldTag){
                divNextToMultiField = multiFieldTag.children();
                if(divNextToMultiField){
                fieldWrapperDiv = divNextToMultiField.children();
                if(fieldWrapperDiv){
               fieldWrapperDiv.children('.js-coral-Autocomplete-tagList').find('[type="button"].coral-TagList-tag-removeButton').each(function(index) {
                var removeFunction = 'function(){removeCrossButton('+index+')'+'}';
                    $(this).attr('data-onRemoveClick',removeFunction);
                    attachRemoveCrossButton();
            });
                }
                }
            }
        }, 1000);
        
        addOnSelectEventToNonMultiFieldSelectBox();
        attachOnLoadEventToSelectBox();
        attachOnClickEventToCheckBox();
        attachOnLoadEventToCheckBox();
        attachOnClickEventToRadioButton();
        window.setTimeout(function() {
        attachDialogOnSubmitEvent();
             }, 100);
        var multiFieldTag = $('.coral-Form-fieldset').children();
        var divNextToMultiField,fieldWrapperDiv,tagDiv;
        if(multiFieldTag){
            divNextToMultiField = multiFieldTag.children();
            if(divNextToMultiField){
            fieldWrapperDiv = divNextToMultiField.children();
            if(fieldWrapperDiv){
            	fieldWrapperDiv.children('.js-coral-Autocomplete-tagList').each(function(index,autoCompleteElement) {
                    $(autoCompleteElement).attr('data-onClassChange','function(){tagAutoComplete('+index+')}');
                });
            }
            
            }
        }
        attachOnClassChange();
    }
	
	function attachEvents(multiField){
		
				var liArr = $(multiField).parent('div[data-init="multifield"]').children(".coral-Multifield-list").children();
				var lastLI = $(liArr).last();
				addOnSelectEventToMultiFieldSelectBox(lastLI, liArr.length);
				addOnLoadEventToMultiFieldCheckBox(lastLI, liArr.length);
		
	}

    function generatePathBasedId(fieldId) {
        var formEl = $("input[name='" + fieldId + "']").closest('form');
        if (formEl && !$("input[name='" + fieldId + "']").val()) {
            var replaceSlash = $(formEl).attr('action');
            if (typeof replaceSlash !== "undefined") {
                replaceSlash = replaceSlash.replace('/_', '_');
                $("input[name='" + fieldId + "']").val(replaceSlash.replace(/[/:.]/g, "_")).trigger('change');
            }
        }
        $("input[name='" + fieldId + "']").attr('readonly', true);
    }

    function addOnSelectEventToNonMultiFieldSelectBox() {
        $('select').each(function() {		
            var dataId = $(this).attr('data-id');
            if (dataId != undefined && dataId != null) {
                return true;
            }
            attachOnSelectEventToSelectBox($(this), 'name');
        });
    }

    function addOnSelectEventToMultiFieldSelectBox(li, index) {
        var selectBoxSelector = "data-id";
        $($(li).find('select')).each(function() {
            var selectName = $(this).attr('name');
            $(this).attr(selectBoxSelector, selectName + index);
            var selectorVal = $(this).attr(selectBoxSelector);
            var selectObj = $(li).find("[" + selectBoxSelector + "='" + selectorVal + "']");
            attachOnLoadEventToSelectBox($(selectObj));
        });
        var selectBoxs = $(li).find('select');
        attachOnSelectEventToSelectBox(selectBoxs, selectBoxSelector);
    }

    function addOnLoadEventToMultiFieldCheckBox(li, index) {
        var checkBoxSelector = "data-id";
        $($(li).find("input[type='checkbox']")).each(function() {
            var checkBoxName = $(this).attr('name');
            $(this).attr(checkBoxSelector, checkBoxName + index);
            var selectorVal = $(this).attr(checkBoxSelector);
            var checkboxObj = $("[" + checkBoxSelector + "='" + selectorVal + "']");
            var funcBody = $(checkboxObj).attr("data-onClick");
            if (typeof funcBody !== "undefined" && funcBody.length != 0) {
                var func = eval("(" + funcBody + ")");
                if ($.isFunction(func)) {
                    $(this).on('click', function() {
                        func();
                    });
                    func();
                }
            }

        });
    }

    function attachRemoveCrossButton(){
        $('[type="button"].coral-TagList-tag-removeButton').each(function() {
     var funcBody = $(this).attr("data-onRemoveClick");
     if (typeof funcBody !== "undefined" && funcBody.length != 0) {
         var func = eval("(" + funcBody + ")");
         if ($.isFunction(func)) {
             $(this).on('click', function() {
                 func();
             });
         }
     }
 });
 }
    
    function attachOnClassChange(){
        $('.js-coral-Autocomplete-tagList').each(function() {
    var funcBody = $(this).attr("data-onClassChange");
    if (typeof funcBody !== "undefined" && funcBody.length != 0) {
        var func = eval("(" + funcBody + ")");
        if ($.isFunction(func)) {
        	$(this).resize(function(e) {
                func();
            });
        }
    }
});
}
})($, $(document));