function diagnosticToolOnLoad(){
    $("[aria-controls='includeIntroPanelTab']").hide();
	if(document.getElementById('includeIntroPanel').checked){
		$("[aria-controls='includeIntroPanelTab']").show();
	}
	showRegistrationFields();
	showSignUpFields();
	showEmailFields();
    showSocialSharingFields();
	addButton('result-section-multifield','confirm-button','js-unilever-save-resultsection','diagnosticTool.confirmChanges','diagnosticTool.confirmChanges.desc');
	var eventAttached=$('.js-unilever-save-resultsection').attr("eventAttached");
	if(eventAttached!="click"){
		$('.js-unilever-save-resultsection').on('click', function() {
			var $form = $(this).closest("form");
			var formName = $form.find("div.cq-dialog-content").attr('data-formName');
			var resultSectionPathArr = $("input[name='./resultSectionPath']");
			var query = "?";
			for (var i = 0; i < resultSectionPathArr.length; ++i) {
				var val = $(resultSectionPathArr[i]).val();
				query = query + "path=" + val + "&";
			}
			var jsonData = [];
			var url = $form.attr("action");
			$.ajax({
				url: $form.attr("action") + ".getResultSectionData" + query,
				method: "GET",
				async: false

			}).done(function(data) {
				jsonData = JSON.parse(data);
				fillSelectBox('./personalisedTextFrom',jsonData,'');
			});
			$(this).attr("eventAttached","click");
			$(window).adaptTo("foundation-ui").alert("Information","Changes Saved");
		});
	}
	addToolTipToMultiBuyConfig('multibuyConfigurationSection');
}

function addToolTipToMultiBuyConfig(multiBuyConfigId){
var multiBuyConfigDescription = $("#"+multiBuyConfigId);
if(multiBuyConfigDescription && $("#"+multiBuyConfigId).children('h3') && $("#"+multiBuyConfigId).attr("data-fielddescription").length>0 && $("#"+multiBuyConfigId).children('h3').children('span').length<1){
	var jsonDescData = [];
$.ajax({
	url: "/bin/multibuy/config/tooltip?fieldDescription="+$("#"+multiBuyConfigId).attr("data-fielddescription"),
	method: "GET",
	async: false

}).done(function(data) {
	jsonDescData = JSON.parse(data);
	
});
var fieldDescriptionTool = "data-quicktip-content="+"'"+jsonDescData[0].value+"'";
var fieldDescriptionAria = " aria-label="+"'"+jsonDescData[0].value+"'";
var toolTipClass = '<'
	                +'span class="coral-Form-fieldinfo coral-Icon coral-Icon--infoCircle coral-Icon--sizeS" data-init="quicktip" data-quicktip-type="info" data-quicktip-arrow="left" '
                    +fieldDescriptionTool+fieldDescriptionAria
                    +' tabindex="0"'
                    +'>'
                    +'</span>';
$(toolTipClass).appendTo($($("#"+multiBuyConfigId).children('h3')));
}
}

function diagnosticToolOnSubmit(){
	$(document).on("click", ".cq-dialog-submit", function(event) {
        var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');

        if (formName && formName == 'diagnosticTool') {
        	$('input[type="text"][name="./resultSectionPath"]').each(function() {
				var urlVal = $(this).val();
				if (urlVal.length == 0) {
					$(window).adaptTo("foundation-ui").alert("Required", "Please Configure result section");
					event.preventDefault();
					return false;
				} else {
					return true;
				}
			});
        	if($(".coral-Multifield-list li [name='./resultSectionPath']").length==0){
                $(window).adaptTo("foundation-ui").alert("Required","Please configure at least one result section");
                event.preventDefault();
                return false;
          	}
        }
	});
}

function addButton(multifieldid, buttonid,jsClass,fieldLabel,fieldDescription) {
    var button='<button type="button" id ="'+buttonid+'" class="coral-Button  coral-Multifield-add '+jsClass+'">'+Granite.I18n.get(fieldLabel)+'</button><span data-init="quicktip" data-quicktip-content="'+Granite.I18n.get(fieldDescription)+'"  data-quicktip-arrow="left" class = "coral-Icon coral-Icon--infoCircle coral-Icon--sizeS" style="position:absolute;"></span>';
    

    $('#'+multifieldid).children(".coral-Multifield-add").after(button);
}

function showSignUpFields(){
	//sign up section
	var enableSignUp = $('[name="./enableSignUp"]');
	var overrideConfigForSignUp = $('[name="./overrideConfigForSignUp"]');
	$(overrideConfigForSignUp).parent().hide();
	$("#includeInDisplaySignUp").hide();
    $("#includeInDisplaySignUpEnd").hide();
	if($(enableSignUp).is(':checked')){
		$(overrideConfigForSignUp).parent().show();
		$("#includeInDisplaySignUpEnd").show();
		if($(overrideConfigForSignUp).is(':checked')){
			$("#includeInDisplaySignUp").show();
		}
	}
}

function showEmailFields(){
	//email section
	var checkbox=$('[name="./enableEmail"]');
	var overrideConfigForEmail = $('[name="./overrideConfigForEmail"]');
	$(overrideConfigForEmail).parent().hide();
	$("#includeInDisplayEmail").hide();
	if($(checkbox).is(':checked')){
		$(overrideConfigForEmail).parent().show();
		if($(overrideConfigForEmail).is(':checked')){
			$("#includeInDisplayEmail").show();
		}
	}
}

function showRegistrationFields(){

	//reistration section
	var enableRegistration = $('[name="./enableRegistration"]');
	var overrideConfigRegistration = $('[name="./overrideConfigForRegistration"]');
	$(overrideConfigRegistration).parent().hide();
	$("#includeInDisplayRegistration").hide();
    $("#includeInDisplayRegistrationEnd").hide();
	if($(enableRegistration).is(':checked')){
		$(overrideConfigRegistration).parent().show();
		$("#includeInDisplayRegistrationEnd").show();
		if($(overrideConfigRegistration).is(':checked')){
			$("#includeInDisplayRegistration").show();
		}
	}
}

function showSocialSharingFields(){
	var checkbox=$('[name="./enableSocialSharing"]');
	var socialOptionsPrefix = $('[name="./socialOptionsPrefix"]').closest(".coral-Form-fieldwrapper");
	var sharingPrefix = $('[name="./sharingPrefix"]').closest(".coral-Form-fieldwrapper");
	var personalisedTextFrom = $('[name="./personalisedTextFrom"]').closest(".coral-Form-fieldwrapper");
	var sharingSuffix = $('[name="./sharingSuffix"]').closest(".coral-Form-fieldwrapper");
	var sharingImage = $('[name="./sharingImage"]').closest(".coral-Form-fieldwrapper");
	var sharingTitle = $('[name="./sharingTitle"]').closest(".coral-Form-fieldwrapper");
	if($(checkbox).is(':checked')){
		socialOptionsPrefix.show();
		sharingPrefix.show();
		personalisedTextFrom.show();
		sharingSuffix.show();
		sharingImage.show();
		sharingTitle.show();
	}
	else{
		socialOptionsPrefix.hide();
		sharingPrefix.hide();
		personalisedTextFrom.hide();
		sharingSuffix.hide();
		sharingImage.hide();
		sharingTitle.hide();
	}
}
function diagnosticToolResultSectionOnLoad() {
	var $allButtons = $('button#confirm-button');
	_.each($allButtons, function(button, index) {
	$(button).remove();
	    });
	var personalisedTextRuleToolTip = $('.coral-TabPanel-content').children('span').attr('data-init','quicktip');
    if($(personalisedTextRuleToolTip)){
      $(personalisedTextRuleToolTip).insertBefore('#personalisedRulesMulti');
    }
    $("[aria-controls='personalisedContentTab']").hide();
		$("[aria-controls='productsTab']").hide();
		$("[aria-controls='articlesTab']").hide();
    if(document.getElementById('personalisedTextFlag').checked){
        $("[aria-controls='personalisedContentTab']").show();
    }
    if(document.getElementById('productsFlag').checked){
		$("[aria-controls='productsTab']").show();
    }
    if(document.getElementById('articlesFlag').checked){
		$("[aria-controls='articlesTab']").show();
    }
    addButton('result-section-multifield','confirm-button','js-unilever-save-resultsection','diagnosticToolResultSection.confirmChanges','diagnosticToolResultSection.confirmChanges.desc');
    var eventAttached=$('#confirm-button').attr("eventAttached");
	if(eventAttached!="click"){
		$('#confirm-button').on('click', function() {
			var $form = $(this).closest("form");
			var formName = $form.find("div.cq-dialog-content").attr('data-formName');
			var pathElementArr = $("input[name='./diagnosticToolPath']");
			var query = "?";
			var proceedFillQuestions = true;
			for (var i = 0; i < pathElementArr.length; ++i) {
				var val = $(pathElementArr[i]).val();
				if(val == ""){
					$(window).adaptTo("foundation-ui").alert("Required","Please configure the required question");
					proceedFillQuestions = false;
					break;
				}
				query = query + "path=" + val + "&";
			}
			var jsonData = [];
			var url = $form.attr("action");
			$.ajax({
				url: $form.attr("action") + ".getQuestionData" + query,
				method: "GET",
				async: false

			}).done(function(data) {
				jsonData = JSON.parse(data);
			});
			var questionMultiFieldArr=$('[data-name="questionmultifield"]');
			if(proceedFillQuestions){
			_.each(questionMultiFieldArr, function(obj, index) {
				var liArr=$(obj).children(".coral-Multifield-list").children(); 
				for (var i = 0; i < liArr.length; ++i) {
					var li = $(liArr[i]);				
					fillQuestionsDropdown(li, jsonData);				
				}
				$(obj).on("click", ".coral-Multifield-add", function(e) {
					var liArr = $(obj).children(".coral-Multifield-list").children();
					var lastLI = $(liArr).last();
					fillQuestionsDropdown(lastLI, jsonData);
				});
			
			});
			$('#personalisedRulesMulti').on("click", ".coral-Multifield-add", function(e) {
				var questionmultifieldArr = $('#personalisedRulesMulti').find("[data-name='questionmultifield']")
				var lastquestionmultifiel = questionmultifieldArr[questionmultifieldArr.length - 1];
				$(lastquestionmultifiel).on("click", ".coral-Multifield-add", function(e) {
					var liArr = $(lastquestionmultifiel).children(".coral-Multifield-list").children();
					var lastLI = $(liArr).last();
					fillQuestionsDropdown(lastLI, jsonData);
				});
			});
			$(this).attr("eventAttached","click");
			$(window).adaptTo("foundation-ui").alert("Information","Changes Saved");
		}
		});
	}
	addToolTipToMultiBuyConfig('multibuyConfigurationSection');
}

function fillQuestionsDropdown(li, jsonData) {
    var selectEle = $(li).find("select[name='./question']");
    var selectBox = new CUI.Select({
        element: $(selectEle).closest(".coral-Select")
    });
    var value = $(selectEle).val();
    if (selectBox._selectList) {
        selectBox._selectList.children().remove();
    }
    var optionList = '';
	var flag=false;
    _.each(jsonData, function(valueObj, index) {
		if(valueObj.value==value){
			flag=true;
		}
        var newObj = {
            "value": valueObj.value,
            "display": valueObj.text
        };
        selectBox.addOption(newObj);
        optionList = optionList + '<option value="' + valueObj.value + '" data-path="'+valueObj.path+'">' + valueObj.text + '</option>';
    });
    $(selectEle).find('option').remove().end().append(optionList).val(value);
	if(!flag){
		var ansEle=$(li).find("select[name='./answer']");
		var ansSelectBox = new CUI.Select({
			element: $(ansEle).closest(".coral-Select")
		});
		selectBox.$element.find('.coral-Select-button-text').html('');
		ansSelectBox._selectList.children().remove();
		ansEle.find('option').remove().end().val('').trigger('change');
	}
}

function diagnosticToolResultSectionListOnLoad() {
    var index = 0;
	var questionMultiFieldArr=$('[data-name="questionmultifield"]');
	_.each(questionMultiFieldArr, function(obj, index) {
		$(obj).on("click", ".coral-Multifield-add", function(e) {
            index++;
            var liArr = $(obj).children(".coral-Multifield-list").children();
            var lastLI = $(liArr).last();
            diagnosticToolResultSectionListListType(lastLI, '');

            var selectBoxSelector = "data-id";
            $($(lastLI).find('select')).each(function() {
                var selectName = $(this).attr('name');
                $(this).attr(selectBoxSelector, selectName + index);
            });
            var selectBoxs = $(lastLI).find('select');
            attachOnSelectEventToSelectBox(selectBoxs, selectBoxSelector);
        });
	});
	
    $('#personalisedRulesMulti').on("click", ".coral-Multifield-add", function(e) {
        var questionmultifieldArr = $('#personalisedRulesMulti').find("[data-name='questionmultifield']")
        var lastquestionmultifiel = questionmultifieldArr[questionmultifieldArr.length - 1];
        $(lastquestionmultifiel).on("click", ".coral-Multifield-add", function(e) {
            index++;
            var liArr = $(lastquestionmultifiel).children(".coral-Multifield-list").children();
            var lastLI = $(liArr).last();
            diagnosticToolResultSectionListListType(lastLI, '');

            var selectBoxSelector = "data-id";
            $($(lastLI).find('select')).each(function() {
                var selectName = $(this).attr('name');
                $(this).attr(selectBoxSelector, selectName + index);
            });
            var selectBoxs = $(lastLI).find('select');
            attachOnSelectEventToSelectBox(selectBoxs, selectBoxSelector);
        });
    });
}

function diagnosticToolResultSectionListOnSelect(event) {
	var selected=$(event.target).children('[name="./question"]').find('option:selected').attr('data-path');
    diagnosticToolResultSectionListListType($(event.target).closest("li"), selected);
}

function diagnosticToolResultSectionListListType(parentLI, selectedValue) {
    var jsonData = [];
    var $form = parentLI.closest("form");
    if (!selectedValue) {
        selectedValue = parentLI.find("#question :selected").attr("data-path");
    }
	if(selectedValue){
		var query = "?";
		query = query + "path=" + selectedValue + "&";
		var url = $form.attr("action");
		$.ajax({
			url: $form.attr("action") + ".getAnswerData" + query,
			method: "GET",
			async: false

		}).done(function(data) {
			if (data != null && data != '') {
				jsonData = JSON.parse(data);
				fillAnswersDropdown(parentLI, jsonData);
			}
		});
	}
}

function fillAnswersDropdown(li, jsonData) {
    var selectEle = $(li).find("select[name='./answer']").closest(".coral-Select");
    if (selectEle.length > 0) {
        var selectBox = new CUI.Select({
            element: selectEle
        });
        if (selectBox._selectList) {
            selectBox._selectList.children().remove();
        }
        var optionList = '';
        _.each(jsonData, function(valueObj, index) {
            var newObj = {
                "value": valueObj.value,
                "display": valueObj.text
            };
            selectBox.addOption(newObj);
            optionList = optionList + '<option value="' + valueObj.value + '">' + valueObj.text + '</option>';
        });
        $(li).find("select[name='./answer']").find('option').remove().end().append(optionList).trigger('change');
    }
}


function diagnosticToolresultContentHideShow() {
    $("[aria-controls='personalisedContentTab']").hide();
    $("[aria-controls='productsTab']").hide();
    $("[aria-controls='articlesTab']").hide();
    if (document.getElementById('personalisedTextFlag').checked) {
        $("[aria-controls='personalisedContentTab']").show();
    }
    if (document.getElementById('productsFlag').checked) {
        $("[aria-controls='productsTab']").show();
    }
    if (document.getElementById('articlesFlag').checked) {
        $("[aria-controls='articlesTab']").show();
    }
}

function diagnosticToolresultSectionOnSubmit(){
	$(document).on("click", ".cq-dialog-submit", function(event) {
        var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');

        if (formName && formName == 'diagnosticToolResultSection') {
	if (document.getElementById('personalisedTextFlag').checked) {
		$('select[name="./question"]').each(function() {
			var questionVal = $(this).val();
			if (questionVal) {
			if (questionVal.length == 0) {
                $(window).adaptTo("foundation-ui").alert("Required", "Please Select Question");
                event.preventDefault();
                return false;
            } else {
                return true;
            }
			}else{
				 $(window).adaptTo("foundation-ui").alert("Required", "Please Select Question");
				 event.preventDefault();
	                return false;
			}
		});
		$('select[name="./answer"]').each(function() {
			var answerVal = $(this).val();
			if(answerVal){
			if (answerVal.length == 0) {
                $(window).adaptTo("foundation-ui").alert("Required", "Please Select Answer");
                event.preventDefault();
                return false;
            } else {
                return true;
            }
		}else{
			$(window).adaptTo("foundation-ui").alert("Required", "Please Select Answer");
			event.preventDefault();
            return false;
		}
		});
    }
    if (document.getElementById('productsFlag').checked) {
    	$('input[type="number"][name="./numberOfProductsToDisplay"]').each(function() {
			var numberOfProductsToDisplay = $(this).val();
			if (numberOfProductsToDisplay<1) {
                $(window).adaptTo("foundation-ui").alert("Required", "A positive value must be provided for number of products to display");
                event.preventDefault();
                return false;
            } else {
                return true;
            }
		});
    }
    if (document.getElementById('articlesFlag').checked) {
    	$('input[type="number"][name="./numberOfArticlesToDisplay"]').each(function() {
			var numberOfArticlesToDisplay = $(this).val();
			if (numberOfArticlesToDisplay<1) {
                $(window).adaptTo("foundation-ui").alert("Required", "A positive value must be provided for number of articles to display");
                event.preventDefault();
                return false;
            } else {
                return true;
            }
		});
    }
        }
	});
}

function diagnosticToolOnIntroPanelClick(){
	$("[aria-controls='includeIntroPanelTab']").hide();
	if(document.getElementById('includeIntroPanel').checked){
		$("[aria-controls='includeIntroPanelTab']").show();
	}
}

function diagnosticToolQuestionOnLoad(fieldId){
	$('input[type="hidden"][name="./responseId"]').each(function(index) {
    	if(!$(this).val()){
        $(this).val(index+1);
    }
    });
	$(document).ready(function(e) {
        if ($('#' + fieldId).val() == "") {
        	var obj=getBrandAndLocale($('#' + fieldId).closest('form'));
            obj["entity"] = "diagnostictool",
            $.ajax({
                type: 'GET',
                url: '/bin/ts/private/uniqueid',
                data: obj,
                success: function(data) {
                	if(data.code == 200){
                		$('#' + fieldId).val(data.responseData.id);
                        $('#' + fieldId).trigger("change");
                	} else{
                		$(window).adaptTo("foundation-ui").alert("Internal Server Error", "Server is down or server doesn't exist and update the server details");
                	}
                }
            });
        }
    });
	var questionType = $('#questionType :selected').val();
	if(questionType == 'sectionDivider'){
		 $("[aria-controls='responseConfigurationTab']").hide();
	}else{
		$("[aria-controls='responseConfigurationTab']").show();
	}
	var switchToNextQuestion = $('#switchToNextQuestion :selected').val();
	if(switchToNextQuestion == 'afterTimeInterval'){
		$("#delayInterval").parent().show();
	}else{
		$("#delayInterval").parent().hide();
	}
	if (document.getElementById('responseImageNotRequired').checked) {
	     $(".coral-Multifield-list li [id='foregroundImageResponse']").parent().hide();

			 $(".coral-Multifield-list li [id='foregroundImageAltTextResponse']").parent().hide();
		}
	    else
	    {
	       $(".coral-Multifield-list li [id='foregroundImageResponse']").parent().show();

		   $(".coral-Multifield-list li [id='foregroundImageAltTextResponse']").parent().show();

	    }
	if (document.getElementById('tagNotRequired').checked) {
	     $(".coral-Multifield-list li [id='matchingTag']").parent().hide();

		}
	    else
	    {
	       $(".coral-Multifield-list li [id='matchingTag']").parent().show();

	    }
}

function diagnosticToolQuestionOnSubmit(){
	$(document).on("click", ".cq-dialog-submit", function(e) {
        var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');
        if (formName && formName == 'diagnosticToolQuestion') {
        	var sourceType= $('[name="./questionType"]').val();
        	if (!(document.getElementById('responseImageNotRequired').checked) && sourceType != 'sectionDivider') {
        		$('input[type="text"][name="./foregroundImageResponse"]').each(function() {
					var urlVal = $(this).val();
					if (urlVal.length == 0) {
						$(window).adaptTo("foundation-ui").alert("Required", "Please Configure the Foreground Image");
						e.preventDefault();
						return false;
					} else {
						return true;
					}
				});
        		$('input[type="text"][name="./foregroundImageAltTextResponse"]').each(function() {
					var urlVal = $(this).val();
					if (urlVal.length == 0) {
						$(window).adaptTo("foundation-ui").alert("Required", "Please Configure the Foreground Image Alt Text");
						e.preventDefault();
						return false;
					} else {
						return true;
					}
				});
        	}
        	if (!(document.getElementById('tagNotRequired').checked) && sourceType != 'sectionDivider') {
        		$('input[type="text"][name="./matchingTag"]').each(function() {
					var urlVal = $(this).val();
					if (urlVal.length == 0) {
						$(window).adaptTo("foundation-ui").alert("Required", "Please Configure the matching Tag");
						e.preventDefault();
						return false;
					} else {
						return true;
					}
				});
        	}

        }
    });
}