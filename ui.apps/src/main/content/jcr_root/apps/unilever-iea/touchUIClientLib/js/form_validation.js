(function($) {
    // function to add a toptip to the invalid filed
	function addTooltip(tooltip, element) {
 		element.addClass("is-invalid");
        element.after(tooltip);
    }
	// create a tooltip element with given message
    function createTooltip(message) {
    	return $("<span class=\"coral-Form-fielderror coral-Icon coral-Icon--alert coral-Icon--sizeS\" data-init=\"quicktip\" data-quicktip-type=\"error\" data-quicktip-arrow=\"right\" data-quicktip-content=\"" + message + "\"></span>");
    }
}(window.jQuery));