
//JS to hide tab on load based on checkbox
function socialMediaGalleryOnSelect(_self, selectedValue) {

	var selectedValue = $('#galleryTypeMode :selected').val();
    if (selectedValue == "dynamic" || !selectedValue) {    	
    	 $("[aria-controls='fixedListTab']").hide();
		 $('#socialChannelsToInclude').show();
		
	} else if(selectedValue == "fixed") {
		 $("[aria-controls='fixedListTab']").show();
		 $('#socialChannelsToInclude').hide();
		$("[aria-controls='twitterTab']").hide();
		$("[aria-controls='instagramTab']").hide();
		$("[aria-controls='facebookTab']").hide();
		$("[aria-controls='videoTab']").hide();
		$("[aria-controls='tabDamTab']").hide();
    }
    socialTabTwitterOnClick();
    socialTabInstagramOnClick();
    socialTabFacebookOnClick();
    socialTabVideoOnClick();
    socialTabLocalContentOnClick();
    socialTabFilterOnClick();
    linkPostToProductOrPage('facebookLinkPostToProductOrPage');
    linkPostToProductOrPage('twitterLinkPostToProductOrPage');
    linkPostToProductOrPage('instagramLinkPostToProductOrPage');
    linkPostToProductOrPage('videoLinkPostToProductOrPage');
    linkPostToProductOrPage('localAssetLinkPostToProductOrPage');
    linkPostToProductOrPage('fixedLinkPostToProductOrPage');

    customInternalLinkTypeSelect('facebookLinkType');
    customInternalLinkTypeSelect('twitterLinkType');
    customInternalLinkTypeSelect('instagramLinkType');
    customInternalLinkTypeSelect('videoLinkType');
    customInternalLinkTypeSelect('localAssetLinkType');
    customInternalLinkTypeSelect('fixedPostLinkType');
    showTabChannelImage();
    showCustomTitleOfTabFixed();
    customFixedTabSelectChannel();
    showCustomChannelImageTabFixed();
    paginationLabelHide();

    twitterSearchPostBy();
    showLocaAssetChannelImage();
	
	showFilterSearchPlaceholder();
}

function socialTabTwitterOnClick() {
	var selectedValue = $('#galleryTypeMode :selected').val();
	if(selectedValue == "dynamic" || !selectedValue) {
        if (document.getElementById('twitter').checked) {
            $("[aria-controls='twitterTab']").prop('disabled', false);
            $("[aria-controls='twitterTab']").show();
			var selectedValue = $('#twitterSearchPostBy :selected').val();
			if(selectedValue == "hashTags" || !selectedValue) {
				$("#twitterSearchPostBy").parents(".coral-Form-fieldwrapper").nextAll().slice(0, 1).show();
				$("#twitterSearchPostBy").parents(".coral-Form-fieldwrapper").nextAll().slice(1, 2).hide();
			} else if(selectedValue == "list"){
				$("#twitterSearchPostBy").parents(".coral-Form-fieldwrapper").nextAll().slice(0, 1).hide();
				$("#twitterSearchPostBy").parents(".coral-Form-fieldwrapper").nextAll().slice(1, 2).show();
			}else{
				$("#twitterSearchPostBy").parents(".coral-Form-fieldwrapper").nextAll().slice(0, 2).hide();
			}			
        } else {
            $("[aria-controls='twitterTab']").hide();
            $("[aria-controls='twitterTab']").prop('disabled', true);
        }
	} else if(selectedValue == "fixed"){
		$("[aria-controls='twitterTab']").hide();
        $("[aria-controls='twitterTab']").prop('disabled', true);
	}
}
function socialTabInstagramOnClick() {
	var selectedValue = $('#galleryTypeMode :selected').val();
	if(selectedValue == "dynamic" || !selectedValue) {
        if (document.getElementById('instagram').checked) {
            $("[aria-controls='instagramTab']").prop('disabled', false);
            $("[aria-controls='instagramTab']").show();
        } else {
            $("[aria-controls='instagramTab']").hide();
            $("[aria-controls='instagramTab']").prop('disabled', true);
        }
	} else if(selectedValue == "fixed"){
		$("[aria-controls='instagramTab']").hide();
        $("[aria-controls='instagramTab']").prop('disabled', true);
	}
}
function socialTabFacebookOnClick() {
	var selectedValue = $('#galleryTypeMode :selected').val();
	if(selectedValue == "dynamic" || !selectedValue) {
        if (document.getElementById('facebook').checked) {
            $("[aria-controls='facebookTab']").prop('disabled', false);
            $("[aria-controls='facebookTab']").show();
        } else {
            $("[aria-controls='facebookTab']").hide();
            $("[aria-controls='facebookTab']").prop('disabled', true);
        }
	} else if(selectedValue == "fixed"){
		$("[aria-controls='facebookTab']").hide();
        $("[aria-controls='facebookTab']").prop('disabled', true);
	}
}
function socialTabVideoOnClick() {
	var selectedValue = $('#galleryTypeMode :selected').val();
	if(selectedValue == "dynamic" || !selectedValue) {
        if (document.getElementById('video').checked) {
            $("[aria-controls='videoTab']").prop('disabled', false);
            $("[aria-controls='videoTab']").show();
            if($('#videoIdRequired').val() == ''){
                $("#videoIdRequired").attr('aria-required', true);
                $("#videoIdRequired").addClass("is-invalid");
            }else{
				$("#videoIdRequired").attr('aria-required', false);
				$("#videoIdRequired").removeClass("is-invalid");
            }
        } else {
            $("[aria-controls='videoTab']").hide();
            $("[aria-controls='videoTab']").prop('disabled', true);
            $("#videoIdRequired").attr('aria-required', false);
            $("#videoIdRequired").removeClass("is-invalid");
        }
	} else if(selectedValue == "fixed"){
		$("[aria-controls='videoTab']").hide();
        $("[aria-controls='videoTab']").prop('disabled', true);
        $("#videoIdRequired").attr('aria-required', false);
        $("#videoIdRequired").removeClass("is-invalid");
	}
}
function socialTabLocalContentOnClick() {
	var selectedValue = $('#galleryTypeMode :selected').val();
	if(selectedValue == "dynamic" || !selectedValue) {
        if (document.getElementById('localAsset').checked) {
            $("[aria-controls='tabDamTab']").prop('disabled', false);
            $("[aria-controls='tabDamTab']").show();
        } else {
            $("[aria-controls='tabDamTab']").hide();
            $("[aria-controls='tabDamTab']").prop('disabled', true);
        }
	} else if(selectedValue == "fixed"){
		$("[aria-controls='tabDamTab']").hide();
        $("[aria-controls='tabDamTab']").prop('disabled', true);
	}
}
function socialTabFilterOnClick() {
	var selectedValue = $('#galleryTypeMode :selected').val();
        if (document.getElementById('enableSocialFilter').checked) {
            $("[aria-controls='filterTab']").prop('disabled', false);
            $("[aria-controls='filterTab']").show();
        } else {
            $("[aria-controls='filterTab']").hide();
            $("[aria-controls='filterTab']").prop('disabled', true);
        }
}
function linkPostToProductOrPage(checkboxId){
    $("input[name='./"+checkboxId+"']").each(function() {
      var dropDownDiv = $(this).parents('.coral-Form-fieldwrapper').next().next();
        var dropDownVal = $(dropDownDiv.children("span")).children("select").val();
        if (undefined != $(this) && $(this).is(":checked")) {
            if(dropDownVal == 'customInternal'){
                $(this).parents('.coral-Form-fieldwrapper').nextAll().slice(0, 4).show();
                $(this).parents('.coral-Form-fieldwrapper').nextAll().slice(8, 9).show();
            }else if(dropDownVal == 'customExternal'){
                $(this).parents('.coral-Form-fieldwrapper').nextAll().slice(0, 9).show();
            }else{
                $(this).parents('.coral-Form-fieldwrapper').nextAll().slice(0, 2).show();
                $(this).parents('.coral-Form-fieldwrapper').nextAll().slice(8, 9).show();
            }
        }else{
            $(this).parents('.coral-Form-fieldwrapper').nextAll().slice(0, 9).hide();
        }
    });
}
function customInternalLinkTypeSelect(dropDownID){
	 $("select[name='./"+dropDownID+"']").each(function() {
		var selectedVal = $(this).val();
        var customType = $(this).parent();
        if (selectedVal == 'customInternal') {
            $(customType).parents('.coral-Form-fieldwrapper').nextAll().slice(0, 2).show();
            $(customType).parents('.coral-Form-fieldwrapper').nextAll().slice(2, 6).hide();
        }else if(selectedVal == 'customExternal'){
            $(customType).parents('.coral-Form-fieldwrapper').nextAll().slice(0, 6).show();
        }else{
            $(customType).parents('.coral-Form-fieldwrapper').nextAll().slice(0, 6).hide();
        }
     });
}
function showTabChannelImage(){
     $("input[name='./setCustomChannelImage']").each(function() {
		if (undefined != $(this) && $(this).is(":checked")) {
			$(this).parents('.coral-Form-fieldwrapper').nextAll().slice(0, 2).show();
        }else{
			$(this).parents('.coral-Form-fieldwrapper').nextAll().slice(0, 2).hide();
        }
     });
}
function showCustomTitleOfTabFixed(){
     $("input[name='./fixedAddCustomTitleCaption']").each(function() {
		if (undefined != $(this) && $(this).is(":checked")) {
			$(this).parents('.coral-Form-fieldwrapper').nextAll().slice(0, 3).show();
        }else{
			$(this).parents('.coral-Form-fieldwrapper').nextAll().slice(0, 3).hide();
        }
     });
}
function customFixedTabSelectChannel(){
	 $("select[name='./fixedChannelType']").each(function() {
		var selectedVal = $(this).val();
        var customType = $(this).parent();
        var divElem = $(customType).parents('.coral-Form-fieldwrapper');
        var dialogFields = $(customType).parents('.coral-Form-fieldwrapper').nextAll();
        if (selectedVal == 'facebook' || selectedVal == 'twitter' || selectedVal == 'instagram') {
        	dialogFields.slice(0, 2).hide();
			dialogFields.slice(2, 4).show();
			dialogFields.slice(4, 6).hide();
            dialogFields.slice(6, 8).show();
            dialogFields.slice(11, 12).hide();
            dialogFields.slice(14, 15).show();
        }else if(selectedVal == 'video'){
        	dialogFields.slice(0, 4).show();
			dialogFields.slice(4, 6).hide();
			dialogFields.slice(6, 8).show();
			dialogFields.slice(11, 12).hide();
			dialogFields.slice(14, 15).show();
        }else if(selectedVal == 'localAsset'){
        	dialogFields.slice(0, 4).hide();
			dialogFields.slice(4, 8).show();
            dialogFields.slice(11, 12).show();
            dialogFields.slice(14, 15).show();
        }
     });
}
function showCustomChannelImageTabFixed(){
     $("input[name='./fixedSetCustomChannelImage']").each(function() {
		if (undefined != $(this) && $(this).is(":checked")) {
			$(this).parents('.coral-Form-fieldwrapper').nextAll().slice(0, 2).show();
        }else{
			$(this).parents('.coral-Form-fieldwrapper').nextAll().slice(0, 2).hide();
        }
     });
}

function mediaGalleryVisualElementControl(){
if (document.getElementById('overrideGlobalConfig').checked) {
    $('#overrideGlobalConfig').parents('.coral-Form-fieldwrapper').nextAll().slice(0, 6).show();                                                     
    }else{
    $('#overrideGlobalConfig').parents('.coral-Form-fieldwrapper').nextAll().slice(0, 6).hide();
     }
}

function socialMediaGalleryOnSubmit(){
	$(document).on("click", ".cq-dialog-submit", function (event) {
		var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');
		if(formName && formName=='socialMediaGallery') {
            var selectedValue12 = $('#galleryTypeMode :selected').val();
    		if (selectedValue12 == "dynamic" || !selectedValue12) {
                //is Social Channel included
                isSocialChannelIncluded();
                if (document.getElementById('facebook').checked) {
                    var message = "Please Enter the facebook account id";
                    hasEnteredAccountId("facebookAccountIdJson", message, "facebookAccountId");

                }                
                if (document.getElementById('twitter').checked) {	
                    var message = "Please Enter the twitter account id";
                    var name = "twitterAccountIdJson";
                    hasEnteredAccountId(name, message, "twitterAccountId");
                }
                if (document.getElementById('instagram').checked) {
                    var message = "Please Enter the instagram account id";
                    hasEnteredAccountId("instagramAccountIdJson", message, "instagramAccountId");
                }
                if (document.getElementById('localAsset').checked) {
                    var message = "Please Enter the Tab Content hash tags";
                    hasEnteredAccountId("localAssetHashTags", message, "localAssetHashTagName");
                }
            }
            checkKeyWord();
            checlFixedTabMandate();
        }
  });
}
function hasEnteredAccountId(name, message, accountId){
	var olObject = $("#"+name).find(".coral-Multifield-list");

       if ( olObject.children().length < 1 ) {
            $(window).adaptTo("foundation-ui").alert("Required",message);
            event.preventDefault();
            return false;
    }else{
        olObject.find("li").each(function(){
            if($(this).find("input[name='./" + accountId + "']").val().length === 0){
                $("input[type='text'][name='./" + accountId + "']").attr('aria-required', true);
                $("input[type='text'][name='./" + accountId + "']").addClass("is-invalid");
            }
        });
    }
}

function isSocialChannelIncluded(){

     if(document.getElementById('video').checked) {
        return true;
    }
     if(document.getElementById('facebook').checked) {
        return true;
    }
     if(document.getElementById('twitter').checked) {
        return true;
    }
    if (document.getElementById('instagram').checked) {
        return true;
    }
     if (document.getElementById('localAsset').checked) {
        return true;
    }else{
        $(window).adaptTo("foundation-ui").alert("Required","Please choose atleast one social channel.");
            event.preventDefault();
            return false;
    }
}

function checkKeyWord(){
	var olObject = $("#keywordFilters").find(".coral-Multifield-list");
	if ( olObject.children().length > 0 ) {
        olObject.find("li").each(function(){
            if($(this).find("input[name='./keyword']").val().length === 0){
                $("input[type='text'][name='./keyword']").attr('aria-required', true);
                $("input[type='text'][name='./keyword']").addClass("is-invalid");
            }
        });
    }
}

function checlFixedTabMandate(){
	 $("select[name='./fixedChannelType']").each(function() {
		var selectedVal = $(this).val();
        var customType = $(this).parent();
        var flag = false;
		var accountIdElem = $(customType).parents('.coral-Form-fieldwrapper').nextAll().slice(0,1).find("input[name='./fixedAccountId']");        
        var postIdElem = $(customType).parents('.coral-Form-fieldwrapper').nextAll().slice(1,2).find("input[name='./fixedPostId']");
        var videoIdElem = $(customType).parents('.coral-Form-fieldwrapper').nextAll().slice(2,3).find("input[name='./fixedVideoId']");
        if (selectedVal == 'facebook' || selectedVal == 'twitter' || selectedVal == 'instagram') {
			if(accountIdElem.val().length === 0){
                accountIdElem.attr('aria-required', true);
                accountIdElem.addClass("is-invalid");
                flag = true;
            }
			if(postIdElem.val().length === 0){
                postIdElem.attr('aria-required', true);
                postIdElem.addClass("is-invalid");
                flag = true;
            }
        }else if(selectedVal == 'video'){
			if(accountIdElem.val().length === 0){
                accountIdElem.attr('aria-required', true);
                accountIdElem.addClass("is-invalid");
                flag = true;
            }
			if(postIdElem.val().length === 0){
                postIdElem.attr('aria-required', true);
                postIdElem.addClass("is-invalid");
                flag = true;
            }
			if(videoIdElem.val().length === 0){
                videoIdElem.attr('aria-required', true);
                videoIdElem.addClass("is-invalid");
                flag = true;
            }
        }
         if(flag){
			return false;
         }
     });
}
function paginationLabelHide(){
	var pagiType = $("select[name='./paginationType']").val();
    if(pagiType == 'pagination'){
		$("select[name='./paginationType']").parents('.coral-Form-fieldwrapper').nextAll().slice(1, 2).hide();
    }else{
		$("select[name='./paginationType']").parents('.coral-Form-fieldwrapper').nextAll().slice(1, 2).show();
    }
}


function twitterSearchPostBy(){
    if (document.getElementById('twitter').checked) {
        var selectedValue = $('#twitterSearchPostBy :selected').val();
        if(selectedValue == "hashTags" || !selectedValue) {
			$("#twitterSearchPostBy").parents(".coral-Form-fieldwrapper").nextAll().slice(0, 1).show();
         	$("#twitterSearchPostBy").parents(".coral-Form-fieldwrapper").nextAll().slice(1, 2).hide();
        } else if(selectedValue == "list"){
			$("#twitterSearchPostBy").parents(".coral-Form-fieldwrapper").nextAll().slice(0, 1).hide();
         	$("#twitterSearchPostBy").parents(".coral-Form-fieldwrapper").nextAll().slice(1, 2).show();
        }else{
			$("#twitterSearchPostBy").parents(".coral-Form-fieldwrapper").nextAll().slice(0, 2).hide();
        }

    }
}
function showLocaAssetChannelImage(){
     $("input[name='./localAssetSetCustomChannelImage']").each(function() {
		if (undefined != $(this) && $(this).is(":checked")) {
			$(this).parents('.coral-Form-fieldwrapper').nextAll().slice(0, 2).show();
        }else{
			$(this).parents('.coral-Form-fieldwrapper').nextAll().slice(0, 2).hide();
        }
     });
}
function showFilterSearchPlaceholder(){
	var enSearchInput = $('input[name="./enableSearchInput"]');
    if(enSearchInput.is(":checked")){
		enSearchInput.parents(".coral-Form-fieldset").nextAll().slice(0,1).show();
    }else{
		enSearchInput.parents(".coral-Form-fieldset").nextAll().slice(0,1).hide();
    }
}
function onKeyUpCheckId() {
  if($('#videoIdRequired').val() == ''){
                $("#videoIdRequired").attr('aria-required', true);
                $("#videoIdRequired").addClass("is-invalid");
            }else{
				$("#videoIdRequired").attr('aria-required', false);
				$("#videoIdRequired").removeClass("is-invalid");
            }
}