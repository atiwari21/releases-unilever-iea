// social gallery v2 on select function
function SocialGalleryV2OnSelect(_self, selectedValue) {

    var selectedValue = $('#aggregationMode :selected').val();
    if (selectedValue == "automatic" || !selectedValue) {    	
    	 $("[aria-controls='fixedListTab']").hide();
         $("input[type='text'][name='./postIdFixed']").attr('aria-required', false);
		 $("input[type='text'][name='./linkPostToFixed']").attr('aria-required', false);
		 $('#socialChannelsToInclude').show();
		
	} else if(selectedValue == "manual") {
		 $("[aria-controls='fixedListTab']").show();
         $("input[type='text'][name='./postIdFixed']").attr('aria-required', true);
		 $("input[type='text'][name='./linkPostToFixed']").attr('aria-required', true);
		 $('#socialChannelsToInclude').hide();
		$("[aria-controls='twitterTab']").hide();
		$("[aria-controls='instagramTab']").hide();
		$("input[type='text'][name='./twitterAccountId']").attr('aria-required', false);
		$("input[type='text'][name='./postIdTwitter']").attr('aria-required', false);
		$("input[type='text'][name='./linkPostToTwitter']").attr('aria-required', false);
		$("input[type='text'][name='./postIdInstagram']").attr('aria-required', false);
		$("input[type='text'][name='./linkPostToInstagram']").attr('aria-required', false);
    }
	SocialChannelsTwitterOnClick();
	SocialChannelsInstagramOnClick();
	
}



function SocialChannelsTwitterOnClick() {
	var selectedValue = $('#aggregationMode :selected').val();
	if(selectedValue == "automatic" || !selectedValue) {
        if (document.getElementById('twitter').checked) {
            $("[aria-controls='twitterTab']").prop('disabled', false);
            $("[aria-controls='twitterTab']").show();
			$("input[type='text'][name='./twitterAccountId']").attr('aria-required', true);
			$("input[type='text'][name='./postIdTwitter']").attr('aria-required', true);
			$("input[type='text'][name='./linkPostToTwitter']").attr('aria-required', true);
        } else {
            $("[aria-controls='twitterTab']").hide();
            $("[aria-controls='twitterTab']").prop('disabled', true);
			$("input[type='text'][name='./twitterAccountId']").attr('aria-required', false);
			$("input[type='text'][name='./postIdTwitter']").attr('aria-required', false);
			$("input[type='text'][name='./linkPostToTwitter']").attr('aria-required', false);
        }
	} else if(selectedValue == "manual"){
		$("[aria-controls='twitterTab']").hide();
        $("[aria-controls='twitterTab']").prop('disabled', true);
	}
}

function SocialChannelsInstagramOnClick() {
	var selectedValue = $('#aggregationMode :selected').val();
	if(selectedValue == "automatic" || !selectedValue) {
        if (document.getElementById('instagram').checked) {
            $("[aria-controls='instagramTab']").prop('disabled', false);
            $("[aria-controls='instagramTab']").show();
			$("input[type='text'][name='./postIdInstagram']").attr('aria-required', true);
			$("input[type='text'][name='./linkPostToInstagram']").attr('aria-required', true);
        } else {
            $("[aria-controls='instagramTab']").hide();
            $("[aria-controls='instagramTab']").prop('disabled', true);
			$("input[type='text'][name='./postIdInstagram']").attr('aria-required', false);
			$("input[type='text'][name='./linkPostToInstagram']").attr('aria-required', false);
        }
	} else if(selectedValue == "manual"){
		$("[aria-controls='instagramTab']").hide();
        $("[aria-controls='instagramTab']").prop('disabled', true);
	}
}



function socialGalleryV2TwitterLinkType(parentLI, selectedValue) {
    $('.linkTypeTwitter').each(function() {
        var type = $(this).closest("li").find('#linkTypeId :selected').val();
        if (type == 'custom' || !type) {
            $(this).closest("li").find("#headingTextId").parent().show();
            $(this).closest("li").find("#linkPostToCtaId").parent().show();
        } else {
            $(this).closest("li").find("#headingTextId").parent().hide();
            $(this).closest("li").find("#linkPostToCtaId").parent().hide();
        }
    });

}

function socialGalleryV2InstagramLinkType(parentLI, selectedValue) {
    $('.linkTypeInstagram').each(function() {
        var type = $(this).closest("li").find('#linkTypeId :selected').val();
        if (type == 'custom' || !type) {
            $(this).closest("li").find("#headingTextId").parent().show();
            $(this).closest("li").find("#linkPostToCtaId").parent().show();
        } else {
            $(this).closest("li").find("#headingTextId").parent().hide();
            $(this).closest("li").find("#linkPostToCtaId").parent().hide();
        }
    });

}

function socialGalleryV2FixedLinkType(parentLI, selectedValue) {
    $('.linkTypeFixed').each(function() {
        var type = $(this).closest("li").find('#linkTypeId :selected').val();
        if (type == 'custom' || !type) {
            $(this).closest("li").find("#headingTextId").parent().show();
            $(this).closest("li").find("#linkPostToCtaId").parent().show();
        } else {
            $(this).closest("li").find("#headingTextId").parent().hide();
            $(this).closest("li").find("#linkPostToCtaId").parent().hide();
        }
    });

}
