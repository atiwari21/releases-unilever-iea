	//Collecting data from multifield and populates the data to multifield items
	(function($, $document) {
		var DATA_EAEM_NESTED = "data-eaem-nested";
		var CFFW = ".coral-Form-fieldwrapper";
		var DATA_NUMBER_OF_ITEMS = "data-custom-multi";
		var NESTED_NUMBER_OF_ITEMS = "data-nested-multi";
		var dataSaved = false;
		var multipleRandomNumbers = false;
		//reads multifield data from server, creates the nested composite multifields and fills them
		var addDataInFields = function() {
			//$(document).on("dialog-ready", function() {
			dataSaved = false;
			var mNameArr = [];
			var mName;
			var eleArr = $("[" + DATA_EAEM_NESTED + "]");
			$.each(eleArr, function(i, ele) {
				mName = $(ele).attr("data-name");
				if ($.inArray(mName, mNameArr) == -1) {
					mNameArr.push(mName);
				}

			});
			$.each(mNameArr, function(i, mn) {
				if (!mn) {
					return;
				}
				var $fieldSets = $("[" + DATA_EAEM_NESTED + "][data-name='" + mn + "']"),
					$form = $fieldSets.closest("form.foundation-form");
				var actionUrl = $form.attr("action") + ".json";
				var postProcess = function(data) {
					
					//strip ./
					mn = mn.substring(2);
					if (!data || !data[mn]) {
						return;
					}

					var mValues = data[mn],
						$field, name;
					if (_.isString(mValues)) {
						mValues = [JSON.parse(mValues)];
					}

					_.each(mValues, function(record, i) {
						if (!record) {
							return;
						}

						if (_.isString(record)) {
							record = JSON.parse(record);
						}

						_.each(record, function(rValue, rKey) {

							$field = $($fieldSets[i]).find("[name='./" + rKey + "']");
							addData($field, rValue, $fieldSets[i],rKey);                        
						});
					});
				};
				var addData = function($field, rValue, $fieldSet, rKey) {
					if (_.isArray(rValue)) {
						if ($field.is(':checkbox') && rValue == "true") {
							$field.prop('checked', true);
						} else if (!_.isEmpty(rValue)) {
							fillNestedFields($($fieldSet).find("[data-init='multifield']"), rValue);
						}
					} else {
						if ($field.is('select')) {
							$field.val(rValue).trigger('change');
							//check added for diagnostic tool response component
							if($($fieldSet).attr("data-name")!="questionmultifield"){
								attachOnLoadEventToSelectBox(($field), rValue);
							}

						}else if ($field.parent('.richtext-container').length) {
								$field.val(rValue);
								$field.parent('.richtext-container').find("div.coral-RichText").html(rValue);
							} else if(rValue.length>0 && rValue.split(",").length>0){
								var $tagField = $($fieldSet).find("[data-fieldname='./" + rKey + "']");
								if($tagField && $tagField.parent().attr("data-multiple")){
									var arr = rValue.split(",");
									for(i=0;i<arr.length;i++){
										var valueString = arr[i];
										var newValueString = valueString.replace(/\//g, ' / ');
										var withOutColonString = newValueString.replace(':', ' : ');
										var firstLetterCapital = withOutColonString.replace(withOutColonString[0], withOutColonString[0].toUpperCase());
										var eleventhLetterCapital = firstLetterCapital.replace(withOutColonString[11], withOutColonString[11].toUpperCase());
										$tagField.append('<li role="option" tabindex="0" class="coral-TagList-tag" title='+JSON.stringify(eleventhLetterCapital)+'><button class="coral-MinimalButton coral-TagList-tag-removeButton" type="button" tabindex="-1" title="Remove"><i class="coral-Icon coral-Icon--sizeXS coral-Icon--close"></i></button><span class="coral-TagList-tag-label">'+eleventhLetterCapital+'</span><input type="hidden" value='+arr[i]+' name='+$tagField.attr("data-fieldname")+'></li>');
										
									}
								}else{
									$field.val(rValue);
								}
							} else {
								$field.val(rValue);
							}
					}

				};

				//creates & fills the nested multifield with data
				var fillNestedFields = function($multifield, valueArr) {
					_.each(valueArr, function(record, index) {
						$multifield.find(".js-coral-Multifield-add").click();
						if (!_.isObject(record)) {
							record = JSON.parse(record);
						}
						//a setTimeout may be needed
						_.each(record, function(value, key) {

							var $field = $($multifield.find("[name='./" + key + "']")[index]);
							addData($field, value,$multifield,key);                        
						})
					})
				};
				$.ajax(actionUrl).done(postProcess);
			});
		};

		var fillValue = function($field, record,fieldObject,initialFieldIndex) {
			var name = $field.attr("name");

			if (!name) {
				return;
			}

			//strip ./
			if (name.indexOf("./") == 0) {
				name = name.substring(2);
			}
			if($field.is('.coral-RichText-isRichTextFlag') && $field.is(':checkbox')){
	                record[name] = getRichTextValueWithoutBlankPtags($field.val());
			}else if($field.is(':checkbox') && !$field.parents(".richtext-container").length) {
				if ($field.is(":checked")) {
					record[name] = [true];
				} else {
					record[name] = [];
				}
			} else if($field.length>1 && name.indexOf("@Delete") > -1 && $field.parent().attr("data-multiple")){
					var tagValue = "";
					for(i=1;i<$field.length;i++){
						if($field[i].value.length>0){
							if(tagValue.length>0){
								tagValue = tagValue+","+$field[i].value;
							}else{
								tagValue = $field[i].value;
							}
						} 
					}
					var tagName = $field[1].name;
					if(tagName.indexOf("./") == 0){
						tagName = tagName.substring(2);
					}
					record[tagName] = tagValue;
				}else if($field.length==2 && name.indexOf("@Delete") > -1){
					var tagName = $field[1].name;
					if(tagName.indexOf("./") == 0){
						tagName = tagName.substring(2);
					}
					record[tagName] = $field[1].value;
				} else {
					record[name] = getRichTextValueWithoutBlankPtags($field.val());
				}
			if(initialFieldIndex == 0){
			var $hiddenInput = $(fieldObject).closest("div.coral-Form-fieldwrapper").siblings("input[type='hidden'][name]");
			$hiddenInput.each(function(i, hiddenInput) {
				var attrName = $(hiddenInput).attr("name");

				if (!attrName) {
					return;
				}

				//strip ./
				if (attrName.indexOf("./") == 0) {
					attrName = attrName.substring(2);
				}
				var randomNumberJS = "";
					if(attrName && attrName == "randomNumber"){
							randomNumberJS = $(hiddenInput).val();
							if (typeof randomNumberJS === "undefined" || randomNumberJS.length == 0) {
								randomNumberJS = Math.round(Math.random() * 999);
								$(hiddenInput).val(randomNumberJS);
							} else {
								$(hiddenInput).val(randomNumberJS);
							}
							$(hiddenInput).trigger('change');
					}else if(attrName){
						randomNumberJS = $(hiddenInput).val();
					}
					
					record[attrName] = randomNumberJS;
			});
			}
			//remove the field, so that individual values are not POSTed
			//$field.remove();
		};

		//for getting the nested multifield data as js objects
		var getRecordFromMultiField = function($multifield) {
			//debugger;
			var $fieldSets = $multifield.find("[class='coral-Form-fieldset']");

			var records = [],
				record, $fields, name;

			$fieldSets.each(function(i, fieldSet) {
				$fields = $(fieldSet).find("[name]");

				record = {};

				$fields.each(function(j, field) {
					fillValue($(field), record, $(field),j);
				});

				if (!$.isEmptyObject(record)) {
					records.push(record)
				}
			});

			return records;
		};

		//collect data from widgets in multifield and POST them to CRX as JSON
		var collectDataFromFields = function() {
			$(".coral-Form").submit(function() {
				if (dataSaved) {
					return;
				} else {
					dataSaved = true;
				}
				var $form = $(this).closest("form.foundation-form");
				var mNameArr = [];
				var noLinksArr = [];
				var mName;
				var eleArr = $("[" + DATA_EAEM_NESTED + "]");
				$.each(eleArr, function(i, ele) {
					mName = $(ele).attr("data-name");
					if ($.inArray(mName, mNameArr) == -1) {
						mNameArr.push(mName);
					}

				});
              //operating over each fieldset i.e different multifields Ex--(products)
				$.each(mNameArr, function(i, mn) {
					var $fieldSets = $("[" + DATA_EAEM_NESTED + "][data-name='" + mn + "']");
					var numberOfNestedItems = $("[" + NESTED_NUMBER_OF_ITEMS + "]").data("name");
					if (numberOfNestedItems == undefined || numberOfNestedItems == 'undefined' || numberOfNestedItems == '' || numberOfNestedItems == null) {
						numberOfNestedItems = "./numberofitems";
					}

					//mn = mn.substring(2);
					var record, $fields, $field, name, $nestedMultiField;
					var numberOfLinks = 0;
					//operating over each multifield item of individaual multifield Ex--(number of products items)
					$fieldSets.each(function(i, fieldSet) {
						$fields = $(fieldSet).children().children(CFFW);

						record = {};
                       //operating over each field of fields inside multifield Ex--(title , image)
						$fields.each(function(j, field) {
							$field = $(field);
							//may be a nested multifield
							$nestedMultiField = $field.find("[data-init='multifield']");
							if ($nestedMultiField.length == 0) {
								fillValue($field.find("[name]"), record,$field,j);
							} else {
								name = $nestedMultiField.find("[class='coral-Form-fieldset']").data("name");
								if (!name) {
									return;
								}

								//strip ./
								name = name.substring(2);

								record[name] = getRecordFromMultiField($nestedMultiField);
							}
							//check for is random numer is inside a multi field
							if(j == 0){
							var $hiddenRandomNumber = $field.closest("div.coral-Form-fieldwrapper").siblings("input[type='hidden'][name]");
							if($hiddenRandomNumber){
								$hiddenRandomNumber.each(function(k, hiddenInput) {
									var attrName = $(hiddenInput).attr("name");

									if (!attrName) {
										return;
									}

									//strip ./
									if (attrName.indexOf("./") == 0) {
										attrName = attrName.substring(2);
									}
									if(attrName == "randomNumber"){
										multipleRandomNumbers = true;
										k = $hiddenRandomNumber.length-1;
									}
									});
							}
							}
						});

						if ($.isEmptyObject(record)) {
							return;
						}
						//add the record JSON in a hidden field as string
						$('<input />').attr('type', 'hidden')
							.attr('name', mn)
							.attr('value', JSON.stringify(record))
							.appendTo($form);


						$.each(record, function(key, value) {
							if(value != null){
							if (value.constructor == Array) {
								var length = value.length;
								$('<input />').attr('type', 'hidden')
									.attr('name', numberOfNestedItems)
									.attr('value', length)
									.appendTo($form);
							}
							var existing = $("[name='" + key + "']");
							$('<input />').attr('type', 'hidden')
								.attr('name', key)
								.attr('value', value)
								.appendTo($form);
							}
						});
						numberOfLinks++;
					});

					var numberOfItemsArray = $("[" + DATA_NUMBER_OF_ITEMS + "]");
					var noLinks;
					$.each(numberOfItemsArray, function(i, ele) {
						noLinks = $(ele).attr("data-name");
						if ($.inArray(noLinks, noLinksArr) == -1) {
							noLinksArr.push(noLinks);
						}

					});
					var numberOfItems = noLinksArr[i];
					if (numberOfItems == undefined || numberOfItems == 'undefined' || numberOfItems == '' || numberOfItems == null) {
						numberOfItems = "./numberofitems";
					}

					$('<input />').attr('type', 'hidden')
						.attr('name', numberOfItems)
						.attr('value', numberOfLinks)
						.appendTo($form);

				});
				if (!multipleRandomNumbers) {
						var $r = $("[name='./randomNumber']");
						if ($r != null) {
							var v = $r.val();
							//alert(v);
							if (typeof v === "undefined" || v.length == 0) {
								var randomNumberJS = Math.round(Math.random() * 999);
								$r.val(randomNumberJS);
							} else {
								$r.val(v);
							}
							$r.trigger('change');
						}
					}
			        multipleRandomNumbers = false;

			});
		};

		//extend otb multifield for adjusting event propagation when there are nested multifields
		//for working around the nested multifield add and reorder
		CUI.Multifield = new Class({
			toString: "Multifield",
			extend: CUI.Multifield,

			construct: function(options) {
				this.script = this.$element.find(".js-coral-Multifield-input-template:last");
			},

			_addListeners: function() {
				this.superClass._addListeners.call(this);

				//otb coral event handler is added on selector .js-coral-Multifield-add
				//any nested multifield add click events are propagated to the parent multifield
				//to prevent adding a new composite field in both nested multifield and parent multifield
				//when user clicks on add of nested multifield, stop the event propagation to parent multifield
				this.$element.on("click", ".js-coral-Multifield-add", function(e) {
					var $form1 = $(".cq-projects-admin-createproject");
					if($form1 && $form1.attr("action")=="/content/projects"){
						$(".js-coral-Multifield-move[type='button']").each(function(){ 
								$(this).remove();
							});
					}
					var itemCount=  $(this).parent().find('.js-coral-Multifield-list').children().size();
					var limit = $(this).closest('form').find("div.cq-dialog-content").attr('data-limit');
					if(limit && itemCount>limit) {
						 $(this).parent().find('.js-coral-Multifield-list').children().last().remove();
						$(window).adaptTo("foundation-ui").alert("Attention Please","You can only add a maximum of "+limit+" sections");
						return false;
					}
					var onclickFunction= $(this).closest('form').find(".coral-Multifield").attr('data-onclickadditem');
					if (typeof onclickFunction !== "undefined" && onclickFunction.length != 0) {
					var onclickJSFunction = eval("(" + onclickFunction + ")");
					if ($.isFunction(onclickJSFunction)) {
						onclickJSFunction();
					}
				}
					e.stopPropagation();
				});

				this.$element.on("drop", function(e) {
					e.stopPropagation();
				});
			}
		});

		CUI.Widget.registry.register("multifield", CUI.Multifield);
		var pageForm = $(".cq-dialog-page");
		if (pageForm != null && pageForm != undefined && pageForm.length > 0) {
			$(document).ready(function() {
				addDataInFields();
				collectDataFromFields();
			});
		} else {
			$document.on("dialog-ready", function() {
				addDataInFields();
				collectDataFromFields();
			});
		}
	})($, $(document));