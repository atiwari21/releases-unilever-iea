function formElementV2OnLoad() {
	var selectedValue=$("[name='./elementType']").val();
	formElementV2ViewType(selectedValue);
}
function formElementV2OnSelect(event) {
    formElementV2ViewType(event.selected);
}
function formElementV2ViewType(selectedValue) {
    if (!selectedValue) {
        selectedValue = 'textfield';
    }
   if (selectedValue == 'textfield') {
        $("#fieldLabelId").parent().parent().show();
		$("#fieldValueId").parent().hide();
		$("#checkedByDefaultId").parent().hide();
		$("#nameId").parent().hide();
		$("#fieldIdId").parent().show();
		$("#placeHolderValueId").parent().show();
		$("#defaultValueId").parent().show();
		$("#tooltipTextId").parent().show();
		$("#rowsId").parent().hide();
		$("#hiddenValueId").parent().hide();
		$("#dataId").parent().hide();
		$("#dataRadioId").parent().hide();
		$("#groupNameId").parent().hide();
		$("#allowMultipleFilesId").parent().parent().hide();
		$("[name='./fieldId']").attr("aria-required",true);		
		$("[aria-controls='validationId']").show();
	    $("#mandatoryFieldId").parent().show();
		$("#numberMinimumValueId").parent().hide();
		$("#numberMaximumValueId").parent().hide();
		$("#textFieldMaxLengthId").parent().show();
		$("#restrictionId").parent().hide();
		$("#fileuploadSupportedFileExtensionId").parent().hide();
		$("#fileuploadMaxFileSizeId").parent().hide();
		$("#telephoneNumMaximumLengthId").parent().hide();
		$("#dateIncludeIntervalRestrictionId").parent().hide();
		$("#validationConstraintTypeId").parent().show();
		$("#emailConstraintTypeId").parent().hide();
		$("#dateConstraintTypeId").parent().hide();
		$("#cardNumberConstraintTypeId").parent().hide();
		$("#telephoneNumberConstraintTypeId").parent().hide();
        $("#validationsValidationErrorMessageId").parent().show();	
        $("#buttonTypeId").parent().hide();
		$("#hiddenTypeId").parent().hide();
		$("#invalidCodeMessageId").parent().parent().hide();
		$("#codeExpiredMessageId").parent().parent().hide();
		$("#globalCountExceedMsgId").parent().parent().hide();
		$("#userCountExceedMsgId").parent().parent().hide();
		formElementV2ValidationsConstraintTypeOnLoad();
		formElementV2ValidationMandatoryFieldOnClick();
		}
	
	if (selectedValue == 'password') {
        $("#fieldLabelId").parent().parent().show();
		$("#fieldValueId").parent().hide();
		$("#checkedByDefaultId").parent().hide();
		$("#nameId").parent().hide();
		$("#fieldIdId").parent().show();
		$("#placeHolderValueId").parent().show();
		$("#defaultValueId").parent().hide();
		$("#tooltipTextId").parent().show();
		$("#rowsId").parent().hide();
		$("#hiddenValueId").parent().hide();
		$("#dataId").parent().hide();
		$("#groupNameId").parent().hide();
		$("#allowMultipleFilesId").parent().parent().hide();
		$("#dataRadioId").parent().hide();
		$("[name='./fieldId']").attr("aria-required",true);		
		$("[aria-controls='validationId']").show();
        $("#mandatoryFieldId").parent().show();
		$("#numberMinimumValueId").parent().hide();
		$("#numberMaximumValueId").parent().hide();
		$("#textFieldMaxLengthId").parent().show();
		$("#restrictionId").parent().hide();
		$("#fileuploadSupportedFileExtensionId").parent().hide();
		$("#fileuploadMaxFileSizeId").parent().hide();
		$("#telephoneNumMaximumLengthId").parent().hide();
		$("#dateIncludeIntervalRestrictionId").parent().hide();
		$("#validationConstraintTypeId").parent().show();
		$("#emailConstraintTypeId").parent().hide();
		$("#dateConstraintTypeId").parent().hide();
		$("#cardNumberConstraintTypeId").parent().hide();
		$("#telephoneNumberConstraintTypeId").parent().hide();
		$("#validationsValidationErrorMessageId").parent().parent().show();
        $("#buttonTypeId").parent().hide();
        $("#hiddenTypeId").parent().hide();
		$("#invalidCodeMessageId").parent().parent().hide();
		$("#codeExpiredMessageId").parent().parent().hide();
		$("#globalCountExceedMsgId").parent().parent().hide();
		$("#userCountExceedMsgId").parent().parent().hide();
		formElementV2ValidationsConstraintTypeOnLoad();
		formElementV2ValidationMandatoryFieldOnClick();
		}

	else if (selectedValue=='number') {
        $("#fieldLabelId").parent().parent().show();
		$("#fieldValueId").parent().hide();
		$("#checkedByDefaultId").parent().hide();
		$("#nameId").parent().hide();
		$("#fieldIdId").parent().show();
		$("#placeHolderValueId").parent().show();
		$("#defaultValueId").parent().show();
		$("#tooltipTextId").parent().show();
		$("#rowsId").parent().hide();
		$("#hiddenValueId").parent().hide();
		$("#dataId").parent().hide();
		$("#dataRadioId").parent().hide();
		$("#groupNameId").parent().hide();
		$("#allowMultipleFilesId").parent().parent().hide();
		$("[name='./fieldId']").attr("aria-required",true);		
		$("[aria-controls='validationId']").show();
		$("#mandatoryFieldId").parent().show();
		$("#numberMinimumValueId").parent().show();
		$("#numberMaximumValueId").parent().show();
		$("#textFieldMaxLengthId").parent().hide();
		$("#restrictionId").parent().hide();
		$("#fileuploadSupportedFileExtensionId").parent().hide();
		$("#fileuploadMaxFileSizeId").parent().hide();
		$("#telephoneNumMaximumLengthId").parent().hide();
		$("#dateIncludeIntervalRestrictionId").parent().hide();
		$("#validationConstraintTypeId").parent().hide();
		$("#emailConstraintTypeId").parent().hide();
		$("#dateConstraintTypeId").parent().hide();
		$("#cardNumberConstraintTypeId").parent().hide();
		$("#telephoneNumberConstraintTypeId").parent().hide();
		$("#validationsvalidationExpressionId").parent().hide();
		$("#validationsValidationErrorMessageId").parent().parent().show();
        $("#buttonTypeId").parent().hide();
        $("#hiddenTypeId").parent().hide();
		$("#invalidCodeMessageId").parent().parent().hide();
		$("#codeExpiredMessageId").parent().parent().hide();
		$("#globalCountExceedMsgId").parent().parent().hide();
		$("#userCountExceedMsgId").parent().parent().hide();
		formElementV2ValidationMandatoryFieldOnClick();
		}
		
	else if (selectedValue=='emailaddress') {
        $("#fieldLabelId").parent().parent().show();
		$("#fieldValueId").parent().hide();
		$("#checkedByDefaultId").parent().hide();
		$("#nameId").parent().hide();
		$("#fieldIdId").parent().show();
		$("#placeHolderValueId").parent().show();
		$("#defaultValueId").parent().show();
		$("#tooltipTextId").parent().show();
		$("#rowsId").parent().hide();
		$("#hiddenValueId").parent().hide();
		$("#dataId").parent().hide();
		$("#dataRadioId").parent().hide();
		$("#groupNameId").parent().hide();
		$("#allowMultipleFilesId").parent().parent().hide();
		$("[name='./fieldId']").attr("aria-required",true);		
		$("[aria-controls='validationId']").show();
		$("#mandatoryFieldId").parent().show();
		$("#numberMinimumValueId").parent().hide();
		$("#numberMaximumValueId").parent().hide();
		$("#textFieldMaxLengthId").parent().hide();
		$("#restrictionId").parent().hide();
		$("#fileuploadSupportedFileExtensionId").parent().hide();
		$("#fileuploadMaxFileSizeId").parent().hide();
		$("#telephoneNumMaximumLengthId").parent().hide();
		$("#dateIncludeIntervalRestrictionId").parent().hide();
		$("#validationConstraintTypeId").parent().hide();
		$("#emailConstraintTypeId").parent().show();
		$("#dateConstraintTypeId").parent().hide();
		$("#cardNumberConstraintTypeId").parent().hide();
		$("#telephoneNumberConstraintTypeId").parent().hide();
		$("#validationsValidationErrorMessageId").parent().parent().show();
        $("#buttonTypeId").parent().hide();
        $("#hiddenTypeId").parent().hide();
		$("#invalidCodeMessageId").parent().parent().hide();
		$("#codeExpiredMessageId").parent().parent().hide();
		$("#globalCountExceedMsgId").parent().parent().hide();
		$("#userCountExceedMsgId").parent().parent().hide();
		formElementV2emailConstraintTypeOnLoad();
		formElementV2ValidationMandatoryFieldOnClick();
		}
		
    else if (selectedValue=='date') {
        $("#fieldLabelId").parent().parent().show();
		$("#fieldValueId").parent().hide();
		$("#checkedByDefaultId").parent().hide();
		$("#nameId").parent().hide();
		$("#fieldIdId").parent().show();
		$("#placeHolderValueId").parent().show();
		$("#defaultValueId").parent().show();
		$("#tooltipTextId").parent().show();
		$("#rowsId").parent().hide();
		$("#hiddenValueId").parent().hide();
		$("#dataId").parent().hide();
		$("#dataRadioId").parent().hide();
		$("#groupNameId").parent().hide();
		$("#allowMultipleFilesId").parent().parent().hide();
		$("[name='./fieldId']").attr("aria-required",true);		
		$("[aria-controls='validationId']").show();
		$("#mandatoryFieldId").parent().show();
		$("#numberMinimumValueId").parent().hide();
		$("#numberMaximumValueId").parent().hide();
		$("#textFieldMaxLengthId").parent().hide();
		$("#restrictionId").parent().show();
		$("#fileuploadSupportedFileExtensionId").parent().hide();
		$("#fileuploadMaxFileSizeId").parent().hide();
		$("#telephoneNumMaximumLengthId").parent().hide();
		$("#dateIncludeIntervalRestrictionId").parent().show();
		$("#validationConstraintTypeId").parent().hide();
		$("#emailConstraintTypeId").parent().hide();
		$("#dateConstraintTypeId").parent().show();
		$("#cardNumberConstraintTypeId").parent().hide();
		$("#telephoneNumberConstraintTypeId").parent().hide();
		$("#validationsValidationErrorMessageId").parent().parent().show();
        $("#buttonTypeId").parent().hide();
        $("#hiddenTypeId").parent().hide();
		$("#invalidCodeMessageId").parent().parent().hide();
		$("#codeExpiredMessageId").parent().parent().hide();
		$("#globalCountExceedMsgId").parent().parent().hide();
		$("#userCountExceedMsgId").parent().parent().hide();
		formElementV2ValidationMandatoryFieldOnClick();
		formElementIncludeIntervalRestrictionOnClick();
		formElementV2dateConstraintTypeOnLoad();
		}
	
	else if (selectedValue=='telephonenumber') {
        $("#fieldLabelId").parent().parent().show();
		$("#fieldValueId").parent().hide();
		$("#checkedByDefaultId").parent().hide();
		$("#nameId").parent().hide();
		$("#fieldIdId").parent().show();
		$("#placeHolderValueId").parent().show();
		$("#defaultValueId").parent().show();
		$("#tooltipTextId").parent().show();
		$("#rowsId").parent().hide();
		$("#hiddenValueId").parent().hide();
		$("#dataId").parent().hide();
		$("#dataRadioId").parent().hide();
		$("#groupNameId").parent().hide();
		$("#allowMultipleFilesId").parent().parent().hide();
		$("[name='./fieldId']").attr("aria-required",true);		
		$("[aria-controls='validationId']").show();
		$("#mandatoryFieldId").parent().show();
		$("#numberMinimumValueId").parent().hide();
		$("#numberMaximumValueId").parent().hide();
		$("#textFieldMaxLengthId").parent().hide();
		$("#restrictionId").parent().hide();
		$("#fileuploadSupportedFileExtensionId").parent().hide();
		$("#fileuploadMaxFileSizeId").parent().hide();
		$("#telephoneNumMaximumLengthId").parent().show();
		$("#dateIncludeIntervalRestrictionId").parent().hide();
		$("#validationConstraintTypeId").parent().hide();
		$("#emailConstraintTypeId").parent().hide();
		$("#dateConstraintTypeId").parent().hide();
		$("#cardNumberConstraintTypeId").parent().hide();
		$("#telephoneNumberConstraintTypeId").parent().show();
		$("#validationsValidationErrorMessageId").parent().parent().show();
        $("#buttonTypeId").parent().hide();
        $("#hiddenTypeId").parent().hide();
		$("#invalidCodeMessageId").parent().parent().hide();
		$("#codeExpiredMessageId").parent().parent().hide();
		$("#globalCountExceedMsgId").parent().parent().hide();
		$("#userCountExceedMsgId").parent().parent().hide();
		formElementV2ValidationMandatoryFieldOnClick();		
		formElementV2telephoneNumberConstraintTypeOnLoad();
		
		}
	
    else if (selectedValue == 'textarea') {
		$("#fieldLabelId").parent().parent().show();
		$("#fieldValueId").parent().hide();
		$("#checkedByDefaultId").parent().hide();
		$("#nameId").parent().hide();
		$("#fieldIdId").parent().show();
		$("#placeHolderValueId").parent().show();
		$("#defaultValueId").parent().show();
		$("#tooltipTextId").parent().show();
		$("#rowsId").parent().show();
		$("#hiddenValueId").parent().hide();
		$("#dataId").parent().hide();
		$("#dataRadioId").parent().hide();
		$("#groupNameId").parent().hide();
		$("#allowMultipleFilesId").parent().parent().hide();
		$("[name='./fieldId']").attr("aria-required",true);		
		$("[aria-controls='validationId']").show();
		$("#mandatoryFieldId").parent().show();
		$("#numberMinimumValueId").parent().hide();
		$("#numberMaximumValueId").parent().hide();
		$("#textFieldMaxLengthId").parent().hide();
		$("#restrictionId").parent().hide();
		$("#fileuploadSupportedFileExtensionId").parent().hide();
		$("#fileuploadMaxFileSizeId").parent().hide();
		$("#telephoneNumMaximumLengthId").parent().hide();
		$("#dateIncludeIntervalRestrictionId").parent().hide();		
		$("#validationConstraintTypeId").parent().show();
		$("#emailConstraintTypeId").parent().hide();
		$("#dateConstraintTypeId").parent().hide();
		$("#cardNumberConstraintTypeId").parent().hide();
		$("#telephoneNumberConstraintTypeId").parent().hide();
		$("#validationsValidationErrorMessageId").parent().parent().show();
        $("#buttonTypeId").parent().hide();
        $("#hiddenTypeId").parent().hide();
		$("#invalidCodeMessageId").parent().parent().hide();
		$("#codeExpiredMessageId").parent().parent().hide();
		$("#globalCountExceedMsgId").parent().parent().hide();
		$("#userCountExceedMsgId").parent().parent().hide();
		formElementV2ValidationMandatoryFieldOnClick();
		formElementV2ValidationsConstraintTypeOnLoad();
	}
	else if (selectedValue == 'checkbox') {
		$("#fieldLabelId").parent().parent().show();
		$("#fieldValueId").parent().show();
		$("#checkedByDefaultId").parent().show();
		$("#nameId").parent().show();
		$("#fieldIdId").parent().show();
		$("#placeHolderValueId").parent().hide();
		$("#defaultValueId").parent().hide();
		$("#tooltipTextId").parent().show();
		$("#rowsId").parent().hide();
		$("#hiddenValueId").parent().hide();
		$("#dataId").parent().hide();
		$("#groupNameId").parent().hide();
		$("#dataRadioId").parent().hide();
		$("#allowMultipleFilesId").parent().parent().hide();
        $("#buttonTypeId").parent().hide();
         $("#hiddenTypeId").parent().hide();
		$("[name='./fieldId']").attr("aria-required",true);
		$("[aria-controls='validationId']").show();
		$("#mandatoryFieldId").parent().show();
		$("#numberMinimumValueId").parent().hide();
		$("#numberMaximumValueId").parent().hide();
		$("#textFieldMaxLengthId").parent().hide();
		$("#restrictionId").parent().hide();
		$("#fileuploadSupportedFileExtensionId").parent().hide();
		$("#fileuploadMaxFileSizeId").parent().hide();		
		$("#validationsValidationErrorMessageId").parent().parent().hide();		
		$("#telephoneNumMaximumLengthId").parent().hide();
		$("#dateIncludeIntervalRestrictionId").parent().hide();
		$("#validationsvalidationExpressionId").parent().hide();
		$("#validationConstraintTypeId").parent().hide();
		$("#emailConstraintTypeId").parent().hide();
		$("#dateConstraintTypeId").parent().hide();
		$("#cardNumberConstraintTypeId").parent().hide();
		$("#telephoneNumberConstraintTypeId").parent().hide();
        $("#buttonTypeId").parent().hide();
        $("#hiddenTypeId").parent().hide();
		$("#invalidCodeMessageId").parent().parent().hide();
		$("#codeExpiredMessageId").parent().parent().hide();
		$("#globalCountExceedMsgId").parent().parent().hide();
		$("#userCountExceedMsgId").parent().parent().hide();
        formElementV2ValidationMandatoryFieldOnClick();
	}
	else if (selectedValue == 'radio') {
		$("#fieldLabelId").parent().parent().hide();
		$("#fieldValueId").parent().hide();
		$("#checkedByDefaultId").parent().hide();
		$("#nameId").parent().hide();
		$("#fieldIdId").parent().hide();
		$("#placeHolderValueId").parent().hide();
		$("#defaultValueId").parent().hide();
		$("#tooltipTextId").parent().hide();
		$("#rowsId").parent().hide();
		$("#hiddenValueId").parent().hide();
		$("#dataId").parent().hide();
		$("#groupNameId").parent().show();
		$("#dataRadioId").parent().show();
		$("#allowMultipleFilesId").parent().parent().hide();
        $("#buttonTypeId").parent().hide();
        $("#hiddenTypeId").parent().hide();
		$("[name='./fieldId']").attr("aria-required",false);
		$("[aria-controls='validationId']").show();
		$("#mandatoryFieldId").parent().show();
		$("#numberMinimumValueId").parent().hide();
		$("#numberMaximumValueId").parent().hide();
		$("#textFieldMaxLengthId").parent().hide();
		$("#restrictionId").parent().hide();
		$("#fileuploadSupportedFileExtensionId").parent().hide();
		$("#fileuploadMaxFileSizeId").parent().hide();		
		$("#validationsValidationErrorMessageId").parent().parent().hide();		
		$("#telephoneNumMaximumLengthId").parent().hide();
		$("#dateIncludeIntervalRestrictionId").parent().hide();
		$("#validationsvalidationExpressionId").parent().hide();
		$("#validationConstraintTypeId").parent().hide();
		$("#emailConstraintTypeId").parent().hide();
		$("#dateConstraintTypeId").parent().hide();
		$("#cardNumberConstraintTypeId").parent().hide();
		$("#telephoneNumberConstraintTypeId").parent().hide();
        $("#buttonTypeId").parent().hide();
        $("#hiddenTypeId").parent().hide();
		$("#invalidCodeMessageId").parent().parent().hide();
		$("#codeExpiredMessageId").parent().parent().hide();
		$("#globalCountExceedMsgId").parent().parent().hide();
		$("#userCountExceedMsgId").parent().parent().hide();
        formElementV2ValidationMandatoryFieldOnClick();
		
	}
	else if (selectedValue == 'dropdown') {
		$("#fieldLabelId").parent().parent().show();
		$("#fieldValueId").parent().hide();
		$("#checkedByDefaultId").parent().hide();
		$("#nameId").parent().hide();
		$("#fieldIdId").parent().show();
		$("#placeHolderValueId").parent().hide();
		$("#defaultValueId").parent().show();
		$("#tooltipTextId").parent().show();
		$("#rowsId").parent().hide();
		$("#hiddenValueId").parent().hide();
		$("#dataId").parent().show();
		$("#groupNameId").parent().hide();
		$("#allowMultipleFilesId").parent().parent().hide();
		$("#dataRadioId").parent().hide();
		$("[name='./fieldId']").attr("aria-required",true);
		$("[aria-controls='validationId']").show();
		$("#mandatoryFieldId").parent().show();
		$("#numberMinimumValueId").parent().hide();
		$("#numberMaximumValueId").parent().hide();
		$("#textFieldMaxLengthId").parent().hide();
		$("#restrictionId").parent().hide();
		$("#fileuploadSupportedFileExtensionId").parent().hide();
		$("#fileuploadMaxFileSizeId").parent().hide();		
		$("#validationsValidationErrorMessageId").parent().parent().hide();		
		$("#telephoneNumMaximumLengthId").parent().hide();
		$("#dateIncludeIntervalRestrictionId").parent().hide();
		$("#validationsvalidationExpressionId").parent().hide();
		$("#validationConstraintTypeId").parent().hide();
		$("#emailConstraintTypeId").parent().hide();
		$("#dateConstraintTypeId").parent().hide();
		$("#cardNumberConstraintTypeId").parent().hide();
		$("#telephoneNumberConstraintTypeId").parent().hide();
        $("#buttonTypeId").parent().hide();
        $("#hiddenTypeId").parent().hide();
		$("#invalidCodeMessageId").parent().parent().hide();
		$("#codeExpiredMessageId").parent().parent().hide();
		$("#globalCountExceedMsgId").parent().parent().hide();
		$("#userCountExceedMsgId").parent().parent().hide();
		formElementV2ValidationMandatoryFieldOnClick();
		
	}
	
	else if (selectedValue == 'fileupload') {
	   $("#fieldLabelId").parent().parent().show();
		$("#fieldValueId").parent().hide();
		$("#checkedByDefaultId").parent().hide();
		$("#nameId").parent().hide();
		$("#fieldIdId").parent().show();
		$("#placeHolderValueId").parent().show();
		$("#defaultValueId").parent().show();
		$("#tooltipTextId").parent().show();
		$("#rowsId").parent().hide();
		$("#hiddenValueId").parent().hide();
		$("#dataId").parent().hide();
		$("#groupNameId").parent().hide();
		$("#dataRadioId").parent().hide();
		$("#allowMultipleFilesId").parent().parent().show();
		$("[aria-controls='validationId']").show();
		$("[name='./fieldId']").attr("aria-required",true);
		$("#mandatoryFieldId").parent().show();
		$("#numberMinimumValueId").parent().hide();
		$("#numberMaximumValueId").parent().hide();
		$("#textFieldMaxLengthId").parent().hide();
		$("#restrictionId").parent().hide();
		$("#fileuploadSupportedFileExtensionId").parent().show();
		$("#fileuploadMaxFileSizeId").parent().show();
        $("#validationsvalidationExpressionId").parent().hide();		
		$("#validationsValidationErrorMessageId").parent().parent().show();		
		$("#telephoneNumMaximumLengthId").parent().hide();
		$("#dateIncludeIntervalRestrictionId").parent().hide();
		$("#validationConstraintTypeId").parent().hide();
		$("#emailConstraintTypeId").parent().hide();
		$("#dateConstraintTypeId").parent().hide();
		$("#cardNumberConstraintTypeId").parent().hide();
        $("#buttonTypeId").parent().hide();
		$("#telephoneNumberConstraintTypeId").parent().hide();
        $("#hiddenTypeId").parent().hide();
		$("#invalidCodeMessageId").parent().parent().hide();
		$("#codeExpiredMessageId").parent().parent().hide();
		$("#globalCountExceedMsgId").parent().parent().hide();
		$("#userCountExceedMsgId").parent().parent().hide();
		formElementV2ValidationMandatoryFieldOnClick();
	}
	
	else if (selectedValue == 'cardnumber') {
	    $("#fieldLabelId").parent().parent().show();
		$("#fieldValueId").parent().hide();
		$("#checkedByDefaultId").parent().hide();
		$("#nameId").parent().hide();
		$("#fieldIdId").parent().show();
		$("#placeHolderValueId").parent().show();
		$("#defaultValueId").parent().show();
		$("#tooltipTextId").parent().show();
		$("#rowsId").parent().hide();
		$("#hiddenValueId").parent().hide();
		$("#dataId").parent().hide();
		$("#groupNameId").parent().hide();
		$("#dataRadioId").parent().hide();
		$("#allowMultipleFilesId").parent().parent().hide();
		$("[name='./fieldId']").attr("aria-required",true);
		$("[aria-controls='validationId']").show();
		$("#mandatoryFieldId").parent().show();
		$("#numberMinimumValueId").parent().hide();
		$("#numberMaximumValueId").parent().hide();
		$("#textFieldMaxLengthId").parent().hide();
		$("#restrictionId").parent().hide();
		$("#fileuploadSupportedFileExtensionId").parent().hide();
		$("#fileuploadMaxFileSizeId").parent().hide();		
		$("#validationsValidationErrorMessageId").parent().parent().show();		
		$("#telephoneNumMaximumLengthId").parent().hide();
		$("#dateIncludeIntervalRestrictionId").parent().hide();
		$("#validationConstraintTypeId").parent().hide();
		$("#emailConstraintTypeId").parent().hide();
		$("#dateConstraintTypeId").parent().hide();
		$("#cardNumberConstraintTypeId").parent().show();
		$("#telephoneNumberConstraintTypeId").parent().hide();
        $("#buttonTypeId").parent().hide();
        $("#hiddenTypeId").parent().hide();
		$("#invalidCodeMessageId").parent().parent().hide();
		$("#codeExpiredMessageId").parent().parent().hide();
		$("#globalCountExceedMsgId").parent().parent().hide();
		$("#userCountExceedMsgId").parent().parent().hide();
		formElementV2ValidationMandatoryFieldOnClick();
		formElementV2CardNumberConstraintTypeOnLoad();
	}
	
    else if (selectedValue == 'hiddenfield') {
		$("#fieldLabelId").parent().parent().hide();
		$("#fieldValueId").parent().hide();
		$("#checkedByDefaultId").parent().hide();
		$("#nameId").parent().hide();
		$("#fieldIdId").parent().show();
		$("#placeHolderValueId").parent().hide();
		$("#defaultValueId").parent().hide();
		$("#tooltipTextId").parent().hide();
		$("#rowsId").parent().hide();
		$("#hiddenValueId").parent().show();
		$("#dataId").parent().hide();
		$("#groupNameId").parent().hide();
		$("#dataRadioId").parent().hide();
		$("#allowMultipleFilesId").parent().parent().hide();
		$("[aria-controls='validationId']").hide();
        $("#buttonTypeId").parent().hide();
        $("#hiddenTypeId").parent().show();
		$("[name='./fieldId']").attr("aria-required",true);
		$("#invalidCodeMessageId").parent().parent().hide();
		$("#codeExpiredMessageId").parent().parent().hide();
		$("#globalCountExceedMsgId").parent().parent().hide();
		$("#userCountExceedMsgId").parent().parent().hide();
	}
    else if (selectedValue == 'formbutton') {
		$("#fieldLabelId").parent().parent().show();
		$("#fieldValueId").parent().hide();
		$("#checkedByDefaultId").parent().hide();
		$("#nameId").parent().show();
		$("#fieldIdId").parent().hide();
		$("#placeHolderValueId").parent().hide();
		$("#defaultValueId").parent().hide();
		$("#tooltipTextId").parent().hide();
		$("#rowsId").parent().hide();
		$("#hiddenValueId").parent().hide();
		$("#dataId").parent().hide();
		$("#groupNameId").parent().hide();
		$("#dataRadioId").parent().hide();
        $("#buttonTypeId").parent().show();
		$("#allowMultipleFilesId").parent().parent().hide();
         $("#hiddenTypeId").parent().hide();
		$("[aria-controls='validationId']").hide();
		$("[name='./fieldId']").attr("aria-required",false);
		$("#invalidCodeMessageId").parent().parent().hide();
		$("#codeExpiredMessageId").parent().parent().hide();
		$("#globalCountExceedMsgId").parent().parent().hide();
		$("#userCountExceedMsgId").parent().parent().hide();
	}
    else if (selectedValue == 'captcha'||selectedValue == 'fieldsetStart' ||selectedValue == 'fieldsetEnd') {
		$("#fieldLabelId").parent().parent().hide();
		$("#fieldValueId").parent().hide();
		$("#checkedByDefaultId").parent().hide();
		$("#nameId").parent().hide();
		$("#fieldIdId").parent().hide();
		$("#placeHolderValueId").parent().hide();
		$("#defaultValueId").parent().hide();
		$("#tooltipTextId").parent().hide();
		$("#rowsId").parent().hide();
		$("#hiddenValueId").parent().hide();
		$("#dataId").parent().hide();
		$("#groupNameId").parent().hide();
		$("#dataRadioId").parent().hide();
		$("#allowMultipleFilesId").parent().parent().hide();
        $("#buttonTypeId").parent().hide();
         $("#hiddenTypeId").parent().hide();
		$("[aria-controls='validationId']").hide();
		$("[name='./fieldId']").attr("aria-required",false);
		$("#invalidCodeMessageId").parent().parent().hide();
		$("#codeExpiredMessageId").parent().parent().hide();
		$("#globalCountExceedMsgId").parent().parent().hide();
		$("#userCountExceedMsgId").parent().parent().hide();
	}
	else if (selectedValue == 'couponcode') {
        $("#fieldLabelId").parent().parent().show();
		$("#fieldValueId").parent().hide();
		$("#checkedByDefaultId").parent().hide();
		$("#nameId").parent().hide();
		$("#fieldIdId").parent().show();
		$("#placeHolderValueId").parent().show();
		$("#defaultValueId").parent().show();
		$("#tooltipTextId").parent().show();
		$("#rowsId").parent().hide();
		$("#hiddenValueId").parent().hide();
		$("#dataId").parent().hide();
		$("#dataRadioId").parent().hide();
		$("#groupNameId").parent().hide();
		$("#allowMultipleFilesId").parent().parent().hide();
		$("[name='./fieldId']").attr("aria-required",true);		
		$("[aria-controls='validationId']").show();
	    $("#mandatoryFieldId").parent().show();
		$("#numberMinimumValueId").parent().hide();
		$("#numberMaximumValueId").parent().hide();
		$("#textFieldMaxLengthId").parent().hide();
		$("#restrictionId").parent().hide();
		$("#fileuploadSupportedFileExtensionId").parent().hide();
		$("#fileuploadMaxFileSizeId").parent().hide();
		$("#telephoneNumMaximumLengthId").parent().hide();
		$("#dateIncludeIntervalRestrictionId").parent().hide();
		$("#validationConstraintTypeId").parent().hide();
		$("#emailConstraintTypeId").parent().hide();
		$("#dateConstraintTypeId").parent().hide();
		$("#cardNumberConstraintTypeId").parent().hide();
		$("#telephoneNumberConstraintTypeId").parent().hide();
		$("#validationsvalidationExpressionId").parent().hide();
        $("#validationsValidationErrorMessageId").parent().parent().hide();
        $("#buttonTypeId").parent().hide();
		$("#hiddenTypeId").parent().hide();
		$("#invalidCodeMessageId").parent().parent().show();
		$("#codeExpiredMessageId").parent().parent().show();
		$("#globalCountExceedMsgId").parent().parent().show();
		$("#userCountExceedMsgId").parent().parent().show();
		formElementV2ValidationMandatoryFieldOnClick();
		}
}

function formElementV2CardNumberConstraintTypeOnLoad(selectedValue) {
	if (selectedValue==null || selectedValue=="undefined" ||selectedValue==""){
		 var constraintExpression = $('#cardNumberConstraintTypeId :selected').val();
		 
    formElementV2CardNumberConstraintType(constraintExpression);
	}
}
function formElementV2CardNumberConstraintTypeOnSelect(event) {
    formElementV2CardNumberConstraintType(event.selected);
}
function formElementV2CardNumberConstraintType(selectedValue) {
    if(selectedValue==null || selectedValue=="undefined" ||selectedValue=="") {		
        $("#validationsvalidationExpressionId").parent().show();		
		}
	else{
	$("#validationsvalidationExpressionId").parent().hide();		
	}
}

function formElementV2ValidationsConstraintTypeOnLoad(selectedValue) {
	if(selectedValue==null || selectedValue=="undefined" ||selectedValue=="") {
	var constraintExpression = $('#validationConstraintTypeId :selected').val();
    formElementV2ValidationsConstraintType(constraintExpression);
	}
}
function formElementV2ValidationsConstraintTypeOnSelect(event) {
    formElementV2ValidationsConstraintType(event.selected);
}
function formElementV2ValidationsConstraintType(selectedValue) {
    if(selectedValue==null || selectedValue=="undefined" ||selectedValue=="") {	
        $("#validationsvalidationExpressionId").parent().show();		
		}
	else{
	$("#validationsvalidationExpressionId").parent().hide();
	}
}

function formElementV2textFieldConstraintTypeOnLoad(selectedValue) {
     if (selectedValue==null || selectedValue=="undefined" ||selectedValue=="") {
        var constraintExpression = $('#textFieldConstraintTypeId :selected').val();
         formElementV2textFieldConstraintType(constraintExpression);}
}
function formElementV2textFieldConstraintTypeOnSelect(event) {
    formElementV2textFieldConstraintType(event.selected);
}
function formElementV2textFieldConstraintType(selectedValue) {
    if (selectedValue==null || selectedValue=="undefined" ||selectedValue=="") {
        $("#validationsvalidationExpressionId").parent().show();
		}
	else{
	$("#validationsvalidationExpressionId").parent().hide();
	}

}

function formElementV2dateConstraintTypeOnLoad(selectedValue) {
    if (selectedValue==null || selectedValue=="undefined" ||selectedValue=="") {
		var constraintExpression = $('#dateConstraintTypeId :selected').val();
	formElementV2dateConstraintType(constraintExpression);
	}
}
function formElementV2dateConstraintTypeOnSelect(event) {
    formElementV2dateConstraintType(event.selected);
}

function formElementV2dateConstraintType(selectedValue) {
    if (selectedValue==null || selectedValue=="undefined" ||selectedValue=="") {	
        $("#validationsvalidationExpressionId").parent().show();		
		}
	else{
	$("#validationsvalidationExpressionId").parent().hide();
	}
}


function formElementV2emailConstraintTypeOnLoad(selectedValue) {
    if (selectedValue==null || selectedValue=="undefined" ||selectedValue==""){
	var constraintExpression = $('#emailConstraintTypeId :selected').val();
	formElementV2emailConstraintType(constraintExpression);
 }
}
function formElementV2emailConstraintTypeOnSelect(event) {
    formElementV2emailConstraintType(event.selected);
}

function formElementV2emailConstraintType(selectedValue) {
    if (selectedValue==null || selectedValue=="undefined" ||selectedValue=="") { 	
        $("#validationsvalidationExpressionId").parent().show();		
		}
	else{
	$("#validationsvalidationExpressionId").parent().hide();
	}
}

function formElementV2telephoneNumberConstraintTypeOnLoad(selectedValue) {
	if (selectedValue==null || selectedValue=="undefined" ||selectedValue==""){
		var constraintExpression = $('#telephoneNumberConstraintTypeId :selected').val();
    formElementV2telephoneNumberConstraintType(constraintExpression);
	}
}
function formElementV2telephoneNumberConstraintTypeOnSelect(event) {
    formElementV2telephoneNumberConstraintType(event.selected);
	
}

function formElementV2telephoneNumberConstraintType(selectedValue) {
	var telephoneNumberConstraintTypeId = document.getElementById("telephoneNumberConstraintTypeId");
    var selectedText =$('#telephoneNumberConstraintTypeId option:selected').text();
	if (selectedText == 'Regular expression') {		
        $("#validationsvalidationExpressionId").parent().show();
		$("#telephoneNumMaximumLengthId").parent().hide();		
		}
	else if(selectedText == 'Numbers only'){
		$("#validationsvalidationExpressionId").parent().hide();
		$("#telephoneNumMaximumLengthId").parent().show();
	}
	else{
	$("#validationsvalidationExpressionId").parent().hide();
	$("#telephoneNumMaximumLengthId").parent().hide();
	}
}

function formElementV2ValidationMandatoryFieldOnClick() {
    /*var mandatoryField = document.getElementById('mandatoryFieldId');
    if (mandatoryField.checked) {
        $('#validationsRequiredErrorMessageId').parent().parent().show();
    } 
	else {
       $('#validationsRequiredErrorMessageId').parent().parent().hide();
    }*/
	$('#validationsRequiredErrorMessageId').parent().parent().show();
}


function formElementIncludeIntervalRestrictionOnClick(){
 var dateIncludeIntervalRestriction = document.getElementById('dateIncludeIntervalRestrictionId');
    if (dateIncludeIntervalRestriction.checked) {
        $('#restrictionId').parent().show();
    } 
	else {
        $('#restrictionId').parent().hide();
    }	
}