//recipe listing  listener before submit
function recipeListingOnSubmit(){
	 $(document).on("click", ".cq-dialog-submit", function (event) {
       var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');
		 if(formName && formName=='recipeListing') {
           
		 var recipeListingcomponentUsageContextValue = $('#componentUsageContextId :selected').val();
		    if (recipeListingcomponentUsageContextValue == "fixedList" || !recipeListingcomponentUsageContextValue) {
		    if($(".coral-Multifield-list li [name='./recipe']").length==0){
          $(window).adaptTo("foundation-ui").alert("Required","Please configure at least one recipe path");
          event.preventDefault();
          return false;
    	}

		    $('input[type="text"][name="./recipePath"]').each(function() {
				var urlVal = $(this).val();
				if (urlVal.length == 0) {
					$(window).adaptTo("foundation-ui").alert("Required", "Please Configure recipe");
					event.preventDefault();
					return false;
				} else {
					return true;
				}
			});
	 }
       }
	 });
}

// Show or Hide Include In Display Section
function showHideIncludeInDisplaySectionRecipe() {  
        if (document.getElementById('isOverrideGlobalConfig').checked) {
            $("#includeInDisplaySection").show(); //displaying the include In Display Section
			$("#limitResultsToSection").show();
			$("#dietaryAttributesForDisplaySection").show();
            $("#recipeAttributesForDisplaySection").show();
			$("#sortingOptionsEnabledSection").show();

        } else {
            $("#includeInDisplaySection").hide();
			$("#limitResultsToSection").hide();
			$("#dietaryAttributesForDisplaySection").hide();
            $("#recipeAttributesForDisplaySection").hide();
			$("#sortingOptionsEnabledSection").hide();
        }
}

//recipe listing listener
function recipeListingOnSelect() { 

  var recipeComponentUsageContextValue = $('#componentUsageContextId :selected').val();
  if (recipeComponentUsageContextValue == "categoryOrLandingPages" || !recipeComponentUsageContextValue) {
  	
  	 $("[aria-controls='tagRelatedFieldId']").hide();
	 $("[aria-controls='fixedListTab']").hide();
     $("[aria-controls='rmsSearchConfigurationSection']").show();
		
		
	} else if(recipeComponentUsageContextValue == "productDetailsPage" || !recipeComponentUsageContextValue) {
		 $("[aria-controls='tagRelatedFieldId']").show();
		 $("[aria-controls='fixedListTab']").hide();
		 $("[aria-controls='rmsSearchConfigurationSection']").hide();
		 
		 
  }	else if(recipeComponentUsageContextValue == "fixedList" || !recipeComponentUsageContextValue){
		 $("[aria-controls='tagRelatedFieldId']").hide();
		 $("[aria-controls='fixedListTab']").show();
		 $("[aria-controls='rmsSearchConfigurationSection']").hide();
		 
		 
  } else {
		 $("[aria-controls='tagRelatedFieldId']").hide();
		 $("[aria-controls='fixedListTab']").hide();
		 $("[aria-controls='rmsSearchConfigurationSection']").hide();
		 
		 
  }
}

//recipe attribute
function recipeAttributes(event){
	if (document.getElementById('overrideGlobalConfigLimit').checked) {
    	$("#attributesForDisplay").show();                                                       
    }else{
    	$("#attributesForDisplay").hide();
    }
}

//recipe dietary attribute
function recipeDietaryAttributes(event){
	if (document.getElementById('overrideGlobalConfigLimit').checked) {
    	$("#dietaryAttributesForDisplay").show();                                                       
    }else{
    	$("#dietaryAttributesForDisplay").hide();
     }
}

//recipe hero on click
function recipeHeroVideoOnSelect(event){
	if (document.getElementById('recipeVideoId').checked) {
		$("[aria-controls='videoControls']").show();                                                    
    }else{
		$("[aria-controls='videoControls']").hide();
    }
}
