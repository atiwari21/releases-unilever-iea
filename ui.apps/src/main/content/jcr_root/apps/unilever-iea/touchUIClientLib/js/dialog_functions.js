function autoIFrameSize(){
   if ($('#autoIFrameSizeCheckbox').is(':checked')) {
       $("input[data-eaem-iframewidth]").parent().hide();
       $("input[data-eaem-iframeheight]").parent().hide();
   }
   else{
		$("input[data-eaem-iframewidth]").parent().show();
        $("input[data-eaem-iframeheight]").parent().show();
   }
}
function enableRestriction(){
    if ($('#enableIframeRestriction').is(':checked')) {
		var enableIframeRestrictionParent = $('#enableIframeRestriction').parent().parent('div');
        $(enableIframeRestrictionParent.parent('div')).children("div").show();
    }
    else{
		var enableIframeRestrictionParent = $('#enableIframeRestriction').parent().parent('div');
        $(enableIframeRestrictionParent.parent('div')).children("div").hide();
		enableIframeRestrictionParent.show();
    }
}
function applyAllRestriction(){
    var applyAllParent = $('#applyAllRestriction').parent().parent('div');
    var applyAllCheckbox = $(applyAllParent.parent('div')).children("div").find("input[type='checkbox']");
    if ($('#applyAllRestriction').is(':checked')) {
        applyAllCheckbox.prop("checked",true);
        applyAllCheckbox.prop("disabled",true);
        $('#applyAllRestriction').prop("disabled",false);
        $('#enableIframeRestriction').prop("disabled",false);
    }
    else{
        applyAllCheckbox.prop("disabled",false);
    }
}
function searchConfigurationOnSelect(){
    if (document.getElementById('hl').checked) {
    $("#hlfl").parent().show();
                $("#hlSimplePre").parent().show();
                $("#hlSimplePost").parent().show();
}
else{
    $("#hlfl").parent().hide();
                $("#hlSimplePre").parent().hide();
                $("#hlSimplePost").parent().hide();

}
}
function searchConfigurationOnLoad() {    

    searchConfigurationOnSelect();

}

//related Products listener
function RelatedProductBaseOnSelect() {

    var relatedProductbuildListValue = $('#buildList :selected').val();
    if (relatedProductbuildListValue == "Fixed List" || !relatedProductbuildListValue) {
    	
    	 $("[aria-controls='configureTagsTab']").hide();
         $("[aria-controls='selectProductTab']").show();
 		$("input[type='text'][name='./productPath']").attr('aria-required', 'true');
 		
		
	} else {
		 $("[aria-controls='configureTagsTab']").show();
         $("[aria-controls='selectProductTab']").hide();
 		$("input[type='text'][name='./productPath']").attr('aria-required', 'false');
 		
 		  

    }
    addToolTipToMultiBuyConfig('multiBuyConfiguration');
}

//body copy steps listener before submit
function RelatedProductBaseOnSubmit(){
	 $(document).on("click", ".cq-dialog-submit", function (event) {
         var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');
		 if(formName && formName=='relatedProducts') {
		 var relatedProductbuildListValue = $('#buildList :selected').val();
		    if (relatedProductbuildListValue == "Fixed List" || !relatedProductbuildListValue) {
		    if($(".coral-Multifield-list li [name='./productPath']").length==0){
            $(window).adaptTo("foundation-ui").alert("Required","Please configure at least one product path");
            event.preventDefault();
            return false;
      	}

	 
	 }
         }
	 });

}



function instaceTypeOnSelect() {
    if ($('#instanceId :selected').val() == "normal") {
        $('#isTakeOverContentActiveId').parent().parent().hide();
       } 
    else if ($('#instanceId :selected').val() == "takeover") {
       $('#isTakeOverContentActiveId').parent().parent().show();
    } 
}
function heroImageVideoTypeOnSelect(){
    if ($('#heroImageVideoId :selected').val() == "imageType"){
        $("[name='./herovideoId']").attr("aria-required",false);
        $('#heroVideoId').parent().hide();
        
        $("[name='./heroPreviewImageUrl']").attr("aria-required",false);
        $('#heroPreviewImageUrlId').parent().hide();
        $("[name='./imageUrl']").attr("aria-required",true);
        $('#ImageUrlId').parent().show();
		$('#videoType').parent().hide();
    }
    else {
        $("[name='./videoId']").attr("aria-required",false);
        $('#videoId').parent().hide();
        $('#smallImageUrlId').parent().hide();
        $("[name='./previewImageUrl']").attr("aria-required",false);
        $('#previewImageUrlId').parent().hide();
        $('#ImageUrlId').parent().hide();
        $("[name='./imageUrl']").attr("aria-required",false);
        $("[name='./videoId']").attr("aria-required",false);
        $('#heroVideoId').parent().show();
        $('#heroPreviewImageUrlId').parent().show();
        $("[name='./herovideoId']").attr("aria-required",true);
        $("[name='./heroPreviewImageUrl']").attr("aria-required",true);
		$('#videoType').parent().show();
    }

}


function heroInstanceTypeOnLoad() {    

    instaceTypeOnSelect();
    heroImageVideoTypeOnSelect();
}

function adaptiveImageOnload(){
 	adaptiveImageIsNotAdaptiveOnClick();
 	adaptiveImageIsCroppingRequiredOnClick();
}

function adaptiveImageV2OnLoad(){
 	adaptiveImageOverrideGlobalConfigClick();
}

function adaptiveImageOverrideGlobalConfigClick(){
if (document.getElementById('overrideGlobalConfig').checked) {
    $("#enableZoomCheck").parent().show();                                                       
    }else{
    $("#enableZoomCheck").parent().hide();
	}
}

function adaptiveImageIsNotAdaptiveOnClick() {
    if (document.getElementById('isnotAdaptive').checked) {
        $('#iscroppingReq').parent().hide();
        $("[aria-controls='croppingdetails']").hide(); //hide the cropping details tab on checking the "Adaptive behavior not required" checkbox
        $("[aria-controls='croppingdetails']").prop('disabled', true);
    } else {
        $('#iscroppingReq').parent().show();
        if (document.getElementById('iscroppingReq').checked) {
            $("[aria-controls='croppingdetails']").prop('disabled', false);
            $("[aria-controls='croppingdetails']").show(); //displaying the cropping details tab when the "cropping required" checkbox is checked
        } else {
            $("[aria-controls='croppingdetails']").prop('disabled', true);
            $("[aria-controls='croppingdetails']").hide();
        }
    }
}

function adaptiveImageIsCroppingRequiredOnClick() {
    if (!document.getElementById('isnotAdaptive').checked) {
        if (document.getElementById('iscroppingReq').checked) {
            $("[aria-controls='croppingdetails']").prop('disabled', false);
            $("[aria-controls='croppingdetails']").show(); //displaying the cropping details tab

            //on dialog load,hiding all the fields of the "Cropping Detail" tab except the main check boxes
            $('#chooseCroppingOption').parent().hide();
            $('#selectCropForTabPortrait').parent().hide();
            $('#selectCropForTabLandscape').parent().hide();
            $('#chooseOptionsForMobilePotrait').parent().hide();
            $('#chooseOptionsForMobileLandscape').parent().hide();

            $('#cropOptionsWeb').hide();
            $('#customCropValuesWeb').hide();
            $('#cropOptionsTabletPotrait').hide();
            $('#customCropValuesTabletPotrait').hide();
            $('#cropOptionsTabletLandscape').hide();
            $('#customCropValuesTabletLandscape').hide();
            $('#cropOptionsMobilePotrait').hide();
            $('#customCropValuesMobilePotrait').hide();
            $('#cropOptionsMobileLandscape').hide();
            $('#customCropValuesMobileLandscape').hide();

            //------------------------executing handlers on dialog load---------------------------
            if (document.getElementById('requiredForWeb').checked) {
                $('#chooseCroppingOption').find('select').prop('disabled', false)
                $('#chooseCroppingOption').parent().show();

                //this takes the function property(which is stored as a node property) from the "Choose Cropping Options" drop down node and executes that function
                var choosrCroppingFunc = $('#chooseCroppingOption').attr('data-onSelectFunc');
                var func = eval("(" + choosrCroppingFunc + ")");
                func();
            } else {
                $('#chooseCroppingOption').parent().hide();
                $('#chooseCroppingOption').find('select').prop('disabled', true)
                $('#cropOptionsWeb').hide();
                $('#customCropValuesWeb').hide();
            }
            if (document.getElementById('reqForTabPotrait').checked) {
                $('#selectCropForTabPortrait').parent().show();
                //this takes the function property(which is stored as a node property) from the "Choose Tablet Portrait Cropping Options" drop down node and executes that function
                var selectCropForTabPortraitFunc = $('#selectCropForTabPortrait').attr('data-onSelectFunc');
                var func = eval("(" + selectCropForTabPortraitFunc + ")");
                func();
            } else {
                $('#selectCropForTabPortrait').parent().hide();
                $('#cropOptionsTabletPotrait').hide();
                $('#customCropValuesTabletPotrait').hide();
            }
            if (document.getElementById('isReqCropForTabLandscape').checked) {
                $('#selectCropForTabLandscape').parent().show();
                //this takes the function property(which is stored as a node property) from the "Choose Tablet Landscape Cropping Options" drop down node and executes that function
                var selectCropForTabLandscapeFunc = $('#selectCropForTabLandscape').attr('data-onSelectFunc');
                var func = eval("(" + selectCropForTabLandscapeFunc + ")");
                func();
            } else {
                $('#selectCropForTabLandscape').parent().hide();
                $('#cropOptionsTabletLandscape').hide();
                $('#customCropValuesTabletLandscape').hide();
            }

            if (document.getElementById('isReqMobilePotrait').checked) {
                $('#chooseOptionsForMobilePotrait').parent().show();
                //this takes the function property(which is stored as a node property) from the "Choose Mobile Potrait Cropping Options" drop down node and executes that function
                var chooseOptionsForMobilePotraitFunc = $('#chooseOptionsForMobilePotrait').attr('data-onSelectFunc');
                var func = eval("(" + chooseOptionsForMobilePotraitFunc + ")");
                func();
            } else {
                $('#chooseOptionsForMobilePotrait').parent().hide();
                $('#cropOptionsMobilePotrait').hide();
                $('#customCropValuesMobilePotrait').hide();
            }

            if (document.getElementById('isReqMobileLandscape').checked) {
                $('#chooseOptionsForMobileLandscape').parent().show();
                //this takes the function property(which is stored as a node property) from the "Choose Mobile Landscape Cropping Options" drop down node and executes that function
                var chooseOptionsForMobileLandscapeFunc = $('#chooseOptionsForMobileLandscape').attr('data-onSelectFunc');
                var func = eval("(" + chooseOptionsForMobileLandscapeFunc + ")");
                func();
            } else {
                $('#chooseOptionsForMobileLandscape').parent().hide();
                $('#cropOptionsMobileLandscape').hide();
                $('#customCropValuesMobileLandscape').hide();
            }

            //-------------------------------------------------------------------------------------
            //-------clicking on the "Required For Web" check box---------------------
            document.getElementById('requiredForWeb').onclick = function() {

                if (document.getElementById('requiredForWeb').checked) {
                    $('#chooseCroppingOption').find('select').prop('disabled', false)
                    $('#chooseCroppingOption').parent().show();

                    //this takes the function property(which is stored as a node property) from the "Choose Cropping Options" drop down node and executes that function
                    var choosrCroppingFunc = $('#chooseCroppingOption').attr('data-onSelectFunc');
                    var func = eval("(" + choosrCroppingFunc + ")");
                    func();
                } else {
                    $('#chooseCroppingOption').parent().hide();
                    $('#chooseCroppingOption').find('select').prop('disabled', true)
                    $('#cropOptionsWeb').hide();
                    $('#customCropValuesWeb').hide();
                }
            }

            //--------------------Clicking on the "Required for Table Potrait" checkbox---------------------
            document.getElementById('reqForTabPotrait').onclick = function() {
                if (document.getElementById('reqForTabPotrait').checked) {
                    $('#selectCropForTabPortrait').parent().show();
                    //this takes the function property(which is stored as a node property) from the "Choose Tablet Portrait Cropping Options" drop down node and executes that function
                    var selectCropForTabPortraitFunc = $('#selectCropForTabPortrait').attr('data-onSelectFunc');
                    var func = eval("(" + selectCropForTabPortraitFunc + ")");
                    func();
                } else {
                    $('#selectCropForTabPortrait').parent().hide();
                    $('#cropOptionsTabletPotrait').hide();
                    $('#customCropValuesTabletPotrait').hide();
                }
            }

            //--------clicking on the "Required for tablet Landscape" check box----------------
            document.getElementById('isReqCropForTabLandscape').onclick = function() {
                if (document.getElementById('isReqCropForTabLandscape').checked) {
                    $('#selectCropForTabLandscape').parent().show();
                    //this takes the function property(which is stored as a node property) from the "Choose Tablet Landscape Cropping Options" drop down node and executes that function
                    var selectCropForTabLandscapeFunc = $('#selectCropForTabLandscape').attr('data-onSelectFunc');
                    var func = eval("(" + selectCropForTabLandscapeFunc + ")");
                    func();
                } else {
                    $('#selectCropForTabLandscape').parent().hide();
                    $('#cropOptionsTabletLandscape').hide();
                    $('#customCropValuesTabletLandscape').hide();
                }
            }

            //------------------clicking on the "Required for Mobile Potrait" checkbox---------------------
            document.getElementById('isReqMobilePotrait').onclick = function() {
                if (document.getElementById('isReqMobilePotrait').checked) {
                    $('#chooseOptionsForMobilePotrait').parent().show();
                    //this takes the function property(which is stored as a node property) from the "Choose Mobile Potrait Cropping Options" drop down node and executes that function
                    var chooseOptionsForMobilePotraitFunc = $('#chooseOptionsForMobilePotrait').attr('data-onSelectFunc');
                    var func = eval("(" + chooseOptionsForMobilePotraitFunc + ")");
                    func();
                } else {
                    $('#chooseOptionsForMobilePotrait').parent().hide();
                    $('#cropOptionsMobilePotrait').hide();
                    $('#customCropValuesMobilePotrait').hide();
                }
            }

            //--------------clicking on the "Required for Mobile Landscape" checkbox---------------------
            document.getElementById('isReqMobileLandscape').onclick = function() {
                if (document.getElementById('isReqMobileLandscape').checked) {
                    $('#chooseOptionsForMobileLandscape').parent().show();
                    //this takes the function property(which is stored as a node property) from the "Choose Mobile Landscape Cropping Options" drop down node and executes that function
                    var chooseOptionsForMobileLandscapeFunc = $('#chooseOptionsForMobileLandscape').attr('data-onSelectFunc');
                    var func = eval("(" + chooseOptionsForMobileLandscapeFunc + ")");
                    func();
                } else {
                    $('#chooseOptionsForMobileLandscape').parent().hide();
                    $('#cropOptionsMobileLandscape').hide();
                    $('#customCropValuesMobileLandscape').hide();
                }
            }
        } else {
            $("[aria-controls='croppingdetails']").hide();
            $("[aria-controls='croppingdetails']").prop('disabled', true);
        }
    }
}

function adaptiveImageSelectWebCropOptionOnSelect() {
    //this check is to display the fields when the drop down is displayed(by clicking on the chekbox above it)
    if ($('#chooseCroppingOption').parent().show()) {
        adaptiveImageSelectWebCropOption();
    }
    //this function is to display the fields when the drop down is used
    document.getElementById('chooseCroppingOption').onclick = function() {
        adaptiveImageSelectWebCropOption();
    }
}

function adaptiveImageSelectWebCropOption() {
    if ($('#chooseCroppingOption :selected').val() == "defSelCrop") {
        $('#cropOptionsWeb').show();
        $('#customCropValuesWeb').hide();
    } else if ($('#chooseCroppingOption :selected').val() == "custom") {
        $('#cropOptionsWeb').hide();
        $('#customCropValuesWeb').show();
    } else {
        $('#cropOptionsWeb').hide();
        $('#customCropValuesWeb').hide();
    }
}

function adaptiveImageSelectTabPCropOptionOnSelect() {
    //this check is to display the fields when the drop down is displayed(by clicking on the chekbox above it)
    if ($('#selectCropForTabPortrait').parent().show()) {
        adaptiveImageSelectTabPCropOption();
    }
    //this function is to display the fields when the drop down is used
    document.getElementById('selectCropForTabPortrait').onclick = function() {
        adaptiveImageSelectTabPCropOption();
    }
}

function adaptiveImageSelectTabPCropOption() {
    if ($('#selectCropForTabPortrait :selected').val() == "defSelCrop") {
        $('#cropOptionsTabletPotrait').show();
        $('#customCropValuesTabletPotrait').hide();
    } else if ($('#selectCropForTabPortrait :selected').val() == "custom") {
        $('#cropOptionsTabletPotrait').hide();
        $('#customCropValuesTabletPotrait').show();
    } else {
        $('#cropOptionsTabletPotrait').hide();
        $('#customCropValuesTabletPotrait').hide();
    }
}

function adaptiveImageSelectTabLCropOptionOnSelect() {
    //this check is to display the fields when the drop down is displayed(by clicking on the chekbox above it)
    if ($('#selectCropForTabLandscape').parent().show()) {
        adaptiveImageSelectTabLCropOption();
    }
    //this function is to display the fields when the drop down is used
    document.getElementById('selectCropForTabLandscape').onclick = function() {
        adaptiveImageSelectTabLCropOption();
    }
}

function adaptiveImageSelectTabLCropOption() {
    if ($('#selectCropForTabLandscape :selected').val() == "defSelCrop") {
        $('#cropOptionsTabletLandscape').show();
        $('#customCropValuesTabletLandscape').hide();
    } else if ($('#selectCropForTabLandscape :selected').val() == "custom") {
        $('#cropOptionsTabletLandscape').hide();
        $('#customCropValuesTabletLandscape').show();
    } else {
        $('#cropOptionsTabletLandscape').hide();
        $('#customCropValuesTabletLandscape').hide();
    }
}

function adaptiveImageSelectMobPCropOptionOnSelect() {
    //this check is to display the fields when the drop down is displayed(by clicking on the chekbox above it)
    if ($('#chooseOptionsForMobilePotrait').parent().show()) {
        adaptiveImageSelectMobPCropOption();
    }
    //this function is to display the fields when the drop down is used
    document.getElementById('chooseOptionsForMobilePotrait').onclick = function() {
        adaptiveImageSelectMobPCropOption();
    }
}

function adaptiveImageSelectMobPCropOption() {
    if ($('#chooseOptionsForMobilePotrait :selected').val() == "defSelCrop") {
        $('#cropOptionsMobilePotrait').show();
        $('#customCropValuesMobilePotrait').hide();
    } else if ($('#chooseOptionsForMobilePotrait :selected').val() == "custom") {
        $('#cropOptionsMobilePotrait').hide();
        $('#customCropValuesMobilePotrait').show();
    } else {
        $('#cropOptionsMobilePotrait').hide();
        $('#customCropValuesMobilePotrait').hide();
    }
}

function adaptiveImageSelectMobLCropOptionOnSelect() {
    //this check is to display the fields when the drop down is displayed(by clicking on the chekbox above it)
    if ($('#chooseOptionsForMobileLandscape').parent().show()) {
        adaptiveImageSelectMobLCropOption();
    }
    //this function is to display the fields when the drop down is used
    document.getElementById('chooseOptionsForMobileLandscape').onclick = function() {
        adaptiveImageSelectMobLCropOption();
    }
}

function adaptiveImageSelectMobLCropOption() {
    if ($('#chooseOptionsForMobileLandscape :selected').val() == "defSelCrop") {
        $('#cropOptionsMobileLandscape').show();
        $('#customCropValuesMobileLandscape').hide();
    } else if ($('#chooseOptionsForMobileLandscape :selected').val() == "custom") {
        $('#cropOptionsMobileLandscape').hide();
        $('#customCropValuesMobileLandscape').show();
    } else {
        $('#cropOptionsMobileLandscape').hide();
        $('#customCropValuesMobileLandscape').hide();
    }
}

function contentResultsGridOnLoad() {
    var onSortingRequiredCheckFunc = $('#isSortingRequiredContentResults').attr('data-onClick');
    if (typeof onSortingRequiredCheckFunc !== "undefined" && onSortingRequiredCheckFunc.length != 0) {
        var func = eval("(" + onSortingRequiredCheckFunc + ")");
        func();
    }

    var buildGridUsingFunc = $('#buildGridUsing').attr('data-onSelectFunc');
    if (typeof buildGridUsingFunc !== "undefined" && buildGridUsingFunc.length != 0) {
        var func = eval("(" + buildGridUsingFunc + ")");
        func();
    }
    var isLazyLoadRequiredFunc = $('#isLazyLoadRequired').attr('data-onClick');
    if (typeof isLazyLoadRequiredFunc !== "undefined" && isLazyLoadRequiredFunc.length != 0) {
        var func = eval("(" + isLazyLoadRequiredFunc + ")");
        func();
    }
}

function contentResultsGridSortingRequiredOnClick() {
    var isSortingRequiredContentResults = document.getElementById('isSortingRequiredContentResults');
    if (isSortingRequiredContentResults.checked) {
        $('#selectSortOptionDefaultDisplayValue').parent().show();
        $('#configureSortOptions').parent().show();
    } else {
        $('#selectSortOptionDefaultDisplayValue').parent().hide();
        $('#configureSortOptions').parent().hide();
    }
}

function contentResultsGridListFromOnSelect() {
    if ($('#buildGridUsing :selected').val() == "children") {
        $("[aria-controls='childPagesTab']").show();
        $("[aria-controls='fixedListTab']").hide();
        $("[aria-controls='tagsTab']").hide();
    } else if ($('#buildGridUsing :selected').val() == "static") {
        $("[aria-controls='childPagesTab']").hide();
        $("[aria-controls='fixedListTab']").show();
        $("[aria-controls='tagsTab']").hide();
    } else if ($('#buildGridUsing :selected').val() == "tags") {
        $("[aria-controls='childPagesTab']").hide();
        $("[aria-controls='fixedListTab']").hide();
        $("[aria-controls='tagsTab']").show();
    }
}

function contentResultsGridLoadMoreOnClick() {
    var isLazyLoadRequired = document.getElementById('isLazyLoadRequired');
    if (isLazyLoadRequired.checked) {
        $('#numberOfResultsPerTurn').parent().show();
    } else {
        $('#numberOfResultsPerTurn').parent().hide();
    }
}

function dynamicPagePropertiesOnLoad(selectedValue) {
    if (selectedValue == "startAndEndDate") {
        $("#separator").parent().show();
    } else {
        $("#separator").parent().hide();
    }
}

function dynamicPageProPageProOptionsOnSelect(event) {
    dynamicPagePropertiesOnLoad(event.selected);
}

function fragmentContainerOnLoad(selectedValue) {
    if (selectedValue == 'segmentation' || selectedValue == undefined) {
        $("[aria-controls='ref']").hide();
    } else if (selectedValue == 'reference') {
        $("[aria-controls='ref']").show();
    }
}

function fragmentContainerFragTypeOnSelect(event) {
    fragmentContainerOnLoad(event.selected);
}


function mediaGalleryOnLoad(_self, selectedValue) {
    mediaGalleryMediaType($(_self).closest("li"), selectedValue);
}

function mediaGalleryMediaTypeOnSelect(event) {
    mediaGalleryMediaType($(event.target).closest("li"), event.selected);
}

function mediaGalleryMediaType(parentLI, selectedValue) {
    if (selectedValue == 'image' || selectedValue == '' || selectedValue == undefined) {
        parentLI.find("#videoUrlId").parent().hide();
        parentLI.find("#videooldId").parent().hide();
        parentLI.find("#externalVideoPlayerId").parent().hide();
        parentLI.find("#imageUrlId").parent().show();
        parentLI.find("#altId").parent().show();
        parentLI.find("#fallbackUrlId").parent().hide();
        parentLI.find("#fallbackAltId").parent().hide();

    } else if (selectedValue == 'Brightcove' || selectedValue == 'Vimeo' || selectedValue == 'YouTube') {
        parentLI.find("#videoUrlId").parent().hide();
        parentLI.find("#videooldId").parent().show();
        parentLI.find("#externalVideoPlayerId").parent().hide();
        parentLI.find("#imageUrlId").parent().show();
        parentLI.find("#altId").parent().hide();
        parentLI.find("#fallbackUrlId").parent().hide();
        parentLI.find("#fallbackAltId").parent().hide();
    } else if (selectedValue == 'dam') {
        parentLI.find("#videoUrlId").parent().show();
        parentLI.find("#videooldId").parent().hide();
        parentLI.find("#externalVideoPlayerId").parent().hide();
        parentLI.find("#imageUrlId").parent().show();
        parentLI.find("#altId").parent().hide();
        parentLI.find("#fallbackUrlId").parent().show();
        parentLI.find("#fallbackAltId").parent().show();
    }
}

function panelOnLoad(selectedValue) {
    panelViewType(selectedValue);
    //execute function when dialog- open - Adaptive Behaviour Not Require check box
    var isNotAdaptivePanelCompFunc = $('#isNotAdaptivePanelComp').attr('data-onClick');
    if (typeof isNotAdaptivePanelCompFunc !== "undefined" && isNotAdaptivePanelCompFunc.length != 0) {
        var func = eval("(" + isNotAdaptivePanelCompFunc + ")");
        func();
    }

    var isCroppingRequiredPanelCompFunc = $('#isCroppingRequiredPanelComp').attr('data-onClick');
    if (typeof isCroppingRequiredPanelCompFunc !== "undefined" && isCroppingRequiredPanelCompFunc.length != 0) {
        var func = eval("(" + isCroppingRequiredPanelCompFunc + ")");
        func();
    }
	loadImage('./file');
}

function panelViewTypeOnSelect(event) {
    panelViewType(event.selected);
}

function panelViewType(selectedValue) {
    if (!selectedValue) {
        selectedValue = 'imagePanel';
    }
    if (selectedValue == 'imagePanel') {
        $("[aria-controls='image']").show();
		 if(document.getElementById('isCroppingRequiredPanelComp').checked){
		$("[aria-controls='croppingDetailsPanelComp']").show();
		}
        $("[aria-controls='advanced']").show();
        $("[aria-controls='baseSettings']").hide();
        $("[aria-controls='controls']").hide();
    } else if (selectedValue == 'videoPanel') {
        $("[aria-controls='image']").hide();
        $("[aria-controls='advanced']").hide();
        $("[aria-controls='baseSettings']").show();
        $("[aria-controls='controls']").show();
		$("[aria-controls='croppingDetailsPanelComp']").hide();
    }
}

function panelIsNotAdaptiveOnClick() {

    if (document.getElementById('isNotAdaptivePanelComp').checked) {
        $('#isCroppingRequiredPanelComp').parent().hide();
        $("[aria-controls='croppingDetailsPanelComp']").hide(); //hide the cropping details tab on checking the "Adaptive behavior not required" checkbox
        $("[aria-controls='croppingDetailsPanelComp']").prop('disabled', true);
    } else {
        $('#isCroppingRequiredPanelComp').parent().show();
        if (document.getElementById('isCroppingRequiredPanelComp').checked) {
            $("[aria-controls='croppingDetailsPanelComp']").prop('disabled', false);
            $("[aria-controls='croppingDetailsPanelComp']").show(); //displaying the cropping details tab when the "cropping required" checkbox is checked
        } else {
            $("[aria-controls='croppingDetailsPanelComp']").prop('disabled', true);
            $("[aria-controls='croppingDetailsPanelComp']").hide();
        }
    }
}

function panelIsCroppingRequiredOnClick() {
    if (document.getElementById('isCroppingRequiredPanelComp').checked) {

        //on dialog load,hiding all the fields of the "Cropping Detail" tab except the main check boxes
        $('#chooseCropOptionPanelComp').parent().hide();
        $('#tabPotrailCropOptionsPanelComp').parent().hide();
        $('#tabLandCropOptionsPanelComp').parent().hide();
        $('#mobPotraitCropOptionsPanelComp').parent().hide();
        $('#mobLandCropOptionPanelComp').parent().hide();

        $('#cropOptionswebPanelComp').hide();
        $('#customCropValuesWebPanelComp').hide();
        $('#cropOptionsTabPotrailPanelComp').hide();
        $('#customCropValTabPotrailPanelComp').hide();
        $('#cropOptionsTabLandPanelComp').hide();
        $('#customCropValTabLandPanelComp').hide();
        $('#cropOptionsMobPotraitPanelComp').hide();
        $('#customCropValMobPotraitPanelComp').hide();
        $('#cropOptionsMobLandPanelComp').hide();
        $('#customCropValMobLandPanelComp').hide();


        if (!document.getElementById('isNotAdaptivePanelComp').checked) {
            $("[aria-controls='croppingDetailsPanelComp']").show(); //displaying the cropping details tab
            $("[aria-controls='croppingDetailsPanelComp']").prop('disabled', false);
        }
        //---------------------executing the handlers on dialog load-------------------------
        if (document.getElementById('requiredForWebPanelComp').checked) {
            $('#chooseCropOptionPanelComp').find('select').prop('disabled', false)
            $('#chooseCropOptionPanelComp').parent().show();

            //this takes the function property(which is stored as a node property) from the "Choose Cropping Options" drop down node and executes that function
            var choosrCroppingFunc = $('#chooseCropOptionPanelComp').attr('data-onSelectFunc');
            var func = eval("(" + choosrCroppingFunc + ")");
            func();
        } else {
            $('#chooseCropOptionPanelComp').parent().hide();
            $('#chooseCropOptionPanelComp').find('select').prop('disabled', true)
            $('#cropOptionswebPanelComp').hide();
            $('#customCropValuesWebPanelComp').hide();
        }
        if (document.getElementById('reqForTabPotraitPanelComp').checked) {
            $('#tabPotrailCropOptionsPanelComp').parent().show();
            //this takes the function property(which is stored as a node property) from the "Choose Tablet Portrait Cropping Options" drop down node and executes that function
            var selectCropForTabPortraitFunc = $('#tabPotrailCropOptionsPanelComp').attr('data-onSelectFunc');
            var func = eval("(" + selectCropForTabPortraitFunc + ")");
            func();
        } else {
            $('#tabPotrailCropOptionsPanelComp').parent().hide();
            $('#cropOptionsTabPotrailPanelComp').hide();
            $('#customCropValTabPotrailPanelComp').hide();
        }
        if (document.getElementById('reqForTabLandscapePanelComp').checked) {
            $('#tabLandCropOptionsPanelComp').parent().show();
            //this takes the function property(which is stored as a node property) from the "Choose Tablet Landscape Cropping Options" drop down node and executes that function
            var selectCropForTabLandscapeFunc = $('#tabLandCropOptionsPanelComp').attr('data-onSelectFunc');
            var func = eval("(" + selectCropForTabLandscapeFunc + ")");
            func();
        } else {
            $('#tabLandCropOptionsPanelComp').parent().hide();
            $('#cropOptionsTabLandPanelComp').hide();
            $('#customCropValTabLandPanelComp').hide();
        }
        if (document.getElementById('reqForMobilePotrailPanelComp').checked) {
            $('#mobPotraitCropOptionsPanelComp').parent().show();
            //this takes the function property(which is stored as a node property) from the "Choose Mobile Potrait Cropping Options" drop down node and executes that function
            var chooseOptionsForMobilePotraitFunc = $('#mobPotraitCropOptionsPanelComp').attr('data-onSelectFunc');
            var func = eval("(" + chooseOptionsForMobilePotraitFunc + ")");
            func();
        } else {
            $('#mobPotraitCropOptionsPanelComp').parent().hide();
            $('#cropOptionsMobPotraitPanelComp').hide();
            $('#customCropValMobPotraitPanelComp').hide();
        }
        if (document.getElementById('reqForMobLandscapePanelComp').checked) {
            $('#mobLandCropOptionPanelComp').parent().show();
            //this takes the function property(which is stored as a node property) from the "Choose Mobile Landscape Cropping Options" drop down node and executes that function
            var chooseOptionsForMobileLandscapeFunc = $('#mobLandCropOptionPanelComp').attr('data-onSelectFunc');
            var func = eval("(" + chooseOptionsForMobileLandscapeFunc + ")");
            func();
        } else {
            $('#mobLandCropOptionPanelComp').parent().hide();
            $('#cropOptionsMobLandPanelComp').hide();
            $('#customCropValMobLandPanelComp').hide();
        }

        //------------------------------------------------------------------------
        //-------clicking on the "Required For Web" check box---------------------
        document.getElementById('requiredForWebPanelComp').onclick = function() {
            if (document.getElementById('requiredForWebPanelComp').checked) {
                $('#chooseCropOptionPanelComp').find('select').prop('disabled', false)
                $('#chooseCropOptionPanelComp').parent().show();

                //this takes the function property(which is stored as a node property) from the "Choose Cropping Options" drop down node and executes that function
                var choosrCroppingFunc = $('#chooseCropOptionPanelComp').attr('data-onSelectFunc');
                var func = eval("(" + choosrCroppingFunc + ")");
                func();
            } else {
                $('#chooseCropOptionPanelComp').parent().hide();
                $('#chooseCropOptionPanelComp').find('select').prop('disabled', true)
                $('#cropOptionswebPanelComp').hide();
                $('#customCropValuesWebPanelComp').hide();
            }
        }

        //--------------------Clicking on the "Required for Table Potrait" checkbox---------------------
        document.getElementById('reqForTabPotraitPanelComp').onclick = function() {
            if (document.getElementById('reqForTabPotraitPanelComp').checked) {
                $('#tabPotrailCropOptionsPanelComp').parent().show();
                //this takes the function property(which is stored as a node property) from the "Choose Tablet Portrait Cropping Options" drop down node and executes that function
                var selectCropForTabPortraitFunc = $('#tabPotrailCropOptionsPanelComp').attr('data-onSelectFunc');
                var func = eval("(" + selectCropForTabPortraitFunc + ")");
                func();
            } else {
                $('#tabPotrailCropOptionsPanelComp').parent().hide();
                $('#cropOptionsTabPotrailPanelComp').hide();
                $('#customCropValTabPotrailPanelComp').hide();
            }
        }

        //--------clicking on the "Required for tablet Landscape" check box----------------
        document.getElementById('reqForTabLandscapePanelComp').onclick = function() {
            if (document.getElementById('reqForTabLandscapePanelComp').checked) {
                $('#tabLandCropOptionsPanelComp').parent().show();
                //this takes the function property(which is stored as a node property) from the "Choose Tablet Landscape Cropping Options" drop down node and executes that function
                var selectCropForTabLandscapeFunc = $('#tabLandCropOptionsPanelComp').attr('data-onSelectFunc');
                var func = eval("(" + selectCropForTabLandscapeFunc + ")");
                func();
            } else {
                $('#tabLandCropOptionsPanelComp').parent().hide();
                $('#cropOptionsTabLandPanelComp').hide();
                $('#customCropValTabLandPanelComp').hide();
            }
        }

        //------------------clicking on the "Required for Mobile Potrait" checkbox---------------------
        document.getElementById('reqForMobilePotrailPanelComp').onclick = function() {
            if (document.getElementById('reqForMobilePotrailPanelComp').checked) {
                $('#mobPotraitCropOptionsPanelComp').parent().show();
                //this takes the function property(which is stored as a node property) from the "Choose Mobile Potrait Cropping Options" drop down node and executes that function
                var chooseOptionsForMobilePotraitFunc = $('#mobPotraitCropOptionsPanelComp').attr('data-onSelectFunc');
                var func = eval("(" + chooseOptionsForMobilePotraitFunc + ")");
                func();
            } else {
                $('#mobPotraitCropOptionsPanelComp').parent().hide();
                $('#cropOptionsMobPotraitPanelComp').hide();
                $('#customCropValMobPotraitPanelComp').hide();
            }
        }

        //--------------clicking on the "Required for Mobile Landscape" checkbox---------------------
        document.getElementById('reqForMobLandscapePanelComp').onclick = function() {
            if (document.getElementById('reqForMobLandscapePanelComp').checked) {
                $('#mobLandCropOptionPanelComp').parent().show();
                //this takes the function property(which is stored as a node property) from the "Choose Mobile Landscape Cropping Options" drop down node and executes that function
                var chooseOptionsForMobileLandscapeFunc = $('#mobLandCropOptionPanelComp').attr('data-onSelectFunc');
                var func = eval("(" + chooseOptionsForMobileLandscapeFunc + ")");
                func();
            } else {
                $('#mobLandCropOptionPanelComp').parent().hide();
                $('#cropOptionsMobLandPanelComp').hide();
                $('#customCropValMobLandPanelComp').hide();
            }
        }
    } else {
        $("[aria-controls='croppingDetailsPanelComp']").hide();
    }
}

function panelSelectWebCropOptionOnSelect() { //this check is to display the fields when the drop down is displayed(by clicking on the chekbox above it)
    if ($('#chooseCropOptionPanelComp').parent().show()) {
        panelSelectWebCropOption()
    }
    //this function is to display the fields when the drop down is used
    document.getElementById('chooseCropOptionPanelComp').onclick = function() {
        panelSelectWebCropOption()
    }
}

function panelSelectWebCropOption() {
    if ($('#chooseCropOptionPanelComp :selected').val() == "defSelCrop") {
        $('#cropOptionswebPanelComp').show();
        $('#customCropValuesWebPanelComp').hide();
    } else if ($('#chooseCropOptionPanelComp :selected').val() == "custom") {
        $('#cropOptionswebPanelComp').hide();
        $('#customCropValuesWebPanelComp').show();
    } else {
        $('#cropOptionswebPanelComp').hide();
        $('#customCropValuesWebPanelComp').hide();
    }
}

function panelSelectTabPCropOptionOnSelect() {

    //this check is to display the fields when the drop down is displayed(by clicking on the chekbox above it)
    if ($('#tabPotrailCropOptionsPanelComp').parent().show()) {
        panelSelectTabPCropOption();
    }
    //this function is to display the fields when the drop down is used
    document.getElementById('tabPotrailCropOptionsPanelComp').onclick = function() {
        panelSelectTabPCropOption();
    }
}

function panelSelectTabPCropOption() {
    if ($('#tabPotrailCropOptionsPanelComp :selected').val() == "defSelCrop") {
        $('#cropOptionsTabPotrailPanelComp').show();
        $('#customCropValTabPotrailPanelComp').hide();
    } else if ($('#tabPotrailCropOptionsPanelComp :selected').val() == "custom") {
        $('#cropOptionsTabPotrailPanelComp').hide();
        $('#customCropValTabPotrailPanelComp').show();
    } else {
        $('#cropOptionsTabPotrailPanelComp').hide();
        $('#customCropValTabPotrailPanelComp').hide();
    }
}

function panelSelectTabLCropOptionOnSelect() {
    //this check is to display the fields when the drop down is displayed(by clicking on the chekbox above it)
    if ($('#tabLandCropOptionsPanelComp').parent().show()) {
        panelSelectTabLCropOption();
    }
    //this function is to display the fields when the drop down is used
    document.getElementById('tabLandCropOptionsPanelComp').onclick = function() {
        panelSelectTabLCropOption();
    }
}

function panelSelectTabLCropOption() {
    if ($('#tabLandCropOptionsPanelComp :selected').val() == "defSelCrop") {
        $('#cropOptionsTabLandPanelComp').show();
        $('#customCropValTabLandPanelComp').hide();
    } else if ($('#tabLandCropOptionsPanelComp :selected').val() == "custom") {
        $('#cropOptionsTabLandPanelComp').hide();
        $('#customCropValTabLandPanelComp').show();
    } else {
        $('#cropOptionsTabLandPanelComp').hide();
        $('#customCropValTabLandPanelComp').hide();
    }
}

function panelSelectMobPCropOptionOnSelect() {
    //this check is to display the fields when the drop down is displayed(by clicking on the chekbox above it)
    if ($('#mobPotraitCropOptionsPanelComp').parent().show()) {
        panelSelectMobPCropOption();
    }
    //this function is to display the fields when the drop down is used
    document.getElementById('mobPotraitCropOptionsPanelComp').onclick = function() {
        panelSelectMobPCropOption();
    }
}

function panelSelectMobPCropOption() {
    if ($('#mobPotraitCropOptionsPanelComp :selected').val() == "defSelCrop") {
        $('#cropOptionsMobPotraitPanelComp').show();
        $('#customCropValMobPotraitPanelComp').hide();
    } else if ($('#mobPotraitCropOptionsPanelComp :selected').val() == "custom") {
        $('#cropOptionsMobPotraitPanelComp').hide();
        $('#customCropValMobPotraitPanelComp').show();
    } else {
        $('#cropOptionsMobPotraitPanelComp').hide();
        $('#customCropValMobPotraitPanelComp').hide();
    }
}

function panelSelectMobLCropOptionOnSelect() {
    //this check is to display the fields when the drop down is displayed(by clicking on the chekbox above it)
    if ($('#mobLandCropOptionPanelComp').parent().show()) {
        panelSelectMobLCropOption();
    }
    //this function is to display the fields when the drop down is used
    document.getElementById('mobLandCropOptionPanelComp').onclick = function() {
        panelSelectMobLCropOption();
    }
}

function panelSelectMobLCropOption() {
    if ($('#mobLandCropOptionPanelComp :selected').val() == "defSelCrop") {
        $('#cropOptionsMobLandPanelComp').show();
        $('#customCropValMobLandPanelComp').hide();
    } else if ($('#mobLandCropOptionPanelComp :selected').val() == "custom") {
        $('#cropOptionsMobLandPanelComp').hide();
        $('#customCropValMobLandPanelComp').show();
    } else {
        $('#cropOptionsMobLandPanelComp').hide();
        $('#customCropValMobLandPanelComp').hide();
    }
}

function plainTextImageOnLoad() {
    var isNotAdaptivePlainTextFunc = $('#isNotAdaptivePlainText').attr('data-onClick');

    if (typeof isNotAdaptivePlainTextFunc !== "undefined" && isNotAdaptivePlainTextFunc.length != 0) {
        var func = eval("(" + isNotAdaptivePlainTextFunc + ")");
        func();
    }
    var isCropReqPlainTextFunc = $('#isCropReqPlainText').attr('data-onClick');
    if (typeof isCropReqPlainTextFunc !== "undefined" && isCropReqPlainTextFunc.length != 0) {
        var func = eval("(" + isCropReqPlainTextFunc + ")");
        func();
    }
	loadImage('./file');
}

function plainTextImageIsNotAdaptiveOnClick() {
    if (document.getElementById('isNotAdaptivePlainText').checked) {
        $('#isCropReqPlainText').parent().hide();
        $("[aria-controls='cropDetailsPlainTextComp']").hide(); //hide the cropping details tab on checking the "Adaptive behavior not required" checkbox
        $("[aria-controls='cropDetailsPlainTextComp']").prop('disabled', true);
    } else {
        $('#isCropReqPlainText').parent().show();
        if (document.getElementById('isCropReqPlainText').checked) {
            $("[aria-controls='cropDetailsPlainTextComp']").prop('disabled', false);
            $("[aria-controls='cropDetailsPlainTextComp']").show(); //displaying the cropping details tab when the "cropping required" checkbox is checked
        } else {
            $("[aria-controls='cropDetailsPlainTextComp']").prop('disabled', true);
            $("[aria-controls='cropDetailsPlainTextComp']").hide();
        }
    }
}

function plainTextImageIsCroppingRequiredOnClick() {
    if (document.getElementById('isCropReqPlainText').checked) {
        //on dialog load,hiding all the fields of the "Cropping Detail" tab except the main check boxes
        $('#chooseCropOptionPlainText').parent().hide();
        $('#tabPotraitCropOptionPlainText').parent().hide();
        $('#tabLandCropOptionsPlainText').parent().hide();
        $('#MobPotraitCropOptionsPlainText').parent().hide();
        $('#mobLandCropOptionsPlainText').parent().hide();

        $('#cropOptionsWebPlainText').hide();
        $('#customCropValWebPlainText').hide();
        $('#cropOptionTabPotraitPlainText').hide();
        $('#custimCropValTabPotraitPlainText').hide();
        $('#cropOptionsTabLandPlainText').hide();
        $('#customCropValTabLandscape').hide();
        $('#cropOptionsMobilePotraitPlainText').hide();
        $('#customCropValMobilePotraitPlainText').hide();
        $('#cropOptionMobLandPlainText').hide();
        $('#customCropValMobLandPlainText').hide();

        if (!document.getElementById('isNotAdaptivePlainText').checked) {
            $("[aria-controls='cropDetailsPlainTextComp']").show(); //displaying the cropping details tab
            $("[aria-controls='cropDetailsPlainTextComp']").prop('disabled', false);
        }

        //--------------------------executing handlers on dialog load--------------------------------
        if (document.getElementById('reqForWebPlainText').checked) {
            $('#chooseCropOptionPlainText').find('select').prop('disabled', false)
            $('#chooseCropOptionPlainText').parent().show();

            //this takes the function property(which is stored as a node property) from the "Choose Cropping Options" drop down node and executes that function
            var choosrCroppingFunc = $('#chooseCropOptionPlainText').attr('data-onSelectFunc');
            var func = eval("(" + choosrCroppingFunc + ")");
            func();
        } else {
            $('#chooseCropOptionPlainText').parent().hide();
            $('#chooseCropOptionPlainText').find('select').prop('disabled', true)
            $('#cropOptionsWebPlainText').hide();
            $('#customCropValWebPlainText').hide();
        }
        if (document.getElementById('reqForTabPotrailPlainText').checked) {
            $('#tabPotraitCropOptionPlainText').parent().show();
            //this takes the function property(which is stored as a node property) from the "Choose Tablet Portrait Cropping Options" drop down node and executes that function
            var selectCropForTabPortraitFunc = $('#tabPotraitCropOptionPlainText').attr('data-onSelectFunc');
            var func = eval("(" + selectCropForTabPortraitFunc + ")");
            func();
        } else {
            $('#tabPotraitCropOptionPlainText').parent().hide();
            $('#cropOptionTabPotraitPlainText').hide();
            $('#custimCropValTabPotraitPlainText').hide();
        }
        if (document.getElementById('reqForTabLandPlainText').checked) {
            $('#tabLandCropOptionsPlainText').parent().show();
            //this takes the function property(which is stored as a node property) from the "Choose Tablet Landscape Cropping Options" drop down node and executes that function
            var selectCropForTabLandscapeFunc = $('#tabLandCropOptionsPlainText').attr('data-onSelectFunc');
            var func = eval("(" + selectCropForTabLandscapeFunc + ")");
            func();
        } else {
            $('#tabLandCropOptionsPlainText').parent().hide();
            $('#cropOptionsTabLandPlainText').hide();
            $('#customCropValTabLandscape').hide();
        }
        if (document.getElementById('reqForMobPotraitPlainText').checked) {
            $('#MobPotraitCropOptionsPlainText').parent().show();
            //this takes the function property(which is stored as a node property) from the "Choose Mobile Potrait Cropping Options" drop down node and executes that function
            var chooseOptionsForMobilePotraitFunc = $('#MobPotraitCropOptionsPlainText').attr('data-onSelectFunc');
            var func = eval("(" + chooseOptionsForMobilePotraitFunc + ")");
            func();
        } else {
            $('#MobPotraitCropOptionsPlainText').parent().hide();
            $('#cropOptionsMobilePotraitPlainText').hide();
            $('#customCropValMobilePotraitPlainText').hide();
        }
        if (document.getElementById('reqForMobLandPlainText').checked) {
            $('#mobLandCropOptionsPlainText').parent().show();
            //this takes the function property(which is stored as a node property) from the "Choose Mobile Landscape Cropping Options" drop down node and executes that function
            var chooseOptionsForMobileLandscapeFunc = $('#mobLandCropOptionsPlainText').attr('data-onSelectFunc');
            var func = eval("(" + chooseOptionsForMobileLandscapeFunc + ")");
            func();
        } else {
            $('#mobLandCropOptionsPlainText').parent().hide();
            $('#cropOptionMobLandPlainText').hide();
            $('#customCropValMobLandPlainText').hide();
        }

        //-------clicking on the "Required For Web" check box---------------------
        document.getElementById('reqForWebPlainText').onclick = function() {
            if (document.getElementById('reqForWebPlainText').checked) {
                $('#chooseCropOptionPlainText').find('select').prop('disabled', false)
                $('#chooseCropOptionPlainText').parent().show();

                //this takes the function property(which is stored as a node property) from the "Choose Cropping Options" drop down node and executes that function
                var choosrCroppingFunc = $('#chooseCropOptionPlainText').attr('data-onSelectFunc');
                var func = eval("(" + choosrCroppingFunc + ")");
                func();
            } else {
                $('#chooseCropOptionPlainText').parent().hide();
                $('#chooseCropOptionPlainText').find('select').prop('disabled', true)
                $('#cropOptionsWebPlainText').hide();
                $('#customCropValWebPlainText').hide();
            }
        }

        //--------------------Clicking on the "Required for Table Potrait" checkbox---------------------
        document.getElementById('reqForTabPotrailPlainText').onclick = function() {
            if (document.getElementById('reqForTabPotrailPlainText').checked) {
                $('#tabPotraitCropOptionPlainText').parent().show();
                //this takes the function property(which is stored as a node property) from the "Choose Tablet Portrait Cropping Options" drop down node and executes that function
                var selectCropForTabPortraitFunc = $('#tabPotraitCropOptionPlainText').attr('data-onSelectFunc');
                var func = eval("(" + selectCropForTabPortraitFunc + ")");
                func();
            } else {
                $('#tabPotraitCropOptionPlainText').parent().hide();
                $('#cropOptionTabPotraitPlainText').hide();
                $('#custimCropValTabPotraitPlainText').hide();
            }
        }

        //--------clicking on the "Required for tablet Landscape" check box----------------
        document.getElementById('reqForTabLandPlainText').onclick = function() {
            if (document.getElementById('reqForTabLandPlainText').checked) {
                $('#tabLandCropOptionsPlainText').parent().show();
                //this takes the function property(which is stored as a node property) from the "Choose Tablet Landscape Cropping Options" drop down node and executes that function
                var selectCropForTabLandscapeFunc = $('#tabLandCropOptionsPlainText').attr('data-onSelectFunc');
                var func = eval("(" + selectCropForTabLandscapeFunc + ")");
                func();
            } else {
                $('#tabLandCropOptionsPlainText').parent().hide();
                $('#cropOptionsTabLandPlainText').hide();
                $('#customCropValTabLandscape').hide();
            }
        }

        //------------------clicking on the "Required for Mobile Potrait" checkbox---------------------
        document.getElementById('reqForMobPotraitPlainText').onclick = function() {
            if (document.getElementById('reqForMobPotraitPlainText').checked) {
                $('#MobPotraitCropOptionsPlainText').parent().show();
                //this takes the function property(which is stored as a node property) from the "Choose Mobile Potrait Cropping Options" drop down node and executes that function
                var chooseOptionsForMobilePotraitFunc = $('#MobPotraitCropOptionsPlainText').attr('data-onSelectFunc');
                var func = eval("(" + chooseOptionsForMobilePotraitFunc + ")");
                func();
            } else {
                $('#MobPotraitCropOptionsPlainText').parent().hide();
                $('#cropOptionsMobilePotraitPlainText').hide();
                $('#customCropValMobilePotraitPlainText').hide();
            }
        }

        //--------------clicking on the "Required for Mobile Landscape" checkbox---------------------
        document.getElementById('reqForMobLandPlainText').onclick = function() {
            if (document.getElementById('reqForMobLandPlainText').checked) {
                $('#mobLandCropOptionsPlainText').parent().show();
                //this takes the function property(which is stored as a node property) from the "Choose Mobile Landscape Cropping Options" drop down node and executes that function
                var chooseOptionsForMobileLandscapeFunc = $('#mobLandCropOptionsPlainText').attr('data-onSelectFunc');
                var func = eval("(" + chooseOptionsForMobileLandscapeFunc + ")");
                func();
            } else {
                $('#mobLandCropOptionsPlainText').parent().hide();
                $('#cropOptionMobLandPlainText').hide();
                $('#customCropValMobLandPlainText').hide();
            }
        }
    } else {
        $("[aria-controls='cropDetailsPlainTextComp']").hide();
    }
}

function plainTextImageSelectWebCropOptionOnSelect() {
    //this check is to display the fields when the drop down is displayed(by clicking on the chekbox above it)
    if ($('#chooseCropOptionPlainText').parent().show()) {
        plainTextImageSelectWebCropOption();
    }
    //this function is to display the fields when the drop down is used
    document.getElementById('chooseCropOptionPlainText').onclick = function() {
        plainTextImageSelectWebCropOption();
    }
}

function plainTextImageSelectWebCropOption() {
    if ($('#chooseCropOptionPlainText :selected').val() == "defSelCrop") {
        $('#cropOptionsWebPlainText').show();
        $('#customCropValWebPlainText').hide();
    } else if ($('#chooseCropOptionPlainText :selected').val() == "custom") {
        $('#cropOptionsWebPlainText').hide();
        $('#customCropValWebPlainText').show();
    } else {
        $('#cropOptionsWebPlainText').hide();
        $('#customCropValWebPlainText').hide();
    }
}

function plainTextImageSelectTabPCropOptionOnSelect() {

    //this check is to display the fields when the drop down is displayed(by clicking on the chekbox above it)
    if ($('#tabPotraitCropOptionPlainText').parent().show()) {
        plainTextImageSelectTabPCropOption();
    }
    //this function is to display the fields when the drop down is used
    document.getElementById('tabPotraitCropOptionPlainText').onclick = function() {
        plainTextImageSelectTabPCropOption();
    }
}

function plainTextImageSelectTabPCropOption() {
    if ($('#tabPotraitCropOptionPlainText :selected').val() == "defSelCrop") {
        $('#cropOptionTabPotraitPlainText').show();
        $('#custimCropValTabPotraitPlainText').hide();
    } else if ($('#tabPotraitCropOptionPlainText :selected').val() == "custom") {
        $('#cropOptionTabPotraitPlainText').hide();
        $('#custimCropValTabPotraitPlainText').show();
    } else {
        $('#cropOptionTabPotraitPlainText').hide();
        $('#custimCropValTabPotraitPlainText').hide();
    }
}

function plainTextImageSelectTabLCropOptionOnSelect() {

    //this check is to display the fields when the drop down is displayed(by clicking on the chekbox above it)
    if ($('#tabLandCropOptionsPlainText').parent().show()) {
        plainTextImageSelectTabLCropOption();
    }
    //this function is to display the fields when the drop down is used
    document.getElementById('tabLandCropOptionsPlainText').onclick = function() {
        plainTextImageSelectTabLCropOption();
    }
}

function plainTextImageSelectTabLCropOption() {
    if ($('#tabLandCropOptionsPlainText :selected').val() == "defSelCrop") {
        $('#cropOptionsTabLandPlainText').show();
        $('#customCropValTabLandscape').hide();
    } else if ($('#tabLandCropOptionsPlainText :selected').val() == "custom") {
        $('#cropOptionsTabLandPlainText').hide();
        $('#customCropValTabLandscape').show();
    } else {
        $('#cropOptionsTabLandPlainText').hide();
        $('#customCropValTabLandscape').hide();
    }
}

function plainTextImageSelectMobPCropOptionOnSelect() {

    //this check is to display the fields when the drop down is displayed(by clicking on the chekbox above it)
    if ($('#MobPotraitCropOptionsPlainText').parent().show()) {
        plainTextImageSelectMobPCropOption();
    }
    //this function is to display the fields when the drop down is used
    document.getElementById('MobPotraitCropOptionsPlainText').onclick = function() {
        plainTextImageSelectMobPCropOption();
    }
}

function plainTextImageSelectMobPCropOption() {
    if ($('#MobPotraitCropOptionsPlainText :selected').val() == "defSelCrop") {
        $('#cropOptionsMobilePotraitPlainText').show();
        $('#customCropValMobilePotraitPlainText').hide();
    } else if ($('#MobPotraitCropOptionsPlainText :selected').val() == "custom") {
        $('#cropOptionsMobilePotraitPlainText').hide();
        $('#customCropValMobilePotraitPlainText').show();
    } else {
        $('#cropOptionsMobilePotraitPlainText').hide();
        $('#customCropValMobilePotraitPlainText').hide();
    }
}

function plainTextImageSelectMobLCropOptionOnSelect() {

    //this check is to display the fields when the drop down is displayed(by clicking on the chekbox above it)
    if ($('#mobLandCropOptionsPlainText').parent().show()) {
        plainTextImageSelectMobLCropOption();
    }
    //this function is to display the fields when the drop down is used
    document.getElementById('mobLandCropOptionsPlainText').onclick = function() {
        plainTextImageSelectMobLCropOption();
    }
}

function plainTextImageSelectMobLCropOption() {
    if ($('#mobLandCropOptionsPlainText :selected').val() == "defSelCrop") {
        $('#cropOptionMobLandPlainText').show();
        $('#customCropValMobLandPlainText').hide();
    } else if ($('#mobLandCropOptionsPlainText :selected').val() == "custom") {
        $('#cropOptionMobLandPlainText').hide();
        $('#customCropValMobLandPlainText').show();
    } else {
        $('#cropOptionMobLandPlainText').hide();
        $('#customCropValMobLandPlainText').hide();
    }
}

function richTextImageOnLoad() {
    $("[aria-controls='croppingDetailsRTI']").hide();

    var funcBody = $('#isNotAdaptiveRTI').attr('data-onClick');
    if (typeof funcBody !== "undefined" && funcBody.length != 0) {
        var func = eval("(" + funcBody + ")");
        func();
    }
    funcBody = $('#isCroppingReqRTI').attr('data-onClick');

    if (typeof funcBody !== "undefined" && funcBody.length != 0) {
        var func = eval("(" + funcBody + ")");
        func();
    }
	loadImage('./file');
}

function richTextImageSelectWebCropOptionOnSelect() {
    //this check is to display the fields when the drop down is displayed(by clicking on the chekbox above it)
    if ($('#chooseCroppingOptionRTI').parent().show()) {
        richTextImageSelectWebCropOption();
    }
    //this function is to display the fields when the drop down is used
    document.getElementById('chooseCroppingOptionRTI').onclick = function() {
        richTextImageSelectWebCropOption();
    }
}

function richTextImageSelectWebCropOption() {
    if ($('#chooseCroppingOptionRTI :selected').val() == "defSelCrop") {
        $('#cropOptionsWebRTI').show();
        $('#customCropValuesWebRTI').hide();
    } else if ($('#chooseCroppingOptionRTI :selected').val() == "custom") {
        $('#cropOptionsWebRTI').hide();
        $('#customCropValuesWebRTI').show();
    } else {
        $('#cropOptionsWebRTI').hide();
        $('#customCropValuesWebRTI').hide();
    }
}

function richTextImageSelectTabPCropOptionOnSelect() {
    //this check is to display the fields when the drop down is displayed(by clicking on the chekbox above it)
    if ($('#selectCropForTabPortraitRTI').parent().show()) {
        richTextImageSelectTabPCropOption();
    }
    //this function is to display the fields when the drop down is used
    document.getElementById('selectCropForTabPortraitRTI').onclick = function() {
        richTextImageSelectTabPCropOption();
    }
}

function richTextImageSelectTabPCropOption() {
    if ($('#selectCropForTabPortraitRTI :selected').val() == "defSelCrop") {
        $('#cropOptionsTabletPotraitRTI').show();
        $('#customCropValuesTabletPotraitRTI').hide();
    } else if ($('#selectCropForTabPortraitRTI :selected').val() == "custom") {
        $('#cropOptionsTabletPotraitRTI').hide();
        $('#customCropValuesTabletPotraitRTI').show();
    } else {
        $('#cropOptionsTabletPotraitRTI').hide();
        $('#customCropValuesTabletPotraitRTI').hide();
    }
}

function richTextImageSelectTabLCropOptionOnSelect() {

    //this check is to display the fields when the drop down is displayed(by clicking on the chekbox above it)
    if ($('#selectCropForTabLandscapeRTI').parent().show()) {
        richTextImageSelectTabLCropOption();
    }
    //this function is to display the fields when the drop down is used
    document.getElementById('selectCropForTabLandscapeRTI').onclick = function() {
        richTextImageSelectTabLCropOption();
    }
}

function richTextImageSelectTabLCropOption() {
    if ($('#selectCropForTabLandscapeRTI :selected').val() == "defSelCrop") {
        $('#cropOptionsTabletLandscapeRTI').show();
        $('#customCropValuesTabletLandscapeRTI').hide();
    } else if ($('#selectCropForTabLandscapeRTI :selected').val() == "custom") {
        $('#cropOptionsTabletLandscapeRTI').hide();
        $('#customCropValuesTabletLandscapeRTI').show();
    } else {
        $('#cropOptionsTabletLandscapeRTI').hide();
        $('#customCropValuesTabletLandscapeRTI').hide();
    }
}

function richTextImageSelectMobPCropOptionOnSelect() {
    //this check is to display the fields when the drop down is displayed(by clicking on the chekbox above it)
    if ($('#chooseOptionsForMobilePotraitRTI').parent().show()) {
        richTextImageSelectMobPCropOption();
    }
    //this function is to display the fields when the drop down is used
    document.getElementById('chooseOptionsForMobilePotraitRTI').onclick = function() {
        richTextImageSelectMobPCropOption();
    }
}

function richTextImageSelectMobPCropOption() {
    if ($('#chooseOptionsForMobilePotraitRTI :selected').val() == "defSelCrop") {
        $('#cropOptionsMobilePotraitRTI').show();
        $('#customCropValuesMobilePotraitRTI').hide();
    } else if ($('#chooseOptionsForMobilePotraitRTI :selected').val() == "custom") {
        $('#cropOptionsMobilePotraitRTI').hide();
        $('#customCropValuesMobilePotraitRTI').show();
    } else {
        $('#cropOptionsMobilePotraitRTI').hide();
        $('#customCropValuesMobilePotraitRTI').hide();
    }
}

function richTextImageSelectMobLCropOptionOnSelect() {
    //this check is to display the fields when the drop down is displayed(by clicking on the chekbox above it)
    if ($('#chooseOptionsForMobileLandscapeRTI').parent().show()) {
        richTextImageSelectMobLCropOption();
    }
    //this function is to display the fields when the drop down is used
    document.getElementById('chooseOptionsForMobileLandscapeRTI').onclick = function() {
        richTextImageSelectMobLCropOption();
    }
}

function richTextImageSelectMobLCropOption() {
    if ($('#chooseOptionsForMobileLandscapeRTI :selected').val() == "defSelCrop") {
        $('#cropOptionsMobileLandscapeRTI').show();
        $('#customCropValuesMobileLandscapeRTI').hide();
    } else if ($('#chooseOptionsForMobileLandscapeRTI :selected').val() == "custom") {
        $('#cropOptionsMobileLandscapeRTI').hide();
        $('#customCropValuesMobileLandscapeRTI').show();
    } else {
        $('#cropOptionsMobileLandscapeRTI').hide();
        $('#customCropValuesMobileLandscapeRTI').hide();
    }
}

function richTextImageIsNotAdaptiveOnClick() {
    if (document.getElementById('isNotAdaptiveRTI').checked) {
        $('#isCroppingReqRTI').parent().hide();
        $("[aria-controls='croppingDetailsRTI']").hide(); //hide the cropping details tab on checking the "Adaptive behavior not required" checkbox
        $("[aria-controls='croppingDetailsRTI']").prop('disabled', true);
    } else {
        $('#isCroppingReqRTI').parent().show();
        if (document.getElementById('isCroppingReqRTI').checked) {
            $("[aria-controls='croppingDetailsRTI']").prop('disabled', false);
            $("[aria-controls='croppingDetailsRTI']").show(); //displaying the cropping details tab when the "cropping required" checkbox is checked
        } else {
            $("[aria-controls='croppingDetailsRTI']").prop('disabled', true);
            $("[aria-controls='croppingDetailsRTI']").hide();
        }
    }
}

function richTextImageIsCroppingRequiredOnClick() {
    if (document.getElementById('isCroppingReqRTI').checked) {
        //on dialog load,hiding all the fields of the "Cropping Detail" tab except the main check boxes
        $('#chooseCroppingOptionRTI').parent().hide();
        $('#selectCropForTabPortraitRTI').parent().hide();
        $('#selectCropForTabLandscapeRTI').parent().hide();
        $('#chooseOptionsForMobilePotraitRTI').parent().hide();
        $('#chooseOptionsForMobileLandscapeRTI').parent().hide();

        $('#cropOptionsWebRTI').hide();
        $('#customCropValuesWebRTI').hide();
        $('#cropOptionsTabletPotraitRTI').hide();
        $('#customCropValuesTabletPotraitRTI').hide();
        $('#cropOptionsTabletLandscapeRTI').hide();
        $('#customCropValuesTabletLandscapeRTI').hide();
        $('#cropOptionsMobilePotraitRTI').hide();
        $('#customCropValuesMobilePotraitRTI').hide();
        $('#cropOptionsMobileLandscapeRTI').hide();
        $('#customCropValuesMobileLandscapeRTI').hide();

        if (!document.getElementById('isNotAdaptiveRTI').checked) {
            $("[aria-controls='croppingDetailsRTI']").show(); //displaying the cropping details tab
            $("[aria-controls='croppingDetailsRTI']").prop('disabled', false);
        }

        //------------------------executing handlers on dialog load---------------------------

        if (document.getElementById('isReqForWebRTI').checked) {
            $('#chooseCroppingOptionRTI').find('select').prop('disabled', false)
            $('#chooseCroppingOptionRTI').parent().show();

            //this takes the function property(which is stored as a node property) from the "Choose Cropping Options" drop down node and executes that function
            var chooseCroppingFuncRTI = $('#chooseCroppingOptionRTI').attr('data-onSelectFunc');
            var func = eval("(" + chooseCroppingFuncRTI + ")");
            func();
        } else {
            $('#chooseCroppingOptionRTI').parent().hide();
            $('#chooseCroppingOptionRTI').find('select').prop('disabled', true)
            $('#cropOptionsWebRTI').hide();
            $('#customCropValuesWebRTI').hide();
        }
        if (document.getElementById('isReqForTabPotraitRTI').checked) {
            $('#selectCropForTabPortraitRTI').parent().show();
            //this takes the function property(which is stored as a node property) from the "Choose Tablet Portrait Cropping Options" drop down node and executes that function
            var selectCropForTabPortraitFuncRTI = $('#selectCropForTabPortraitRTI').attr('data-onSelectFunc');
            var func = eval("(" + selectCropForTabPortraitFuncRTI + ")");
            func();
        } else {
            $('#selectCropForTabPortraitRTI').parent().hide();
            $('#cropOptionsTabletPotraitRTI').hide();
            $('#customCropValuesTabletPotraitRTI').hide();
        }
        if (document.getElementById('isReqForTabLandscapeRTI').checked) {
            $('#selectCropForTabLandscapeRTI').parent().show();
            //this takes the function property(which is stored as a node property) from the "Choose Tablet Landscape Cropping Options" drop down node and executes that function
            var selectCropForTabLandscapeFuncRTI = $('#selectCropForTabLandscapeRTI').attr('data-onSelectFunc');
            var func = eval("(" + selectCropForTabLandscapeFuncRTI + ")");
            func();
        } else {
            $('#selectCropForTabLandscapeRTI').parent().hide();
            $('#cropOptionsTabletLandscapeRTI').hide();
            $('#customCropValuesTabletLandscapeRTI').hide();
        }

        if (document.getElementById('isReqForMobilePotraitRTI').checked) {
            $('#chooseOptionsForMobilePotraitRTI').parent().show();
            //this takes the function property(which is stored as a node property) from the "Choose Mobile Potrait Cropping Options" drop down node and executes that function
            var chooseOptionsForMobilePotraitFuncRTI = $('#chooseOptionsForMobilePotraitRTI').attr('data-onSelectFunc');
            var func = eval("(" + chooseOptionsForMobilePotraitFuncRTI + ")");
            func();
        } else {
            $('#chooseOptionsForMobilePotraitRTI').parent().hide();
            $('#cropOptionsMobilePotraitRTI').hide();
            $('#customCropValuesMobilePotraitRTI').hide();
        }

        if (document.getElementById('isReqForMobileLandscapeRTI').checked) {
            $('#chooseOptionsForMobileLandscapeRTI').parent().show();
            //this takes the function property(which is stored as a node property) from the "Choose Mobile Landscape Cropping Options" drop down node and executes that function
            var chooseOptionsForMobileLandscapeFuncRTI = $('#chooseOptionsForMobileLandscapeRTI').attr('data-onSelectFunc');
            var func = eval("(" + chooseOptionsForMobileLandscapeFuncRTI + ")");
            func();
        } else {
            $('#chooseOptionsForMobileLandscapeRTI').parent().hide();
            $('#cropOptionsMobileLandscapeRTI').hide();
            $('#customCropValuesMobileLandscapeRTI').hide();
        }

        //-------------------------------------------------------------------------------------
        //-------clicking on the "Required For Web" check box---------------------
        document.getElementById('isReqForWebRTI').onclick = function() {
            if (document.getElementById('isReqForWebRTI').checked) {
                $('#chooseCroppingOptionRTI').find('select').prop('disabled', false)
                $('#chooseCroppingOptionRTI').parent().show();

                //this takes the function property(which is stored as a node property) from the "Choose Cropping Options" drop down node and executes that function
                var chooseCroppingFuncRTI = $('#chooseCroppingOptionRTI').attr('data-onSelectFunc');
                var func = eval("(" + chooseCroppingFuncRTI + ")");
                func();
            } else {
                $('#chooseCroppingOptionRTI').parent().hide();
                $('#chooseCroppingOptionRTI').find('select').prop('disabled', true)
                $('#cropOptionsWebRTI').hide();
                $('#customCropValuesWebRTI').hide();
            }
        }

        //--------------------Clicking on the "Required for Table Potrait" checkbox---------------------
        document.getElementById('isReqForTabPotraitRTI').onclick = function() {
            if (document.getElementById('isReqForTabPotraitRTI').checked) {
                $('#selectCropForTabPortraitRTI').parent().show();
                //this takes the function property(which is stored as a node property) from the "Choose Tablet Portrait Cropping Options" drop down node and executes that function
                var selectCropForTabPortraitFuncRTI = $('#selectCropForTabPortraitRTI').attr('data-onSelectFunc');
                var func = eval("(" + selectCropForTabPortraitFuncRTI + ")");
                func();
            } else {
                $('#selectCropForTabPortraitRTI').parent().hide();
                $('#cropOptionsTabletPotraitRTI').hide();
                $('#customCropValuesTabletPotraitRTI').hide();
            }
        }

        //--------clicking on the "Required for tablet Landscape" check box----------------
        document.getElementById('isReqForTabLandscapeRTI').onclick = function() {
            if (document.getElementById('isReqForTabLandscapeRTI').checked) {
                $('#selectCropForTabLandscapeRTI').parent().show();
                //this takes the function property(which is stored as a node property) from the "Choose Tablet Landscape Cropping Options" drop down node and executes that function
                var selectCropForTabLandscapeFuncRTI = $('#selectCropForTabLandscapeRTI').attr('data-onSelectFunc');
                var func = eval("(" + selectCropForTabLandscapeFuncRTI + ")");
                func();
            } else {
                $('#selectCropForTabLandscapeRTI').parent().hide();
                $('#cropOptionsTabletLandscapeRTI').hide();
                $('#customCropValuesTabletLandscapeRTI').hide();
            }
        }

        //------------------clicking on the "Required for Mobile Potrait" checkbox---------------------
        document.getElementById('isReqForMobilePotraitRTI').onclick = function() {
            if (document.getElementById('isReqForMobilePotraitRTI').checked) {
                $('#chooseOptionsForMobilePotraitRTI').parent().show();
                //this takes the function property(which is stored as a node property) from the "Choose Mobile Potrait Cropping Options" drop down node and executes that function
                var chooseOptionsForMobilePotraitFuncRTI = $('#chooseOptionsForMobilePotraitRTI').attr('data-onSelectFunc');
                var func = eval("(" + chooseOptionsForMobilePotraitFuncRTI + ")");
                func();
            } else {
                $('#chooseOptionsForMobilePotraitRTI').parent().hide();
                $('#cropOptionsMobilePotraitRTI').hide();
                $('#customCropValuesMobilePotraitRTI').hide();
            }
        }

        //--------------clicking on the "Required for Mobile Landscape" checkbox---------------------
        document.getElementById('isReqForMobileLandscapeRTI').onclick = function() {
            if (document.getElementById('isReqForMobileLandscapeRTI').checked) {
                $('#chooseOptionsForMobileLandscapeRTI').parent().show();
                //this takes the function property(which is stored as a node property) from the "Choose Mobile Landscape Cropping Options" drop down node and executes that function
                var chooseOptionsForMobileLandscapeFuncRTI = $('#chooseOptionsForMobileLandscapeRTI').attr('data-onSelectFunc');
                var func = eval("(" + chooseOptionsForMobileLandscapeFuncRTI + ")");
                func();
            } else {
                $('#chooseOptionsForMobileLandscapeRTI').parent().hide();
                $('#cropOptionsMobileLandscapeRTI').hide();
                $('#customCropValuesMobileLandscapeRTI').hide();
            }
        }
    } else {
        $("[aria-controls='croppingDetailsRTI']").hide();
    }
}

function videoOnLoad(selectedValue) {
 
 if ($('#videoViewType :selected').val() == "youtube")
{
 $("#videoIdId").parent().show();
        $("#videoTitleId").parent().show();
        $("#subHeadLineId").parent().show();
        $("#isAdaptiveId").parent().show();
        $("#videoLinkId").parent().hide();
		$('#videoIdId').parent().show();
}
else if($('#videoViewType :selected').val() == "brightcove")
{
 $("#videoIdId").parent().show();
        $("#videoTitleId").parent().show();
        $("#subHeadLineId").parent().show();
        $("#isAdaptiveId").parent().show();
        $("#videoLinkId").parent().hide();
		$('#videoIdId').parent().show();
}
else if($('#videoViewType :selected').val() == "dam")
{
 $("#videoIdId").parent().hide();
        $("#videoTitleId").parent().show();
        $("#subHeadLineId").parent().show();
        $("#isAdaptiveId").parent().show();
        $("#videoLinkId").parent().show();
		$('#videoIdId').parent().hide();
}
else if($('#videoViewType :selected').val() == "vimeo")
{
$("#videoIdId").parent().show();
        $("#videoTitleId").parent().show();
        $("#subHeadLineId").parent().show();
        $("#isAdaptiveId").parent().show();
        $("#videoLinkId").parent().hide();
		$('#videoIdId').parent().show();
}
if($('#displayoption :selected').val() == "Embedded")
{
$("#thumbnailurl").parent().hide();
$("#thumbnailalt").parent().hide();
}
else if($('#displayoption :selected').val() == "Overlay")
{
$('#thumbnailurl').parent().show();
$('#thumbnailalt').parent().show();
}
    
}




function videoViewTypeOnSelect(event) {
    videoOnLoad(event.selected);
}

function videoDisplayOptionOnSelect()
{
if($('#displayoption :selected').val() == "Embedded")
{
$("#thumbnailurl").parent().hide();
$("#thumbnailalt").parent().hide();
}
else if($('#displayoption :selected').val() == "Overlay")
{
$('#thumbnailurl').parent().show();
$('#thumbnailalt').parent().show();
}

}

function formOnLoad(selectedValue) {
	if(!selectedValue) {
		selectedValue = 'message';
		$("#successMessageId").parent().parent().show();
		$("#successPageId").parent().hide();
	}
	if (selectedValue == 'message') {
		$("#successMessageId").parent().parent().show();
		$("#successPageId").parent().hide();
	} else if (selectedValue == 'redirect') {
		$("#successMessageId").parent().parent().hide();
		$("#successPageId").parent().show();
	} else if (selectedValue == 'store-content') {
		$("#actionConfigId").show();
	} else if(selectedValue == ''){
		$("#actionConfigId").hide();
	}

    formCreateFormId();
	formCreateContentPath();
}

function formSuccessConfigOnSelect(event) {
	formOnLoad(event.selected);
}

function formActionTypeOnSelect(event) {
	var selectedValue=event.selected;
	if (selectedValue == 'store-content') {
		$("#actionConfigId").show();
	} else if(selectedValue == ''){
		$("#actionConfigId").hide();
	}
}


function formElementOnLoad(selectedValue) {
	if (selectedValue == 'text'|| !selectedValue) {
		$("#fieldLabelId").parent().show();
		$("#elementIdId").parent().show();
		$("#descriptionId").parent().show();
		$("#multivalueId").parent().show();
		$("#textDefaultValueId").parent().show();
		$("#hideTitleId").parent().show();
		$("#dateFormatId").parent().hide();
		$("#dataRadioId").parent().hide();
		$("#dataId").parent().hide();
		$("#itemsLoadPathId").parent().hide();
		$("#multiSelectionId").parent().hide();
		$("#linkId").parent().hide();
        $("#formElementCheckBoxId").parent().hide();
        $("#openInParentWindow").parent().hide();
        $("#checkboxId").parent().hide();
        $("#checkedIndexId").parent().hide();
        $("#languageCodeId").parent().hide();
		}
	 else if (selectedValue == 'dropdown') {
		$("#fieldLabelId").parent().show();
		$("#elementIdId").parent().show();
		$("#descriptionId").parent().show();
		$("#itemsLoadPathId").parent().show();
		$("#textDefaultValueId").parent().show();
		$("#hideTitleId").parent().show();
		$("#dataId").parent().show();
		$("#multiSelectionId").parent().show();
		$("#dateFormatId").parent().hide();
		$("#dataRadioId").parent().hide();
		$("#multivalueId").parent().hide();
		$("#linkId").parent().hide();
        $("#formElementCheckBoxId").parent().hide();
        $("#openInParentWindow").parent().hide();
        $("#checkboxId").parent().hide();
        $("#checkedIndexId").parent().hide();
        $("#languageCodeId").parent().hide();
	}
	else if (selectedValue == 'radio') {
		$("#fieldLabelId").parent().show();
		$("#elementIdId").parent().show();
		$("#descriptionId").parent().show();
		$("#itemsLoadPathId").parent().show();
		$("#textDefaultValueId").parent().show();
		$("#hideTitleId").parent().show();
		$("#dataRadioId").parent().show();
		$("#dateFormatId").parent().hide();
		$("#dataId").parent().hide();
		$("#multivalueId").parent().hide();
		$("#multiSelectionId").parent().hide();
		$("#linkId").parent().hide();
        $("#formElementCheckBoxId").parent().hide();
        $("#openInParentWindow").parent().hide();
        $("#checkboxId").parent().hide();
        $("#checkedIndexId").parent().show();
        $("#languageCodeId").parent().hide();
	}
	else if (selectedValue == 'calendar') {
		$("#fieldLabelId").parent().show();
		$("#elementIdId").parent().show();
		$("#descriptionId").parent().show();
		$("#hideTitleId").parent().show();
		$("#dateFormatId").parent().show();
		$("#dataRadioId").parent().hide();
		$("#dataId").parent().hide();
		$("#itemsLoadPathId").parent().hide();
		$("#textDefaultValueId").parent().hide();
		$("#multivalueId").parent().hide();
		$("#multiSelectionId").parent().hide();
		$("#linkId").parent().hide();
        $("#formElementCheckBoxId").parent().hide();
        $("#openInParentWindow").parent().hide();
        $("#checkboxId").parent().hide();
        $("#checkedIndexId").parent().hide();
        $("#languageCodeId").parent().hide();
	}
	else if (selectedValue == 'link') {
		$("#fieldLabelId").parent().show();
		$("#elementIdId").parent().show();
		$("#descriptionId").parent().show();
		$("#hideTitleId").parent().show();
		$("#dateFormatId").parent().show();
		$("#dataRadioId").parent().hide();
		$("#dataId").parent().hide();
		$("#itemsLoadPathId").parent().hide();
		$("#textDefaultValueId").parent().hide();
		$("#multivalueId").parent().hide();
		$("#multiSelectionId").parent().hide();
		$("#linkId").parent().show();
        $("#formElementCheckBoxId").parent().hide();
        $("#openInParentWindow").parent().show();
        $("#checkboxId").parent().hide();
        $("#checkedIndexId").parent().hide();
        $("#languageCodeId").parent().hide();
	}
    else if (selectedValue == 'checkbox') {
		$("#fieldLabelId").parent().show();
		$("#elementIdId").parent().show();
		$("#descriptionId").parent().show();
		$("#hideTitleId").parent().show();
		$("#dateFormatId").parent().show();
		$("#dataRadioId").parent().hide();
		$("#dataId").parent().hide();
		$("#itemsLoadPathId").parent().hide();
		$("#textDefaultValueId").parent().hide();
		$("#multivalueId").parent().hide();
		$("#multiSelectionId").parent().hide();
		$("#linkId").parent().hide();
        $("#formElementCheckBoxId").parent().show();
        $("#openInParentWindow").parent().hide();
        $("#checkboxId").parent().hide();
        $("#checkedIndexId").parent().hide();
        $("#languageCodeId").parent().hide();
	}
    else if (selectedValue == 'multiplecheckbox') {
		$("#fieldLabelId").parent().show();
		$("#elementIdId").parent().show();
		$("#descriptionId").parent().show();
		$("#hideTitleId").parent().show();
		$("#dateFormatId").parent().show();
		$("#dataRadioId").parent().hide();
		$("#dataId").parent().hide();
		$("#itemsLoadPathId").parent().hide();
		$("#textDefaultValueId").parent().hide();
		$("#multivalueId").parent().hide();
		$("#multiSelectionId").parent().hide();
		$("#linkId").parent().hide();
        $("#formElementCheckBoxId").parent().show();
        $("#openInParentWindow").parent().hide();
        $("#checkboxId").parent().show();
        $("#checkedIndexId").parent().show();
        $("#languageCodeId").parent().hide();
	}
    else if (selectedValue == 'captcha') {
		$("#fieldLabelId").parent().show();
		$("#elementIdId").parent().show();
		$("#descriptionId").parent().show();
		$("#hideTitleId").parent().show();
		$("#dateFormatId").parent().show();
		$("#dataRadioId").parent().hide();
		$("#dataId").parent().hide();
		$("#itemsLoadPathId").parent().hide();
		$("#textDefaultValueId").parent().hide();
		$("#multivalueId").parent().hide();
		$("#multiSelectionId").parent().hide();
		$("#linkId").parent().hide();
        $("#formElementCheckBoxId").parent().show();
        $("#openInParentWindow").parent().hide();
        $("#checkboxId").parent().hide();
        $("#checkedIndexId").parent().hide();
        $("#languageCodeId").parent().show();
	}
}

function searchInputOnLoad() {
   searchInputLoadMoreOnClick();
    $('.cq-dialog-submit').click(function() {
	var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');
		if(formName && formName=='searchinput') {

        if ($("[name='./basePath']").val() == "/content" || $("[name='./basePath']").val() == "/content/") {
            alert("Search Under : Cannot search directly under content. Please enter a correct path ");
            return;
        }
        if ($("[name='./basePath']").val().indexOf('/content') !== -1) {
            alert("Search Under: please enter a proper path");
        }
        if ($("[name='./resultPagePath']").val() == "/content" || $("[name='./resultPagePath']").val() == "/content/") {
            alert("Results Page Path : Cannot search directly under content. Please enter a correct path ");
        }
        if ($("[name='./resultPagePath']").val().indexOf('/content') !== -1) {
            alert("Results Page Path : please enter a proper path");
        }
	}
    });
}

function searchInputLoadMoreOnClick() {
    if (document.getElementById('lazyLoad').checked) {
        $('#numberOfResultsSearch').parent().show();
        $('#limit').parent().hide();
    } else {
        $('#numberOfResultsSearch').parent().hide();
        $('#limit').parent().show();
    }
}

function formCreateFormId() {
    var formEl = $("input[name='./formid']").closest('form');
    if (formEl && !$("input[name='./formid']").val()) {
        var replaceSlash=$(formEl).attr('action');
        replaceSlash=replaceSlash.replace('/_','_');
        $("input[name='./formid']").val(replaceSlash.replace(/[/:.]/g, "_")).trigger('change');
    }
    $("input[name='./formid']").attr('readonly', true);
}

function formCreateContentPath() {
    var formEl = $("input[name='./action']").closest('form');
    if (formEl && !$("input[name='./action']").val()) {
        $("input[name='./action']").val("/content/usergenerated" + $(formEl).attr('action').replace(/^.content/, "").replace(/jcr.content.*/, "") + $(formEl).attr('id') + new Date().getTime()).trigger('change');
    }
    $("input[name='./action']").attr('readonly', true);
}

function hotspotOnload() {
    //loadImage('./file'); temporary fix
}
//product copy listener
function productCopyOnSelect() {

    var productCopySelectedValue = $('#source :selected').val();
    if (productCopySelectedValue == "text" || !productCopySelectedValue) {
        $("[aria-controls='textTab']").show();
        $("[aria-controls='productDataTab']").hide();
        $("#accordionTitle").attr('aria-required', 'true');
        $("#categoryTitle").attr('aria-required', 'false');
    } else if ($('#source :selected').val() == "product data") {
        $("[aria-controls='textTab']").hide();
        $("[aria-controls='productDataTab']").show();
        $("#categoryTitle").attr('aria-required', 'true');
        $("#accordionTitle").attr('aria-required', 'false');
    }
    $(document).on("click", ".cq-dialog-submit", function (e) {
		var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');
		if(formName && formName=='productcopy') {
        var $selectedSource = $('#source :selected').val(),
            $textTabHeading = $('#accordionTitle').val(),
            $productDataTabHeading = $('#categoryTitle').val();

        if($selectedSource == "text") {
            if(!$textTabHeading) {
				var $textTab = $("#accordionTitle").closest("#textTab").attr("id");
                if($textTab) {
                    $('a[aria-controls="' + $textTab + '"]').trigger("click");
                }
            }
		} else if($selectedSource == "product data") {
            if(!$productDataTabHeading) {
				var $productDataTab = $("#categoryTitle").closest("#productDataTab").attr("id");
                if($productDataTab) {
                    $('a[aria-controls="' + $productDataTab + '"]').trigger("click");
                }
            }
			if($(".coral-Multifield-list li [name='./productCopyPropertyName']").length==0){
            $(window).adaptTo("foundation-ui").alert("Required","Please configure at least one property attribute");
            e.preventDefault();
            return false;
      	}
		}

}
    });
}



//------------------- Whats New component. This function will hide/unhide Product/ Product Range Tab depending on what the user selects---------------------------


function whatsNewOnSubmit()
{

     $(document).on("click", ".cq-dialog-submit", function (event) {
	 	var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');
		if(formName && formName=='whatsnew') {
	 if ($('#selectProductOrRange :selected').val() == "product") 
	 {
	 if ($('#productPopulationType :selected').val() == "Manual")
	 {
	    if($(".coral-Multifield-list li [name='./addProduct']").length==0){
            $(window).adaptTo("foundation-ui").alert("Required","Please configure at least one product");
            //event.preventDefault();
            return false;
      	}
         if($(".coral-Multifield-list li [name='./addProduct']").length>5){
            $(window).adaptTo("foundation-ui").alert("Error","Maximum of 5 products are allowed");
            //event.preventDefault();
            return false;
      	}
	 }
		 
	}
		else 
		{
		 if ($('#rangePopulationType :selected').val() == "Manual")
		 {
			 if($(".coral-Multifield-list li [name='./rangeImage']").length==0){
            $(window).adaptTo("foundation-ui").alert("Required","Please configure at least one product range");
            //event.preventDefault();
            return false;
      	}
              if($(".coral-Multifield-list li [name='./rangeImage']").length>5){
            $(window).adaptTo("foundation-ui").alert("Error","Maximum of 5 ranges are allowed");
            //event.preventDefault();
            return false;
      	}
		 }
		 else 
		 {
		  if($(".coral-Multifield-list li [name='./prodRangePageAuto']").length==0){
            $(window).adaptTo("foundation-ui").alert("Required","Please configure at least one product range");
            //event.preventDefault();
            return false;
      	}
             if($(".coral-Multifield-list li [name='./prodRangePageAuto']").length>5){
            $(window).adaptTo("foundation-ui").alert("Error","Maximum of 5 ranges are allowed");
            //event.preventDefault();
            return false;
      	}
		 }
		
		
		}
	 }
	 });

}



function whatsNewProdOrRangeOnSelect()
{


if ($('#selectProductOrRange :selected').val() == "product")  //product tab is displayed
{ 
      $("[aria-controls='productsTab']").show(); //show products tab
      $("[aria-controls='productsTab']").prop('disabled', false);
      $("[aria-controls='productRangeTab']").prop('disabled', true); // hide the product range tab
      $("[aria-controls='productRangeTab']").hide(); 
      
	  $("[name='./productTitle']").attr("aria-required",true);

	  
	  $("[name='./addProduct']").attr("aria-required",true);
	$("[name='./CTAButtonTextProduct']").attr("aria-required",true);
	  $("[name='./prodCtaLabelAuto']").attr("aria-required",true);
	  
      // disable individual fields in the product range tab

	  $("[name='./rangeTitle']").attr("aria-required",false);
      $("[name='./rangeImage']").attr("aria-required",false);
	  $("[name='./rangeImageAltText']").attr("aria-required",false);
	  
    $("[name='./rangeName']").attr("aria-required",false);
    $("[name='./rangeShortCopy']").attr("aria-required",false);
    $("[name='./CTAButtonText']").attr("aria-required",false);
    $("[name='./CTAButtonLink']").attr("aria-required",false);
	$("[name='./CTAButtonTextAuto']").attr("aria-required",false);
	$("[name='./prodRangePageAuto']").attr("aria-required",false);


}

else  //Product Range tab is displayed
{
	$("[aria-controls='productsTab']").hide(); //hide products tab
   // $("[aria-controls='productsTab']").prop('disabled', true);
	$("[aria-controls='productRangeTab']").prop('disabled', false);
    $("[aria-controls='productRangeTab']").show(); //shor products range tab
	$("[name='./productTitle']").attr("aria-required",false);

	
	$("[name='./addProduct']").attr("aria-required",false);
	$("[name='./CTAButtonTextProduct']").attr("aria-required",false);
	$("[name='./prodCtaLabelAuto']").attr("aria-required",false);
	
		  $("[name='./rangeTitle']").attr("aria-required",true);
      $("[name='./rangeImage']").attr("aria-required",true);
	  $("[name='./rangeImageAltText']").attr("aria-required",true);
	  
    $("[name='./rangeName']").attr("aria-required",true);
    $("[name='./rangeShortCopy']").attr("aria-required",true);
    $("[name='./CTAButtonText']").attr("aria-required",true);
    $("[name='./CTAButtonLink']").attr("aria-required",true);
	$("[name='./CTAButtonTextAuto']").attr("aria-required",true);
	$("[name='./prodRangePageAuto']").attr("aria-required",true);
    


}

}

//Whats New component. Hide/unhide manual entry of products
function whatsNewProductPopulationType()
{


	
	if($('#selectProductOrRange :selected').val() == "product")
	{
if ($('#productPopulationType :selected').val() == "Manual")  //Manual entry is enables
{ 

	
    $("[data-name='./addProducts']").attr("style","display:block");
	 
    $("[name='./prodCtaLabelAuto']").attr("aria-required",false);
    $("[name='./prodCtaLabelAuto']").parent().hide();
	
	$("[name='./addProduct']").attr("aria-required",true);
	$("[name='./CTAButtonTextProduct']").attr("aria-required",true);
	$("[data-fieldname='./filterTagsNamespace']").parent().parent().hide();
	
}

else if ($('#productPopulationType :selected').val() == "Automatic")  //manual entry is disabled
{
   $("[data-name='./addProducts']").attr("style","display:none");
	
    $("[name='./prodCtaLabelAuto']").attr("aria-required",true);
    $("[name='./prodCtaLabelAuto']").parent().show();
	
	
	$("[name='./addProduct']").attr("aria-required",false);
	$("[name='./CTAButtonTextProduct']").attr("aria-required",false);
	$("[data-fieldname='./filterTagsNamespace']").parent().parent().show();

   }
   }


}
function whatsNewRangePopulationType()
{ 

if($('#selectProductOrRange :selected').val() == "productRange")
{

    if ($('#rangePopulationType :selected').val() == "Manual")  //Manual entry is enabled
{
    $("[data-name='./numLinksAuto']").prev().hide();
	$("[data-name='./numLinks']").prev().show();

  
    



        $("[name='./rangeTitle']").attr("aria-required",true);
        $("[name='./rangeImage']").attr("aria-required",true);
		$("[name='./rangeImageAltText']").attr("aria-required",true);
        $("[name='./rangeName']").attr("aria-required",true);
        $("[name='./rangeShortCopy']").attr("aria-required",true);
        $("[name='./CTAButtonText']").attr("aria-required",true);
        $("[name='./CTAButtonLink']").attr("aria-required",true);
		
		$("[name='./CTAButtonTextAuto']").attr("aria-required",false);
		$("[name='./prodRangePageAuto']").attr("aria-required",false);

		
		


}
    else
    {
        
              
		 $("[data-name='./numLinksAuto']").prev().show();
	$("[data-name='./numLinks']").prev().hide();




        $("[name='./CTAButtonTextAuto']").attr("aria-required",true);

        

        $("[name='./prodRangePageAuto']").attr("aria-required",true);

		$("[name='./rangeTitle']").attr("aria-required",false);
        $("[name='./rangeImage']").attr("aria-required",false);
		$("[name='./rangeImageAltText']").attr("aria-required",false);
        $("[name='./rangeName']").attr("aria-required",false);
        $("[name='./rangeShortCopy']").attr("aria-required",false);
        $("[name='./CTAButtonText']").attr("aria-required",false);
        $("[name='./CTAButtonLink']").attr("aria-required",false);
		
		
    }
	}


}



 
//product story listeners
function productStoryOnLoadFunc(_self, selectedValue) {
    productStoryOnSelection($(_self).closest("li"), selectedValue);
}

function productStoryOnSelect(event) {
    productStoryOnSelection($(event.target).closest("li"), event.selected);
}

function productStoryOnSelection(parentLI, selectedValue) {
$(document).on("click", ".cq-dialog-submit", function (e) {
	var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');
		if(formName && formName=='homestory') {
        var $storyTitleOne = parentLI.find("#storyTitle").val(),
            $leftImageOne = parentLI.find("[name='./leftImageStory']").val(),
            $rightImageOne = parentLI.find("[name='./rightImageStory']").val(),
            $labelTextOne = parentLI.find('#labelFieldText').val(),
            $linkTextOne = parentLI.find("[name='./ctaLinkText']").val(),
            $altLeftImage = parentLI.find("[name='./altLeft']").val(),
            $altRightImage = parentLI.find("[name='./altRight']").val(),
            $altCentreImage = parentLI.find("[name='./altCentre']").val(),
			shortCopy = parentLI.find("#homeStoryShortCopy").val();
            if(!$storyTitleOne || !shortCopy || !$leftImageOne || !$rightImageOne || !$labelTextOne || !$linkTextOne || !$altLeftImage || !$altRightImage || !$altCentreImage) {
				var $textTab = $("[name='./homeStoryTitle']").closest("#productStoryTab").attr("id");
                if($textTab) {
                    $('a[aria-controls="' + $textTab + '"]').trigger("click");
                }
           }
		}
    });
}

function productStoryAnchorOnSelect(){
 var $anchorLink = $("#labelId").val();
    if (document.getElementById('showAsNavigation').checked) {
    $("input[type='text'][name='./label']").attr('aria-required', 'true');
}
else{
$("input[type='text'][name='./label']").attr('aria-required', 'false');
$("input[type='text'][name='./label']").attr('aria-invalid', 'false');
$("input[type='text'][name='./label']").attr('class', 'coral-Form-field coral-Textfield');
$("input[type='text'][name='./label']").next('span').remove();

}
}


//body copy steps listener before submit
function bodyCopyStepsOnSubmit(){
    var imageneeded = false;
	 $(document).on("click", ".cq-dialog-submit", function (event) {

	 	var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');
          if (formName && formName=='bodycopysteps' && document.getElementById('isImage').checked) {
				imageneeded = true;
              
                  $(".coral-Multifield-list li [name='./image']").attr('aria-required', 'true');
                   $(".coral-Multifield-list li [name='./altImage']").attr('aria-required', 'true');

			}

		if(formName && formName=='bodycopysteps') {
		    if($(".coral-Multifield-list li [name='./step']").length==0){
            $(window).adaptTo("foundation-ui").alert("Required","Please configure at least one body copy list");
            event.preventDefault();
            return false;
      	}

	 checkRichTextMandatory(event);
	 }
	 });
}

function bodyCopyOnLoad(){
     if (document.getElementById('isImage').checked) {
     $(".coral-Multifield-list li [id='tipsImage']").parent().show();

		 $(".coral-Multifield-list li [id='altImage']").parent().show();
	}
    else
    {
       $(".coral-Multifield-list li [id='tipsImage']").parent().hide();

		 $(".coral-Multifield-list li [id='altImage']").parent().hide();

    }

}



function checkRichTextMandatory(event) {
   var anchorLink = $("#bodyCopyStepsLabelId").val();
    $('input:hidden[name="./copyStepDescription"]').each(function() {
             $("a[aria-controls='bodyCopyStepsTab']").trigger("click");
         if($(this).val().length==0 && $(this).closest(".coral-Form-fieldset").find('input[name="./copyStepHeadline"]').val()) {
             alert("Please fill description field");
                 event.preventDefault();
                 return false;
         }
     });
}

//category overview on submit function for adding minimum 1 navigation list
function categoryOverviewOnSubmit() {
    $(document).on("click", ".cq-dialog-submit", function(event) {
		var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');
		if(formName && formName=='categoryoverview') {
        if ($(".coral-Multifield-list li [name='./label']").length == 0) {
            $(window).adaptTo("foundation-ui").alert("Required", "Please configure at least one navigation list.");
            event.preventDefault();
            return false;
        }
        checkCategoryOverviewMandatory(event);
		}
    });
}

//zip file download on submit function for adding minimum 1 list
function zipFileDownloadOnSubmit() {
    $(document).on("click", ".cq-dialog-submit", function(event) {
		var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');
		if(formName && formName=='zipfiledownload') {
        if ($(".coral-Multifield-list li [name='./path']").length == 0) {
            $(window).adaptTo("foundation-ui").alert("Required", "Please configure at least one file for zip.");
            event.preventDefault();
            return false;
        }
		}
    });
}

function checkCategoryOverviewMandatory(event) {
    $('input[type=text][name="./subHeading"]').each(function() {
        if ($(this).val().length > 0 && !($(this).closest(".coral-Form-fieldset").find('input[name="./label"]').val())) {
            $(window).adaptTo("foundation-ui").alert("Required", "At least one navigation link must be added to the list.");
            event.preventDefault();
            return false;
        }
    });
}


//home story on submit function for minimum sections
function homeStoryOnSubmit(){
    $(document).on("click", ".cq-dialog-submit", function (event) {
		var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');
		if(formName && formName=='homestory') {
		var $anchorLink = $("#labelId").val();
        if($(".coral-Multifield-list li [name='./homeStoryTitle']").length==0){
            $(window).adaptTo("foundation-ui").alert("Required","Please configure at least one story section");
            event.preventDefault();
            return false;
      	}
		if (document.getElementById('showAsNavigation').checked) {
                  if(!$anchorLink){
                      var $anchorTab = $("#labelId").closest("#anchorLinkTab").attr("id");
                      if($anchorTab){
                          $('a[aria-controls="' + $anchorTab + '"]').trigger("click");
						  event.stopPropagation();
                      }
                  }
            }
		}
    });
}

//multiple related products article on submit function for minimum product
function multipleRelatedProductsArticleOnSubmit(){
    $(document).on("click", ".cq-dialog-submit", function (event) {
		var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');
		if(formName && formName=='multiplerelatedproductsarticle') {
        if($(".coral-Multifield-list li [name='./addProduct']").length==0){
            $(window).adaptTo("foundation-ui").alert("Required","Please select at least one product path");
            event.preventDefault();
            return false;
      	} 
        if($(".coral-Multifield-list li [name='./addProduct']").length>6){
            $(window).adaptTo("foundation-ui").alert("Attention Please","You can only select a maximum of 6 products");
            event.preventDefault();
            return false;
      	} 
		}
    });
}



//featured product listener
function featuredProductOnSelect() {
    var featuredProductSelectedValue = $('#featuredProductType :selected').val();
    if (featuredProductSelectedValue == "Auto" || !featuredProductSelectedValue) {
        $("#productPage").parent().hide();
        $("#featuredProductInfo").parent().hide();
        $("input[type='text'][name='./productPage']").attr('aria-required', 'false');
		$("[data-fieldname='./filterTagsNamespace']").parent().parent().show();
    } else if ($('#featuredProductType :selected').val() == "Manual") {
        $("#productPage").parent().show();
        $("#featuredProductInfo").parent().show();
        $("input[type='text'][name='./productPage']").attr('aria-required', 'true');
		$("[data-fieldname='./filterTagsNamespace']").parent().parent().hide();
    }
    $(document).on("click", ".cq-dialog-submit", function (e) {
		var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');
		if(formName && formName=='featuredproduct') {
        var selectedSource = $('#featuredProductType :selected').val();
        var background = $("[name='./featuredbackgroundImage']").val();
        var pagePath = $("[name='./productPage']").val();
        var ctaLabel = $("[name='./ctaLabel']").val();
        var anchor = $("[name='./label']").val();
        if(selectedSource){
            if(selectedSource == "Auto") {
            if(!background || !ctaLabel) {
				var $textTab = $("#featuredProductType").closest("#featuredProduct").attr("id");
                if($textTab) {
                    $('a[aria-controls="' + $textTab + '"]').trigger("click");
                }
            }
		} else if(selectedSource == "Manual") {
            if(!background || !ctaLabel || !pagePath) {
				var $productDataTab = $("#featuredProductType").closest("#featuredProduct").attr("id");
                if($productDataTab) {
                    $('a[aria-controls="' + $productDataTab + '"]').trigger("click");
                }
            }
        }
        }

}
    });

}

function featuredProductAnchorOnSelect(){
    if (document.getElementById('showAsNavigationFeaturedProduct').checked) {
    $("input[type='text'][name='./label']").attr('aria-required', 'true');
}
else{
$("input[type='text'][name='./label']").attr('aria-required', 'false');
$("input[type='text'][name='./label']").attr('aria-invalid', 'false');
$("input[type='text'][name='./label']").attr('class', 'coral-Form-field coral-Textfield');
$("input[type='text'][name='./label']").next('span').remove();

}
}

function featuredProductOnSubmit() {
    $(document).on("click", ".cq-dialog-submit", function (event) {
		var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');
		if(formName && formName=='featuredproduct') {
		if (document.getElementById('showAsNavigationFeaturedProduct').checked) {
            var $anchorLink = $("#labelId").val();
                  if(!$anchorLink){
                      var $anchorTab = $("#labelId").closest("#anchorLinkTabFeatured").attr("id");
                      if($anchorTab){
                          $('a[aria-controls="' + $anchorTab + '"]').trigger("click");
                      }
                  }
            }
		}
    });
}

//featured product range listener
function featuredProductRangeOnSelect() {
    var featuredProductSelectedValue = $('#featuredProductRangeType :selected').val();
    if (featuredProductSelectedValue == "Auto" || !featuredProductSelectedValue) {
        $("#featuredProductRangePage").parent().show();
        $("#featuredProductRangeImage").parent().hide();
        $("#altFeaturedProductRangeImage").parent().hide();
        $("#featuredProductRangeName").parent().hide();
        $("#featuredProductRangeShortCopy").parent().hide();
        $("#featuredProductNavigationURL").parent().hide();
        $("input[type='text'][name='./productRangePage']").attr('aria-required', 'true');
		$("input[type='text'][name='./productShortCopy']").attr('aria-required', 'false');
        $("input[type='text'][name='./productImage']").attr('aria-required', 'false');
        $("input[type='text'][name='./productName']").attr('aria-required', 'false');
        $("input[type='text'][name='./navigationURL']").attr('aria-required', 'false');
        $("input[type='text'][name='./altImage']").attr('aria-required', 'false');
    } else if ($('#featuredProductRangeType :selected').val() == "Manual") {
        $("#featuredProductRangePage").parent().hide();
        $("#featuredProductRangeImage").parent().show();
        $("#altFeaturedProductRangeImage").parent().show();
        $("#featuredProductRangeName").parent().show();
        $("#featuredProductRangeShortCopy").parent().show();
        $("#featuredProductNavigationURL").parent().show();
        $("input[type='text'][name='./productRangePage']").attr('aria-required', 'false');
		$("input[type='text'][name='./productShortCopy']").attr('aria-required', 'true');
        $("input[type='text'][name='./productImage']").attr('aria-required', 'true');
        $("input[type='text'][name='./productName']").attr('aria-required', 'true');
        $("input[type='text'][name='./navigationURL']").attr('aria-required', 'true');
         $("input[type='text'][name='./altImage']").attr('aria-required', 'true');
    }


}


//spotlight listener
function spotlightOnSelect() {
    var spotlightSelectedValue = $('#spotlightType :selected').val();
    if (spotlightSelectedValue == "Auto" || !spotlightSelectedValue) {
        $("#spotlightForegroundImage").parent().hide();
        $("#featuredProductRangePage").parent().show();
        $("#altspotlightForegroundImage").parent().hide();
		$("#featuredCategoryType").parent().show();
        $("#spotlightTheme").parent().hide();
        $("#spotlightRTE").parent().hide();
        $("#spotlightExpirynotice").parent().hide();
        $("#spotlightNavigationURL").parent().hide();
        $("input[type='text'][name='./foregroundImage']").attr('aria-required', 'false');
		$("input[type='text'][name='./rte']").attr('aria-required', 'false');
        $("input[type='text'][name='./altForegroundImage']").attr('aria-required', 'false');
        $("input[type='text'][name='./expirynotice']").attr('aria-required', 'false');
        $("input[type='text'][name='./navigationURL']").attr('aria-required', 'false');
		 $("input[type='text'][name='./landingPage']").attr('aria-required', 'true');
    } else if (spotlightSelectedValue == "Manual") {
        $("#spotlightForegroundImage").parent().show();
        $("#featuredProductRangePage").parent().hide();
        $("#altspotlightForegroundImage").parent().show();
        $("#featuredCategoryType").parent().hide();
        $("#spotlightTheme").parent().show();
        $("#spotlightRTE").parent().show();
        $("#spotlightExpirynotice").parent().show();
        $("#spotlightNavigationURL").parent().show();
        $("input[type='text'][name='./foregroundImage']").attr('aria-required', 'true');
		$("input[type='text'][name='./rte']").attr('aria-required', 'true');
        $("input[type='text'][name='./altForegroundImage']").attr('aria-required', 'true');
        $("input[type='text'][name='./expirynotice']").attr('aria-required', 'true');
        $("input[type='text'][name='./navigationURL']").attr('aria-required', 'true');
		 $("input[type='text'][name='./landingPage']").attr('aria-required', 'false');
    }


}

function spotlightOnSubmit() {
    $(document).on("click", ".cq-dialog-submit", function (event) {

            var startDate = $("input[type='hidden'][name='./startDate']").val();
        	var endDate = $("input[type='hidden'][name='./endDate']").val();
        	var startd = new Date(startDate);
        	var endd = new Date(endDate);
        	if(endd < startd)
            {
            $(window).adaptTo("foundation-ui").alert("EndDate", "Please enter end date greater than start date");
            event.preventDefault();
            return false;
            }







    });
}

function featuredProductRangeAnchorOnSelect(){
    if (document.getElementById('featuredProductRangeShowAsNavigation').checked) {
    $("input[type='text'][name='./label']").attr('aria-required', 'true');
}
else{
$("input[type='text'][name='./label']").attr('aria-required', 'false');
$("input[type='text'][name='./label']").attr('aria-invalid', 'false');
$("input[type='text'][name='./label']").attr('class', 'coral-Form-field coral-Textfield');
$("input[type='text'][name='./label']").next('span').remove();

}
}

function featuredProductRangeOnSubmit() {
    $(document).on("click", ".cq-dialog-submit", function (event) {
		var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');
		if(formName && formName=='featuredproductrange') {
                    var selectedSource = $('#featuredProductRangeType :selected').val();
        var background = $("[name='./backgroundRangeImage']").val();
        var pagePath = $("[name='./productRangePage']").val();
        var ctaLabel = $("[name='./ctaLabel']").val();
        var productImage = $("[name='./productImage']").val();
        var productName = $("[name='./productName']").val();
        var navigationURL = $("[name='./navigationURL']").val();
        var altRangeImage = $("#altFeaturedProductRangeImage").val();
        if(selectedSource){
            if(selectedSource == "Auto") {
            if(!background || !ctaLabel || !pagePath) {
				var $textTab = $("#featuredProductRangeType").closest("#featuredProductRangeTab").attr("id");
                if($textTab) {
                    $('a[aria-controls="' + $textTab + '"]').trigger("click");
                }
            }
		} else if(selectedSource == "Manual") {
            if(!background || !ctaLabel || !productImage || !productName || !navigationURL || !altRangeImage) {
				var $productDataTab = $("#featuredProductRangeType").closest("#featuredProductRangeTab").attr("id");
                if($productDataTab) {
                    $('a[aria-controls="' + $productDataTab + '"]').trigger("click");
                }
            }
        }
        }

		if (document.getElementById('featuredProductRangeShowAsNavigation').checked) {
            var $anchorLink = $("#labelId").val();
                  if(!$anchorLink){
                      var $anchorTab = $("#labelId").closest("#anchorLinkTabFeaturedProduct").attr("id");
                      if($anchorTab){
                          $('a[aria-controls="' + $anchorTab + '"]').trigger("click");
                      }
                  }
            }
		}
    });
}



function featuredArticleOnSubmit() {
    $(document).on("click", ".cq-dialog-submit", function (event) {
		var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');
		if(formName && formName=='featuredarticle') {
		if (document.getElementById('featuredShowAsNavigation').checked) {
            var $anchorLink = $("#labelId").val();
                  if(!$anchorLink){
                      var $anchorTab = $("#labelId").closest("#featuredArticleTab").attr("id");
                      if($anchorTab){
                          $('a[aria-controls="' + $anchorTab + '"]').trigger("click");
                      }
                  }
            }
		}
    });
}

function featuredArticleAnchorOnSelect(){
    if (document.getElementById('featuredShowAsNavigation').checked) {
    $("input[type='text'][name='./label']").attr('aria-required', 'true');
}
else{
$("input[type='text'][name='./label']").attr('aria-required', 'false');
$("input[type='text'][name='./label']").attr('aria-invalid', 'false');
$("input[type='text'][name='./label']").attr('class', 'coral-Form-field coral-Textfield');
$("input[type='text'][name='./label']").next('span').remove();

}
}

//featured product range listener
function featuredArticleOnLoad() {
    var featuredArticleSelectedValue = $('#featuredArticleType :selected').val();
    if (featuredArticleSelectedValue == "Auto" || !featuredArticleSelectedValue) {
        $("#featuredArticlePage").parent().hide();
        $("input[type='text'][name='./articlePage']").attr('aria-required', 'false');
		$("[data-fieldname='./filterTagsNamespace']").parent().parent().show();
    } else if ($('#featuredArticleType :selected').val() == "Manual") {
        $("#featuredArticlePage").parent().show();
        $("input[type='text'][name='./articlePage']").attr('aria-required', 'true');
		$("[data-fieldname='./filterTagsNamespace']").parent().parent().hide();
    }
    $(document).on("click", ".cq-dialog-submit", function (e) {
		var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');
		if(formName && formName=='featuredarticle') {
        var selectedSource = $('#featuredArticleType :selected').val();
        var pagePath = $("[name='./articlePage']").val();
        var ctaLabel = $("[name='./ctaLabel']").val();
        if(selectedSource){
            if(selectedSource == "Auto") {
            if(!ctaLabel) {
				var $textTab = $("#featuredArticleType").closest("#featuredArticleTypeTab").attr("id");
                if($textTab) {
                    $('a[aria-controls="' + $textTab + '"]').trigger("click");
                }
            }
		}
            else if(selectedSource == "Manual") {
            if(!ctaLabel || !pagePath) {
				var $textTab = $("#featuredArticleType").closest("#featuredArticleTypeTab").attr("id");
                if($textTab) {
                    $('a[aria-controls="' + $textTab + '"]').trigger("click");
                }
            }
		}
        }

}
    });

}

function globalNavigationOnLoad(event, selectedValue) {

    $("select[name*='./selectTab']").each(function(){
        var nameAtt = $(this).attr('name');
        var lastChar = nameAtt[nameAtt.length-1];
        handleShowHideOfGlobalNavigation($(this).val(),lastChar);
        globalNavigationDisplayImageOnSelect(event);
    });
    showHideGlobalNavTabs($("select[name*='./tabCount']").val());
    
}
function globalNavigationTabCountOnSelect(event) {
    showHideGlobalNavTabs($("select[name*='./tabCount']").val());
}

function globalNavigationSelectTabOnSelect(event) {
    var targetSelect = $( event.target).find( "select" )[0].name;
	var lastChar = targetSelect[targetSelect.length-1];

    handleShowHideOfGlobalNavigation(event.selected,lastChar);
}

function globalNavigationDisplayImageOnSelect(event) {
	$("input[name*='./displayBackgroundImage']").each(function(){
		var nameAtt = $(this).attr('name');
        var lastChar = nameAtt[nameAtt.length-1];
        if (this.checked) {
    		$("#backgroundImageId" + lastChar).parent().show();
			$("#backgroundImageAltId" + lastChar).parent().show();
    	} else {
    		$("#backgroundImageId" + lastChar).parent().hide();
			$("#backgroundImageAltId" + lastChar).parent().hide();
    	}
	});
}

function handleShowHideOfGlobalNavigation(selectedValue, lastChar) {
	if(!selectedValue) {
		selectedValue = 'custom';
		$("#slideTypeId"+lastChar).parent().show();
		$("#titleId"+lastChar).parent().hide();
		$("#parentPageId"+lastChar).parent().hide();
		$("#multiLinksId"+lastChar).parent().hide();
	}
	if (selectedValue == 'custom') {
		$("#slideTypeId"+lastChar).parent().show();
		$("#titleId"+lastChar).parent().hide();
		$("#parentPageId"+lastChar).parent().hide();
		$("#multiLinksId"+lastChar).parent().hide();
	} else if (selectedValue == 'manual') {
		$("#slideTypeId"+lastChar).parent().hide();
		$("#titleId"+lastChar).parent().show();
		$("#parentPageId"+lastChar).parent().hide();
		$("#multiLinksId"+lastChar).parent().show();
	} else if (selectedValue == 'auto') {
		$("#slideTypeId"+lastChar).parent().hide();
		$("#titleId"+lastChar).parent().hide();
		$("#parentPageId"+lastChar).parent().show();
		$("#multiLinksId"+lastChar).parent().hide();
	}
}

function showHideGlobalNavTabs(tabNumber) {
    if(!tabNumber) {
		tabNumber=1;
    }
    for (var i = 1; i <= 7; ++i) {
        $("[aria-controls='navItem"+i+"']").hide();
    }

	for (var i = 1; i <= tabNumber; ++i) {
        $("[aria-controls='navItem"+i+"']").show();
    }
}

//articleListing
function articleListingOnLoad(){
var ArticleTypeSelectedValue = $('#articleType :selected').val();
    if(ArticleTypeSelectedValue == 'Auto' || !ArticleTypeSelectedValue){
	 $("[aria-controls='manualTab']").hide(); 
        $("[aria-controls='autoTab']").show();
    }
    else if (ArticleTypeSelectedValue == "Manual") {
		 $("[aria-controls='autoTab']").hide();
         $("[aria-controls='manualTab']").show();
    }
}

//articleListing on submit
function articleListingOnSubmit(){
		$(document).on("click", ".cq-dialog-submit", function (event) {
		 var ArticleTypeSelectedValue = $('#articleType :selected').val();
	 	var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');
		if(formName && formName=='pagelisting' && ArticleTypeSelectedValue == 'Manual') {
		    if($(".coral-Multifield-list li [name='./articlePath']").length==0){
            $(window).adaptTo("foundation-ui").alert("Required","Please configure at least one page path list");
            event.preventDefault();
            return false;
      	} 
	 }
	 });	 
      
}

//featured category listener
function featuredCategoryOnSelect() {
    var featuredCategorySelectedValue = $('#featuredCategoryType :selected').val();
    if (featuredCategorySelectedValue == "Product" || !featuredCategorySelectedValue) {
        $("#featuredCategoryBackgroundImage").parent().show();
		$("#featuredCategoryAltBackgroundImage").parent().show();
        $("input[type='text'][name='./backgroundImage']").attr('aria-required', 'true');
		$("input[type='text'][name='./altBackgroundImage']").attr('aria-required', 'true');
	} else {
        $("#featuredCategoryBackgroundImage").parent().hide();
		$("#featuredCategoryAltBackgroundImage").parent().hide();
        $("input[type='text'][name='./backgroundImage']").attr('aria-required', 'false');
		$("input[type='text'][name='./altBackgroundImage']").attr('aria-required', 'false');
    }
}

//social gallery carousel expanded listener
function socialGalleryCarouselExpandedOnSelect() {

    var socialGalleryCarouselExpandedSelectedValue = $('#type :selected').val();
    if (socialGalleryCarouselExpandedSelectedValue == "automated" || !socialGalleryCarouselExpandedSelectedValue) {
        $("[aria-controls='automatedTab']").show();
        $("[aria-controls='manualTab']").hide();
    } else if ($('#type :selected').val() == "manual") {
        $("[aria-controls='automatedTab']").hide();
        $("[aria-controls='manualTab']").show();
    }
    $(document).on("click", ".cq-dialog-submit", function (e) {
		var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');
		if(formName && formName=='socialgallerycarouselexpanded') {
        var $selectedSource = $('#type :selected').val(),
            $automatedTabHeading = $('#automatedTitle').val(),
            $manualTabHeading = $('#manualTitle').val();

        if($selectedSource == "automated") {
            if(!$automatedTabHeading) {
				var $automatedTab = $("#automatedTitle").closest("#automatedTab").attr("id");
                if($automatedTab) {
                    $('a[aria-controls="' + $automatedTab + '"]').trigger("click");
                }
            }
		} else if($selectedSource == "manual") {
            if(!$manualTabHeading) {
				var $manualTab = $("#manualTitle").closest("#manualTab").attr("id");
                if($manualTab) {
                    $('a[aria-controls="' + $manualTab + '"]').trigger("click");
                }
            }
			
      	}
		}

    });
}

//social gallery video. Image section tab
function imageSectionRequiredOnClick() {
        if (document.getElementById('isImageSectionReq').checked) {
            $("[aria-controls='imageSection']").prop('disabled', false);
            $("[aria-controls='imageSection']").show(); //displaying the image section details tab

        } else {
            $("[aria-controls='imageSection']").hide();
            $("[aria-controls='imageSection']").prop('disabled', true);
        }
}
//social gallery video. Video section tab
function videoSectionRequiredOnClick() {
        if (document.getElementById('isVideoSectionReq').checked) {
            $("[aria-controls='videoSection']").prop('disabled', false);
            $("[aria-controls='videoSection']").show(); //displaying the Video section details tab

        } else {
            $("[aria-controls='videoSection']").hide();
            $("[aria-controls='videoSection']").prop('disabled', true);
        }
		$("#channelPlayListId").parent().show();
   if (document.getElementById('fixedListId').checked) {
		 $("#socialGalleryVideoPostsMultifieldId").parent().hide();
        $("#socialGalleryFixedVideoId").parent().show();
    }
    else{
        $("#socialGalleryVideoPostsMultifieldId").parent().show();
        $("#socialGalleryFixedVideoId").parent().hide();

    }

}
//featured tweet TAB listener
function featuredTwitterPostOnSubmit() {

    $(document).on("click", ".cq-dialog-submit", function(event) {

        var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');
        if (formName && formName == 'featuredtwitterpost') {
            if ($(".coral-Multifield-list li [name='./tweetId']").length > 1) {
				 $("input[type='hidden'][name='./startDate']").attr('aria-required', 'true');
                $("input[type='text'][name='./rotationPeriod']").attr('aria-required', 'true');
                if (!($("input[type='hidden'][name='./startDate']").val())) {
                    $(window).adaptTo("foundation-ui").alert("Required", "Please specify the start date");
                    event.preventDefault();
                    return false;
                }

            }
        }
    });
}	
//featured tweet TAB listener
function featuredTweetTABOnSubmit() {

    $(document).on("click", ".cq-dialog-submit", function(event) {
        var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');
        if (formName && formName == 'featuredtweettab') {
            if ($(".coral-Multifield-list li [name='./imagePath']").length > 1) {
                $("input[type='hidden'][name='./startDate']").attr('aria-required', 'true');
                $("input[type='text'][name='./rotationPeriod']").attr('aria-required', 'true');
                if (!($("input[type='hidden'][name='./startDate']").val())) {
                    $(window).adaptTo("foundation-ui").alert("Required", "Please specify the start date");
                    event.preventDefault();
                    return false;
                }

            }
        }
    });
}
//Social Carousel Expanded View listener
function expandedViewRequiredOnClick() {
        if (document.getElementById('isExpandedViewReq').checked) {
            $("[aria-controls='expandedView']").prop('disabled', false);
            $("[aria-controls='expandedView']").show(); //displaying the Video section details tab

        } else {
            $("[aria-controls='expandedView']").hide();
            $("[aria-controls='expandedView']").prop('disabled', true);
        }
}

//---------------- quick poll--------
function togglePollOptions() {
    var opinionTypeSelectedValue = $('#opinionType :selected').val();
    if (opinionTypeSelectedValue == "text" || !opinionTypeSelectedValue) {
        $("#textOnlyMultifield").parent().show();
        $("#textAndImageMultifield").parent().hide();
        $("[data-name='./freeTextFieldset']").hide();
		$("[name='./answerText']").attr("aria-required",true);
		$("[name='./opinionText']").attr("aria-required",false);
        $("[name='./opinionImage']").attr("aria-required",false);
        $("[name='./opinionImageAltText']").attr("aria-required",false);
    } else if (opinionTypeSelectedValue == "textAndImage") {
        $("#textOnlyMultifield").parent().hide();
        $("#textAndImageMultifield").parent().show();
        $("[data-name='./freeTextFieldset']").hide();
		$("[name='./answerText']").attr("aria-required",false);
		$("[name='./opinionText']").attr("aria-required",false);
        $("[name='./opinionImage']").attr("aria-required",true);
        $("[name='./opinionImageAltText']").attr("aria-required",true);
    } else {
        $("#textOnlyMultifield").parent().hide();
        $("#textAndImageMultifield").parent().hide();
        $("[data-name='./freeTextFieldset']").show();
		$("[name='./answerText']").attr("aria-required",false);
		$("[name='./opinionText']").attr("aria-required",false);
        $("[name='./opinionImage']").attr("aria-required",false);
        $("[name='./opinionImageAltText']").attr("aria-required",false);
    }
}

function toggleDataVisualization(){

                if ($('#dataPresentation :selected').val() == "recommendedContent"){
                    
                          $("[data-name='./recommendPages']").show();

                }
                else if($('#dataPresentation :selected').val() == "dataVisual"){
                                  $("[data-name='./recommendPages']").hide();
                }

}
function generatePathBasedId(fieldId){

    	var formEl = $("input[name='"+fieldId+"']").closest('form');
		if (formEl && !$("input[name='"+fieldId+"']").val()) {
        	var replaceSlash=$(formEl).attr('action');            
			if(typeof replaceSlash !== "undefined"){
                replaceSlash=replaceSlash.replace('/_','_');
        		$("input[name='"+fieldId+"']").val(replaceSlash.replace(/[/:.]/g, "_")).trigger('change');
            }
    	}
    	$("input[name='"+fieldId+"']").attr('readonly', true);
	}

//Quick poll generate poll id
function pouplatePollId(fieldId) {
    $(document).ready(function(e) {
        if ($('#' + fieldId).val() == "") {
        	var obj=getBrandAndLocale($('#' + fieldId).closest('form'));
			obj["entity"]="quickpoll";
            $.ajax({
                type: 'GET',
                url: '/bin/ts/private/quickpoll/pollId',
                data: obj,
                success: function(data) {
                    $('#' + fieldId).val(data.responseData.results[0].pollId);
                    $('#' + fieldId).trigger("change");

                }
            });
        }
        $('#' + fieldId).attr('readonly', true);
    });
}

//Quick Poll post question
function postQuestionsToTS() {
    $(document).on("click", ".cq-dialog-submit", function(e) {
        var mandatoryFlag = false;
        mandatoryFlag = checkQuickPollMandatory(e);
		
        var f = $(this).closest('form');
        var obj1 = getBrandAndLocale($(this).closest('form'));
        var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');
        if (formName && formName == 'questions') {
            $(document).ready(function() {
                var obj = getMultifieldData();
                obj["entity"] = "quickpoll";
                obj["brand"] = obj1["brand"];
                obj["locale"] = obj1["locale"];

                $.ajax({
                    url: '/bin/ts/private/quickpoll/question',
                    type: "POST",
                    data: JSON.stringify(obj),
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    complete: function(data1, xhr, errors) {
                        if (data1.status === 200 && mandatoryFlag) {
                            $(window).adaptTo("foundation-ui").alert("Message", "Question posted successfully");
                        } else if (!mandatoryFlag) {

                        } else {
                            $(window).adaptTo("foundation-ui").alert("Message", "Error while posting Question");
                        }

                    }

                })

            });

        }
    });

}

function checkQuickPollMandatory(e) {
    var flag = false;
    if ($('#opinionType :selected').val() == "text") {
        $('input[type=text][name="./answerText"]').each(function() {
            if ($(this).val().length == 0 && !($(this).closest(".coral-Form-fieldset").find('input[name="./answerText"]').val())) {
                $(window).adaptTo("foundation-ui").alert("Required", "Please configure Answer Text field");
                e.preventDefault();
                return false;
            } else {
                flag = true;
            }
        });
    } else if ($('#opinionType :selected').val() == "textAndImage") {
        $('input[type=text][name="./opinionText"]').each(function() {
            if ($(this).val().length == 0 && !($(this).closest(".coral-Form-fieldset").find('input[name="./opinionImage"]').val()) || !($(this).closest(".coral-Form-fieldset").find('input[name="./opinionImageAltText"]').val())) {
                $(window).adaptTo("foundation-ui").alert("Required", "Please configure the mandatory fields");
                e.preventDefault();
                return false;
            }else {
                flag = true;
            }
        });
    }
    return flag;
}

function getBrandAndLocale(form) {
    var formActionUrl = $(form).attr('action');
    var formActionElements = formActionUrl.split("/");
    var obj = {};
    obj["brand"] = titleCase(formActionElements[2])
    var localeArr = formActionElements[3].split("_");
	
    var locale = localeArr[0];
	if(localeArr[1]!=null){
		locale=locale+"-"+localeArr[1].toUpperCase();
	}
    obj["locale"] = locale;
    return obj;
}

function titleCase(str) {
    return str.charAt(0).toUpperCase() + str.substring(1).toLowerCase();
}

function getMultifieldData() {
    var obj = {};
    var obj1 = {};
    var answers = [];
    var pollId = $("input[type='text'][name='./id']").val();
    var startDate = $("input[type='hidden'][name='./startDate']").val();

	var startdt = new Date(startDate).toISOString();

    var endDate = $("input[type='hidden'][name='./endDate']").val();

    var enddt = new Date(endDate).toISOString();
    var headline = $("input[type='text'][name='./questionHeadline']").val();
    var subHeadline = $("input[type='text'][name='./questionSubHeadline']").val();
    var DATA_EAEM_NESTED = "data-eaem-nested";
    var CFFW = ".coral-Form-fieldwrapper";
    var DATA_NUMBER_OF_ITEMS = "data-custom-multi";
    var NESTED_NUMBER_OF_ITEMS = "data-nested-multi";
    obj1["headline"] = headline;
    obj1["subHeadline"] = subHeadline;
    obj["pollId"] = pollId;
    obj["startDate"] = startdt;
    obj["endDate"] = enddt;


    var $form = $(this).closest("form.foundation-form");
    var mNameArr = [];
    var noLinksArr = [];
    var mName;
    var eleArr = $("[" + DATA_EAEM_NESTED + "]");
    $.each(eleArr, function(i, ele) {
        mName = $(ele).attr("data-name");
        if ($.inArray(mName, mNameArr) == -1) {
            mNameArr.push(mName);
        }
    });

    $.each(mNameArr, function(i, mn) {

        var $fieldSets = $("[" + DATA_EAEM_NESTED + "][data-name='" + mn + "']");

        //mn = mn.substring(2);
        var record, $fields, $field, name;
        var numberOfLinks = 0;
        $fieldSets.each(function(i, fieldSet) {
            $fields = $(fieldSet).children().children(CFFW);
            record = {};

            $fields.each(function(j, field) {
                $field = $(field);
                var name = $field.find("[name]").attr("name");
                if (!name) {
                    return;
                }
                //strip ./
                if (name.indexOf("./") == 0) {
                    name = name.substring(2);
                }

                var obj2 = {};
                if ($('#opinionType :selected').val() == "text" && name == "answerText") {
                    obj2["answerText"] = $field.find("[name]").val();
                    obj1["freeText"] = false;
                    answers.push(obj2);
                } else if ($('#opinionType :selected').val() == "textAndImage" && name == "opinionText") {
                    obj1["freeText"] = false;
                    obj2["answerText"] = $field.find("[name]").val();
                    answers.push(obj2);

                } else if ($('#opinionType :selected').val() == "freeText") {
                    obj1["freeText"] = true;
                    answers = null;
                }
                obj1["answers"] = answers;
            });
        });
    });
    obj["question"] = obj1;

    return obj;
}

/**
 * Featured Tweet TAB component
 * Rotation Period field looking for input as numeric value.
 *
 * The validator keys off of the ".field-rotationPeriod" selector. To use in Touch
 * UI dialogs, add the class "field-rotationPeriod" to a textfield.
 *
 * <phonenumber
 *     jcr:primaryType="nt:unstructured"
 *     sling:resourceType="granite/ui/components/foundation/form/textfield"
 *     fieldLabel="Rotation Period"
 *     class="field-rotationPeriod"
 *     name="./rotationPeriod"/>
 *
 */
$.validator.register({
  selector: '.field-rotationPeriod',
  validate: function(el) {
    var field,
        value;

    field = el.closest(".coral-Form-field");
    value = el.val();

    if (!/^\d+(\.\d+)?$/.test(value)) {
      return Granite.I18n.get('Only numeric values can be entered.');
    }
  },
  show: function (el, message) {
    var fieldErrorEl,
        field,
        error,
        arrow;

    fieldErrorEl = $("<span class='coral-Form-fielderror coral-Icon coral-Icon--alert coral-Icon--sizeS' data-init='quicktip' data-quicktip-type='error' />");
    field = el.closest(".coral-Form-field");

    field.attr("aria-invalid", "true")
      .toggleClass("is-invalid", true);

    field.nextAll(".coral-Form-fieldinfo")
      .addClass("u-coral-screenReaderOnly");

    error = field.nextAll(".coral-Form-fielderror");

    if (error.length === 0) {
      arrow = field.closest("form").hasClass("coral-Form--vertical") ? "right" : "top";

      fieldErrorEl
        .attr("data-quicktip-arrow", arrow)
        .attr("data-quicktip-content", message)
        .insertAfter(field);
    } else {
      error.data("quicktipContent", message);
    }
  },
  clear: function (el) {
    var field = el.closest(".coral-Form-field");

    field.removeAttr("aria-invalid").removeClass("is-invalid");

    field.nextAll(".coral-Form-fielderror").tooltip("hide").remove();
    field.nextAll(".coral-Form-fieldinfo").removeClass("u-coral-screenReaderOnly");
  }
});

function visualElementControlOnLoad(){
if (document.getElementById('overrideGlobalConfig').checked) {
    $("#visualElementControlId").show();                                                       
    }else{
    $("#visualElementControlId").hide();
     }
}
function onHeroV2Change(){
	var heroType=$("[name='./heroType']").val();
	console.log(heroType);
	if(heroType == "image"){
		$("[aria-controls='image']").show();
		$("[aria-controls='video']").hide();
        $("input[type='text'][name='./videoId']").attr('aria-required','false');
	}
	else if(heroType == "video"){
		$("[aria-controls='video']").show();
		$("[aria-controls='image']").hide();
		$("input[type='text'][name='./videoId']").attr('aria-required','true');
	}
	else{
		$("[aria-controls='video']").hide();
		$("[aria-controls='image']").hide();
		$("input[type='text'][name='./videoId']").attr('aria-required','false');
	}
    
}


//spotlight listener
function featuredContentOnload() {
    var spotlightSelectedValue = $('#spotlightType :selected').val();
    if (spotlightSelectedValue == "Provide Manual Content" || !spotlightSelectedValue) {
        $("[aria-controls='manualContentTabId']").show();
        $("[aria-controls='selectPageTabId']").hide();
        $("[aria-controls='tagsPageTabId']").hide();
		$("[aria-controls='visualElementControl']").hide();
        $("input[type='text'][name='./pageCtaUrl']").attr('aria-required', 'false');
    } else if (spotlightSelectedValue == "Identify Page By Tags") {
        $("[aria-controls='tagsPageTabId']").show();
        $("[aria-controls='manualContentTabId']").hide();
        $("[aria-controls='selectPageTabId']").hide();
		$("[aria-controls='visualElementControl']").show();
		$("input[type='text'][name='./pageCtaUrl']").attr('aria-required', 'false');
    } else if (spotlightSelectedValue == "Manually Select Page") {
        $("[aria-controls='selectPageTabId']").show();
        $("[aria-controls='manualContentTabId']").hide();
        $("[aria-controls='tagsPageTabId']").hide();
		$("[aria-controls='visualElementControl']").show();
        $("input[type='text'][name='./pageCtaUrl']").attr('aria-required', 'true');
    }

}
function featuredContentOnSubmit() {
    $(document).on("click", ".cq-dialog-submit", function (event) {
    	var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');
    if(formName && formName=='featuredContent')
    {
            var startDate = $("input[type='hidden'][name='./startDate']").val();
        	var endDate = $("input[type='hidden'][name='./endDate']").val();
        	var startd = new Date(startDate);
        	var endd = new Date(endDate);
        	if(endd < startd)
            {
            $(window).adaptTo("foundation-ui").alert("EndDate", "Please enter end date greater than start date");
            event.preventDefault();
            return false;
            }
    }

    });
}

//ordered list listener before submit
function orderedListOnSubmit(){
    var imageneeded = false;
	 $(document).on("click", ".cq-dialog-submit", function (event) {

	 	var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');
          if (document.getElementById('isImage').checked) {
				imageneeded = true;
              
              if(formName && formName=='orderedList')
              {

                  $(".coral-Multifield-list li [name='./image']").attr('aria-required', 'true');
                  $(".coral-Multifield-list li [name='./altImage']").attr('aria-required', 'true');
				
              }
			}
         else
         {
              if(formName && formName=='orderedList')
              {

				  $(".coral-Multifield-list li [name='./image']").attr('aria-required', 'false');
                  $(".coral-Multifield-list li [name='./altImage']").attr('aria-required', 'false');
              }
         }

			if(formName && formName=='orderedList') {
		    	if($(".coral-Multifield-list li [name='./step']").length==0){
           			 $(window).adaptTo("foundation-ui").alert("Required","Please configure at least one ordered list");
            			event.preventDefault();
            return false;
      	}
	 }

	 });
}

function orderedListOnLoad(){
     if (document.getElementById('isImage').checked) {
     $(".coral-Multifield-list li [id='tipsImage']").parent().show();

		 $(".coral-Multifield-list li [id='altImage']").parent().show();
	}
    else
    {
       $(".coral-Multifield-list li [id='tipsImage']").parent().hide();

		 $(".coral-Multifield-list li [id='altImage']").parent().hide();

    }

}
//featured product V2 listener
function featuredProductV2OnSelect() {
    var featuredProductSelectedValue = $('#featuredProductType :selected').val();
    if (featuredProductSelectedValue == "Automatically" || !featuredProductSelectedValue) {
        $("[aria-controls='configureTagsTab']").show();
        $("[aria-controls='selectProductTab']").hide();
		$("input[type='text'][name='./productPage']").attr('aria-required', 'false');
    } else if ($('#featuredProductType :selected').val() == "Manually") {
        $("[aria-controls='configureTagsTab']").hide();
        $("[aria-controls='selectProductTab']").show();
		$("input[type='text'][name='./productPage']").attr('aria-required', 'true');
    }

}
//featured Product V2, Show or Hide Include In Display Section
function showHideIncludeInDisplaySection() {
    if (document.getElementById('isOverrideGlobalConfig').checked) {
    	if($("#includeInDisplaySection")){
    		$("#includeInDisplaySection").show(); //displaying the include In Display Section
    	}
		if($("#multiBuyConfiguration")){
        $("#multiBuyConfiguration").show();
		}
		if($("#multibuyConfiguration")){
	        $("#multibuyConfiguration").show();
			}
		if($("#multibuyConfigurationSection")){
		 $("#multibuyConfigurationSection").show();
		 }
    } else {
    	if($("#includeInDisplaySection")){
        $("#includeInDisplaySection").hide();
        }
        if($("#multiBuyConfiguration")){
        $("#multiBuyConfiguration").hide();
		}
        if($("#multibuyConfiguration")){
	        $("#multibuyConfiguration").hide();
			}
        if($("#multibuyConfigurationSection")){
        $("#multibuyConfigurationSection").hide();
        }
    }
}


function analyticsContextOnSelect(event) {
    analyticsContextOnLoad(event.selected);
}

function analyticsContextOnLoad(selectedValue) {
	if(selectedValue==null || selectedValue=="undefined"){
		window.setTimeout(function(){
			var analyticsContextType = $('#analyticsContext :selected').val();
			showHideCustomAnalyticField(analyticsContextType);
		},1000);
	}
	else{
		showHideCustomAnalyticField(selectedValue);
	}
}
function showHideCustomAnalyticField(val){
	
	if (  typeof val=="string"){
    var showCustomField = 'false';
     if (!val) {
        val = 'Article';
    }
    if (val == "Custom") {
        showCustomField = 'true';
                                   }
    if(showCustomField == 'true'){
$("#customAnalyticsContext").parent().show();
    }else{
$("#customAnalyticsContext").parent().hide();
    }
	}else{
$("#customAnalyticsContext").parent().hide();
    }
}

/**
 * CSS Styles class
 * CSS Style class field looking for input as 'Lower case letters [ a - z ] & spaces, without any special characters or numbers'.
 *
 * The validator keys off the ".field-class" selector. To use in Touch
 * UI dialogs, add the class "field-class" to a textfield.
 *
 *
 */
$.validator.register({
  selector: '.field-class',
  validate: function(el) {
    var field,
        value;

    field = el.closest(".coral-Form-field");
    value = el.val();

    if (!/^[-_a-z ]+$/.test(value) && value.length !==0) {
	return Granite.I18n.get('Please specify CSS Style class which allows only lower case letters [ a - z ], spaces and hiphen[-], without any special characters or numbers e.g. "sample class"');
    }
  }
});

function relatedArticlesMediaTypeOnSelect(event){
    var buildNavigationUsingVal = $('#buildNavigationUsingId :selected').val();
    if(buildNavigationUsingVal == 'fixedList' || !buildNavigationUsingVal){
    $("[aria-controls='fixedListTabId']").show();
    $("[aria-controls='tagRelatedFieldId']").hide();
	$("[aria-controls='allThingsHairId']").hide();
    }else if(buildNavigationUsingVal == 'tags'){
    $("[aria-controls='tagRelatedFieldId']").show();
    $("[aria-controls='fixedListTabId']").hide();
	$("[aria-controls='allThingsHairId']").hide();
    }else{
	$("[aria-controls='allThingsHairId']").show();
	$("[aria-controls='fixedListTabId']").hide();
	$("[aria-controls='tagRelatedFieldId']").hide();
	}
	
	
	var buildListUsingVal = $('#buildListUsingId :selected').val();
	if(buildNavigationUsingVal == 'allThingsHair' && buildListUsingVal == 'articleSearch' || !buildListUsingVal){
	$("#articleListMultifield").parent().hide();
        $("#searchKeywordId").parent().show();
        $("[name='./searchKeyword']").attr("aria-required",true);
        $("#limitResultCountToSection").show();
        $("#limitResultToCategoriesMultifield").parent().show();
        $("#excludeArticleIdsMultifield").parent().show();
        $("[name='./articleId']").attr("aria-required",false);
    }else{
        $("#articleListMultifield").parent().show();
		$("#searchKeywordId").parent().hide();
		$("[name='./searchKeyword']").attr("aria-required",false);
		$("#limitResultCountToSection").hide();
		$("#limitResultToCategoriesMultifield").parent().hide();
		$("#excludeArticleIdsMultifield").parent().hide();

	}


	
	
if (document.getElementById('overrideGlobalConfigLimit').checked) {
    $("#visualElementControlIdLimit").show();                                                       
    }else{
    $("#visualElementControlIdLimit").hide();
     }
	
		$('.externalUrl').each(function() {
			if (this.checked) {
				$(this).closest("li").find("#teaserImageId").parent().show();
				$(this).closest("li").find("#teaserImageAltId").parent().show();
				$(this).closest("li").find("#teaserTitleId").parent().show();
				$(this).closest("li").find("#teaserCopyId").parent().parent().show();
				$(this).closest("li").find("#teaserLongCopyId").parent().parent().show();
			} else {
				$(this).closest("li").find("#teaserImageId").parent().hide();
				$(this).closest("li").find("#teaserImageAltId").parent().hide();
				$(this).closest("li").find("#teaserTitleId").parent().hide();
				$(this).closest("li").find("#teaserCopyId").parent().parent().hide();
				$(this).closest("li").find("#teaserLongCopyId").parent().parent().hide();
			}
		});
}

function relatedArticlesOnSubmit() {
	$(document).on("click", ".cq-dialog-submit", function(e) {
        var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');
        var buildListUsingVal = $('#buildListUsingId :selected').val();
        if (formName && formName == 'relatedArticles') {
			var sourceType= $('[name="./buildNavigationUsing"]').val();
			if(sourceType=='fixedList'){
				$('input[type="text"][name="./url"]').each(function() {
					var urlVal = $(this).val();
					if (urlVal.length == 0) {
						$(window).adaptTo("foundation-ui").alert("Required", "Please Configure the Article URL");
						e.preventDefault();
						return false;
					} else {
						return true;
					}
				});
			}
			}

         if (formName && formName == 'relatedArticles') {
			var sourceType= $('[name="./buildNavigationUsing"]').val();
            var buildListType= $('[name="./buildListUsing"]').val();
			if(sourceType=='allThingsHair' && buildListType =='articleIds'){
				$('input[type="text"][name="./articleId"]').each(function() {
					var urlVal = $(this).val();
					if (urlVal.length == 0) {
						$(window).adaptTo("foundation-ui").alert("Required", "Please Configure the Article Id");
						e.preventDefault();
						return false;
					} else {
						return true;
					}
				});
			}
			}
        });
}

function sectionNavigationV2OnLoad(){
var buildNavigationUsingVal = $('#sectionusingBuildNavigationId :selected').val();
    if(buildNavigationUsingVal=='fixedList' || !buildNavigationUsingVal){
    $("[aria-controls='sectionNavigationsId']").show();
    $("[aria-controls='sectionChildPagesId']").hide();
    }else if(buildNavigationUsingVal=='childPages'){
    $("[aria-controls='sectionChildPagesId']").show();
    $("[aria-controls='sectionNavigationsId']").hide();
    }
}

function sectionNavigationV2OnSubmit() {
    $(document).on("click", ".cq-dialog-submit", function(e) {
        var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');
        if (formName && formName == 'sectionNavigationV2') {
			var sourceType= $('[name="./buildNavigationUsing"]').val();
			if(sourceType=='fixedList'){
				$('input[type="text"][name="./url"]').each(function() {
					var urlVal = $(this).val();
					if (urlVal.length == 0) {
						$(window).adaptTo("foundation-ui").alert("Required", "Please Configure the Navigation URL");
						e.preventDefault();
						return false;
					} else {
						return true;
					}
				});
			} else if(sourceType=='childPages'){
				$('input:hidden[name="./parentPageUrl"]').each(function() {
					var urlVal = $(this).val();
					if (urlVal.length == 0) {
						$(window).adaptTo("foundation-ui").alert("Required", "Please Configure the Parent URL");
						e.preventDefault();
						return false;
					} else {
						return true;
					}
				});
			}
        }
    });
}

function searchInputV2OnLoad(event){
       searchInputOverrideConfigurationsOnSelect(event);
}


function searchInputOverrideConfigurationsOnSelect(event){
     if (document.getElementById('overrideGlobalConfigSearchInput').checked) {
	$("#disableContentTypeFilter").parent().show();
    $("#disableAutoSuggest").parent().show(); 
    $("#disableAutoComplete").parent().show(); 
	$("#autoSuggestResultCount").parent().show();   
    }
    else{
    $("#disableAutoSuggest").parent().hide(); 
    $("#disableAutoComplete").parent().hide(); 
	$("#autoSuggestResultCount").parent().hide();  
    $("#disableContentTypeFilter").parent().hide();
	}	
}
     
//tags on load function	 
function tagsOnLoad() {
    var tagsSelectedValue = $('#buildTagsUsing :selected').val();
    if (tagsSelectedValue == "Parent page tags" || !tagsSelectedValue) {
        $("[aria-controls='tagHierarchiesTabId']").show();
        $("[aria-controls='tagsTabId']").hide();
    } else if (tagsSelectedValue == "Manual tags") {
        $("[aria-controls='tagsTabId']").show();
        $("[aria-controls='tagHierarchiesTabId']").hide();
    }
}

//tags on submit function
function tagsOnSubmit() {
    $(document).on("click", ".cq-dialog-submit", function(event) {
        var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');
        if (formName && formName == 'tags' && $('#buildTagsUsing :selected').val() == 'Manual tags') {

            if ($(".js-coral-Autocomplete-tagList [name='./tagsToDisplay']").length == 0) {
                $(window).adaptTo("foundation-ui").alert("Required", "Please specify at least one Tag in Tags tab");
                event.preventDefault();
                return false;
            }
        }
    });
}

function pageListingV2MediaTypeOnSelect(event){
    var buildNavigationUsingVal = $('#buildNavigationUsingId :selected').val();
    if(buildNavigationUsingVal == 'fixedList' || !buildNavigationUsingVal){
    $("[aria-controls='fixedListTabId']").show();
    $("[aria-controls='tagRelatedFieldId']").hide();
    $("[aria-controls='childPagesTab']").hide();
    $('.externalUrl').each(function() {
			if (this.checked) {
				$(this).closest("li").find("#teaserImageId").parent().show();
				$(this).closest("li").find("#teaserImageAltId").parent().show();
				$(this).closest("li").find("#teaserTitleId").parent().show();
				$(this).closest("li").find("#teaserCopyId").parent().parent().show();
				$(this).closest("li").find("#teaserLongCopyId").parent().parent().show();
			} else {
				$(this).closest("li").find("#teaserImageId").parent().hide();
				$(this).closest("li").find("#teaserImageAltId").parent().hide();
				$(this).closest("li").find("#teaserTitleId").parent().hide();
				$(this).closest("li").find("#teaserCopyId").parent().parent().hide();
				$(this).closest("li").find("#teaserLongCopyId").parent().parent().hide();
			}
		});
    }else if(buildNavigationUsingVal == 'tags'){
    $("[aria-controls='tagRelatedFieldId']").show();
    $("[aria-controls='fixedListTabId']").hide();
    $("[aria-controls='childPagesTab']").hide();
    }else if(buildNavigationUsingVal == 'childPages'){
    $("[aria-controls='childPagesTab']").show();
    $("[aria-controls='tagRelatedFieldId']").hide();
    $("[aria-controls='fixedListTabId']").hide();
       
    }

if (document.getElementById('overrideGlobalConfigLimit').checked) {
    $("#visualElementControlIdLimit").show();                                                       
    }else{
    $("#visualElementControlIdLimit").hide();
     }

}

function pageListingV2OnSubmit() {
    $(document).on("click", ".cq-dialog-submit", function(e) {
        var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');
        if (formName && formName == 'pageListingV2') {
			var sourceType= $('[name="./buildNavigationUsing"]').val();
            $('input[type="text"][name="./filterParentTagOne"]').each(function() {
                var tagValue = $(this).val();
                if (tagValue.length == 0) {
                    $(window).adaptTo("foundation-ui").alert("Required", "Please Configure Filter Parent Tag");
                    e.preventDefault();
                    return false;
                } else {
                    return true;
                }
            });
			if(sourceType=='fixedList'){
				$('input[type="text"][name="./url"]').each(function() {
					var urlVal = $(this).val();
					if (urlVal.length == 0) {
						$(window).adaptTo("foundation-ui").alert("Required", "Please Configure the Navigation URL");
						e.preventDefault();
						return false;
					} else {
						return true;
					}
				});
			} else if(sourceType=='childPages'){
				$('input:hidden[name="./parentPageUrl"]').each(function() {
					var urlVal = $(this).val();
					if (urlVal.length == 0) {
						$(window).adaptTo("foundation-ui").alert("Required", "Please Configure the Parent URL");
						e.preventDefault();
						return false;
					} else {
						return true;
					}
				});
			}
        }
    });
}

function tabbedContentOnSubmit(){
    $(document).on("click", ".cq-dialog-submit", function (e) {
        var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');
    var defaultValue = $('input[type="text"][name="./defaultIndex"]').val();
 if(formName && formName=='tabbedContent' && (defaultValue==0 && defaultValue<0)) {
    $(window).adaptTo("foundation-ui").alert("Required","Please Enter default index starting from 1");
            e.preventDefault();
            return false;
 }
    });
}

//store search listener before submit
function StoreSearchOnSubmit(){
	 $(document).on("click", ".cq-dialog-submit", function (event) {
         var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');
		 if(formName && formName=='storeSearch') {
		  
		    if($(".coral-Multifield-list li [name='./categoryPageUrl']").length==0){
            $(window).adaptTo("foundation-ui").alert("Required","Please configure at least one category page path");
            event.preventDefault();
            return false;
      	}

	 

         }
	 });

}

//product listing listener
function productListingV2OnSelect() {

  var productListingbuildListValue = $('#buildListUsingId :selected').val();
  if (productListingbuildListValue == "fixedList" || !productListingbuildListValue) {
  	
  	 $("[aria-controls='tagRelatedFieldId']").hide();
       $("[aria-controls='fixedListTab']").show();


	} else {
		 $("[aria-controls='tagRelatedFieldId']").show();
       $("[aria-controls='fixedListTab']").hide();



  }
  addToolTipToMultiBuyConfig('multibuyConfigurationSection');
}

//product listing v2 listener before submit
function productListingV2OnSubmit(){
	 $(document).on("click", ".cq-dialog-submit", function (event) {
       var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');
		 if(formName && formName=='productListingV2') {

		 var productListingbuildListValue = $('#buildListUsingId :selected').val();
		    if (productListingbuildListValue == "fixedList" || !productListingbuildListValue) {
		    if($(".coral-Multifield-list li [name='./productPath']").length==0){
          $(window).adaptTo("foundation-ui").alert("Required","Please configure at least one product path");
          event.preventDefault();
          return false;
    	}

		    $('input[type="text"][name="./productPath"]').each(function() {
				var urlVal = $(this).val();
				if (urlVal.length == 0) {
					$(window).adaptTo("foundation-ui").alert("Required", "Please Configure Product");
					event.preventDefault();
					return false;
				} else {
					return true;
				}
			});
	 }
       }
	 });
}

function pagePropertyListOnLoad(_self, selectedValue) {
	pagePropertyListType($(_self).closest("li"), selectedValue);
}


function pagePropertyListOnSelect(event) {
	pagePropertyListType($(event.target).closest("li"), event.selected);
}

function pagePropertyListType(parentLI, selectedValue) {
    if(!selectedValue){
       selectedValue =  parentLI.find("#pagePropertyList :selected").val();
    }
	if (selectedValue == 'custom') {
        parentLI.find("#crxProperty").parent().show();
        parentLI.find("#crxProperty").attr("aria-required",true);
    } 
    else{
        parentLI.find("#crxProperty").parent().hide();
        parentLI.find("#crxProperty").attr("aria-required",false);
		}
}

function formV2OnLoad(selectedValue) {
	if(!selectedValue) {
		selectedValue = 'display message';
		$("#successMessageId").parent().parent().show();
		$("#redirectPageId").parent().hide();
		$("[name='./successMessage']").attr("aria-required",true);
		$("[name='./redirectPage']").attr("aria-required",false);
        $("#customTypeNameId").parent().hide();
        $("#customFormURLId").parent().hide();
	}
	if (selectedValue == 'display message') {
		$("#successMessageId").parent().parent().show();
		$("#redirectPageId").parent().hide();
		$("[name='./successMessage']").attr("aria-required",true);
		$("[name='./redirectPage']").attr("aria-required",false);
	} else if (selectedValue == 'redirect to') {
		$("#successMessageId").parent().parent().hide();
		$("#redirectPageId").parent().show();
		$("[name='./successMessage']").attr("aria-required",false);
		$("[name='./redirectPage']").attr("aria-required",true);
	}

    formV2CreateFormIdentifier();	
}

function formOnSuccessfulFormSubmissionOnSelect(event) {
	formV2OnLoad(event.selected);
}

function formV2CreateFormIdentifier() {
	var formEl = $("input[name='./formIdentifier']").closest('form');
    if (formEl && !$("input[name='./formIdentifier']").val()) {
        var replaceSlash=$(formEl).attr('action');
        replaceSlash=replaceSlash.replace('/_','_');
        $("input[name='./formIdentifier']").val(replaceSlash.replace(/[/:.]/g, "_")).trigger('change');
    }
    $("input[name='./formIdentifier']").attr('readonly', true);
}

// Social Gallery Tab V2 functions start
function socialGalleryTabV2LinkTypeOnLoad(_self, selectedValue) {
    var selectedValue = $('#aggregationModeId :selected').val();
    if (selectedValue == "automatic" || !selectedValue) {
		socialGalleryTabV2LinkType($(_self).closest("li"), selectedValue);
    }else if (selectedValue == "manual" || !selectedValue){
    	socialGalleryTabV2FixedLinkType($(_self).closest("li"), selectedValue);
    }
}


function socialGalleryTabV2LinkTypeOnSelect(event) {
	socialGalleryTabV2LinkType($(event.target).closest("li"), event.selected);
}

function socialGalleryTabV2LinkType(parentLI, selectedValue) {

    $('.linkTypeClass').each(function() {
        var type = $(this).closest("li").find('#linkTypeId :selected').val();
        if (type == 'custom' || !type) {
            $(this).closest("li").find("#headingTextId").parent().show();
            $(this).closest("li").find("#linkPostToCtaId").parent().show();
        } else {
            $(this).closest("li").find("#headingTextId").parent().hide();
            $(this).closest("li").find("#linkPostToCtaId").parent().hide();
        }
    });

}

// social gallery Tab v2 Aggregation Mode on select function
function aggregationModeOnSelect() {

    var selectedValue = $('#aggregationModeId :selected').val();
    if (selectedValue == "automatic" || !selectedValue) {    	
		$("[aria-controls='fixedTabId']").hide();
		$("[aria-controls='serachConfigTabId']").show();
		$('#socialChannelsToIncludeId').show();
        $("[name='./imagePath']").attr("aria-required", true);
        $("[name='./pagePath']").attr("aria-required", true);
        $("[name='./hashTag']").attr("aria-required", true);
        $("[name='./fixedTabImagePath']").attr("aria-required", false);
        $("[name='./fixedTabPagePath']").attr("aria-required", false);
	} else if(selectedValue == "manual") {
		$("[aria-controls='serachConfigTabId']").hide();
		$("[aria-controls='fixedTabId']").show();
		$('#socialChannelsToIncludeId').hide();
        $("[name='./fixedTabImagePath']").attr("aria-required", true);
        $("[name='./fixedTabPagePath']").attr("aria-required", true);
		$("[name='./imagePath']").attr("aria-required", false);
        $("[name='./pagePath']").attr("aria-required", false);
        $("[name='./hashTag']").attr("aria-required", false);
    }
}

// fixed tab
function socialGalleryTabV2FixedLinkTypeOnSelect(event) {
	socialGalleryTabV2FixedLinkType($(event.target).closest("li"), event.selected);
}

function socialGalleryTabV2FixedLinkType(parentLI, selectedValue) {

        $('.fixedLinkTypeClass').each(function() {
        var type = $(this).closest("li").find('#fixedTabLinkTypeId :selected').val();
        if (type == 'custom' || !type) {
            $(this).closest("li").find("#fixedTabHeadingTextId").parent().show();
            $(this).closest("li").find("#fixedTabLinkPostToCtaId").parent().show();
        } else {
            $(this).closest("li").find("#fixedTabHeadingTextId").parent().hide();
            $(this).closest("li").find("#fixedTabLinkPostToCtaId").parent().hide();
        }
    });
}

function socialGalleryTabV2OnSubmit(event) {
    $(document).on("click", ".cq-dialog-submit", function(event) {
        var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');
        if (formName && formName == 'socialGalleryTabV2') {
            var aggregationMode = $('#aggregationModeId :selected').val();
            if (aggregationMode == "automatic" || !aggregationMode) {
                if ($(".coral-Multifield-list li [name='./hashTag']").length == 0) {
                    $(window).adaptTo("foundation-ui").alert("Required", "Please configure at least one hashTag");
                    event.preventDefault();
                    return false;
                }
            }
        }
    });
}

// Social Gallery Tab V2 functions end


function mediaGalleryV2OnLoad(_self, selectedValue) {
    galleryMediaTypeOnSelect($(_self).closest("li"), selectedValue);
    videoGalleryMediaTypeOnSelect($(_self).closest("li"), selectedValue);
    imageAndVideoGalleryMediaTypeOnSelect($(_self).closest("li"), selectedValue);
}


function galleryMediaTypeOnSelect(parentLI, selectedValue) {
    var galleryType = $('#galleryTypeId :selected').val();
    if (galleryType == 'images' || !galleryType) {
        $("[aria-controls='ImageGalleryTabId']").show();
        $("[aria-controls='videoGalleryTabId']").hide();
        $("[aria-controls='imageAndVideoGalleryTabId']").hide();
        $("[aria-controls='videoControlsTabId']").hide();
        $("[name='./imageUrl']").attr("aria-required", false);
        $("[name='./videoId']").attr("aria-required", false);
        $("[name='./playlistId']").attr("aria-required", false);
        $("[name='./channelId']").attr("aria-required", false);
        $("[name='./selectImageMixTab']").attr("aria-required", false);
        $("[name='./videoIdMixTab']").attr("aria-required", false);
    } else if (galleryType == 'videos') {
        $("[aria-controls='ImageGalleryTabId']").hide();
        $("[aria-controls='videoGalleryTabId']").show();
        $("[aria-controls='imageAndVideoGalleryTabId']").hide();
        $("[aria-controls='videoControlsTabId']").show();
        $("[name='./imageUrl']").attr("aria-required", false);
        $("[name='./videoId']").attr("aria-required", true);
        $("[name='./playlistId']").attr("aria-required", false);
        $("[name='./channelId']").attr("aria-required", false);
        $("[name='./selectImageMixTab']").attr("aria-required", false);
        $("[name='./videoIdMixTab']").attr("aria-required", false);
    } else {
        $("[aria-controls='ImageGalleryTabId']").hide();
        $("[aria-controls='videoGalleryTabId']").hide();
        $("[aria-controls='imageAndVideoGalleryTabId']").show();
        $("[aria-controls='videoControlsTabId']").show();
        $("[name='./imageUrl']").attr("aria-required", false);
        $("[name='./videoId']").attr("aria-required", false);
        $("[name='./playlistId']").attr("aria-required", false);
        $("[name='./channelId']").attr("aria-required", false);
        $("[name='./selectImageMixTab']").attr("aria-required", false);
        $("[name='./videoIdMixTab']").attr("aria-required", false);
    }
}

function videoGalleryMediaTypeOnSelect(parentLI, selectedValue) {
    var configureUsing = $('#configureUsingId :selected').val();
    if (configureUsing == 'fixedList' || !configureUsing) {
        $("#selectVideosId").parent().show();
        $("#playlistId").parent().hide();
        $("#channelId").parent().hide();
        $("#limitResultsToId").parent().hide();
        $("[name='./imageUrl']").attr("aria-required", true);
        $("[name='./playlistId']").attr("aria-required", false);
        $("[name='./channelId']").attr("aria-required", false);
    } else if (configureUsing == 'playlist') {
        $("#selectVideosId").parent().hide();
        $("#playlistId").parent().show();
        $("#channelId").parent().hide();
        $("#limitResultsToId").parent().show();
        $("[name='./imageUrl']").attr("aria-required", false);
        $("[name='./playlistId']").attr("aria-required", true);
        $("[name='./channelId']").attr("aria-required", false);
    } else {
        $("#selectVideosId").parent().hide();
        $("#playlistId").parent().hide();
        $("#channelId").parent().show();
        $("#limitResultsToId").parent().show();
        $("[name='./imageUrl']").attr("aria-required", false);
        $("[name='./playlistId']").attr("aria-required", false);
        $("[name='./channelId']").attr("aria-required", true);
    }
}

function imageAndVideoGalleryMediaTypeOnSelect(parentLI, selectedValue) {


    $('.typeMixTab').each(function() {
        var type = $(this).closest("li").find('#typeId :selected').val();
        if (type == 'image' || !type) {
            $(this).closest("li").find("#selectImageMixGalleryId").parent().show();
            $(this).closest("li").find("#imageCaptionMixGalleryId").parent().parent().show();
            $(this).closest("li").find('#imageAltId').parent().show();
            $(this).closest("li").find("#videoSourceMixGalleryId").parent().hide();
            $(this).closest("li").find("#videoMixGalleryId").parent().hide();
            $(this).closest("li").find("#previewImageMixGalleryId").parent().hide();
            $(this).closest("li").find('#previewImageAltId').parent().hide();
            $(this).closest("li").find("#videoCaptionMixGalleryId").parent().parent().hide();
            $(this).closest("li").find("[name='./selectImageMixTab']").attr("aria-required", true);
            $(this).closest("li").find("[name='./videoIdMixTab']").attr("aria-required", false);
        } else {
            $(this).closest("li").find("#selectImageMixGalleryId").parent().hide();
            $(this).closest("li").find("#imageCaptionMixGalleryId").parent().parent().hide();
            $(this).closest("li").find('#imageAltId').parent().hide();
            $(this).closest("li").find("#videoSourceMixGalleryId").parent().show();
            $(this).closest("li").find("#videoMixGalleryId").parent().show();
            $(this).closest("li").find("#previewImageMixGalleryId").parent().show();
            $(this).closest("li").find('#previewImageAltId').parent().show();
            $(this).closest("li").find("#videoCaptionMixGalleryId").parent().parent().show();
            $(this).closest("li").find("[name='./selectImageMixTab']").attr("aria-required", false);
            $(this).closest("li").find("[name='./videoIdMixTab']").attr("aria-required", true);
        }
    });
}

function mediaGalleryV2OnSubmit(event) {
    $(document).on("click", ".cq-dialog-submit", function(event) {
        var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');

        if (formName && formName == 'mediaGalleryV2') {

            var galleryTypeValue = $('#galleryTypeId :selected').val();
            var configureUsingValue = $('#configureUsingId :selected').val();

            if (galleryTypeValue == "images" || !galleryTypeValue) {
                if ($(".coral-Multifield-list li [name='./imageUrl']").length == 0) {
                    $(window).adaptTo("foundation-ui").alert("Required", "Please configure at least one image");
                    event.preventDefault();
                    return false;
                }
                $('input[type="text"][name="./imageUrl"]').each(function() {
                    var urlVal = $(this).val();
                    if (urlVal.length == 0) {
                        $(window).adaptTo("foundation-ui").alert("Required", "Please Configure image");
                        event.preventDefault();
                        return false;
                    } else {
                        return true;
                    }
                });

            } else if (galleryTypeValue == "videos" && configureUsingValue == "fixedList") {
                if ($(".coral-Multifield-list li [name='./videoId']").length == 0) {
                    $(window).adaptTo("foundation-ui").alert("Required", "Please configure at least one video");
                    event.preventDefault();
                    return false;
                }

                $('input[type="text"][name="./videoId"]').each(function() {
                    var urlVal = $(this).val();
                    if (urlVal.length == 0) {
                        $(window).adaptTo("foundation-ui").alert("Required", "Please Configure video");
                        event.preventDefault();
                        return false;
                    } else {
                        return true;
                    }
                });

            } else if (galleryTypeValue == "mixOfImagesAndVideos") {
                if ($(".coral-Multifield-list li [name='./typeMixTab']").length == 0) {
                    $(window).adaptTo("foundation-ui").alert("Required", "Please configure at least one video or image");
                    event.preventDefault();
                    return false;
                }

                var typeValue = $(this).closest("li").find('#typeId :selected').val();
                if (typeValue == "image") {
                    $('input[type="text"][name="./selectImageMixTab"]').each(function() {
                        var urlVal = $(this).val();
                        if (urlVal.length == 0) {
                            $(window).adaptTo("foundation-ui").alert("Required", "Please Configure image");
                            event.preventDefault();
                            return false;
                        } else {
                            return true;
                        }
                    });
                } else if (typeValue == "video") {
                    $('input[type="text"][name="./videoIdMixTab"]').each(function() {
                        var urlVal = $(this).val();
                        if (urlVal.length == 0) {
                            $(window).adaptTo("foundation-ui").alert("Required", "Please Configure video");
                            event.preventDefault();
                            return false;
                        } else {
                            return true;
                        }
                    });
                } else {
                    return true;
                }

            } else {
                return true;
            }
        }
    });
}

//removal of multifield tag on cross button click
function removeCrossButton(indexEle) {
	var removalFlag = true;
	$('[type="button"].coral-TagList-tag-removeButton').each(
			function(index, indexButton) {
				var clickEvent = $(indexButton).attr('data-onRemoveClick');
				var removeFuncIndex;
				if(clickEvent){
					removeFuncIndex = clickEvent[29];
				if (removalFlag && removeFuncIndex == indexEle.toString()
						&& clickEvent[30] == ')') {
					$(indexButton).closest('.coral-TagList-tag').remove();
					removalFlag = false;
				} else {
					removeFuncIndex = clickEvent[29] + clickEvent[30];
					if (removalFlag && removeFuncIndex == indexEle.toString() && clickEvent[31] == ')') {
						$(indexButton).closest('.coral-TagList-tag').remove();
						removalFlag = false;
					}else {
						removeFuncIndex = clickEvent[29] + clickEvent[30] + clickEvent[31];
						if (removalFlag && removeFuncIndex == indexEle.toString() && clickEvent[32] == ')') {
							$(indexButton).closest('.coral-TagList-tag').remove();
							removalFlag = false;
						}
					}
				}
				}
			});

}

function cleanipediaPairedContentOnClick() {	
        if (document.getElementById('pairedContentCheckBox').checked) {
            $("[aria-controls='pairedContentTab']").show();
			$("input[type='text'][name='./articleId']").attr('aria-required', false);
        } else {
            $("[aria-controls='pairedContentTab']").hide();
			$("input[type='text'][name='./articleId']").attr('aria-required', false);
        }	
}

function cleanipediaRelatedContentOnClick() {	
        if (document.getElementById('relatedContentCheckBox').checked) {
            $("[aria-controls='relatedContentId']").show();
			$("input[type='text'][name='./contentQuery']").attr('aria-required', true);
        } else {
            $("[aria-controls='relatedContentId']").hide();
			$("input[type='text'][name='./contentQuery']").attr('aria-required', false);
        }	
}

function tagAutoComplete(index){
	$('.js-coral-Autocomplete-tagList').each(function(index,indexUl) {
		var ulResizeEvent = $(indexUl).attr('data-onClassChange');
		var updateIndex = ulResizeEvent[27];
	   if(ulResizeEvent){
		   if(updateIndex == index.toString() && ulResizeEvent[28] == ')'){
			   removeDuplicateMultiTagInsideTag(indexUl);
		   } else {
			   updateIndex = ulResizeEvent[27]+ulResizeEvent[28];
			   if(updateIndex == index.toString() && ulResizeEvent[29] == ')'){
				   removeDuplicateMultiTagInsideTag(indexUl);
			   }else {
				   updateIndex = ulResizeEvent[27] + ulResizeEvent[28] + ulResizeEvent[29];
					if (updateIndex == index.toString() && ulResizeEvent[30] == ')') {
						removeDuplicateMultiTagInsideTag(indexUl);
					}
				}
		   }
	   }
	            });
	}

function removeDuplicateMultiTagInsideTag(indexUl){
	var lis = $(indexUl).children();
	   for(i=0;i<lis.length-1;i++){
		   var lastLi = $(lis[lis.length-1]);
		   var lastLiText = $(lastLi).text();
		   if($(lis[i]).text() == lastLiText){
			   lastLi.remove();
			   break;
		   }
	   }
}

function overwriteReadTimeOnLoad(){
	if (document.getElementById('overwriteReadTime').checked) {
		$('input[type="number"][name="./pageReadTime"]').prop('disabled',false);
        $('input[type="number"][name="./pageReadTime"]').attr('readonly',false);
        $('#pageReadTime button').prop('disabled', false);
	}else{
		$('input[type="number"][name="./pageReadTime"]').prop('disabled',true);
        $('input[type="number"][name="./pageReadTime"]').attr('readonly',true);
        $('#pageReadTime button').prop('disabled', true);
	}
}
//Reviews component. Multiple Product View Supported
function multipleProductViewOnClick() {
    var multipleProductViewSupported = document.getElementById('multipleProductViewId');
    if (multipleProductViewSupported != null && multipleProductViewSupported !== "undefined") {
        if (document.getElementById('multipleProductViewId').checked) {
            $("#singleProductViewId").prop("checked", false);
            $("#productPageId").parent().hide();
			$("#htmlCodeId").parent().show();
            $("[name='./htmlCode']").attr("aria-required", true);
        } else {
            $("#productPageId").parent().show();
			$("#htmlCodeId").parent().hide();
            $("[name='./htmlCode']").attr("aria-required", false);
        }
    }
}
//Reviews component. Single Product View Supported
function singleProductViewOnClick() {
        if (document.getElementById('singleProductViewId').checked) {
            $("#multipleProductViewId").prop("checked", false);
            $("#productPageId").parent().show();
			$("#htmlCodeId").parent().hide();
            $("[name='./htmlCode']").attr("aria-required", false);
        } else {
            multipleProductViewOnClick();
        }
}

function resourceListingOnSelect(ele){
    var fileNumberId = parseInt($(ele).val());
	var eleArr=$(ele).closest('.coral-Form-fieldwrapper').nextAll();
    for(var i=0;i<eleArr.length;i++){
    	$(eleArr[i]).hide();
    }
    for(var i=1;i<=fileNumberId;++i){
    	if(eleArr.find("[name='./filePath"+i+"']")){
    		eleArr.find("[name='./filePath"+i+"']").attr("aria-required", true);
    		$(eleArr[i-1]).show();
    	}	
    }
    for(var i=fileNumberId;i<eleArr.length;++i){
        var j = i+1;
        if(eleArr.find("[name='./filePath"+j+"']")){
        	eleArr.find("[name='./filePath"+j+"']").attr("aria-required", false);
    		$(eleArr[i]).hide();
        }
    }
}

function pageListingV2NumberOfPages(event){
	if (document.getElementById('pageListingV2SummariseNumberOfpages').checked) {
    $("#pageListingV2PageCountLabel").show();                                                       
    }else{
    $("#pageListingV2PageCountLabel").hide();
     }
}

function pageListingV2Resources(event){
	if (document.getElementById('pageListingV2SummariseResources').checked) {
    $("#pageListingV2ResourceCountLabel").show();                                                       
    }else{
    $("#pageListingV2ResourceCountLabel").hide();
     }
}

function changeDescriptionInDisplaySection() {
    var options=$('#providerId option:selected').val();
    var key="socialSharing.description";
    if(options == 'jiaThis'){ 
        key='socialSharing.description_jia';
        $('#codesOptionsId').siblings('span.coral-Form-fieldinfo').data('quicktip-content',CQ.I18n.getMessage(key));
    }else{
        key="socialSharing.description";
        $('#codesOptionsId').siblings('span.coral-Form-fieldinfo').data('quicktip-content',CQ.I18n.getMessage(key));
    }
}

function heroLaunchVideoOnLoad() {
	$("[aria-controls='controlsId']").hide();                                                
}

function onCouponTypeChange(){

	var couponType=$("[name='./couponType']").val();
if (document.getElementById('multipleSelection').checked) {
$("#multiCtaLabel").parent().show();
}
    else
    {
$("#multiCtaLabel").parent().hide();
    }

      if(couponType=="Manual")
      {$("#multiCtaLabel").parent().hide();

$("#multipleSelection").parent().parent().hide();


		$("[aria-controls='manualConfiguration']").show();
		$("[aria-controls='webBricksConfiguration']").hide();

          $("input[type='text'][name='./offerCodeWeb']").attr('aria-required', false);
           $("input[type='text'][name='./checkCodeWeb']").attr('aria-required', false);
           $("input[type='text'][name='./longKeyWeb']").attr('aria-required', false);
           $("input[type='text'][name='./shortKeyWeb']").attr('aria-required', false);


	}

 else if(couponType=="WebBricks")
 {
		$("[aria-controls='manualConfiguration']").hide();
		$("[aria-controls='webBricksConfiguration']").show();
$("#multipleSelection").parent().parent().show();

     $("input[type='text'][name='./offerCodeWeb']").attr('aria-required',true);
           $("input[type='text'][name='./checkCodeWeb']").attr('aria-required',true);
           $("input[type='text'][name='./longKeyWeb']").attr('aria-required',true);
           $("input[type='text'][name='./shortKeyWeb']").attr('aria-required',true);
	     }

    else
    {$("[aria-controls='manualConfiguration']").hide();
		$("[aria-controls='webBricksConfiguration']").hide();
		$("#multiCtaLabel").parent().hide();
		$("#multipleSelection").parent().parent().hide();

     $("input[type='text'][name='./offerCodeWeb']").attr('aria-required', false);
           $("input[type='text'][name='./checkCodeWeb']").attr('aria-required', false);
           $("input[type='text'][name='./longKeyWeb']").attr('aria-required', false);
           $("input[type='text'][name='./shortKeyWeb']").attr('aria-required', false);

    }


    
}

function formTypeOnSelect(event) {
    formType(event.selected);
}

function formType(selectedValue){
    var formType = $('#formTypeId :selected').val();
    if(formType == 'custom') {
		$("#customTypeNameId").parent().show();
        $("[name='./customTypeName']").attr("aria-required", true);
	}
    else{
        $("#customTypeNameId").parent().hide();
        $("[name='./customTypeName']").attr("aria-required", false);
    }
}
function isSecurePageOnLoad(){
	if (document.getElementById('isSecurePage').checked) {        
		$("input[type='text'][name='./redirectUrl']").attr('aria-required', 'true');
        $('input[type="text"][name="./redirectUrl"]').attr('readonly',false);
	}else{
        $("input[type='text'][name='./redirectUrl']").attr('aria-required', 'false');
        $('input[type="text"][name="./redirectUrl"]').attr('readonly',true);
	}
}

//global footer v2 validation on brandAltText listener
function globalFooterV2OnSubmit() {
    $(document).on("click", ".cq-dialog-submit", function(event) {
        var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');
        if (formName && formName == 'globalfooterv2') {
            if (($("input[type='text'][name='./brandLogoIcon']").val().length > 0)) {
                $("input[type='text'][name='./brandAltText']").attr('aria-required', 'true');
            } else {
                $("input[type='text'][name='./brandAltText']").attr('aria-required', 'false');
            }
        }
    });
}

function includeVideoOnLoad(){
	$("[aria-controls='videoControlsTab']").hide();
	$("input[type='text'][name='./videoId']").attr('aria-required', 'false');
	if (document.getElementById('includeVideoId').checked) {        
		$("[aria-controls='videoControlsTab']").show();
		$("input[type='text'][name='./videoId']").attr('aria-required', 'true');
	}else{
		$("[aria-controls='videoControlsTab']").hide();
		$("input[type='text'][name='./videoId']").attr('aria-required', 'false');
	}
}

function fixedRadioOnSelect(event) {
    fixedRadioOnClick(event.selected);
} 

function fixedRadioOnClick(){
    $("#channelPlayListId").parent().hide();
    $("#socialGalleryVideoPostsMultifieldId").parent().hide();
    $("#socialGalleryFixedVideoId").parent().show();


}

function channelPlayListOnSelect(event) {
    ChannelPlayListRadioOnClick(event.selected);
}

function ChannelPlayListRadioOnClick(){
    $("#channelPlayListId").parent().show();
    $("#socialGalleryVideoPostsMultifieldId").parent().show();
    $("#socialGalleryFixedVideoId").parent().hide();
}
function showHidePreFillFormUrl(){
if (document.getElementById('preFillFormWithProfileDataId').checked) {
            $("#preFillFormUrlId").parent().show();  //displaying the include In Display Section
        } else {
            $("#preFillFormUrlId").parent().hide(); 
        }
}
// live Chat v2 chat Service on select function
function chatServiceOnSelect() {
    var selectedValue = $('#chatServiceId :selected').val();
    if (selectedValue == "liveChatOnly" || !selectedValue) {    	
		$("[aria-controls='fbMessengerTabId']").hide();
		$("[aria-controls='liveChatTabId']").show();
        $("#otherChatServiceId").parent().hide();
	} else if(selectedValue == "fbMessengerOnly") {
		$("[aria-controls='fbMessengerTabId']").show();
		$("[aria-controls='liveChatTabId']").hide();
        $("#otherChatServiceId").parent().hide();
    } else if(selectedValue == "liveChatWithOtherService") {
		$("[aria-controls='fbMessengerTabId']").show();
		$("[aria-controls='liveChatTabId']").show();
        $("#otherChatServiceId").parent().show();
    }
}
// live Chat v2 facebook widget type on select function
function cleanipediaRelatedContentOnSubmit(){
	$(document).on("click", ".cq-dialog-submit", function (event) {
        var formName = $(this).closest('form').find("div.cq-dialog-content").attr('data-formName');
		if(formName && formName=='cleanipediaRelatedContent') {
		    if($(".coral-Multifield-list li [name='./articleId']").length==0){
           		$(window).adaptTo("foundation-ui").alert("Required","Please configure at least one article");
            	event.preventDefault();
            return false;
      	}
	 }
	});
}function fbWidgetTypeOnSelect() {
    var selectedValue = $('#fbWidgetTypeId :selected').val();
	if(document.getElementById('isOverrideGlobalConfig').checked){
		if (selectedValue == "messageUs" || !selectedValue) {
			$("#buttonSizeId").parent().hide();
			$("#buttonColourId").parent().hide();
			$("#fbAppUniqueId").parent().hide();
			$("#messengerCtaId").parent().show();
		} else if(selectedValue == "sendToMessenger") {
			$("#messengerCtaId").parent().hide();
			$("#buttonSizeId").parent().show();
			$("#buttonColourId").parent().show();
			$("#fbAppUniqueId").parent().show();
		}
	}
}

function soundCloudAttributes(event){
	if (document.getElementById('overrideGlobalConfigLimit').checked) {
    	$("#soundCloudAttributesForDisplay").show();
        $("#embeddedPlayerType").show();
    }else{
    	$("#soundCloudAttributesForDisplay").hide();
        $("#embeddedPlayerType").hide();
     }
}

function widgetTypeOnSelect(){
    var widgetType = $('#soundCloudWidgetType :selected').val();
	var widgetParentNext = $('#soundCloudWidgetType').parent().nextAll();
    if(widgetType=='Sound Widget' || !soundCloudWidgetType){
    	$("#soundCloudStartingTrack").parent().hide();
		widgetParentNext.slice(0, 1).show();
        widgetParentNext.slice(1, 3).hide();
    }else if(widgetType=='User Widget'){
    	$("#soundCloudStartingTrack").parent().show();
		widgetParentNext.slice(0, 2).hide();
        widgetParentNext.slice(2, 3).show();
    }else if(widgetType=='PlayList Widget'){
    	$("#soundCloudStartingTrack").parent().show();
		widgetParentNext.slice(0, 1).hide();
        widgetParentNext.slice(1, 2).show();
		widgetParentNext.slice(2, 3).hide();
    }
}

function onAudioPlayerChange(){
    var audioSource=$('#audioSourceId :selected').val();
	if(audioSource == "soundCloud"){
		$("[aria-controls='audioPlayerSoundCloudAttributes']").show();
	}
    else {
        $("[aria-controls='audioPlayerSoundCloudAttributes']").hide();
	}

}

//breadcrum on select

function breadcrumbTypeOnSelect(){
    if ($('#typeId :selected').val() == "page"){
        $('#parentlevel').parent().hide();
		$('#startingpage').parent().show();
        }
    else {
        $('#startingpage').parent().hide();
		$('#parentlevel').parent().show();
    }

}
//Live Chat V2, Show or Hide Include In Display Section
function liveChatGlobalConfigShowHide() {
    if (document.getElementById('isOverrideGlobalConfig').checked) {
        $("#includeInDisplaySection").show(); //displaying the include In Display Section
    } else {
        $("#includeInDisplaySection").hide();
    }
    fbWidgetTypeOnSelect();
}

//SocialShares code selection
function socialShareCodeOnSelect(){
    if ($('#providerId :selected').val() == "jiaThis"){
    	$('.jiaThis-select').parent().show();
		$('.addThis-select').parent().hide();
		$('.addThis-select select').attr("name","");
		$('.jiaThis-select select').attr("name","./serviceCode");
       }
   else {
       	$('.jiaThis-select').parent().hide();
		$('.addThis-select').parent().show();
		$('.jiaThis-select select').attr("name","");
		$('.addThis-select select').attr("name","./serviceCode");
   }

}
//Coupons component. Hide Expired Coupons
function expiredCouponsShowHide() {
        if (document.getElementById('hideExpiredCouponsId').checked) {
            $("#expiryMessage").parent().parent().hide();
        } else {
            $("#expiryMessage").parent().parent().show();
        }
}
//The following code will remove max-with from RTE
$(document).on("click", ".cq-dialog-submit", function (event) {
	$(".coral-RichText-ui").find(".is-floating").each(function() {
		 $(this).addClass("is-active");
	 });
	$('.coral-RichText-editable').each(function(index){
		$(this).attr("contenteditable","true");
		if(!($(this).html()) || $(this).html().length<1){
			$(this).html('<p><br _rte_temp_br="brEOB"></p>');
		}
		var pVal = "";
		$(this).children('p').each(function(i){
			$(this).css("max-width","");
            var pStyle = $(this).attr('style');
			if(pStyle){
				pVal = pVal+"<p style='" +$(this).attr('style')+"'"+">"+$(this).html()+"</p>";
            }else if($(this).html().length>0){
				pVal = pVal+"<p>"+$(this).html()+"</p>";
            }
		});
		if(pVal.length>0 && pVal.indexOf("<p")>=0){
		$($(this).siblings('input[type="hidden"]')[0]).val(pVal);
		}
	});
}); 

function rewardPointsOnLoad(){
	rewardPointsShowTypeOnSelect();
}

function rewardPointsShowTypeOnSelect(){
    if ($('#showId :selected').val() == "pointsAndNumber"){
        $('#pointsHeadingTextId').parent().parent().show();
		$('#pointsLabelId').parent().show();
        $('#codeEnteredHeadingTextId').parent().parent().show();
		$('#codeEnteredLabelId').parent().show();
        }
    else if ($('#showId :selected').val() == "pointsOnly"){
        $('#pointsHeadingTextId').parent().parent().show();
		$('#pointsLabelId').parent().show();
        $('#codeEnteredHeadingTextId').parent().parent().hide();
		$('#codeEnteredLabelId').parent().hide();
    }
    else {
        $('#pointsHeadingTextId').parent().parent().hide();
		$('#pointsLabelId').parent().hide();
        $('#codeEnteredHeadingTextId').parent().parent().show();
		$('#codeEnteredLabelId').parent().show();
    }

}


function siteConfigOnLoad(_self, selectedValue) {
	siteConfigKeyType($(_self).closest("li"), selectedValue);
	if(!(_self) || _self == undefined){
	siteConfigTypeSelect();
	}
	window.setTimeout(function() {
        $(".coral-TabPanel-content").scrollTop(0);
    }, 100);
}

function siteConfigTypeSelect(){
	var $allCategoryTypes = $("select[name='./categoryType']");
	_.each($allCategoryTypes, function(selectBox, index) {
	var box = $(selectBox).val();
	var configValue = $(selectBox).closest('.coral-Form-fieldwrapper').siblings('.coral-Form-fieldwrapper').find('#configValue');
	var configRteValue = $(selectBox).closest('.coral-Form-fieldwrapper').siblings('.coral-Form-fieldwrapper').find('#configRteValue');
	if($(configValue)){
		$(configValue).parent().show();
		$(configRteValue).parent().parent().hide();
		}
		if(box && box == 'RICHTEXT' && $(configValue)){
			$(configValue).parent().hide();
			$(configRteValue).parent().parent().show();
		}
	    });
}

function siteConfigKeyTypeOnSelect(event) {
	siteConfigKeyType($(event.target).closest("li"), event.selected);
}

function siteConfigKeyType(parentLI, selectedValue) {
    if (selectedValue == 'STRING' || selectedValue == '' || selectedValue == undefined || !selectedValue) {
        parentLI.find("#configRteValue").parent().parent().hide();
        parentLI.find("#configValue").parent().show();

    } else if (selectedValue == 'RICHTEXT') {
    	 parentLI.find("#configRteValue").parent().parent().show();
         parentLI.find("#configValue").parent().hide();
    }
}

function showHideVideoControls(id){
	if(id !== undefined){
		var dialogForm = $('#' + id).parents('form.cq-dialog');
		var navTab = dialogForm.find('nav.coral-TabPanel-navigation');
		var selectedValue = $('#' + id +' :selected').val();
		var displayOverlay = dialogForm.find("input[name='./displayInOverlay']").parents(".coral-Form-fieldwrapper--singleline");
		var hideControls =  dialogForm.find("input[name='./hideVideoControls']").parents(".coral-Form-fieldwrapper--singleline");
		var muteAudio = dialogForm.find("input[name='./muteVideoAudio']").parents(".coral-Form-fieldwrapper--singleline");
		var loopVideo = dialogForm.find("input[name='./loopVideoPlayback']").parents(".coral-Form-fieldwrapper--singleline");
		var relatedVideos = dialogForm.find("input[name='./relatedVideoAtEnd']").parents(".coral-Form-fieldwrapper--singleline");
		var autoPlay = dialogForm.find("input[name='./autoplay']").parents(".coral-Form-fieldwrapper--singleline");
		
		var controlSection = displayOverlay.parent('section.coral-Form-fieldset');
		controlSection.show();
		//for socialMediaGallery.
		var resourceVal = dialogForm.find("input[name='./sling:resourceType']").val();
		
		var navIndex = 0;
		var checkForFixedList = false;
		if(resourceVal.indexOf('/videoPlayer') != -1){
			navIndex = 2;
			navTab.find("a:nth-child(2)").show();
		}else if(resourceVal.indexOf('/mediaGalleryV2') != -1){
			navIndex = 5;
			if(navTab.find("a:nth-child(3)").is(':visible')){
				navTab.find("a:nth-child(5)").show();
			}else if(navTab.find("a:nth-child(4)").is(':visible')){
				navTab.find("a:nth-child(5)").show();
			}
			if(dialogForm.find("select[name='./galleryType']").val().indexOf('mixOfImagesAndVideos') != -1){
				checkForFixedList = true;
			}
		}
		if(resourceVal !== undefined && resourceVal.indexOf('socialMediaGallery') != -1){
			relatedVideos = dialogForm.find("input[name='./showRelatedVideos']").parents(".coral-Form-fieldwrapper--singleline");
			if(dialogForm.find("select[name='./galleryType']").val().indexOf('fixed') != -1){
				checkForFixedList = true;
			}
			if(!dialogForm.find('input[name="./overrideGlobalConfig"]').prop('checked')){
				controlSection.hide();
				return;
			}
		}

		if(checkForFixedList){
			displayOverlay.show();
			hideControls.show();
			muteAudio.show();
			loopVideo.show();
			relatedVideos.show();
			autoPlay.show();
			return;
		}
		if (selectedValue == "youku" || !selectedValue) {
			displayOverlay.show();
			autoPlay.show();
			hideControls.hide();
			muteAudio.hide();
			loopVideo.hide();
			relatedVideos.hide();
		}
		if (selectedValue == "iqiyi" || !selectedValue) {
			autoPlay.show();
			displayOverlay.hide();
			hideControls.hide();
			muteAudio.hide();
			loopVideo.hide();
			relatedVideos.hide();
		}
		if (selectedValue == "tencent" || !selectedValue) {
			autoPlay.show();
			displayOverlay.hide();
			hideControls.hide();
			muteAudio.hide();
			loopVideo.hide();
			relatedVideos.hide();
		}
		if (selectedValue == "youtube" || !selectedValue) {
			displayOverlay.show();
			hideControls.show();
			muteAudio.show();
			loopVideo.show();
			relatedVideos.show();
			autoPlay.show();
		}
		if (selectedValue == "tudou" || !selectedValue) {
			controlSection.hide();
			if(navIndex > 0){
				navTab.find("a:nth-child(" + navIndex + ")").hide();
			}
		}
		if (selectedValue == "sohu" || !selectedValue) {
			controlSection.hide();
			if(navIndex > 0){
				navTab.find("a:nth-child(" + navIndex + ")").hide();
			}
		}
		if (selectedValue == "vimeo" || !selectedValue) {
			controlSection.hide();
			if(navIndex > 0){
				navTab.find("a:nth-child(" + navIndex + ")").hide();
			}
		}
	}
}

function showHideMultiBuyKeys() {
	$("#multiBuyAllowAddRemoveId").parent().parent().hide();
	$("#multiBuyDefaultProductSelectionId").parent().parent().hide();
    if (document.getElementById('multiBuyEnabledId').checked) {
    	$("#multiBuyAllowAddRemoveId").parent().parent().show();
    }
    if (document.getElementById('multiBuyEnabledId').checked && document.getElementById('multiBuyAllowAddRemoveId').checked) {
    	$("#multiBuyDefaultProductSelectionId").parent().parent().show();
    }
}
function paginationLabelHideRewardRedemption(){
	var pagiType = $("select[name='./paginationType']").val();
    if(pagiType == 'pagination'){
		$("select[name='./paginationType']").parents('.coral-Form-fieldwrapper').nextAll().slice(1, 2).hide();
    }else{
		$("select[name='./paginationType']").parents('.coral-Form-fieldwrapper').nextAll().slice(1, 2).show();
    }
}