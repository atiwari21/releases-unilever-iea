$(document).on('cui-contentloaded.data-api', function (e) {
    var $selectList = $('.js-coral-Autocomplete-selectList');
    $($selectList).each(function() {
        $(this).html($(this).children("li").sort(function(a, b) {
            a = $(a).text();
            b = $(b).text();
            if (a > b || a === void 0){
                return 1;
            }else if (a < b || b === void 0){
                return -1;
            }
            
            return 1;
        }));
    });
});