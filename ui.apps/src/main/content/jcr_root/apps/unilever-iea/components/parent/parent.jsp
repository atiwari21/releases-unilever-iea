<%--
   Copyright 2013, IEA, SapientNitro;  All Rights Reserved.
 
   This software is the confidential and proprietary information of
   SapientNitro, ("Confidential Information"). You shall not
   disclose such Confidential Information and shall use it only in
   accordance with the terms of the license agreement you entered into
   with SapientNitro.

  ==============================================================================

  Parent Component

  <parent.jsp>

  This script is the default script that is executed for all child components.
  This component serves as a framework which will be extended
  by all the IEA components.
       Responsibilities:-
       1. Include common clientlibs for author/publish modes.
       2. Validate the configuration state of the component.
       3. Delegate to process.jsp when the component is in configured state.

       Usage Principle :- 
       1. Take care in creating the validation.jsp in respective child components
       and defining the editmodecondition in that.
       2. Do not override this file under any circumstances.

       Input:- editModeCondition (defaults to false), componentTitle.
       Output:- Includes process.jsp
  ==============================================================================
--%>
           <%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="/apps/unilever-iea/commons/global.jsp" %>
<cq:setContentBundle />

<cq:include script="validation.jsp" />

<%--
Component Title will give a component name and by default it will 
use this name for showing the config message.
Use a i18n node in your component and define a key value pair as
           <component-name>:<new-value>
--%>
<%-- Added try catch for error handling purpose .It will be removed after development  --%>
<c:choose>
    <c:when test="${editModeCondition}">
        <c:if test="${requestScope['com.day.cq.wcm.api.WCMMode']=='EDIT' 
          || requestScope['com.day.cq.wcm.api.WCMMode']=='DESIGN' || requestScope['com.day.cq.wcm.api.WCMMode']=='PREVIEW'}">
               <div id="iea-component-${componentTitle}" class="cq-heading">
           		 <img src="/libs/cq/ui/resources/0.gif"
               	 	class="cq-list-placeholder" alt="author image" />
                  <h3>
                      <fmt:message key="configureLabel" />  
                      &nbsp;<fmt:message key="${component.title}"/>&nbsp;
                      <fmt:message key="component" />
                  </h3>
              </div>
        </c:if>
    </c:when>

 	<c:otherwise>
                <c:if test="${requestScope['com.day.cq.wcm.api.WCMMode']=='EDIT' 
          || requestScope['com.day.cq.wcm.api.WCMMode']=='DESIGN' || requestScope['com.day.cq.wcm.api.WCMMode'] == 'PREVIEW'}">
               <div id="iea-component-${componentTitle}" class="cq-heading" style="display: none;">
                    <h4>
                      &nbsp;<fmt:message key="${component.title}"/>&nbsp;
                  </h4>
              </div>
        </c:if>
            <cq:include script="process.jsp" />
    </c:otherwise>
</c:choose>