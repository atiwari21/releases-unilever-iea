<%@page import="org.apache.sling.api.resource.ValueMap"%>
<%@ include file="/apps/iea/commons/global.jsp" %>
<%@page import="org.json.JSONObject"%>
<%@page import="org.apache.sling.api.SlingHttpServletRequest"%>
<%@page import="org.json.JSONArray"%>
<%@page import="javax.jcr.Node"%>
<%@page import="org.apache.sling.api.resource.Resource"%>
<%

    //JSONArray arr=new JSONArray();
String[] pathArr=slingRequest.getParameterValues("path")!=null?slingRequest.getParameterValues("path"):new String[]{};

String[] arr=new String[pathArr.length];

JSONArray jsonArr=new JSONArray();

for(int i=0;i<pathArr.length;++i){
    String path=pathArr[i];

	Resource questionRes=resourceResolver.getResource(path);

	ValueMap vm=questionRes.getValueMap();

    String headingText=(String)vm.get("headingText");
    String questionId=(String)vm.get("questionId");
	JSONObject printObj=new JSONObject();
	printObj.put("text",headingText);
    printObj.put("value",questionId);
    printObj.put("path",path);

	jsonArr.put(printObj);

    JSONObject obj=new JSONObject();
    obj.put("diagnosticToolPath", path);
    arr[i]=obj.toString();
}
currentNode.setProperty("questionsConfiguration", (Value)null);
if(arr.length>0){
	currentNode.setProperty("questionsConfiguration", arr);
}
resourceResolver.commit();


out.print(""+jsonArr);
%>
