<%--
  <validation.jsp>
    
  Copyright 2013 IEA, SapientNitro
  All Rights Reserved.

  This software is the confidential and proprietary information of
  SapientNitro, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with SapientNitro.

  ==============================================================================
  This file is used to initialize the edit mode condition and the component title
  for performing unconfigured component validation on the author side. 
  ==============================================================================

--%>
<%@ include file="/libs/foundation/global.jsp" %>
<c:if test="${requestScope['com.day.cq.wcm.api.WCMMode']=='EDIT'}">
    <cq:includeClientLib categories="iea.custom-xtype" />
    <cq:includeClientLib categories="iea.widgets" />
</c:if>

<c:set var="editModeCondition" value="${empty properties.tabTitle}" scope="request"/>
<c:set var="componentTitle" value="tab" scope="request"/>

