<%@ include file="/apps/iea/commons/global.jsp" %>
<cq:include script="/apps/iea/commons/init.html" />
<%@ taglib prefix="wcmmode" uri="http://www.adobe.com/consulting/acs-aem-commons/wcmmode" %>
<c:if test="${isAdaptive != 'true'}">
<%@include file="/apps/iea/commons/clientlibs.jsp"%>
<%@ page import="com.unilever.platform.foundation.components.helper.*" %>
<%@ page import="com.sapient.platform.iea.aem.utils.CommonUtils" %>
<c:set var = "serverSideValidation" value = "${properties.serverSideValidation}" scope = "request" />
<cq:setContentBundle />
<c:choose>
      <c:when test="${not empty properties.clientSideComponentName}">
      	<c:set var="clientSideComponentName" value="${properties.clientSideComponentName}" scope="request" />
      </c:when>

      <c:otherwise>
    	<c:set var="clientSideComponentName" value="${component.name}" scope="request" />
      </c:otherwise>
</c:choose>
<c:set var="isAjax" value="${properties.isAjax}" />
<c:set var="redirectURL" value="${properties.redirect}" />
<c:set var="type" value="${properties.type}" />
<c:set var="class" value="${properties.class}" />
<c:set var="randomNumber" value="${data.randomNumber}" />
<c:if test="<%= CommonUtils.isMultiValued((Object)pageContext.getAttribute("randomNumber"))%>">
    <c:set var="randomNumber" value="${data.randomNumber[0]}" />
</c:if>

<script type="module/config" id="form-${randomNumber}-config">
        <ieaFoundation:jsonConverter data="${data}" errorReporting="false"/>
</script>
<div class="container-fluid  ${clientSideComponentName} ${' '} <c:if test="${not empty type}">  ${properties.type}  </c:if>${' '} <c:if test="${not empty class}">  ${properties.class}  </c:if>" data-config="form-${randomNumber}-config" data-role="${clientSideComponentName}" data-server = "true">
<c:set var="action" value="${data.formV2.formSubmitPath}"/>
<c:set var="componentExperienceVariant" value="${data.formV2.componentExperienceVariant}"/>
<c:set var="redirectPage" value="${properties.redirectPage}"/>
    <c:if test="${not empty redirectPage}">
<c:set var="redirectPageFullURL" value='<%=ComponentUtil.getFullURL(resourceResolver, resourceResolver.map((String)pageContext.getAttribute("redirectPage")), slingRequest)%>'/>
</c:if>
    <c:set var="componentVariant"><%=properties.get("viewType","defaultView")%></c:set>
    <form name="${properties.formIdentifier}" id = "${properties.formIdentifier}" action="${action}" method="POST"  data-ajax="${properties.isAjax}" data-post-url="<%=resourceResolver.map(currentPage.getPath())%>.dcspost.html" novalidate data-componentname="${component.name}" data-component-variants="${componentVariant}" data-component-experience-variant="${componentExperienceVariant}" data-component-positions="<%=ComponentUtil.getComponentPosition(currentPage,slingRequest)%>"
<c:if test="${isAjax}">

 	onsubmit="return false;" 
</c:if>
<c:if test="${not empty redirectPage}">
	data-redirect="${redirectPageFullURL}" 
</c:if>
data-fail="${properties.failureDescription}"  class="form-horizontal form-validate form" role="form" >

<input type="hidden" name="hidden_redirect" data-exclude="true" value="${redirectPageFullURL}" />
<input type="hidden" name="hidden_formIdentifier" data-exclude="true" value="${data.form.formIdentifier}" />
<input type="hidden" name="hidden_successMessage" data-exclude="true" value="${data.form.successMessage}" />
<input type="hidden" name="hidden_formSubmitPath" data-exclude="true" value="${data.form.formSubmitPath}" />
<input type="hidden" name="hidden_isAjax" data-exclude="true" value="${data.form.isAjax}" />
<input type="hidden" name="hidden_currentPath" data-exclude="true" value="${data.path}" />
    <c:set var="formDataUrl" value="${data.formV2.formDataURL}"/>
	<input type="hidden" name="form-data-url" data-exclude="true" value="${formDataUrl}" />
	

    <c:set var="getSurveyUrl" value="${data.formV2.getSurveyUrl}"/>
	<input type="hidden" name="get-survey-url" data-exclude="true" value="${getSurveyUrl}" />	
	
	
<c:if test="${wcmmode:isPreview(pageContext) || wcmmode:isDisabled(pageContext)}">
    <% componentContext.setDefaultDecorationTagName(""); %>
</c:if>
<cq:include path="form_par" resourceType="foundation/components/parsys" />
<c:if test="${wcmmode:isPreview(pageContext) || wcmmode:isDisabled(pageContext)}">
    <% componentContext.setDefaultDecorationTagName("div"); %>
</c:if>
            <div id="renderJson"></div>
    </form>

</div>
</c:if>