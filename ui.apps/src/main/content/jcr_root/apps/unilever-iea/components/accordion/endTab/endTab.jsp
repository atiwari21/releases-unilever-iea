<%--

  <endTab.jsp>

  Copyright 2013 IEA, SapientNitro
  All Rights Reserved.

  This software is the confidential and proprietary information of
  SapientNitro, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with SapientNitro.

  ==============================================================================

  This is to provide an end bar for the Tab component.
  This fil is kept blank as no action is required after include of this component.
  It will always be used as a part of its parent component.

  ==============================================================================

--%>