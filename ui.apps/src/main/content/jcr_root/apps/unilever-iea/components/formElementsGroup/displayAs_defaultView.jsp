<%--
  <process.jsp>

  Copyright 2013 IEA, SapientNitro
  All Rights Reserved.

  This software is the confidential and proprietary information of
  SapientNitro, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with SapientNitro.

  ==============================================================================
  This file contains the main logic to create the tab component display which
  includes a title on the top and a content parsys for every configured tab. 
  ==============================================================================

--%>

<%@ include file="/libs/foundation/global.jsp" %>
<cq:include script="/apps/iea/commons/init.html" />

<cq:setContentBundle />    
<div data-role="${properties.clientSideComponentName}" class="${properties.clientSideComponentName}">
<div class="${properties.clientSideComponentName} ${data.formElementsGroup.style.class}" aria-label="${properties.clientSideComponentName}"
     role="tablist"
     data-componentname="${data.formElementsGroup.componentName}"
     data-component-variants="${data.formElementsGroup.componentVariation}"
     data-component-positions="${data.formElementsGroup.componentPosition}">
    ${data.formElementsGroup.text}
	<cq:include path="form_elements_group_par" resourceType="foundation/components/parsys" />
</div>
</div>