<%--

  <constraintType.json.jsp>

  Copyright 2013 IEA, SapientNitro
  All Rights Reserved.

  This software is the confidential and proprietary information of
  SapientNitro, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with SapientNitro.

  ==============================================================================

  This will return the json for constraint type dropdown.

  ==============================================================================

--%>
<%@ include file="/libs/foundation/global.jsp" %>


[  
   {  
      "text":"Please Select",
      "value":""
   },
   {  
      "text":"CCV",
      "value":"[0-9]{3,4}"
   },
   {  
      "text":"Date",
      "value":"(^(?:0[1-9]|[12][0-9]|3[01])[/](?:0[1-9]|1[012])[/][0-9]{4}$)|(^(?:0[1-9]|1[012])[/](?:0[1-9]|[12][0-9]|3[01])[/][0-9]{4}$)"
   },
   {  
      "text":"Email",
      "value":"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$"
   },
   {  
      "text":"Luhn Algorithm",
      "value":"creditcard"
   },
   {  
      "text":"Month",
      "value":"0[1-9]|1[0-2]"
   },
   {  
      "text":"Name",
      "value":"^[a-zA-Z0-9 ]*[a-zA-Z ]+[a-zA-Z0-9 ]*$"
   },
   {  
      "text":"Numeric",
      "value":"^[0-9]*$"
   },
   {  
      "text":"Year",
      "value":"^[0-9]{2}$|^[0-9]{4}$"
   },
   {  
      "text":"nameuk",
      "value":"^[a-zA-Z\\s]{2,50}$"
   },
   {  
      "text":"mobileuk",
      "value":"^[0-9+#*()\\\\.\\\\-\\\\s]{8,20}$"
   },
   {  
      "text":"emaileuk",
      "value":"^[a-zA-Z0-9_+-]+(\\.[a-zA-Z0-9_+-]+)*@[a-zA-Z0-9\\\\-]+(\\.[a-zA-Z0-9\\-]+)*(\\.[a-zA-Z]{1,})$"
   },
   {  
      "text":"contactusname",
      "value":"^[a-zA-Z0-9\\s]{1,50}$"
   },
   {  
      "text":"fullname",
      "value":"^[a-zA-Z\\\\s]+"
   },
   {  
      "text":"Checkout Full Name",
      "value":"^[A-Za-z0-9]{2}"
   },
   {  
      "text":"Checkout Address",
      "value":"^[A-Za-z0-9]{2}"
   },
   {  
      "text":"postalcode",
      "value":"^([A-Pa-pR-UWYZr-uwyzr0-9][A-Ha-hK-Yk-y0-9][AEHMNPRTVXYaehmnprtvxy0-9]?[ABEHMNPRVWXYabehmnprvwxy0-9]? {1,2}[0-9][ABD-HJLN-UW-Zabd-hjln-uw-z]{2}|GIR 0AA)$"
   },
   {  
      "text":"E-mail Address",
      "value":"^[a-zA-Z0-9_+-]+(\\.[a-zA-Z0-9_+-]+)*@[a-zA-Z0-9\\-]+(\\.[a-zA-Z0-9\\-]+)*(\\.[a-zA-Z]{2,})$"
   },
   {  
      "text":"Prefix",
      "value":"^[\\p{L}\\-'()\\.]{1}[\\s\\p{L}\\p{N}\\-'()\\.]{1,9}$"
   },
   {  
      "text":"First Name/Last Name",
      "value":"^[\\p{L}\\p{N}]{1}[\\s\\p{L}\\p{N}\\-'()\\.,#\\/&]{1,29}$"
   },
   {  
      "text":"Address1/Address2",
      "value":"^[\\p{L}\\p{N}]{1}[\\s\\p{L}\\p{N}\\-'()\\.,#\\/&]{1,39}$"
   },
   {  
      "text":"Address3/City",
      "value":"^[\\p{L}\\p{N}]{1}[\\s\\p{L}\\p{N}\\-'()\\.,#\\/&]{1,29}$"
   },
   {  
      "text":"State",
      "value":"^[\\p{L}\\p{N}]{1}[\\s\\p{L}\\p{N}\\-'()\\.,#\\/&]{1,}$"
   },
   {  
      "text":"us_PostalCode",
      "value":"^\\d{5}(-\\d{4})?$"
   },
   {  
      "text":"PhoneNumber",
      "value":"^\\(?([0-9]{3})\\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$"
   },
   {  
      "text":"PhoneExtn",
      "value":"^[0-9\\-\\.\\s\\(\\)]{0,6}$"
   },
   {  
      "text":"UpcCode",
      "value":"^[0-9]{0,10}$"
   },
   {  
      "text":"Comments",
      "value":"^[\\p{L}\\p{N}\\p{P}\\p{Sc}]{1}[\\s\\p{L}\\p{N}\\p{P}\\p{So}\\p{Sc}+~]{0,1000}$"
   },
   {  
      "text":"province",
      "value":"^[\\p{L}\\p{N}\\-'()\\.,#\\/]{1}[\\s\\p{L}\\p{N}\\-'()\\.,#\\/&]{1,}$"
   },
   {  
      "text":"MobileNumber",
      "value":"^\\+\\(?([0-9]{1})?([0-9]{3})\\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$"
   },
   {  
      "text":"password",
      "value":"^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!*?&])[A-Za-z\\d][A-Za-z\\d@$!*?&]{7,}$"
   },
   {  
      "text":"ca_PostalCode",
      "value":"^[ABCEGHJKLMNPRSTVXYabceghjklmnprstvxy]{1}\\d{1}[A-Za-z]{1}\\s{0,1}\\d{1}[A-Za-z]{1}\\d{1}$"
   }
   ]
