<%--
/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
 --%>

<%@ include file="/apps/iea/commons/global.jsp" %>

<c:set var="editModeCondition" value="${(empty properties.randomNumber) && (empty properties.text)}" scope="request"/>
