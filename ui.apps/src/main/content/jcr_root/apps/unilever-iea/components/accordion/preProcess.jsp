<%--

  Copyright 2013 IEA, SapientNitro

  All Rights Reserved.

  This software is the confidential and proprietary information of

  SapientNirto, ("Confidential Information"). You shall not

  disclose such Confidential Information and shall use it only in

  accordance with the terms of the license agreement you entered into

  with SapientNitro.

  ==============================================================================

  <pre-process.jsp>

  This JSP is the default implementation of the pre-process.jsp which will be 
  overriden by each of the child components, if required. 

      Responsibility:-
      1. Populate the viewType supplied via dialog.


      Benefits of using this before process.jsp:-
	  1. Situations might arise where flexibility might be needed to be provided
         where developer would not want to use the name viewType but any other name
		 for reasons like in list component where whole logic is hard-wired with "listFrom"
         keyword and it cant be overriden.
      2. In framework this would expect the variable name as viewType only but extend this file 
         to customize the behaviour.
         /*It could contain other code as well. Presently this is the condition which could be 
           identified.
          */
      Conventions:-
      1. The view type from dialog will be stored under name viewType.
      2. Only override this file when there is some additional component level
          processing needs to be done.

      Inputs:- View Name from dialog.

      Outputs:- Sets the view name in viewType key.
  ==============================================================================

--%>
<%@ include file="/apps/iea/commons/global.jsp" %>
<c:set var="viewType" value="${properties.viewType}" scope="request" />
<c:set var="clientSideComponentName" value = "accordion-content" scope="request" />

<c:set var="list" value="${data.panelList}" scope="request" />

<c:if test="${not empty list && fn:length(list[0].randomNumber) >0 }">
    <c:set var="randomNumber" value="${list[0].randomNumber}" scope="request"/>
</c:if>
