<%--

  Copyright 2013 IEA, SapientNitro

  All Rights Reserved.

  This software is the confidential and proprietary information of

  SapientNirto, ("Confidential Information"). You shall not

  disclose such Confidential Information and shall use it only in

  accordance with the terms of the license agreement you entered into

  with SapientNitro.

  ==============================================================================

  <process.jsp>

  This JSP is the default implementation of the process.jsp which will be 
  overriden by each of the child components, if required. 

      Responsibility:-
      1. Include the parsys structures based on user inputs.
      2. Include component level clientlibs.


  ==============================================================================

--%>
 <%@ include file="/libs/foundation/global.jsp" %>
    <%@page import="com.sapient.platform.iea.aem.utils.ClientLibUtils" %>
    <cq:setContentBundle />
<div class= "c-column-control <c:if test="${not empty properties.type}">c-column-control--${properties.type}</c:if> <c:if test="${not empty properties.class}">c-column-control--${properties.class}</c:if> ">

            <div id="iea-component-layout-container-${data.randomNumber}">

                <c:if test="${requestScope['com.day.cq.wcm.api.WCMMode']=='EDIT'}">
                    <cq:includeClientLib categories="iea.custom-xtype" />
                </c:if>

<c:set var="prefix" value ="clientlib.iea" />
<c:set var="componentName" value="${component.name}" />

               <!--   <c:set var="prefix" value="clientlib.iea" />-->
                <c:if test="${not empty runMode}">
                    <c:set var="prefix" value="${prefix}.${runMode}" />
                </c:if>
                   
                <c:if test="<%= ClientLibUtils.clientLibExists((String) request.getAttribute("runMode"),(String) pageContext.getAttribute("componentName"),(String) request.getAttribute("clientLibName"))%>">
        <c:set var="prefix" value ="clientlib.iea.${clientLibName}.${runMode}" />
</c:if>
                <c:set var="componentLib" value="${prefix}.${component.name}" />
                <cq:includeClientLib categories="${componentLib}" />

                <c:set var="layout" value="${properties.layout}" />
                <c:set var="classIds" value="${fn:split(layout, '|')}" />
                <c:set var="cols" value="${fn:length(classIds)}" />
                <div class="section clearfix <c:if test="${not empty properties.sectionName}">
                    ${properties.sectionName}</c:if>" >
                    <div id="container_${data.randomNumber}" class="column-container row">

                        <c:forEach var="col" begin="1" end="${cols}" varStatus="i">
                            <c:set var="class" value="col-md-${classIds[i.index-1]}" />
                            <div class="${class}">
                                <c:set var="parPath" value="container_par_${col}" />
                                <cq:include path="${parPath}" resourceType="foundation/components/parsys" />
                            </div>
                        </c:forEach>

                	</div>
               </div>
            </div>
</div>
