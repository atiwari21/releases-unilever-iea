<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="org.apache.commons.collections.MapUtils"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="com.unilever.platform.foundation.components.helper.ComponentUtil"%>
<%@page import="org.apache.sling.api.resource.ValueMap"%>
<%@ include file="/apps/iea/commons/global.jsp" %>
<%@page import="org.json.JSONObject"%>
<%@page import="org.apache.sling.api.SlingHttpServletRequest"%>
<%@page import="org.json.JSONArray"%>
<%@page import="javax.jcr.Node"%>
<%@page import="org.apache.sling.api.resource.Resource"%>
<%

    //JSONArray arr=new JSONArray();
String pathArr=slingRequest.getParameter("path");
Resource questionRes=resourceResolver.getResource(pathArr);
List<Map<String,String>> answersList = ComponentUtil.getNestedMultiFieldProperties(questionRes, "responseOptionJson");
String[] arr=new String[answersList.size()];
JSONArray jsonArr=new JSONArray();
String[] responseIdArray = ComponentUtil.getPropertyValueArray(questionRes.getValueMap(), "responseId");
int responseIdCount = 0;
for(Map<String,String> answerObj : answersList){
    String answerElement=MapUtils.getString(answerObj, "responseText", "");
    String responseId = MapUtils.getString(answerObj, "responseId", "");
    if (answerObj.get("responseId") == null && responseIdArray[responseIdCount] != null) {
		responseId = responseIdArray[responseIdCount];
	}
    JSONObject printObj=new JSONObject();
	printObj.put("text",answerElement);
    printObj.put("value",responseId);
    jsonArr.put(printObj);
}
out.print(jsonArr);

%>
