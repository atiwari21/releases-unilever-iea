<%@ include file="/apps/iea/commons/global.jsp" %>
<%@page session="false" import="com.day.cq.wcm.api.WCMMode" %>
<c:set var="dataExclude"><%=properties.get("dataExclude","false")%></c:set>
<c:set var="hiddenClass"><%=properties.get("hiddenClass","")%></c:set>
<c:if test="${dataExclude eq 'true'}">
    <c:set var="excludeParam" value="data-exclude='true'"></c:set>
</c:if>
<input type="hidden" ${excludeParam} name="${properties['name']}"  id="${properties['name']}" value="${properties['value']}" data-type="${properties['inputType']}" hiddenClass="${hiddenClass}" />
<%
if (WCMMode.fromRequest(request) == WCMMode.EDIT || WCMMode.fromRequest(request) == WCMMode.DESIGN) {

        String title =  properties.get("jcr:title", "form hidden");
		String name =  properties.get("name", "name");
        String value =  properties.get("value", "value");

    %>
		<h4>Hidden Field</h4></br>
        Field Name: <%= name %>
		<%
    }
%>