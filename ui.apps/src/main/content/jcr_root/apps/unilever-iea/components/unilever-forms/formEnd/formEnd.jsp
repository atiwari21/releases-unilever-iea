<%--

  Copyright 2013 IEA, SapientNitro

  All Rights Reserved.

  This software is the confidential and proprietary information of

  SapientNirto, ("Confidential Information"). You shall not

  disclose such Confidential Information and shall use it only in

  accordance with the terms of the license agreement you entered into

  with SapientNitro.

  ==============================================================================

--%>
<%@ include file="/apps/iea/commons/global.jsp" %>
<cq:setContentBundle />

  <div class="form-group u-center">
          <div class="col-sm-12">
              <button id="3432-submit" type="submit" name="32432-submit" class="o-btn o-btn--ternary o-disabled c-form-submit-btn js-egifting-submit-btn">${properties['name']}<span id="payAmount"></span></button>
          </div>
      </div>
            


