<%--
  <tabData.jsp>

  Copyright 2013 IEA, SapientNitro
  All Rights Reserved.

  This software is the confidential and proprietary information of
  SapientNitro, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with SapientNitro.

  ==============================================================================
  This file contains the main logic to create the tab component display which
  includes a title on the top and a content parsys for every configured tab. 
  ==============================================================================

--%>

<%@ include file="/libs/foundation/global.jsp" %>
<cq:include script="/apps/iea/commons/init.html" />


     <cq:include path = "par_tab_${data.tabList[0].randomNumber}"
                        resourceType = "foundation/components/parsys" />



