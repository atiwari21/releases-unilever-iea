<%--

  Copyright 2013 IEA, SapientNitro

  All Rights Reserved.

  This software is the confidential and proprietary information of

  SapientNirto, ("Confidential Information"). You shall not

  disclose such Confidential Information and shall use it only in

  accordance with the terms of the license agreement you entered into

  with SapientNitro.

  ==============================================================================

  <process.jsp>

  This JSP is overriden here as its resonsibilities had to be changed for 
  form component specifically. 

      Responsibility:-
      1. Include the specific view as selected by the author.
      2. Call the init data and making the node data available.
      3. Include View specific and component level clientlibs.
      4. Include a parsys for the form elements.
      5. Include the end of form.
      6. Include the hidden variables.

      Inputs:- viewType (Default value is 
               (displayAs_defaultView)). View type may come from different
                   Source variables (dialogs) but would be set by pre-process.jsp.

      Outputs:- Includes the parsys for form elements and the end of form.
  ==============================================================================

--%>
<%@ include file="/apps/unilever-iea/commons/global.jsp" %>
<cq:include script="/apps/iea/commons/init.html" />
<%@include file="/apps/iea/commons/clientlibs.jsp"%>
<%@ page import="com.sapient.platform.iea.aem.utils.CommonUtils" %>

<c:set var = "serverSideValidation" value = "${properties.serverSideValidation}" scope = "request" />
<cq:setContentBundle />
<c:choose>
      <c:when test="${not empty properties.clientSideComponentName}">
      	<c:set var="clientSideComponentName" value="${properties.clientSideComponentName}" scope="request" />
      </c:when>

      <c:otherwise>
    	<c:set var="clientSideComponentName" value="${component.name}" scope="request" />
      </c:otherwise>
</c:choose>
<c:set var="isAjax" value="${properties.isAjax}" />
<c:set var="redirectURL" value="${properties.redirect}" />
<c:set var="randomNumber" value="${data.randomNumber}" />
<c:if test="<%= CommonUtils.isMultiValued((Object)pageContext.getAttribute("randomNumber"))%>">
    <c:set var="randomNumber" value="${data.randomNumber[0]}" />
</c:if>

<script type="module/config" id="form-${randomNumber}-config"> 
	<ieaFoundation:jsonConverter data="${data}" errorReporting="false"/>
</script>
<div class="container  ${clientSideComponentName}" data-config="form-${randomNumber}-config" data-role="${clientSideComponentName}" data-server = "true">

    <form name="egiftingForm" id = "egiftingForm" method="POST"  action="<%=resourceResolver.map(currentPage.getPath())%>.dcstransactionpost.html"  data-ajax="${properties.isAjax}" data-post-url="<%=resourceResolver.map(currentPage.getPath())%>.dcstransactionpost.html" class="form form-horizontal form-validate" role="form" data-fail="${properties.failureDescription}"
		<c:if test="${isAjax}">
 			onsubmit="return false;" 
		</c:if>
    	<c:if test="${not empty redirectURL}">
			data-redirect="${properties.redirect}" 
		</c:if>
	

			<input type="hidden" name="hidden_redirect" value="${data.form.redirect}" />
			<input type="hidden" name="hidden_formid" value="${data.form.formid}" />
			<input type="hidden" name="hidden_successDescription" value="${data.form.successDescription}" />
			<input type="hidden" name="hidden_action" value="${data.form.action}" />
			<input type="hidden" name="hidden_isAjax" value="${data.form.isAjax}" />
			<input type="hidden" name="hidden_actionType" value="${data.form.actionType}" />
			<input type="hidden" name="hidden_currentPath" value="${data.path}" />
			<input type="hidden" name="brand_market" value="<%=brandMarket.getBrand()+"_"+brandMarket.getMarket()%>"/>
			<cq:include path="form_par" resourceType="foundation/components/parsys" />
			<cq:include path="formend" resourceType="unilever-iea/components/unilever-forms/formEnd" />
			<div id="renderJson"></div>
	</form>
    <form name="redirectForm" id="redirectForm" method="GET" action="">
        <input type="hidden" name="errorMsg"  id="errorMsg"/>
    </form>


</div>

