<%--
  <process.jsp>

  Copyright 2013 IEA, SapientNitro
  All Rights Reserved.

  This software is the confidential and proprietary information of
  SapientNitro, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with SapientNitro.

  ==============================================================================
  This file contains the main logic to create the tab component display which
  includes a title on the top and a content parsys for every configured tab. 
  ==============================================================================

--%>

<%@ include file="/libs/foundation/global.jsp" %>
<cq:include script="/apps/iea/commons/init.html" />

<cq:setContentBundle />
<div
	class="tabbed-content<c:if test="${not empty data.tabbedContent.style.type}"> ${data.tabbedContent.style.type}</c:if><c:if test="${not empty data.tabbedContent.style.class}"> ${data.tabbedContent.style.class}</c:if>"
	data-componentname="${data.tabbedContent.componentName}"
	data-role="tabbed-content"
	data-component-variants="${data.tabbedContent.componentVariation}"
    data-component-experience-variant="${data.tabbedContent.componentExperienceVariant}"
	data-component-positions="${data.tabbedContent.componentPosition}">

	<c:set var="defaultIndex" value="${data.tabbedContent.defaultIndex}"/>
    <ul class="tabbed-content-tabs" role="tablist" data-default-index="${data.tabbedContent.defaultIndex}">
        <c:forEach items="${data.tabbedContent.tabList}" var="tabbedList" varStatus="status">
            <li class="tabbed-content__tab <c:if test="${status.index+1 eq data.tabbedContent.defaultIndex}"> tabbed-content__tab--is-active </c:if>">
                <a aria-controls="tabbed-content-panel__body-${tabbedList.randomNumber}"
                   class="tabbed-content__link"
                   role="tab"
                   data-toggle="tab"
                   title="${tabbedList.tabTitle}">
                    ${tabbedList.tabTitle}
                </a>
            </li>
        </c:forEach>
    </ul>
    <div class="tabbed-content-panel">
        <c:forEach items="${data.tabbedContent.tabList}" var="tabbedList" varStatus="status">
            <div id="tabbed-content-panel__body-${tabbedList.randomNumber}"
                 class="tabbed-content-panel__body <c:if test="${status.index+1 eq data.tabbedContent.defaultIndex}"> tabbed-content-panel__body--is-active </c:if><c:if test="${data.tabbedContent.style.type}">accordion-v2-panel--${data.tabbedContent.style.type}</c:if><c:if test="${data.tabbedContent.style.class}">${data.tabbedContent.style.class}</c:if>"
                 role="tabpanel">
                <cq:include path="par_tab_${tabbedList.randomNumber}"
				resourceType="foundation/components/parsys" />
            </div>
        </c:forEach>
    </div>
	<c:if test="${requestScope['com.day.cq.wcm.api.WCMMode']=='EDIT'}">
		<cq:include path="end"
			resourceType="/apps/unilever-iea/components/tabbedContent/endTab" />
	</c:if>
  </div>
