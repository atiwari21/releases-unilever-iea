<%--

  <contentType.json.jsp>

  Copyright 2013 IEA, SapientNitro
  All Rights Reserved.

  This software is the confidential and proprietary information of
  SapientNitro, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with SapientNitro.

  ==============================================================================

  This will return the json for content type dropdown.

  ==============================================================================

--%>
<%@include file="/apps/iea/commons/global.jsp" %>

<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="org.apache.commons.collections.MapUtils"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="com.unilever.platform.foundation.components.helper.ComponentUtil"%>
<%@page import="org.apache.sling.api.resource.ValueMap"%>

<%@page import="org.json.JSONObject"%>
<%@page import="org.apache.sling.api.SlingHttpServletRequest"%>
<%@page import="org.json.JSONArray"%>
<%@page import="javax.jcr.Node"%>
<%@page import="org.apache.sling.api.resource.Resource"%>
<%@page import="org.apache.sling.api.resource.ResourceResolver"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="java.util.Locale"%>
<%@page import="com.day.cq.i18n.I18n"%>
<%
 final Locale pageLocale = currentPage.getLanguage(true);
    final ResourceBundle resourceBundle = slingRequest.getResourceBundle(pageLocale);
    I18n i18n = new I18n(resourceBundle);
String optionsPath = slingRequest.getParameter("optionsPath");

if (optionsPath == null || "".equals(optionsPath)) {
	optionsPath = "/apps/unilever-iea/commons/fields_touch/contentType/items";
}
Resource itemsRes=resourceResolver.getResource(optionsPath);
Node rootNode = itemsRes.adaptTo(Node.class);
NodeIterator contentTypeOptionNodes = rootNode.getNodes();

JSONArray jsonArr=new JSONArray();

while(contentTypeOptionNodes.hasNext()){
            Node optionNode = contentTypeOptionNodes.nextNode();
			String text = optionNode.getProperty("text").getString();
    		String value= optionNode.getProperty("value").getString();
			text = i18n.get(text);
    		JSONObject printObj=new JSONObject();
    if(!("").equals(value)){
	printObj.put("text",text);
    printObj.put("value",value);
    jsonArr.put(printObj);
    }
        }
out.print(jsonArr);

%>