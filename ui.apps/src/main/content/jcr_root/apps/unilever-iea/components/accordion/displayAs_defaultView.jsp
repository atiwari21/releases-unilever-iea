<%--
  <process.jsp>

  Copyright 2013 IEA, SapientNitro
  All Rights Reserved.

  This software is the confidential and proprietary information of
  SapientNitro, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with SapientNitro.

  ==============================================================================
  This file contains the main logic to create the tab component display which
  includes a title on the top and a content parsys for every configured tab. 
  ==============================================================================

--%>

<%@ include file="/libs/foundation/global.jsp" %>
<cq:include script="/apps/iea/commons/init.html" />

<c:set var="styleType" value=" ${data.accordion.style.type}"/>
<c:set var="styleClass" value=" ${data.accordion.style.class}"/>

<cq:setContentBundle />
<div data-role="accordion-v2">
    <div class="accordion-v2 ${styleType} ${styleClass} <c:if test="${data.accordion.multipleOpenItems}"> js-accordion-v2--is-multi-expand </c:if> "
     aria-label="accordion"
aria-multiselectable="<c:choose><c:when test="${data.accordion.multipleOpenItems}">true</c:when><c:otherwise>false</c:otherwise></c:choose>"
     role="tablist"
     data-componentname="${data.accordion.componentName}"
     data-component-variants="${data.accordion.componentVariation}"
	 data-component-experience-variant="${data.accordion.componentExperienceVariant}"
     data-component-positions="${data.accordion.componentPosition}">

    <c:forEach items="${data.accordion.panelList}" var="panel">

        <div class="accordion-v2-panel ${panel.cssStylePanel}">

            <div id="accordion-v2-panel-heading-${panel.randomNumber}"
            class="accordion-v2-panel-heading <c:choose><c:when test="${panel.expandedByDefault != 'true'}"> accordion-v2-panel-heading--is-collapsed </c:when><c:otherwise></c:otherwise></c:choose>"
            aria-expanded="<c:choose><c:when test="${panel.expandedByDefault != 'true'}">true</c:when><c:otherwise>false</c:otherwise></c:choose>"
                 aria-controls="accordion-v2-panel__body-${panel.randomNumber}"
                 role="tab"
                 data-component-panel-heading="${panel.panelHeading}">
                <h2 class="accordion-v2-panel-heading__title">
                    <a class="accordion-v2-panel-heading__link" title="${panel.panelHeading}">
                        ${panel.panelHeading}
                    </a>
                    <span class="accordion-v2-panel-heading__icon-expand icon-arrow-down"></span>
					<span class="accordion-v2-panel-heading__icon-collapse icon-arrow-up"></span>
                </h2>
            </div>

            <div id="accordion-v2-panel__body-${panel.randomNumber}"
            class="accordion-v2-panel__body <c:choose><c:when test="${panel.expandedByDefault != 'true'}"> accordion-v2-panel__body--is-collapsed </c:when><c:otherwise></c:otherwise></c:choose>"
                 aria-labelledby="accordion-v2-panel-heading-${panel.randomNumber}"
                 role="tabpanel">
                <cq:include path="par_tab_${panel.randomNumber}"
				resourceType="foundation/components/parsys" />
            </div>
        </div>
    </c:forEach>
	<c:if test="${requestScope['com.day.cq.wcm.api.WCMMode']=='EDIT'}">
		<cq:include path="end"
		resourceType="/apps/unilever-iea/components/accordion/endTab" />
	</c:if>

</div>
</div>