<%--

  Copyright 2013 IEA, SapientNitro

  All Rights Reserved.

  This software is the confidential and proprietary information of

  SapientNirto, ("Confidential Information"). You shall not

  disclose such Confidential Information and shall use it only in

  accordance with the terms of the license agreement you entered into

  with SapientNitro.

  ==============================================================================

  <process.jsp>

  This JSP is the default implementation of the process.jsp which will be 
  overriden by each of the child components, if required. 

      Responsibility:-
      1. Include the specific view as selected by the author.
      2. Call the init data and making the node data available.
                  3. Include View specific and component level clientlibs.

      Usage:- 
                  1. Do not override this file. Use pre-process instead.

      Inputs:- componentTitle, viewType (Default value is 
               (displayAs_defaultView)). View type may come from different
                   Source variables (dialogs) but would be set by pre-process.jsp.

      Outputs:- Includes the respective view.
  ==============================================================================

--%>
<%@ include file="/apps/unilever-iea/commons/jsp/global.jsp" %>
<cq:include script="/apps/unilever-iea/commons/jsp/init.html" />
<%@page import="com.sapient.platform.iea.aem.utils.ClientLibUtils" %>
<cq:setContentBundle />
<cq:include script="preProcess.jsp" />
<c:set var="prefix" value ="clientlib.iea.${runMode}" /> 
<c:set var="componentName" value="${component.name}" />

<c:if test="<%= ClientLibUtils.clientLibExists((String) request.getAttribute("runMode"),(String) pageContext.getAttribute("componentName"),(String) request.getAttribute("clientLibName"))%>">
        <c:set var="prefix" value ="clientlib.iea.${clientLibName}.${runMode}" />
</c:if>

<c:set var="componentLib" value="${prefix}.${component.name}" />

<c:choose>
    <c:when test="${empty viewType}">
                                <c:set var="view" value="defaultView" scope = "request" />
    </c:when>
    <c:otherwise>
                                <c:set var="view" value="${viewType}" scope = "request" />
                                <c:set var="componentViewLib" value="${componentLib}.${viewType}" />
    </c:otherwise>
</c:choose>

<c:if test="${requestScope['com.day.cq.wcm.api.WCMMode']!='EDIT' && 
		requestScope['com.day.cq.wcm.api.WCMMode']!='DESIGN'}">
            <cq:includeClientLib categories="${componentLib}" />
</c:if>

<c:if test="${not empty componentViewLib}">
                <cq:includeClientLib categories="${componentViewLib}" />
</c:if>

<c:set var="selectors" value="${slingRequest.requestPathInfo.selectors}" />

<c:if test="${not empty selectors}">
    <c:set var="isbot" value="${selectors[0]}" />
</c:if>

<c:choose>
    <c:when test="${isbot eq 'bot'}">
        <cq:include script="displayAs_${view}.jsp" />
    </c:when>
    <c:otherwise>
            <c:choose>
                <c:when test="${properties.clientSideTemplate}">
                                <cq:include script="clientSide.jsp" /> 
                </c:when>
                <c:otherwise>
                     <cq:include script="displayAs_${view}.jsp" />
                </c:otherwise>
    </c:choose>
    </c:otherwise>
</c:choose>
<c:set var="viewType" value="" scope="request"/>
