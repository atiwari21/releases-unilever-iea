<%--

  <constraintType.json.jsp>

  Copyright 2013 IEA, SapientNitro
  All Rights Reserved.

  This software is the confidential and proprietary information of
  SapientNitro, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with SapientNitro.

  ==============================================================================

  This will return the json for constraint type dropdown.

  ==============================================================================

--%>
<%@ include file="/libs/foundation/global.jsp" %>

[

{
"text":"dd/mm/yyyy",
"value":"dd/mm/yyyy_/%5E%28%3F%3A0%5B1-9%5D%7C%5B12%5Dd%7C3%5B01%5D%29%5B/%5D%28%3F%3A0%5B1-9%5D%7C1%5B012%5D%29%5B/%5D%5B0-9%5D%7B4%7D%24"
 },
{
"text":"mm/dd/yyyy",
"value":"mm/dd/yyyy_/%5E%28%3F%3A0%5B1-9%5D%7C1%5B012%5D%29%5B/%5D%28%3F%3A0%5B1-9%5D%7C%5B12%5Dd%7C3%5B01%5D%29%5B/%5D%5B0-9%5D%7B4%7D%24"
},
{
"text":"yyyy-mm-dd'T'HH:mm:ss",
"value":"yyyy-mm-dd_/%5E(19%7C20)%5B0-9%5D%5B0-9%5D-(0%5B0-9%5D%7C1%5B0-2%5D)-(0%5B1-9%5D%7C(%5B12%5D%5B0-9%5D%7C3%5B01%5D)T(%5B01%5D%5B0-9%5D%7C2%5B0-3%5D)%3A%5B0-5%5D%5B0-9%5D%3A%5B0-5%5D%5B0-9%5D%24"
}
]