<%--
/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
 --%>

<%@ include file="/apps/iea/commons/global.jsp"%>
<cq:include script="/apps/iea/commons/init.html" />
<%@page import="com.sapient.platform.iea.aem.utils.ClientLibUtils"%>
<%@page import="com.sapient.platform.iea.aem.core.utils.SlingUtils"%>

<cq:setContentBundle />

<c:set var="clientSideComponentName" value="${component.name}"
	scope="request" />
<cq:include script="preProcess.jsp" />
<%@include file="/apps/iea/commons/clientlibs.jsp"%>

<c:forEach var="selector" items="${slingRequest.requestPathInfo.selectors}">
	<c:set var="isbot" value="${selector}" scope="request" />
</c:forEach>

		<c:choose>
			<c:when test="${isbot eq 'bot'}">
				<div class="clearfix component ${clientSideComponentName}" data-config="${clientSideComponentName}-${data.randomNumber}-config" data-server="true" data-role="${clientSideComponentName}">
				<ieaFoundation:fetchTemplatePath view="${view}" />
				<cq:include script="${templateView}" />
                 <script type="module/config" id="content-results-grid-${data.randomNumber}-config">

               {
                    "data": {
                        "contentResultsGrid": {
                            "configurations": {
                                "jsonUrl": "${currentNode.path}.data.all.json"

                            }
                        }
                    }
				}
				</script>
                </div>
			</c:when>
        	<c:otherwise>
				<c:choose>
					<c:when test="${properties.clientSideRendering}">
                        <c:set var="jsonPath" value="${currentNode.path}.data.all.json" scope="request" />
						<div class="clearfix component ${clientSideComponentName}" data-role="${clientSideComponentName}"
							data-path="<%= SlingUtils.mapURL((String) request.getAttribute("jsonPath"), resourceResolver)%>"
							data-server="false">
                        </div>
					</c:when>
					<c:otherwise>
                        <div class="clearfix component ${clientSideComponentName}" data-config="${clientSideComponentName}-${data.randomNumber}-config" data-server="true" data-role="${clientSideComponentName}">
                        <ieaFoundation:fetchTemplatePath view="${view}" />
                        <cq:include script="${templateView}" />
                    	 <script type="module/config" id="content-results-grid-${data.randomNumber}-config">

                           {
                                "data": {
                                    "contentResultsGrid": {
                                        "configurations": {
                                            "jsonUrl": "${currentNode.path}.data.all.json"

                                        }
                                    }
                                }
                            }
                         </script>
                         </div>
					</c:otherwise>
				</c:choose>
			</c:otherwise>
		</c:choose>
<c:set var="isbot" value="" scope="request" />
<c:set var="viewType" value="" scope="request" />
