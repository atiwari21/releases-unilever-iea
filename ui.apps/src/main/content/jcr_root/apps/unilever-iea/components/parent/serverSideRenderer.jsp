<%@ include file="/apps/unilever-iea/commons/global.jsp" %>
    <cq:include script="/apps/iea/commons/init.html" />

<%
    String resourceType = properties.get("sling:resourceType", "");
    %>
<c:set var="resourceType" value="<%=resourceType %>" scope="request" />

<c:if test="${resourceType ne 'wcm/msm/components/ghost'}" > 
  <c:if test="${(empty properties.renderConfig || properties.renderConfig eq true)}" > 

    <div class="clearfix component ${clientSideComponentName}" data-config="${clientSideComponentName}-${data.randomNumber}-config" data-server="true" data-role="${clientSideComponentName}">
      </c:if>
      <c:choose>

            <c:when test="${isAdaptive == 'true'}">
                        <ieaFoundation:fetchTemplatePath view="${view}" />
                        <c:set var="componentViewType" value="displayAs_${view}" />
                       <c:set var="componentViewType" value="${fn:replace(componentViewType, 
                                             ' ', '_')}" />
				<c:set var="featureViewType" value="displayAs_defaultView" />
                <c:if test="${properties['featurephoneViewType'] != null}" > 
                    <c:set var="featureViewType" value="displayAs_${properties['featurephoneViewType']}" />
                </c:if>
                <c:set var="featurephoneTemplateView" value="<%= AdaptiveUtil.getAdaptiveTemplate(slingRequest)%>" scope="request"/>
                        <c:set var="featurephoneViewTemplate" value="${fn:replace(featurephoneTemplateView, 
                                             componentViewType, featureViewType)}" /> 
                        <c:set var="templateView" value="${featurephoneViewTemplate}" scope="request"/>
                <cq:include script="${featurephoneViewTemplate}" />
            </c:when>
                    <c:otherwise>
                        <%@include file="/apps/unilever-iea/commons/includeServerRenderingScript.jsp"%>
                        <ieaFoundation:fetchTemplatePath view="${view}" />
                        <cq:include script="${templateView}" />
                    </c:otherwise>
                </c:choose> 

       <c:if test="${(empty properties.renderConfig || properties.renderConfig eq true)}" > 
    </div>
</c:if>
    </c:if>