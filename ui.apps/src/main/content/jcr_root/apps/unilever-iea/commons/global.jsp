<%@ page trimDirectiveWhitespaces="true"%>
<%@include file="/apps/iea/commons/global.jsp" %>
<%@page import="com.unilever.platform.aem.foundation.utils.AdaptiveUtil" %>
<c:set var="isAdaptive" value="<%= AdaptiveUtil.isAdaptive(slingRequest)%>" scope="request"/> 
<c:set var="adaptiveName" value="<%= AdaptiveUtil.getAdaptiveName(slingRequest)%>" scope="request"/>
<c:set var="adaptiveClientLibName" value="<%= AdaptiveUtil.getAdaptiveTenantName(slingRequest)%>" scope="request"/>
<c:set var="selectorURL" value="<%= slingRequest.getRequestPathInfo().getSelectorString()%>"/>
<%@ page import="com.unilever.platform.aem.foundation.core.dto.BrandMarketBean" %>
<%
BrandMarketBean brandMarket =new BrandMarketBean(currentPage);
%>
<%@taglib prefix="globalConfig" uri="http://unilever.com/functions/globalconfigutils"%>
<%@taglib prefix="releasedetails" uri="http://unilever.com/functions/releasedetails"%>