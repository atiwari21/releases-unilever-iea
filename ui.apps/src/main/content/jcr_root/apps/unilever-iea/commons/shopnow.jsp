<%@ page trimDirectiveWhitespaces="true"%>
<%@include file="/apps/iea/commons/global.jsp" %>

<%

com.unilever.platform.aem.foundation.configuration.ConfigurationService configurationService = sling.getService(com.unilever.platform.aem.foundation.configuration.ConfigurationService.class);
String serviceProvider = configurationService.getConfigValue(currentPage, "shopNow", "serviceProviderName");
serviceProvider = serviceProvider !=null ? serviceProvider :"";
java.util.Map<String, String> serviceProviderConfig = configurationService.getCategoryConfiguration(currentPage, serviceProvider);
java.util.Map<String, String> shopNowConfig = configurationService.getCategoryConfiguration(currentPage, "shopNow");

String serviceScript = "/apps/unilever-iea/commons/" + serviceProvider + ".jsp";

%>
<c:set var="serviceProviderConfig" value="<%=serviceProviderConfig%>" scope="request" />
<c:set var="shopNowConfig" value="<%=shopNowConfig%>" scope="request" />

<c:if test="${serviceProviderConfig.enabled=='true' && serviceProviderConfig.serviceURL!=null}">
    <c:set var="serviceURL" value="${serviceProviderConfig.serviceURL}" scope="request" />
    <c:set var="applicationID" value="${serviceProviderConfig.applicationID}" scope="request" />
                <cq:include script="<%=serviceScript%>"/>
</c:if>
