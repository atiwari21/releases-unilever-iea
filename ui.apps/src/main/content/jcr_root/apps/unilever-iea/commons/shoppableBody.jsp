<%@ page trimDirectiveWhitespaces="true"%>
<%@include file="/apps/iea/commons/global.jsp" %>
	<c:if test="${serviceProviderConfig!=null && serviceProviderConfig.enabled=='true'  && serviceProviderConfig.serviceURL!=null}">
		<script>if (!window.jQuery) { document.write('<script src="/etc/ui/${clientLibName}/clientlibs/core/require/js/libs/vendor/jquery/dist/jquery.js"><\/script>'); }</script>    
        <c:if test="${ requestScope['com.day.cq.wcm.api.WCMMode']=='EDIT' || requestScope['com.day.cq.wcm.api.WCMMode']=='DESIGN' ||requestScope['com.day.cq.wcm.api.WCMMode']=='PREVIEW'}">
		<script id="shoppable_unlv_bundle_v0-1" options='"token":"${serviceProviderConfig.apiTokenAuthor}", "developerMode":"${serviceProviderConfig.developerMode}"' src="${serviceProviderConfig.serviceURLBundle}/assets/shoppable_unlv_bundle_v0-1.js?<%=currentPage.getLanguage(false)%>"></script>
		</c:if>
		<c:if test="${!( requestScope['com.day.cq.wcm.api.WCMMode']=='EDIT' || requestScope['com.day.cq.wcm.api.WCMMode']=='DESIGN' ||requestScope['com.day.cq.wcm.api.WCMMode']=='PREVIEW')}">
            <script id="shoppable_unlv_bundle_v0-1" options='"token":"${serviceProviderConfig.apiToken}", "developerMode":"${serviceProviderConfig.developerMode}"' src="${serviceProviderConfig.serviceURLBundle}/assets/shoppable_unlv_bundle_v0-1.js?<%=currentPage.getLanguage(false)%>"></script>
		</c:if>
		<script src="/etc/ui/${clientLibName}/clientlibs/core/core/config/shoppable.js" type="text/javascript"></script>
	 </c:if>