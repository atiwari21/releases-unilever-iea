<%--

  ==============================================================================

  Global IEA script.

  @see /libs/foundation/global.jsp for full description.

  All the custom tag libs for engaged now are defined in this file.


  ==============================================================================

--%>
    <%@ page trimDirectiveWhitespaces="true"%>
<%@include file="/libs/foundation/global.jsp" %>
<%@taglib prefix="ieaFoundation" uri="http://iea.com/platform/aem/foundation/core/tags/1.0" %>
<%@taglib prefix="ieaComponents" uri="http://iea.com/platform/aem/components/core/tags/1.0"%>