<%@ page trimDirectiveWhitespaces="true"%>
<%@include file="/apps/iea/commons/global.jsp" %>

<c:if test="${isAdaptive == 'false'}">
<script>
    (function() {var fjs = document.createElement('script');
     fjs.type = 'text/javascript'; fjs.async = true;
     fjs.src =   '${serviceURL}';  // Service URL Value from the configuration
     var s = document.getElementsByTagName('script')[0];
     s.parentNode.insertBefore(fjs, s);})();
</script>
<script>
 
  window.constantCoAppId =  '${applicationID}';  // Application ID from the configuration.
 
</script>
</c:if>