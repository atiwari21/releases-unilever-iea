<%--
  /*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
--%>
    <%@include file="/apps/iea/commons/global.jsp" %>
<%@page import="java.util.Map"%>
<%@page import="com.unilever.platform.aem.foundation.core.dto.BrandMarketBean"%>
<%@page import="com.adobe.granite.ui.components.ds.SimpleDataSource"%>
<%@page import="org.apache.sling.api.resource.ResourceMetadata"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.adobe.granite.ui.components.ds.ValueMapResource"%>
<%@page import="org.apache.sling.api.resource.ValueMap"%>
<%@page import="com.adobe.cq.commerce.common.ValueMapDecorator"%>
<%@page import="org.apache.sling.api.resource.Resource"%>
<%@page import="com.unilever.platform.aem.foundation.configuration.ConfigurationService"%>
<%@page import="org.apache.sling.api.resource.ResourceResolver"%>
<%@page import="org.apache.sling.api.SlingHttpServletRequest"%>
<%@page import="javax.jcr.Node"%>
<%@page import="com.adobe.granite.ui.components.Config,
    com.adobe.granite.ui.components.ExpressionHelper,
com.adobe.granite.ui.components.ds.AbstractDataSource,
com.adobe.granite.ui.components.ds.DataSource,
com.adobe.granite.ui.components.ds.EmptyDataSource,
com.adobe.granite.ui.components.ComponentHelper,
java.util.Iterator,
java.util.ArrayList,
java.util.List"%>
    
    <%
    
    final ComponentHelper cmp = new ComponentHelper(pageContext);
ExpressionHelper ex = cmp.getExpressionHelper();

Config cfg = new Config(resource.getChild(Config.DATASOURCE));
String channelPath = ex.getString(cfg.get("channel", String.class));
String defaultValue = ex.getString(cfg.get("defaultContentTypeVal", String.class));
String optionsPath = ex.getString(cfg.get("optionsPath", "/apps/unilever-iea/commons/fields_touch/subContentType/items"));
List<Resource> fakeResourceList = new ArrayList<Resource>();
ValueMap vm = null;

request.setAttribute(DataSource.class.getName(), EmptyDataSource.instance()); 
Resource  itemsRes=resourceResolver.getResource(optionsPath);
Node rootNode = itemsRes.adaptTo(Node.class);
NodeIterator contentTypeOptionNodes = rootNode.getNodes();
while(contentTypeOptionNodes.hasNext()){
    vm = new ValueMapDecorator(new HashMap<String, Object>());
    Node optionNode = contentTypeOptionNodes.nextNode();
    String text = optionNode.getProperty("text").getString();
    String value= optionNode.getProperty("value").getString();

    if(defaultValue!=null && defaultValue.equals("select")){
        vm.put("value", value);
        vm.put("text", text);
        fakeResourceList.add(new ValueMapResource(resourceResolver, new ResourceMetadata(), "nt:unstructured", vm));
    }
    else if(!("").equals(value)){
            vm.put("value", value);
            vm.put("text", text);
            fakeResourceList.add(new ValueMapResource(resourceResolver, new ResourceMetadata(), "nt:unstructured", vm));

    }    
}

DataSource ds = new SimpleDataSource(fakeResourceList.iterator());
request.setAttribute(DataSource.class.getName(), ds);
%>

