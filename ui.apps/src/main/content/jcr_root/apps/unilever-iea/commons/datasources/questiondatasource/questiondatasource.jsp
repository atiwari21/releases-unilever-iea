<%--
  /*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
--%>
<%@page import="com.unilever.platform.foundation.components.helper.ComponentUtil"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.apache.commons.collections.MapUtils"%>
<%@page import="org.apache.sling.api.resource.ResourceResolver"%>
<%@page import="java.util.Map"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.apache.sling.api.SlingHttpServletRequest"%>
<%@page import="org.json.JSONArray"%>
<%@page import="javax.jcr.Node"%>
<%@page import="org.apache.sling.api.resource.Resource"%>
<%@page import="com.unilever.platform.aem.foundation.core.dto.BrandMarketBean"%>
<%@page import="com.adobe.granite.ui.components.ds.SimpleDataSource"%>
<%@page import="org.apache.sling.api.resource.ResourceMetadata"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.adobe.granite.ui.components.ds.ValueMapResource"%>
<%@page import="org.apache.sling.api.resource.ValueMap"%>
<%@page import="com.adobe.cq.commerce.common.ValueMapDecorator"%>
<%@page import="org.apache.sling.api.resource.Resource"%>
<%@page import="com.unilever.platform.aem.foundation.configuration.ConfigurationService"%>
<%@page session="false"
            import="com.adobe.granite.ui.components.Config,
                  com.adobe.granite.ui.components.ExpressionHelper,
                  com.adobe.granite.ui.components.ds.AbstractDataSource,
                  com.adobe.granite.ui.components.ds.DataSource,
                  com.adobe.granite.ui.components.ds.EmptyDataSource,
				  com.adobe.granite.ui.components.ComponentHelper,
                  org.apache.sling.api.resource.Resource,
                  java.util.Iterator,
                  java.util.ArrayList,
                  java.util.List"%><%
%>
<%@include file="/libs/foundation/global.jsp"%>
<%
	final ComponentHelper cmp = new ComponentHelper(pageContext);
    ExpressionHelper ex = cmp.getExpressionHelper();
	
    Config cfg = new Config(resource.getChild(Config.DATASOURCE));
    String channelPath = ex.getString(cfg.get("channel", String.class));

	
    request.setAttribute(DataSource.class.getName(), EmptyDataSource.instance()); 
    
    
    //ResourceResolver resourceResolver=null;
    
    //Create an ArrayList to hold data
    List<Resource> fakeResourceList = new ArrayList<Resource>();

    ValueMap vm = null;

    Resource  currentResource=resourceResolver.getResource(channelPath);
    
    
    String[] sectionsArr= ComponentUtil.getPropertyValueArray(currentResource.getValueMap(), "questionsConfiguration");
    
    
    for(String section:sectionsArr){
        vm = new ValueMapDecorator(new HashMap<String, Object>());  
        JSONObject obj=new JSONObject(section);
        String questionPath=(String)obj.get("diagnosticToolPath");

        Resource questionRes=resourceResolver.getResource(questionPath);

        ValueMap vm1=questionRes.getValueMap();

        String headingText=(String)vm1.get("headingText");
        String questionId=(String)vm1.get("questionId");

        vm.put("value", questionId);
        vm.put("text", headingText);
        vm.put("path", questionPath);
        fakeResourceList.add(new ValueMapResource(resourceResolver, new ResourceMetadata(), "nt:unstructured", vm));
    }

    
    //Create a DataSource that is used to populate the drop-down control
    DataSource ds = new SimpleDataSource(fakeResourceList.iterator());
    request.setAttribute(DataSource.class.getName(), ds);
    
%>

