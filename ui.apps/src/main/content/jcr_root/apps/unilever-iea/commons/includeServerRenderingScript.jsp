<%--
  <validation.jsp>
    
  Copyright 2013 IEA, SapientNitro
  All Rights Reserved.

  This software is the confidential and proprietary information of
  SapientNitro, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with SapientNitro.

  ==============================================================================
  This is the default file which is used to initialize the edit mode condition 
  for performing unconfigured component validation on the author side. 

      Usage :-
      Override this file to provide component specific validation conditions.
  ==============================================================================

--%>
<%@ page trimDirectiveWhitespaces="true"%>

<c:set var="clientContext" value="${globalConfig:getCategoryConfig(resource,'clientContext')}"/>

  <c:if test="${(data.clientSideRendering eq false) && (empty properties.renderConfig || properties.renderConfig eq true)}" > 
	<script type="module/config" id="<%=request.getAttribute("clientSideComponentName")%>-${data.randomNumber}-config">

 	<c:choose>
    	<c:when test="${clientContext.enabled=='true'}">
			var config = <ieaFoundation:jsonConverter data="${data}"/>;
		</c:when>

		<c:otherwise>
			<ieaFoundation:jsonConverter data="${data}"/>
        </c:otherwise>
	</c:choose>
	</script>
  </c:if>



