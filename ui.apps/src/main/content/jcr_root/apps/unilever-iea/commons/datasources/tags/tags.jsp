<%--
  ADOBE CONFIDENTIAL

  Copyright 2014 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%><%
%><%@include file="/libs/foundation/global.jsp"%><%
%><%@page session="false"
          import="java.util.Arrays,
		          java.util.List,
		          java.util.Locale,
		          java.util.HashMap,
                  com.day.cq.wcm.api.Page,
		          org.apache.commons.collections.Transformer,
		          org.apache.commons.collections.iterators.IteratorChain,
		          org.apache.commons.collections.iterators.TransformIterator,
		          org.apache.sling.api.resource.ResourceMetadata,
		          org.apache.sling.api.resource.ResourceResolver,
                  org.apache.sling.api.SlingHttpServletRequest,
		          org.apache.sling.api.wrappers.ValueMapDecorator,
		          com.adobe.granite.ui.components.Config,
                  org.apache.commons.lang.StringUtils,
		          com.adobe.granite.ui.components.ds.DataSource,
		          com.adobe.granite.ui.components.ds.SimpleDataSource,
		          com.adobe.granite.ui.components.ds.ValueMapResource,
		          com.day.cq.tagging.Tag,
                  com.day.cq.wcm.api.PageManager,
                  com.unilever.platform.foundation.viewhelper.base.BaseViewHelper,
		          com.day.cq.tagging.TagManager"%><%
/**
    A datasource returning tag key-value pairs that is suitable to be used for select or autocomplete (or compatible) components.

    @datasource
    @name Tags
    @location /apps/unilever-iea/commons/datasources/tags

    @property {String[]} [namespaces] The namespaces of the tags to return. Only the tag which namespace equals to one of this values is returned. If this property is not specified, all tags are returned.

 */
Config dsCfg = new Config(resource.getChild("datasource")); 
List<String> namespaces = Arrays.asList(dsCfg.get("namespaces", new String[0]));

TagManager tm = resourceResolver.adaptTo(TagManager.class);


IteratorChain tags = new IteratorChain();

if (tm != null) {
    for (Tag ns : tm.getNamespaces()) {
        if (namespaces.size() == 0 || namespaces.contains(ns.getName())) {
            tags.addIterator(ns.listAllSubTags());
        }
    }
}



final ResourceResolver resolver = resourceResolver;
final Locale locale =  BaseViewHelper.getLocale(pageManager.getContainingPage(StringUtils.substringAfter(slingRequest.getRequestURI(), "html")));
//System.out.println(slingRequest.getResource().getPath());
@SuppressWarnings("unchecked")
DataSource ds = new SimpleDataSource(new TransformIterator(tags, new Transformer() {
    public Object transform(Object o) {
        Tag tag = (Tag) o;
        
        String tagId = tag.getTagID();

        ValueMap vm = new ValueMapDecorator(new HashMap<String, Object>());
        vm.put("value", tagId);
        vm.put("text",tag.getTitlePath(locale));

        return new ValueMapResource(resolver, new ResourceMetadata(), "nt:unstructured", vm);
    }
}));

request.setAttribute(DataSource.class.getName(), ds);
%>