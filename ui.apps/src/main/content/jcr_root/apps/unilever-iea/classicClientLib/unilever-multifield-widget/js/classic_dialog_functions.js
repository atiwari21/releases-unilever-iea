var DIAGNOSTIC_TOOL_RESULT_SECTION_DIALOG="";
var DIAGNOSTIC_TOOL_DIALOG="";
var QUESTION_LIST_JSONDATA = [];
var ANSWER_LIST_JSONDATA = [];


function formElementViewTypeSelectionChanged(box,value) { 

	var refrence = box.ownerCt.findByType('selection');
	var textfield = box.ownerCt.findByType('textfield');
	var multifield = box.ownerCt.findByType('multifield');
	var dialogfieldset = box.ownerCt.findByType('dialogfieldset');
	var pathfieldset = box.ownerCt.findByType('pathfield');
	var panelValidationSelection = this.findParentByType('tabpanel').getItem(1).findByType('selection') ;
	var panelValidationTextArea = this.findParentByType('tabpanel').getItem(1).findByType('textarea') ;

	for(j=0;j<multifield.length;j++)
	{

		if(multifield[j].name =='./data' || multifield[j].name =='./dataRadio')
		{
			multifield[j].hide();
		}

	}
	for(j=0;j<textfield.length;j++)
	{
		if(textfield[j].name =='./defaultValue')
		{
			textfield[j].hide();
		}
	}
	for(j=0;j<pathfieldset.length;j++)
	{
		if(pathfieldset[j].name =='./optionsLoadPath')
		{
			pathfieldset[j].hide();
		}
		if(pathfieldset[j].name =='./link')
		{
			pathfieldset[j].hide();
		}
	}
	for(i=0;i<refrence.length;i++)
	{
		if(refrence[i].name =='./viewType')
		{  
						switch(box.getRawValue())
			{
			case "Radio":

				for(j=0; j<panelValidationSelection.length;j++) {
					if( panelValidationSelection[j].name =='./constraintRegEx')
					{
						panelValidationSelection[j].hide();
					}
				}
				for(j=0; j<panelValidationTextArea.length;j++) {
					if( panelValidationTextArea[j].name =='./constraintMessage')
					{
						panelValidationTextArea[j].hide();
					}
				}
				for(j=0;j<multifield.length;j++)
				{	
					if( multifield[j].name =='./dataRadio')
					{
						multifield[j].show();
					}
					if(multifield[j].name =='./data')
					{
						multifield[j].hide();
					}
				}
				for(j=0;j<refrence.length;j++)
				{
					refrence[j].show();
				}

				for(j=0;j<textfield.length;j++)
				{
					if(textfield[j].name =='./defaultValue')
					{
						textfield[j].show();
					}
				}
				for(j=0;j<pathfieldset.length;j++)
				{
					if(pathfieldset[j].name =='./optionsLoadPath')
					{
						pathfieldset[j].show();
					}
				}
				for(j=0;j<refrence.length;j++)
				{
					if(refrence[j].name =='./hideTitle')
					{
						refrence[j].show();
					}
					if(refrence[j].name =='./multiSelection')
					{
						refrence[j].hide();
					}
					if(refrence[j].name =='./dateFormat')
					{
						refrence[j].hide();
					}
					if(refrence[j].name =='./checked' || refrence[j].name =='./openInParentWindow')
		            {
			            refrence[j].hide();
		            }
				}
			
			break;
			
		case "link":
			for(j=0;j<pathfieldset.length;j++)
			{
				if(pathfieldset[j].name =='./link')
				{
					pathfieldset[j].show();
				}
				if(refrence[j].name =='./checked')
		        {
			        refrence[j].hide();
		        }
				if(refrence[j].name =='./openInParentWindow')
		        {
			        refrence[j].hide();
		        }
			}
			

		case "Dropdown":

			for(j=0; j<panelValidationSelection.length;j++) {
					if( panelValidationSelection[j].name =='./constraintRegEx')
					{
						panelValidationSelection[j].hide();
					}
				}
				for(j=0; j<panelValidationTextArea.length;j++) {
					if( panelValidationTextArea[j].name =='./constraintMessage')
					{
						panelValidationTextArea[j].hide();
					}
				}
			for(j=0;j<multifield.length;j++)
			{
				if (multifield[j].name =='./validations' || multifield[j].name =='./dataRadio')
				{
					multifield[j].hide();
				}
				if(multifield[j].name =='./data')
				{
					multifield[j].show();
				}

			}
			for(j=0;j<refrence.length;j++)
			{
				refrence[j].show();
			}
			for(j=0;j<textfield.length;j++)
			{
				if(textfield[j].name =='./defaultValue')
				{
					textfield[j].show();
				}
				
			}
			for(j=0;j<pathfieldset.length;j++)
			{
			if(pathfieldset[j].name =='./optionsLoadPath')
				{
					pathfieldset[j].show();
				}
			}
			for(j=0;j<refrence.length;j++)
			{
				if(refrence[j].name =='./hideTitle')
				{
					refrence[j].show();
				}
				if(refrence[j].name =='./multiSelection')
				{
					refrence[j].show();
				}
				if(refrence[j].name =='./dateFormat')
				{
					refrence[j].hide();
				}
				if(refrence[j].name =='./checked' || refrence[j].name =='./openInParentWindow')
		        {
			        refrence[j].hide();
		        }
			}
			
		break;

	case "Calendar":
		for(j=0; j<panelValidationSelection.length;j++) {
					if( panelValidationSelection[j].name =='./constraintRegEx')
					{
						panelValidationSelection[j].hide();
					}
				}
				for(j=0; j<panelValidationTextArea.length;j++) {
					if( panelValidationTextArea[j].name =='./constraintMessage')
					{
						panelValidationTextArea[j].hide();
					}
				}
		for(j=0;j<multifield.length;j++)
		{

			if(multifield[j].name =='./data' || multifield[j].name =='./dataRadio')
			{
				multifield[j].hide();
			}
		}
		for(j=0;j<refrence.length;j++)
		{
			refrence[j].show();
		}

		for(j=0;j<refrence.length;j++)
		{
			if(refrence[j].name =='./hideTitle')
			{
				refrence[j].show();
			}
			if(refrence[j].name =='./multiSelection')
			{
				refrence[j].hide();
			}
			if(refrence[j].name =='./dateFormat')
			{
				refrence[j].show();
			}
			if(refrence[j].name =='./checked' || refrence[j].name =='./openInParentWindow')
		    {
			    refrence[j].hide();
		    }
		}
	
	break;

case "Text Field":

for(j=0; j<panelValidationSelection.length;j++) {
					if( panelValidationSelection[j].name =='./constraintRegEx')
					{
						panelValidationSelection[j].show();
					}
				}
				for(j=0; j<panelValidationTextArea.length;j++) {
					if( panelValidationTextArea[j].name =='./constraintMessage')
					{
						panelValidationTextArea[j].show();
					}
				}
	for(j=0;j<multifield.length;j++)
	{

		if(multifield[j].name =='./data' || multifield[j].name =='./dataRadio')
		{
			multifield[j].hide();
		}

	}
	for(j=0;j<textfield.length;j++)
	{
		if(textfield[j].name =='./defaultValue')
		{
			textfield[j].show();
		}
	}
	for(j=0;j<pathfieldset.length;j++)
	{
		if(pathfieldset[j].name =='./optionsLoadPath')
		{
			pathfieldset[j].hide();
		}
	}
	for(j=0;j<refrence.length;j++)
	{
		if(refrence[j].name =='./hideTitle')
		{
			refrence[j].show();
		}
		if(refrence[j].name =='./multiSelection')
		{
			refrence[j].hide();
		}
		if(refrence[j].name =='./dateFormat')
		{
			refrence[j].hide();
		}
		if(refrence[j].name =='./checked' || refrence[j].name =='./openInParentWindow')
		{
			refrence[j].hide();
		}
	}


break;
case "checkbox":

for(j=0; j<panelValidationSelection.length;j++) {
					if( panelValidationSelection[j].name =='./constraintRegEx')
					{
						panelValidationSelection[j].show();
					}
				}
				for(j=0; j<panelValidationTextArea.length;j++) {
					if( panelValidationTextArea[j].name =='./constraintMessage')
					{
						panelValidationTextArea[j].show();
					}
				}
	for(j=0;j<multifield.length;j++)
	{

		if(multifield[j].name =='./data' || multifield[j].name =='./dataRadio')
		{
			multifield[j].hide();
		}

	}
	for(j=0;j<textfield.length;j++)
	{
		if(textfield[j].name =='./defaultValue')
		{
			textfield[j].show();
		}
	}
	for(j=0;j<pathfieldset.length;j++)
	{
		if(pathfieldset[j].name =='./optionsLoadPath')
		{
			pathfieldset[j].hide();
		}
	}
	for(j=0;j<refrence.length;j++)
	{
		if(refrence[j].name =='./hideTitle')
		{
			refrence[j].show();
		}
		if(refrence[j].name =='./multiSelection')
		{
			refrence[j].hide();
		}
		if(refrence[j].name =='./dateFormat')
		{
			refrence[j].hide();
		}
		if(refrence[j].name =='./checked' || refrence[j].name =='./openInParentWindow')
		{
			refrence[j].show();
		}
		
	}


break;
}   
}				

} //viewType for loop ends
for(k=0;k<refrence.length;k++)
{
	if(refrence[k].name == "./multivalue"){
		if(box.getRawValue() === "Text Field")
		{
			refrence[k].show();
		}
		else{
			refrence[k].hide();
		}
	}	
}

} 
// countrySelectorOnLoad
function countrySelectorOnLoad(box, value) {

	var tabPanel = box.ownerCt.findParentByType('tabpanel');
	var dialog = box.ownerCt.findParentByType('dialog');
	var linkType = dialog.getField("./type");
	var internalLanguage = dialog.getField("./internalLanguage");
	var countryName = dialog.getField("./countryName");
	var countryCode = dialog.getField("./countryCode");
	var externalJson = dialog.getField("./externalJson");
	var currentObject = dialog.getField("./externalJson");

	var selectedValue = box.getRawValue();

	if (selectedValue == 'Internal Type') {

		internalLanguage.show();
		countryName.hide();
		countryCode.hide();
		externalJson.setVisible(false);
		internalLanguage.allowBlank = false;
		countryName.allowBlank = true;
		countryCode.allowBlank = true;
		var dialogFields = CQ.Util.findFormFields(currentObject);
		for (var name in dialogFields) {
			var indexNum = 0;

			for (var i = 0; i < dialogFields[name].length; i++) {
				if (dialogFields[name][i].name != "./languageCode" && dialogFields[name][i].name != "./externalJson" && dialogFields[name][i].name != "./externalJson@Delete") {
					dialogFields[name][i].allowBlank = true;
				}
			}
		}
	} else {

		internalLanguage.hide();
		countryName.show();
		countryCode.show();
		externalJson.setVisible(true);

		internalLanguage.allowBlank = true;

		countryName.allowBlank = false;

		countryCode.allowBlank = false;



		var dialogFields = CQ.Util.findFormFields(currentObject);
		for (var name in dialogFields) {
			var indexNum = 0;

			for (var i = 0; i < dialogFields[name].length; i++) {
				if (dialogFields[name][i].name != "./languageCode" && dialogFields[name][i].name != "./externalJson" && dialogFields[name][i].name != "./externalJson@Delete") {
					dialogFields[name][i].allowBlank = false;
				}
			}
		}


	}
}

function categoryOverview(dialog){
       var count = 0;
    var currentObj = dialog.getField("./navigationLinks");
    var currentObjs = document.getElementsByName("./label");
    var title = document.getElementsByName("./subHeading");
    var dialogFields = CQ.Util.findFormFields(currentObj);
    for (var name in dialogFields) {
        if ("./navigationLinks" == name) {
            for (var i = 0; i < dialogFields[name].length; i++) {
                var compositeField = dialogFields[name][i];
                count = i + 1;
            }
        }
    }
	if (count == 0 || count < 0) {
            CQ.Ext.Msg.show({
                title: 'Confirm', msg: 'Please configure at least one navigation list',
                icon: CQ.Ext.MessageBox.ERROR, buttons: CQ.Ext.Msg.OK
            });
            return false;
    }
	count =0;
    for (var x = 0; x < title.length; x++) // comparison should be "<" not "<="
    {
        var parentElement = $(title[x]).closest('.x-panel-bwrap');
        if(parentElement && parentElement.find('input[name="./label"]').length==0) {
			 count = x + 1;
        }
    }

	if (count != 0) {
        CQ.Ext.Msg.show({title: 'Confirm', msg: 'At least one navigation link must be added to the list',
            icon: CQ.Ext.MessageBox.ERROR, buttons: CQ.Ext.Msg.OK
        });
        return false;
    }

}

function zipFileDownload(dialog){
    var count = 0;
    var currentObj = dialog.getField("./filesForDownload");
    var dialogFields = CQ.Util.findFormFields(currentObj);
    for (var name in dialogFields) {
      if ("./path" == name) {
            for (var i = 0; i < dialogFields[name].length; i++) {
                var compositeField = dialogFields[name][i];
                count = i + 1;
            }
        }
    }
	if (count == 0 || count < 1) {
            CQ.Ext.Msg.show({
                title: 'Confirm', msg: 'Please configure at least one file for zip.',
                icon: CQ.Ext.MessageBox.ERROR, buttons: CQ.Ext.Msg.OK
            });
            return false;
    }
}

function globalNavFieldHandler(box,value)
{
	parRefs = box.ownerCt.findByType('selection');
	pathfield = box.ownerCt.findByType('pathfield');
	textfield = box.ownerCt.findByType('textfield');
	multifieldManual = box.ownerCt.findByType('multifield');
	paragraphreference = box.ownerCt.findByType('paragraphreference');

	for(j=0;j<parRefs.length;j++)
	{	var lastChar = parRefs[j].name[parRefs[j].name.length-1];
		if(parRefs[j].name =='./selectTab'+lastChar)
		{
			switch(box.getRawValue()){
				case "Custom":
					for(var i=0;i<textfield.length;i++){
					if(textfield[i].getName() == "./title"+lastChar){
						textfield[i].hide();

					}
					}
					for(var i=0;i<paragraphreference.length;i++){
					if(paragraphreference[i].getName() == "./slideTypeCustom"+lastChar  ){
						paragraphreference[i].show();
					}
					}
					for(var i=0;i<multifieldManual.length;i++){
					if(multifieldManual[i].getName() == "./navItemLink"+lastChar  ){
						multifieldManual[i].hide();
					}
					}
					for(var i=0;i<pathfield.length;i++){
					if(pathfield[i].getName() == "./parentPage"+lastChar  ){
						pathfield[i].hide();
					}
					}
					break;
				case "Manual":					
					for(var i=0;i<textfield.length;i++){
					if(textfield[i].getName() == "./title"+lastChar  ){
						textfield[i].show();
						
					}
					}
					for(var i=0;i<paragraphreference.length;i++){
					if(paragraphreference[i].getName() == "./slideTypeCustom"+lastChar  ){
						paragraphreference[i].hide();
					}
					}
					for(var i=0;i<multifieldManual.length;i++){
					if(multifieldManual[i].getName() == "./navItemLink"+lastChar  ){
						multifieldManual[i].show();
					}
					}
					for(var i=0;i<pathfield.length;i++){
					if(pathfield[i].getName() == "./parentPage"+lastChar ){
						pathfield[i].hide();
					}
					}
					break;
				case "Auto":					
					for(var i=0;i<textfield.length;i++){
					if(textfield[i].getName() == "./title"+lastChar  ){
						textfield[i].hide();
						
					}
					}
					for(var i=0;i<paragraphreference.length;i++){
					if(paragraphreference[i].getName() == "./slideTypeCustom"+lastChar ){
						paragraphreference[i].hide();
					}
					}
					for(var i=0;i<multifieldManual.length;i++){
					if(multifieldManual[i].getName() == "./navItemLink"+lastChar ){
						multifieldManual[i].hide();
					}
					}
					for(var i=0;i<pathfield.length;i++){
					if(pathfield[i].getName() == "./parentPage"+lastChar ){
						pathfield[i].show();
					}
					}
					break;
			}		
		}
		
	}

}


function globalNavigationClassicOnLoad(box,value)	{
	parRefs = box.ownerCt.findByType('selection');
	if(parRefs[0].name =='./tabCount') {
		var tabPanel = box.findParentByType('tabpanel');
		for(i=1;i<=7;i++) {
			tabPanel.hideTabStripItem(i);
		}			
		if(!value) {
			value=parRefs[0].value;
		}			
		for(i=1;i<=value;i++) {
			tabPanel.unhideTabStripItem(i);
		}		
	}
}

//search Listing
function searchListingOnLoad(box,value)
{

var tabPanel = box.ownerCt.findParentByType('tabpanel');

var dialog = box.ownerCt.findParentByType('dialog');
var hl = dialog.getField('./hl');
var hlFl = dialog.getField('./hl.fl');
var hlSimplePre = dialog.getField('./hl.simple.pre');
var hlSimplePost = dialog.getField('./hl.simple.post');
alert('hl2222');
if(box.getRawValue()=="on" || box.getRawValue()=="true")
	{   
        alert('show');
		hlSimplePre.show();
		hlSimplePost.show();	
	   hlFl.show();
	}
	else
	{  hlSimplePre.hide();
		hlSimplePost.hide();
	    hlFl.hide();
	}
}

//articleListing
function showsAsNavigationSelectionChanged(box,value){
var dialog = box.ownerCt.findParentByType('dialog');
var label = dialog.getField('./label');
if(box.getRawValue() == "true"){
label.allowBlank = false;
}else{
label.allowBlank = true;
}
}
//ArticleListing
function articleListingOnLoad(box, value) {
	var dialog = box.ownerCt.findParentByType('dialog');
    var tabPanel = box.findParentByType('tabpanel');
    if(box.getRawValue() == "Manual")
    {
        tabPanel.hideTabStripItem(1);
        tabPanel.unhideTabStripItem(2);
    }
    else
    {
         tabPanel.hideTabStripItem(2);
        tabPanel.unhideTabStripItem(1);
    }
}
//countryBeforeSubmit
	function countryBeforeSubmit(dialog) {
		var count = 0;
		var currentObj = dialog.getField("./externalJson");
		var type = dialog.getField("./type").getRawValue();
		var dialogFields = CQ.Util.findFormFields(currentObj);
		if(type == 'External Type'){
		for (var name in dialogFields) {
			if ("./languagesLink" == name) {
				for (var i = 0; i < dialogFields[name].length; i++) {
					var compositeField = dialogFields[name][i];
					count = i + 1;
				}
			}
			if (count == 0 || count < 0) {
				CQ.Ext.Msg.show({
					title: 'Confirm',
					msg: 'Please select at least one country path',
					icon: CQ.Ext.MessageBox.ERROR,
					buttons: CQ.Ext.Msg.OK
				});
				return false;
			} else if(count > 5){
				CQ.Ext.Msg.show({
					title: 'Confirm',
					msg: 'You can only select a maximum of 5 country',
					icon: CQ.Ext.MessageBox.ERROR,
					buttons: CQ.Ext.Msg.OK
				});
				return false;
			}
		}
		}
	}
//bodyCopySteps
function bodyCopyStepsBeforeSubmit(dialog) {
    var count = 0;
    var currentObj = dialog.getField("./sections");
    var dialogFields = CQ.Util.findFormFields(currentObj);
    var imageneeded =  dialog.getField("./isImageRequired");
    var image = false;
    var altimage = false;
    for (var name in dialogFields) {
        if ("./step"==name) {
            for (var i = 0; i < dialogFields[name].length; i++) {
                var compositeField = dialogFields[name][i];
                count = i + 1;
            }
                                }


            if("./image"==name){

                 if(imageneeded.getRawValue() == 'true'){

					 for (var i = 0; i < dialogFields[name].length; i++) {
                		var compositeField = dialogFields[name][i];
                        compositeField.allowBlank = false;
                        if(compositeField.getRawValue() == "")
                         {
							image = true;
                         }


           			 }




            }


        }

          if("./altImage"==name){

                 if(imageneeded.getRawValue() == 'true'){

					 for (var i = 0; i < dialogFields[name].length; i++) {
                		var compositeField = dialogFields[name][i];
                        compositeField.allowBlank = false;
                        if(compositeField.getRawValue() == "")
                         {
							altimage = true;
                         }


           			 }




            }


        }
                                if (count == 0 || count < 0) {
                                                CQ.Ext.Msg.show({
                                                                title: 'Confirm',
                                                                msg: 'Please configure at least one body copy list',
                                                                icon: CQ.Ext.MessageBox.ERROR,
                                                                buttons: CQ.Ext.Msg.OK
                                                });
                                                return false;
                                }
        			  if (image == true) {
                                                CQ.Ext.Msg.show({
                                                                title: 'Confirm',
                                                                msg: 'Please enter the image path',
                                                                icon: CQ.Ext.MessageBox.ERROR,
                                                                buttons: CQ.Ext.Msg.OK
                                                });
                                                return false;
                                }
        			 if (altimage == true) {
                                                CQ.Ext.Msg.show({
                                                                title: 'Confirm',
                                                                msg: 'Please enter the alternate text',
                                                                icon: CQ.Ext.MessageBox.ERROR,
                                                                buttons: CQ.Ext.Msg.OK
                                                });
                                                return false;
                                }
                }
}

function bodyCopyOnLoad(dialog) {
    var currentObj = dialog.getField("./sections");
    var dialogFields = CQ.Util.findFormFields(currentObj);
    var imageneeded =  dialog.getField("./isImageRequired");



          for (var name in dialogFields) {

			 if("./altImage"==name){
				 if(imageneeded.getRawValue() == 'true'){
                  for (var i = 0; i < dialogFields[name].length; i++) {
                		var compositeField = dialogFields[name][i];
                        compositeField.show();
           			 }

            		 }
                 else
                 {

						 for (var i = 0; i < dialogFields[name].length; i++) {
                		var compositeField = dialogFields[name][i];
                        compositeField.hide();
           			 }

                 }

          		}
               if("./image"==name){

				 if(imageneeded.getRawValue() == 'true'){
                  for (var i = 0; i < dialogFields[name].length; i++) {
                		var compositeField = dialogFields[name][i];
                        compositeField.show();
           			 }

            		 }
                 else
                 {

						 for (var i = 0; i < dialogFields[name].length; i++) {
                		var compositeField = dialogFields[name][i];
                        compositeField.hide();
           			 }

                 }

          }

     }


}



function bodyCopyOnChange(comp, val, isChecked){
bodyCopyOnLoad(comp.ownerCt.findParentByType('dialog'));
}






//featuredArticle

function featuredArticleOnLoad(box, value) {
    var dialog = box.ownerCt.findParentByType('dialog');
	var articlePage = dialog.getField("./articlePage");
	var filterTagsNamespace = dialog.getField("./filterTagsNamespace");
	
    if(box.getRawValue()=='Auto' || !(box.getRawValue())){
	filterTagsNamespace.show(); 	
	articlePage.hide();
	articlePage.allowBlank = true;
	}else{
	filterTagsNamespace.hide();	
	articlePage.show();
	articlePage.allowBlank = false;
	}
}

//featuredProduct
function featuredProductOnLoad(box, value) {
    parRefs = box.ownerCt.findByType('selection');

    var dialog = box.ownerCt.findParentByType('dialog');
	var productPage = dialog.getField("./productPage");
	var information = dialog.getField("./info");
	var filterTagsNamespace = dialog.getField("./filterTagsNamespace");
    for (i = 0; i < parRefs.length; i++) {
        if (parRefs[i].name == './featuredProductType') {
            switch (box.getRawValue()) {
                case "Auto":
					filterTagsNamespace.show();
					productPage.hide();
					productPage.allowBlank = true;
					information.hide();
                    break;
                case "Manual":
					filterTagsNamespace.hide();
				    productPage.show();
					productPage.allowBlank = false;
					information.show();
                    break;

            }
        }

    }

}

//featuredProductRange

function featuredProductRangeOnLoad(box, value) {
    var dialog = box.ownerCt.findParentByType('dialog');
	var rangePage = dialog.getField("./productRangePage");
	var rangImage = dialog.getField("./productImage");
	var rangName = dialog.getField("./productName");
	var shortCopy = dialog.getField("./productShortCopy");
	var navigationURL = dialog.getField("./navigationURL");
	var altProductImage = dialog.getField("./altImage");
    if(box.getRawValue()=='Auto' || !(box.getRawValue())){
	rangePage.show();
	rangImage.hide();
	rangName.hide();
	shortCopy.hide();
	altProductImage.hide();
	navigationURL.hide();
	rangePage.allowBlank = false;
	rangImage.allowBlank = true;
	rangName.allowBlank = true;
	navigationURL.allowBlank = true;
	shortCopy.allowBlank = true;
	altProductImage.allowBlank = true;
	}else{
	rangePage.hide();
	rangImage.show();
	rangName.show();
	shortCopy.show();
	navigationURL.show();
	altProductImage.show();
	rangePage.allowBlank = true;
	rangImage.allowBlank = false;
	rangName.allowBlank = false;
	navigationURL.allowBlank = false;
	shortCopy.allowBlank = false;
	altProductImage.allowBlank =false;
	}

}

//hero

function heroOnLoad(box,value)
{
var tabPanel = box.ownerCt.findParentByType('tabpanel');
var dialog = box.ownerCt.findParentByType('dialog');
var heroImageVideo = dialog.getField("./heroImageVideo").getValue();
var imageUrl = dialog.getField("./imageUrl");
var altImage = dialog.getField("./altImage");
var herovideoId = dialog.getField("./herovideoId");
var heroPreviewImageUrl = dialog.getField("./heroPreviewImageUrl");


var selectedValue = box.getRawValue();
var title = dialog.getField("./title");
var introCopy = dialog.getField("./introCopy");
var videoType = dialog.getField("./videoType");
	if(selectedValue == 'Image hero' || !selectedValue){
	    imageUrl.show();
		imageUrl.allowBlank = false;
		altImage.show();
        herovideoId.hide();		
		heroPreviewImageUrl.hide();
		herovideoId.allowBlank = true;
		heroPreviewImageUrl.allowBlank = true;
		videoType.hide();
	}else{
		imageUrl.hide();
		altImage.show();
        herovideoId.show();
		heroPreviewImageUrl.show();
		imageUrl.allowBlank = true;
		herovideoId.allowBlank = false;
		heroPreviewImageUrl.allowBlank = false;
		videoType.show();

	}
}

//homeStoryBeforeSubmit
function homeStoryBeforeSubmit(dialog) {
    var count = 0;
    var currentObj = dialog.getField("./sections");
    var dialogFields = CQ.Util.findFormFields(currentObj);
    for (var name in dialogFields) {
        if ("./homeStoryTitle"==name) {
            for (var i = 0; i < dialogFields[name].length; i++) {
                var compositeField = dialogFields[name][i];
                count = i + 1;
            }
                                }
                                if (count == 0 || count < 0) {
                                                CQ.Ext.Msg.show({
                                                                title: 'Confirm',
                                                                msg: 'Please configure at least one body copy list',
                                                                icon: CQ.Ext.MessageBox.ERROR,
                                                                buttons: CQ.Ext.Msg.OK
                                                });
                                                return false;
                                }
                }
}

//multipleRelatedProductsArticleBeforeSubmit
	function multipleRelatedProductsArticleBeforeSubmit(dialog) {
		var count = 0;
		var currentObj = dialog.getField("./panelsRelated");
		var dialogFields = CQ.Util.findFormFields(currentObj);
		for (var name in dialogFields) {
			if ("./addProduct" == name) {
				for (var i = 0; i < dialogFields[name].length; i++) {
					var compositeField = dialogFields[name][i];
					count = i + 1;
				}
			}
			if (count == 0 || count < 0) {
				CQ.Ext.Msg.show({
					title: 'Confirm',
					msg: 'Please select at least one product path',
					icon: CQ.Ext.MessageBox.ERROR,
					buttons: CQ.Ext.Msg.OK
				});
				return false;
			} else if(count > 6){
				CQ.Ext.Msg.show({
					title: 'Confirm',
					msg: 'You can only select a maximum of 6 products',
					icon: CQ.Ext.MessageBox.ERROR,
					buttons: CQ.Ext.Msg.OK
				});
				return false;
			}
		}
	}

//productCopy 
function productCopyBeforeSubmit(dialog) {
    var count = 0;
    var currentObj = dialog.getField("./description");
	var source = dialog.getField("./source").getRawValue();
    var dialogFields = CQ.Util.findFormFields(currentObj);
	if(source=='Product Data'){
    for (var name in dialogFields) {
        if ("./productCopyPropertyName"==name) {
            for (var i = 0; i < dialogFields[name].length; i++) {
                var compositeField = dialogFields[name][i];
                count = i + 1;
            }
                                }
                                if (count == 0 || count < 0) {
                                                CQ.Ext.Msg.show({
                                                                title: 'Confirm',
                                                                msg: 'Please configure at least one property attribute',
                                                                icon: CQ.Ext.MessageBox.ERROR,
                                                                buttons: CQ.Ext.Msg.OK
                                                });
                                                return false;
                                }
                }
				}
}

//productCopy On Load

function productCopyOnload(box, value) {
    parRefs = box.ownerCt.findByType('selection');

    var dialog = box.ownerCt.findParentByType('dialog');
    var productCopyTitle = dialog.getField('./productCopyTitle');
    var productCopyCategoryTitle= dialog.getField('./productCopyCategoryTitle');
    for (i = 0; i < parRefs.length; i++) {
        if (parRefs[i].name == './source') {
            switch (box.getRawValue()) {
                case "Text":
                    var tabPanel = box.ownerCt.findParentByType('tabpanel');
                    tabPanel.unhideTabStripItem(0);
                    tabPanel.unhideTabStripItem(1);
                    tabPanel.hideTabStripItem(2);
                    productCopyTitle.allowBlank = false;
                    productCopyCategoryTitle.allowBlank = true;
                    break;
                case "Product Data":
                    var tabPanel = box.ownerCt.findParentByType('tabpanel');
                    tabPanel.unhideTabStripItem(0);
                    tabPanel.unhideTabStripItem(2);
                    tabPanel.hideTabStripItem(1);
                    productCopyCategoryTitle.allowBlank = false;
                    productCopyTitle.allowBlank = true;
                    break;

            }
        }

    }

}

//productRangePopulationTypeOnLoad

function productRangePopulationTypeOnLoad(box,value)
{

var dialog = box.ownerCt.findParentByType('dialog');
var rangePopulationType = dialog.getField("./rangePopulationType").getValue();
				  


var productRanges = dialog.getField("./panels");
var productRangesAuto=dialog.getField("./panelsAuto");

if(rangePopulationType == 'Manual')
	{
	
		productRanges.show();
		productRangesAuto.hide();
		
		
		//enable range manual fields
		var currentObj =  dialog.getField("./panels");
		var dialogFields = CQ.Util.findFormFields(currentObj);
				  for (var name in dialogFields)
                  {
				  var indexNum = 0;

                  for (var i = 0; i < dialogFields[name].length; i++) 
                  {
				 if(dialogFields[name][i].name!="./openRangeInNewWindow" && dialogFields[name][i].name != "./panels" && dialogFields[name][i].name != "./panels@Delete" )
                 {
						dialogFields[name][i].allowBlank=false;
					}
				  }
				  }
				  
				  
				//disable range automatic fields
				var currentObj1 =  dialog.getField("./panelsAuto");
				var dialogFields1 = CQ.Util.findFormFields(currentObj1);
				  for (var name in dialogFields1)
                  {
				  var indexNum = 0;

                  for (var i = 0; i < dialogFields1[name].length; i++) 
                  {
				  
				 if(dialogFields[name][i].name!="./openRangeAutoInNewWindow" && dialogFields[name][i].name != "./panelsAuto" && dialogFields[name][i].name != "./panelsAuto@Delete" )
                 {

				 dialogFields[name][i].allowBlank=true;
							
					}
				  }
				  }
				  
				  				
		
		
		
		
		
		
	}else{
		productRanges.hide();
		productRangesAuto.show();
		
		
		//disable range manual fields
		var currentObj =  dialog.getField("./panels");
		var dialogFields = CQ.Util.findFormFields(currentObj);
				  for (var name in dialogFields)
                  {
				  var indexNum = 0;

                  for (var i = 0; i < dialogFields[name].length; i++) 
                  {
				  if(dialogFields[name][i].name!="./openRangeInNewWindow" && dialogFields[name][i].name != "./panels" && dialogFields[name][i].name != "./panels@Delete" )
                 {
						dialogFields[name][i].allowBlank=true;
					}
				  }
				  }
				  
				  //enable range automatic fields
				var currentObj =  dialog.getField("./panelsAuto");
				var dialogFields = CQ.Util.findFormFields(currentObj);
				  for (var name in dialogFields)
                  {
				  var indexNum = 0;

                  for (var i = 0; i < dialogFields[name].length; i++) 
                  {
				 if(dialogFields[name][i].name!="./openRangeAutoInNewWindow" && dialogFields[name][i].name != "./panelsAuto" && dialogFields[name][i].name != "./panelsAuto@Delete" )
                 {
				 
				 dialogFields[name][i].allowBlank=false;
							
					}
				  }
				  }
	

	}
}

//productPopulationTypeOnLoad

function productPopulationTypeOnLoad(box,value) {  
    var dialog = box.ownerCt.findParentByType('dialog');
    var productPopulationType = dialog.getField("./productPopulationType").getValue();
    var rangeTitle = dialog.getField("./rangeTitle");

    var panelsProducts = dialog.getField("./panelsProducts");
    var prodCtaLabelAuto = dialog.getField("./prodCtaLabelAuto");

    var currentObj = dialog.getField("./panelsProducts");
	var filterTagsNamespace = dialog.getField("./filterTagsNamespace");

    if (productPopulationType == 'Manual') {
        panelsProducts.setVisible(true);
        prodCtaLabelAuto.allowBlank = true;
        prodCtaLabelAuto.setVisible(false);
		filterTagsNamespace.hide();

        var dialogFields = CQ.Util.findFormFields(currentObj);
        for (var name in dialogFields) {
            var indexNum = 0;

            for (var i = 0; i < dialogFields[name].length; i++) {
                if (dialogFields[name][i].name != "./openProductInNewWindow" && dialogFields[name][i].name != "./panelsProducts" && dialogFields[name][i].name != "./panelsProducts@Delete") {
                    dialogFields[name][i].allowBlank = false;
                }
            }
        }

    } else {
        prodCtaLabelAuto.allowBlank = false;
        prodCtaLabelAuto.setVisible(true);
        panelsProducts.setVisible(false);
		filterTagsNamespace.show();
    
        var dialogFields = CQ.Util.findFormFields(currentObj);
        for (var name in dialogFields) {
            var indexNum = 0;

            for (var i = 0; i < dialogFields[name].length; i++) {
                if (dialogFields[name][i].name != "./openProductInNewWindow" && dialogFields[name][i].name != "./panelsProducts" && dialogFields[name][i].name != "./panelsProducts@Delete") {
                    dialogFields[name][i].allowBlank = true;

                }
            }
        }

    }
	}

//productORRangePopulationTypeOnLoad 

function productORRangePopulationTypeOnLoad(box,value)
{
var tabPanel = box.ownerCt.findParentByType('tabpanel');
var dialog = box.ownerCt.findParentByType('dialog');
var selectProductOrRange = dialog.getField("./selectProductOrRange").getValue();

var productPopulationType=dialog.getField("./productPopulationType");
var rangePopulationType=dialog.getField("./rangePopulationType");

var rangeTitle=dialog.getField("./rangeTitle");
var productTitle=dialog.getField("./productTitle");

var prodCtaLabelAuto=dialog.getField("./prodCtaLabelAuto");
var addProduct=dialog.getField("./addProduct");
var CTAButtonTextProduct=dialog.getField("./CTAButtonTextProduct");

//range fields
var rangeTitle=dialog.getField("./rangeTitle");
var rangeImage=dialog.getField("./rangeImage");
var rangeImageAltText=dialog.getField("./rangeImageAltText");
var rangeName=dialog.getField("./rangeName");
var rangeShortCopy=dialog.getField("./rangeShortCopy");
var CTAButtonText=dialog.getField("./CTAButtonText");

var CTAButtonLink=dialog.getField("./CTAButtonLink");
var CTAButtonTextAuto=dialog.getField("./CTAButtonTextAuto");
var prodRangePageAuto=dialog.getField("./prodRangePageAuto");




	if(selectProductOrRange=="product")
	{
		tabPanel.unhideTabStripItem(1);
		tabPanel.hideTabStripItem(2);
		
		
		//enable fields in product tab
		productTitle.allowBlank=false;
		if(productPopulationType=='Automatic')
		{
		prodCtaLabelAuto.allowBlank=false;
		}
		else{
		var currentObj =  dialog.getField("./panelsProducts");
			var dialogFields = CQ.Util.findFormFields(currentObj);
				  for (var name in dialogFields)
                  {
				  var indexNum = 0;

                  for (var i = 0; i < dialogFields[name].length; i++) 
                  {
				if( dialogFields[name][i].name != "./openProductInNewWindow" && dialogFields[name][i].name != "./panelsProducts" && dialogFields[name][i].name != "./panelsProducts@Delete")
                 {
						dialogFields[name][i].allowBlank=false;
						
					}
				  }
				  }
				  }
				  
				  
		
		
		
		
		//disabling product range fields-----------
		rangeTitle.allowBlank=true;
		//disable manual range fields
		var currentObj =  dialog.getField("./panels");
			var dialogFields = CQ.Util.findFormFields(currentObj);
				  for (var name in dialogFields)
                  {
				  var indexNum = 0;

                  for (var i = 0; i < dialogFields[name].length; i++) 
                  {
				 if(dialogFields[name][i].name!="./openRangeInNewWindow" && dialogFields[name][i].name != "./panels" && dialogFields[name][i].name != "./panels@Delete" )
                 {
						dialogFields[name][i].allowBlank=true;
					}
				  }
				  }
				  
				  //disable automatic range fields
				  var currentObj =  dialog.getField("./panelsAuto");
					var dialogFields = CQ.Util.findFormFields(currentObj);
				  for (var name in dialogFields)
                  {
				  var indexNum = 0;

                  for (var i = 0; i < dialogFields[name].length; i++) 
                  {
				 if(dialogFields[name][i].name!="./openRangeAutoInNewWindow" && dialogFields[name][i].name != "./panelsAuto" && dialogFields[name][i].name != "./panelsAuto@Delete" )
                 {
				 dialogFields[name][i].allowBlank=true;
							
					}
				  }
				  }
				  
				  
				  
				  
		
		
		
		
		
	}else{
	
	tabPanel.unhideTabStripItem(2);
		tabPanel.hideTabStripItem(1);
	
	//disable fields in product tab
	productTitle.allowBlank=true;
	prodCtaLabelAuto.allowBlank=true;
		var currentObj =  dialog.getField("./panelsProducts");
		var dialogFields = CQ.Util.findFormFields(currentObj);
				  for (var name in dialogFields)
                  {
				  var indexNum = 0;

                  for (var i = 0; i < dialogFields[name].length; i++) 
                  {
				 if( dialogFields[name][i].name != "./openProductInNewWindow" && dialogFields[name][i].name != "./panelsProducts" && dialogFields[name][i].name != "./panelsProducts@Delete")
                 {
						dialogFields[name][i].allowBlank=true;
						
					}
				  }
				  }
	
				
		
		//enabling range fields
		rangeTitle.allowBlank=false;
		
		if(rangePopulationType=='Manual')
		{
				var currentObj =  dialog.getField("./panels");
				var dialogFields = CQ.Util.findFormFields(currentObj);
				  for (var name in dialogFields)
                  {
				  var indexNum = 0;

                  for (var i = 0; i < dialogFields[name].length; i++) 
                  {
				  if(dialogFields[name][i].name!="./openRangeInNewWindow" && dialogFields[name][i].name != "./panels" && dialogFields[name][i].name != "./panels@Delete" )
                 {
						dialogFields[name][i].allowBlank=true;
					}
				  }
				  }
		}
		else if(rangePopulationType=='Automatic')
		{
		 var currentObj =  dialog.getField("./panelsAuto");
					var dialogFields = CQ.Util.findFormFields(currentObj);
				  for (var name in dialogFields)
                  {
				  var indexNum = 0;

                  for (var i = 0; i < dialogFields[name].length; i++) 
                  {
				if(dialogFields[name][i].name!="./openRangeAutoInNewWindow" && dialogFields[name][i].name != "./panelsAuto" && dialogFields[name][i].name != "./panelsAuto@Delete" )
                 {
				 dialogFields[name][i].allowBlank=true;
							
					}
				  }
				  }
		}
		

		
		

	}
}

//whatsNewBeforeSubmit

//whatsNewBeforeSubmit

function whatsNewBeforeSubmit(dialog){
var prodList = dialog.getField('./panelsProducts'); 
var prodOrRange=dialog.getField('./selectProductOrRange').getValue();
if(prodOrRange=='product')
{

if(dialog.getField('./productPopulationType').getValue()=='Manual')
{

 if((prodList.items.getCount() - 1) <1)
 {
   CQ.Ext.Msg.show(
                {title: 'Error',  msg: 'Please configure atleast 1 product',icon:CQ.Ext.MessageBox.WARNING,buttons: CQ.Ext.Msg.OK}
            );
		return false;
 }
 }
 }
 else
 {
 if(dialog.getField('./rangePopulationType').getValue()=='Manual')
 {
 var prodRangeList = dialog.getField('./panels'); 
 if((prodRangeList.items.getCount() - 1) <1)
 {
   CQ.Ext.Msg.show(
                {title: 'Error',  msg: 'Please configure atleast 1 product range.',icon:CQ.Ext.MessageBox.WARNING,buttons: CQ.Ext.Msg.OK}
            );
		return false;
 }
 }

 else if(dialog.getField('./rangePopulationType').getValue()=='Automatic')
 {
  var prodRangeList = dialog.getField('./panelsAuto'); 
 if((prodRangeList.items.getCount() - 1) <1)
 {
   CQ.Ext.Msg.show(
                {title: 'Error',  msg: 'Please configure atleast 1 product range.',icon:CQ.Ext.MessageBox.WARNING,buttons: CQ.Ext.Msg.OK}
            );
		return false;
 }
 }
 }

 }

//whatsNewOnLoad

function whatsNewOnLoad(dialog)
{

	var rangeTitle=dialog.getField("./rangeTitle");
	var productTitle=dialog.getField("./productTitle");
	var selectProductOrRange = dialog.getField("./selectProductOrRange").getValue();
if(selectProductOrRange=='product')
{
rangeTitle.allowBlank=true;
var currentObj =  dialog.getField("./panels");
var dialogFields = CQ.Util.findFormFields(currentObj);
				  for (var name in dialogFields)
                  {
				  var indexNum = 0;

                  for (var i = 0; i < dialogFields[name].length; i++) 
                  {
				 if(dialogFields[name][i].name!="./openRangeInNewWindow" && dialogFields[name][i].name != "./panels" && dialogFields[name][i].name != "./panels@Delete" )
                 {
						dialogFields[name][i].allowBlank=true;
					}
				  }
				  }				  

				  var currentObj =  dialog.getField("./panelsAuto");
var dialogFields = CQ.Util.findFormFields(currentObj);
				  for (var name in dialogFields)
                  {
				  var indexNum = 0;

                  for (var i = 0; i < dialogFields[name].length; i++) 
                  {
				if(dialogFields[name][i].name!="./openRangeAutoInNewWindow" && dialogFields[name][i].name != "./panelsAuto" && dialogFields[name][i].name != "./panelsAuto@Delete" )
                 {
				 dialogFields[name][i].allowBlank=true;

					}
				  }
				  }

}
else{
productTitle.allowBlank=true;
var currentObj =  dialog.getField("./panelsProducts");
var dialogFields = CQ.Util.findFormFields(currentObj);
				  for (var name in dialogFields)
                  {
				  var indexNum = 0;

                  for (var i = 0; i < dialogFields[name].length; i++) 
                  {
				 if( dialogFields[name][i].name != "./openProductInNewWindow" && dialogFields[name][i].name != "./panelsProducts" && dialogFields[name][i].name != "./panelsProducts@Delete")
                 {
						dialogFields[name][i].allowBlank=true;

					}
				  }
				  }


}

}

//featured category listener
function featuredCategoryOnLoad(box, value) {
    var dialog = box.ownerCt.findParentByType('dialog');
	var pageType = dialog.getField("./pageType");
	var backgroundImage = dialog.getField("./backgroundImage");
    var altBackground = dialog.getField("./altBackgroundImage");
	if(box.getRawValue()=='Product' || !(box.getRawValue())){
	backgroundImage.show();
    altBackground.show();
	backgroundImage.allowBlank = false;
    altBackground.allowBlank = false;
	}else{
	backgroundImage.hide();
    altBackground.hide();
	backgroundImage.allowBlank = true;
    altBackground.allowBlank = true;
	}


}

//social gallery carousel expanded listener
function socialGalleryCarouselExpandedOnLoad(box, value) {
 parRefs = box.ownerCt.findByType('selection');

 var dialog = box.ownerCt.findParentByType('dialog');
 for (i = 0; i < parRefs.length; i++) {
  if (parRefs[i].name == './type') {
   switch (box.getRawValue()) {
    case "Manual":
     var tabPanel = box.ownerCt.findParentByType('tabpanel');
     tabPanel.unhideTabStripItem(0);
     tabPanel.hideTabStripItem(1);
     tabPanel.unhideTabStripItem(2);
     break;
    case "Automated":
     var tabPanel = box.ownerCt.findParentByType('tabpanel');
     tabPanel.unhideTabStripItem(0);
     tabPanel.unhideTabStripItem(1);
     tabPanel.unhideTabStripItem(2);
     break;

   }
  }

 }

}

//quick poll listeners
function toggleOpinionType(box,value)	
{
var dialog = box.ownerCt.findParentByType('dialog');
var textOnly = dialog.getField("./textOnly");
var textAndImage = dialog.getField("./textAndImage");
var freeTextPlaceholder = dialog.getField("./freeTextPlaceholder");
var freeTextCharCount = dialog.getField("./freeTextCharCount");
var selectedValue = box.getValue();

if("text"==selectedValue || !selectedValue)
{
textOnly.show();
textAndImage.hide();
freeTextCharCount.hide();
freeTextPlaceholder.hide()

 
}
else if("textAndImage"==selectedValue)
{
textOnly.hide();
textAndImage.show();
freeTextCharCount.hide();
freeTextPlaceholder.hide();

}
else if("freeText"==selectedValue)
{
textOnly.hide();
textAndImage.hide();
freeTextCharCount.show();
freeTextPlaceholder.show();

}

}

function toggleDataPresentation(box,value)	
{

var dialog = box.ownerCt.findParentByType('dialog');
var recommendContent = dialog.getField("./recommendContent");
var recommendedIntroText=dialog.getField("./recommendedIntroText");    

if(box.getRawValue()=='Data Visual' || !(box.getRawValue()))

{
recommendedIntroText.hide();
recommendContent.hide();
}
else if(box.getRawValue()=='Recommended Content')
{
recommendedIntroText.show();
recommendContent.show();
}


}

function pouplatePollId(box, value) {


    var dialog = box.ownerCt.findParentByType('dialog');
    var pollId = dialog.getField("./id").getValue();

    if (pollId == 'undefined' || pollId == '') {
        var obj = {};
        var formActionElements;
        getBrandData(dialog, formActionElements, obj);

        $.ajax({
            type: 'GET',
            url: '/bin/ts/private/quickpoll/pollId',
            data: obj,
            success: function(data) {
                document.getElementById('pollId').value = data.responseData.results[0].pollId;
            }
        });

    }


}

function postPollDataBeforeSubmit(dialog) {

    var formActionElements;

    var obj = {};
    var obj1 = {};
    var obj2 = {};
    var answers = [];

    getBrandData(dialog, formActionElements, obj);

    var pollId = dialog.getField("./id").getValue();
    var startDate = dialog.getField("./startDate").getValue();
    var endDate = dialog.getField("./endDate").getValue();
    var headline = dialog.getField("./questionHeadline").getValue();
    var subHeadline = dialog.getField("./questionSubHeadline").getValue();

    obj["pollId"] = pollId;
    obj["startDate"] = startDate;
    obj["endDate"] = endDate;

    obj1["headline"] = headline;
    obj1["subHeadline"] = subHeadline;


    var opinionType = dialog.getField("./opinionType").getRawValue();
    var answerText = document.getElementsByName("./answerText");
    var opinionText = document.getElementsByName("./opinionText");

    if (opinionType == 'Text') {
        var currentObj = dialog.getField("./textOnly");
        var dialogFields = CQ.Util.findFormFields(currentObj);
        for (var name in dialogFields) {
            if ("./answerText" == name) {
                for (var i = 0; i < dialogFields[name].length; i++) {
                    obj2 = {};
                    obj2["answerText"] = dialogFields[name][i].getValue();
                    answers.push(obj2);
                }


            }


        }

    } else if (opinionType == 'Text and Image') {
        var currentObj = dialog.getField("./textAndImage");
        var dialogFields = CQ.Util.findFormFields(currentObj);
        for (var name in dialogFields) {
            if ("./opinionText" == name) {
                for (var i = 0; i < dialogFields[name].length; i++) {
                    obj2 = {};
                    obj2["answerText"] = dialogFields[name][i].getValue();
                    answers.push(obj2);
                }


            }


        }

    }
    obj1["freeText"] = false;
    obj1["answers"] = answers;

    obj["questions"] = obj1;
    $.ajax({
        url: '/bin/ts/private/quickpoll/question',
        type: "POST",
        data: JSON.stringify(obj),
        dataType: "json",
        contentType: "application/json; charset=utf-8",

        complete: function(data, xhr, errors) {
            if (data1.status === 200) {
                CQ.Ext.Msg.show({
                    title: 'Message',
                    msg: 'Question posted successfully',
                    icon: CQ.Ext.MessageBox.WARNING,
                    buttons: CQ.Ext.Msg.OK
                });
            } else {
                CQ.Ext.Msg.show({
                    title: 'Message',
                    msg: 'Error while posting Question',
                    icon: CQ.Ext.MessageBox.WARNING,
                    buttons: CQ.Ext.Msg.OK
                });
            }
        }

    })
}

//Get Brand data from form action url
function getBrandData(dialog, formActionElements, obj) {
    var formActionUrl = dialog.form.url;
    formActionElements = formActionUrl.split("/");
    obj["brand"] = formActionElements[2];
    obj["entity"] = "quickpoll";
    obj["locale"] = formActionElements[3];
}

//visaul element On Load
function visualElementControlOnLoad(box,value) {
	parRefs = box.ownerCt.findByType('selection');
    var dialog = box.ownerCt.findParentByType('dialog');
    var overrideGlobalConfig = dialog.getField('./overrideGlobalConfig');
    var includeInResults = dialog.getField('./includeInResults');
    var teaserImageFlag = dialog.getField('./teaserImageFlag');
    var teaserCopyFlag = dialog.getField('./teaserCopyFlag');
    var teaserLongCopyFlag = dialog.getField('./teaserLongCopyFlag');
    var teaserPublishDateFlag = dialog.getField('./teaserPublishDateFlag');
    var parentPageDetailsFlag = dialog.getField('./parentPageDetailsFlag');
if(overrideGlobalConfig.getRawValue() == 'true' || overrideGlobalConfig.getRawValue() == 'on'){
    teaserImageFlag.show();
    teaserCopyFlag.show();
    teaserLongCopyFlag.show();
    teaserPublishDateFlag.show();
    parentPageDetailsFlag.show();
    includeInResults.show();
}else{
    teaserImageFlag.hide();
    teaserCopyFlag.hide();
    teaserLongCopyFlag.hide();
    teaserPublishDateFlag.hide();
    parentPageDetailsFlag.hide();
    includeInResults.hide();
}
}


function onHeroV2Change(box,value){
	var tabPanel = box.ownerCt.findParentByType('tabpanel');
	var dialog = box.ownerCt.findParentByType('dialog');
	var heroImageVideo = dialog.getField("./heroType").getValue();
    var videoId= dialog.getField('./videoId');
					
	if(heroImageVideo==="image"){
		tabPanel.unhideTabStripItem(1);
        tabPanel.hideTabStripItem(2);
        videoId.allowBlank=true;
	}
	else if(heroImageVideo==="video"){
		tabPanel.hideTabStripItem(1);
        tabPanel.unhideTabStripItem(2);
        videoId.allowBlank=false;
	}
	else{
		tabPanel.hideTabStripItem(1);
        tabPanel.hideTabStripItem(2);
        videoId.allowBlank=true;
	}
}

function heroV2VisualElementControlTab(box,value){
	var tabPanel = box.ownerCt.findParentByType('tabpanel');
	var dialog = box.ownerCt.findParentByType('dialog');
    var overrideGlobalConfig = dialog.getField('./overrideGlobalConfig');
    var readMoreEnabled = dialog.getField('./readMoreEnabled');

	if(overrideGlobalConfig.getRawValue() == 'true' || overrideGlobalConfig.getRawValue() == 'on'){
        readMoreEnabled.show();
    } else{
        readMoreEnabled.hide();
    }
}

// Featured Product V2 show hide Include in display section
function showHideIncludeInDisplay(comp, val, isChecked){
    var panel = comp.findParentByType("panel"); //find the parent panel container
    var includeInDisplaySection = panel.getComponent("includeInDisplaySection"); //find the component with itemId includeInDisplaySection
    var multiBuyConfiguration = panel.getComponent("multiBuyConfiguration");
    var dialog = comp.ownerCt.findParentByType('dialog');
    var overrideGlobal = dialog.getField('./overrideGlobalConfig');
    /*hide or show component based on checked value */
    if(overrideGlobal.getValue() == 'true'){
    	includeInDisplaySection.show();
    	if(multiBuyConfiguration){
    	multiBuyConfiguration.show();
    	}
    }else{
    	includeInDisplaySection.hide();
    	if(multiBuyConfiguration){
    	multiBuyConfiguration.hide();
    	}
    }
}

function analyticsContextOnLoad(box, value) {
 parRefs = box.ownerCt.findByType('selection');
    var dialog = box.ownerCt.findParentByType('dialog');
    var analtyticContextType = dialog.getField('./configurejson').getValue();
    var customAnalyticsContext = dialog.getField('./customAnalyticsContext');
    if(analtyticContextType == "Custom"){
customAnalyticsContext.show();
    }else{
customAnalyticsContext.hide();
    }
}

function featuredContentOnload(box, value) {
    parRefs = box.ownerCt.findByType('selection');
    var dialog = box.ownerCt.findParentByType('dialog');
    var pageCtaUrl = dialog.getField('./pageCtaUrl');
    var overrideGlobalConfig = dialog.getField('./overrideGlobalConfig');
    var includeInResults = dialog.getField('./includeInResults');
    var teaserImageFlag = dialog.getField('./teaserImageFlag');
    var teaserCopyFlag = dialog.getField('./teaserCopyFlag');
    var teaserLongCopyFlag = dialog.getField('./teaserLongCopyFlag');
    var teaserPublishDateFlag = dialog.getField('./teaserPublishDateFlag');
    var parentPageDetailsFlag = dialog.getField('./parentPageDetailsFlag');
    for (i = 0; i < parRefs.length; i++) {
        if (parRefs[i].name == './featuredContentType') {
            switch (box.getRawValue()) {
                case "Provide Manual Content":
                    var tabPanel = box.ownerCt.findParentByType('tabpanel');
                    tabPanel.unhideTabStripItem(0);
                    tabPanel.unhideTabStripItem(1);
                    tabPanel.hideTabStripItem(2);
					tabPanel.hideTabStripItem(3);
					tabPanel.hideTabStripItem(4);
                    pageCtaUrl.allowBlank = true;
                    break;
                case "Manually Select Page":
                    var tabPanel = box.ownerCt.findParentByType('tabpanel');
                    tabPanel.unhideTabStripItem(0);
                    tabPanel.unhideTabStripItem(2);
                    tabPanel.hideTabStripItem(1);
					tabPanel.hideTabStripItem(3);
					tabPanel.unhideTabStripItem(4);
                    pageCtaUrl.allowBlank = false;
                    break;
				case "Identify Page By Tags":
                    var tabPanel = box.ownerCt.findParentByType('tabpanel');
                    tabPanel.unhideTabStripItem(0);
                    tabPanel.hideTabStripItem(1);
                    tabPanel.hideTabStripItem(2);
					tabPanel.unhideTabStripItem(3);
					tabPanel.unhideTabStripItem(4);
                    pageCtaUrl.allowBlank = true;
                    break;
                default :
                    var tabPanel = box.ownerCt.findParentByType('tabpanel');
                    tabPanel.unhideTabStripItem(0);
                    tabPanel.unhideTabStripItem(1);
                    tabPanel.hideTabStripItem(2);
					tabPanel.hideTabStripItem(3);
					tabPanel.hideTabStripItem(4);
                    pageCtaUrl.allowBlank = true;
            }
        }

    }
}

//visaul element On Load
function relatedArticlesOnLoad(box,value) {
	parRefs = box.ownerCt.findByType('selection');
    var dialog = box.ownerCt.findParentByType('dialog');
    var overrideGlobalConfig = dialog.getField('./overrideGlobalConfig');
    var includeInResults = dialog.getField('./includeInResults');
    var teaserImageFlag = dialog.getField('./teaserImageFlag');
    var teaserCopyFlag = dialog.getField('./teaserCopyFlag');
    var teaserLongCopyFlag = dialog.getField('./teaserLongCopyFlag');
    var teaserPublishDateFlag = dialog.getField('./teaserPublishDateFlag');
    var parentPageDetailsFlag = dialog.getField('./parentPageDetailsFlag');
    var limit = dialog.getField('./limit');
    var buildListUsing = dialog.getField('./buildNavigationUsing').getValue();
    var tabPanel = box.ownerCt.findParentByType('tabpanel');

	var articleMultifield = dialog.getField('./articleListSection');
	var searchKeyword = dialog.getField('./searchKeyword');
	var limitResultCount = dialog.getField('./limitResultCount');
	var limitResultToCategoriesSections = dialog.getField('./limitResultToCategoriesSections');
	var excludeArticleIdsSections = dialog.getField('./excludeArticleIdsSections');
	var articleId = dialog.getField('./articleId');
	var limitResults = dialog.getField('./limitResults');
	var excludeArticle = dialog.getField('./excludeArticle');
	var multifield = box.ownerCt.findByType('multifield');

    if(buildListUsing == 'fixedList' || !buildListUsing){
          tabPanel.unhideTabStripItem(1);
          tabPanel.hideTabStripItem(2);
		  tabPanel.hideTabStripItem(3);
    }else if(buildListUsing == 'tags'){
          tabPanel.unhideTabStripItem(2);
          tabPanel.hideTabStripItem(1);
		  tabPanel.hideTabStripItem(3);
    }else {
		  tabPanel.unhideTabStripItem(3);
		  tabPanel.hideTabStripItem(1);
		  tabPanel.hideTabStripItem(2);
	}
	var buildArticleListUsing = dialog.getField('./buildListUsing').getValue();
	if(buildListUsing == 'allThingsHair' && buildArticleListUsing == 'articleSearch' || !buildArticleListUsing){
          for(var i=0;i<multifield.length;i++){
					if(multifield[i].getName() == "./articleListSection" ){
						multifield[i].hide();
					}
					}
		  searchKeyword.show();
		  searchKeyword.allowBlank=false;
		  limitResultCount.show();
		  for(var i=0;i<multifield.length;i++){
					if(multifield[i].getName() == "./limitResultToCategoriesSections" || multifield[i].getName() == "./excludeArticleIdsSections"){
						multifield[i].show();
					}
					}

	}else{
		  for(var i=0;i<multifield.length;i++){
					if(multifield[i].getName() == "./articleListSection" ){
						multifield[i].show();
					}
					}
		  searchKeyword.hide();
		  searchKeyword.allowBlank=true;
		  limitResultCount.hide();
		  limitResultToCategoriesSections.hide();
		  excludeArticleIdsSections.hide();
	}

if(overrideGlobalConfig.getRawValue() == 'true' || overrideGlobalConfig.getRawValue() == 'on'){
    teaserImageFlag.show();
    teaserCopyFlag.show();
    teaserLongCopyFlag.show();
    teaserPublishDateFlag.show();
    parentPageDetailsFlag.show();
    includeInResults.show();
    limit.show();
}else{
    teaserImageFlag.hide();
    teaserCopyFlag.hide();
    teaserLongCopyFlag.hide();
    teaserPublishDateFlag.hide();
    parentPageDetailsFlag.hide();
    includeInResults.hide();
    limit.hide();
}
var currentObj = dialog.getField("./pages");
var dialogFields = CQ.Util.findFormFields(currentObj);
    var answers = [];
for (var name in dialogFields)
{


if("./externalUrlRequired"==name)
	{
        for (var i = 0; i < dialogFields[name].length; i++) {
                var compositeField = dialogFields[name][i];
            if(compositeField.getRawValue()=='true'){
 answers.push(true);
            }else{
answers.push(false);
            }
          
            }

	}

if("./teaserImage"==name || "./teaserImageAltText"==name || "./teaserTitle"==name || "./text"==name || "./teaserLongCopy"==name){
for (var i = 0; i < dialogFields[name].length; i++) {
var compositeField = dialogFields[name][i];
if(answers[i]){
compositeField.show();
}else{
compositeField.hide();
}
}
}
}
}


//orderedList Steps
function orderedListBeforeSubmit(dialog) {
    var count = 0;
    var currentObj = dialog.getField("./sections");
    var dialogFields = CQ.Util.findFormFields(currentObj);
    var imageneeded =  dialog.getField("./isImageRequired");
    var image = false;
    var altimage = false;
    for (var name in dialogFields) {
        if ("./step"==name) {
            for (var i = 0; i < dialogFields[name].length; i++) {
                var compositeField = dialogFields[name][i];
                count = i + 1;
            }
          }


            if("./image"==name){

                 if(imageneeded.getRawValue() == 'true'){

					 for (var i = 0; i < dialogFields[name].length; i++) {
                		var compositeField = dialogFields[name][i];
                        compositeField.allowBlank = false;
                        if(compositeField.getRawValue() == "")
                         {
							image = true;
                         }


           			 }

                 }
            }

          if("./altImage"==name){

                 if(imageneeded.getRawValue() == 'true'){

					 for (var i = 0; i < dialogFields[name].length; i++) {
                		var compositeField = dialogFields[name][i];
                        compositeField.allowBlank = false;
                        if(compositeField.getRawValue() == "")
                         {
							altimage = true;
                         }

           			 }
                 }
        }
                                if (count == 0 || count < 0) {
                                                CQ.Ext.Msg.show({
                                                                title: 'Confirm',
                                                                msg: 'Please configure at least one body copy list',
                                                                icon: CQ.Ext.MessageBox.ERROR,
                                                                buttons: CQ.Ext.Msg.OK
                                                });
                                                return false;
                                }
        			  if (image == true) {
                                                CQ.Ext.Msg.show({
                                                                title: 'Confirm',
                                                                msg: 'Please enter the image path',
                                                                icon: CQ.Ext.MessageBox.ERROR,
                                                                buttons: CQ.Ext.Msg.OK
                                                });
                                                return false;
                                }
        			 if (altimage == true) {
                                                CQ.Ext.Msg.show({
                                                                title: 'Confirm',
                                                                msg: 'Please enter the alternate text',
                                                                icon: CQ.Ext.MessageBox.ERROR,
                                                                buttons: CQ.Ext.Msg.OK
                                                });
                                                return false;
                                }
                }
}

function orderedListOnLoad(dialog) {
    var currentObj = dialog.getField("./sections");
    var dialogFields = CQ.Util.findFormFields(currentObj);
    var imageneeded =  dialog.getField("./isImageRequired");
	var image= dialog.getField('./image');
    var altImage= dialog.getField('./altImage');


          for (var name in dialogFields) {


              if("./image"==name){

				 if(imageneeded.getRawValue() == 'true'){
                  for (var i = 0; i < dialogFields[name].length; i++) {
                		var compositeField = dialogFields[name][i];
                        compositeField.show();
           			 }

            		 }
                 else
                 {
					for (var i = 0; i < dialogFields[name].length; i++) {
                		var compositeField = dialogFields[name][i];
                        compositeField.hide();
           			 }

                 }

          }

			 if("./altImage"==name){
				 if(imageneeded.getRawValue() == 'true'){
                  for (var i = 0; i < dialogFields[name].length; i++) {
                		var compositeField = dialogFields[name][i];
                        compositeField.show();
           			 }

            		 }
                 else
                 {

						 for (var i = 0; i < dialogFields[name].length; i++) {
                		var compositeField = dialogFields[name][i];
                        compositeField.hide();
           			 }

                 }

          		}


     }


}



function orderedListOnChange(comp, val, isChecked){
orderedListOnLoad(comp.ownerCt.findParentByType('dialog'));
}

function sectionNavigationV2OnLoad(box, value) {
    var dialog = box.ownerCt.findParentByType('dialog');
    var currentObj = dialog.getField("./listLinks");
    var dialogFields = CQ.Util.findFormFields(currentObj);
    var currentObjOne = dialog.getField("./childPagesList");
    var dialogFieldsOne = CQ.Util.findFormFields(currentObjOne);
    var buildNavigationUsing = dialog.getField('./buildNavigationUsing');
    var tabPanel = box.ownerCt.findParentByType('tabpanel');
    if(buildNavigationUsing.getValue()=='fixedList' || !buildNavigationUsing.getValue()){
    tabPanel.unhideTabStripItem(1);
    tabPanel.hideTabStripItem(2);
        for (var name in dialogFields) {
            if("./url"==name){
            for (var i = 0; i < dialogFields[name].length; i++) {
                		var compositeField = dialogFields[name][i];
                        compositeField.allowBlank = false;
           			 }
            }
        }
        for (var name in dialogFieldsOne) {
            if("./parentPageUrl"==name){
            for (var i = 0; i < dialogFields[name].length; i++) {
                		var compositeField = dialogFields[name][i];
                        compositeField.allowBlank = true;
           			 }
            }
        }
    }else{
    tabPanel.unhideTabStripItem(2);
    tabPanel.hideTabStripItem(1);
        for (var name in dialogFields) {
            if("./url"==name){
            for (var i = 0; i < dialogFields[name].length; i++) {
                		var compositeField = dialogFields[name][i];
                        compositeField.allowBlank = true;
           			 }
            }
        }
        for (var name in dialogFieldsOne) {
            if("./parentPageUrl"==name){
            for (var i = 0; i < dialogFields[name].length; i++) {
                		var compositeField = dialogFields[name][i];
                        compositeField.allowBlank = false;
           			 }
            }
        }
    }
}


function searchInputOnLoad(box,value) {
	parRefs = box.ownerCt.findByType('checkbox');
    var dialog = box.ownerCt.findParentByType('dialog');
    var overrideGlobalConfig = dialog.getField('./overrideGlobalConfig');
    var disableAutoSuggest = dialog.getField('./disableAutoSuggest');
    var disableAutoComplete = dialog.getField('./disableAutoComplete');
    var autoSuggestResultCount = dialog.getField('./autoSuggestResultCount');
    var disableContentTypeFilter = dialog.getField('./disableContentTypeFilter');

 
if(overrideGlobalConfig.getRawValue() == 'true' || overrideGlobalConfig.getRawValue() == 'on'){
    disableAutoSuggest.show();
    disableAutoComplete.show();
    autoSuggestResultCount.show();
    disableContentTypeFilter.show();

}else{
    disableAutoSuggest.hide();
    disableAutoComplete.hide();
    autoSuggestResultCount.hide();
    disableContentTypeFilter.hide();

}
}


//Related Products On Load
function RelatedProductsOnSelect(box,value) {

	parRefs = box.ownerCt.findByType('selection');
  var dialog = box.ownerCt.findParentByType('dialog');
  var overrideGlobalConfig = dialog.getField('./overrideGlobalConfig');
  var includeInResults = dialog.getField('./includeInResults');
  var limit = dialog.getField('./limit');
  var productRating = dialog.getField('./productRating');
  var quickView = dialog.getField('./quickView');
  var buyItNow = dialog.getField('./buyItNow');
  var buildListUsing = dialog.getField('./buildList').getValue();
  var tabPanel = box.ownerCt.findParentByType('tabpanel');
  if(buildListUsing == 'Fixed List' || !buildListUsing){
        tabPanel.unhideTabStripItem(1);
        tabPanel.hideTabStripItem(2);
		  limit.hide();
  }else{
        tabPanel.unhideTabStripItem(2);
        tabPanel.hideTabStripItem(1);
		  limit.show();
  }

if(overrideGlobalConfig.getRawValue() == 'true' || overrideGlobalConfig.getRawValue() == 'on'){
  includeInResults.show();
  productRating.show();
  quickView.show();
  buyItNow.show();
}else{
  includeInResults.hide();  
  productRating.hide();
  quickView.hide();
  buyItNow.hide();
}
}

//Related Products On Submit
function RelatedProductsOnSubmit(dialog) {
  var count = 0;
	var prodList = dialog.getField('./productsPathmultifield'); 
   var buildListUsing = dialog.getField('./buildList').getValue();
	  if(buildListUsing == 'Fixed List' || !buildListUsing){
		var dialogFields = CQ.Util.findFormFields(prodList);
		for (var name in dialogFields) {

			if ("./productPath" == name) {
				for (var i = 0; i < dialogFields[name].length; i++) {
					var compositeField = dialogFields[name][i];
					count = i + 1;
				}
          }
			if (count == 0 || count < 0) {
				CQ.Ext.Msg.show({
					title: 'Confirm',
					msg: 'Please select at least one product path',
					icon: CQ.Ext.MessageBox.ERROR,
					buttons: CQ.Ext.Msg.OK
				});
				return false;
			} 
		}

    }

}

//page listing On Load
function pageListingV2OnLoad(box, value) {
    parRefs = box.ownerCt.findByType('selection');
    var dialog = box.ownerCt.findParentByType('dialog');
    var overrideGlobalConfig = dialog.getField('./overrideGlobalConfig');
    var includeInResults = dialog.getField('./includeInResults');
    var teaserImageFlag = dialog.getField('./teaserImageFlag');
    var teaserCopyFlag = dialog.getField('./teaserCopyFlag');
    var teaserLongCopyFlag = dialog.getField('./teaserLongCopyFlag');
    var teaserPublishDateFlag = dialog.getField('./teaserPublishDateFlag');
    var parentPageDetailsFlag = dialog.getField('./parentPageDetailsFlag');
    var limit = dialog.getField('./limit');
    var buildListUsing = dialog.getField('./buildNavigationUsing').getValue();
    var tabPanel = box.ownerCt.findParentByType('tabpanel');
    var summariseNumberOfPages = dialog.getField('./summariseNumberOfPages');
    var summariseResources = dialog.getField('./summariseResources');
    var pageCountLabel = dialog.getField('./pageCountLabel');
    var resourceCountLabel = dialog.getField('./resourceCountLabel');
    if (buildListUsing == 'fixedList' || !buildListUsing) {
        tabPanel.unhideTabStripItem(1);
        tabPanel.hideTabStripItem(2);
        tabPanel.hideTabStripItem(3);
        var currentObj = dialog.getField("./pages");
        var dialogFields = CQ.Util.findFormFields(currentObj);
        var answers = [];
        for (var name in dialogFields) {


            if ("./externalUrlRequired" == name) {
                for (var i = 0; i < dialogFields[name].length; i++) {
                    var compositeField = dialogFields[name][i];
                    if (compositeField.getRawValue() == 'true') {
                        answers.push(true);
                    } else {
                        answers.push(false);
                    }

                }

            }
            if ("./articleUrl" == name) {
                for (var i = 0; i < dialogFields[name].length; i++) {
                    var compositeField = dialogFields[name][i];
                    compositeField.allowBlank = false;
                }
            }
            if ("./teaserImage" == name || "./teaserImageAltText" == name || "./teaserTitle" == name || "./text" == name || "./teaserLongCopy" == name) {
                for (var i = 0; i < dialogFields[name].length; i++) {
                    var compositeField = dialogFields[name][i];
                    if (answers[i]) {
                        compositeField.show();
                    } else {
                        compositeField.hide();
                    }
                }
            }
        }
        var currentObjOne = dialog.getField("./parentPagesJson");
        var dialogFieldsOne = CQ.Util.findFormFields(currentObjOne);
        for (var name in dialogFieldsOne) {

            if ("./parentPageUrl" == name) {
                for (var i = 0; i < dialogFieldsOne[name].length; i++) {
                    var compositeFieldOne = dialogFieldsOne[name][i];
                    compositeFieldOne.allowBlank = true;
                }
            }
        }
    } else if (buildListUsing == 'tags') {
        tabPanel.unhideTabStripItem(2);
        tabPanel.hideTabStripItem(1);
        tabPanel.hideTabStripItem(3);
        var currentObjOne = dialog.getField("./parentPagesJson");
        var dialogFieldsOne = CQ.Util.findFormFields(currentObjOne);
        for (var name in dialogFieldsOne) {

            if ("./parentPageUrl" == name) {
                for (var i = 0; i < dialogFieldsOne[name].length; i++) {
                    var compositeFieldOne = dialogFieldsOne[name][i];
                    compositeFieldOne.allowBlank = true;
                }
            }
        }



        var currentObj = dialog.getField("./pages");
        var dialogFields = CQ.Util.findFormFields(currentObj);
        for (var name in dialogFields) {

            if ("./articleUrl" == name) {
                for (var i = 0; i < dialogFields[name].length; i++) {
                    var compositeField = dialogFields[name][i];
                    compositeField.allowBlank = true;
                }
            }
        }
    } else if (buildListUsing == 'childPages') {
        tabPanel.unhideTabStripItem(3);
        tabPanel.hideTabStripItem(1);
        tabPanel.hideTabStripItem(2);
        var currentObjOne = dialog.getField("./parentPagesJson");
        var dialogFieldsOne = CQ.Util.findFormFields(currentObjOne);
        for (var name in dialogFieldsOne) {

            if ("./parentPageUrl" == name) {
                for (var i = 0; i < dialogFieldsOne[name].length; i++) {
                    var compositeFieldOne = dialogFieldsOne[name][i];
                    compositeFieldOne.allowBlank = false;
                }
            }
        }



        var currentObj = dialog.getField("./pages");
        var dialogFields = CQ.Util.findFormFields(currentObj);
        for (var name in dialogFields) {

            if ("./articleUrl" == name) {
                for (var i = 0; i < dialogFields[name].length; i++) {
                    var compositeField = dialogFields[name][i];
                    compositeField.allowBlank = true;
                }
            }
        }
    }
    if (overrideGlobalConfig.getRawValue() == 'true' || overrideGlobalConfig.getRawValue() == 'on') {
        teaserImageFlag.show();
        teaserCopyFlag.show();
        teaserLongCopyFlag.show();
        teaserPublishDateFlag.show();
        parentPageDetailsFlag.show();
        includeInResults.show();
        limit.show();
    } else {
        teaserImageFlag.hide();
        teaserCopyFlag.hide();
        teaserLongCopyFlag.hide();
        teaserPublishDateFlag.hide();
        parentPageDetailsFlag.hide();
        includeInResults.hide();
        limit.hide();
    }
    if (summariseNumberOfPages.getRawValue() == 'true' || summariseNumberOfPages.getRawValue() == 'on') {
    	pageCountLabel.show();
    } else {
    	pageCountLabel.hide();
    }
    if (summariseResources.getRawValue() == 'true' || summariseResources.getRawValue() == 'on') {
    	resourceCountLabel.show();
    } else {
    	resourceCountLabel.hide();  
    }

}

//tags on load function
function tagsOnLoad(box, value) {
    parRefs = box.ownerCt.findByType('selection');
    var dialog = box.ownerCt.findParentByType('dialog');
    var tagHierarchiesToUse = dialog.getField('./tagHierarchiesToUse');
	var tagsToDisplay = dialog.getField('./tagsToDisplay');
    for (i = 0; i < parRefs.length; i++) {
        if (parRefs[i].name == './buildTagsUsing') {
            switch (box.getRawValue()) {
                case "Parent page tags":
                    var tabPanel = box.ownerCt.findParentByType('tabpanel');
                    tabPanel.unhideTabStripItem(0);
                    tabPanel.unhideTabStripItem(1);
                    tabPanel.hideTabStripItem(2);
					tabPanel.unhideTabStripItem(3);
					tagsToDisplay.allowBlank = true;
                    break;
                case "Manual tags":
                    var tabPanel = box.ownerCt.findParentByType('tabpanel');
                    tabPanel.unhideTabStripItem(0);
                    tabPanel.unhideTabStripItem(2);
                    tabPanel.hideTabStripItem(1);
					tabPanel.unhideTabStripItem(3);
					tagsToDisplay.allowBlank = false;
                    break;
				default :
                    var tabPanel = box.ownerCt.findParentByType('tabpanel');
                    tabPanel.unhideTabStripItem(0);
                    tabPanel.unhideTabStripItem(1);
                    tabPanel.hideTabStripItem(2);
					tabPanel.unhideTabStripItem(3);
					tagsToDisplay.allowBlank = true;
            }
        }

    }
}


//product listing On Load
function productListingV2OnLoad(box,value) {

	parRefs = box.ownerCt.findByType('selection');
  var dialog = box.ownerCt.findParentByType('dialog');
  var overrideGlobalConfig = dialog.getField('./overrideGlobalConfig');
  var includeInResults = dialog.getField('./includeInResults');
  var limit = dialog.getField('./limit');
  var productRating = dialog.getField('./productRating');
  var quickView = dialog.getField('./quickView');
  var buyItNow = dialog.getField('./buyItNow');
  var buildListUsing = dialog.getField('./buildListUsing').getValue();
  var tabPanel = box.ownerCt.findParentByType('tabpanel');
  if(buildListUsing == 'fixedList' || !buildListUsing){
        tabPanel.unhideTabStripItem(1);
        tabPanel.hideTabStripItem(2);
  }else{
        tabPanel.unhideTabStripItem(2);
        tabPanel.hideTabStripItem(1);
  }

if(overrideGlobalConfig.getRawValue() == 'true' || overrideGlobalConfig.getRawValue() == 'on'){
  includeInResults.show();
  productRating.show();
  quickView.show();
  buyItNow.show();
}else{
   includeInResults.hide();  
  productRating.hide();
  quickView.hide();
  buyItNow.hide();
}
}

//product listing On Submit
function productListingV2OnSubmit(dialog) {
  var count = 0;
	var prodList = dialog.getField('./sections'); 
   var buildListUsing = dialog.getField('./buildListUsing').getValue();
	  if(buildListUsing == 'fixedList' || !buildListUsing){
		var dialogFields = CQ.Util.findFormFields(prodList);
		for (var name in dialogFields) {

			if ("./productPath" == name) {
				for (var i = 0; i < dialogFields[name].length; i++) {
					var compositeField = dialogFields[name][i];
                    compositeField.allowBlank = false;
					count = i + 1;
				}
          }
			if (count == 0 || count < 0) {
				CQ.Ext.Msg.show({
					title: 'Confirm',
					msg: 'Please configure at least one product path',
					icon: CQ.Ext.MessageBox.ERROR,
					buttons: CQ.Ext.Msg.OK
				});
				return false;
			} 
		}

    }

}
//productAwardsOverview visaul element On Load
function productAwardsOverviewVisualTabOnLoad(box,value) {
    var dialog = box.ownerCt.findParentByType('dialog');
    var overrideGlobalConfig = dialog.getField('./overrideGlobalConfig');
    var hideCta = dialog.getField('./hideCta');
    var totalAwardsToDisplay = dialog.getField('./totalAwardsToDisplay');
    var numberOfPrimaryAwardsToDisplay = dialog.getField('./numberOfPrimaryAwardsToDisplay');
    var numberOfSecondaryAwardsToDisplay = dialog.getField('./numberOfSecondaryAwardsToDisplay');
    
if(overrideGlobalConfig.getRawValue() == 'true' || overrideGlobalConfig.getRawValue() == 'on'){
    hideCta.show();
    numberOfPrimaryAwardsToDisplay.show();
    numberOfSecondaryAwardsToDisplay.show();
	totalAwardsToDisplay.show();
}else{
    hideCta.hide();
    numberOfPrimaryAwardsToDisplay.hide();
    numberOfSecondaryAwardsToDisplay.hide();
	totalAwardsToDisplay.hide();
}
}

function formElementV2ViewTypeSelectionChanged(box,value) { 
parRefs = box.ownerCt.findByType('selection');
 var dialog = box.ownerCt.findParentByType('dialog');
    var groupName = dialog.getField('./groupName');
    var fieldLabel = dialog.getField('./fieldLabel');
    var fieldValue = dialog.getField('./fieldValue');
    var checkedByDefault = dialog.getField('./checkedByDefault');
    var name = dialog.getField('./name');
    var fieldId = dialog.getField('./fieldId');
    var placeHolderValue = dialog.getField('./placeHolderValue');
    var defaultValue = dialog.getField('./defaultValue');
    var tooltipText = dialog.getField('./tooltipText');
    var rows = dialog.getField('./rows');
    var hiddenValue = dialog.getField('./hiddenValue');
    var data = dialog.getField('./data');
	var datavalues = dialog.getField('./datavalues');
    var allowMultipleFiles = dialog.getField('./allowMultipleFiles');
    var buttonType = dialog.getField('./buttonType');
    
    var tabPanel = box.ownerCt.findParentByType('tabpanel');
    
    var restrictions = dialog.getField('./restriction');
    var validationsMandatoryField = dialog.getField('./validationsMandatoryField');
    var validationsRequiredErrorMessage = dialog.getField('./validationsRequiredErrorMessage');
    var numberMinimumValue = dialog.getField('./numberMinimumValue');
    var numberMaximumValue = dialog.getField('./numberMaximumValue');
    var textFieldMaxLength = dialog.getField('./textFieldMaxLength');
    var dateIncludeIntervalRestriction = dialog.getField('./dateIncludeIntervalRestriction');
    var fileuploadSupportedFileExtension = dialog.getField('./fileuploadSupportedFileExtension');
    var maxFileSize = dialog.getField('./fileuploadMaxFileSize');
    var validationsConstraintType = dialog.getField('./validationsConstraintType');
    var emailConstraintType = dialog.getField('./emailConstraintType');
    var dateConstraintType = dialog.getField('./dateConstraintType');
    var cardNumberRequiredErrorMessage = dialog.getField('./cardNumberConstraintType');
    var telephoneNumberConstraintType = dialog.getField('./telephoneNumberConstraintType');
    var telephoneNumMaximumLength = dialog.getField('./telephoneNumMaximumLength');
    var validationsvalidationExpression = dialog.getField('./validationsvalidationExpression');
    var validationsvalidationErrorMessage = dialog.getField('./validationsvalidationErrorMessage');    
    var dataRadioFieldIdName = './fieldIdRadio';
    var dataDisplayTextName = './displayText';
    var dataValueFieldName = './value'; 
	var invalidCodeMessage = dialog.getField('./invalidCodeMessage');
	var codeExpiredMessage = dialog.getField('./codeExpiredMessage');
	var globalCountExceedMsg = dialog.getField('./globalCountExceedMsg');
	var userCountExceedMsg = dialog.getField('./userCountExceedMsg');	
    
	switch(box.getRawValue()){
	            case "Text field":
				    var tabPanel = box.ownerCt.findParentByType('tabpanel');
					    groupName.hide();
                        fieldLabel.show();
						fieldValue.hide();
						checkedByDefault.hide();
						name.hide();
						fieldId.show();
						placeHolderValue.show();
						defaultValue.show();
						tooltipText.show();
						rows.hide();
						hiddenValue.hide();
						data.hide();
						datavalues.hide();
						allowMultipleFiles.hide();
                        buttonType.hide();
                        name.allowBlank = true;
                        fieldLabel.allowBlank = true;
						fieldId.allowBlank = false;
						
						tabPanel.unhideTabStripItem(1);
						
                        validationsMandatoryField.show();
					    validationsRequiredErrorMessage.show();
					    numberMinimumValue.hide();
					    numberMaximumValue.hide();
					    textFieldMaxLength.show();
					    dateIncludeIntervalRestriction.hide();
					    restrictions.hide();
					    fileuploadSupportedFileExtension.hide();
					    maxFileSize.hide();
					    validationsConstraintType.show();
					    emailConstraintType.hide();
					    dateConstraintType.hide();
					    cardNumberRequiredErrorMessage.hide();
					    telephoneNumberConstraintType.hide();
					    telephoneNumMaximumLength.hide();                        
					    validationsvalidationErrorMessage.show();
						invalidCodeMessage.hide();
						codeExpiredMessage.hide();
						globalCountExceedMsg.hide();
						userCountExceedMsg.hide();
					    formElementV2ValidationsConstraintTypeOnLoad(box, value);
					    formElementV2allowBlankCheck(datavalues,dataRadioFieldIdName,true);
					    formElementV2allowBlankCheck(data,dataDisplayTextName,true);
					    formElementV2allowBlankCheck(data,dataValueFieldName,true);
                break;
                case "Text area":
				    var tabPanel = box.ownerCt.findParentByType('tabpanel');
					    groupName.hide();
                        fieldLabel.show();
						fieldValue.hide();
						checkedByDefault.hide();
						name.hide();
						fieldId.show();
						placeHolderValue.show();
						defaultValue.show();
						tooltipText.show();
						rows.show();
						hiddenValue.hide();
						data.hide();
						datavalues.hide();
						allowMultipleFiles.hide();
                        buttonType.hide();
                        name.allowBlank = true;
                        fieldLabel.allowBlank = true;
						fieldId.allowBlank = false;
						
						tabPanel.unhideTabStripItem(1);
						
						validationsMandatoryField.show();
					    validationsRequiredErrorMessage.show();
					    numberMinimumValue.hide();
					    numberMaximumValue.hide();
					    textFieldMaxLength.hide();
					    dateIncludeIntervalRestriction.hide();
					    restrictions.hide();
					    fileuploadSupportedFileExtension.hide();
					    maxFileSize.hide();
					    validationsConstraintType.show();
					    emailConstraintType.hide();
					    dateConstraintType.hide();
					    cardNumberRequiredErrorMessage.hide();
					    telephoneNumberConstraintType.hide();
					    telephoneNumMaximumLength.hide();
					    validationsvalidationErrorMessage.show();
						invalidCodeMessage.hide();
						codeExpiredMessage.hide();
						globalCountExceedMsg.hide();
						userCountExceedMsg.hide();
					    formElementV2ValidationsConstraintTypeOnLoad(box, value);
					    formElementV2allowBlankCheck(datavalues,dataRadioFieldIdName,true);
					    formElementV2allowBlankCheck(data,dataDisplayTextName,true);
					    formElementV2allowBlankCheck(data,dataValueFieldName,true);
                break;
				case "Number":
				    var tabPanel = box.ownerCt.findParentByType('tabpanel');
					    groupName.hide();
                        fieldLabel.show();
						fieldValue.hide();
						checkedByDefault.hide();
						name.hide();
						fieldId.show();
						placeHolderValue.show();
						defaultValue.show();
						tooltipText.show();
						rows.hide();
						hiddenValue.hide();
						data.hide();
						datavalues.hide();
						allowMultipleFiles.hide();
                        buttonType.hide();
                        name.allowBlank = true;
                        fieldLabel.allowBlank = true;
						fieldId.allowBlank = false;
						
						tabPanel.unhideTabStripItem(1);
						
						validationsMandatoryField.show();
					    validationsRequiredErrorMessage.show();
					    numberMinimumValue.show();
					    numberMaximumValue.show();
					    textFieldMaxLength.hide();
					    dateIncludeIntervalRestriction.hide();
					    restrictions.hide();
					    fileuploadSupportedFileExtension.hide();
					    maxFileSize.hide();
					    validationsConstraintType.hide();
					    emailConstraintType.hide();
					    dateConstraintType.hide();
					    cardNumberRequiredErrorMessage.hide();
					    telephoneNumberConstraintType.hide();
					    telephoneNumMaximumLength.hide();
					    validationsvalidationExpression.hide();
					    validationsvalidationErrorMessage.show();
						invalidCodeMessage.hide();
						codeExpiredMessage.hide();
						globalCountExceedMsg.hide();
						userCountExceedMsg.hide();
					    formElementV2allowBlankCheck(datavalues,dataRadioFieldIdName,true);
					    formElementV2allowBlankCheck(data,dataDisplayTextName,true);
					    formElementV2allowBlankCheck(data,dataValueFieldName,true);
                break;
				case "Email":
				    var tabPanel = box.ownerCt.findParentByType('tabpanel');
					    groupName.hide();
                        fieldLabel.show();
						fieldValue.hide();
						checkedByDefault.hide();
						name.hide();
						fieldId.show();
						placeHolderValue.show();
						defaultValue.show();
						tooltipText.show();
						rows.hide();
						hiddenValue.hide();
						data.hide();
						datavalues.hide();
						allowMultipleFiles.hide();
                        buttonType.hide();
                        name.allowBlank = true;
                        fieldLabel.allowBlank = true;
                        fieldId.allowBlank = false;
						
						tabPanel.unhideTabStripItem(1);
						
						validationsMandatoryField.show();
					    validationsRequiredErrorMessage.show();
					    numberMinimumValue.hide();
					    numberMaximumValue.hide();
					    textFieldMaxLength.hide();
					    dateIncludeIntervalRestriction.hide();
					    restrictions.hide();
					    fileuploadSupportedFileExtension.hide();
					    maxFileSize.hide();
					    validationsConstraintType.hide();
					    emailConstraintType.show();
					    dateConstraintType.hide();
					    cardNumberRequiredErrorMessage.hide();
					    telephoneNumberConstraintType.hide();
					    telephoneNumMaximumLength.hide();
					    validationsvalidationErrorMessage.show();
						invalidCodeMessage.hide();
						codeExpiredMessage.hide();
						globalCountExceedMsg.hide();
						userCountExceedMsg.hide();
					    formElementV2emailConstraintTypeOnLoad(box,value);
					    formElementV2allowBlankCheck(datavalues,dataRadioFieldIdName,true);
					    formElementV2allowBlankCheck(data,dataDisplayTextName,true);
					    formElementV2allowBlankCheck(data,dataValueFieldName,true);
                break;
				case "Password":
				    var tabPanel = box.ownerCt.findParentByType('tabpanel');
					    groupName.hide();
                        fieldLabel.show();
						fieldValue.hide();
						checkedByDefault.hide();
						name.hide();
						fieldId.show();
						placeHolderValue.show();
						defaultValue.hide();
						tooltipText.show();
						rows.hide();
						hiddenValue.hide();
						data.hide();
						datavalues.hide();
						allowMultipleFiles.hide();
						buttonType.hide();
						name.allowBlank = true;
						fieldLabel.allowBlank = true;
                        fieldId.allowBlank = false;
						
						tabPanel.unhideTabStripItem(1);
						
						validationsMandatoryField.show();
					    validationsRequiredErrorMessage.show();
					    numberMinimumValue.hide();
					    numberMaximumValue.hide();
					    textFieldMaxLength.show();
					    dateIncludeIntervalRestriction.hide();
					    restrictions.hide();
					    fileuploadSupportedFileExtension.hide();
					    maxFileSize.hide();
					    validationsConstraintType.show();
					    emailConstraintType.hide();
					    dateConstraintType.hide();
					    cardNumberRequiredErrorMessage.hide();
					    telephoneNumberConstraintType.hide();
					    telephoneNumMaximumLength.hide();
					    validationsvalidationErrorMessage.show();
						invalidCodeMessage.hide();
						codeExpiredMessage.hide();
						globalCountExceedMsg.hide();
						userCountExceedMsg.hide();
					    formElementV2ValidationsConstraintTypeOnLoad(box, value);
					    formElementV2allowBlankCheck(datavalues,dataRadioFieldIdName,true);
					    formElementV2allowBlankCheck(data,dataDisplayTextName,true);
					    formElementV2allowBlankCheck(data,dataValueFieldName,true);
                break;
				case "Date":
				    var tabPanel = box.ownerCt.findParentByType('tabpanel');
					    groupName.hide();
                        fieldLabel.show();
						fieldValue.hide();
						checkedByDefault.hide();
						name.hide();
						fieldId.show();
						placeHolderValue.show();
						defaultValue.show();
						tooltipText.show();
						rows.hide();
						hiddenValue.hide();
						data.hide();
						datavalues.hide();
						allowMultipleFiles.hide();
                        buttonType.hide();
                        name.allowBlank = true;
                        fieldLabel.allowBlank = true;
						fieldId.allowBlank = false;
						
						tabPanel.unhideTabStripItem(1);
						
						validationsMandatoryField.show();
					    validationsRequiredErrorMessage.show();
					    numberMinimumValue.hide();
					    numberMaximumValue.hide();
					    textFieldMaxLength.hide();
					    dateIncludeIntervalRestriction.show();
					    restrictions.hide();
					    fileuploadSupportedFileExtension.hide();
					    maxFileSize.hide();
					    validationsConstraintType.hide();
					    emailConstraintType.hide();
					    dateConstraintType.show();
					    cardNumberRequiredErrorMessage.hide();
					    telephoneNumberConstraintType.hide();
					    telephoneNumMaximumLength.hide();
					    validationsvalidationErrorMessage.show();
						invalidCodeMessage.hide();
						codeExpiredMessage.hide();
						globalCountExceedMsg.hide();
						userCountExceedMsg.hide();
					    formElementV2dateConstraintTypeOnLoad(box, value);
					    formElementV2allowBlankCheck(datavalues,dataRadioFieldIdName,true);
					    formElementV2allowBlankCheck(data,dataDisplayTextName,true);
					    formElementV2allowBlankCheck(data,dataValueFieldName,true);
                break;
				case "Telephone number":
				    var tabPanel = box.ownerCt.findParentByType('tabpanel');
					    groupName.hide();
                        fieldLabel.show();
						fieldValue.hide();
						checkedByDefault.hide();
						name.hide();
						fieldId.show();
						placeHolderValue.show();
						defaultValue.show();
						tooltipText.show();
						rows.hide();
						hiddenValue.hide();
						data.hide();
						datavalues.hide();
						allowMultipleFiles.hide();
                        buttonType.hide();
                        name.allowBlank = true;
                        fieldLabel.allowBlank = true;
						fieldId.allowBlank = false;
						
						tabPanel.unhideTabStripItem(1);
						
						validationsMandatoryField.show();
					    validationsRequiredErrorMessage.show();
					    numberMinimumValue.hide();
					    numberMaximumValue.hide();
					    textFieldMaxLength.hide();
					    dateIncludeIntervalRestriction.hide();
					    restrictions.hide();
					    fileuploadSupportedFileExtension.hide();
					    maxFileSize.hide();
					    validationsConstraintType.hide();
					    emailConstraintType.hide();
					    dateConstraintType.hide();
					    cardNumberRequiredErrorMessage.hide();
					    telephoneNumberConstraintType.show();
					    telephoneNumMaximumLength.hide();
					    validationsvalidationErrorMessage.show();
						invalidCodeMessage.hide();
						codeExpiredMessage.hide();
						globalCountExceedMsg.hide();
						userCountExceedMsg.hide();
					    formElementV2telephoneNumberConstraintTypeOnLoad(box, value);
					    formElementV2allowBlankCheck(datavalues,dataRadioFieldIdName,true);
					    formElementV2allowBlankCheck(data,dataDisplayTextName,true);
					    formElementV2allowBlankCheck(data,dataValueFieldName,true);
                break;
				case "Checkbox":
				    var tabPanel = box.ownerCt.findParentByType('tabpanel');
					    groupName.hide();
                        fieldLabel.show();
						fieldValue.show();
						checkedByDefault.show();
						name.show();
						fieldId.show();
						placeHolderValue.hide();
						defaultValue.hide();
						tooltipText.show();
						rows.hide();
						hiddenValue.hide();
						data.hide();
						datavalues.hide();
						allowMultipleFiles.hide();
                        buttonType.hide();
						invalidCodeMessage.hide();
						codeExpiredMessage.hide();
						globalCountExceedMsg.hide();
						userCountExceedMsg.hide();
                        name.allowBlank = true;
                        fieldLabel.allowBlank = true;
						fieldId.allowBlank = false;
						tabPanel.unhideTabStripItem(1);
						
						validationsMandatoryField.show();
					    validationsRequiredErrorMessage.show();
					    numberMinimumValue.hide();
					    numberMaximumValue.hide();
					    textFieldMaxLength.hide();
					    dateIncludeIntervalRestriction.hide();
					    restrictions.hide();
					    fileuploadSupportedFileExtension.hide();
					    maxFileSize.hide();
					    validationsConstraintType.hide();
					    emailConstraintType.hide();
					    dateConstraintType.hide();
					    cardNumberRequiredErrorMessage.hide();
					    telephoneNumberConstraintType.hide();
					    telephoneNumMaximumLength.hide();
					    validationsvalidationExpression.hide();
					    validationsvalidationErrorMessage.hide();
						invalidCodeMessage.hide();
						codeExpiredMessage.hide();
						globalCountExceedMsg.hide();
						userCountExceedMsg.hide();
					    formElementV2allowBlankCheck(datavalues,dataRadioFieldIdName,true);
					    formElementV2allowBlankCheck(data,dataDisplayTextName,false);
					    formElementV2allowBlankCheck(data,dataValueFieldName,false);

                break;
				case "Radio button":
				    var tabPanel = box.ownerCt.findParentByType('tabpanel'); 
					    groupName.show();
                        fieldLabel.hide();
						fieldValue.hide();
						checkedByDefault.hide();
						name.hide();
						fieldId.hide();
						placeHolderValue.hide();
						defaultValue.hide();
						tooltipText.hide();
						rows.hide();
						hiddenValue.hide();
						data.hide();
						datavalues.show();
						allowMultipleFiles.hide();
                        buttonType.hide();
						invalidCodeMessage.hide();
						codeExpiredMessage.hide();
						globalCountExceedMsg.hide();
						userCountExceedMsg.hide();
                        name.allowBlank = true;
                        fieldLabel.allowBlank = true;
						fieldId.allowBlank = true;
						tabPanel.unhideTabStripItem(1);
						
						validationsMandatoryField.show();
					    validationsRequiredErrorMessage.show();
					    numberMinimumValue.hide();
					    numberMaximumValue.hide();
					    textFieldMaxLength.hide();
					    dateIncludeIntervalRestriction.hide();
					    restrictions.hide();
					    fileuploadSupportedFileExtension.hide();
					    maxFileSize.hide();
					    validationsConstraintType.hide();
					    emailConstraintType.hide();
					    dateConstraintType.hide();
					    cardNumberRequiredErrorMessage.hide();
					    telephoneNumberConstraintType.hide();
					    telephoneNumMaximumLength.hide();
					    validationsvalidationExpression.hide();
					    validationsvalidationErrorMessage.hide();
						invalidCodeMessage.hide();
						codeExpiredMessage.hide();
						globalCountExceedMsg.hide();
						userCountExceedMsg.hide();
					    formElementV2allowBlankCheck(datavalues,dataRadioFieldIdName,true);
					    formElementV2allowBlankCheck(data,dataDisplayTextName,false);
					    formElementV2allowBlankCheck(data,dataValueFieldName,false);
						
                break;
				case "Dropdown":
				    var tabPanel = box.ownerCt.findParentByType('tabpanel');
					    groupName.hide();
                        fieldLabel.show();
						fieldValue.hide();
						checkedByDefault.hide();
						name.hide();
						fieldId.show();
						placeHolderValue.hide();
						defaultValue.show();
						tooltipText.show();
						rows.hide();
						hiddenValue.hide();
						data.show();
						datavalues.hide();
						allowMultipleFiles.hide();
                        buttonType.hide();
                        name.allowBlank = true;
                        fieldLabel.allowBlank = true;
						fieldId.allowBlank = false;
						
						tabPanel.unhideTabStripItem(1);
						
						validationsMandatoryField.show();
					    validationsRequiredErrorMessage.show();
					    numberMinimumValue.hide();
					    numberMaximumValue.hide();
					    textFieldMaxLength.hide();
					    dateIncludeIntervalRestriction.hide();
					    restrictions.hide();
					    fileuploadSupportedFileExtension.hide();
					    maxFileSize.hide();
					    validationsConstraintType.hide();
					    emailConstraintType.hide();
					    dateConstraintType.hide();
					    cardNumberRequiredErrorMessage.hide();
					    telephoneNumberConstraintType.hide();
					    telephoneNumMaximumLength.hide();
					    validationsvalidationExpression.hide();
					    validationsvalidationErrorMessage.hide();
						invalidCodeMessage.hide();
						codeExpiredMessage.hide();
						globalCountExceedMsg.hide();
						userCountExceedMsg.hide();
					    formElementV2allowBlankCheck(datavalues,dataRadioFieldIdName,true);
					    formElementV2allowBlankCheck(data,dataDisplayTextName,false);
					    formElementV2allowBlankCheck(data,dataValueFieldName,false);
                break;
				case "Card number":
				    var tabPanel = box.ownerCt.findParentByType('tabpanel');
					    groupName.hide();
                        fieldLabel.show();
						fieldValue.hide();
						checkedByDefault.hide();
						name.hide();
						fieldId.show();
						placeHolderValue.show();
						defaultValue.show();
						tooltipText.show();
						rows.hide();
						hiddenValue.hide();
						data.hide();
						datavalues.hide();
						allowMultipleFiles.hide();
                        buttonType.hide();
                        name.allowBlank = true;
                        fieldLabel.allowBlank = true;
						fieldId.allowBlank = false;
						
						tabPanel.unhideTabStripItem(1);

						validationsMandatoryField.show();
					    validationsRequiredErrorMessage.show();
					    numberMinimumValue.hide();
					    numberMaximumValue.hide();
					    textFieldMaxLength.hide();
					    dateIncludeIntervalRestriction.hide();
					    restrictions.hide();
					    fileuploadSupportedFileExtension.hide();
					    maxFileSize.hide();
					    validationsConstraintType.hide();
					    emailConstraintType.hide();
					    dateConstraintType.hide();
					    cardNumberRequiredErrorMessage.show();
					    telephoneNumberConstraintType.hide();
					    telephoneNumMaximumLength.hide();
					    validationsvalidationErrorMessage.show();
						invalidCodeMessage.hide();
						codeExpiredMessage.hide();
						globalCountExceedMsg.hide();
						userCountExceedMsg.hide();
					    formElementV2CardNumberConstraintTypeOnLoad(box,value);
					    formElementV2allowBlankCheck(datavalues,dataRadioFieldIdName,true);
					    formElementV2allowBlankCheck(data,dataDisplayTextName,true);
					    formElementV2allowBlankCheck(data,dataValueFieldName,true);
                break;
				case "File upload":
				    var tabPanel = box.ownerCt.findParentByType('tabpanel');
					    groupName.hide();
                        fieldLabel.show();
						fieldValue.hide();
						checkedByDefault.hide();
						name.hide();
						fieldId.show();
						placeHolderValue.show();
						defaultValue.show();
						tooltipText.show();
						rows.hide();
						hiddenValue.hide();
						data.hide();
						datavalues.hide();
						allowMultipleFiles.show();
                        buttonType.hide();
                        name.allowBlank = true;
                        fieldLabel.allowBlank = true;
						fieldId.allowBlank = false;
						
						tabPanel.unhideTabStripItem(1);
						
						validationsMandatoryField.show();
					    validationsRequiredErrorMessage.show();
					    numberMinimumValue.hide();
					    numberMaximumValue.hide();
					    textFieldMaxLength.hide();
					    dateIncludeIntervalRestriction.hide();
					    restrictions.hide();
					    fileuploadSupportedFileExtension.show();
					    maxFileSize.show();
					    validationsConstraintType.hide();
					    emailConstraintType.hide();
					    dateConstraintType.hide();
					    cardNumberRequiredErrorMessage.hide();
					    telephoneNumberConstraintType.hide();
					    telephoneNumMaximumLength.hide();
					    validationsvalidationExpression.hide();
					    validationsvalidationErrorMessage.show();
						invalidCodeMessage.hide();
						codeExpiredMessage.hide();
						globalCountExceedMsg.hide();
						userCountExceedMsg.hide();
					    formElementV2allowBlankCheck(datavalues,dataRadioFieldIdName,true);
					    formElementV2allowBlankCheck(data,dataDisplayTextName,true);
					    formElementV2allowBlankCheck(data,dataValueFieldName,true);
                break;
				case "Hidden":
				    var tabPanel = box.ownerCt.findParentByType('tabpanel');
					    groupName.hide();
                        fieldLabel.hide();
						fieldValue.hide();
						checkedByDefault.hide();
						name.hide();
						fieldId.show();
						placeHolderValue.hide();
						defaultValue.hide();
						tooltipText.hide();
						rows.hide();
						hiddenValue.show();
						data.hide();
						datavalues.hide();
						allowMultipleFiles.hide();
                        buttonType.hide();
						invalidCodeMessage.hide();
						codeExpiredMessage.hide();
						globalCountExceedMsg.hide();
						userCountExceedMsg.hide();
                        name.allowBlank = true;
						fieldId.allowBlank = false;		
						fieldLabel.allowBlank = true;
						formElementV2allowBlankCheck(datavalues,dataRadioFieldIdName,true);
					    formElementV2allowBlankCheck(data,dataDisplayTextName,true);
					    formElementV2allowBlankCheck(data,dataValueFieldName,true);
						tabPanel.hideTabStripItem(1);
                break;
				case "Captcha validation":
				    var tabPanel = box.ownerCt.findParentByType('tabpanel');
					    groupName.hide();
                        fieldLabel.hide();
						fieldValue.hide();
						checkedByDefault.hide();
						name.hide();
						fieldId.hide();
						placeHolderValue.hide();
						defaultValue.hide();
						tooltipText.hide();
						rows.hide();
						hiddenValue.hide();
						data.hide();
						datavalues.hide();
						allowMultipleFiles.hide();
                        buttonType.hide();
						invalidCodeMessage.hide();
						codeExpiredMessage.hide();
						globalCountExceedMsg.hide();
						userCountExceedMsg.hide();
                        name.allowBlank = true;
						fieldId.allowBlank = true;
						fieldLabel.allowBlank = true;
						formElementV2allowBlankCheck(datavalues,dataRadioFieldIdName,true);
					    formElementV2allowBlankCheck(data,dataDisplayTextName,true);
					    formElementV2allowBlankCheck(data,dataValueFieldName,true);
						tabPanel.hideTabStripItem(1);
                break;
                case "Button":
				    var tabPanel = box.ownerCt.findParentByType('tabpanel');
					    groupName.hide();
                        fieldLabel.show();
						fieldValue.hide();
						checkedByDefault.hide();
						name.show();
						fieldId.hide();
						placeHolderValue.hide();
						defaultValue.hide();
						tooltipText.hide();
						rows.hide();
						hiddenValue.hide();
						data.hide();
						datavalues.hide();
						allowMultipleFiles.hide();
                        buttonType.show();	
						invalidCodeMessage.hide();
						codeExpiredMessage.hide();
						globalCountExceedMsg.hide();
						userCountExceedMsg.hide();
                        fieldId.allowBlank = true;
                        fieldLabel.allowBlank = false;
                        name.allowBlank = false;
                        formElementV2allowBlankCheck(datavalues,dataRadioFieldIdName,true);
                        formElementV2allowBlankCheck(data,dataDisplayTextName,true);
                        formElementV2allowBlankCheck(data,dataValueFieldName,true);
                        tabPanel.hideTabStripItem(1);
                break;
				case "Coupon code":
				    var tabPanel = box.ownerCt.findParentByType('tabpanel');
					    groupName.hide();
                        fieldLabel.show();
						fieldValue.hide();
						checkedByDefault.hide();
						name.hide();
						fieldId.show();
						placeHolderValue.show();
						defaultValue.show();
						tooltipText.show();
						rows.hide();
						hiddenValue.hide();
						data.hide();
						datavalues.hide();
						allowMultipleFiles.hide();
                        buttonType.hide();
                        name.allowBlank = true;
                        fieldLabel.allowBlank = true;
						fieldId.allowBlank = false;
						
						tabPanel.unhideTabStripItem(1);
						
                        validationsMandatoryField.show();
					    validationsRequiredErrorMessage.show();
					    numberMinimumValue.hide();
					    numberMaximumValue.hide();
					    textFieldMaxLength.hide();
					    dateIncludeIntervalRestriction.hide();
					    restrictions.hide();
					    fileuploadSupportedFileExtension.hide();
					    maxFileSize.hide();
					    validationsConstraintType.hide();
					    emailConstraintType.hide();
					    dateConstraintType.hide();
					    cardNumberRequiredErrorMessage.hide();
					    telephoneNumberConstraintType.hide();
					    telephoneNumMaximumLength.hide();
						validationsvalidationExpression.hide();						
					    validationsvalidationErrorMessage.hide();
						invalidCodeMessage.show();
						codeExpiredMessage.show();
						globalCountExceedMsg.show();
						userCountExceedMsg.show();
					    formElementV2allowBlankCheck(datavalues,dataRadioFieldIdName,true);
					    formElementV2allowBlankCheck(data,dataDisplayTextName,true);
					    formElementV2allowBlankCheck(data,dataValueFieldName,true);
                break;
				default :
                    var tabPanel = box.ownerCt.findParentByType('tabpanel');
					    groupName.hide();
                        fieldLabel.show();
						fieldValue.hide();
						checkedByDefault.hide();
						name.hide();
						fieldId.show();
						placeHolderValue.show();
						defaultValue.show();
						tooltipText.show();
						rows.hide();
						hiddenValue.hide();
						data.hide();
						datavalues.hide();
						allowMultipleFiles.hide();
                        buttonType.hide();
						fieldId.allowBlank = false;
						tabPanel.hideTabStripItem(1);
	}
	
}  

function formElementV2allowBlankCheck(multifieldname,fieldName,allowBlankValue){
    var datavaluesfields = CQ.Util.findFormFields(multifieldname);
    	for (var name in datavaluesfields) {
    		if (fieldName == name) {
            for (var i = 0; i < datavaluesfields[name].length; i++) {
                var compositeField = datavaluesfields[name][i];
                compositeField.allowBlank = allowBlankValue;
            }
        }
    }
}

function formElementV2CardNumberConstraintTypeOnLoad(box,value) { 
	parRefs = box.ownerCt.findByType('selection');
	var dialog = box.ownerCt.findParentByType('dialog');
	var elementType = dialog.getField('./elementType');
	var validationsvalidationExpression = dialog.getField('./validationsvalidationExpression');
	var cardNumberConstraintType = dialog.getField('./cardNumberConstraintType').getRawValue();	
	if (elementType.getRawValue() == "Card number") {
		if(cardNumberConstraintType==""||cardNumberConstraintType==null||cardNumberConstraintType=="undefined" ||cardNumberConstraintType=="Regular expression"){
			validationsvalidationExpression.show();
		} else {
			validationsvalidationExpression.hide();
		}
	}
}

function formElementV2emailConstraintTypeOnLoad(box,value) { 
	parRefs = box.ownerCt.findByType('selection');
	var dialog = box.ownerCt.findParentByType('dialog');
	var elementType = dialog.getField('./elementType');
	var validationsvalidationExpression = dialog.getField('./validationsvalidationExpression');
	var emailConstraintType = dialog.getField('./emailConstraintType').getRawValue();
	if (elementType.getRawValue() == "Email") {
		if(emailConstraintType==""||emailConstraintType==null||emailConstraintType=="undefined" ||emailConstraintType=="Regular expression"){
			validationsvalidationExpression.show();
		} else {
			validationsvalidationExpression.hide();
		}
	}
}

function formElementV2ValidationsConstraintTypeOnLoad(box, value) {
	parRefs = box.ownerCt.findByType('selection');
	var dialog = box.ownerCt.findParentByType('dialog');
	var elementType = dialog.getField('./elementType');
	var validationsvalidationExpression = dialog.getField('./validationsvalidationExpression');
	var validationsConstraintType = dialog.getField('./validationsConstraintType').getRawValue();
	if (elementType.getRawValue() == "Text field" || elementType.getRawValue() == "Text area" || elementType.getRawValue() == "Password") {
		if(validationsConstraintType==""||validationsConstraintType==null||validationsConstraintType=="undefined" ||validationsConstraintType=="Regular expression"){
			validationsvalidationExpression.show();
		} else {
			validationsvalidationExpression.hide();
		}
	}
}


function formElementV2IncludeRestrictionsOnClick(box,value) {
 parRefs = box.ownerCt.findByType('selection');
 var dialog = box.ownerCt.findParentByType('dialog');
    var dateIncludeIntervalRestriction = dialog.getField('./dateIncludeIntervalRestriction');
    var elementType = dialog.getField('./elementType');
    var restriction = dialog.getField('./restriction');
   if(elementType.getRawValue()=="Date" && (dateIncludeIntervalRestriction.getRawValue() == 'true' || dateIncludeIntervalRestriction.getRawValue() == 'on')){
	restriction.show();
   }else{
	restriction.hide();
   }
}

function formElementV2textFieldConstraintTypeOnLoad(box,value) { 
 parRefs = box.ownerCt.findByType('selection');
 var dialog = box.ownerCt.findParentByType('dialog');
    var textFieldValidationExpression = dialog.getField('./textFieldValidationExpression');
   if(box.getRawValue()==""||box.getRawValue()==null||box.getRawValue()=="undefined" ||box.getRawValue()=="Regular expression"){
	textFieldValidationExpression.show();	
   }else{
	textFieldValidationExpression.hide();
   }
}

function formElementV2dateConstraintTypeOnLoad(box, value) {
	parRefs = box.ownerCt.findByType('selection');
	var dialog = box.ownerCt.findParentByType('dialog');
	var elementType = dialog.getField('./elementType');
	var validationsvalidationExpression = dialog.getField('./validationsvalidationExpression');
	var dateConstraintType = dialog.getField('./dateConstraintType').getRawValue();	
	if (elementType.getRawValue() == "Date") {
		if(dateConstraintType==""||dateConstraintType==null||dateConstraintType=="undefined" ||dateConstraintType=="Regular expression"){
			validationsvalidationExpression.show();
		} else {
			validationsvalidationExpression.hide();
		}
	}
}

function formElementV2telephoneNumberConstraintTypeOnLoad(box, value) {
	parRefs = box.ownerCt.findByType('selection');
	var dialog = box.ownerCt.findParentByType('dialog');
	var elementType = dialog.getField('./elementType');
	var telephoneNumMaximumLength = dialog.getField('./telephoneNumMaximumLength');
	var validationsvalidationExpression = dialog.getField('./validationsvalidationExpression');
	var telephoneNumberConstraintType = dialog.getField('./telephoneNumberConstraintType').getRawValue();
	if (elementType.getRawValue() == "Telephone number") {
		if(telephoneNumberConstraintType==""||telephoneNumberConstraintType==null||telephoneNumberConstraintType=="undefined" ||telephoneNumberConstraintType=="Regular expression"){
			validationsvalidationExpression.show();
		} else {
			validationsvalidationExpression.hide();
		}
		if(telephoneNumberConstraintType==""||telephoneNumberConstraintType==null||telephoneNumberConstraintType=="undefined" ||telephoneNumberConstraintType=="Numbers only"){
			telephoneNumMaximumLength.show();
		} else {
			telephoneNumMaximumLength.hide();
		}
	}
}

function formElementV2validationsMandatoryFieldOnClick(box,value) { 
 parRefs = box.ownerCt.findByType('selection');
 var dialog = box.ownerCt.findParentByType('dialog');
    var validationsMandatoryField = dialog.getField('./validationsMandatoryField');
	var validationsRequiredErrorMessage = dialog.getField('./validationsRequiredErrorMessage');
   if(validationsMandatoryField.getRawValue() == 'true' || validationsMandatoryField.getRawValue() == 'on'){
	validationsRequiredErrorMessage.show();	
   }else{
	validationsRequiredErrorMessage.hide();
   }
}

function formElementV2EmailMandatoryFieldOnClick(box,value) { 
 parRefs = box.ownerCt.findByType('selection');
 var dialog = box.ownerCt.findParentByType('dialog');
    var emailMandatoryField = dialog.getField('./emailMandatoryField');
	var emailRequiredErrorMessage = dialog.getField('./emailRequiredErrorMessage');
   if(emailMandatoryField.getRawValue() == 'true' || emailMandatoryField.getRawValue() == 'on'){
	emailRequiredErrorMessage.show();	
   }else{
	emailRequiredErrorMessage.hide();
   }
}

function formElementV2TextFieldMandatoryFieldOnClick(box,value) { 
 parRefs = box.ownerCt.findByType('selection');
 var dialog = box.ownerCt.findParentByType('dialog');
    var textFieldMandatoryField = dialog.getField('./textFieldMandatoryField');
	var textFieldRequiredErrorMessage = dialog.getField('./textFieldRequiredErrorMessage');
   if(textFieldMandatoryField.getRawValue() == 'true' || textFieldMandatoryField.getRawValue() == 'on'){
	textFieldRequiredErrorMessage.show();	
   }else{
	textFieldRequiredErrorMessage.hide();
   }
}

function formElementV2NumberMandatoryFieldOnClick(box,value) { 
 parRefs = box.ownerCt.findByType('selection');
 var dialog = box.ownerCt.findParentByType('dialog');
    var numberMandatoryField = dialog.getField('./numberMandatoryField');
	var numberRequiredErrorMessage = dialog.getField('./numberRequiredErrorMessage');
   if(numberMandatoryField.getRawValue() == 'true' || numberMandatoryField.getRawValue() == 'on'){
	numberRequiredErrorMessage.show();	
   }else{
	numberRequiredErrorMessage.hide();
   }
}

function formElementV2DateMandatoryFieldOnClick(box,value) { 
 parRefs = box.ownerCt.findByType('selection');
 var dialog = box.ownerCt.findParentByType('dialog');
    var dateMandatoryField = dialog.getField('./dateMandatoryField');
	var dateRequiredErrorMessage = dialog.getField('./dateRequiredErrorMessage');
   if(dateMandatoryField.getRawValue() == 'true' || dateMandatoryField.getRawValue() == 'on'){
	dateRequiredErrorMessage.show();	
   }else{
	dateRequiredErrorMessage.hide();
   }
}

function formElementV2DropdownMandatoryFieldOnClick(box,value) { 
 parRefs = box.ownerCt.findByType('selection');
 var dialog = box.ownerCt.findParentByType('dialog');
    var dropdownMandatoryField = dialog.getField('./dropdownMandatoryField');
	var dropdownRequiredErrorMessage = dialog.getField('./dropdownRequiredErrorMessage');
   if(dropdownMandatoryField.getRawValue() == 'true' || dropdownMandatoryField.getRawValue() == 'on'){
	dropdownRequiredErrorMessage.show();	
   }else{
	dropdownRequiredErrorMessage.hide();
   }
}

function formElementV2FileuploadMandatoryFieldOnClick(box,value) { 
 parRefs = box.ownerCt.findByType('selection');
 var dialog = box.ownerCt.findParentByType('dialog');
    var fileuploadMandatoryField = dialog.getField('./fileuploadMandatoryField');
	var fileuploadRequiredErrorMessage = dialog.getField('./fileuploadRequiredErrorMessage');
   if(fileuploadMandatoryField.getRawValue() == 'true' || fileuploadMandatoryField.getRawValue() == 'on'){
	fileuploadRequiredErrorMessage.show();	
   }else{
	fileuploadRequiredErrorMessage.hide();
   }
}

function formElementV2cardNumberMandatoryFieldOnClick(box,value) { 
 parRefs = box.ownerCt.findByType('selection');
 var dialog = box.ownerCt.findParentByType('dialog');
    var cardNumberMandatoryField = dialog.getField('./cardNumberMandatoryField');
	var cardNumberRequiredErrorMessage = dialog.getField('./cardNumberRequiredErrorMessage');
   if(cardNumberMandatoryField.getRawValue() == 'true' || cardNumberMandatoryField.getRawValue() == 'on'){
	cardNumberRequiredErrorMessage.show();	
   }else{
	cardNumberRequiredErrorMessage.hide();
   }
}

function formElementV2TelephoneNumbermandatoryFieldOnClick(box,value) { 
 parRefs = box.ownerCt.findByType('selection');
 var dialog = box.ownerCt.findParentByType('dialog');
    var telephoneNumbermandatoryField = dialog.getField('./telephoneNumbermandatoryField');
	var telephoneNumbervalidationExpression = dialog.getField('./telephoneNumbervalidationExpression');
   if(telephoneNumbermandatoryField.getRawValue() == 'true' || telephoneNumbermandatoryField.getRawValue() == 'on'){
	telephoneNumbervalidationExpression.show();	
   }else{
	telephoneNumbervalidationExpression.hide();
   }
}
//Recipe Hero on click
function recipeHeroVideoOnSelect(box,value) {
    var dialog = box.ownerCt.findParentByType('dialog');
	var recipeVideo = dialog.getField('./recipeVideo');
    var tabPanel = box.ownerCt.findParentByType('tabpanel');
    
	
	if(recipeVideo.getRawValue() == 'true' || recipeVideo.getRawValue() == 'on'){
          tabPanel.unhideTabStripItem(1);
    }else{
          tabPanel.hideTabStripItem(1);
    }

}

//recipe Dietary Attributes
function recipeDietaryAttributes(box,value) {
    var dialog = box.ownerCt.findParentByType('dialog');
	var overrideGlobalConfig = dialog.getField('./overrideGlobalConfig');
    var dietaryAttributesForDisplay = dialog.getField('./dietaryAttributesForDisplay');
    var vegetarian = dialog.getField('./vegetarian');
	var vegan = dialog.getField('./vegan');
	var nutFree = dialog.getField('./nutFree');
	var pregnancySafe = dialog.getField('./pregnancySafe');
	var glutenFree = dialog.getField('./glutenFree');
	var lactoseFree = dialog.getField('./lactoseFree');
	var rawFood = dialog.getField('./rawFood');
	var eggFree = dialog.getField('./eggFree');
	var dairyFree = dialog.getField('./dairyFree');
	var paleoDiet = dialog.getField('./paleoDiet');

    if(overrideGlobalConfig.getRawValue() == 'true' || overrideGlobalConfig.getRawValue() == 'on'){
    dietaryAttributesForDisplay.show();
    vegetarian.show();
    vegan.show();
    nutFree.show();
    pregnancySafe.show();
    glutenFree.show();
    lactoseFree.show();
	rawFood.show();
	eggFree.show();
	dairyFree.show();
	paleoDiet.show();
	}else{
    dietaryAttributesForDisplay.hide();
    vegetarian.hide();
    vegan.hide();
    nutFree.hide();
    pregnancySafe.hide();
    glutenFree.hide();
    lactoseFree.hide();
	rawFood.hide();
	eggFree.hide();
	dairyFree.hide();
	paleoDiet.hide();
	}

}

//recipe Attributes
function recipeAttributes(box,value) {
    var dialog = box.ownerCt.findParentByType('dialog');
	var overrideGlobalConfig = dialog.getField('./overrideGlobalConfig');
    var attributesForDisplay = dialog.getField('./attributesForDisplay');
    var serves = dialog.getField('./serves');
	var makes = dialog.getField('./makes');
	var cookingTime = dialog.getField('./cookingTime');
	var preparationTime = dialog.getField('./preparationTime');
	var waitingTime = dialog.getField('./waitingTime');
	var marinateTime = dialog.getField('./marinateTime');
	var ovenTime = dialog.getField('./ovenTime');
	var freezeTime = dialog.getField('./freezeTime');
	var chillTime = dialog.getField('./chillTime');
	var brewTime = dialog.getField('./brewTime');
    var totalTime = dialog.getField('./totalTime');

    if(overrideGlobalConfig.getRawValue() == 'true' || overrideGlobalConfig.getRawValue() == 'on'){
    	attributesForDisplay.show();
    	serves.show();
    	makes.show();
    	cookingTime.show();
    	preparationTime.show();
    	waitingTime.show();
    	marinateTime.show();
		ovenTime.show();
		freezeTime.show();
		chillTime.show();
		brewTime.show();
    	totalTime.show();
	}else{
    	attributesForDisplay.hide();
    	serves.hide();
    	makes.hide();
    	cookingTime.hide();
    	preparationTime.hide();
    	waitingTime.hide();
    	marinateTime.hide();
		ovenTime.hide();
		freezeTime.hide();
		chillTime.hide();
		brewTime.hide();
    	totalTime.hide();
	}

}
//social gallery Tab V2
function socialGalleryTabV2AggregationMode(box,value)
{
	var dialog = box.ownerCt.findParentByType('dialog');
	var tabPanel = box.ownerCt.findParentByType('tabpanel');
	var aggregationMode = dialog.getField('./aggregationMode').getValue();
	var socialChannelsToInclude = dialog.getField('./socialChannels');
	if(aggregationMode =='automatic')
	{
		tabPanel.hideTabStripItem(2);
		tabPanel.unhideTabStripItem(1);
		socialChannelsToInclude.show();
	}
	else
	{
		tabPanel.hideTabStripItem(1);
		tabPanel.unhideTabStripItem(2);
		socialChannelsToInclude.hide();
	}
}
// social gallery Tab V2 Link type on select for search tab
function socialGalleryTabV2linkType(box,value) {
			parRefs = box.ownerCt.findByType('selection');
			var dialog = box.ownerCt.findParentByType('dialog');
			var currentObj = dialog.getField("./linkPostsToContentJson");
			var dialogFields = CQ.Util.findFormFields(currentObj);
			var answers = [];
			for (var name in dialogFields)
			{
				if("./linkType"==name)
					{
						for (var i = 0; i < dialogFields[name].length; i++) {
								var compositeField = dialogFields[name][i].getValue();
								if(compositeField =='custom'){
									answers.push(true);
								}else{
									answers.push(false);
								}
							}
					}

				if("./headingTextCustom"==name || "./ctaLabelCustom"==name){
					for (var i = 0; i < dialogFields[name].length; i++) {
						var compositeField = dialogFields[name][i];
						if(answers[i]){
							compositeField.show();
						}else{
							compositeField.hide();
						}
					}
				}
			}
}
// social gallery Tab V2 Link type on select for fixed tab
function socialGalleryTabV2FixedTabLinkType(box,value) {
			parRefs = box.ownerCt.findByType('selection');
			var dialog = box.ownerCt.findParentByType('dialog');
			var currentObj = dialog.getField("./socialPostsToDisplayJson");
			var dialogFields = CQ.Util.findFormFields(currentObj);
			var answers = [];
			for (var name in dialogFields)
			{
				if("./fixedTabLinkType"==name)
					{
						for (var i = 0; i < dialogFields[name].length; i++) {
								var compositeField = dialogFields[name][i].getValue();
								if(compositeField =='custom'){
									answers.push(true);
								}else{
									answers.push(false);
								}
							}
					}

				if("./fixedTabHeadingTextCustom"==name || "./fixedTabCtaLabelCustom"==name){
					for (var i = 0; i < dialogFields[name].length; i++) {
						var compositeField = dialogFields[name][i];
						if(answers[i]){
							compositeField.show();
						}else{
							compositeField.hide();
						}
					}
				}
			}
}
// social gallery V2 Link type on select for fixed tab
function socialGalleryV2linkType(box,value) {
	var dialog = box.ownerCt.findParentByType('dialog');
	var currentObj = dialog.getField("./socialPostsToDisplayJson");
	var dialogFields = CQ.Util.findFormFields(currentObj);
	var answers = [];
	for (var name in dialogFields)
	{
		if("./linkTypeFixed"==name)
			{
				for (var i = 0; i < dialogFields[name].length; i++) {
					var compositeField = dialogFields[name][i].getValue();
					if(compositeField =='custom'){
									answers.push(true);
					}else{
									answers.push(false);
					}
				}
			}

		if("./headingTextCustomFixed"==name || "./ctaLabelCustomFixed"==name){
			for (var i = 0; i < dialogFields[name].length; i++) {
				var compositeField = dialogFields[name][i];
				if(answers[i]){
								compositeField.show();
				}else{
								compositeField.hide();
				}
			}
		}
	}
}
// social gallery V2 Link type on select for Facebook tab
function socialGalleryV2linkTypeFacebook(box,value) {
	var dialog = box.ownerCt.findParentByType('dialog');
	var currentObj = dialog.getField("./linkPostsFacebookJson");
	var dialogFields = CQ.Util.findFormFields(currentObj);
	var answers = [];
	for (var name in dialogFields)
	{
		if("./linkTypeFacebook"==name)
			{
				for (var i = 0; i < dialogFields[name].length; i++) {
					var compositeField = dialogFields[name][i].getValue();
					if(compositeField =='custom'){
									answers.push(true);
					}else{
									answers.push(false);
					}
				}
			}

		if("./headingTextCustomFacebook"==name || "./ctaLabelCustomFacebook"==name){
			for (var i = 0; i < dialogFields[name].length; i++) {
				var compositeField = dialogFields[name][i];
				if(answers[i]){
								compositeField.show();
				}else{
								compositeField.hide();
				}
			}
		}
	}
}
// social gallery V2 Link type on select for Twitter tab
function socialGalleryV2linkTypeTwitter(box,value) {
	var dialog = box.ownerCt.findParentByType('dialog');
	var currentObj = dialog.getField("./linkPostsTwitterJson");
	var dialogFields = CQ.Util.findFormFields(currentObj);
	var answers = [];
	for (var name in dialogFields)
	{
		if("./linkTypeTwitter"==name)
			{
				for (var i = 0; i < dialogFields[name].length; i++) {
					var compositeField = dialogFields[name][i].getValue();
					if(compositeField =='custom'){
									answers.push(true);
					}else{
									answers.push(false);
					}
				}
			}

		if("./headingTextCustomTwitter"==name || "./ctaLabelCustomTwitter"==name){
			for (var i = 0; i < dialogFields[name].length; i++) {
				var compositeField = dialogFields[name][i];
				if(answers[i]){
								compositeField.show();
				}else{
								compositeField.hide();
				}
			}
		}
	}
}
// social gallery V2 Link type on select for Instagram tab
function socialGalleryV2linkTypeInstagram(box,value) {
	var dialog = box.ownerCt.findParentByType('dialog');
	var currentObj = dialog.getField("./linkPostsInstagramJson");
	var dialogFields = CQ.Util.findFormFields(currentObj);
	var answers = [];
	for (var name in dialogFields)
	{
		if("./linkTypeInstagram"==name)
			{
				for (var i = 0; i < dialogFields[name].length; i++) {
					var compositeField = dialogFields[name][i].getValue();
					if(compositeField =='custom'){
									answers.push(true);
					}else{
									answers.push(false);
					}
				}
			}

		if("./headingTextCustomInstagram"==name || "./ctaLabelCustomInstagram"==name){
			for (var i = 0; i < dialogFields[name].length; i++) {
				var compositeField = dialogFields[name][i];
				if(answers[i]){
								compositeField.show();
				}else{
								compositeField.hide();
				}
			}
		}
	}
}

function recipelistingOnLoad(box,value) {

	parRefs = box.ownerCt.findByType('selection');
  var dialog = box.ownerCt.findParentByType('dialog');
  var componentUsageContext = dialog.getField('./componentUsageContext').getValue();
  var tabPanel = box.ownerCt.findParentByType('tabpanel');
  if(componentUsageContext == 'categoryOrLandingPages' || !componentUsageContext){
        tabPanel.unhideTabStripItem(1);
        tabPanel.hideTabStripItem(2);
		tabPanel.hideTabStripItem(3);
  }else if(componentUsageContext == 'productDetailsPage' || !componentUsageContext){
        tabPanel.unhideTabStripItem(2);
        tabPanel.hideTabStripItem(1);
		tabPanel.hideTabStripItem(3);
  }else if(componentUsageContext == 'fixedList' || !componentUsageContext){
		tabPanel.unhideTabStripItem(3);
		tabPanel.hideTabStripItem(1);
		tabPanel.hideTabStripItem(2);
  }else{
		tabPanel.hideTabStripItem(1);
		tabPanel.hideTabStripItem(2);
		tabPanel.hideTabStripItem(3);
  }
}

function showRecipeHideIncludeInDisplay(comp, val, isChecked){
    var panel = comp.findParentByType("panel"); //find the parent panel container
    var includeInDisplaySection = panel.getComponent("includeInDisplaySection"); //find the component with itemId includeInDisplaySection

    /*hide or show component based on checked value */
    isChecked ? includeInDisplaySection.show() : includeInDisplaySection.hide(); 
}

function recipeListingOnSubmit(dialog) {
  var count = 0;
	var recipeList = dialog.getField('./sections'); 
   var componentUsageContext = dialog.getField('./componentUsageContext').getValue();
	  if(componentUsageContext == 'fixedList' || !componentUsageContext){
		var dialogFields = CQ.Util.findFormFields(recipeList);
		for (var name in dialogFields) {

			if ("./recipe" == name) {
				for (var i = 0; i < dialogFields[name].length; i++) {
					var compositeField = dialogFields[name][i];
                    compositeField.allowBlank = false;
					count = i + 1;
				}
          }
			if (count == 0 || count < 0) {
				CQ.Ext.Msg.show({
					title: 'Confirm',
					msg: 'Please configure at least one product path',
					icon: CQ.Ext.MessageBox.ERROR,
					buttons: CQ.Ext.Msg.OK
				});
				return false;
			} 
		}

    }

}

function diagnosticToolQuestionOnLoad(dialog) {
    var currentObj = dialog.getField("./responseOptionJson");
    var dialogFields = CQ.Util.findFormFields(currentObj);
    var responseImageNotRequired =  dialog.getField("./responseImageNotRequired").getValue();
    for (var name in dialogFields) {
        if("./foregroundImageResponse"==name){
            if(responseImageNotRequired == 'true'){
                for (var i = 0; i < dialogFields[name].length; i++) {
                    var compositeField = dialogFields[name][i];
                    compositeField.hide();
                }                
            }
            else
            {  
                for (var i = 0; i < dialogFields[name].length; i++) {
                    var compositeField = dialogFields[name][i];
                    compositeField.show();
                }
            }
        }
        if("./foregroundImageAltTextResponse"==name){            
            if(responseImageNotRequired == 'true'){
                for (var i = 0; i < dialogFields[name].length; i++) {
                    var compositeField = dialogFields[name][i];
                    compositeField.hide();
                }
            }
            else
            {
                for (var i = 0; i < dialogFields[name].length; i++) {
                    var compositeField = dialogFields[name][i];
                    compositeField.show();
                }                
            }            
        }
        if("./responseId"==name){
            for (var i = 0; i < dialogFields[name].length; i++) {
                var compositeField = dialogFields[name][i];
                compositeField.setValue(i+1);
            }                
            
        }
    }  

    var questionIdObj = dialog.getField("./questionId");
    var questionId = questionIdObj.getValue();
    if (questionId == 'undefined' || questionId == '') {
        var obj = {};
		var formActionElements;
		var entityValue = "diagnostictool";
        var brandData = getBrandDataGeneric(dialog, formActionElements, obj, entityValue);
        $.ajax({
            type: 'GET',
            url: '/bin/ts/private/uniqueid',
            data: brandData,
            success: function(data) {
                questionIdObj.setValue(data.responseData.id);
            }
        });        
    }   
}

function getBrandDataGeneric(dialog, formActionElements, obj, entityValue) {
    var formActionUrl = dialog.form.url;
    formActionElements = formActionUrl.split("/");
    obj["brand"] = formActionElements[2];
    obj["entity"] = entityValue;
    obj["locale"] = formActionElements[3];
    return obj;
}

function diagnosticToolQuestionOnChange(comp, val, isChecked){
diagnosticToolQuestionOnLoad(comp.ownerCt.findParentByType('dialog'));
}

function diagnosticToolQuestionTagOnLoad(dialog) {
    var currentObj = dialog.getField("./responseOptionJson");
    var dialogFields = CQ.Util.findFormFields(currentObj);
    var tagNotRequired =  dialog.getField("./tagNotRequired").getValue();
    for (var name in dialogFields) {
        if("./matchingTag"==name){
            if(tagNotRequired == 'true'){
                for (var i = 0; i < dialogFields[name].length; i++) {
                    var compositeField = dialogFields[name][i];
                    compositeField.hide();
                }                
            }
            else
            {                
                for (var i = 0; i < dialogFields[name].length; i++) {
                    var compositeField = dialogFields[name][i];
                    compositeField.show();
                }
            }
        }
    }
}

function diagnosticToolQuestionTagOnChange(comp, val, isChecked){
diagnosticToolQuestionTagOnLoad(comp.ownerCt.findParentByType('dialog'));
}

function diagnosticToolQuestionBeforeSubmit(dialog) {
    var currentObj = dialog.getField("./responseOptionJson");
    var dialogFields = CQ.Util.findFormFields(currentObj);
    var responseImageNotRequired =  dialog.getField("./responseImageNotRequired").getValue();
    var tagNotRequired =  dialog.getField("./tagNotRequired").getValue();
    var image = false;
    var altimage = false;
    for (var name in dialogFields) {
        if("./foregroundImageResponse"==name){
            if(responseImageNotRequired != 'true'){
                for (var i = 0; i < dialogFields[name].length; i++) {
                    var compositeField = dialogFields[name][i];
                    compositeField.show();
                    if(compositeField.getRawValue() == "")
                    {
                        image = true;
                    }
                }                
            }
        }
        if("./foregroundImageAltTextResponse"==name){            
            if(responseImageNotRequired != 'true'){
                for (var i = 0; i < dialogFields[name].length; i++) {
                    var compositeField = dialogFields[name][i];
                    compositeField.show();
                    if(compositeField.getRawValue() == "")
                    {
                        altimage = true;
                    }
                }
            }          
        }
        if(image == true) {
            CQ.Ext.Msg.show({
                title: 'Confirm',
                msg: 'Please enter the image path',
                icon: CQ.Ext.MessageBox.ERROR,
                buttons: CQ.Ext.Msg.OK
            });
            return false;
        }
        if(altimage == true) {
            CQ.Ext.Msg.show({
                title: 'Confirm',
                msg: 'Please enter the alternate text',
                icon: CQ.Ext.MessageBox.ERROR,
                buttons: CQ.Ext.Msg.OK
            });
            return false;
        }
    }
}

function diagnosticToolResultSectionOnLoad(dialog){
	DIAGNOSTIC_TOOL_RESULT_SECTION_DIALOG = dialog;
    var questionConf = DIAGNOSTIC_TOOL_RESULT_SECTION_DIALOG.getField("./questionsConfiguration");
    if(questionConf.getValue() != ""){
        var dialogFields = CQ.Util.findFormFields(questionConf);
		var queryQ = "?";
		var queryA = [];
        var query = "?";
		var formActionUrl = DIAGNOSTIC_TOOL_RESULT_SECTION_DIALOG.form.url;
		for (var name in dialogFields) {
			if("./diagnosticToolPath"==name){
				for (var i = 0; i < dialogFields[name].length; i++) {
					var compositeField = dialogFields[name][i];
					queryQ = queryQ + "path=" + compositeField.getRawValue() + "&";
				}
			}
		}

		$.ajax({
			url: formActionUrl + ".getQuestionData" + queryQ,
			method: "GET",
			async: false
			
		}).done(function(data) {
			QUESTION_LIST_JSONDATA = JSON.parse(data);
		});
		var personalisedTextRulesCurrentObj = DIAGNOSTIC_TOOL_RESULT_SECTION_DIALOG.getField("./personalisedTextRules");
		var personalisedTextRulesDialogFields = CQ.Util.findFormFields(personalisedTextRulesCurrentObj);
		for (var name in personalisedTextRulesDialogFields) {
			if("./personalisedQuestions"==name){
				for (var i = 0; i < personalisedTextRulesDialogFields[name].length; i++) {
					var personalisedTextRulesCompositeField = personalisedTextRulesDialogFields[name][i];
					var personalisedQuestionsDialogFields = CQ.Util.findFormFields(personalisedTextRulesCompositeField);
					for (var name in personalisedQuestionsDialogFields) {
						if("./question"==name){
							for (var j = 0; j < personalisedQuestionsDialogFields[name].length; j++) {
								var personalisedQuestionsCompositeField = personalisedQuestionsDialogFields[name][j];
								personalisedQuestionsCompositeField.setOptions(QUESTION_LIST_JSONDATA);
								var currentDiagnosticToolResultJSON = CQ.HTTP.get(CQ.HTTP.externalize(formActionUrl + ".json"));
								var selectedvalue = currentDiagnosticToolResultJSON.body;
								var json = JSON.parse(selectedvalue);
								var obj = json["question"];
								personalisedQuestionsCompositeField.setValue(obj[j]);

								var selectedValue = personalisedQuestionsCompositeField.getValue();
								for(var k=0;k<QUESTION_LIST_JSONDATA.length;k++){ 
									if(selectedValue == QUESTION_LIST_JSONDATA[k].value){
										query = query + "path=" + QUESTION_LIST_JSONDATA[k].path + "&";
                                        queryA.push(query);
                                        query = "?";
										break;
									}
								}
							}
						}
						else if("./answer"==name){
                        for (var j = 0; j < personalisedQuestionsDialogFields[name].length; j++) {
                            var personalisedAnswersCompositeField = personalisedQuestionsDialogFields[name][j];
                                $.ajax({
                                    url: formActionUrl + ".getAnswerData" + queryA[j],
                                    method: "GET",
                                    async: false
    
                                }).done(function(data) {
                                    ANSWER_LIST_JSONDATA = JSON.parse(data);
                                });
                                personalisedAnswersCompositeField.setOptions(ANSWER_LIST_JSONDATA);
                            var currentDiagnosticToolResultSectionJSON = CQ.HTTP.get(CQ.HTTP.externalize(formActionUrl + ".json"));
								var selectedvalue = currentDiagnosticToolResultSectionJSON.body;
								var json = JSON.parse(selectedvalue);
								var obj = json["answer"];
                            personalisedAnswersCompositeField.setValue(obj[j]);
                        }
                    } 

					}

                    name="./personalisedQuestions";
				}
			}
		}

    }
}

function addItemOnClick(dialog){
    var personalisedTextRulesCurrentObj = dialog.getField("./personalisedTextRules");
    var personalisedTextRulesDialogFields = CQ.Util.findFormFields(personalisedTextRulesCurrentObj);
    for (var name in personalisedTextRulesDialogFields) {
        if("./personalisedQuestions"==name){
			for (var i = 0; i < personalisedTextRulesDialogFields[name].length; i++) {
                var personalisedTextRulesCompositeField = personalisedTextRulesDialogFields[name][i];
  				var personalisedQuestionsDialogFields = CQ.Util.findFormFields(personalisedTextRulesCompositeField);
				for (var name in personalisedQuestionsDialogFields) {
					if("./question"==name){
                        for (var j = 0; j < personalisedQuestionsDialogFields[name].length; j++) {
							var personalisedQuestionsCompositeField = personalisedQuestionsDialogFields[name][j];
                            personalisedQuestionsCompositeField.setOptions(QUESTION_LIST_JSONDATA);
                        }
                    }

                }
                name = "./personalisedQuestions";
            }
        }
    }
}

function getAnswersList(box,value){
	var dialog = box.ownerCt.findParentByType('dialog');
    var formActionUrl = dialog.form.url;
var personalisedTextRulesCurrentObj = dialog.getField("./personalisedTextRules");
var personalisedTextRulesDialogFields = CQ.Util.findFormFields(personalisedTextRulesCurrentObj);
    var query = "?";
    var queryAns = [];
    for (var name in personalisedTextRulesDialogFields) {
        if("./personalisedQuestions"==name){
            for (var i = 0; i < personalisedTextRulesDialogFields[name].length; i++) {
                var personalisedTextRulesCompositeField = personalisedTextRulesDialogFields[name][i];
                var personalisedQuestionsDialogFields = CQ.Util.findFormFields(personalisedTextRulesCompositeField);
                for (var name in personalisedQuestionsDialogFields) {
                    if("./question"==name){
                        for (var j = 0; j < personalisedQuestionsDialogFields[name].length; j++) {
                            var personalisedQuestionsCompositeField = personalisedQuestionsDialogFields[name][j];
                            var selectedValue = personalisedQuestionsCompositeField.getValue();
                            for(var k=0;k<QUESTION_LIST_JSONDATA.length;k++){ 
                                if(selectedValue == QUESTION_LIST_JSONDATA[k].value){
                                    query = query + "path=" + QUESTION_LIST_JSONDATA[k].path + "&";
                                     queryAns.push(query);
                                     query = "?";
                                     break;
                                }
                            }
                        }
                    }
                    
                    else if("./answer"==name){
                        for (var j = 0; j < personalisedQuestionsDialogFields[name].length; j++) {
                            var personalisedAnswersCompositeField = personalisedQuestionsDialogFields[name][j];
                                $.ajax({
                                    url: formActionUrl + ".getAnswerData" + queryAns[j],
                                    method: "GET",
                                    async: false
    
                                }).done(function(data) {
                                    ANSWER_LIST_JSONDATA = JSON.parse(data);
                                });
                                personalisedAnswersCompositeField.setOptions(ANSWER_LIST_JSONDATA);
                        }
                    }                    
                }
                name="./personalisedQuestions";
                queryAns = [];

            }
        }
    }
}

function diagnosticToolResultSectionBeforeSubmit(dialog) {
    var flagProducts = true;
    var flagArticles = true;
    var flagNullFields = true;
    flagProducts = validateNumberOfProductsToDisplay(dialog);
	flagArticles = validateNumberOfArticlesToDisplay(dialog);
    if(flagProducts == undefined){
        flagProducts = true;
    }
    if(flagArticles == undefined){
        flagArticles = true;
    }

    var personalisedTextRulesCurrentObj = dialog.getField("./personalisedTextRules");
    var personalisedTextRulesDialogFields = CQ.Util.findFormFields(personalisedTextRulesCurrentObj);
    for (var name in personalisedTextRulesDialogFields) {
        if("./personalisedQuestions"==name){
			for (var i = 0; i < personalisedTextRulesDialogFields[name].length; i++) {
                var personalisedTextRulesCompositeField = personalisedTextRulesDialogFields[name][i];
  				var personalisedQuestionsDialogFields = CQ.Util.findFormFields(personalisedTextRulesCompositeField);
                for (var name in personalisedQuestionsDialogFields) {
                    if("./question"==name){
                        for (var j = 0; j < personalisedQuestionsDialogFields[name].length; j++) {
                            var personalisedQuestionsCompositeField = personalisedQuestionsDialogFields[name][j];
                            if(personalisedQuestionsCompositeField.getValue() == ''){
                                CQ.Ext.Msg.show({
                                    title: 'Confirm',
                                    msg: 'Please select question',
                                    icon: CQ.Ext.MessageBox.ERROR,
                                    buttons: CQ.Ext.Msg.OK
                                });
                                flagNullFields = false;
                                break;
                                
                            }
                        }
                    }
                    else if("./answer"==name){
                        for (var j = 0; j < personalisedQuestionsDialogFields[name].length; j++) {
                            var personalisedAnswersCompositeField = personalisedQuestionsDialogFields[name][j];
                            if(personalisedAnswersCompositeField.getValue() == ''){
                                CQ.Ext.Msg.show({
                                    title: 'Confirm',
                                    msg: 'Please select answer',
                                    icon: CQ.Ext.MessageBox.ERROR,
                                    buttons: CQ.Ext.Msg.OK
                                });
                                flagNullFields = false;
                                break;
                                
                            }
                        }
                    }

                }
                name = "./personalisedQuestions";
            }
        }
    }

    return (flagProducts && flagArticles && flagNullFields);
}

function validateNumberOfProductsToDisplay(dialog){
	var currentObj = dialog.getField("./products");
    var dialogFields = CQ.Util.findFormFields(currentObj);
    for (var name in dialogFields) {
        if("./numberOfProductsToDisplay"==name){
            for (var i = 0; i < dialogFields[name].length; i++) {
                var compositeField = dialogFields[name][i];
                if(compositeField.getRawValue() < 1){
                    CQ.Ext.Msg.show({
                        title: 'Confirm',
                        msg: 'A positive value must be provided for number of products to display',
                        icon: CQ.Ext.MessageBox.ERROR,
                        buttons: CQ.Ext.Msg.OK
                    });
                    return false;
                }                
            }            
        }
    }
}

function validateNumberOfArticlesToDisplay(dialog){
	var currentObj = dialog.getField("./articles");
    var dialogFields = CQ.Util.findFormFields(currentObj);
    for (var name in dialogFields) {
        if("./numberOfArticlesToDisplay"==name){
            for (var i = 0; i < dialogFields[name].length; i++) {
                var compositeField = dialogFields[name][i];
                if(compositeField.getRawValue() < 1){
                    CQ.Ext.Msg.show({
                        title: 'Confirm',
                        msg: 'A positive value must be provided for number of articles to display',
                        icon: CQ.Ext.MessageBox.ERROR,
                        buttons: CQ.Ext.Msg.OK
                    });
                    return false;
                }                
            }            
        }
    }
}

function save(){
    var currentObj = DIAGNOSTIC_TOOL_RESULT_SECTION_DIALOG.getField("./questionsConfiguration");
    var dialogFields = CQ.Util.findFormFields(currentObj);
    var query = "?";
    var formActionUrl = DIAGNOSTIC_TOOL_RESULT_SECTION_DIALOG.form.url;
    for (var name in dialogFields) {
        if("./diagnosticToolPath"==name){
            for (var i = 0; i < dialogFields[name].length; i++) {
                var compositeField = dialogFields[name][i];
                if(compositeField.getRawValue() == ""){
                    CQ.Ext.Msg.show({
                        title: 'Confirm',
                        msg: 'Please configure the required question',
                        icon: CQ.Ext.MessageBox.ERROR,
                        buttons: CQ.Ext.Msg.OK
                    });
                    return false;
                } 
                else{
                    query = query + "path=" + compositeField.getRawValue() + "&";
                }
            }
        }
    }
    
    $.ajax({
        url: formActionUrl + ".getQuestionData" + query,
        method: "GET",
        async: false
        
    }).done(function(data) {
        QUESTION_LIST_JSONDATA = JSON.parse(data);
    });
    CQ.Ext.Msg.show({
                        title: 'Confirm',
                        msg: 'Changes Saved',
                        icon: CQ.Ext.MessageBox.MESSAGE,
                        buttons: CQ.Ext.Msg.OK
                    });
    var personalisedTextRulesCurrentObj = DIAGNOSTIC_TOOL_RESULT_SECTION_DIALOG.getField("./personalisedTextRules");
    var personalisedTextRulesDialogFields = CQ.Util.findFormFields(personalisedTextRulesCurrentObj);
    for (var name in personalisedTextRulesDialogFields) {
        if("./personalisedQuestions"==name){
			for (var i = 0; i < personalisedTextRulesDialogFields[name].length; i++) {
                var personalisedTextRulesCompositeField = personalisedTextRulesDialogFields[name][i];
  				var personalisedQuestionsDialogFields = CQ.Util.findFormFields(personalisedTextRulesCompositeField);
				for (var name in personalisedQuestionsDialogFields) {
					if("./question"==name){
                        for (var j = 0; j < personalisedQuestionsDialogFields[name].length; j++) {
							var personalisedQuestionsCompositeField = personalisedQuestionsDialogFields[name][j];
                            personalisedQuestionsCompositeField.setOptions(QUESTION_LIST_JSONDATA);
                        }
                    }

                }
                name = "./personalisedQuestions";
            }
        }
    }
    return true;
}

function diagnosticToolOnLoad(dialog) {
    DIAGNOSTIC_TOOL_DIALOG = dialog;
    var currentObj = DIAGNOSTIC_TOOL_DIALOG.getField("./resultSections");
    var dialogFields = CQ.Util.findFormFields(currentObj);
    var personalisedTextFormJSON = [];
    var query = "?";
    var formActionUrl = DIAGNOSTIC_TOOL_DIALOG.form.url;
	if(currentObj.getValue() != ""){
        for (var name in dialogFields) {
            if("./resultSectionPath"==name){
                for (var i = 0; i < dialogFields[name].length; i++) {
                    var compositeField = dialogFields[name][i];
                    query = query + "path=" + compositeField.getRawValue() + "&";
                }
            }
        }
        $.ajax({
            url: formActionUrl + ".getResultSectionData" + query,
            method: "GET",
            async: false

        }).done(function(data) {
            personalisedTextFormJSON = JSON.parse(data);
        });
        var personalisedTextFromField = DIAGNOSTIC_TOOL_DIALOG.getField("./personalisedTextFrom");
        personalisedTextFromField.setOptions(personalisedTextFormJSON);
		var currentDiagnosticToolJSON = CQ.HTTP.get(CQ.HTTP.externalize(formActionUrl + ".json"));
		var selectedvalue = currentDiagnosticToolJSON.body;
        var json = JSON.parse(selectedvalue);
        personalisedTextFromField.setValue(json["personalisedTextFrom"]);
    }
}

function diagnosticToolBeforeSubmit(dialog) {
    var currentObj = DIAGNOSTIC_TOOL_DIALOG.getField("./resultSections");
	var values = currentObj.getValue();
    if(values == ""){
         CQ.Ext.Msg.show({
                        title: 'Confirm',
                        msg: 'Please configure the atleast one result section',
                        icon: CQ.Ext.MessageBox.ERROR,
                        buttons: CQ.Ext.Msg.OK
                    });
                    return false;
    }

}

function populatePersonalisedTextForm(){
    var currentObj = DIAGNOSTIC_TOOL_DIALOG.getField("./resultSections");
    var dialogFields = CQ.Util.findFormFields(currentObj);
    var personalisedTextFormJSON = [];
    var query = "?";
    var formActionUrl = DIAGNOSTIC_TOOL_DIALOG.form.url;
    if(currentObj.getValue() == ""){
        CQ.Ext.Msg.show({
                        title: 'Confirm',
                        msg: 'Please configure the atleast one result section',
                        icon: CQ.Ext.MessageBox.ERROR,
                        buttons: CQ.Ext.Msg.OK
                    });
                    return false;
    }
    else{
        for (var name in dialogFields) {
            if("./resultSectionPath"==name){
                for (var i = 0; i < dialogFields[name].length; i++) {
                    var compositeField = dialogFields[name][i];
                    query = query + "path=" + compositeField.getRawValue() + "&";
                }
            }
        }
        $.ajax({
            url: formActionUrl + ".getResultSectionData" + query,
            method: "GET",
            async: false

        }).done(function(data) {
            personalisedTextFormJSON = JSON.parse(data);
        });
        CQ.Ext.Msg.show({
                        title: 'Confirm',
                        msg: 'Changes Saved',
                        icon: CQ.Ext.MessageBox.MESSAGE,
                        buttons: CQ.Ext.Msg.OK
                    });
    }

    var personalisedTextFrom = DIAGNOSTIC_TOOL_DIALOG.getField("./personalisedTextFrom");
	personalisedTextFrom.setOptions(personalisedTextFormJSON);
	return true;

}

function overwriteReadTimeOnLoad(box,value){
    var dialog = box.ownerCt.findParentByType('dialog');
	var overwriteReadTime = dialog.getField("./overwriteReadTime").getValue();
	var pageReadTime = dialog.getField("./pageReadTime");
	if(overwriteReadTime == 'true'){
		pageReadTime.enable();
	}else{
		pageReadTime.disable();
	}
}

function resourceListingV2OnLoad(box,value) {

	var dialog = box.ownerCt.findParentByType('dialog');


	var filePath1 = document.getElementsByName("./filePath1");
    var filePath2 = document.getElementsByName("./filePath2");
	var filePath3 = document.getElementsByName("./filePath3");
	var filePath4 = document.getElementsByName("./filePath4");
	var filePath5 = document.getElementsByName("./filePath5");
	var filePath6 = document.getElementsByName("./filePath6");
	var filePath7 = document.getElementsByName("./filePath7");
	var filePath8 = document.getElementsByName("./filePath8");
	var filePath9 = document.getElementsByName("./filePath9");
	var filePath10 = document.getElementsByName("./filePath10");


	var fileNumber = document.getElementsByName("./fileNumber");
	var dialogFields = CQ.Util.findFormFields(fileNumber);                                             

   for(var name in dialogFields) {                                            
    if (name == '1' || !name) {
		filePath1.show();
		filePath2.hide();
		filePath3.hide();
		filePath4.hide();
		filePath5.hide();
		filePath6.hide();
		filePath7.hide();
		filePath8.hide();
		filePath9.hide();
		filePath10.hide();
	} else if(fileNumber == '2'){
		filePath1.show();
		filePath2.show();
		filePath3.hide();
		filePath4.hide();
		filePath5.hide();
		filePath6.hide();
		filePath7.hide();
		filePath8.hide();
		filePath9.hide();
		filePath10.hide();
	}else if(fileNumber == '3'){
		filePath1.show();
		filePath2.show();
		filePath3.show();
		filePath4.hide();
		filePath5.hide();
		filePath6.hide();
		filePath7.hide();
		filePath8.hide();
		filePath9.hide();
		filePath10.hide();
	}else if(fileNumber == '4'){
		filePath1.show();
		filePath2.show();
		filePath3.show();
		filePath4.show();
		filePath5.hide();
		filePath6.hide();
		filePath7.hide();
		filePath8.hide();
		filePath9.hide();
		filePath10.hide();
	}else if(fileNumber == '5'){
		filePath1.show();
		filePath2.show();
		filePath3.show();
		filePath4.show();
		filePath5.show();
		filePath6.hide();
		filePath7.hide();
		filePath8.hide();
		filePath9.hide();
		filePath10.hide();
	}else if(fileNumber == '6'){
		filePath1.show();
		filePath2.show();
		filePath3.show();
		filePath4.show();
		filePath5.show();
		filePath6.show();
		filePath7.hide();
		filePath8.hide();
		filePath9.hide();
		filePath10.hide();
	}else if(fileNumber == '7'){
		filePath1.show();
		filePath2.show();
		filePath3.show();
		filePath4.show();
		filePath5.show();
		filePath6.show();
		filePath7.show();
		filePath8.hide();
		filePath9.hide();
		filePath10.hide();
	}else if(fileNumber == '8'){
		filePath1.show();
		filePath2.show();
		filePath3.show();
		filePath4.show();
		filePath5.show();
		filePath6.show();
		filePath7.show();
		filePath8.show();
		filePath9.hide();
		filePath10.hide();
	}else if(fileNumber == '9'){
		filePath1.show();
		filePath2.show();
		filePath3.show();
		filePath4.show();
		filePath5.show();
		filePath6.show();
		filePath7.show();
		filePath8.show();
		filePath9.show();
		filePath10.hide();
	}else if(fileNumber == '10'){
		filePath1.show();
		filePath2.show();
		filePath3.show();
		filePath4.show();
		filePath5.show();
		filePath6.show();
		filePath7.show();
		filePath8.show();
		filePath9.show();
		filePath10.show();
	}
   }
}
function isSecurePageOnLoad(box,value){
    var dialog = box.ownerCt.findParentByType('dialog');
	var isSecurePage = dialog.getField("./isSecurePage").getValue();
	var redirectUrl = dialog.getField("./redirectUrl");
	if(isSecurePage == 'true'){
		redirectUrl.allowBlank = false;
	}else{
		redirectUrl.allowBlank = true;
	}
}

function includeVideoOnLoad(box,value){
    var dialog = box.ownerCt.findParentByType('dialog');
	var includeVideo = dialog.getField("./includeVideo").getValue();
	var videoId = dialog.getField("./videoId");
	var tabPanel = box.findParentByType('tabpanel');
	tabPanel.hideTabStripItem(4);
	videoId.allowBlank = true;
	if(includeVideo == 'true' || includeVideo == 'on'){
		tabPanel.unhideTabStripItem(4);
		videoId.allowBlank = false;
	}else{
		tabPanel.hideTabStripItem(4);
		videoId.allowBlank = true;
	}
}

function getTimeZone() {
    var offset = new Date().getTimezoneOffset(),
        o = Math.abs(offset);
    return (offset < 0 ? "+" : "-") + ("00" + Math.floor(o / 60)).slice(-2) + ":" + ("00" + (o % 60)).slice(-2);
}

function updatedDateTime(dialog, fieldName, id) {
    var startDate = dialog.getField(fieldName);
    var startDateValue = startDate.getValue();
    if(startDateValue && startDateValue.toString().length>0){
    var date = startDateValue;
    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();
    var milliSec = date.getMilliseconds();
    var formattedDate = year + '-' + monthIndex + '-' + day + 'T' + date.toTimeString().substring(0, 8) + '.' + milliSec + getTimeZone();
    if (document.getElementById(id).value.indexOf("\P") >= 0 || document.getElementById(id).value.indexOf("\A") >= 0) {
        document.getElementById(id).value = formattedDate;
    }
    }
}
//live chat V2 chat service
function chatServiceOnSelect(box,value)
{
	var dialog = box.ownerCt.findParentByType('dialog');
	var tabPanel = box.ownerCt.findParentByType('tabpanel');
	var chatService = dialog.getField('./chatService').getValue();
	var otherChatService = dialog.getField('./otherChatService');
	if(chatService =='liveChatOnly')
	{
		tabPanel.hideTabStripItem(2);
		tabPanel.unhideTabStripItem(1);
		otherChatService.hide();
	}
	else if(chatService =='fbMessengerOnly')
	{
		tabPanel.hideTabStripItem(1);
		tabPanel.unhideTabStripItem(2);
		otherChatService.hide();
	}
	else if(chatService =='liveChatWithOtherService')
	{
		tabPanel.unhideTabStripItem(1);
		tabPanel.unhideTabStripItem(2);
		otherChatService.show();
	}
}
//live chat V2 widget type on select
function fbWidgetTypeOnSelect(box,value)
{
	var dialog = box.ownerCt.findParentByType('dialog');
	var widgetType = dialog.getField('./fbWidgetType').getValue();
	var overrideGlobalConfig = dialog.getField('./overrideGlobalConfig').getValue();
	var messengerCtaLabel = dialog.getField('./messengerCtaLabel');
	var buttonSize = dialog.getField('./buttonSize');
	var buttonColour = dialog.getField('./buttonColour');
	var fbAppId = dialog.getField('./fbAppId');
	if(overrideGlobalConfig == "true" || !overrideGlobalConfig){
		if(widgetType =='messageUs')
		{
			messengerCtaLabel.show();
			buttonSize.hide();
			buttonColour.hide();
			fbAppId.hide();
		}
		else if(widgetType =='sendToMessenger')
		{
			messengerCtaLabel.hide();
			buttonSize.show();
			buttonColour.show();
			fbAppId.show();
		}
	}
}

function soundCloudAttributes(box,value) {
    var dialog = box.ownerCt.findParentByType('dialog');
    var overrideGlobalConfig = dialog.getField('./overrideGlobalConfig');
    var soundCloudAttributes = dialog.getField('./soundCloudAttributes');
    var embeddedPlayerType = dialog.getField('./embeddedPlayerType');
    if(overrideGlobalConfig.getRawValue() == 'true' || overrideGlobalConfig.getRawValue() == 'on'){
        soundCloudAttributes.show();
        embeddedPlayerType.show();
    }else{
        soundCloudAttributes.hide();
        embeddedPlayerType.hide();
    }
}

function onAudioSourceChange(box,value){
	var tabPanel = box.ownerCt.findParentByType('tabpanel');
	var dialog = box.ownerCt.findParentByType('dialog');
	var audioSource = dialog.getField("./audioSource").getValue();

	if(audioSource==="soundCloud"){
        tabPanel.unhideTabStripItem(1);
        videoId.allowBlank=true;
	}
	else{
		tabPanel.hideTabStripItem(1);

	}
}

function onSoundcloudWidgetTypeChange(box,value){
    var tabPanel = box.ownerCt.findParentByType('tabpanel');
	var dialog = box.ownerCt.findParentByType('dialog');
	var widgetType = dialog.getField("./widgetType").getValue();
    var startingTrack = dialog.getField("./startingTrack");
	var playlistId = dialog.getField("./playlistId");
    var trackId = dialog.getField("./trackId");
	var userId = dialog.getField("./userId");
	if(widgetType == 'Sound Widget' || !widgetType){
		startingTrack.hide();
		playlistId.hide();
		userId.hide();
        trackId.show();
    }else if(widgetType == 'User Widget'){
		startingTrack.show();
		userId.show();
		playlistId.hide();
        trackId.hide();
    }else {
		startingTrack.show();
		playlistId.show();
        trackId.hide();
		userId.hide();
	}

}
//Coupons Component Coupon Provider | Tab show hide
function couponsProvider(box,value)
{
	var dialog = box.ownerCt.findParentByType('dialog');
	var tabPanel = box.ownerCt.findParentByType('tabpanel');
	var couponProvider = dialog.getField('./couponType').getValue();
	var multipleSelection = dialog.getField('./multipleSelection');
	var multiCtaLabel = dialog.getField('./multiCtaLabel');
	if(couponProvider =='Manual')
	{
		tabPanel.hideTabStripItem(2);
		tabPanel.unhideTabStripItem(1);
		multipleSelection.hide();
		multiCtaLabel.hide();
	}
	else if(couponProvider =='WebBricks')
	{
		tabPanel.hideTabStripItem(1);
		tabPanel.unhideTabStripItem(2);
		multipleSelection.show();
	}else {
		tabPanel.hideTabStripItem(2);
		tabPanel.hideTabStripItem(1);
		multipleSelection.hide();
		multiCtaLabel.hide();
	}
}
//Coupons Component hide expired coupons
function hideExpiredCoupons(comp, val, isChecked){
    var panel = comp.findParentByType("panel");
    var couponExpiryMessage = panel.getComponent("couponExpiryMessageId");
    var dialog = comp.ownerCt.findParentByType('dialog');
	var hideExpiredCoupons = dialog.getField('./hideExpiredCoupons').getValue();
    if(hideExpiredCoupons == "true" || !hideExpiredCoupons){
    	couponExpiryMessage.hide();
    }else{
    	couponExpiryMessage.show();
    }
}
//Coupons Multi CTA Label hide
function allowMultipleSelection(comp, val, isChecked){
    var panel = comp.findParentByType("panel");
    var multiCtaLabel = panel.getComponent("multiCtaLabelId");
    var dialog = comp.ownerCt.findParentByType('dialog');
	var multipleSelection = dialog.getField('./multipleSelection').getValue();
    if(multipleSelection == "true" || !multipleSelection){
    	multiCtaLabel.show();
    }else{
    	multiCtaLabel.hide();
    }
}

function rewardPointsOnLoad(box,value)
{
var tabPanel = box.ownerCt.findParentByType('tabpanel');
var dialog = box.ownerCt.findParentByType('dialog');
var show = dialog.getField("./show").getValue();
var pointsHeadingText = dialog.getField("./pointsHeadingText");
var pointsLabel = dialog.getField("./pointsLabel");
var codeEnteredHeadingText = dialog.getField("./codeEnteredHeadingText");
var codeEnteredLabel = dialog.getField("./codeEnteredLabel");

	if(show == 'pointsAndNumber' || !show){
        pointsHeadingText.show();
        pointsLabel.show();
        codeEnteredHeadingText.show();
        codeEnteredLabel.show();
	}else if(show == 'pointsOnly'){
		pointsHeadingText.show();
        pointsLabel.show();
        codeEnteredHeadingText.hide();
        codeEnteredLabel.hide();
    }else {
        pointsHeadingText.hide();
        pointsLabel.hide();
        codeEnteredHeadingText.show();
        codeEnteredLabel.show();
    }
}function showHideVideoControlsForClassic(box,value){
	var dialog = box.ownerCt.findParentByType('dialog');
	var tabPanel = box.ownerCt.findParentByType('tabpanel');
	var videoSourceType = value;
	if(value instanceof Object){
		videoSourceType = box.value;
	}
	var displayOverlay = dialog.getField('./displayInOverlay');
	var autoPlay = dialog.getField('./autoplay');
	var hideControls = dialog.getField('./hideVideoControls');
	var muteAudio = dialog.getField('./muteVideoAudio');
	var loopVideo = dialog.getField('./loopVideoPlayback');
	var relatedVideos = dialog.getField('./relatedVideoAtEnd');
	if(relatedVideos === undefined){
		relatedVideos = dialog.getField('./showRelatedVideos');
	}
	if (videoSourceType == "youku") {
		displayOverlay.show();
		hideControls.hide();
		muteAudio.hide();
		loopVideo.hide();
		relatedVideos.hide();
		autoPlay.show();
	}
	if (videoSourceType == "iqiyi") {
		displayOverlay.hide();
		hideControls.hide();
		muteAudio.hide();
		loopVideo.hide();
		relatedVideos.hide();
		autoPlay.show();
	}
	if (videoSourceType == "tencent") {
		displayOverlay.hide();
		hideControls.hide();
		muteAudio.hide();
		loopVideo.hide();
		relatedVideos.hide();
		autoPlay.show();
	}
	if (videoSourceType == "youtube") {
		displayOverlay.show();
		hideControls.show();
		muteAudio.show();
		loopVideo.show();
		relatedVideos.show();
		autoPlay.show();
	}
	if (videoSourceType == "tudou") {
		//not defined
		displayOverlay.hide();
		hideControls.hide();
		muteAudio.hide();
		loopVideo.hide();
		relatedVideos.hide();
		autoPlay.hide();
	}
	if (videoSourceType == "sohu") {
		//not defined
		displayOverlay.hide();
		hideControls.hide();
		muteAudio.hide();
		loopVideo.hide();
		relatedVideos.hide();
		autoPlay.hide();
	}
	if (videoSourceType == "vimeo") {
		//not defined
		displayOverlay.hide();
		hideControls.hide();
		muteAudio.hide();
		loopVideo.hide();
		relatedVideos.hide();
		autoPlay.hide();
	}

}