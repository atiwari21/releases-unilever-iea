function socialMediaGalleryGalleryType(box,value)
{
	var dialog = box.ownerCt.findParentByType('dialog');
	var tabPanel = box.ownerCt.findParentByType('tabpanel');
	var galleryType = dialog.getField('./galleryType').getValue();
	var socialChannelsToInclude = dialog.findById("socialChannelsToInclude");



	if(galleryType =='dynamic')
	{

		tabPanel.hideTabStripItem(6);

		socialChannelsToInclude.show();

	}
	else
	{
		tabPanel.unhideTabStripItem(6);

		socialChannelsToInclude.hide();
        tabPanel.hideTabStripItem(5);
        tabPanel.hideTabStripItem(4);
        tabPanel.hideTabStripItem(1);
        tabPanel.hideTabStripItem(3);
        tabPanel.hideTabStripItem(2);

	}
}

//Social Media Gallery Link Type Facebook
function socialMediaGalleryV2linkTypeFacebook(box,value)
{
	showHideVideoControlsForClassic(box,value);
	var dialog = box.ownerCt.findParentByType('dialog');
	var tabPanel = box.ownerCt.findParentByType('tabpanel');
    var linkPostTo=dialog.findById("linkPostTo");
	var fbLinkTypeValue = dialog.findById("fbLinkTypeId").getValue();
    var fbLinkType =dialog.findById("fbLinkTypeId");
    var customLinkHeadingText=dialog.findById("customLinkHeadingText");
    var customCtaLabel=dialog.findById("customCtaLabel"); 
	var customTeaserTitle=dialog.findById("linkPostToCtaId");
	var customTeaserDescription=dialog.findById("customTeaserDescription");
    var customTeaserImage=dialog.findById("customTeaserImage");
    var customTeaserImageAlt=dialog.findById("customTeaserImageAlt");
	var linkPost = dialog.findById("linkPost").getValue();
    var newWindow= dialog.findById("newWindow1");





 if(linkPost=='true')
 {
  if(fbLinkTypeValue =='customInternal')
  {

customTeaserTitle.hide();
        customTeaserDescription.hide();
		customTeaserImage.hide();	
        customTeaserImageAlt.hide();
		customLinkHeadingText.show();
        customCtaLabel.show();
      newWindow.show();
	}


    if(fbLinkTypeValue =='article')
	{

		linkPostTo.show();
        fbLinkType.show();
		customLinkHeadingText.hide();
        customCtaLabel.hide();
		customTeaserTitle.hide();
		customTeaserDescription.hide();
		customTeaserImage.hide();	
        customTeaserImageAlt.hide();
        newWindow.show();


	}
    if(fbLinkTypeValue =='product')
	{

		linkPostTo.show();
        fbLinkType.show();
		customLinkHeadingText.hide();
        customCtaLabel.hide();
		customTeaserTitle.hide();
		customTeaserDescription.hide();
		customTeaserImage.hide();	
        customTeaserImageAlt.hide();
        newWindow.show();


	}
      if(fbLinkTypeValue =='customExternal')
	{
		customLinkHeadingText.show();
        customCtaLabel.show();
		customTeaserTitle.show();
		customTeaserDescription.show();
		customTeaserImage.show();	
        customTeaserImageAlt.show();
        newWindow.show();
   }
}
    else
    {


        fbLinkType.hide();

        linkPostTo.hide();
		customLinkHeadingText.hide();
        customCtaLabel.hide();
		customTeaserTitle.hide();
		customTeaserDescription.hide();
		customTeaserImage.hide();	
        customTeaserImageAlt.hide();
        newWindow.hide();

    }
}

//Social Media Gallery Link Type Twitter
function socialMediaGalleryV2linkTypeTwitter(box,value)
{
	var dialog = box.ownerCt.findParentByType('dialog');
	var tabPanel = box.ownerCt.findParentByType('tabpanel');
    var linkPostTo=dialog.findById("linkPostToTwitter");
	var fbLinkTypeValue = dialog.findById("fbLinkTypeIdTwitter").getValue();
    var fbLinkType =dialog.findById("fbLinkTypeIdTwitter");
    var customLinkHeadingText=dialog.findById("customLinkHeadingTextTwitter");
    var customCtaLabel=dialog.findById("customCtaLabelTwitter"); 
	var customTeaserTitle=dialog.findById("linkPostToCtaIdTwitter");
	var customTeaserDescription=dialog.findById("customTeaserDescriptionTwitter");
    var customTeaserImage=dialog.findById("customTeaserImageTwitter");
    var customTeaserImageAlt=dialog.findById("customTeaserImageAltTwitter");
	var linkPost = dialog.findById("linkPostTwitter").getValue();
    var newWindow= dialog.findById("newWindowTwitter");





 if(linkPost=='true')
 {

  if(fbLinkTypeValue =='customInternal')
  {

customTeaserTitle.hide();
        customTeaserDescription.hide();
		customTeaserImage.hide();	
        customTeaserImageAlt.hide();
		customLinkHeadingText.show();
        customCtaLabel.show();
      newWindow.show();
	}


    if(fbLinkTypeValue =='article')
	{

		linkPostTo.show();
        fbLinkType.show();
		customLinkHeadingText.hide();
        customCtaLabel.hide();
		customTeaserTitle.hide();
		customTeaserDescription.hide();
		customTeaserImage.hide();	
        customTeaserImageAlt.hide();
        newWindow.show();


	}
    if(fbLinkTypeValue =='product')
	{

		linkPostTo.show();
        fbLinkType.show();
		customLinkHeadingText.hide();
        customCtaLabel.hide();
		customTeaserTitle.hide();
		customTeaserDescription.hide();
		customTeaserImage.hide();	
        customTeaserImageAlt.hide();
        newWindow.show();


	}
      if(fbLinkTypeValue =='customExternal')
	{
		customLinkHeadingText.show();
        customCtaLabel.show();
		customTeaserTitle.show();
		customTeaserDescription.show();
		customTeaserImage.show();	
        customTeaserImageAlt.show();
        newWindow.show();
   }
}
    else
    {


        fbLinkType.hide();
        linkPostTo.hide();
		customLinkHeadingText.hide();
        customCtaLabel.hide();
		customTeaserTitle.hide();
		customTeaserDescription.hide();
		customTeaserImage.hide();	
        customTeaserImageAlt.hide();
        newWindow.hide();

    }
}

//Social Media Gallery Link Type Instagram
function socialMediaGalleryV2linkTypeInstagram(box,value)
{
	var dialog = box.ownerCt.findParentByType('dialog');
	var tabPanel = box.ownerCt.findParentByType('tabpanel');
    var linkPostTo=dialog.findById("linkPostToInstagram");
	var fbLinkTypeValue = dialog.findById("fbLinkTypeIdInstagram").getValue();
    var fbLinkType =dialog.findById("fbLinkTypeIdInstagram");
    var customLinkHeadingText=dialog.findById("customLinkHeadingTextInstagram");
    var customCtaLabel=dialog.findById("customCtaLabelInstagram"); 
	var customTeaserTitle=dialog.findById("linkPostToCtaIdInstagram");
	var customTeaserDescription=dialog.findById("customTeaserDescriptionInstagram");
    var customTeaserImage=dialog.findById("customTeaserImageInstagram");
    var customTeaserImageAlt=dialog.findById("customTeaserImageAltInstagram");
	var linkPost = dialog.findById("linkPostInstagram").getValue();
    var newWindow= dialog.findById("newWindowInstagram");





 if(linkPost=='true')
 {
  if(fbLinkTypeValue =='customInternal')
  {

customTeaserTitle.hide();
        customTeaserDescription.hide();
		customTeaserImage.hide();	
        customTeaserImageAlt.hide();
		customLinkHeadingText.show();
        customCtaLabel.show();
      newWindow.show();
	}


    if(fbLinkTypeValue =='article')
	{

		linkPostTo.show();
        fbLinkType.show();
		customLinkHeadingText.hide();
        customCtaLabel.hide();
		customTeaserTitle.hide();
		customTeaserDescription.hide();
		customTeaserImage.hide();	
        customTeaserImageAlt.hide();
        newWindow.show();


	}
    if(fbLinkTypeValue =='product')
	{

		linkPostTo.show();
        fbLinkType.show();
		customLinkHeadingText.hide();
        customCtaLabel.hide();
		customTeaserTitle.hide();
		customTeaserDescription.hide();
		customTeaserImage.hide();	
        customTeaserImageAlt.hide();
        newWindow.show();


	}
      if(fbLinkTypeValue =='customExternal')
	{
		customLinkHeadingText.show();
        customCtaLabel.show();
		customTeaserTitle.show();
		customTeaserDescription.show();
		customTeaserImage.show();	
        customTeaserImageAlt.show();
        newWindow.show();
   }
}
    else
    {

        fbLinkType.hide();

        linkPostTo.hide();
		customLinkHeadingText.hide();
        customCtaLabel.hide();
		customTeaserTitle.hide();
		customTeaserDescription.hide();
		customTeaserImage.hide();	
        customTeaserImageAlt.hide();
        newWindow.hide();

    }
}

//Social Media Gallery Link Type Video
function socialMediaGalleryV2linkTypeVideo(box,value)
{
	var dialog = box.ownerCt.findParentByType('dialog');
	var tabPanel = box.ownerCt.findParentByType('tabpanel');
    var linkPostTo=dialog.findById("linkPostToVideo");
	var fbLinkTypeValue = dialog.findById("fbLinkTypeIdVideo").getValue();
    var fbLinkType =dialog.findById("fbLinkTypeIdVideo");
    var customLinkHeadingText=dialog.findById("customLinkHeadingTextVideo");
    var customCtaLabel=dialog.findById("customCtaLabelVideo"); 
	var customTeaserTitle=dialog.findById("linkPostToCtaIdVideo");
	var customTeaserDescription=dialog.findById("customTeaserDescriptionVideo");
    var customTeaserImage=dialog.findById("customTeaserImageVideo");
    var customTeaserImageAlt=dialog.findById("customTeaserImageAltVideo");
	var linkPost = dialog.findById("linkPostVideo").getValue();
    var newWindow= dialog.findById("newWindowVideo");





 if(linkPost=='true')
 {
  if(fbLinkTypeValue =='customInternal')
  {

customTeaserTitle.hide();
        customTeaserDescription.hide();
		customTeaserImage.hide();	
        customTeaserImageAlt.hide();
		customLinkHeadingText.show();
        customCtaLabel.show();
      newWindow.show();
	}


    if(fbLinkTypeValue =='article')
	{

		linkPostTo.show();
        fbLinkType.show();
		customLinkHeadingText.hide();
        customCtaLabel.hide();
		customTeaserTitle.hide();
		customTeaserDescription.hide();
		customTeaserImage.hide();	
        customTeaserImageAlt.hide();
        newWindow.show();


	}
    if(fbLinkTypeValue =='product')
	{

		linkPostTo.show();
        fbLinkType.show();
		customLinkHeadingText.hide();
        customCtaLabel.hide();
		customTeaserTitle.hide();
		customTeaserDescription.hide();
		customTeaserImage.hide();	
        customTeaserImageAlt.hide();
        newWindow.show();


	}
      if(fbLinkTypeValue =='customExternal')
	{
		customLinkHeadingText.show();
        customCtaLabel.show();
		customTeaserTitle.show();
		customTeaserDescription.show();
		customTeaserImage.show();	
        customTeaserImageAlt.show();
        newWindow.show();
   }
}
    else
    {


        fbLinkType.hide();
        linkPostTo.hide();
		customLinkHeadingText.hide();
        customCtaLabel.hide();
		customTeaserTitle.hide();
		customTeaserDescription.hide();
		customTeaserImage.hide();	
        customTeaserImageAlt.hide();
        newWindow.hide();
    }
}

//Social Media Gallery Link Type tabDam
function socialMediaGalleryV2linkTypetabDam(box,value)
{
	var dialog = box.ownerCt.findParentByType('dialog');
	var tabPanel = box.ownerCt.findParentByType('tabpanel');
    var linkPostTo=dialog.findById("linkPostTotabDam");
	var fbLinkTypeValue = dialog.findById("fbLinkTypeIdtabDam").getValue();
    var fbLinkType =dialog.findById("fbLinkTypeIdtabDam");
    var customLinkHeadingText=dialog.findById("customLinkHeadingTexttabDam");
    var customCtaLabel=dialog.findById("customCtaLabeltabDam"); 
	var customTeaserTitle=dialog.findById("linkPostToCtaIdtabDam");
	var customTeaserDescription=dialog.findById("customTeaserDescriptiontabDam");
    var customTeaserImage=dialog.findById("customTeaserImagetabDam");
    var customTeaserImageAlt=dialog.findById("customTeaserImageAlttabDam");
	var linkPost = dialog.findById("linkPosttabDam").getValue();
    var newWindow= dialog.findById("newWindowtabDam");



 if(linkPost=='true')
 {
  if(fbLinkTypeValue =='customInternal')
  {

customTeaserTitle.hide();
        customTeaserDescription.hide();
		customTeaserImage.hide();	
        customTeaserImageAlt.hide();
		customLinkHeadingText.show();
        customCtaLabel.show();
      newWindow.show();
	}


    if(fbLinkTypeValue =='article')
	{

		linkPostTo.show();
        fbLinkType.show();
		customLinkHeadingText.hide();
        customCtaLabel.hide();
		customTeaserTitle.hide();
		customTeaserDescription.hide();
		customTeaserImage.hide();	
        customTeaserImageAlt.hide();
        newWindow.show();


	}
    if(fbLinkTypeValue =='product')
	{

		linkPostTo.show();
        fbLinkType.show();
		customLinkHeadingText.hide();
        customCtaLabel.hide();
		customTeaserTitle.hide();
		customTeaserDescription.hide();
		customTeaserImage.hide();	
        customTeaserImageAlt.hide();
        newWindow.show();


	}
      if(fbLinkTypeValue =='customExternal')
	{
		customLinkHeadingText.show();
        customCtaLabel.show();
		customTeaserTitle.show();
		customTeaserDescription.show();
		customTeaserImage.show();	
        customTeaserImageAlt.show();
        newWindow.show();
   }
}
    else
    {
        fbLinkType.hide();
        linkPostTo.hide();
		customLinkHeadingText.hide();
        customCtaLabel.hide();
		customTeaserTitle.hide();
		customTeaserDescription.hide();
		customTeaserImage.hide();	
        customTeaserImageAlt.hide();
        newWindow.hide();

    }
}


//Social Media Gallery Custom Channel Image Checkbox
function socialMediaGalleryV2setCustomChannelImage(box,value)
{
var dialog = box.ownerCt.findParentByType('dialog');
	var tabPanel = box.ownerCt.findParentByType('tabpanel');
	var isCustomImage = dialog.findById("isCustomImage").getValue();
	var channelImage = dialog.findById("channelImage");
    var channelImageAlt = dialog.findById("channelImageAlt");

    if(isCustomImage=='true')
    {

		channelImage.show();
		channelImageAlt.show();
    }
    else
    {

		channelImage.hide();
		channelImageAlt.hide();
    }
}

//Social Media Gallery Fixed List
function socialMediaGalleryV2fixedList(box,value)
{

    var dialog = box.ownerCt.findParentByType('dialog');
	var tabPanel = box.ownerCt.findParentByType('tabpanel');
    var channelType = dialog.findById("channelType").getValue();
    var videoId = dialog.findById("videoId");
	var videoSource = dialog.findById("videoSource");
    var tabImage = dialog.findById("tabImage");
	var tabImageAlt = dialog.findById("tabImageAlt");
	var isCustomImage = dialog.findById("isCustomImage");
    var addCustomTitleCaptionValue = dialog.findById("addCustomTitleCaption").getValue();
	var addCustomTitleCaption = dialog.findById("addCustomTitleCaption");
	var fixedPostLongDescription = dialog.findById("fixedPostLongDescription");
    var fixedPostTitle = dialog.findById("fixedPostTitle");
    var fixedPostShortDescription = dialog.findById("fixedPostShortDescription");
	var  channelImage = dialog.findById("channelImage");
	var  channelImageAlt = dialog.findById("channelImageAlt");
    var  accountId = dialog.findById("accountId");
    var  postId = dialog.findById("postId");
	var postCSSClass = dialog.findById("postCSSClass");


    
    if(channelType=='facebook' ||channelType=='twitter' || channelType=='Instagram')

       {
           channelImage.hide();
			channelImageAlt.hide();
           videoId.hide();
			videoSource.hide();
           tabImage.hide();
           tabImageAlt.hide();
           isCustomImage.hide();




       }

    if(channelType=='video')
    {
		videoId.show();
        videoSource.show();
    }
    if(channelType=='tabOrDAM')
    {  
		accountId.hide();
		postId.hide();
		videoId.hide();
        videoSource.hide();
        channelImage.hide();
        channelImageAlt.hide();
		tabImage.show();
        tabImageAlt.show();
        isCustomImage.show();





    }



    if(addCustomTitleCaptionValue=='true')
    {


		fixedPostLongDescription.show();
		fixedPostTitle.show();
		fixedPostShortDescription.show();
    }
    else
    {

        fixedPostLongDescription.hide();
		fixedPostTitle.hide();
		fixedPostShortDescription.hide();
    }

}

//Social Media Gallery Link Type Fixed List
function socialMediaGalleryV2linkTypeFixed(box,value)
{
	var dialog = box.ownerCt.findParentByType('dialog');
	var tabPanel = box.ownerCt.findParentByType('tabpanel');
    var linkPostTo=dialog.findById("linkPostToFixed");
	var fbLinkTypeValue = dialog.findById("fbLinkTypeIdFixed").getValue();
    var fbLinkType =dialog.findById("fbLinkTypeIdFixed");
    var customLinkHeadingText=dialog.findById("customLinkHeadingTextFixed");
    var customCtaLabel=dialog.findById("customCtaLabelFixed"); 
	var customTeaserTitle=dialog.findById("linkPostToCtaIdFixed");
	var customTeaserDescription=dialog.findById("customTeaserDescriptionFixed");
    var customTeaserImage=dialog.findById("customTeaserImageFixed");
    var customTeaserImageAlt=dialog.findById("customTeaserImageAltFixed");
	var linkPost = dialog.findById("linkPostFixed").getValue();
    var newWindow= dialog.findById("newWindowFixed");





 if(linkPost=='true')
 {

  if(fbLinkTypeValue =='customInternal')
  {

customTeaserTitle.hide();
        customTeaserDescription.hide();
		customTeaserImage.hide();	
        customTeaserImageAlt.hide();
		customLinkHeadingText.show();
        customCtaLabel.show();
      newWindow.show();
	}


    if(fbLinkTypeValue =='article')
	{

		linkPostTo.show();
        fbLinkType.show();
		customLinkHeadingText.hide();
        customCtaLabel.hide();
		customTeaserTitle.hide();
		customTeaserDescription.hide();
		customTeaserImage.hide();	
        customTeaserImageAlt.hide();
        newWindow.show();


	}
    if(fbLinkTypeValue =='product')
	{

		linkPostTo.show();
        fbLinkType.show();
		customLinkHeadingText.hide();
        customCtaLabel.hide();
		customTeaserTitle.hide();
		customTeaserDescription.hide();
		customTeaserImage.hide();	
        customTeaserImageAlt.hide();
        newWindow.show();


	}
      if(fbLinkTypeValue =='customExternal')
	{
		customLinkHeadingText.show();
        customCtaLabel.show();
		customTeaserTitle.show();
		customTeaserDescription.show();
		customTeaserImage.show();	
        customTeaserImageAlt.show();
        newWindow.show();
   }
}
    else
    {


        fbLinkType.hide();
        linkPostTo.hide();
		customLinkHeadingText.hide();
        customCtaLabel.hide();
		customTeaserTitle.hide();
		customTeaserDescription.hide();
		customTeaserImage.hide();	
        customTeaserImageAlt.hide();
        newWindow.hide();
    }
}

function SocialshowHideIncludeInDisplay(comp, val, isChecked){
    var panel = comp.findParentByType("panel");
    var visualPostDetails = panel.getComponent("includeInDisplaySection"); 
    var multiBuyConfiguration = panel.getComponent("multiBuyConfiguration");
    var dialog = comp.ownerCt.findParentByType('dialog');
    var overrideGlobal = dialog.getField('./overrideGlobalConfig');
    /*hide or show component based on checked value */
    if(overrideGlobal.getValue() == 'true'){
    	includeInDisplaySection.show();
    	if(multiBuyConfiguration){
    	multiBuyConfiguration.show();
    	}
    }else{
    	includeInDisplaySection.hide();
    	if(multiBuyConfiguration){
    	multiBuyConfiguration.hide();
    	}
    }
}
function socialMediaGalleryV2Visual(box,value)
{
	var dialog = box.ownerCt.findParentByType('dialog');
	var tabPanel = box.ownerCt.findParentByType('tabpanel');
    var overrideGlobalConfig=dialog.findById("overrideGlobalConfig");
    var overrideGlobalConfigValue=dialog.findById("overrideGlobalConfig").getValue();
	var visualPostDetails = dialog.findById("visualPostDetails");
    var linkToSocialChannels =dialog.findById("linkToSocialChannels");
    var buyNowVisual=dialog.findById("buyNowVisual");
    var limtVisual=dialog.findById("limtVisual"); 
	var videoSettings=dialog.findById("videoSettings");
	var newWindowVisual= dialog.findById("newWindowVisual");





 if(overrideGlobalConfigValue!='true')
 {
    visualPostDetails.hide();
    linkToSocialChannels.hide();
	buyNowVisual.hide();
     limtVisual.hide();
	newWindowVisual.hide();
     videoSettings.hide();


 }
    else
    {
        visualPostDetails.show();
    linkToSocialChannels.show();
	buyNowVisual.show();
     limtVisual.show();
	newWindowVisual.show();
     videoSettings.show();

    }
}   
function socialMediaGalleryBeforeSubmit(dialog) {        	    
    var count = 0;
    var currentObj = dialog.findById("socialChannelsToInclude");
    var twitter = document.getElementsByName("./twitter")[0].checked;


    var instagram = document.getElementsByName("./twitter")[0].checked
    var facebook = document.getElementsByName("./facebook")[0].checked
    var video = document.getElementsByName("./video")[0].checked
    var localAsset = document.getElementsByName("./localAsset")[0].checked
    var dialogFields = CQ.Util.findFormFields(currentObj);
    var facebookAccountId =  document.getElementsByName("./facebookAccountId");
    var twitterAccountId =  document.getElementsByName("./twitterAccountId");
	var instagramAccountId =  document.getElementsByName("./instagramAccountId");
    var tabDamId =  document.getElementsByName("./localAssetHashTagName");
    var check=true;

	var galleryType = dialog.getField('./galleryType').getValue();


    if(galleryType =='dynamic'){

	check=isSocialChannelIncluded(twitter, instagram, facebook, video, localAsset);

if(twitter==true)
{
    var message="Please Enter the twitter account id";
	check=hasEnteredAccountId(twitterAccountId, message,"twitterAccountId");
     if(check==false)
     {
         return false;
     }


}
         if (instagram==true) {	
                    var message = "Please Enter the instagram account id";
                   check=hasEnteredAccountId(instagramAccountId, message,"instagramAccountId");

					 if(check==false)
     {
         return false;
     }
                 }


                if (facebook==true) {
                    var message = "Please Enter the facebook account id";
                    check=hasEnteredAccountId(facebookAccountId, message,"facebookAccountId");
                     if(check==false)
     {
         return false;
     }

                }
               if (localAsset==true) {
                    var message = "Please Enter the Tab Content hash tags";
                    check=hasEnteredAccountId(tabDamId, message,"localAssetHashTagName");
                    if(check==false)
     {
         return false;
     }


                }


    if(check==false)
    {
        return false;
    }
    else
    {
        return true;
    }
    }
    else
    {
       return true;
    }


}







function isSocialChannelIncluded(twitter, instagram, facebook, video, localAsset){	
     if(twitter==true) {
        return true;
    }
     if(instagram==true) {
        return true;
    }
     if(facebook==true) {
        return true;
    }
    if (video==true) {
        return true;
    }
     if (localAsset==true) {
        return true;
    }else{
        CQ.Ext.Msg.show({
                                    title: 'Confirm',
                                     msg: 'Please enter atleast one social channel',
                                                                icon: CQ.Ext.MessageBox.ERROR,
                                         buttons: CQ.Ext.Msg.OK
                                                });
                                                return false;
    }
}
function hasEnteredAccountId(name, message,accountId){
	var olObject =name


       if ( olObject.length < 1 ) {
            CQ.Ext.Msg.show({
                                    title: 'Confirm',
                                     msg: message,
                                                                icon: CQ.Ext.MessageBox.ERROR,
                                         buttons: CQ.Ext.Msg.OK
                                                });

                                                return false;
    }else{
              return true;

            }
}
function enableFilterSearhPlaceholder(box,value)
{
	var dialog = box.ownerCt.findParentByType('dialog');
    var enablePlaceholder=dialog.findById("enableFilterSearchPlaceholder").getValue();
	var filterSearchPlaceholder = dialog.findById("filterSearchPlaceholder");
    if(enablePlaceholder !='true')
	{
		filterSearchPlaceholder.hide();
	}
	else{
		filterSearchPlaceholder.show();
	}
}