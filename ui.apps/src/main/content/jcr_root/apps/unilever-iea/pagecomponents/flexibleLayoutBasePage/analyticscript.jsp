<%@page
	import="com.unilever.platform.aem.foundation.configuration.GlobalConfiguration"%>
<%@ include file="/apps/unilever-iea/commons/global.jsp"%>
<%@ page
	import="com.unilever.platform.foundation.template.helper.AnalyticHelper,com.unilever.platform.aem.foundation.configuration.ConfigurationService"%>
<%
	AnalyticHelper.setAnalyticsAttributes(pageContext, sling,
			currentPage, currentNode);
%>
<c:choose>
	<c:when test="${isAdaptive == 'false'}">
		<c:if test="${analyticsDTO != null}">
			<script>
				function isMobile() {
					var check = false;
					(function(a) {
						if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i
								.test(a)
								|| /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i
										.test(a.substr(0, 4))) {
							check = true;
						}
					})(navigator.userAgent || navigator.vendor || window.opera);
					return check;
				}

				var channelVal;
				if (isMobile()) {
					channelVal = "Mobile Site"
				} else {
					channelVal = "Brand Site"
				}

				var digitalData = {};
				digitalData = {
					siteInfo : {
						globalbrand : '${analyticsDTO.globalbrand}',
						localbrand : '${analyticsDTO.localbrand}',
						channel : '',
						country : '${analyticsDTO.country}',
						sitetype : '${analyticsDTO.sitetype}',
						category : '${analyticsDTO.category}'
					},

					page : {
						pageInfo : {
							destinationURL : '${fullURL}'
						},
						category : {
							pageType : "${pageType}",
							contentType : "${contentType}"
						}
					},
					video : [],
                    campaign: [],
					product : [],
					privacy : {
						accessCategories : [ {
							domains : []
						} ]
					},
					component : [],
					trackingInfo : {
						GID : '${analyticsDTO.nonProdGid}',
						"un" : "",
						tool : [ {
							ids : ""
						} ]
					},
					promotion : []

				};
				
				var contextualizationData = {
					areaInfo : {
						'weather' : '',
						'temperature' : ''
					},
					
					profileData : {
						'crmid' : '',
						'age' : ''	
					}
				};
				
				window.onload = function() {
					try {
						if (CQ && CQ_Analytics) {
							CQ_Analytics.ClientContextUtils.onStoreInitialized('weatherstore', 
									listen,true);

							//listen for the store's update event
							function listen() {
								var weatherStore = CQ_Analytics.ClientContextMgr
										.getRegisteredStore("weatherstore");
								if (weatherStore) {
									var weatherStoreData = weatherStore
											.getData();
									if (weatherStoreData) {
										contextualizationData.areaInfo.weather = weatherStoreData.weather;
										contextualizationData.areaInfo.temperature = weatherStoreData.tempC;
									}

								}
							}
							
							CQ_Analytics.ClientContextUtils.onStoreInitialized('unileverprofilestore', 
									listenprofilestore,true);
							
							function listenprofilestore() {     
								var profileStore = CQ_Analytics.ClientContextMgr
										.getRegisteredStore("unileverprofilestore");
								if (profileStore) {
									var profileStoreData = profileStore
											.getData();
									
									contextualizationData.profileData.crmid = profileStoreData["CRMProviderMemberships/0/ProviderUserId"];
									
									if (profileStoreData && profileStoreData.Birthday) {
										var birthday = new Date(profileStoreData.Birthday);										
										var currentDate = new Date();
										contextualizationData.profileData.age = currentDate.getFullYear() - birthday.getFullYear();
									    var month = currentDate.getMonth() - birthday.getMonth();
									    if (month < 0 || (month === 0 && currentDate.getDate() < birthday.getDate())) {
									    	contextualizationData.profileData.age--;
									    }
									}

								}
							}

						}
					} catch (err) {
						console.log('contextualization is disabled');
					}
				};
				digitalData.areaInfo = contextualizationData.areaInfo;
				digitalData.siteInfo.channel = channelVal;
				digitalData.trackingInfo = {};
				digitalData.trackingInfo.tool = [ {} ];
				digitalData.privacy = {};
				digitalData.privacy.accessCategories = [ {} ];
				digitalData.privacy.accessCategories[0].domains = [];
				digitalData.event = [];
				(function(d, u) {
					var isProdEnv = '${analyticsDTO.isProdEnv}';
					if (isProdEnv != 'true') {
						u = ('https:' == document.location.protocol ? 'https://'
								: 'http://')
								+ '${analyticsDTO.nonProdDomain}';
						digitalData.trackingInfo.GID = '${analyticsDTO.nonProdGid}';
						digitalData.trackingInfo.tool[0].id = '${analyticsDTO.nonProdTrackToolId}';
						digitalData.privacy.accessCategories[0].domains[0] = '${analyticsDTO.accessCategoriesDomains}';

					} else {
						u = ('https:' == document.location.protocol ? 'https://'
								: 'http://')
								+ '${analyticsDTO.prodDomain}';
						digitalData.trackingInfo.GID = '${analyticsDTO.prodGid}';
						digitalData.trackingInfo.tool[0].id = '${analyticsDTO.prodTrackToolId}';
						digitalData.privacy.accessCategories[0].domains[0] = '${analyticsDTO.accessCategoriesDomains}';
					}
					document
							.write("<script type='text/javascript' src='" + u + "/ct/" + digitalData.trackingInfo.GID + "/u.js'></scr" + "ipt>");
				})(document);
			</script>
		</c:if>
	</c:when>
	<c:otherwise>
		<%
			ConfigurationService configurationService = (ConfigurationService) sling
							.getService(ConfigurationService.class);
					GlobalConfiguration globalConfiguration = (GlobalConfiguration) sling
							.getService(GlobalConfiguration.class);
					String featurePhoneAnalytics = globalConfiguration
							.getConfigValue("featurePhoneAnalyticsURL");
					pageContext.setAttribute("featurePhoneAnalyticsURL",
							featurePhoneAnalytics);
		%>
		<img
			src="${featurePhoneAnalyticsURL}title=${pageProperties['jcr:title']}&url=${fullURL}&gid=${analyticsDTO.isProdEnv ? analyticsDTO.prodTrackToolId : analyticsDTO.nonProdTrackToolId}"
			width="1" height="1" />
	</c:otherwise>
</c:choose>