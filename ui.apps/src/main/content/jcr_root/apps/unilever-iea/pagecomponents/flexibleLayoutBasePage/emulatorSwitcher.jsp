<%--  
<emulatorSwitcher.jsp>

  Copyright 2013 IEA, SapientNitro
  All Rights Reserved.

  This software is the confidential and proprietary information of
  SapientNitro, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with SapientNitro.

  ==============================================================================
   Page Template
   A template which acts as base for various templates.
  ==============================================================================

--%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="/libs/foundation/global.jsp" %>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
	 $(function() {
		 $(parent.document.querySelectorAll(".editor-EmulatorBar-toolbar-actions")).hide();

     });

    var originalPath = "${currentPage.path}" + ".html";

    console.log(originalPath);

    var createPath = function(a) {
      if ("native" === a) {
        return originalPath;
      }
      var b = originalPath, c = b.substr(b.lastIndexOf("/") + 1).split(".");

      return b.substring(0, b.lastIndexOf(".") + 1) + (a + "." + c[c.length - 1]);
    };

    $(function() {
      $(parent.document.querySelectorAll(".editor-EmulatorBar-toolbar")).find("[data-device]").on("click", function(a) {
        a.preventDefault();
      
        window.location = createPath($(this).data("device"));
      });
    });

</script>