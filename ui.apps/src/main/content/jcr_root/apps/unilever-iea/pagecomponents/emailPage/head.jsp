<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="/apps/unilever-iea/commons/global.jsp" %>
<%@ page import ="com.unilever.platform.foundation.template.helper.SEOHelper"%>
<%@taglib prefix="iea" uri="http://iea.com/platform/aem/components/core/tags/1.0"%>
<cq:include script="/apps/wcm/core/components/init/init.jsp" />
<cq:include script="appConfig.jsp" />
<%
	SEOHelper.setSEOAttributes(resourceResolver,currentPage,request,sling);
	String schemaUrl = request.getProtocol().split("/")[0].toLowerCase()+"://schema.org/WebSite";
%>
<head <c:if test="${not empty requestScope.isHomePage}"> itemscope itemtype="<%=schemaUrl%>" </c:if>>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta charset="utf-8" />

<c:if test="<%= ((String) request.getAttribute("runMode")).contains("author") %>">
<cq:include script="/libs/wcm/mobile/components/simulator/simulator.jsp"/>
<cq:include script="emulatorSwitcher.jsp"/> 
</c:if>
<c:set var="description" value="${pageProperties['jcr:description']}" />
<meta name="description" content="${description}" />
<c:set var="keywords" value="${pageProperties['keywords']}" />
<c:if test="${not empty keywords }">
	<meta name="keywords" content="${keywords}" />
</c:if>
<meta http-equiv="content-language" content="${requestScope.lang}" />
<c:forEach items="${requestScope.otherMetaList}" var="item"> 
    <meta name="${item.name}" content="${item.content}"/>
</c:forEach>

<c:set var="baseURL" value="${fn:replace(slingRequest.requestURL, slingRequest.requestURI, '')}" />


<c:set var="fullURL" value="${requestScope.fullURL}" />
<c:if test="${empty fullURL}">
    <c:set var="fullURL" value="${baseURL}${currentPage.path}.html" />
</c:if>


<meta http-equiv="cleartype" content="on" />
<meta name="HandheldFriendly" content="True" />
<meta name="MobileOptimized" content="320" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

</head> 