<%@ include file="/apps/unilever-iea/commons/global.jsp" %>
<%@page import="com.sapient.platform.iea.aem.utils.ClientLibUtils" %>
<%@page import="java.util.Locale,java.util.ResourceBundle"%>
<c:set var="componentName" value="${component.name}" />
<c:set var="wrapperClass" value="en-flexible" />
<c:set var="wrapperClassAdaptive" value="en-flexible navOpen" />
<c:if test="${not empty properties.wrapperClass}">
    <c:set var="wrapperClass" value="${properties.wrapperClass}" />
</c:if>

<c:if test="${requestScope['com.day.cq.wcm.api.WCMMode'] == 'EDIT'}">
	<c:set var="ediModeClass" value=" wcmmode-edit"/>
</c:if>

<c:set var="bodyClass" value=""/>
<c:if test="${not empty properties.bodyClass}">
    <c:set var="bodyClass" value="${properties.bodyClass} "/>
</c:if>
<%
final Locale pageLocale = currentPage.getLanguage(false);
final String customClass = pageProperties.getInherited("customBodyClass", "");
String customBodyClass = "";
if(!customClass.isEmpty()){
	customBodyClass = " "+customClass;
}
%>
<c:set var="rtlConfig" value="${globalConfig:getCategoryConfig(resource,'rtlConfig')}"/>
<body data-config="/bin/iea/${tenantName}/${tenantName}.tenantinfo.json" class="${bodyClass}${ediModeClass} js <%= pageLocale %><%= customBodyClass %> <c:if test="${rtlConfig != null && rtlConfig.enabled=='true' && rtlConfig.enabled!= null}">  ${' '} ${rtlConfig.class} </c:if>">


	 <c:choose>
            <c:when test="${fn:contains(selectorURL, '.fullmenu')}">
                <div id="wrapper" class="${wrapperClassAdaptive}">
            </c:when>
            <c:otherwise>
            <div id="wrapper" class="${wrapperClass}">
            </c:otherwise>
    </c:choose>
        <c:if test="${not properties.hideHeader}">
            <cq:include script="header.jsp" />
        </c:if>
        <cq:include script="content.jsp" />
     </div>
</body>
