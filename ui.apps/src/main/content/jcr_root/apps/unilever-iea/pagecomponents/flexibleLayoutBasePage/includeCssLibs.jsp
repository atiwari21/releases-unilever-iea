<%--  
 <includeCssLibs.jsp>
    This file is used for including css clientlibs.

    If tenant core clientlib(clientlib.iea.<tenantname>.core) exists include that,
    else it includes framework core clientlib(clientlib.iea.core).

    Tenant can override this file in case they want to change or add more use cases.

--%>

<%@ include file="/apps/iea/commons/global.jsp" %>
<%@page import="com.sapient.platform.iea.aem.utils.ClientLibUtils" %>

<c:choose>
	<c:when test="${not empty clientLibName }">
	<%-- Hard coded value of css and js tenant feature-phone will be removed after it is being completely implemented --%>
		<c:if test="${isAdaptive == 'true'}">
              <cq:includeClientLib css="clientlib.iea.${adaptiveClientLibName}.core" />
        </c:if>
        <c:if test="${isAdaptive != 'true'}">
			<cq:include script="tenantCore.jsp" />
        </c:if>
	</c:when>
	<c:otherwise>
	<%-- sets up clientlib category, path
		and loads browser supported IEA core css clientlib --%>
		<cq:include script="platformCore.jsp" />
	</c:otherwise>
</c:choose>