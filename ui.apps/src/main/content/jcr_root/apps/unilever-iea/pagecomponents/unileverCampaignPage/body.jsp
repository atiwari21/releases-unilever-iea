<%@ include file="/apps/unilever-iea/commons/global.jsp" %>
<%@page import="com.sapient.platform.iea.aem.utils.ClientLibUtils" %>
<%@page import="java.util.Locale,java.util.ResourceBundle"%>

<c:set var="componentName" value="${component.name}" />
<c:set var="wrapperClass" value="en-flexible" />
<c:if test="${not empty properties.wrapperClass}">
    <c:set var="wrapperClass" value="${properties.wrapperClass}" />
</c:if>

<c:if test="${requestScope['com.day.cq.wcm.api.WCMMode'] == 'EDIT'}">
	<c:set var="ediModeClass" value=" wcmmode-edit"/>
</c:if>

<c:set var="bodyClass" value=""/>
<c:if test="${not empty properties.bodyClass}">
    <c:set var="bodyClass" value="${properties.bodyClass} "/>
</c:if>
<%
final Locale pageLocale = currentPage.getLanguage(false);
final String customClass = pageProperties.getInherited("customBodyClass", "");
String customBodyClass = "";
if(!customClass.isEmpty()){
	customBodyClass = " "+customClass;
}
%>
<!--[if IE 7 ]><body class="ie ie7 lt-ie8 lt-ie9 lt-ie10 no-mq form-page ${bodyClass}${ediModeClass} js <%= pageLocale %><%= customBodyClass %>"><![endif]-->
<!--[if IE 8 ]><body class="ie ie8 lt-ie9 lt-ie10 no-mq form-page ${bodyClass}${ediModeClass} js <%= pageLocale %><%= customBodyClass %>"><![endif]-->
<!--[if IE 9 ]><body class="ie ie9 lt-ie10 js form-page ${bodyClass}${ediModeClass} js <%= pageLocale %><%= customBodyClass %>"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<c:set var="rtlConfig" value="${globalConfig:getCategoryConfig(resource,'rtlConfig')}"/>
<body data-config="/bin/iea/${tenantName}/${tenantName}.tenantinfo.json" class="${bodyClass}${ediModeClass} js <%= pageLocale %><%= customBodyClass %> <c:if test="${rtlConfig != null && rtlConfig.enabled=='true' && rtlConfig.enabled!= null}">  ${' '} ${rtlConfig.class} </c:if>">

    <!--<![endif]-->

	 <div id="wrapper" class="${wrapperClass}">
        <c:if test="${not properties.hideHeader}">
            <cq:include script="header.jsp" />
        </c:if>
        <cq:include script="content.jsp" />
     </div>
	<cq:include path="cloudservices" resourceType="cq/cloudserviceconfigs/components/servicecomponents"/>
    <cq:include script="includeJS.jsp" />
<cq:include script="addthis.jsp"/>
<script src="/etc/ui/${clientLibName}/clientlibs/core/core/config/settings.js" type="text/javascript"></script>

<script type="text/javascript">
    if(typeof _satellite != 'undefined')
    	_satellite.pageBottom();
</script>
</body>