<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="/apps/unilever-iea/commons/global.jsp" %>

<cq:setContentBundle />
<c:set var="customCookie" value="${globalConfig:getCategoryConfig(resource,'cookiePopup')}"/>
<c:if test="${isAdaptive == 'false' && (customCookie.isEnabled=='true' || customCookie==null || customCookie.isEnabled==null)}">
<%-- change of cookie script for loading from cdn --%>
    <c:choose>
    	<c:when test="${not empty customCookie.url}">
            <script type="text/javascript" src="${customCookie.url}" 
				<c:forEach var="entry" items="${customCookie}">
                    <c:if test="${fn:startsWith(entry.key, 'data-')}">
                        ${entry.key} =${entry.value} 
                     </c:if>
                </c:forEach>
            >
                <c:forEach var="entry" items="${customCookie}">
                    <c:if test="${entry.key!='url' && entry.key!='isEnabled' && not (fn:startsWith(entry.key, 'data-'))}">
                        var ${entry.key} =${entry.value};
                     </c:if>
                </c:forEach>
            </script>
        </c:when>
        <c:otherwise>
			<script type="text/javascript" src="//az417220.vo.msecnd.net/tg.js" data-org="unilever_support"></script>
        </c:otherwise>
    </c:choose>
</c:if>


<footer>
	<c:if test="${empty requestScope.nofooter}">
	  <cq:include path="footerpar" resourceType="foundation/components/iparsys" />
	</c:if>
</footer>