<%@ include file="/apps/unilever-iea/commons/global.jsp" %>
<cq:setContentBundle />

	<c:set var="title" value="${currentPage.title}" />
	<section class="o-content-wrapper o-content-border js-blur-bg js-main-wrapper clearfix">

        <cq:include path="par" resourceType="foundation/components/parsys" />
        <c:if test="${not properties.hideFooter}">
		 <!--excludesearch--> <cq:include script="footer.jsp" /><!--endexcludesearch-->
		</c:if>
    </section>



	<%@include file="/apps/iea/commons/clientlibs.jsp"%>

	<c:set var="includeCategory" value="css" scope="request" />
    <cq:include path="includeLib" resourceType="iea/components/includeClientLib" />
<c:if test="${isAdaptive == 'false'}">
    <c:set var="includeCategory" value="js" scope="request" />
    <cq:include path="includeLib" resourceType="iea/components/includeClientLib" />
</c:if>
<cq:include script="/apps/unilever-iea/commons/shopnow.jsp"/>