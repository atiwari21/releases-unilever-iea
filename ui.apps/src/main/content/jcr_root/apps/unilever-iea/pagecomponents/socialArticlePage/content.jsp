<%@ include file="/apps/unilever-iea/commons/global.jsp" %>
    <cq:setContentBundle />

    <c:set var="title" value="${currentPage.title}" />
    <section class="o-content-wrapper o-content-border js-blur-bg js-main-wrapper clearfix">

        <cq:include path="breadCrumb" resourceType="/apps/unilever-iea/components/breadcrumb" />
        <cq:include path="bodyCopyHeadline" resourceType="/apps/unilever-iea/components/bodyCopyHeadline" />
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-8 col-sm-offset-2 o-article-wrapper">
                    <cq:include path="content_par_6_column" resourceType="foundation/components/parsys" />
                </div>
                <c:if test="${requestScope['com.day.cq.wcm.api.WCMMode'] != 'EDIT'}">
                    <div class="col-md-3 js-multiple-related-placeholder hidden-sm hidden-xs"></div>
                </c:if>
            </div>
        </div>

        <cq:include path="content_par_12_column" resourceType="foundation/components/parsys" />
        <c:choose>
            <c:when test="${requestScope['com.day.cq.wcm.api.WCMMode'] == 'EDIT'}">
                <div class="wcmmode-edit">
                    <cq:include path="multipleRelatedArtSidebar" resourceType="/apps/unilever-iea/components/multipleRelatedArticlesSidebar" />
                </div>
            </c:when>
            <c:otherwise>
                <cq:include path="multipleRelatedArtSidebar" resourceType="/apps/unilever-iea/components/multipleRelatedArticlesSidebar" />
            </c:otherwise>
        </c:choose>
        <cq:include path="socialSharing" resourceType="/apps/unilever-iea/components/socialSharing" />
        <cq:include path="productTags" resourceType="/apps/unilever-iea/components/productTags" />
        <cq:include path="reviews_par" resourceType="foundation/components/parsys" />
        <c:if test="${not properties.hideFooter}">
            <!--excludesearch-->
            <cq:include script="footer.jsp" />
            <!--endexcludesearch-->
        </c:if>
    </section>
    <%@include file="/apps/iea/commons/clientlibs.jsp"%>

        <c:set var="includeCategory" value="css" scope="request" />
        <cq:include path="includeLib" resourceType="iea/components/includeClientLib" />

        <c:set var="includeCategory" value="js" scope="request" />
        <cq:include path="includeLib" resourceType="iea/components/includeClientLib" />
		<cq:include script="/apps/unilever-iea/commons/shopnow.jsp"/>