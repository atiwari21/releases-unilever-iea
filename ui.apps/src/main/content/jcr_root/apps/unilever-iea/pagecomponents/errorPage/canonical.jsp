<%@ include file="/apps/unilever-iea/commons/global.jsp" %>
<title><c:set var="title" value="${pageProperties.pageTitle}" />
    <c:if test="${empty title}">
        <c:set var="title" value="${pageProperties['jcr:title']}" />
    </c:if> <c:if test="${title != null}">
        <c:out value="${title}" />
    </c:if></title>
