<%@include file="/libs/wcm/global.jsp"%>
<cq:include script="/libs/wcm/core/components/init/init.jsp"/>
<%
   %><%@ page import="com.day.cq.wcm.foundation.ELEvaluator,
   org.apache.commons.lang3.ArrayUtils,
   com.day.cq.wcm.api.WCMMode,
   com.day.cq.i18n.I18n,
   com.unilever.platform.foundation.components.helper.ComponentUtil,
   org.apache.sling.settings.SlingSettingsService"%><%
   I18n i18n = new I18n(slingRequest);
   // try to resolve the redirect target in order to the the title
   String path = properties.get("redirectTarget", "");
   String type = properties.get("type", Integer.toString(HttpServletResponse.SC_MOVED_PERMANENTLY));
   // legacy default is to only redirect in publish mode:
    String[] redirectModes = properties.get("redirectModes", new String[]{"DISABLED","PREVIEW"});
   
   // resolve variables in path
   path = ELEvaluator.evaluate(path, slingRequest, pageContext);
   // check for recursion
    if (path.equals("") && path.isEmpty()) {
	    %>
		<p align="center">
			<%= i18n.get("This page redirects to {0}") %>
		</p>
		<%
	}
    else if (!path.equals("") && !path.equals(currentPage.getPath()) && !path.isEmpty()) {
		SlingSettingsService slingSettings = sling.getService(SlingSettingsService.class);
		if (ArrayUtils.contains(redirectModes, WCMMode.fromRequest(request).name()) || slingSettings.getRunModes().contains("publish")) {
            response.setStatus(Integer.valueOf(Integer.parseInt(type)));
            response.setHeader("Location",ComponentUtil.getFullURL(resourceResolver,path));
            return;
        } else {
            Page target = pageManager.getPage(path);
            String title = target == null ? path : target.getTitle();
            %>
<p align="center">
    <%= i18n.get("This page redirects to {0}", null, "<a href=\"" + xssAPI.getValidHref(path) + "\">" + xssAPI.filterHTML(title) + "</a>") %>
</p>
<%
        }
    }
	else {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
   %>