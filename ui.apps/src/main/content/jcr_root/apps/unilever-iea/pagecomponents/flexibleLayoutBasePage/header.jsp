<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="/apps/unilever-iea/commons/global.jsp" %>

<cq:setContentBundle />
<c:if test="${requestScope.tenantInfo.tenantAnalyticsInfo.isAnalytics=='true' && requestScope['com.day.cq.wcm.api.WCMMode']=='DISABLED' }">
	<cq:include script="analytics/analyticsHeader.jsp" /> 
</c:if>
    
	
	<header class="js-navigation">
        <div class="c-secondary-nav">
		<cq:include path="secondarypar"
            	resourceType="foundation/components/iparsys" />
		</div>
        <cq:include path="headerpar"
            	resourceType="foundation/components/iparsys" />
    </header>
	
	