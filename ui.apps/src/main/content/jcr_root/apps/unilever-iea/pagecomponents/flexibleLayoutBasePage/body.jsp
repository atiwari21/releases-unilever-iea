<%@ include file="/apps/unilever-iea/commons/global.jsp" %>
<%@page import="com.sapient.platform.iea.aem.utils.ClientLibUtils,
    com.unilever.platform.aem.foundation.configuration.ConfigurationService" %>
<%@page import="java.util.Locale,java.util.ResourceBundle"%>
<%! 
    
	public static final String ENABLED ="enabled";
	public static final String TRUE ="true";
	
%>
<!-- Content Type Schemas -->
<c:set var="productSchema" value="http://schema.org/Product" />
<c:set var="articleSchema" value="http://schema.org/Article" />
<c:set var="recipeSchema" value="https://schema.org/Recipe" />
<c:set var="isArticleType" value="false" />
<!-- Content Type Schemas -->

<c:set var="componentName" value="${component.name}" />
<c:set var="wrapperClass" value="en-flexible" />
<c:set var="wrapperClassAdaptive" value="en-flexible navOpen" />
<c:if test="${not empty properties.wrapperClass}">
    <c:set var="wrapperClass" value="${properties.wrapperClass}" />
</c:if>

<c:set var="pageSchema" value="" />

<c:if test="${requestScope['com.day.cq.wcm.api.WCMMode'] == 'EDIT'}">
	<c:set var="ediModeClass" value=" wcmmode-edit "/>
</c:if>

<%
final Locale pageLocale = currentPage.getLanguage(false);
final String customClass = pageProperties.getInherited("customBodyClass", "");
final String contentType = pageProperties.get("contentType", "");
String customBodyClass = "";
if(!customClass.isEmpty()){
	customBodyClass = " "+customClass;
}
com.unilever.platform.aem.foundation.configuration.ConfigurationService configurationService = sling.getService(com.unilever.platform.aem.foundation.configuration.ConfigurationService.class);
%>
<c:set var="contentType" value="<%=contentType%>" />
<c:set var="bodyClass" value=""/>
<c:if test="${not empty properties.bodyClass}">
    <c:set var="bodyClass" value="${properties.bodyClass} "/>
</c:if>

<!-- Content Schema Conditions -->
<c:choose>
    <c:when test="${contentType eq 'Product'}">
        <c:set var="pageSchema" value="${productSchema}" />
    </c:when>
    <c:when test="${contentType eq 'recipe'}">
        <c:set var="pageSchema" value="${recipeSchema}" />
    </c:when>
    <c:when test="${(contentType eq 'Article') || (contentType eq 'Social Article') || (contentType eq 'Social Mission Article Page')}">
        <c:set var="pageSchema" value="${articleSchema}" />
        <c:set var="isArticleType" value="true" />
    </c:when>
</c:choose>
<!-- Content Schema Conditions -->
<c:set var="rtlConfig" value="${globalConfig:getCategoryConfig(resource,'rtlConfig')}"/>
<!--[if IE 7 ]><body class="ie ie7 lt-ie8 lt-ie9 lt-ie10 no-mq ${bodyClass} ${ediModeClass}<%= pageLocale %><%= customBodyClass %> <c:if test="${rtlConfig != null && rtlConfig.enabled=='true' && rtlConfig.enabled!= null}">  ${' '} ${rtlConfig.class} </c:if>"><![endif]-->
<!--[if IE 8 ]><body class="ie ie8 lt-ie9 lt-ie10 no-mq ${bodyClass} ${ediModeClass}<%= pageLocale %><%= customBodyClass %> <c:if test="${rtlConfig != null && rtlConfig.enabled=='true' && rtlConfig.enabled!= null}">  ${' '} ${rtlConfig.class} </c:if>"><![endif]-->
<!--[if IE 9 ]><body class="ie ie9 lt-ie10 js ${bodyClass} ${ediModeClass}<%= pageLocale %><%= customBodyClass %> <c:if test="${rtlConfig != null && rtlConfig.enabled=='true' && rtlConfig.enabled!= null}">  ${' '} ${rtlConfig.class} </c:if>"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->

<body data-config="/bin/iea/${tenantName}/${tenantName}.tenantinfo.json" class="${bodyClass}${ediModeClass} js <%= pageLocale %><%= customBodyClass %> <c:if test="${rtlConfig != null && rtlConfig.enabled=='true' && rtlConfig.enabled!= null}">  ${' '} ${rtlConfig.class} </c:if>" <c:if test="${not empty pageSchema}">itemscope itemtype="${pageSchema}"</c:if>>
<c:set var="clientContext" value="${globalConfig:getCategoryConfig(resource,'clientContext')}"/>
<c:if test="${clientContext.enabled=='true'}">
<cq:include path="clientcontext" resourceType="cq/personalization/components/clientcontext"/>
</c:if>
<cq:include script="analyticscript.jsp" />
    <!--<![endif]-->

	 <c:choose>
            <c:when test="${fn:contains(selectorURL, '.fullmenu')}">
                <div id="wrapper" class="${wrapperClassAdaptive}">
            </c:when>
            <c:otherwise>
            <div id="wrapper" class="${wrapperClass}">
            </c:otherwise>
    </c:choose>
    <c:choose>
    	<c:when test="${isArticleType eq 'true'}">
    	<article>
        <c:if test="${not properties.hideHeader}">
            <cq:include script="header.jsp" />
        </c:if>
        <cq:include script="content.jsp" />
        </article>
        </c:when>
        <c:otherwise>
         <c:if test="${not properties.hideHeader}">
            <cq:include script="header.jsp" />
        </c:if>
        <cq:include script="content.jsp" />
        </c:otherwise>
    </c:choose>
     </div>

         <c:if test="${isAdaptive == 'false'}">
            <c:if test="${shopNowConfig!=null}">
		             <c:if test="${shopNowConfig.enabled=='true' && shopNowConfig.serviceProviderName == 'shoppable'}">
                        <cq:include script="/apps/unilever-iea/commons/shoppableBody.jsp"/>
					 </c:if>
				  </c:if>
	    <cq:include script="/apps/unilever-iea/commons/campaignOfferFlow.jsp" />
        <cq:include script="includeJS.jsp" />
<script src="/etc/ui/${clientLibName}/clientlibs/core/core/config/settings.js" type="text/javascript"></script>
</c:if>
<cq:include script="addthis.jsp"/>
<cq:include path="cloudservices" resourceType="cq/cloudserviceconfigs/components/servicecomponents"/>
</body>
