<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="/apps/unilever-iea/commons/global.jsp" %>
<%@ page import ="com.unilever.platform.foundation.template.helper.SEOHelper"%>
<%@ page import ="com.unilever.platform.foundation.components.helper.ComponentUtil"%>
<%@page import="com.sapient.platform.iea.aem.utils.ClientLibUtils,
    com.unilever.platform.aem.foundation.configuration.ConfigurationService,
	com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility" %>
<%@taglib prefix="iea" uri="http://iea.com/platform/aem/components/core/tags/1.0"%>
<cq:include script="/apps/wcm/core/components/init/init.jsp" />
<cq:include script="appConfig.jsp" />
<%! 
    public static final String FONT_SETTING ="googleFontSetting";
    public static final String IS_GOOGLE_FONT_ENABLED ="isGoogleFontEnabled";
    public static final String GOOGLE_FONT_URL ="googleFontURL";
    public static final String TRUE ="true";
    public static final String IS_FONT_DOT_COM_ENABLED ="isFontDotComEnabled";
    public static final String FONT_DOT_COM ="fontDotCom";
    public static final String FONT_DOT_COM_URL ="url";
	public static final String ENABLED ="enabled";
	public static final String TYPOGRAPHY ="typography";
%>  
<c:if test="${requestScope['com.day.cq.wcm.api.WCMMode'] != 'EDIT'}">
<c:if test="${requestScope['com.day.cq.wcm.api.WCMMode'] != 'DESIGN'}">
<c:if test="<%= !ComponentUtil.isComponentExist(currentPage) %>">
    <c:if test="<%= "true".equals(properties.get("isSecurePage", String.class)) %>">
<%
    response.setStatus(401);
    response.setContentType("text/html"); 
    response.setCharacterEncoding("utf-8");
%>
</c:if>
</c:if>
</c:if>
</c:if>
<%
    com.unilever.platform.aem.foundation.configuration.ConfigurationService configurationService = sling.getService(com.unilever.platform.aem.foundation.configuration.ConfigurationService.class);
    String isGoogleFontEnabled = configurationService.getConfigValue(currentPage, FONT_SETTING, IS_GOOGLE_FONT_ENABLED);
    Boolean isGoogleFontEnabledFlag = isGoogleFontEnabled !=null && !isGoogleFontEnabled.isEmpty() && isGoogleFontEnabled.equalsIgnoreCase(TRUE) ? true :false;
    String isFontDotComEnabled = configurationService.getConfigValue(currentPage, FONT_DOT_COM, IS_FONT_DOT_COM_ENABLED);
    Boolean isFontDotComEnabledFlag = isFontDotComEnabled !=null && !isFontDotComEnabled.isEmpty() && isFontDotComEnabled.equalsIgnoreCase(TRUE) ? true :false;
    String googleFontURL=null;
    String fontsDotComUrl=null;
    if(isGoogleFontEnabledFlag){
    	googleFontURL = configurationService.getConfigValue(currentPage, FONT_SETTING, GOOGLE_FONT_URL);
    	googleFontURL = googleFontURL !=null && !googleFontURL.isEmpty() ? googleFontURL :null;	
    }  
    if(isFontDotComEnabledFlag){
    	fontsDotComUrl = configurationService.getConfigValue(currentPage, FONT_DOT_COM, FONT_DOT_COM_URL);
    	fontsDotComUrl = fontsDotComUrl !=null && !fontsDotComUrl.isEmpty() ? fontsDotComUrl :null;	
    }
%>
<c:set var="isGoogleFontEnabledFlag" value="<%=isGoogleFontEnabledFlag%>" scope="request" />
<c:set var="googleFontURL" value="<%=googleFontURL%>" scope="request" />
<c:set var="isFontDotComEnabledFlag" value="<%=isFontDotComEnabledFlag%>" scope="request" />
<c:set var="fontsDotComUrl" value="<%=fontsDotComUrl%>" scope="request" />
<%
	SEOHelper.setSEOAttributes(resourceResolver,currentPage,request,sling);
	String schemaUrl = request.getProtocol().split("/")[0].toLowerCase()+"://schema.org/WebSite";
%>
<!--${releasedetails:getReleaseMessage()}-->
<head <c:if test="${not empty requestScope.isHomePage}"> itemscope itemtype="<%=schemaUrl%>" </c:if>>
<%
	                        
	                        Boolean enableODP= Boolean.parseBoolean(GlobalConfigurationUtility.getValueFromConfiguration(configurationService,currentPage,"metaODP","enabled"));
							Boolean enableYDIR= Boolean.parseBoolean(GlobalConfigurationUtility.getValueFromConfiguration(configurationService,currentPage,"metaYDIR","enabled"));
	

if(enableODP){ %>

<meta name="robots" content="noodp"/>
<% } %>

<%if(enableYDIR){ %>

<meta name="robots" content="noydir"/>
<%} %>
<c:set var="typography"
	value="${globalConfig:getCategoryConfig(resource,'typography')}" />
<c:if test="${typography.enabled=='true'}">
	<link rel="stylesheet" type="text/css" href="${typography.url}" />
</c:if>
<c:set var="clientContext" value="${globalConfig:getCategoryConfig(resource,'clientContext')}"/>
<c:if test="${clientContext.enabled=='true'}">
<cq:include script="/libs/cq/cloudserviceconfigs/components/servicelibs/servicelibs.jsp"/>
</c:if>

<!--adaptive appName = ${adaptiveClientLibName}--> 

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta charset="utf-8" />
<cq:include script="canonical.jsp" />
<c:if test="<%= ((String) request.getAttribute("runMode")).contains("author") %>">
<cq:include script="/libs/wcm/mobile/components/simulator/simulator.jsp"/>
<cq:include script="emulatorSwitcher.jsp"/> 
</c:if>
<c:set var="description" value="${pageProperties['jcr:description']}" />
<meta name="description" content="${description}" />
<c:set var="keywords" value="${pageProperties['keywords']}" />
<c:if test="${not empty keywords }">
	<meta name="keywords" content="${keywords}" />
</c:if>
<meta http-equiv="content-language" content="${requestScope.lang}" />
<cq:include script="follow.jsp" />

<c:forEach items="${requestScope.otherMetaList}" var="item"> 
    <meta name="${item.name}" content="${item.content}"/>
</c:forEach>
<c:set var="reqURL"><%=ComponentUtil.getURLWithUpdatedProtocol(slingRequest.getRequestURL().toString(), request)%></c:set>
<c:set var="baseURL" value="${fn:replace(reqURL, slingRequest.requestURI, '')}" />
<c:set var="fbLikeDesc">
    <c:out value="${fn:trim(pageProperties['fbLikeDescription'])}" escapeXml="true" />
</c:set>
<c:set var="fullURL" value="${requestScope.fullURL}" />
<c:if test="${empty fullURL}">
    <c:set var="fullURL" value="${baseURL}${currentPage.path}.html" />
</c:if>
<c:if test="${not empty requestScope.socialShareImage}">
    <c:set var="socialShareImage" value="${requestScope.socialShareImage}" />
</c:if>
<meta http-equiv="cleartype" content="on" />
<meta name="HandheldFriendly" content="True" />
<meta name="MobileOptimized" content="320" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<c:set var="instantarticle" value="${globalConfig:getCategoryConfig(resource,'instantarticle')}" />
	<c:set var="fbpageid" value="${instantarticle.fbpageid}"/>
<c:if test="${not empty fbpageid}">
	<meta property="fb:pages" content="${fbpageid}" />
</c:if>


<meta property="og:url" content="${fullURL}" />
<meta property="og:title" content="${requestScope.ogTitle}" />
<meta property="og:site_name" content="${requestScope.ogSiteName}" />
<meta property="og:type" content="${requestScope.og_type}" />
<meta property="og:description" content="${requestScope.ogDescription}" />
<c:if test="${not empty socialShareImage}">
<meta property="og:image" content="${socialShareImage}" />
</c:if>
<meta name="fragment" content="!" /> 


<!--twitter tags-->
<c:set var="twitterCard" value="${pageProperties['twitterCard']}" />
<c:if test="${empty twitterCard}">
    <c:set var="twitterCard" value="summary" />
</c:if> 

<meta name="twitter:card" content="${twitterCard}" />
<meta name="twitter:url" content="${fullURL}" />
<meta name="twitter:title" content="${requestScope.twitterTitle}" />
<meta name="twitter:description" content="${requestScope.twitterDescription}" />
<c:if test="${not empty socialShareImage}">
<meta name="twitter:image" content="${socialShareImage}" />
</c:if>    

<link href="${baseURL}${requestScope.favIconPath}" type="image/x-icon" rel="shortcut icon" />
<link rel="apple-touch-icon" sizes="76x76" href="${baseURL}${requestScope.favIconIPADPath}" />
<cq:include script="hreflang.jsp" />
<!--custom javascript code-->
<c:if test="${isAdaptive == 'false'}">
<script>
	var jTitle="${pageProperties['jcr:title']}";
</script>
</c:if>
<!--end-->

<c:if test="${ requestScope['com.day.cq.wcm.api.WCMMode']=='EDIT' || requestScope['com.day.cq.wcm.api.WCMMode']=='DESIGN'}">
	<cq:includeClientLib categories="cq.foundation,cq.widgets,iea.widgets,iea.custom-xtype" />
</c:if>
<c:if test="${ requestScope['com.day.cq.wcm.api.WCMMode']!='EDIT'}">
	<cq:includeClientLib categories="rte.customcss" />
</c:if>

<%-- This file is used for including css clientlibs --%>
<cq:include script="includeCssLibs.jsp" />

<%-- change of cookie script for loading from cdn --%>
<c:if test="${isAdaptive == 'false'}">

<%-- Rating and Review Section Start --%>
	<cq:include script="ratingreview.html" />
<%-- Rating and Review Section End --%>
<%-- Google Font Section Start --%>
 <c:if test="${(isGoogleFontEnabledFlag) && (not empty googleFontURL)}">
 	<link href='${googleFontURL}' rel='stylesheet' type='text/css'>
 </c:if>
<%-- Google Font  Section End --%>
</c:if>

<%-- Google Font Section Start --%>
 <c:if test="${not empty fontsDotComUrl}">
 	<script type='text/javascript' src='${fontsDotComUrl}'></script>
 </c:if>
<%-- Google Font  Section End --%>
</head> 