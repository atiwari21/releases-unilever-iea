<%@ include file="/libs/foundation/global.jsp" %>
<%@ include file="/apps/unilever-iea/commons/global.jsp" %>
<%@page import="com.sapient.platform.iea.aem.utils.ClientLibUtils" %>

<c:set var="mainJsPath" value ="/etc/ui/${clientLibName}/clientlibs/core/require/js/main.js" />
    <c:set var="mainJs" value ="/etc/ui/${clientLibName}/clientlibs/core/require/js/main" />

<%--break the required js and main js in two separate script tag to enable the versioning of main js --%>
<%--remove the data-main attribute because the internal routing from main.1234.js to main.js is unable to understood by requirejs --%>

<script src="/etc/ui/iea/clientlibs/core/require/js/require.js"></script>
<ieaFoundation:versionMainJs path="${mainJsPath}" />

<c:choose>
    <c:when test="<%= ClientLibUtils.isPathExists(resourceResolver, (String)pageContext.getAttribute("mainJsPath"))%>">

        <script>
            var mainJSVersionURL;
         	if("${mainjs_version}"){ 
                mainJSVersionURL = '${mainJs}' + '.' + '${mainjs_version}';
            } else { 
				mainJSVersionURL= '${mainJs}';
            }

            
            require.config({
                paths: {
                    'main': mainJSVersionURL
                }
            });
        </script>

      </c:when>
          <%--if no clinrlib exist in request then by default iea main js will be called --%>
      <c:otherwise>
       <script>
            var mainJSURL = '/etc/ui/iea/clientlibs/core/require/js/main';
            require.config({
                paths: {
                    'main': mainJSURL
                }
            });
       </script>

      </c:otherwise>

</c:choose>
    <script>
    require(['main'], function(main) {
        
    });
   </script>