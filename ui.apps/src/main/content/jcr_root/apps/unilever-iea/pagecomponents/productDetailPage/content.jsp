<%@ include file="/apps/unilever-iea/commons/global.jsp" %>
<cq:setContentBundle />

<c:set var="title" value="${currentPage.title}" />
     <section class="o-content-wrapper o-content-border js-blur-bg js-main-wrapper clearfix">
        <!--excludesearch--><cq:include path="breadcrumb" resourceType="unilever-iea/components/breadcrumb"/><!--endexcludesearch-->
         <cq:include path="productoverview" resourceType="unilever-iea/components/productOverview"/>
       <section class="o-product-copy">
        	<cq:include path="productcopy_1" resourceType="unilever-iea/components/productCopy"/>
         	<cq:include path="productcopy_2" resourceType="unilever-iea/components/productCopy"/>
         	<cq:include path="productcopy_3" resourceType="unilever-iea/components/productCopy"/>
         	<cq:include path="productcopy_4" resourceType="unilever-iea/components/productCopy"/>
         	<cq:include path="productcopy_5" resourceType="unilever-iea/components/productCopy"/> 
           <cq:include path="smartlabel_par" resourceType="foundation/components/parsys" /> 
       </section>
       <cq:include path="reviews" resourceType="unilever-iea/components/reviews"/>
              <cq:include path="content_par" resourceType="foundation/components/parsys" />

	   <cq:include path="productrange" resourceType="unilever-iea/components/productRange"/>
	   <cq:include path="alternateproducts" resourceType="unilever-iea/components/multipleRelatedProductsAlternate"/>
       <cq:include path="multipleRelatedArticle" resourceType="unilever-iea/components/multipleRelatedArticles"/>
	   <!-- <div>Place Holder For Ratings and Review</div>
            <div>Place Holder For Multiple Related Products</div>
            <div>Place Holder For Multiple Related Articles</div> -->
       <cq:include path="producttags" resourceType="unilever-iea/components/productTags"/>


	<c:if test="${not properties.hideFooter}">
		  <!--excludesearch--> <cq:include script="footer.jsp" /><!--endexcludesearch-->
	</c:if>

    </section>
<cq:include path="productzoom" resourceType="unilever-iea/components/zoomModal"/>

	<%@include file="/apps/iea/commons/clientlibs.jsp"%>

	<c:set var="includeCategory" value="css" scope="request" />
    <cq:include path="includeLib"
                resourceType="iea/components/includeClientLib" />
<c:if test="${isAdaptive == 'false'}">
    <c:set var="includeCategory" value="js" scope="request" />
    <cq:include path="includeLib"
            resourceType="iea/components/includeClientLib" />
</c:if>
<cq:include script="/apps/unilever-iea/commons/shopnow.jsp"/>

